import { DataSourceOptions } from "typeorm"

export const ormConnectionOptions = {
  core: (): DataSourceOptions => ({
    name: "core",
    type: "postgres",
    url: process.env.CORE_DATABASE_URL,
    synchronize: false,
    logging: process.env.TYPEORM_LOG_QUERIES === "true",
    entities: ["dist/database/core/**/*.entity{.ts,.js}"],
    migrations: ["dist/database/core/migrations/**/*.{.ts,.js}"],
    subscribers: ["dist/database/core/subscriber/**/*.{.ts,.js}"],
  }),
}
