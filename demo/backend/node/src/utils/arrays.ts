import { array } from "fp-ts"
import * as S from "fp-ts/string"

export const subStringArrays = (array1: string[], array2: string[]) => {
  return array.difference(S.Eq)(array1, array2)
}

export const subArrays = <T>(
  array1: T[],
  array2: T[],
  eqFn: (a: T, b: T) => boolean
) => {
  return array.difference({
    equals: eqFn,
  })(array1, array2)
}
