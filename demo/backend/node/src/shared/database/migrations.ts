import { Log } from "@punks/backend-core"
import { DataSource, DataSourceOptions } from "typeorm"

export async function runDatabaseMigrations(
  connectionParams: DataSourceOptions,
  databaseName: string
) {
  const logger = Log.getLogger(`databaseMigrations ${databaseName}`)
  const connection = new DataSource(connectionParams)
  await connection.initialize()

  logger.info("runDatabaseMigrations -> started")
  const pendingMigrations = await connection.showMigrations()
  if (pendingMigrations) {
    await connection.runMigrations()
    logger.info("runDatabaseMigrations -> database migrations completed")
  } else {
    logger.info("runDatabaseMigrations -> no database migrations to run")
  }

  await connection.destroy()
  logger.info("runDatabaseMigrations -> completed")
}
