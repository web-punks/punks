import { ApiProperty } from "@nestjs/swagger"
import { IEntityFacet, IEntityFacetValue } from "@punks/backend-entity-manager"

export class NumericFacetValue implements IEntityFacetValue<number> {
  @ApiProperty()
  value: number

  @ApiProperty()
  label: string

  @ApiProperty()
  count: number
}

export class NumericFacet implements IEntityFacet<number> {
  @ApiProperty({ type: [NumericFacetValue] })
  values: NumericFacetValue[]
}

export class BooleanFacetValue implements IEntityFacetValue<boolean> {
  @ApiProperty()
  value: boolean

  @ApiProperty()
  label: string

  @ApiProperty()
  count: number
}

export class BooleanFacet implements IEntityFacet<boolean> {
  @ApiProperty({ type: [BooleanFacetValue] })
  values: BooleanFacetValue[]
}

export class StringFacetValue implements IEntityFacetValue<string> {
  @ApiProperty()
  value: string

  @ApiProperty()
  label: string

  @ApiProperty()
  count: number
}

export class StringFacet implements IEntityFacet<string> {
  @ApiProperty({ type: [StringFacetValue] })
  values: StringFacetValue[]
}
