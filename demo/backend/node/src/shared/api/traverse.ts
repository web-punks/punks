import { ApiProperty } from "@nestjs/swagger"
import { ITraverseFilters } from "@punks/backend-entity-manager"

export class EntityChildren {
  @ApiProperty()
  count: number
}

export type EntitiesChildrenMap = Record<string, EntityChildren>

export class EntitiesTraverseFilters implements ITraverseFilters {
  @ApiProperty({ required: false })
  rootOnly?: boolean

  @ApiProperty({ required: false })
  parentIds?: string[]
}
