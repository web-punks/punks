import { ApiProperty } from "@nestjs/swagger"
import { ITreeNodePaths } from "../models/trees"

export class EntityTreeNodePaths implements ITreeNodePaths {
  @ApiProperty({ type: [String] })
  idPath: string[]

  @ApiProperty({ type: [String] })
  namePath: string[]
}
