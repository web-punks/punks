import { Module } from "@nestjs/common"
import {
  EntityManagerModule,
  AuthenticationModule,
} from "@punks/backend-entity-manager"
import { EntityModules } from "../entities/modules"
import { CqrsModule } from "@nestjs/cqrs"

@Module({
  imports: [
    CqrsModule,
    EntityManagerModule,
    AuthenticationModule,
    ...EntityModules,
  ],
  exports: [
    CqrsModule,
    EntityManagerModule,
    AuthenticationModule,
    ...EntityModules,
  ],
})
export class SharedModule {}
