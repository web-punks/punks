import { Catch } from "@nestjs/common"
import {
  AppExceptionsFilterBase,
  PipelineInvocationError,
  RuntimeErrorInformation,
} from "@punks/backend-entity-manager"
import { Log } from "@punks/backend-core"

@Catch()
export class AppExceptionsFilter extends AppExceptionsFilterBase {
  protected async logError(info: RuntimeErrorInformation): Promise<void> {
    if (info.exception instanceof PipelineInvocationError) {
      this.logPipelineError(info.exception)
      return
    }

    Log.getLogger("[AppExceptionsFilter]").error(
      `Internal Server Error: ${info.exception?.message}`,
      {
        exception: {
          message: info.exception?.message,
          stack: info.exception.stack,
        },
      }
    )
  }

  protected logPipelineError(error: PipelineInvocationError) {
    Log.getLogger("[AppExceptionsFilter]").error(
      `Pipeline Error: ${error.errorType} - ${error.message}`,
      {
        details: error.rawResult.stepResults,
      }
    )
  }

  protected getCustomErrorStatusCode(exception: any): number | undefined {
    return undefined
  }
}
