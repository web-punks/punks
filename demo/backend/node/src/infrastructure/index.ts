import { AuthenticationInfrastructureModule } from "./authentication/module"
import { CacheInfrastructureModule } from "./cache/module"
import { EmailInfrastructureModule } from "./email/module"
import { EventsInfrastructureModule } from "./events/module"
import { VersioningInfrastructureModule } from "./versioning/module"

export const InfrastructureModules = [
  AuthenticationInfrastructureModule,
  CacheInfrastructureModule,
  EventsInfrastructureModule,
  EmailInfrastructureModule,
  VersioningInfrastructureModule,
]
