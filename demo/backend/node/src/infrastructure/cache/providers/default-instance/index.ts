import {
  WpCacheInstance,
  TypeormCacheInstance,
} from "@punks/backend-entity-manager"
import { AppCacheEntryEntity } from "@/database/core/entities/appCacheEntry.entity"
import { AppCacheEntryEntityManager } from "@/entities/appCacheEntries/appCacheEntry.manager"
import { Repository } from "typeorm"
import { AppCacheEntryRepository } from "@/database/core/repositories/appCacheEntry.repository"

@WpCacheInstance("default")
export class DefaultCacheInstance extends TypeormCacheInstance<AppCacheEntryEntity> {
  constructor(private readonly manager: AppCacheEntryEntityManager) {
    super("default")
  }

  protected getRepository(): Repository<AppCacheEntryEntity> {
    return (
      this.manager.getRepository() as AppCacheEntryRepository
    ).getInnerRepository()
  }
}
