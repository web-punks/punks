import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { CacheProviders } from "./providers"
import { AppCacheEntryEntityModule } from "@/entities/appCacheEntries/appCacheEntry.module"

@Module({
  imports: [SharedModule, AppCacheEntryEntityModule],
  providers: [...CacheProviders],
  exports: [...CacheProviders],
})
export class CacheInfrastructureModule {}
