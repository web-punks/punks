import { AppRoleEntityManager } from "../../../../entities/appRoles/appRole.manager"
import { AppRoleEntity } from "../../../../database/core/entities/appRole.entity"
import { AppPermissionEntity } from "../../../../database/core/entities/appPermission.entity"
import {
  DeepPartial,
  IAuthPermissionService,
  WpPermissionsService,
} from "@punks/backend-entity-manager"

@WpPermissionsService()
export class AuthPermissionService
  implements IAuthPermissionService<AppPermissionEntity>
{
  constructor(private readonly rolesManager: AppRoleEntityManager) {}

  async ensure(
    uid: string,
    data: DeepPartial<Omit<AppRoleEntity, "uid">>
  ): Promise<AppRoleEntity> {
    const role = await this.getByUid(uid)
    if (role) {
      return role
    }
    return await this.create({
      ...data,
      uid,
    })
  }

  async create(data: DeepPartial<AppRoleEntity>): Promise<AppRoleEntity> {
    const ref = await this.rolesManager.manager.create.execute(data)
    const role = await this.rolesManager.manager.get.execute(ref.id)
    if (!role) {
      throw new Error(`Role not found with id ${ref.id}`)
    }
    return role
  }

  async update(id: string, data: DeepPartial<AppRoleEntity>): Promise<void> {
    await this.rolesManager.manager.update.execute(id, data)
  }

  async delete(id: string): Promise<void> {
    await this.rolesManager.manager.delete.execute(id)
  }

  async getById(id: string): Promise<AppRoleEntity | undefined> {
    return await this.rolesManager.manager.get.execute(id)
  }

  async getByUid(uid: string): Promise<AppRoleEntity | undefined> {
    const { items } = await this.rolesManager.manager.search.execute({
      filters: {
        uid: [uid],
      },
    })
    return items[0]
  }
}
