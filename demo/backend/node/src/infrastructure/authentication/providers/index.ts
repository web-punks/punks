import { AuthPermissionService } from "./permissions"
import { AuthRolesService } from "./roles"
import { AuthUserRolesService } from "./userRoles"
import { AuthUserService } from "./users"

export const AuthProviders = [
  AuthRolesService,
  AuthPermissionService,
  AuthUserRolesService,
  AuthUserService,
]
