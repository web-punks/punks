export type AppPermissionItem = {
  uid: string
  name: string
}

export type AppRoleItem = {
  uid: string
  name: string
  permissions: AppPermissionItem[]
}
