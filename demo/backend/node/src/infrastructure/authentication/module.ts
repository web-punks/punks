import { Module } from "@nestjs/common"
import { SharedModule } from "@/shared/module"
import { AuthProviders } from "./providers"
import { AuthUserRolesService } from "./providers/userRoles"

@Module({
  imports: [SharedModule],
  providers: [...AuthProviders],
  exports: [AuthUserRolesService],
})
export class AuthenticationInfrastructureModule {}
