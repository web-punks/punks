import {
  IAuthOrganizationalUnit,
  IAuthenticationContext,
} from "@punks/backend-entity-manager"

export type AppUserInfo = {
  email: string
  firstName: string
  lastName: string
}

export type AppUserContext = {
  organizationId?: string
  organizationUid?: string
  tenantId: string
  tenantUid: string
  descendantOrganizationalUnits?: IAuthOrganizationalUnit[]
}

export type AppUserRole = {
  uid: string
}

export type AppUserPermission = {
  uid: string
}

export type AppUserOrganizationalUnit = {
  id: string
  uid: string
}

export class AppAuthContext implements IAuthenticationContext<AppUserContext> {
  isAuthenticated: boolean
  isAnonymous: boolean
  userId?: string
  userContext?: AppUserContext
  userInfo?: AppUserInfo
  userRoles?: AppUserRole[]
  userPermissions?: AppUserPermission[]
  userOrganizationalUnits?: AppUserOrganizationalUnit[]
}
