import { AppEventLogEntity } from "../../../../database/core/entities/appEventLog.entity"
import { AppEventLogEntityManager } from "../../../../entities/appEventLogs/appEventLog.manager"
import { WpEventsTracker, IEventsTracker } from "@punks/backend-entity-manager"

@WpEventsTracker()
export class EventTrackerProvider implements IEventsTracker<AppEventLogEntity> {
  constructor(private readonly eventLogs: AppEventLogEntityManager) {}

  async track(item: AppEventLogEntity): Promise<void> {
    await this.eventLogs.manager.create.execute({
      type: item.type,
      payload: item.payload,
      userId: item.userId,
      userName: item.userName,
      timestamp: item.timestamp,
    })
  }
}
