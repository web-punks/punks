import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { EventProviders } from "./providers"
import { AppEventLogEntityModule } from "../../entities/appEventLogs/appEventLog.module"

@Module({
  imports: [SharedModule, AppEventLogEntityModule],
  providers: [...EventProviders],
  exports: [],
})
export class EventsInfrastructureModule {}
