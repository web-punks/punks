import { Module } from "@nestjs/common"
import { SharedModule } from "@/shared/module"
import { VersioningProviders } from "./providers"

@Module({
  imports: [SharedModule],
  providers: [...VersioningProviders],
  exports: [],
})
export class VersioningInfrastructureModule {}
