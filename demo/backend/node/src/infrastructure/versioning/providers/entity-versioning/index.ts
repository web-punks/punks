import {
  newUuid,
  EntityVersionInput,
  IEntityVersioningProvider,
  IEntityVersioningResults,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchInput,
  WpEntityVersioningProvider,
} from "@punks/backend-entity-manager"
import { AppEntityVersionRepository } from "@/database/core/repositories/appEntityVersion.repository"

@WpEntityVersioningProvider()
export class EntityVersioningProvider implements IEntityVersioningProvider {
  constructor(
    private readonly entityVersionsRepo: AppEntityVersionRepository
  ) {}

  isEnabled(): boolean {
    return true
  }

  async createVersion<TEntity, TEntityId = string>(
    input: EntityVersionInput<TEntity, TEntityId>
  ): Promise<void> {
    await this.entityVersionsRepo.create({
      id: newUuid(),
      entityId: input.entityId as string,
      entityType: input.entityType,
      operationType: input.operationType,
      data: input.data,
      operationUserId: input.modifiedByUserId,
    })
  }

  async searchVersions<TEntity, TCursor = number>(
    input: IEntityVersionsSearchInput<TCursor>
  ): Promise<IEntityVersioningResults<TEntity, TCursor>> {
    const filter = {
      entityId: input.params.entity.entityId,
      entityType: input.entityType,
    }
    const results = await this.entityVersionsRepo.find({
      where: filter,
      skip: input.params.paging?.cursor
        ? (input.params.paging.cursor as number) * input.params.paging.pageSize
        : undefined,
      take: input.params.paging?.pageSize,
      order: {
        createdOn: input.params.sorting?.direction ?? "asc",
      },
    })

    const totResults = await this.entityVersionsRepo.count({
      where: filter,
    })

    const items = results.map((r) => r.data as TEntity)
    const cursor = (input.params.paging?.cursor as number) ?? 0
    const currentPageResults = items.length
    const hasMorePages = input.params.paging
      ? cursor * input.params.paging.pageSize + currentPageResults < totResults
      : false

    return {
      items,
      paging: input.params.paging
        ? ({
            pageIndex: cursor,
            pageSize: input.params.paging.pageSize,
            totItems: totResults,
            totPageItems: currentPageResults,
            totPages: Math.ceil(totResults / input.params.paging.pageSize),
            currentPageCursor: cursor,
            prevPageCursor: cursor > 0 ? cursor - 1 : undefined,
            nextPageCursor: hasMorePages ? cursor + 1 : undefined,
          } as IEntityVersionsResultsPaging<TCursor>)
        : undefined,
    }
  }
}
