import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppTenantEntity,
  AppTenantEntityId,
} from "../../database/core/entities/appTenant.entity"
import { AppTenantSearchParameters } from "../../entities/appTenants/appTenant.types"
import {
  AppTenantCreateData,
  AppTenantUpdateData,
  AppTenantSorting,
  AppTenantCursor,
  AppTenantSheetItem,
} from "../../entities/appTenants/appTenant.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appTenant")
export class AppTenantSerializer extends NestEntitySerializer<
  AppTenantEntity,
  AppTenantEntityId,
  AppTenantCreateData,
  AppTenantUpdateData,
  AppTenantSearchParameters,
  AppTenantSorting,
  AppTenantCursor,
  AppTenantSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appTenant", registry)
  }

  protected async loadEntities(
    filters: AppTenantSearchParameters
  ): Promise<AppTenantEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppTenantEntity[]
  ): Promise<AppTenantSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppTenantSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppTenantEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
