import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppTenantActions } from "./appTenant.actions"
import {
  AppTenantDto,
  AppTenantCreateDto,
  AppTenantUpdateDto,
} from "./appTenant.dto"
import {
  AppTenantExportRequest,
  AppTenantExportResponse,
  AppTenantSampleDownloadRequest,
  AppTenantSampleDownloadResponse,
  AppTenantSearchRequest,
  AppTenantSearchResponse,
  AppTenantVersionsSearchRequest,
  AppTenantVersionsSearchResponse,
} from "./appTenant.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appTenant")
export class AppTenantController {
  constructor(private readonly actions: AppTenantActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appTenantGet",
  })
  @ApiOkResponse({
    type: AppTenantDto,
  })
  async item(@Param("id") id: string): Promise<AppTenantDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appTenantCreate",
  })
  @ApiOkResponse({
    type: AppTenantDto,
  })
  async create(@Body() data: AppTenantCreateDto): Promise<AppTenantDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppTenantDto,
  })
  @ApiOperation({
    operationId: "appTenantUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppTenantUpdateDto
  ): Promise<AppTenantDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appTenantDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appTenantSearch",
  })
  @ApiOkResponse({
    type: AppTenantSearchResponse,
  })
  async search(
    @Body() request: AppTenantSearchRequest
  ): Promise<AppTenantSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appTenantImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appTenantExport",
  })
  @ApiOkResponse({
    type: AppTenantExportResponse,
  })
  async export(
    @Body() request: AppTenantExportRequest
  ): Promise<AppTenantExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appTenantSampleDownload",
  })
  @ApiOkResponse({
    type: AppTenantSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppTenantSampleDownloadRequest
  ): Promise<AppTenantSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appTenantVersions",
  })
  @ApiOkResponse({
    type: AppTenantVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppTenantVersionsSearchRequest
  ): Promise<AppTenantVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
