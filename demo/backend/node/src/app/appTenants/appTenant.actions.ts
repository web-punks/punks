import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppTenantCreateDto,
  AppTenantDto,
  AppTenantListItemDto,
  AppTenantUpdateDto,
} from "./appTenant.dto"
import {
  AppTenantEntity,
  AppTenantEntityId,
} from "../../database/core/entities/appTenant.entity"
import {
  AppTenantCursor,
  AppTenantFacets,
  AppTenantSorting,
} from "../../entities/appTenants/appTenant.models"
import {
  AppTenantDeleteParameters,
  AppTenantSearchParameters,
} from "../../entities/appTenants/appTenant.types"

@WpEntityActions("appTenant")
export class AppTenantActions extends NestEntityActions<
  AppTenantEntity,
  AppTenantEntityId,
  AppTenantCreateDto,
  AppTenantUpdateDto,
  AppTenantDto,
  AppTenantListItemDto,
  AppTenantDeleteParameters,
  AppTenantSearchParameters,
  AppTenantSorting,
  AppTenantCursor,
  AppTenantFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appTenant", registry)
  }
}
