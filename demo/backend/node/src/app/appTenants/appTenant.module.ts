import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppTenantActions } from "./appTenant.actions"
import { AppTenantController } from "./appTenant.controller"
import { AppTenantConverter } from "./appTenant.converter"
import { AppTenantEntityModule } from "../../entities/appTenants/appTenant.module"
import { AppTenantSerializer } from "./appTenant.serializer"

@Module({
  imports: [SharedModule, AppTenantEntityModule],
  providers: [
    AppTenantActions,
    AppTenantConverter,
    AppTenantSerializer,
  ],
  controllers: [AppTenantController],
})
export class AppTenantAppModule {}
