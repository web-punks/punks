import { ApiProperty } from "@nestjs/swagger"

export class AppTenantDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppTenantListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppTenantCreateDto {}

export class AppTenantUpdateDto {}
