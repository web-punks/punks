import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppTenantSearchParameters,
  AppTenantSearchResults,
  AppTenantVersionsSearchParameters,
  AppTenantVersionsSearchResults,
} from "../../entities/appTenants/appTenant.types"
import { AppTenantListItemDto } from "./appTenant.dto"

export class AppTenantSearchRequest {
  @ApiProperty()
  params: AppTenantSearchParameters
}

export class AppTenantSearchResponse extends AppTenantSearchResults<AppTenantListItemDto> {
  @ApiProperty({ type: [AppTenantListItemDto] })
  items: AppTenantListItemDto[]
}

export class AppTenantEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppTenantExportRequest {
  @ApiProperty()
  options: AppTenantEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppTenantSearchParameters
}

export class AppTenantExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppTenantSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppTenantSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppTenantVersionsSearchRequest {
  @ApiProperty()
  params: AppTenantVersionsSearchParameters
}

export class AppTenantVersionsSearchResponse extends AppTenantVersionsSearchResults<AppTenantListItemDto> {
  @ApiProperty({ type: [AppTenantListItemDto] })
  items: AppTenantListItemDto[]
}
