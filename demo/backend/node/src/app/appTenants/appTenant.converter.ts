import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppTenantCreateDto,
  AppTenantDto,
  AppTenantListItemDto,
  AppTenantUpdateDto,
} from "./appTenant.dto"
import { AppTenantEntity } from "../../database/core/entities/appTenant.entity"

@WpEntityConverter("appTenant")
export class AppTenantConverter
  implements
    IEntityConverter<
      AppTenantEntity,
      AppTenantDto,
      AppTenantListItemDto,
      AppTenantCreateDto,
      AppTenantUpdateDto
    >
{
  toListItemDto(entity: AppTenantEntity): AppTenantListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppTenantEntity): AppTenantDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppTenantCreateDto): DeepPartial<AppTenantEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppTenantUpdateDto): DeepPartial<AppTenantEntity> {
    return {
      ...input,
    }
  }
}
