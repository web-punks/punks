import { ApiProperty } from "@nestjs/swagger"

export class AppFileReferenceDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppFileReferenceListItemDto {
  @ApiProperty()
  id: string
}

export class AppFileReferenceCreateDto {}

export class AppFileReferenceUpdateDto {}
