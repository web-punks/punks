import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppFileReferenceActions } from "./appFileReference.actions"
import {
  AppFileReferenceDto,
  AppFileReferenceCreateDto,
  AppFileReferenceUpdateDto,
} from "./appFileReference.dto"
import {
  AppFileReferenceExportRequest,
  AppFileReferenceExportResponse,
  AppFileReferenceSampleDownloadRequest,
  AppFileReferenceSampleDownloadResponse,
  AppFileReferenceSearchRequest,
  AppFileReferenceSearchResponse,
  AppFileReferenceVersionsSearchRequest,
  AppFileReferenceVersionsSearchResponse,
} from "./appFileReference.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appFileReference")
export class AppFileReferenceController {
  constructor(private readonly actions: AppFileReferenceActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appFileReferenceGet",
  })
  @ApiOkResponse({
    type: AppFileReferenceDto,
  })
  async item(
    @Param("id") id: string
  ): Promise<AppFileReferenceDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appFileReferenceCreate",
  })
  @ApiOkResponse({
    type: AppFileReferenceDto,
  })
  async create(
    @Body() data: AppFileReferenceCreateDto
  ): Promise<AppFileReferenceDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppFileReferenceDto,
  })
  @ApiOperation({
    operationId: "appFileReferenceUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppFileReferenceUpdateDto
  ): Promise<AppFileReferenceDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appFileReferenceDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appFileReferenceSearch",
  })
  @ApiOkResponse({
    type: AppFileReferenceSearchResponse,
  })
  async search(
    @Body() request: AppFileReferenceSearchRequest
  ): Promise<AppFileReferenceSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appFileReferenceImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appFileReferenceExport",
  })
  @ApiOkResponse({
    type: AppFileReferenceExportResponse,
  })
  async export(
    @Body() request: AppFileReferenceExportRequest
  ): Promise<AppFileReferenceExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appFileReferenceSampleDownload",
  })
  @ApiOkResponse({
    type: AppFileReferenceSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppFileReferenceSampleDownloadRequest
  ): Promise<AppFileReferenceSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appFileReferenceVersions",
  })
  @ApiOkResponse({
    type: AppFileReferenceVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppFileReferenceVersionsSearchRequest
  ): Promise<AppFileReferenceVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
