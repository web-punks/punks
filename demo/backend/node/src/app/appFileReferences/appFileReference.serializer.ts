import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppFileReferenceEntity,
  AppFileReferenceEntityId,
} from "../../database/core/entities/appFileReference.entity"
import { AppFileReferenceSearchParameters } from "../../entities/appFileReferences/appFileReference.types"
import {
  AppFileReferenceCreateData,
  AppFileReferenceUpdateData,
  AppFileReferenceSorting,
  AppFileReferenceCursor,
  AppFileReferenceSheetItem,
} from "../../entities/appFileReferences/appFileReference.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appFileReference")
export class AppFileReferenceSerializer extends NestEntitySerializer<
  AppFileReferenceEntity,
  AppFileReferenceEntityId,
  AppFileReferenceCreateData,
  AppFileReferenceUpdateData,
  AppFileReferenceSearchParameters,
  AppFileReferenceSorting,
  AppFileReferenceCursor,
  AppFileReferenceSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appFileReference", registry)
  }

  protected async loadEntities(
    filters: AppFileReferenceSearchParameters
  ): Promise<AppFileReferenceEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppFileReferenceEntity[]
  ): Promise<AppFileReferenceSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppFileReferenceSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppFileReferenceEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
