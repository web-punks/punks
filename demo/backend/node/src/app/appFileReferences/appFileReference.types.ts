import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppFileReferenceSearchParameters,
  AppFileReferenceSearchResults,
  AppFileReferenceVersionsSearchParameters,
  AppFileReferenceVersionsSearchResults,
} from "../../entities/appFileReferences/appFileReference.types"
import { AppFileReferenceListItemDto } from "./appFileReference.dto"

export class AppFileReferenceSearchRequest {
  @ApiProperty()
  params: AppFileReferenceSearchParameters
}

export class AppFileReferenceSearchResponse extends AppFileReferenceSearchResults<AppFileReferenceListItemDto> {
  @ApiProperty({ type: [AppFileReferenceListItemDto] })
  items: AppFileReferenceListItemDto[]
}

export class AppFileReferenceEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppFileReferenceExportRequest {
  @ApiProperty()
  options: AppFileReferenceEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppFileReferenceSearchParameters
}

export class AppFileReferenceExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppFileReferenceSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppFileReferenceSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppFileReferenceVersionsSearchRequest {
  @ApiProperty()
  params: AppFileReferenceVersionsSearchParameters
}

export class AppFileReferenceVersionsSearchResponse extends AppFileReferenceVersionsSearchResults<AppFileReferenceListItemDto> {
  @ApiProperty({ type: [AppFileReferenceListItemDto] })
  items: AppFileReferenceListItemDto[]
}
