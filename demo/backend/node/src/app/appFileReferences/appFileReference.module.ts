import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppFileReferenceActions } from "./appFileReference.actions"
import { AppFileReferenceController } from "./appFileReference.controller"
import { AppFileReferenceConverter } from "./appFileReference.converter"
import { AppFileReferenceEntityModule } from "../../entities/appFileReferences/appFileReference.module"
import { AppFileReferenceSerializer } from "./appFileReference.serializer"

@Module({
  imports: [SharedModule, AppFileReferenceEntityModule],
  providers: [
    AppFileReferenceActions,
    AppFileReferenceConverter,
    AppFileReferenceSerializer,
  ],
  controllers: [AppFileReferenceController],
})
export class AppFileReferenceAppModule {}
