import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppFileReferenceCreateDto,
  AppFileReferenceDto,
  AppFileReferenceListItemDto,
  AppFileReferenceUpdateDto,
} from "./appFileReference.dto"
import {
  AppFileReferenceEntity,
  AppFileReferenceEntityId,
} from "../../database/core/entities/appFileReference.entity"
import {
  AppFileReferenceCursor,
  AppFileReferenceFacets,
  AppFileReferenceSorting,
} from "../../entities/appFileReferences/appFileReference.models"
import {
  AppFileReferenceDeleteParameters,
  AppFileReferenceSearchParameters,
} from "../../entities/appFileReferences/appFileReference.types"

@WpEntityActions("appFileReference")
export class AppFileReferenceActions extends NestEntityActions<
  AppFileReferenceEntity,
  AppFileReferenceEntityId,
  AppFileReferenceCreateDto,
  AppFileReferenceUpdateDto,
  AppFileReferenceDto,
  AppFileReferenceListItemDto,
  AppFileReferenceDeleteParameters,
  AppFileReferenceSearchParameters,
  AppFileReferenceSorting,
  AppFileReferenceCursor,
  AppFileReferenceFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appFileReference", registry)
  }
}
