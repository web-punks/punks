import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppFileReferenceCreateDto,
  AppFileReferenceDto,
  AppFileReferenceListItemDto,
  AppFileReferenceUpdateDto,
} from "./appFileReference.dto"
import { AppFileReferenceEntity } from "../../database/core/entities/appFileReference.entity"

@WpEntityConverter("appFileReference")
export class AppFileReferenceConverter
  implements
    IEntityConverter<
      AppFileReferenceEntity,
      AppFileReferenceDto,
      AppFileReferenceListItemDto,
      AppFileReferenceCreateDto,
      AppFileReferenceUpdateDto
    >
{
  toListItemDto(entity: AppFileReferenceEntity): AppFileReferenceListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppFileReferenceEntity): AppFileReferenceDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppFileReferenceCreateDto): DeepPartial<AppFileReferenceEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppFileReferenceUpdateDto): DeepPartial<AppFileReferenceEntity> {
    return {
      ...input,
    }
  }
}
