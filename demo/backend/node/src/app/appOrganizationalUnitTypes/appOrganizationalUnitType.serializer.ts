import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId,
} from "../../database/core/entities/appOrganizationalUnitType.entity"
import { AppOrganizationalUnitTypeSearchParameters } from "../../entities/appOrganizationalUnitTypes/appOrganizationalUnitType.types"
import {
  AppOrganizationalUnitTypeCreateData,
  AppOrganizationalUnitTypeUpdateData,
  AppOrganizationalUnitTypeSorting,
  AppOrganizationalUnitTypeCursor,
  AppOrganizationalUnitTypeSheetItem,
} from "../../entities/appOrganizationalUnitTypes/appOrganizationalUnitType.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appOrganizationalUnitType")
export class AppOrganizationalUnitTypeSerializer extends NestEntitySerializer<
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId,
  AppOrganizationalUnitTypeCreateData,
  AppOrganizationalUnitTypeUpdateData,
  AppOrganizationalUnitTypeSearchParameters,
  AppOrganizationalUnitTypeSorting,
  AppOrganizationalUnitTypeCursor,
  AppOrganizationalUnitTypeSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganizationalUnitType", registry)
  }

  protected async loadEntities(
    filters: AppOrganizationalUnitTypeSearchParameters
  ): Promise<AppOrganizationalUnitTypeEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppOrganizationalUnitTypeEntity[]
  ): Promise<AppOrganizationalUnitTypeSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppOrganizationalUnitTypeSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppOrganizationalUnitTypeEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
