import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppOrganizationalUnitTypeActions } from "./appOrganizationalUnitType.actions"
import { AppOrganizationalUnitTypeController } from "./appOrganizationalUnitType.controller"
import { AppOrganizationalUnitTypeConverter } from "./appOrganizationalUnitType.converter"
import { AppOrganizationalUnitTypeEntityModule } from "../../entities/appOrganizationalUnitTypes/appOrganizationalUnitType.module"
import { AppOrganizationalUnitTypeSerializer } from "./appOrganizationalUnitType.serializer"
import { AppOrganizationalUnitTypesTemplates } from "./templates"
import { AppOrganizationalUnitTypeChildEntityModule } from "@/entities/appOrganizationalUnitTypeChildren/appOrganizationalUnitTypeChild.module"

@Module({
  imports: [
    SharedModule,
    AppOrganizationalUnitTypeEntityModule,
    AppOrganizationalUnitTypeChildEntityModule,
  ],
  providers: [
    AppOrganizationalUnitTypeActions,
    AppOrganizationalUnitTypeConverter,
    AppOrganizationalUnitTypeSerializer,
    ...AppOrganizationalUnitTypesTemplates,
  ],
  controllers: [AppOrganizationalUnitTypeController],
})
export class AppOrganizationalUnitTypeAppModule {}
