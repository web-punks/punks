import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitTypeCreateDto,
  AppOrganizationalUnitTypeDto,
  AppOrganizationalUnitTypeListItemDto,
  AppOrganizationalUnitTypeUpdateDto,
} from "./appOrganizationalUnitType.dto"
import {
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId,
} from "../../database/core/entities/appOrganizationalUnitType.entity"
import {
  AppOrganizationalUnitTypeCursor,
  AppOrganizationalUnitTypeFacets,
  AppOrganizationalUnitTypeSorting,
} from "../../entities/appOrganizationalUnitTypes/appOrganizationalUnitType.models"
import {
  AppOrganizationalUnitTypeDeleteParameters,
  AppOrganizationalUnitTypeSearchParameters,
} from "../../entities/appOrganizationalUnitTypes/appOrganizationalUnitType.types"
import { AppOrganizationalUnitTypeUpdateTemplate } from "./templates/ou-type-update"
import { AppOrganizationalUnitTypeCreateTemplate } from "./templates/ou-type-create"

@WpEntityActions("appOrganizationalUnitType")
export class AppOrganizationalUnitTypeActions extends NestEntityActions<
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId,
  AppOrganizationalUnitTypeCreateDto,
  AppOrganizationalUnitTypeUpdateDto,
  AppOrganizationalUnitTypeDto,
  AppOrganizationalUnitTypeListItemDto,
  AppOrganizationalUnitTypeDeleteParameters,
  AppOrganizationalUnitTypeSearchParameters,
  AppOrganizationalUnitTypeSorting,
  AppOrganizationalUnitTypeCursor,
  AppOrganizationalUnitTypeFacets
> {
  constructor(
    registry: EntityManagerRegistry,
    private readonly createPipeline: AppOrganizationalUnitTypeCreateTemplate,
    private readonly updatePipeline: AppOrganizationalUnitTypeUpdateTemplate
  ) {
    super("appOrganizationalUnitType", registry)
  }

  async create(
    data: AppOrganizationalUnitTypeCreateDto
  ): Promise<AppOrganizationalUnitTypeDto> {
    const entity = await this.createPipeline.invoke({
      data,
    })
    return this.converter.toEntityDto(entity)
  }

  async update(
    id: AppOrganizationalUnitTypeEntityId,
    data: AppOrganizationalUnitTypeUpdateDto
  ): Promise<AppOrganizationalUnitTypeDto> {
    const entity = await this.updatePipeline.invoke({
      id,
      data,
    })
    return this.converter.toEntityDto(entity)
  }
}
