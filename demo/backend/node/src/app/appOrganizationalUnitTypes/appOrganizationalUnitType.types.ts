import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitTypeSearchParameters,
  AppOrganizationalUnitTypeSearchResults,
  AppOrganizationalUnitTypeVersionsSearchParameters,
  AppOrganizationalUnitTypeVersionsSearchResults,
} from "../../entities/appOrganizationalUnitTypes/appOrganizationalUnitType.types"
import { AppOrganizationalUnitTypeListItemDto } from "./appOrganizationalUnitType.dto"

export class AppOrganizationalUnitTypeSearchRequest {
  @ApiProperty()
  params: AppOrganizationalUnitTypeSearchParameters
}

export class AppOrganizationalUnitTypeSearchResponse extends AppOrganizationalUnitTypeSearchResults<AppOrganizationalUnitTypeListItemDto> {
  @ApiProperty({ type: [AppOrganizationalUnitTypeListItemDto] })
  items: AppOrganizationalUnitTypeListItemDto[]
}

export class AppOrganizationalUnitTypeEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppOrganizationalUnitTypeExportRequest {
  @ApiProperty()
  options: AppOrganizationalUnitTypeEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppOrganizationalUnitTypeSearchParameters
}

export class AppOrganizationalUnitTypeExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppOrganizationalUnitTypeSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppOrganizationalUnitTypeSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppOrganizationalUnitTypeVersionsSearchRequest {
  @ApiProperty()
  params: AppOrganizationalUnitTypeVersionsSearchParameters
}

export class AppOrganizationalUnitTypeVersionsSearchResponse extends AppOrganizationalUnitTypeVersionsSearchResults<AppOrganizationalUnitTypeListItemDto> {
  @ApiProperty({ type: [AppOrganizationalUnitTypeListItemDto] })
  items: AppOrganizationalUnitTypeListItemDto[]
}
