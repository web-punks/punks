import { ApiProperty } from "@nestjs/swagger"
import { AppOrganizationReferenceDto } from "../appOrganizations/appOrganization.dto"

export class AppOrganizationalUnitTypeAllowedChildType {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string
}

export class AppOrganizationalUnitTypeDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string

  @ApiProperty()
  allowAsRoot: boolean

  @ApiProperty({ type: [AppOrganizationalUnitTypeAllowedChildType] })
  allowedChildrenTypes: AppOrganizationalUnitTypeAllowedChildType[]

  @ApiProperty()
  organization: AppOrganizationReferenceDto

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppOrganizationalUnitTypeListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string

  @ApiProperty()
  allowAsRoot: boolean

  @ApiProperty({ type: [AppOrganizationalUnitTypeAllowedChildType] })
  allowedChildrenTypes: AppOrganizationalUnitTypeAllowedChildType[]

  @ApiProperty()
  organization: AppOrganizationReferenceDto

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppOrganizationalUnitTypeAllowedChildTypeInput {
  @ApiProperty()
  id: string
}

export class AppOrganizationalUnitTypeCreateDto {
  @ApiProperty()
  organizationId: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string

  @ApiProperty()
  allowAsRoot: boolean

  @ApiProperty({
    type: [AppOrganizationalUnitTypeAllowedChildTypeInput],
    required: false,
  })
  allowedChildrenTypes?: AppOrganizationalUnitTypeAllowedChildTypeInput[]
}

export class AppOrganizationalUnitTypeUpdateDto {
  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string

  @ApiProperty()
  allowAsRoot: boolean

  @ApiProperty({
    type: [AppOrganizationalUnitTypeAllowedChildTypeInput],
    required: false,
  })
  allowedChildrenTypes?: AppOrganizationalUnitTypeAllowedChildTypeInput[]
}
