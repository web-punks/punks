import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppOrganizationalUnitTypeActions } from "./appOrganizationalUnitType.actions"
import {
  AppOrganizationalUnitTypeDto,
  AppOrganizationalUnitTypeCreateDto,
  AppOrganizationalUnitTypeUpdateDto,
} from "./appOrganizationalUnitType.dto"
import {
  AppOrganizationalUnitTypeExportRequest,
  AppOrganizationalUnitTypeExportResponse,
  AppOrganizationalUnitTypeSampleDownloadRequest,
  AppOrganizationalUnitTypeSampleDownloadResponse,
  AppOrganizationalUnitTypeSearchRequest,
  AppOrganizationalUnitTypeSearchResponse,
  AppOrganizationalUnitTypeVersionsSearchRequest,
  AppOrganizationalUnitTypeVersionsSearchResponse,
} from "./appOrganizationalUnitType.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appOrganizationalUnitType")
export class AppOrganizationalUnitTypeController {
  constructor(private readonly actions: AppOrganizationalUnitTypeActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeGet",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeDto,
  })
  async item(
    @Param("id") id: string
  ): Promise<AppOrganizationalUnitTypeDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeCreate",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeDto,
  })
  async create(
    @Body() data: AppOrganizationalUnitTypeCreateDto
  ): Promise<AppOrganizationalUnitTypeDto> {
    return await this.actions.create(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeDto,
  })
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppOrganizationalUnitTypeUpdateDto
  ): Promise<AppOrganizationalUnitTypeDto> {
    return await this.actions.update(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeSearch",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeSearchResponse,
  })
  async search(
    @Body() request: AppOrganizationalUnitTypeSearchRequest
  ): Promise<AppOrganizationalUnitTypeSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeExport",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeExportResponse,
  })
  async export(
    @Body() request: AppOrganizationalUnitTypeExportRequest
  ): Promise<AppOrganizationalUnitTypeExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeSampleDownload",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppOrganizationalUnitTypeSampleDownloadRequest
  ): Promise<AppOrganizationalUnitTypeSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeVersions",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppOrganizationalUnitTypeVersionsSearchRequest
  ): Promise<AppOrganizationalUnitTypeVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
