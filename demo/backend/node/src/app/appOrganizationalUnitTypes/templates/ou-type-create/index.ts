import {
  EntityReference,
  IPipelineTemplateBuilder,
  NestPipelineTemplate,
  PipelineDefinition,
  WpPipeline,
} from "@punks/backend-entity-manager"
import { AppAuthContext } from "@/infrastructure/authentication"
import { AppOrganizationalUnitTypeCreateInput } from "./models"
import { AppOrganizationalUnitTypeEntityManager } from "@/entities/appOrganizationalUnitTypes/appOrganizationalUnitType.manager"
import {
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId,
} from "@/database/core/entities/appOrganizationalUnitType.entity"
import { AppOrganizationalUnitTypeChildEntityManager } from "@/entities/appOrganizationalUnitTypeChildren/appOrganizationalUnitTypeChild.manager"

@WpPipeline("organizationalUnitTypeCreate")
export class AppOrganizationalUnitTypeCreateTemplate extends NestPipelineTemplate<
  AppOrganizationalUnitTypeCreateInput,
  AppOrganizationalUnitTypeEntity,
  AppAuthContext
> {
  constructor(
    private readonly otTypes: AppOrganizationalUnitTypeEntityManager,
    private readonly otTypeChildren: AppOrganizationalUnitTypeChildEntityManager
  ) {
    super()
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<
      AppOrganizationalUnitTypeCreateInput,
      AppAuthContext
    >
  ): PipelineDefinition<
    AppOrganizationalUnitTypeCreateInput,
    AppOrganizationalUnitTypeEntity,
    AppAuthContext
  > {
    return builder
      .addStep<EntityReference<AppOrganizationalUnitTypeEntityId>>((step) => {
        step.addOperation({
          name: "OU type create",
          action: async (input) => {
            this.logger.info("OU type create", {
              data: input.data,
            })
            return await this.otTypes.manager.create.execute({
              allowAsRoot: input.data.allowAsRoot,
              name: input.data.name,
              uid: input.data.uid,
              organization: {
                id: input.data.organizationId,
              },
            })
          },
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "OU type children insert",
          action: async (input, state) => {
            for (const child of state.pipelineInput.data.allowedChildrenTypes ??
              []) {
              this.logger.info("OU type children add", {
                contactId: input.id,
                child,
              })
              await this.otTypeChildren.manager.create.execute({
                parentType: {
                  id: input.id,
                },
                childType: {
                  id: child.id,
                },
              })
            }
          },
          skipIf: (_, state) => !state.pipelineInput.data.allowedChildrenTypes,
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Map output",
          action: async (input, state) => {
            return await this.otTypes.manager.get.execute(
              this.utils.getStepOutput<
                EntityReference<AppOrganizationalUnitTypeEntityId>
              >(state, 0).id
            )
          },
        })
      })
      .complete()
  }
}
