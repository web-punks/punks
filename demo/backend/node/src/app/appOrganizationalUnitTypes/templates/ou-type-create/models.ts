import { AppOrganizationalUnitTypeCreateDto } from "../../appOrganizationalUnitType.dto"

export type AppOrganizationalUnitTypeCreateInput = {
  data: AppOrganizationalUnitTypeCreateDto
}
