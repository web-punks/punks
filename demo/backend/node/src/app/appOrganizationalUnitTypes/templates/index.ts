import { AppOrganizationalUnitTypeCreateTemplate } from "./ou-type-create"
import { AppOrganizationalUnitTypeUpdateTemplate } from "./ou-type-update"

export const AppOrganizationalUnitTypesTemplates = [
  AppOrganizationalUnitTypeCreateTemplate,
  AppOrganizationalUnitTypeUpdateTemplate,
]
