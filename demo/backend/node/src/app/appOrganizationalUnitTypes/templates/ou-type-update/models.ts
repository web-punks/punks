import { AppOrganizationalUnitTypeUpdateDto } from "../../appOrganizationalUnitType.dto"

export type AppOrganizationalUnitTypeUpdateInput = {
  id: string
  data: AppOrganizationalUnitTypeUpdateDto
}
