import { difference } from "fp-ts/Array"
import * as S from "fp-ts/string"
import {
  EntityReference,
  IPipelineTemplateBuilder,
  NestPipelineTemplate,
  PipelineDefinition,
  WpPipeline,
} from "@punks/backend-entity-manager"
import { AppAuthContext } from "@/infrastructure/authentication"
import { AppOrganizationalUnitTypeUpdateInput } from "./models"
import { AppOrganizationalUnitTypeEntityManager } from "@/entities/appOrganizationalUnitTypes/appOrganizationalUnitType.manager"
import {
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId,
} from "@/database/core/entities/appOrganizationalUnitType.entity"
import { AppOrganizationalUnitTypeChildEntityManager } from "@/entities/appOrganizationalUnitTypeChildren/appOrganizationalUnitTypeChild.manager"

@WpPipeline("organizationalUnitTypeUpdate")
export class AppOrganizationalUnitTypeUpdateTemplate extends NestPipelineTemplate<
  AppOrganizationalUnitTypeUpdateInput,
  AppOrganizationalUnitTypeEntity,
  AppAuthContext
> {
  constructor(
    private readonly otTypes: AppOrganizationalUnitTypeEntityManager,
    private readonly otTypeChildren: AppOrganizationalUnitTypeChildEntityManager
  ) {
    super()
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<
      AppOrganizationalUnitTypeUpdateInput,
      AppAuthContext
    >
  ): PipelineDefinition<
    AppOrganizationalUnitTypeUpdateInput,
    AppOrganizationalUnitTypeEntity,
    AppAuthContext
  > {
    return builder
      .addStep<EntityReference<AppOrganizationalUnitTypeEntityId>>((step) => {
        step.addOperation({
          name: "OU type update",
          action: async (input) => {
            this.logger.info("OU type update", {
              data: input.data,
            })
            return await this.otTypes.manager.update.execute(input.id, {
              allowAsRoot: input.data.allowAsRoot,
              name: input.data.name,
              uid: input.data.uid,
            })
          },
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "OU type children update",
          action: async (input, state) => {
            const currentChildren =
              await this.otTypeChildren.manager.search.execute({
                filters: {
                  parentTypeId: state.pipelineInput.id,
                },
                relations: {
                  childType: true,
                },
              })

            const currentChildrenIds = currentChildren.items.map(
              (x) => x.childType.id
            )
            const newChildrenIds =
              state.pipelineInput.data.allowedChildrenTypes.map((x) => x.id)

            const missingChildrenIds = difference(S.Eq)(
              newChildrenIds,
              currentChildrenIds
            )

            for (const missingChildId of missingChildrenIds) {
              this.logger.info("OU type children update add", {
                contactId: input.id,
                missingChildId,
              })
              await this.otTypeChildren.manager.create.execute({
                parentType: {
                  id: input.id,
                },
                childType: {
                  id: missingChildId,
                },
              })
            }

            const exceedingChildrenIds = difference(S.Eq)(
              currentChildrenIds,
              newChildrenIds
            )
            for (const exceedingChildrenId of exceedingChildrenIds) {
              this.logger.info("OU type children update remove", {
                contactId: input.id,
                exceedingChildrenId,
              })
              await this.otTypeChildren.manager.delete.execute(
                currentChildren.items.find(
                  (x) => x.childType.id === exceedingChildrenId
                ).id
              )
            }
          },
          skipIf: (_, state) => !state.pipelineInput.data.allowedChildrenTypes,
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Map output",
          action: async (input, state) => {
            return await this.otTypes.manager.get.execute(
              this.utils.getStepOutput<
                EntityReference<AppOrganizationalUnitTypeEntityId>
              >(state, 0).id
            )
          },
        })
      })
      .complete()
  }
}
