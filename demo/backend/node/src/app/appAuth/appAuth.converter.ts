import { Injectable } from "@nestjs/common"
import { UserProfileResponse } from "./appAuth.dto"
import { AppUserEntity } from "@/database/core/entities/appUser.entity"
import { IAuthRole } from "@punks/backend-entity-manager"
import { AppTenantEntity } from "@/database/core/entities/appTenant.entity"
import { AppOrganizationEntity } from "@/database/core/entities/appOrganization.entity"
import { AppRoleEntity } from "@/database/core/entities/appRole.entity"
import { AppPermissionEntity } from "@/database/core/entities/appPermission.entity"
import { AppRoleType } from "@/middleware/authentication/roles"
import { AppPermissionType } from "@/middleware/authentication/permissions"

@Injectable()
export class AppAuthConverter {
  toUserProfile({
    user,
    userRoles,
    organizations,
    tenants,
    roles,
    permissions,
  }: {
    user: AppUserEntity
    userRoles: IAuthRole[]
    tenants: AppTenantEntity[]
    organizations: AppOrganizationEntity[]
    roles: AppRoleEntity[]
    permissions: AppPermissionEntity[]
  }): UserProfileResponse {
    return {
      user: {
        id: user.id,
        userName: user.userName,
        email: user.email,
        disabled: user.disabled,
        verified: user.verified,
        profile: {
          firstName: user.profile.firstName,
          lastName: user.profile.lastName,
        },
        organization: user.organization
          ? {
              id: user.organization.id,
              name: user.organization.name,
              uid: user.organization.uid,
            }
          : undefined,
        tenant: user.tenant
          ? {
              id: user.tenant.id,
              uid: user.tenant.uid,
              name: user.tenant.name,
            }
          : undefined,
      },
      roles: userRoles.map((role: AppRoleEntity) => ({
        id: role.id,
        name: role.name,
        enabled: role.enabled,
        uid: role.uid as AppRoleType,
      })),
      permissions: permissions.map((permission) => ({
        id: permission.id,
        name: permission.name,
        uid: permission.uid as AppPermissionType,
      })),
      resources: {
        organizations: organizations.map((org) => ({
          id: org.id,
          name: org.name,
          uid: org.uid,
        })),
        tenants: tenants.map((tenant) => ({
          id: tenant.id,
          name: tenant.name,
          uid: tenant.uid,
        })),
        roles: roles.map((role) => ({
          id: role.id,
          name: role.name,
          enabled: role.enabled,
          uid: role.uid as AppRoleType,
        })),
      },
    }
  }
}
