import { Injectable } from "@nestjs/common"
import {
  AppSessionService,
  IAuthOrganizationalUnit,
  IAuthPermission,
  IAuthRole,
  IAuthUser,
  IAuthenticationContext,
  IAuthenticationContextProvider,
} from "@punks/backend-entity-manager"
import { AppOrganizationRepository } from "@/database/core/repositories/appOrganization.repository"
import { AppOrganizationEntityManager } from "@/entities/appOrganizations/appOrganization.manager"
import { AppUserContext } from "@/infrastructure/authentication"

@Injectable()
export class AppAuthContextProvider
  implements IAuthenticationContextProvider<AppUserContext>
{
  constructor(
    private readonly appSessionService: AppSessionService,
    private readonly organizations: AppOrganizationEntityManager
  ) {}

  async getContext(): Promise<IAuthenticationContext<AppUserContext>> {
    const auth = this.getRequestAuth()

    if (!auth) {
      return Promise.resolve({
        isAuthenticated: false,
        isAnonymous: true,
        userId: undefined,
        userContext: undefined,
        userInfo: undefined,
        userRoles: [],
        userPermissions: [],
        userOrganizationalUnits: [],
      })
    }

    const organizationId = auth.organizationId ?? auth.user?.organization?.id
    const organization = organizationId
      ? await this.organizations
          .getRepository<AppOrganizationRepository>()
          .getOrganization(organizationId)
      : undefined

    return Promise.resolve({
      isAuthenticated: true,
      isAnonymous: false,
      userId: auth.user.id,
      userContext: organization
        ? {
            organizationId: organization.id,
            organizationUid: organization.uid,
            tenantId: organization.tenant.id,
            tenantUid: organization.tenant.uid,
            descendantOrganizationalUnits: auth.descendantOrganizationalUnits,
          }
        : undefined,
      userInfo: auth.user.profile
        ? {
            email: auth.user.email,
            firstName: auth.user.profile.firstName,
            lastName: auth.user.profile.lastName,
          }
        : undefined,
      userRoles:
        auth.roles?.map((x) => ({
          id: x.id,
          uid: x.uid,
        })) ?? [],
      userPermissions:
        auth.permissions?.map((x) => ({
          id: x.id,
          uid: x.uid,
        })) ?? [],
      userOrganizationalUnits:
        auth.organizationalUnits?.map((x) => ({
          id: x.id,
          uid: x.uid,
        })) ?? [],
    })
  }

  private getRequestAuth() {
    const request = this.appSessionService.retrieveRequest() as any
    const auth = request?.auth
    if (!auth) {
      return undefined
    }

    return {
      user: auth.user as IAuthUser,
      roles: auth.roles as IAuthRole[],
      permissions: auth.permissions as IAuthPermission[],
      organizationalUnits:
        auth.organizationalUnits as IAuthOrganizationalUnit[],
      descendantOrganizationalUnits:
        auth.descendantOrganizationalUnits as IAuthOrganizationalUnit[],
      organizationId: request.headers["app-organization-id"] as string,
    }
  }
}
