import { Body, Controller, Post, Get, Param } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  Authenticated,
  CurrentUser,
  CurrentUserData,
  Public,
} from "@punks/backend-entity-manager"
import { AppAuthActions } from "./appAuth.actions"
import {
  UserEmailVerifyCompleteRequest,
  UserEmailVerifyRequest,
  UserImpersonateResponse,
  UserLoginRequest,
  UserLoginResponse,
  UserPasswordResetCompleteRequest,
  UserPasswordResetRequest,
  UserPasswordResetResponse,
  UserProfileResponse,
  UserRegisterRequest,
  UserRegisterResponse,
} from "./appAuth.dto"
import { UserImpersonate } from "@/middleware/authentication/guards"

@Public()
@Controller("v1/appAuth")
export class AppAuthController {
  constructor(private readonly actions: AppAuthActions) {}

  @Authenticated()
  @Get("profile")
  @ApiOperation({
    operationId: "userProfile",
  })
  @ApiOkResponse({
    type: UserProfileResponse,
  })
  async userProfile(
    @CurrentUser() data: CurrentUserData
  ): Promise<UserProfileResponse> {
    return await this.actions.getUserProfile(data)
  }

  @Post("login")
  @ApiOperation({
    operationId: "userLogin",
  })
  @ApiOkResponse({
    type: UserLoginResponse,
  })
  async login(@Body() data: UserLoginRequest): Promise<UserLoginResponse> {
    return await this.actions.login(data)
  }

  @Post("register")
  @ApiOperation({
    operationId: "userRegister",
  })
  @ApiOkResponse({
    type: UserRegisterResponse,
  })
  async register(
    @Body() data: UserRegisterRequest
  ): Promise<UserRegisterResponse> {
    return await this.actions.register(data)
  }

  @Post("verify")
  @ApiOperation({
    operationId: "userVerify",
  })
  async verify(@Body() data: UserEmailVerifyRequest): Promise<void> {
    await this.actions.userVerifyRequest(data)
  }

  @Post("verifyComplete")
  @ApiOperation({
    operationId: "userVerifyComplete",
  })
  async verifyComplete(
    @Body() data: UserEmailVerifyCompleteRequest
  ): Promise<void> {
    await this.actions.userVerifyComplete(data)
  }

  @Post("passwordReset")
  @ApiOperation({
    operationId: "userPasswordReset",
  })
  @ApiOkResponse({
    type: UserPasswordResetResponse,
  })
  async passwordReset(
    @Body() data: UserPasswordResetRequest
  ): Promise<UserPasswordResetResponse> {
    return await this.actions.passwordReset(data)
  }

  @Post("passwordResetComplete")
  @ApiOperation({
    operationId: "userPasswordResetComplete",
  })
  async passwordResetComplete(
    @Body() data: UserPasswordResetCompleteRequest
  ): Promise<void> {
    await this.actions.passwordResetComplete(data)
  }

  @UserImpersonate()
  @Post("impersonate/:userId")
  @ApiOperation({
    operationId: "impersonate",
  })
  @ApiOkResponse({
    type: UserImpersonateResponse,
  })
  async impersonate(
    @Param("userId") userId: string
  ): Promise<UserImpersonateResponse> {
    return await this.actions.impersonate(userId)
  }
}
