import { Injectable } from "@nestjs/common"
import {
  AuthenticationService,
  CurrentUserData,
} from "@punks/backend-entity-manager"
import {
  UserEmailVerifyCompleteRequest,
  UserEmailVerifyRequest,
  UserImpersonateResponse,
  UserLoginRequest,
  UserPasswordResetCompleteRequest,
  UserPasswordResetRequest,
  UserProfileResponse,
  UserRegisterRequest,
} from "./appAuth.dto"
import { AppAuthConverter } from "./appAuth.converter"
import { AppUserRepository } from "@/database/core/repositories/appUser.repository"
import { AppOrganizationRepository } from "@/database/core/repositories/appOrganization.repository"
import { AppRoleEntityManager } from "@/entities/appRoles/appRole.manager"
import { AuthUserRolesService } from "@/infrastructure/authentication/providers/userRoles"

@Injectable()
export class AppAuthActions {
  constructor(
    private readonly converter: AppAuthConverter,
    private readonly auth: AuthenticationService,
    private readonly users: AppUserRepository,
    private readonly roles: AppRoleEntityManager,
    private readonly organizations: AppOrganizationRepository,
    private readonly userRolesService: AuthUserRolesService
  ) {}

  async getUserProfile(data: CurrentUserData): Promise<UserProfileResponse> {
    const user = await this.users.getUserDetails(data.user.id)
    const roles = await this.roles.manager.search.execute({})
    const permissions = await this.userRolesService.getUserPermissions(user.id)
    return this.converter.toUserProfile({
      user,
      userRoles: data.roles,
      organizations: user.organization
        ? [user.organization]
        : await this.organizations.getTenantOrganizations(user.tenant.id),
      tenants: [user.tenant],
      roles: roles.items,
      permissions,
    })
  }

  async login(data: UserLoginRequest) {
    return await this.auth.userLogin({
      userName: data.userName,
      password: data.password,
      context: data.userContext,
    })
  }

  async register(data: UserRegisterRequest) {
    return await this.auth.userRegister({
      email: data.email,
      userName: data.userName,
      password: data.password,
      registrationInfo: data.data,
      context: data.userContext,
      callback: data.callback,
      languageCode: data.languageCode,
    })
  }

  async passwordReset(data: UserPasswordResetRequest) {
    return await this.auth.userPasswordResetRequest({
      email: data.userName,
      context: data.userContext,
      callback: data.callback,
      languageCode: data.languageCode,
    })
  }

  async passwordResetComplete(data: UserPasswordResetCompleteRequest) {
    return await this.auth.userPasswordResetFinalize({
      token: data.token,
      newPassword: data.newPassword,
      temporary: false,
    })
  }

  async impersonate(userId: string): Promise<UserImpersonateResponse> {
    const user = await this.auth.usersService.getById(userId)
    return await this.auth.userImpersonate({
      userName: user.userName,
    })
  }

  async userVerifyRequest(data: UserEmailVerifyRequest) {
    return await this.auth.userVerifyRequest({
      email: data.email,
      context: data.userContext,
      callback: data.callback,
      languageCode: data.languageCode,
    })
  }

  async userVerifyComplete(data: UserEmailVerifyCompleteRequest) {
    return await this.auth.userVerifyComplete({
      token: data.token,
    })
  }
}
