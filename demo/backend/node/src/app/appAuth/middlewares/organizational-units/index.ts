import { AppOrganizationalUnitService } from "@/entities/appOrganizationalUnits/appOrganizationalUnit.service"
import {
  IAuthOrganizationalUnit,
  IAuthenticationData,
  IAuthenticationMiddleware,
  WpGlobalAuthenticationMiddleware,
} from "@punks/backend-entity-manager"

@WpGlobalAuthenticationMiddleware("organizationalUnits")
export class AppAuthenticationMiddleware
  implements IAuthenticationMiddleware<IAuthenticationData, any>
{
  constructor(
    private readonly organizationalUnitService: AppOrganizationalUnitService
  ) {}

  async processContext(context: IAuthenticationData): Promise<{
    descendantOrganizationalUnits: IAuthOrganizationalUnit[]
  }> {
    const descendantOrganizationalUnits = context.organizationalUnits?.length
      ? await this.organizationalUnitService.getOrganizationalUnitsDescendants(
          context.organizationalUnits as IAuthOrganizationalUnit[]
        )
      : []

    return {
      descendantOrganizationalUnits,
    }
  }
  getPriority(): number {
    return 0
  }
}
