import { Module } from "@nestjs/common"
import { AuthenticationModule } from "@punks/backend-entity-manager"
import { SharedModule } from "../../shared/module"
import { AppAuthActions } from "./appAuth.actions"
import { AppAuthController } from "./appAuth.controller"
import { AppAuthConverter } from "./appAuth.converter"
import { AuthenticationInfrastructureModule } from "@/infrastructure/authentication/module"
import { AppAuthContextProvider } from "./context"
import { AppOrganizationalUnitEntityModule } from "@/entities/appOrganizationalUnits/appOrganizationalUnit.module"
import { AppAuthenticationMiddleware } from "./middlewares/organizational-units"

@Module({
  imports: [
    SharedModule,
    AuthenticationModule,
    AuthenticationInfrastructureModule,
    AppOrganizationalUnitEntityModule,
  ],
  providers: [
    AppAuthActions,
    AppAuthConverter,
    AppAuthContextProvider,
    AppAuthenticationMiddleware,
  ],
  controllers: [AppAuthController],
})
export class AppAuthAppModule {}
