import { ApiProperty } from "@nestjs/swagger"
import { UserRegistrationError } from "@punks/backend-entity-manager"
import {
  AuthUserContext,
  AuthUserRegistrationData,
} from "../../infrastructure/authentication/models"
import { AppPermissionType } from "@/middleware/authentication/permissions"
import { AppRoleType } from "@/middleware/authentication/roles"

export class UserOrganization {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string
}

export class UserTenant {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string
}

export class UserRole {
  @ApiProperty()
  id: string

  @ApiProperty({ enum: AppRoleType })
  uid: AppRoleType

  @ApiProperty()
  enabled: boolean

  @ApiProperty()
  name: string
}

export class UserPermission {
  @ApiProperty()
  id: string

  @ApiProperty({ enum: AppPermissionType })
  uid: string

  @ApiProperty()
  name: string
}

export class UserProfilePersonalData {
  @ApiProperty()
  firstName: string

  @ApiProperty()
  lastName: string
}

export class UserProfileData {
  @ApiProperty()
  id: string

  @ApiProperty()
  userName: string

  @ApiProperty()
  email: string

  @ApiProperty()
  verified: boolean

  @ApiProperty()
  disabled: boolean

  @ApiProperty()
  profile: UserProfilePersonalData

  @ApiProperty({ required: false })
  organization?: UserOrganization

  @ApiProperty()
  tenant: UserTenant
}

export class UserResources {
  @ApiProperty({ type: [UserOrganization] })
  organizations: UserOrganization[]

  @ApiProperty({ type: [UserTenant] })
  tenants: UserTenant[]

  @ApiProperty({ type: [UserRole] })
  roles: UserRole[]
}

export class UserProfileResponse {
  @ApiProperty()
  user: UserProfileData

  @ApiProperty({ type: [UserRole] })
  roles: UserRole[]

  @ApiProperty({ type: [UserPermission] })
  permissions: UserPermission[]

  @ApiProperty()
  resources: UserResources
}

export class OperationCallbackTemplate {
  @ApiProperty()
  urlTemplate: string

  @ApiProperty()
  tokenPlaceholder: string
}

export class UserLoginRequest {
  @ApiProperty()
  userName: string

  @ApiProperty()
  userContext: AuthUserContext

  @ApiProperty()
  password: string
}

export class UserLoginResponse {
  @ApiProperty()
  success: boolean

  @ApiProperty({ required: false })
  token?: string
}

export class UserImpersonateResponse {
  @ApiProperty()
  token: string
}

export class UserRegisterRequest {
  @ApiProperty()
  email: string

  @ApiProperty()
  userName: string

  @ApiProperty()
  password: string

  @ApiProperty()
  data: AuthUserRegistrationData

  @ApiProperty()
  userContext: AuthUserContext

  @ApiProperty()
  callback: OperationCallbackTemplate

  @ApiProperty()
  languageCode: string
}

export class UserRegisterResponse {
  @ApiProperty()
  success: boolean

  @ApiProperty({ enum: UserRegistrationError, required: false })
  error?: UserRegistrationError
}

export class UserPasswordResetRequest {
  @ApiProperty()
  userName: string

  @ApiProperty({ required: false })
  userContext?: AuthUserContext

  @ApiProperty()
  callback: OperationCallbackTemplate

  @ApiProperty()
  languageCode: string
}

export class UserPasswordResetResponse {
  @ApiProperty()
  success: boolean
}

export class UserPasswordResetCompleteRequest {
  @ApiProperty()
  token: string

  @ApiProperty()
  newPassword: string
}

export class UserEmailVerifyCompleteRequest {
  @ApiProperty()
  token: string
}

export class UserEmailVerifyRequest {
  @ApiProperty()
  email: string

  @ApiProperty({ required: false })
  userContext?: AuthUserContext

  @ApiProperty()
  callback: OperationCallbackTemplate

  @ApiProperty()
  languageCode: string
}
