import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppEventLogEntity,
  AppEventLogEntityId,
} from "../../database/core/entities/appEventLog.entity"
import { AppEventLogSearchParameters } from "../../entities/appEventLogs/appEventLog.types"
import {
  AppEventLogCreateData,
  AppEventLogUpdateData,
  AppEventLogSorting,
  AppEventLogCursor,
  AppEventLogSheetItem,
} from "../../entities/appEventLogs/appEventLog.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appEventLog")
export class AppEventLogSerializer extends NestEntitySerializer<
  AppEventLogEntity,
  AppEventLogEntityId,
  AppEventLogCreateData,
  AppEventLogUpdateData,
  AppEventLogSearchParameters,
  AppEventLogSorting,
  AppEventLogCursor,
  AppEventLogSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEventLog", registry)
  }

  protected async loadEntities(
    filters: AppEventLogSearchParameters
  ): Promise<AppEventLogEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppEventLogEntity[]
  ): Promise<AppEventLogSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppEventLogSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppEventLogEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
