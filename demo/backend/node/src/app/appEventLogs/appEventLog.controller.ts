import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppEventLogActions } from "./appEventLog.actions"
import {
  AppEventLogDto,
  AppEventLogCreateDto,
  AppEventLogUpdateDto,
} from "./appEventLog.dto"
import {
  AppEventLogExportRequest,
  AppEventLogExportResponse,
  AppEventLogSampleDownloadRequest,
  AppEventLogSampleDownloadResponse,
  AppEventLogSearchRequest,
  AppEventLogSearchResponse,
  AppEventLogVersionsSearchRequest,
  AppEventLogVersionsSearchResponse,
} from "./appEventLog.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appEventLog")
export class AppEventLogController {
  constructor(private readonly actions: AppEventLogActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appEventLogGet",
  })
  @ApiOkResponse({
    type: AppEventLogDto,
  })
  async item(@Param("id") id: string): Promise<AppEventLogDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appEventLogCreate",
  })
  @ApiOkResponse({
    type: AppEventLogDto,
  })
  async create(@Body() data: AppEventLogCreateDto): Promise<AppEventLogDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppEventLogDto,
  })
  @ApiOperation({
    operationId: "appEventLogUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppEventLogUpdateDto
  ): Promise<AppEventLogDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appEventLogDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appEventLogSearch",
  })
  @ApiOkResponse({
    type: AppEventLogSearchResponse,
  })
  async search(
    @Body() request: AppEventLogSearchRequest
  ): Promise<AppEventLogSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appEventLogImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appEventLogExport",
  })
  @ApiOkResponse({
    type: AppEventLogExportResponse,
  })
  async export(
    @Body() request: AppEventLogExportRequest
  ): Promise<AppEventLogExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appEventLogSampleDownload",
  })
  @ApiOkResponse({
    type: AppEventLogSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppEventLogSampleDownloadRequest
  ): Promise<AppEventLogSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appEventLogVersions",
  })
  @ApiOkResponse({
    type: AppEventLogVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppEventLogVersionsSearchRequest
  ): Promise<AppEventLogVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
