import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppEventLogCreateDto,
  AppEventLogDto,
  AppEventLogListItemDto,
  AppEventLogUpdateDto,
} from "./appEventLog.dto"
import {
  AppEventLogEntity,
  AppEventLogEntityId,
} from "../../database/core/entities/appEventLog.entity"
import {
  AppEventLogCursor,
  AppEventLogFacets,
  AppEventLogSorting,
} from "../../entities/appEventLogs/appEventLog.models"
import {
  AppEventLogDeleteParameters,
  AppEventLogSearchParameters,
} from "../../entities/appEventLogs/appEventLog.types"

@WpEntityActions("appEventLog")
export class AppEventLogActions extends NestEntityActions<
  AppEventLogEntity,
  AppEventLogEntityId,
  AppEventLogCreateDto,
  AppEventLogUpdateDto,
  AppEventLogDto,
  AppEventLogListItemDto,
  AppEventLogDeleteParameters,
  AppEventLogSearchParameters,
  AppEventLogSorting,
  AppEventLogCursor,
  AppEventLogFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEventLog", registry)
  }
}
