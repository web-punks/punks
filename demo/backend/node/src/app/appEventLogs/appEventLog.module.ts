import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppEventLogActions } from "./appEventLog.actions"
import { AppEventLogController } from "./appEventLog.controller"
import { AppEventLogConverter } from "./appEventLog.converter"
import { AppEventLogEntityModule } from "../../entities/appEventLogs/appEventLog.module"
import { AppEventLogSerializer } from "./appEventLog.serializer"

@Module({
  imports: [SharedModule, AppEventLogEntityModule],
  providers: [
    AppEventLogActions,
    AppEventLogConverter,
    AppEventLogSerializer,
  ],
  controllers: [AppEventLogController],
})
export class AppEventLogAppModule {}
