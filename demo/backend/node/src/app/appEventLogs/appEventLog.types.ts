import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppEventLogSearchParameters,
  AppEventLogSearchResults,
  AppEventLogVersionsSearchParameters,
  AppEventLogVersionsSearchResults,
} from "../../entities/appEventLogs/appEventLog.types"
import { AppEventLogListItemDto } from "./appEventLog.dto"

export class AppEventLogSearchRequest {
  @ApiProperty()
  params: AppEventLogSearchParameters
}

export class AppEventLogSearchResponse extends AppEventLogSearchResults<AppEventLogListItemDto> {
  @ApiProperty({ type: [AppEventLogListItemDto] })
  items: AppEventLogListItemDto[]
}

export class AppEventLogEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppEventLogExportRequest {
  @ApiProperty()
  options: AppEventLogEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppEventLogSearchParameters
}

export class AppEventLogExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppEventLogSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppEventLogSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppEventLogVersionsSearchRequest {
  @ApiProperty()
  params: AppEventLogVersionsSearchParameters
}

export class AppEventLogVersionsSearchResponse extends AppEventLogVersionsSearchResults<AppEventLogListItemDto> {
  @ApiProperty({ type: [AppEventLogListItemDto] })
  items: AppEventLogListItemDto[]
}
