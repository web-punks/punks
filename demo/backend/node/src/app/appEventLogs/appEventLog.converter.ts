import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppEventLogCreateDto,
  AppEventLogDto,
  AppEventLogListItemDto,
  AppEventLogUpdateDto,
} from "./appEventLog.dto"
import { AppEventLogEntity } from "../../database/core/entities/appEventLog.entity"

@WpEntityConverter("appEventLog")
export class AppEventLogConverter
  implements
    IEntityConverter<
      AppEventLogEntity,
      AppEventLogDto,
      AppEventLogListItemDto,
      AppEventLogCreateDto,
      AppEventLogUpdateDto
    >
{
  toListItemDto(entity: AppEventLogEntity): AppEventLogListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppEventLogEntity): AppEventLogDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppEventLogCreateDto): DeepPartial<AppEventLogEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppEventLogUpdateDto): DeepPartial<AppEventLogEntity> {
    return {
      ...input,
    }
  }
}
