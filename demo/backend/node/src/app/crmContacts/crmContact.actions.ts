import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  CrmContactCreateDto,
  CrmContactDto,
  CrmContactListItemDto,
  CrmContactUpdateDto,
} from "./crmContact.dto"
import { CrmContactEntity } from "../../database/core/entities/crmContact.entity"
import {
  CrmContactCursor,
  CrmContactEntityId,
  CrmContactFacets,
  CrmContactSorting,
} from "../../entities/crmContacts/crmContact.models"
import {
  CrmContactDeleteParameters,
  CrmContactSearchParameters,
} from "../../entities/crmContacts/crmContact.types"

@WpEntityActions("crmContact")
export class CrmContactActions extends NestEntityActions<
  CrmContactEntity,
  CrmContactEntityId,
  CrmContactCreateDto,
  CrmContactUpdateDto,
  CrmContactDto,
  CrmContactListItemDto,
  CrmContactDeleteParameters,
  CrmContactSearchParameters,
  CrmContactSorting,
  CrmContactCursor,
  CrmContactFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("crmContact", registry)
  }
}
