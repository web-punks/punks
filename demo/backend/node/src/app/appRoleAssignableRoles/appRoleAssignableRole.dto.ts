import { ApiProperty } from "@nestjs/swagger"

export class AppRoleAssignableRoleDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppRoleAssignableRoleListItemDto {
  @ApiProperty()
  id: string
}

export class AppRoleAssignableRoleCreateDto {}

export class AppRoleAssignableRoleUpdateDto {}
