import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppRoleAssignableRoleActions } from "./appRoleAssignableRole.actions"
import {
  AppRoleAssignableRoleDto,
  AppRoleAssignableRoleCreateDto,
  AppRoleAssignableRoleUpdateDto,
} from "./appRoleAssignableRole.dto"
import {
  AppRoleAssignableRoleExportRequest,
  AppRoleAssignableRoleExportResponse,
  AppRoleAssignableRoleSampleDownloadRequest,
  AppRoleAssignableRoleSampleDownloadResponse,
  AppRoleAssignableRoleSearchRequest,
  AppRoleAssignableRoleSearchResponse,
  AppRoleAssignableRoleVersionsSearchRequest,
  AppRoleAssignableRoleVersionsSearchResponse,
} from "./appRoleAssignableRole.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appRoleAssignableRole")
export class AppRoleAssignableRoleController {
  constructor(private readonly actions: AppRoleAssignableRoleActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appRoleAssignableRoleGet",
  })
  @ApiOkResponse({
    type: AppRoleAssignableRoleDto,
  })
  async item(
    @Param("id") id: string
  ): Promise<AppRoleAssignableRoleDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appRoleAssignableRoleCreate",
  })
  @ApiOkResponse({
    type: AppRoleAssignableRoleDto,
  })
  async create(
    @Body() data: AppRoleAssignableRoleCreateDto
  ): Promise<AppRoleAssignableRoleDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppRoleAssignableRoleDto,
  })
  @ApiOperation({
    operationId: "appRoleAssignableRoleUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppRoleAssignableRoleUpdateDto
  ): Promise<AppRoleAssignableRoleDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appRoleAssignableRoleDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appRoleAssignableRoleSearch",
  })
  @ApiOkResponse({
    type: AppRoleAssignableRoleSearchResponse,
  })
  async search(
    @Body() request: AppRoleAssignableRoleSearchRequest
  ): Promise<AppRoleAssignableRoleSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appRoleAssignableRoleImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appRoleAssignableRoleExport",
  })
  @ApiOkResponse({
    type: AppRoleAssignableRoleExportResponse,
  })
  async export(
    @Body() request: AppRoleAssignableRoleExportRequest
  ): Promise<AppRoleAssignableRoleExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appRoleAssignableRoleSampleDownload",
  })
  @ApiOkResponse({
    type: AppRoleAssignableRoleSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppRoleAssignableRoleSampleDownloadRequest
  ): Promise<AppRoleAssignableRoleSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appRoleAssignableRoleVersions",
  })
  @ApiOkResponse({
    type: AppRoleAssignableRoleVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppRoleAssignableRoleVersionsSearchRequest
  ): Promise<AppRoleAssignableRoleVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
