import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppRoleAssignableRoleCreateDto,
  AppRoleAssignableRoleDto,
  AppRoleAssignableRoleListItemDto,
  AppRoleAssignableRoleUpdateDto,
} from "./appRoleAssignableRole.dto"
import {
  AppRoleAssignableRoleEntity,
  AppRoleAssignableRoleEntityId,
} from "../../database/core/entities/appRoleAssignableRole.entity"
import {
  AppRoleAssignableRoleCursor,
  AppRoleAssignableRoleFacets,
  AppRoleAssignableRoleSorting,
} from "../../entities/appRoleAssignableRoles/appRoleAssignableRole.models"
import {
  AppRoleAssignableRoleDeleteParameters,
  AppRoleAssignableRoleSearchParameters,
} from "../../entities/appRoleAssignableRoles/appRoleAssignableRole.types"

@WpEntityActions("appRoleAssignableRole")
export class AppRoleAssignableRoleActions extends NestEntityActions<
  AppRoleAssignableRoleEntity,
  AppRoleAssignableRoleEntityId,
  AppRoleAssignableRoleCreateDto,
  AppRoleAssignableRoleUpdateDto,
  AppRoleAssignableRoleDto,
  AppRoleAssignableRoleListItemDto,
  AppRoleAssignableRoleDeleteParameters,
  AppRoleAssignableRoleSearchParameters,
  AppRoleAssignableRoleSorting,
  AppRoleAssignableRoleCursor,
  AppRoleAssignableRoleFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRoleAssignableRole", registry)
  }
}
