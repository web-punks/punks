import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppRoleAssignableRoleActions } from "./appRoleAssignableRole.actions"
import { AppRoleAssignableRoleController } from "./appRoleAssignableRole.controller"
import { AppRoleAssignableRoleConverter } from "./appRoleAssignableRole.converter"
import { AppRoleAssignableRoleEntityModule } from "../../entities/appRoleAssignableRoles/appRoleAssignableRole.module"
import { AppRoleAssignableRoleSerializer } from "./appRoleAssignableRole.serializer"

@Module({
  imports: [SharedModule, AppRoleAssignableRoleEntityModule],
  providers: [
    AppRoleAssignableRoleActions,
    AppRoleAssignableRoleConverter,
    AppRoleAssignableRoleSerializer,
  ],
  controllers: [AppRoleAssignableRoleController],
})
export class AppRoleAssignableRoleAppModule {}
