import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppRoleAssignableRoleSearchParameters,
  AppRoleAssignableRoleSearchResults,
  AppRoleAssignableRoleVersionsSearchParameters,
  AppRoleAssignableRoleVersionsSearchResults,
} from "../../entities/appRoleAssignableRoles/appRoleAssignableRole.types"
import { AppRoleAssignableRoleListItemDto } from "./appRoleAssignableRole.dto"

export class AppRoleAssignableRoleSearchRequest {
  @ApiProperty()
  params: AppRoleAssignableRoleSearchParameters
}

export class AppRoleAssignableRoleSearchResponse extends AppRoleAssignableRoleSearchResults<AppRoleAssignableRoleListItemDto> {
  @ApiProperty({ type: [AppRoleAssignableRoleListItemDto] })
  items: AppRoleAssignableRoleListItemDto[]
}

export class AppRoleAssignableRoleEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppRoleAssignableRoleExportRequest {
  @ApiProperty()
  options: AppRoleAssignableRoleEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppRoleAssignableRoleSearchParameters
}

export class AppRoleAssignableRoleExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppRoleAssignableRoleSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppRoleAssignableRoleSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppRoleAssignableRoleVersionsSearchRequest {
  @ApiProperty()
  params: AppRoleAssignableRoleVersionsSearchParameters
}

export class AppRoleAssignableRoleVersionsSearchResponse extends AppRoleAssignableRoleVersionsSearchResults<AppRoleAssignableRoleListItemDto> {
  @ApiProperty({ type: [AppRoleAssignableRoleListItemDto] })
  items: AppRoleAssignableRoleListItemDto[]
}
