import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppRoleAssignableRoleEntity,
  AppRoleAssignableRoleEntityId,
} from "../../database/core/entities/appRoleAssignableRole.entity"
import { AppRoleAssignableRoleSearchParameters } from "../../entities/appRoleAssignableRoles/appRoleAssignableRole.types"
import {
  AppRoleAssignableRoleCreateData,
  AppRoleAssignableRoleUpdateData,
  AppRoleAssignableRoleSorting,
  AppRoleAssignableRoleCursor,
  AppRoleAssignableRoleSheetItem,
} from "../../entities/appRoleAssignableRoles/appRoleAssignableRole.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appRoleAssignableRole")
export class AppRoleAssignableRoleSerializer extends NestEntitySerializer<
  AppRoleAssignableRoleEntity,
  AppRoleAssignableRoleEntityId,
  AppRoleAssignableRoleCreateData,
  AppRoleAssignableRoleUpdateData,
  AppRoleAssignableRoleSearchParameters,
  AppRoleAssignableRoleSorting,
  AppRoleAssignableRoleCursor,
  AppRoleAssignableRoleSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRoleAssignableRole", registry)
  }

  protected async loadEntities(
    filters: AppRoleAssignableRoleSearchParameters
  ): Promise<AppRoleAssignableRoleEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppRoleAssignableRoleEntity[]
  ): Promise<AppRoleAssignableRoleSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppRoleAssignableRoleSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppRoleAssignableRoleEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
