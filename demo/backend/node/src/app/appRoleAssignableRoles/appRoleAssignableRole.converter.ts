import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppRoleAssignableRoleCreateDto,
  AppRoleAssignableRoleDto,
  AppRoleAssignableRoleListItemDto,
  AppRoleAssignableRoleUpdateDto,
} from "./appRoleAssignableRole.dto"
import { AppRoleAssignableRoleEntity } from "../../database/core/entities/appRoleAssignableRole.entity"

@WpEntityConverter("appRoleAssignableRole")
export class AppRoleAssignableRoleConverter
  implements
    IEntityConverter<
      AppRoleAssignableRoleEntity,
      AppRoleAssignableRoleDto,
      AppRoleAssignableRoleListItemDto,
      AppRoleAssignableRoleCreateDto,
      AppRoleAssignableRoleUpdateDto
    >
{
  toListItemDto(entity: AppRoleAssignableRoleEntity): AppRoleAssignableRoleListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppRoleAssignableRoleEntity): AppRoleAssignableRoleDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppRoleAssignableRoleCreateDto): DeepPartial<AppRoleAssignableRoleEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppRoleAssignableRoleUpdateDto): DeepPartial<AppRoleAssignableRoleEntity> {
    return {
      ...input,
    }
  }
}
