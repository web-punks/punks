import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationEntity,
  AppOrganizationEntityId,
} from "../../database/core/entities/appOrganization.entity"
import { AppOrganizationSearchParameters } from "../../entities/appOrganizations/appOrganization.types"
import {
  AppOrganizationCreateData,
  AppOrganizationUpdateData,
  AppOrganizationSorting,
  AppOrganizationCursor,
  AppOrganizationSheetItem,
} from "../../entities/appOrganizations/appOrganization.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appOrganization")
export class AppOrganizationSerializer extends NestEntitySerializer<
  AppOrganizationEntity,
  AppOrganizationEntityId,
  AppOrganizationCreateData,
  AppOrganizationUpdateData,
  AppOrganizationSearchParameters,
  AppOrganizationSorting,
  AppOrganizationCursor,
  AppOrganizationSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganization", registry)
  }

  protected async loadEntities(
    filters: AppOrganizationSearchParameters
  ): Promise<AppOrganizationEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppOrganizationEntity[]
  ): Promise<AppOrganizationSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppOrganizationSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppOrganizationEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
