import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationCreateDto,
  AppOrganizationDto,
  AppOrganizationListItemDto,
  AppOrganizationUpdateDto,
} from "./appOrganization.dto"
import {
  AppOrganizationEntity,
  AppOrganizationEntityId,
} from "../../database/core/entities/appOrganization.entity"
import {
  AppOrganizationCursor,
  AppOrganizationFacets,
  AppOrganizationSorting,
} from "../../entities/appOrganizations/appOrganization.models"
import {
  AppOrganizationDeleteParameters,
  AppOrganizationSearchParameters,
} from "../../entities/appOrganizations/appOrganization.types"

@WpEntityActions("appOrganization")
export class AppOrganizationActions extends NestEntityActions<
  AppOrganizationEntity,
  AppOrganizationEntityId,
  AppOrganizationCreateDto,
  AppOrganizationUpdateDto,
  AppOrganizationDto,
  AppOrganizationListItemDto,
  AppOrganizationDeleteParameters,
  AppOrganizationSearchParameters,
  AppOrganizationSorting,
  AppOrganizationCursor,
  AppOrganizationFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganization", registry)
  }
}
