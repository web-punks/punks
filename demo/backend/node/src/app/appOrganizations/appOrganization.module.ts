import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppOrganizationActions } from "./appOrganization.actions"
import { AppOrganizationController } from "./appOrganization.controller"
import { AppOrganizationConverter } from "./appOrganization.converter"
import { AppOrganizationEntityModule } from "../../entities/appOrganizations/appOrganization.module"
import { AppOrganizationSerializer } from "./appOrganization.serializer"

@Module({
  imports: [SharedModule, AppOrganizationEntityModule],
  providers: [
    AppOrganizationActions,
    AppOrganizationConverter,
    AppOrganizationSerializer,
  ],
  controllers: [AppOrganizationController],
})
export class AppOrganizationAppModule {}
