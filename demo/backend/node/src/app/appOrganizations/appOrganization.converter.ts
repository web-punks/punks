import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationCreateDto,
  AppOrganizationDto,
  AppOrganizationListItemDto,
  AppOrganizationUpdateDto,
} from "./appOrganization.dto"
import { AppOrganizationEntity } from "../../database/core/entities/appOrganization.entity"

@WpEntityConverter("appOrganization")
export class AppOrganizationConverter
  implements
    IEntityConverter<
      AppOrganizationEntity,
      AppOrganizationDto,
      AppOrganizationListItemDto,
      AppOrganizationCreateDto,
      AppOrganizationUpdateDto
    >
{
  toListItemDto(entity: AppOrganizationEntity): AppOrganizationListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppOrganizationEntity): AppOrganizationDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppOrganizationCreateDto): DeepPartial<AppOrganizationEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppOrganizationUpdateDto): DeepPartial<AppOrganizationEntity> {
    return {
      ...input,
    }
  }
}
