import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppOrganizationSearchParameters,
  AppOrganizationSearchResults,
  AppOrganizationVersionsSearchParameters,
  AppOrganizationVersionsSearchResults,
} from "../../entities/appOrganizations/appOrganization.types"
import { AppOrganizationListItemDto } from "./appOrganization.dto"

export class AppOrganizationSearchRequest {
  @ApiProperty()
  params: AppOrganizationSearchParameters
}

export class AppOrganizationSearchResponse extends AppOrganizationSearchResults<AppOrganizationListItemDto> {
  @ApiProperty({ type: [AppOrganizationListItemDto] })
  items: AppOrganizationListItemDto[]
}

export class AppOrganizationEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppOrganizationExportRequest {
  @ApiProperty()
  options: AppOrganizationEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppOrganizationSearchParameters
}

export class AppOrganizationExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppOrganizationSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppOrganizationSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppOrganizationVersionsSearchRequest {
  @ApiProperty()
  params: AppOrganizationVersionsSearchParameters
}

export class AppOrganizationVersionsSearchResponse extends AppOrganizationVersionsSearchResults<AppOrganizationListItemDto> {
  @ApiProperty({ type: [AppOrganizationListItemDto] })
  items: AppOrganizationListItemDto[]
}
