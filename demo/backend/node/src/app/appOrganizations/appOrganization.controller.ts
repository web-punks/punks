import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppOrganizationActions } from "./appOrganization.actions"
import {
  AppOrganizationDto,
  AppOrganizationCreateDto,
  AppOrganizationUpdateDto,
} from "./appOrganization.dto"
import {
  AppOrganizationExportRequest,
  AppOrganizationExportResponse,
  AppOrganizationSampleDownloadRequest,
  AppOrganizationSampleDownloadResponse,
  AppOrganizationSearchRequest,
  AppOrganizationSearchResponse,
  AppOrganizationVersionsSearchRequest,
  AppOrganizationVersionsSearchResponse,
} from "./appOrganization.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appOrganization")
export class AppOrganizationController {
  constructor(private readonly actions: AppOrganizationActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appOrganizationGet",
  })
  @ApiOkResponse({
    type: AppOrganizationDto,
  })
  async item(@Param("id") id: string): Promise<AppOrganizationDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appOrganizationCreate",
  })
  @ApiOkResponse({
    type: AppOrganizationDto,
  })
  async create(
    @Body() data: AppOrganizationCreateDto
  ): Promise<AppOrganizationDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppOrganizationDto,
  })
  @ApiOperation({
    operationId: "appOrganizationUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppOrganizationUpdateDto
  ): Promise<AppOrganizationDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appOrganizationDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appOrganizationSearch",
  })
  @ApiOkResponse({
    type: AppOrganizationSearchResponse,
  })
  async search(
    @Body() request: AppOrganizationSearchRequest
  ): Promise<AppOrganizationSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appOrganizationImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appOrganizationExport",
  })
  @ApiOkResponse({
    type: AppOrganizationExportResponse,
  })
  async export(
    @Body() request: AppOrganizationExportRequest
  ): Promise<AppOrganizationExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appOrganizationSampleDownload",
  })
  @ApiOkResponse({
    type: AppOrganizationSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppOrganizationSampleDownloadRequest
  ): Promise<AppOrganizationSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appOrganizationVersions",
  })
  @ApiOkResponse({
    type: AppOrganizationVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppOrganizationVersionsSearchRequest
  ): Promise<AppOrganizationVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
