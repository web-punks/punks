import { ApiProperty } from "@nestjs/swagger"
import { FileData } from "@punks/backend-entity-manager"

export interface FileUploadInput {
  file: FileData
}

export class FileUploadData {
  @ApiProperty()
  folderPath: string

  @ApiProperty()
  metadata?: Record<string, any>
}

export class FileUploadResponse {
  @ApiProperty()
  fileId: string
}
