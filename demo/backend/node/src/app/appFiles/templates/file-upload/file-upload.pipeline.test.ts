import { Repository } from "typeorm"
import { createTestServer } from "@/testing"
import { FileUploadTemplate } from "."
import { AppFileAppModule } from "../../appFile.module"
import { AppFileReferenceEntity } from "@/database/core/entities/appFileReference.entity"

describe("Test File Upload Pipeline", () => {
  let filesRefRepo: Repository<AppFileReferenceEntity>
  let template: FileUploadTemplate
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { server, dataSource, terminate } = await createTestServer({
      extraModules: [AppFileAppModule],
    })
    filesRefRepo = dataSource.getRepository(AppFileReferenceEntity)
    template = server.get(FileUploadTemplate)
    terminateServer = terminate
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should run the pipeline template successfully", async () => {
    const result = await template.execute({
      input: {
        file: {
          content: Buffer.from("test1234567890"),
          contentType: "text/plain",
          fileName: "test.txt",
          folderPath: "test",
        },
      },
      context: {
        isAnonymous: true,
        isAuthenticated: false,
      },
    })

    expect(result.type).toBe("success")
    const item = await filesRefRepo.findOne({
      where: {
        id: result.output?.fileId,
      },
    })
    expect(item).toMatchObject({
      id: result.output?.fileId,
      fileName: "test.txt",
      folder: "test",
    })
  })
})
