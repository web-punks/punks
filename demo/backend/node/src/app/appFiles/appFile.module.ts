import { Module } from "@nestjs/common"
import { SharedModule } from "@/shared/module"
import { AppRoleEntityModule } from "@/entities/appRoles/appRole.module"
import { AppFileController } from "./appFile.controller"
import { FileTemplates } from "./templates"

@Module({
  imports: [SharedModule, AppRoleEntityModule],
  providers: [...FileTemplates],
  controllers: [AppFileController],
})
export class AppFileAppModule {}
