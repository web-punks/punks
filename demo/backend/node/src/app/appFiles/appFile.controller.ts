import { Controller, Get, Post, Query, UploadedFile } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { FileGetDownloadUrlTemplate } from "./templates/file-get-download-url"
import { FileGetDownloadUrlResult } from "./templates/file-get-download-url/models"
import { FileUploadTemplate } from "./templates/file-upload"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appFile")
export class AppFileController {
  constructor(
    private readonly getDownloadUrlTemplate: FileGetDownloadUrlTemplate,
    private readonly fileUploadTemplate: FileUploadTemplate
  ) {}

  @Post("upload")
  @ApiOperation({
    operationId: "fileUpload",
  })
  @ApiBodyFile("file")
  async import(
    @Query() folderPath: string,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.fileUploadTemplate.invoke({
      file: {
        content: file.buffer,
        contentType: file.mimetype,
        fileName: file.originalname,
        folderPath,
      },
    })
  }

  @Get("getDownloadUrl")
  @ApiOkResponse({
    type: FileGetDownloadUrlResult,
  })
  @ApiOperation({
    operationId: "getFileDownloadUrl",
  })
  async getDownloadUrl(
    @Query() fileId: string
  ): Promise<FileGetDownloadUrlResult> {
    return await this.getDownloadUrlTemplate.invoke({
      fileId,
    })
  }
}
