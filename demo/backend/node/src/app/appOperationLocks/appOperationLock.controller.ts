import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppOperationLockActions } from "./appOperationLock.actions"
import {
  AppOperationLockDto,
  AppOperationLockCreateDto,
  AppOperationLockUpdateDto,
} from "./appOperationLock.dto"
import {
  AppOperationLockExportRequest,
  AppOperationLockExportResponse,
  AppOperationLockSampleDownloadRequest,
  AppOperationLockSampleDownloadResponse,
  AppOperationLockSearchRequest,
  AppOperationLockSearchResponse,
  AppOperationLockVersionsSearchRequest,
  AppOperationLockVersionsSearchResponse,
} from "./appOperationLock.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appOperationLock")
export class AppOperationLockController {
  constructor(private readonly actions: AppOperationLockActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appOperationLockGet",
  })
  @ApiOkResponse({
    type: AppOperationLockDto,
  })
  async item(@Param("id") id: string): Promise<AppOperationLockDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appOperationLockCreate",
  })
  @ApiOkResponse({
    type: AppOperationLockDto,
  })
  async create(@Body() data: AppOperationLockCreateDto): Promise<AppOperationLockDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppOperationLockDto,
  })
  @ApiOperation({
    operationId: "appOperationLockUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppOperationLockUpdateDto
  ): Promise<AppOperationLockDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appOperationLockDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appOperationLockSearch",
  })
  @ApiOkResponse({
    type: AppOperationLockSearchResponse,
  })
  async search(
    @Body() request: AppOperationLockSearchRequest
  ): Promise<AppOperationLockSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appOperationLockImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appOperationLockExport",
  })
  @ApiOkResponse({
    type: AppOperationLockExportResponse,
  })
  async export(
    @Body() request: AppOperationLockExportRequest
  ): Promise<AppOperationLockExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appOperationLockSampleDownload",
  })
  @ApiOkResponse({
    type: AppOperationLockSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppOperationLockSampleDownloadRequest
  ): Promise<AppOperationLockSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appOperationLockVersions",
  })
  @ApiOkResponse({
    type: AppOperationLockVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppOperationLockVersionsSearchRequest
  ): Promise<AppOperationLockVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
