import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppOperationLockSearchParameters,
  AppOperationLockSearchResults,
  AppOperationLockVersionsSearchParameters,
  AppOperationLockVersionsSearchResults,
} from "../../entities/appOperationLocks/appOperationLock.types"
import { AppOperationLockListItemDto } from "./appOperationLock.dto"

export class AppOperationLockSearchRequest {
  @ApiProperty()
  params: AppOperationLockSearchParameters
}

export class AppOperationLockSearchResponse extends AppOperationLockSearchResults<AppOperationLockListItemDto> {
  @ApiProperty({ type: [AppOperationLockListItemDto] })
  items: AppOperationLockListItemDto[]
}

export class AppOperationLockEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppOperationLockExportRequest {
  @ApiProperty()
  options: AppOperationLockEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppOperationLockSearchParameters
}

export class AppOperationLockExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppOperationLockSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppOperationLockSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppOperationLockVersionsSearchRequest {
  @ApiProperty()
  params: AppOperationLockVersionsSearchParameters
}

export class AppOperationLockVersionsSearchResponse extends AppOperationLockVersionsSearchResults<AppOperationLockListItemDto> {
  @ApiProperty({ type: [AppOperationLockListItemDto] })
  items: AppOperationLockListItemDto[]
}
