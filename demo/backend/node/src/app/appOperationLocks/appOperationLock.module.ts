import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppOperationLockActions } from "./appOperationLock.actions"
import { AppOperationLockController } from "./appOperationLock.controller"
import { AppOperationLockConverter } from "./appOperationLock.converter"
import { AppOperationLockEntityModule } from "../../entities/appOperationLocks/appOperationLock.module"
import { AppOperationLockSerializer } from "./appOperationLock.serializer"

@Module({
  imports: [SharedModule, AppOperationLockEntityModule],
  providers: [
    AppOperationLockActions,
    AppOperationLockConverter,
    AppOperationLockSerializer,
  ],
  controllers: [AppOperationLockController],
})
export class AppOperationLockAppModule {}
