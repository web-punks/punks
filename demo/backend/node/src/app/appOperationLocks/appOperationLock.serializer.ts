import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppOperationLockEntity,
  AppOperationLockEntityId,
} from "../../database/core/entities/appOperationLock.entity"
import { AppOperationLockSearchParameters } from "../../entities/appOperationLocks/appOperationLock.types"
import {
  AppOperationLockCreateData,
  AppOperationLockUpdateData,
  AppOperationLockSorting,
  AppOperationLockCursor,
  AppOperationLockSheetItem,
} from "../../entities/appOperationLocks/appOperationLock.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appOperationLock")
export class AppOperationLockSerializer extends NestEntitySerializer<
  AppOperationLockEntity,
  AppOperationLockEntityId,
  AppOperationLockCreateData,
  AppOperationLockUpdateData,
  AppOperationLockSearchParameters,
  AppOperationLockSorting,
  AppOperationLockCursor,
  AppOperationLockSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOperationLock", registry)
  }

  protected async loadEntities(
    filters: AppOperationLockSearchParameters
  ): Promise<AppOperationLockEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppOperationLockEntity[]
  ): Promise<AppOperationLockSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppOperationLockSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppOperationLockEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
