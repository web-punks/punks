import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppOperationLockCreateDto,
  AppOperationLockDto,
  AppOperationLockListItemDto,
  AppOperationLockUpdateDto,
} from "./appOperationLock.dto"
import { AppOperationLockEntity } from "../../database/core/entities/appOperationLock.entity"

@WpEntityConverter("appOperationLock")
export class AppOperationLockConverter
  implements
    IEntityConverter<
      AppOperationLockEntity,
      AppOperationLockDto,
      AppOperationLockListItemDto,
      AppOperationLockCreateDto,
      AppOperationLockUpdateDto
    >
{
  toListItemDto(entity: AppOperationLockEntity): AppOperationLockListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppOperationLockEntity): AppOperationLockDto {
    return {
      ...this.toListItemDto(entity),
    }
  }

  createDtoToEntity(
    input: AppOperationLockCreateDto
  ): DeepPartial<AppOperationLockEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(
    input: AppOperationLockUpdateDto
  ): DeepPartial<AppOperationLockEntity> {
    return {
      ...input,
    }
  }
}
