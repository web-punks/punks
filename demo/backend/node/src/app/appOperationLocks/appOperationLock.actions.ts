import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppOperationLockCreateDto,
  AppOperationLockDto,
  AppOperationLockListItemDto,
  AppOperationLockUpdateDto,
} from "./appOperationLock.dto"
import {
  AppOperationLockEntity,
  AppOperationLockEntityId,
} from "../../database/core/entities/appOperationLock.entity"
import {
  AppOperationLockCursor,
  AppOperationLockFacets,
  AppOperationLockSorting,
} from "../../entities/appOperationLocks/appOperationLock.models"
import {
  AppOperationLockDeleteParameters,
  AppOperationLockSearchParameters,
} from "../../entities/appOperationLocks/appOperationLock.types"

@WpEntityActions("appOperationLock")
export class AppOperationLockActions extends NestEntityActions<
  AppOperationLockEntity,
  AppOperationLockEntityId,
  AppOperationLockCreateDto,
  AppOperationLockUpdateDto,
  AppOperationLockDto,
  AppOperationLockListItemDto,
  AppOperationLockDeleteParameters,
  AppOperationLockSearchParameters,
  AppOperationLockSorting,
  AppOperationLockCursor,
  AppOperationLockFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOperationLock", registry)
  }
}
