import { ApiProperty } from "@nestjs/swagger"

export class AppOperationLockDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty({ required: false })
  lockedBy?: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppOperationLockListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty({ required: false })
  lockedBy?: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppOperationLockCreateDto {}

export class AppOperationLockUpdateDto {}
