import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppOrganizationalUnitActions } from "./appOrganizationalUnit.actions"
import {
  AppOrganizationalUnitDto,
  AppOrganizationalUnitCreateDto,
  AppOrganizationalUnitUpdateDto,
  AppOrganizationalUnitTreeResponse,
} from "./appOrganizationalUnit.dto"
import {
  AppOrganizationalUnitExportRequest,
  AppOrganizationalUnitExportResponse,
  AppOrganizationalUnitSampleDownloadRequest,
  AppOrganizationalUnitSampleDownloadResponse,
  AppOrganizationalUnitSearchRequest,
  AppOrganizationalUnitSearchResponse,
  AppOrganizationalUnitVersionsSearchRequest,
  AppOrganizationalUnitVersionsSearchResponse,
} from "./appOrganizationalUnit.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appOrganizationalUnit")
export class AppOrganizationalUnitController {
  constructor(private readonly actions: AppOrganizationalUnitActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appOrganizationalUnitGet",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitDto,
  })
  async item(
    @Param("id") id: string
  ): Promise<AppOrganizationalUnitDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appOrganizationalUnitCreate",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitDto,
  })
  async create(
    @Body() data: AppOrganizationalUnitCreateDto
  ): Promise<AppOrganizationalUnitDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppOrganizationalUnitDto,
  })
  @ApiOperation({
    operationId: "appOrganizationalUnitUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppOrganizationalUnitUpdateDto
  ): Promise<AppOrganizationalUnitDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appOrganizationalUnitDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appOrganizationalUnitSearch",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitSearchResponse,
  })
  async search(
    @Body() request: AppOrganizationalUnitSearchRequest
  ): Promise<AppOrganizationalUnitSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("tree")
  @ApiOperation({
    operationId: "appOrganizationalUnitTree",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTreeResponse,
  })
  async tree(
    @Body() request: AppOrganizationalUnitSearchRequest
  ): Promise<AppOrganizationalUnitTreeResponse> {
    return await this.actions.buildTree(request)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appOrganizationalUnitImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appOrganizationalUnitExport",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitExportResponse,
  })
  async export(
    @Body() request: AppOrganizationalUnitExportRequest
  ): Promise<AppOrganizationalUnitExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appOrganizationalUnitSampleDownload",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppOrganizationalUnitSampleDownloadRequest
  ): Promise<AppOrganizationalUnitSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appOrganizationalUnitVersions",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppOrganizationalUnitVersionsSearchRequest
  ): Promise<AppOrganizationalUnitVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
