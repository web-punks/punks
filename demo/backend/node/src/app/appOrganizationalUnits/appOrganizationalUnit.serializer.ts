import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitEntityId,
} from "../../database/core/entities/appOrganizationalUnit.entity"
import { AppOrganizationalUnitSearchParameters } from "../../entities/appOrganizationalUnits/appOrganizationalUnit.types"
import {
  AppOrganizationalUnitCreateData,
  AppOrganizationalUnitUpdateData,
  AppOrganizationalUnitSorting,
  AppOrganizationalUnitCursor,
  AppOrganizationalUnitSheetItem,
} from "../../entities/appOrganizationalUnits/appOrganizationalUnit.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appOrganizationalUnit")
export class AppOrganizationalUnitSerializer extends NestEntitySerializer<
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitEntityId,
  AppOrganizationalUnitCreateData,
  AppOrganizationalUnitUpdateData,
  AppOrganizationalUnitSearchParameters,
  AppOrganizationalUnitSorting,
  AppOrganizationalUnitCursor,
  AppOrganizationalUnitSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganizationalUnit", registry)
  }

  protected async loadEntities(
    filters: AppOrganizationalUnitSearchParameters
  ): Promise<AppOrganizationalUnitEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppOrganizationalUnitEntity[]
  ): Promise<AppOrganizationalUnitSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppOrganizationalUnitSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppOrganizationalUnitEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
