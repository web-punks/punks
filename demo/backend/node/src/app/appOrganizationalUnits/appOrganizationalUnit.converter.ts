import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitCreateDto,
  AppOrganizationalUnitDto,
  AppOrganizationalUnitListItemDto,
  AppOrganizationalUnitTreeNodeDto,
  AppOrganizationalUnitTreeResponse,
  AppOrganizationalUnitUpdateDto,
} from "./appOrganizationalUnit.dto"
import { AppOrganizationalUnitEntity } from "@/database/core/entities/appOrganizationalUnit.entity"
import { toAppOrganizationReferenceDto } from "../appOrganizations/appOrganization.utils"
import {
  AppOrganizationalUnitTree,
  AppOrganizationalUnitTreeNode,
} from "@/entities/appOrganizationalUnits/appOrganizationalUnit.models"

@WpEntityConverter("appOrganizationalUnit")
export class AppOrganizationalUnitConverter
  implements
    IEntityConverter<
      AppOrganizationalUnitEntity,
      AppOrganizationalUnitDto,
      AppOrganizationalUnitListItemDto,
      AppOrganizationalUnitCreateDto,
      AppOrganizationalUnitUpdateDto
    >
{
  toListItemDto(
    entity: AppOrganizationalUnitEntity
  ): AppOrganizationalUnitListItemDto {
    return this.toEntityDto(entity)
  }

  toEntityDto(entity: AppOrganizationalUnitEntity): AppOrganizationalUnitDto {
    const { parent, type, ...data } = entity
    return {
      ...data,
      type: {
        id: type.id,
        name: type.name,
        uid: type.uid,
      },
      parentId: parent?.id,
      organization: entity.organization
        ? toAppOrganizationReferenceDto(entity.organization)
        : undefined,
    }
  }

  createDtoToEntity(
    input: AppOrganizationalUnitCreateDto
  ): DeepPartial<AppOrganizationalUnitEntity> {
    const { organizationId, typeId, parentId, ...params } = input
    return {
      ...params,
      type: {
        id: typeId,
      },
      parent: parentId
        ? {
            id: parentId,
          }
        : null,
      organization: organizationId
        ? {
            id: organizationId,
          }
        : undefined,
    }
  }

  updateDtoToEntity(
    input: AppOrganizationalUnitUpdateDto
  ): DeepPartial<AppOrganizationalUnitEntity> {
    return {
      name: input.name,
      uid: input.uid,
    }
  }

  toTreeResponse(
    tree: AppOrganizationalUnitTree
  ): AppOrganizationalUnitTreeResponse {
    const mapper = (
      node: AppOrganizationalUnitTreeNode
    ): AppOrganizationalUnitTreeNodeDto => ({
      ...node,
      value: this.toEntityDto(node.value),
      children: node.children.map(mapper),
    })

    return {
      tree: {
        root: tree.root.map(mapper),
      },
    }
  }
}
