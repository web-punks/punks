import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppOrganizationalUnitActions } from "./appOrganizationalUnit.actions"
import { AppOrganizationalUnitController } from "./appOrganizationalUnit.controller"
import { AppOrganizationalUnitConverter } from "./appOrganizationalUnit.converter"
import { AppOrganizationalUnitEntityModule } from "../../entities/appOrganizationalUnits/appOrganizationalUnit.module"
import { AppOrganizationalUnitSerializer } from "./appOrganizationalUnit.serializer"

@Module({
  imports: [SharedModule, AppOrganizationalUnitEntityModule],
  providers: [
    AppOrganizationalUnitActions,
    AppOrganizationalUnitConverter,
    AppOrganizationalUnitSerializer,
  ],
  controllers: [AppOrganizationalUnitController],
})
export class AppOrganizationalUnitAppModule {}
