import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitCreateDto,
  AppOrganizationalUnitDto,
  AppOrganizationalUnitListItemDto,
  AppOrganizationalUnitTreeResponse,
  AppOrganizationalUnitUpdateDto,
} from "./appOrganizationalUnit.dto"
import {
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitEntityId,
} from "../../database/core/entities/appOrganizationalUnit.entity"
import {
  AppOrganizationalUnitCursor,
  AppOrganizationalUnitFacets,
  AppOrganizationalUnitSorting,
} from "../../entities/appOrganizationalUnits/appOrganizationalUnit.models"
import {
  AppOrganizationalUnitDeleteParameters,
  AppOrganizationalUnitSearchParameters,
} from "../../entities/appOrganizationalUnits/appOrganizationalUnit.types"
import { AppOrganizationalUnitSearchRequest } from "./appOrganizationalUnit.types"
import { AppOrganizationalUnitConverter } from "./appOrganizationalUnit.converter"
import { AppOrganizationalUnitService } from "@/entities/appOrganizationalUnits/appOrganizationalUnit.service"

@WpEntityActions("appOrganizationalUnit")
export class AppOrganizationalUnitActions extends NestEntityActions<
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitEntityId,
  AppOrganizationalUnitCreateDto,
  AppOrganizationalUnitUpdateDto,
  AppOrganizationalUnitDto,
  AppOrganizationalUnitListItemDto,
  AppOrganizationalUnitDeleteParameters,
  AppOrganizationalUnitSearchParameters,
  AppOrganizationalUnitSorting,
  AppOrganizationalUnitCursor,
  AppOrganizationalUnitFacets
> {
  constructor(
    registry: EntityManagerRegistry,
    private readonly service: AppOrganizationalUnitService
  ) {
    super("appOrganizationalUnit", registry)
  }

  async buildTree(
    request: AppOrganizationalUnitSearchRequest
  ): Promise<AppOrganizationalUnitTreeResponse> {
    const tree = await this.service.buildTree(request.params)
    return (this.converter as AppOrganizationalUnitConverter).toTreeResponse(
      tree
    )
  }
}
