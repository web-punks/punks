import { ApiProperty } from "@nestjs/swagger"
import { AppOrganizationReferenceDto } from "../appOrganizations/appOrganization.dto"
import { EntityTreeNodePaths } from "@/shared/api/trees"
import { ITree, ITreeNode } from "@punks/backend-core"

export class AppOrganizationalUnitType {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string
}

export class AppOrganizationalUnitDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  type: AppOrganizationalUnitType

  @ApiProperty({ required: false })
  parentId?: string

  @ApiProperty()
  organization: AppOrganizationReferenceDto

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppOrganizationalUnitListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  type: AppOrganizationalUnitType

  @ApiProperty({ required: false })
  parentId?: string

  @ApiProperty()
  organization: AppOrganizationReferenceDto

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppOrganizationalUnitCreateDto {
  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  typeId: string

  @ApiProperty({ required: false })
  parentId?: string

  @ApiProperty()
  organizationId: string
}

export class AppOrganizationalUnitUpdateDto {
  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  typeId: string

  @ApiProperty({ required: false })
  parentId?: string
}

export class AppOrganizationalUnitTreeNodeDto
  implements ITreeNode<string, AppOrganizationalUnitListItemDto>
{
  @ApiProperty()
  id: string

  @ApiProperty()
  value: AppOrganizationalUnitListItemDto

  @ApiProperty()
  paths: EntityTreeNodePaths

  @ApiProperty({ type: [AppOrganizationalUnitTreeNodeDto] })
  children: AppOrganizationalUnitTreeNodeDto[]
}

export class AppOrganizationalUnitTreeDto
  implements ITree<string, AppOrganizationalUnitListItemDto>
{
  @ApiProperty({ type: [AppOrganizationalUnitTreeNodeDto] })
  root: AppOrganizationalUnitTreeNodeDto[]
}

export class AppOrganizationalUnitTreeResponse {
  @ApiProperty()
  tree: AppOrganizationalUnitTreeDto
}
