import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitSearchParameters,
  AppOrganizationalUnitSearchResults,
  AppOrganizationalUnitVersionsSearchParameters,
  AppOrganizationalUnitVersionsSearchResults,
} from "../../entities/appOrganizationalUnits/appOrganizationalUnit.types"
import { AppOrganizationalUnitListItemDto } from "./appOrganizationalUnit.dto"
import { EntitiesChildrenMap } from "@/shared/api/traverse"

export class AppOrganizationalUnitSearchRequest {
  @ApiProperty()
  params: AppOrganizationalUnitSearchParameters
}

export class AppOrganizationalUnitSearchResponse extends AppOrganizationalUnitSearchResults<AppOrganizationalUnitListItemDto> {
  @ApiProperty({ type: [AppOrganizationalUnitListItemDto] })
  items: AppOrganizationalUnitListItemDto[]

  @ApiProperty({
    type: "object",
    required: false,
  })
  childrenMap?: EntitiesChildrenMap
}

export class AppOrganizationalUnitEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppOrganizationalUnitExportRequest {
  @ApiProperty()
  options: AppOrganizationalUnitEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppOrganizationalUnitSearchParameters
}

export class AppOrganizationalUnitExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppOrganizationalUnitSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppOrganizationalUnitSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppOrganizationalUnitVersionsSearchRequest {
  @ApiProperty()
  params: AppOrganizationalUnitVersionsSearchParameters
}

export class AppOrganizationalUnitVersionsSearchResponse extends AppOrganizationalUnitVersionsSearchResults<AppOrganizationalUnitListItemDto> {
  @ApiProperty({ type: [AppOrganizationalUnitListItemDto] })
  items: AppOrganizationalUnitListItemDto[]
}
