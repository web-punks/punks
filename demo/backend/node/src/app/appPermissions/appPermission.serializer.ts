import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppPermissionEntity,
  AppPermissionEntityId,
} from "../../database/core/entities/appPermission.entity"
import { AppPermissionSearchParameters } from "../../entities/appPermissions/appPermission.types"
import {
  AppPermissionCreateData,
  AppPermissionUpdateData,
  AppPermissionSorting,
  AppPermissionCursor,
  AppPermissionSheetItem,
} from "../../entities/appPermissions/appPermission.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appPermission")
export class AppPermissionSerializer extends NestEntitySerializer<
  AppPermissionEntity,
  AppPermissionEntityId,
  AppPermissionCreateData,
  AppPermissionUpdateData,
  AppPermissionSearchParameters,
  AppPermissionSorting,
  AppPermissionCursor,
  AppPermissionSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appPermission", registry)
  }

  protected async loadEntities(
    filters: AppPermissionSearchParameters
  ): Promise<AppPermissionEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppPermissionEntity[]
  ): Promise<AppPermissionSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppPermissionSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppPermissionEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
