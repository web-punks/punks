import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppPermissionCreateDto,
  AppPermissionDto,
  AppPermissionListItemDto,
  AppPermissionUpdateDto,
} from "./appPermission.dto"
import { AppPermissionEntity } from "../../database/core/entities/appPermission.entity"

@WpEntityConverter("appPermission")
export class AppPermissionConverter
  implements
    IEntityConverter<
      AppPermissionEntity,
      AppPermissionDto,
      AppPermissionListItemDto,
      AppPermissionCreateDto,
      AppPermissionUpdateDto
    >
{
  toListItemDto(entity: AppPermissionEntity): AppPermissionListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppPermissionEntity): AppPermissionDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppPermissionCreateDto): DeepPartial<AppPermissionEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppPermissionUpdateDto): DeepPartial<AppPermissionEntity> {
    return {
      ...input,
    }
  }
}
