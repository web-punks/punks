import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppPermissionActions } from "./appPermission.actions"
import {
  AppPermissionDto,
  AppPermissionCreateDto,
  AppPermissionUpdateDto,
} from "./appPermission.dto"
import {
  AppPermissionExportRequest,
  AppPermissionExportResponse,
  AppPermissionSampleDownloadRequest,
  AppPermissionSampleDownloadResponse,
  AppPermissionSearchRequest,
  AppPermissionSearchResponse,
  AppPermissionVersionsSearchRequest,
  AppPermissionVersionsSearchResponse,
} from "./appPermission.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appPermission")
export class AppPermissionController {
  constructor(private readonly actions: AppPermissionActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appPermissionGet",
  })
  @ApiOkResponse({
    type: AppPermissionDto,
  })
  async item(@Param("id") id: string): Promise<AppPermissionDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appPermissionCreate",
  })
  @ApiOkResponse({
    type: AppPermissionDto,
  })
  async create(
    @Body() data: AppPermissionCreateDto
  ): Promise<AppPermissionDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppPermissionDto,
  })
  @ApiOperation({
    operationId: "appPermissionUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppPermissionUpdateDto
  ): Promise<AppPermissionDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appPermissionDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appPermissionSearch",
  })
  @ApiOkResponse({
    type: AppPermissionSearchResponse,
  })
  async search(
    @Body() request: AppPermissionSearchRequest
  ): Promise<AppPermissionSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appPermissionImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appPermissionExport",
  })
  @ApiOkResponse({
    type: AppPermissionExportResponse,
  })
  async export(
    @Body() request: AppPermissionExportRequest
  ): Promise<AppPermissionExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appPermissionSampleDownload",
  })
  @ApiOkResponse({
    type: AppPermissionSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppPermissionSampleDownloadRequest
  ): Promise<AppPermissionSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appPermissionVersions",
  })
  @ApiOkResponse({
    type: AppPermissionVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppPermissionVersionsSearchRequest
  ): Promise<AppPermissionVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
