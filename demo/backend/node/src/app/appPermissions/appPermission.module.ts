import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppPermissionActions } from "./appPermission.actions"
import { AppPermissionController } from "./appPermission.controller"
import { AppPermissionConverter } from "./appPermission.converter"
import { AppPermissionEntityModule } from "../../entities/appPermissions/appPermission.module"
import { AppPermissionSerializer } from "./appPermission.serializer"

@Module({
  imports: [SharedModule, AppPermissionEntityModule],
  providers: [
    AppPermissionActions,
    AppPermissionConverter,
    AppPermissionSerializer,
  ],
  controllers: [AppPermissionController],
})
export class AppPermissionAppModule {}
