import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppPermissionSearchParameters,
  AppPermissionSearchResults,
  AppPermissionVersionsSearchParameters,
  AppPermissionVersionsSearchResults,
} from "../../entities/appPermissions/appPermission.types"
import { AppPermissionListItemDto } from "./appPermission.dto"

export class AppPermissionSearchRequest {
  @ApiProperty()
  params: AppPermissionSearchParameters
}

export class AppPermissionSearchResponse extends AppPermissionSearchResults<AppPermissionListItemDto> {
  @ApiProperty({ type: [AppPermissionListItemDto] })
  items: AppPermissionListItemDto[]
}

export class AppPermissionEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppPermissionExportRequest {
  @ApiProperty()
  options: AppPermissionEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppPermissionSearchParameters
}

export class AppPermissionExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppPermissionSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppPermissionSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppPermissionVersionsSearchRequest {
  @ApiProperty()
  params: AppPermissionVersionsSearchParameters
}

export class AppPermissionVersionsSearchResponse extends AppPermissionVersionsSearchResults<AppPermissionListItemDto> {
  @ApiProperty({ type: [AppPermissionListItemDto] })
  items: AppPermissionListItemDto[]
}
