import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppPermissionCreateDto,
  AppPermissionDto,
  AppPermissionListItemDto,
  AppPermissionUpdateDto,
} from "./appPermission.dto"
import {
  AppPermissionEntity,
  AppPermissionEntityId,
} from "../../database/core/entities/appPermission.entity"
import {
  AppPermissionCursor,
  AppPermissionFacets,
  AppPermissionSorting,
} from "../../entities/appPermissions/appPermission.models"
import {
  AppPermissionDeleteParameters,
  AppPermissionSearchParameters,
} from "../../entities/appPermissions/appPermission.types"

@WpEntityActions("appPermission")
export class AppPermissionActions extends NestEntityActions<
  AppPermissionEntity,
  AppPermissionEntityId,
  AppPermissionCreateDto,
  AppPermissionUpdateDto,
  AppPermissionDto,
  AppPermissionListItemDto,
  AppPermissionDeleteParameters,
  AppPermissionSearchParameters,
  AppPermissionSorting,
  AppPermissionCursor,
  AppPermissionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appPermission", registry)
  }
}
