import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppEntityVersionActions } from "./appEntityVersion.actions"
import {
  AppEntityVersionDto,
  AppEntityVersionCreateDto,
  AppEntityVersionUpdateDto,
} from "./appEntityVersion.dto"
import {
  AppEntityVersionExportRequest,
  AppEntityVersionExportResponse,
  AppEntityVersionSampleDownloadRequest,
  AppEntityVersionSampleDownloadResponse,
  AppEntityVersionSearchRequest,
  AppEntityVersionSearchResponse,
  AppEntityVersionVersionsSearchRequest,
  AppEntityVersionVersionsSearchResponse,
} from "./appEntityVersion.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appEntityVersion")
export class AppEntityVersionController {
  constructor(private readonly actions: AppEntityVersionActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appEntityVersionGet",
  })
  @ApiOkResponse({
    type: AppEntityVersionDto,
  })
  async item(
    @Param("id") id: string
  ): Promise<AppEntityVersionDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appEntityVersionCreate",
  })
  @ApiOkResponse({
    type: AppEntityVersionDto,
  })
  async create(
    @Body() data: AppEntityVersionCreateDto
  ): Promise<AppEntityVersionDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppEntityVersionDto,
  })
  @ApiOperation({
    operationId: "appEntityVersionUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppEntityVersionUpdateDto
  ): Promise<AppEntityVersionDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appEntityVersionDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appEntityVersionSearch",
  })
  @ApiOkResponse({
    type: AppEntityVersionSearchResponse,
  })
  async search(
    @Body() request: AppEntityVersionSearchRequest
  ): Promise<AppEntityVersionSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appEntityVersionImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appEntityVersionExport",
  })
  @ApiOkResponse({
    type: AppEntityVersionExportResponse,
  })
  async export(
    @Body() request: AppEntityVersionExportRequest
  ): Promise<AppEntityVersionExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appEntityVersionSampleDownload",
  })
  @ApiOkResponse({
    type: AppEntityVersionSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppEntityVersionSampleDownloadRequest
  ): Promise<AppEntityVersionSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appEntityVersionVersions",
  })
  @ApiOkResponse({
    type: AppEntityVersionVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppEntityVersionVersionsSearchRequest
  ): Promise<AppEntityVersionVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
