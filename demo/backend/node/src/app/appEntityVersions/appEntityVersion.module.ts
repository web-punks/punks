import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppEntityVersionActions } from "./appEntityVersion.actions"
import { AppEntityVersionController } from "./appEntityVersion.controller"
import { AppEntityVersionConverter } from "./appEntityVersion.converter"
import { AppEntityVersionEntityModule } from "../../entities/appEntityVersions/appEntityVersion.module"
import { AppEntityVersionSerializer } from "./appEntityVersion.serializer"

@Module({
  imports: [SharedModule, AppEntityVersionEntityModule],
  providers: [
    AppEntityVersionActions,
    AppEntityVersionConverter,
    AppEntityVersionSerializer,
  ],
  controllers: [AppEntityVersionController],
})
export class AppEntityVersionAppModule {}
