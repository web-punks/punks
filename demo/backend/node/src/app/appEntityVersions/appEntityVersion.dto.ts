import { ApiProperty } from "@nestjs/swagger"

export class AppEntityVersionDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppEntityVersionListItemDto {
  @ApiProperty()
  id: string
}

export class AppEntityVersionCreateDto {}

export class AppEntityVersionUpdateDto {}
