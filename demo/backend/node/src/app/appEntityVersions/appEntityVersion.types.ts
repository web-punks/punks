import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppEntityVersionSearchParameters,
  AppEntityVersionSearchResults,
  AppEntityVersionVersionsSearchParameters,
  AppEntityVersionVersionsSearchResults,
} from "../../entities/appEntityVersions/appEntityVersion.types"
import { AppEntityVersionListItemDto } from "./appEntityVersion.dto"

export class AppEntityVersionSearchRequest {
  @ApiProperty()
  params: AppEntityVersionSearchParameters
}

export class AppEntityVersionSearchResponse extends AppEntityVersionSearchResults<AppEntityVersionListItemDto> {
  @ApiProperty({ type: [AppEntityVersionListItemDto] })
  items: AppEntityVersionListItemDto[]
}

export class AppEntityVersionEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppEntityVersionExportRequest {
  @ApiProperty()
  options: AppEntityVersionEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppEntityVersionSearchParameters
}

export class AppEntityVersionExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppEntityVersionSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppEntityVersionSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppEntityVersionVersionsSearchRequest {
  @ApiProperty()
  params: AppEntityVersionVersionsSearchParameters
}

export class AppEntityVersionVersionsSearchResponse extends AppEntityVersionVersionsSearchResults<AppEntityVersionListItemDto> {
  @ApiProperty({ type: [AppEntityVersionListItemDto] })
  items: AppEntityVersionListItemDto[]
}
