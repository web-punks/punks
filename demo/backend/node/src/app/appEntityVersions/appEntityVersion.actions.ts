import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppEntityVersionCreateDto,
  AppEntityVersionDto,
  AppEntityVersionListItemDto,
  AppEntityVersionUpdateDto,
} from "./appEntityVersion.dto"
import {
  AppEntityVersionEntity,
  AppEntityVersionEntityId,
} from "../../database/core/entities/appEntityVersion.entity"
import {
  AppEntityVersionCursor,
  AppEntityVersionFacets,
  AppEntityVersionSorting,
} from "../../entities/appEntityVersions/appEntityVersion.models"
import {
  AppEntityVersionDeleteParameters,
  AppEntityVersionSearchParameters,
} from "../../entities/appEntityVersions/appEntityVersion.types"

@WpEntityActions("appEntityVersion")
export class AppEntityVersionActions extends NestEntityActions<
  AppEntityVersionEntity,
  AppEntityVersionEntityId,
  AppEntityVersionCreateDto,
  AppEntityVersionUpdateDto,
  AppEntityVersionDto,
  AppEntityVersionListItemDto,
  AppEntityVersionDeleteParameters,
  AppEntityVersionSearchParameters,
  AppEntityVersionSorting,
  AppEntityVersionCursor,
  AppEntityVersionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEntityVersion", registry)
  }
}
