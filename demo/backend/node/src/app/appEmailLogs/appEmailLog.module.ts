import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppEmailLogActions } from "./appEmailLog.actions"
import { AppEmailLogController } from "./appEmailLog.controller"
import { AppEmailLogConverter } from "./appEmailLog.converter"
import { AppEmailLogEntityModule } from "../../entities/appEmailLogs/appEmailLog.module"
import { AppEmailLogSerializer } from "./appEmailLog.serializer"

@Module({
  imports: [SharedModule, AppEmailLogEntityModule],
  providers: [
    AppEmailLogActions,
    AppEmailLogConverter,
    AppEmailLogSerializer,
  ],
  controllers: [AppEmailLogController],
})
export class AppEmailLogAppModule {}
