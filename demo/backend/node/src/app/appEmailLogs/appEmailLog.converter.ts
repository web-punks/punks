import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppEmailLogCreateDto,
  AppEmailLogDto,
  AppEmailLogListItemDto,
  AppEmailLogUpdateDto,
} from "./appEmailLog.dto"
import { AppEmailLogEntity } from "../../database/core/entities/appEmailLog.entity"

@WpEntityConverter("appEmailLog")
export class AppEmailLogConverter
  implements
    IEntityConverter<
      AppEmailLogEntity,
      AppEmailLogDto,
      AppEmailLogListItemDto,
      AppEmailLogCreateDto,
      AppEmailLogUpdateDto
    >
{
  toListItemDto(entity: AppEmailLogEntity): AppEmailLogListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppEmailLogEntity): AppEmailLogDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppEmailLogCreateDto): DeepPartial<AppEmailLogEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppEmailLogUpdateDto): DeepPartial<AppEmailLogEntity> {
    return {
      ...input,
    }
  }
}
