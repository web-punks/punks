import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppEmailLogCreateDto,
  AppEmailLogDto,
  AppEmailLogListItemDto,
  AppEmailLogUpdateDto,
} from "./appEmailLog.dto"
import {
  AppEmailLogEntity,
  AppEmailLogEntityId,
} from "../../database/core/entities/appEmailLog.entity"
import {
  AppEmailLogCursor,
  AppEmailLogFacets,
  AppEmailLogSorting,
} from "../../entities/appEmailLogs/appEmailLog.models"
import {
  AppEmailLogDeleteParameters,
  AppEmailLogSearchParameters,
} from "../../entities/appEmailLogs/appEmailLog.types"

@WpEntityActions("appEmailLog")
export class AppEmailLogActions extends NestEntityActions<
  AppEmailLogEntity,
  AppEmailLogEntityId,
  AppEmailLogCreateDto,
  AppEmailLogUpdateDto,
  AppEmailLogDto,
  AppEmailLogListItemDto,
  AppEmailLogDeleteParameters,
  AppEmailLogSearchParameters,
  AppEmailLogSorting,
  AppEmailLogCursor,
  AppEmailLogFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEmailLog", registry)
  }
}
