import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppEmailLogEntity,
  AppEmailLogEntityId,
} from "../../database/core/entities/appEmailLog.entity"
import { AppEmailLogSearchParameters } from "../../entities/appEmailLogs/appEmailLog.types"
import {
  AppEmailLogCreateData,
  AppEmailLogUpdateData,
  AppEmailLogSorting,
  AppEmailLogCursor,
  AppEmailLogSheetItem,
} from "../../entities/appEmailLogs/appEmailLog.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appEmailLog")
export class AppEmailLogSerializer extends NestEntitySerializer<
  AppEmailLogEntity,
  AppEmailLogEntityId,
  AppEmailLogCreateData,
  AppEmailLogUpdateData,
  AppEmailLogSearchParameters,
  AppEmailLogSorting,
  AppEmailLogCursor,
  AppEmailLogSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEmailLog", registry)
  }

  protected async loadEntities(
    filters: AppEmailLogSearchParameters
  ): Promise<AppEmailLogEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppEmailLogEntity[]
  ): Promise<AppEmailLogSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppEmailLogSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppEmailLogEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
