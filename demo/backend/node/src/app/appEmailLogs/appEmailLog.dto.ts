import { ApiProperty } from "@nestjs/swagger"

export class AppEmailLogDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  emailType: string

  @ApiProperty({ type: [String], required: false })
  to?: string[]

  @ApiProperty({ type: [String], required: false })
  cc?: string[]

  @ApiProperty({ type: [String], required: false })
  bcc?: string[]

  @ApiProperty({ type: Object, required: false })
  payload?: any

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppEmailLogListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  emailType: string

  @ApiProperty({ type: [String], required: false })
  to?: string[]

  @ApiProperty({ type: [String], required: false })
  cc?: string[]

  @ApiProperty({ type: [String], required: false })
  bcc?: string[]

  @ApiProperty({ type: Object, required: false })
  payload?: any

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppEmailLogCreateDto {}

export class AppEmailLogUpdateDto {}
