import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppEmailLogActions } from "./appEmailLog.actions"
import {
  AppEmailLogDto,
  AppEmailLogCreateDto,
  AppEmailLogUpdateDto,
} from "./appEmailLog.dto"
import {
  AppEmailLogExportRequest,
  AppEmailLogExportResponse,
  AppEmailLogSampleDownloadRequest,
  AppEmailLogSampleDownloadResponse,
  AppEmailLogSearchRequest,
  AppEmailLogSearchResponse,
  AppEmailLogVersionsSearchRequest,
  AppEmailLogVersionsSearchResponse,
} from "./appEmailLog.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appEmailLog")
export class AppEmailLogController {
  constructor(private readonly actions: AppEmailLogActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appEmailLogGet",
  })
  @ApiOkResponse({
    type: AppEmailLogDto,
  })
  async item(@Param("id") id: string): Promise<AppEmailLogDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appEmailLogCreate",
  })
  @ApiOkResponse({
    type: AppEmailLogDto,
  })
  async create(@Body() data: AppEmailLogCreateDto): Promise<AppEmailLogDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppEmailLogDto,
  })
  @ApiOperation({
    operationId: "appEmailLogUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppEmailLogUpdateDto
  ): Promise<AppEmailLogDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appEmailLogDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appEmailLogSearch",
  })
  @ApiOkResponse({
    type: AppEmailLogSearchResponse,
  })
  async search(
    @Body() request: AppEmailLogSearchRequest
  ): Promise<AppEmailLogSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appEmailLogImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appEmailLogExport",
  })
  @ApiOkResponse({
    type: AppEmailLogExportResponse,
  })
  async export(
    @Body() request: AppEmailLogExportRequest
  ): Promise<AppEmailLogExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appEmailLogSampleDownload",
  })
  @ApiOkResponse({
    type: AppEmailLogSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppEmailLogSampleDownloadRequest
  ): Promise<AppEmailLogSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appEmailLogVersions",
  })
  @ApiOkResponse({
    type: AppEmailLogVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppEmailLogVersionsSearchRequest
  ): Promise<AppEmailLogVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
