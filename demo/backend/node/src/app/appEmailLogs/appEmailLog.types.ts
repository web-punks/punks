import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppEmailLogSearchParameters,
  AppEmailLogSearchResults,
  AppEmailLogVersionsSearchParameters,
  AppEmailLogVersionsSearchResults,
} from "../../entities/appEmailLogs/appEmailLog.types"
import { AppEmailLogListItemDto } from "./appEmailLog.dto"

export class AppEmailLogSearchRequest {
  @ApiProperty()
  params: AppEmailLogSearchParameters
}

export class AppEmailLogSearchResponse extends AppEmailLogSearchResults<AppEmailLogListItemDto> {
  @ApiProperty({ type: [AppEmailLogListItemDto] })
  items: AppEmailLogListItemDto[]
}

export class AppEmailLogEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppEmailLogExportRequest {
  @ApiProperty()
  options: AppEmailLogEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppEmailLogSearchParameters
}

export class AppEmailLogExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppEmailLogSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppEmailLogSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppEmailLogVersionsSearchRequest {
  @ApiProperty()
  params: AppEmailLogVersionsSearchParameters
}

export class AppEmailLogVersionsSearchResponse extends AppEmailLogVersionsSearchResults<AppEmailLogListItemDto> {
  @ApiProperty({ type: [AppEmailLogListItemDto] })
  items: AppEmailLogListItemDto[]
}
