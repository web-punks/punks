import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppUserGroupMemberCreateDto,
  AppUserGroupMemberDto,
  AppUserGroupMemberListItemDto,
  AppUserGroupMemberUpdateDto,
} from "./appUserGroupMember.dto"
import { AppUserGroupMemberEntity } from "../../database/core/entities/appUserGroupMember.entity"

@WpEntityConverter("appUserGroupMember")
export class AppUserGroupMemberConverter
  implements
    IEntityConverter<
      AppUserGroupMemberEntity,
      AppUserGroupMemberDto,
      AppUserGroupMemberListItemDto,
      AppUserGroupMemberCreateDto,
      AppUserGroupMemberUpdateDto
    >
{
  toListItemDto(entity: AppUserGroupMemberEntity): AppUserGroupMemberListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppUserGroupMemberEntity): AppUserGroupMemberDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppUserGroupMemberCreateDto): DeepPartial<AppUserGroupMemberEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppUserGroupMemberUpdateDto): DeepPartial<AppUserGroupMemberEntity> {
    return {
      ...input,
    }
  }
}
