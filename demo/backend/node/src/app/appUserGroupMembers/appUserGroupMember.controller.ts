import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppUserGroupMemberActions } from "./appUserGroupMember.actions"
import {
  AppUserGroupMemberDto,
  AppUserGroupMemberCreateDto,
  AppUserGroupMemberUpdateDto,
} from "./appUserGroupMember.dto"
import {
  AppUserGroupMemberExportRequest,
  AppUserGroupMemberExportResponse,
  AppUserGroupMemberSampleDownloadRequest,
  AppUserGroupMemberSampleDownloadResponse,
  AppUserGroupMemberSearchRequest,
  AppUserGroupMemberSearchResponse,
  AppUserGroupMemberVersionsSearchRequest,
  AppUserGroupMemberVersionsSearchResponse,
} from "./appUserGroupMember.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appUserGroupMember")
export class AppUserGroupMemberController {
  constructor(private readonly actions: AppUserGroupMemberActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appUserGroupMemberGet",
  })
  @ApiOkResponse({
    type: AppUserGroupMemberDto,
  })
  async item(
    @Param("id") id: string
  ): Promise<AppUserGroupMemberDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appUserGroupMemberCreate",
  })
  @ApiOkResponse({
    type: AppUserGroupMemberDto,
  })
  async create(
    @Body() data: AppUserGroupMemberCreateDto
  ): Promise<AppUserGroupMemberDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppUserGroupMemberDto,
  })
  @ApiOperation({
    operationId: "appUserGroupMemberUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppUserGroupMemberUpdateDto
  ): Promise<AppUserGroupMemberDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appUserGroupMemberDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appUserGroupMemberSearch",
  })
  @ApiOkResponse({
    type: AppUserGroupMemberSearchResponse,
  })
  async search(
    @Body() request: AppUserGroupMemberSearchRequest
  ): Promise<AppUserGroupMemberSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appUserGroupMemberImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appUserGroupMemberExport",
  })
  @ApiOkResponse({
    type: AppUserGroupMemberExportResponse,
  })
  async export(
    @Body() request: AppUserGroupMemberExportRequest
  ): Promise<AppUserGroupMemberExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appUserGroupMemberSampleDownload",
  })
  @ApiOkResponse({
    type: AppUserGroupMemberSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppUserGroupMemberSampleDownloadRequest
  ): Promise<AppUserGroupMemberSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appUserGroupMemberVersions",
  })
  @ApiOkResponse({
    type: AppUserGroupMemberVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppUserGroupMemberVersionsSearchRequest
  ): Promise<AppUserGroupMemberVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
