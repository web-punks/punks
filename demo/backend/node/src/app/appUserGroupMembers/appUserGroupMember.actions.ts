import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppUserGroupMemberCreateDto,
  AppUserGroupMemberDto,
  AppUserGroupMemberListItemDto,
  AppUserGroupMemberUpdateDto,
} from "./appUserGroupMember.dto"
import {
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
} from "../../database/core/entities/appUserGroupMember.entity"
import {
  AppUserGroupMemberCursor,
  AppUserGroupMemberFacets,
  AppUserGroupMemberSorting,
} from "../../entities/appUserGroupMembers/appUserGroupMember.models"
import {
  AppUserGroupMemberDeleteParameters,
  AppUserGroupMemberSearchParameters,
} from "../../entities/appUserGroupMembers/appUserGroupMember.types"

@WpEntityActions("appUserGroupMember")
export class AppUserGroupMemberActions extends NestEntityActions<
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
  AppUserGroupMemberCreateDto,
  AppUserGroupMemberUpdateDto,
  AppUserGroupMemberDto,
  AppUserGroupMemberListItemDto,
  AppUserGroupMemberDeleteParameters,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSorting,
  AppUserGroupMemberCursor,
  AppUserGroupMemberFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroupMember", registry)
  }
}
