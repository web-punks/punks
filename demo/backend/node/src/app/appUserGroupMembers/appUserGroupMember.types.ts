import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSearchResults,
  AppUserGroupMemberVersionsSearchParameters,
  AppUserGroupMemberVersionsSearchResults,
} from "../../entities/appUserGroupMembers/appUserGroupMember.types"
import { AppUserGroupMemberListItemDto } from "./appUserGroupMember.dto"

export class AppUserGroupMemberSearchRequest {
  @ApiProperty()
  params: AppUserGroupMemberSearchParameters
}

export class AppUserGroupMemberSearchResponse extends AppUserGroupMemberSearchResults<AppUserGroupMemberListItemDto> {
  @ApiProperty({ type: [AppUserGroupMemberListItemDto] })
  items: AppUserGroupMemberListItemDto[]
}

export class AppUserGroupMemberEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppUserGroupMemberExportRequest {
  @ApiProperty()
  options: AppUserGroupMemberEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppUserGroupMemberSearchParameters
}

export class AppUserGroupMemberExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppUserGroupMemberSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppUserGroupMemberSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppUserGroupMemberVersionsSearchRequest {
  @ApiProperty()
  params: AppUserGroupMemberVersionsSearchParameters
}

export class AppUserGroupMemberVersionsSearchResponse extends AppUserGroupMemberVersionsSearchResults<AppUserGroupMemberListItemDto> {
  @ApiProperty({ type: [AppUserGroupMemberListItemDto] })
  items: AppUserGroupMemberListItemDto[]
}
