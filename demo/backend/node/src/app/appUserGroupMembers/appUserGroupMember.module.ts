import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppUserGroupMemberActions } from "./appUserGroupMember.actions"
import { AppUserGroupMemberController } from "./appUserGroupMember.controller"
import { AppUserGroupMemberConverter } from "./appUserGroupMember.converter"
import { AppUserGroupMemberEntityModule } from "../../entities/appUserGroupMembers/appUserGroupMember.module"
import { AppUserGroupMemberSerializer } from "./appUserGroupMember.serializer"

@Module({
  imports: [SharedModule, AppUserGroupMemberEntityModule],
  providers: [
    AppUserGroupMemberActions,
    AppUserGroupMemberConverter,
    AppUserGroupMemberSerializer,
  ],
  controllers: [AppUserGroupMemberController],
})
export class AppUserGroupMemberAppModule {}
