import { ApiProperty } from "@nestjs/swagger"

export class AppUserGroupMemberDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserGroupMemberListItemDto {
  @ApiProperty()
  id: string
}

export class AppUserGroupMemberCreateDto {}

export class AppUserGroupMemberUpdateDto {}
