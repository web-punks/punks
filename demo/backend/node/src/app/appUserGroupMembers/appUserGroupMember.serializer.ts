import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
} from "../../database/core/entities/appUserGroupMember.entity"
import { AppUserGroupMemberSearchParameters } from "../../entities/appUserGroupMembers/appUserGroupMember.types"
import {
  AppUserGroupMemberCreateData,
  AppUserGroupMemberUpdateData,
  AppUserGroupMemberSorting,
  AppUserGroupMemberCursor,
  AppUserGroupMemberSheetItem,
} from "../../entities/appUserGroupMembers/appUserGroupMember.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appUserGroupMember")
export class AppUserGroupMemberSerializer extends NestEntitySerializer<
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
  AppUserGroupMemberCreateData,
  AppUserGroupMemberUpdateData,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSorting,
  AppUserGroupMemberCursor,
  AppUserGroupMemberSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroupMember", registry)
  }

  protected async loadEntities(
    filters: AppUserGroupMemberSearchParameters
  ): Promise<AppUserGroupMemberEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppUserGroupMemberEntity[]
  ): Promise<AppUserGroupMemberSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppUserGroupMemberSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppUserGroupMemberEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
