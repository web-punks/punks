import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppCompanySearchParameters,
  AppCompanySearchResults,
  AppCompanyVersionsSearchParameters,
  AppCompanyVersionsSearchResults,
} from "../../entities/appCompanies/appCompany.types"
import { AppCompanyListItemDto } from "./appCompany.dto"

export class AppCompanySearchRequest {
  @ApiProperty()
  params: AppCompanySearchParameters
}

export class AppCompanySearchResponse extends AppCompanySearchResults<AppCompanyListItemDto> {
  @ApiProperty({ type: [AppCompanyListItemDto] })
  items: AppCompanyListItemDto[]
}

export class AppCompanyEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppCompanyExportRequest {
  @ApiProperty()
  options: AppCompanyEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppCompanySearchParameters
}

export class AppCompanyExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppCompanySampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppCompanySampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppCompanyVersionsSearchRequest {
  @ApiProperty()
  params: AppCompanyVersionsSearchParameters
}

export class AppCompanyVersionsSearchResponse extends AppCompanyVersionsSearchResults<AppCompanyListItemDto> {
  @ApiProperty({ type: [AppCompanyListItemDto] })
  items: AppCompanyListItemDto[]
}
