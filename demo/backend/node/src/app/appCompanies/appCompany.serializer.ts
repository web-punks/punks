import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppCompanyEntity,
  AppCompanyEntityId,
} from "../../database/core/entities/appCompany.entity"
import { AppCompanySearchParameters } from "../../entities/appCompanies/appCompany.types"
import {
  AppCompanyCreateData,
  AppCompanyUpdateData,
  AppCompanySorting,
  AppCompanyCursor,
  AppCompanySheetItem,
} from "../../entities/appCompanies/appCompany.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appCompany")
export class AppCompanySerializer extends NestEntitySerializer<
  AppCompanyEntity,
  AppCompanyEntityId,
  AppCompanyCreateData,
  AppCompanyUpdateData,
  AppCompanySearchParameters,
  AppCompanySorting,
  AppCompanyCursor,
  AppCompanySheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appCompany", registry)
  }

  protected async loadEntities(
    filters: AppCompanySearchParameters
  ): Promise<AppCompanyEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppCompanyEntity[]
  ): Promise<AppCompanySheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppCompanySheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppCompanyEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
