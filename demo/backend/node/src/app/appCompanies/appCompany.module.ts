import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppCompanyActions } from "./appCompany.actions"
import { AppCompanyController } from "./appCompany.controller"
import { AppCompanyConverter } from "./appCompany.converter"
import { AppCompanyEntityModule } from "../../entities/appCompanies/appCompany.module"
import { AppCompanySerializer } from "./appCompany.serializer"

@Module({
  imports: [SharedModule, AppCompanyEntityModule],
  providers: [
    AppCompanyActions,
    AppCompanyConverter,
    AppCompanySerializer,
  ],
  controllers: [AppCompanyController],
})
export class AppCompanyAppModule {}
