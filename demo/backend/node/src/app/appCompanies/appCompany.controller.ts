import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppCompanyActions } from "./appCompany.actions"
import {
  AppCompanyDto,
  AppCompanyCreateDto,
  AppCompanyUpdateDto,
} from "./appCompany.dto"
import {
  AppCompanyExportRequest,
  AppCompanyExportResponse,
  AppCompanySampleDownloadRequest,
  AppCompanySampleDownloadResponse,
  AppCompanySearchRequest,
  AppCompanySearchResponse,
  AppCompanyVersionsSearchRequest,
  AppCompanyVersionsSearchResponse,
} from "./appCompany.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appCompany")
export class AppCompanyController {
  constructor(private readonly actions: AppCompanyActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appCompanyGet",
  })
  @ApiOkResponse({
    type: AppCompanyDto,
  })
  async item(@Param("id") id: string): Promise<AppCompanyDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appCompanyCreate",
  })
  @ApiOkResponse({
    type: AppCompanyDto,
  })
  async create(@Body() data: AppCompanyCreateDto): Promise<AppCompanyDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppCompanyDto,
  })
  @ApiOperation({
    operationId: "appCompanyUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppCompanyUpdateDto
  ): Promise<AppCompanyDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appCompanyDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appCompanySearch",
  })
  @ApiOkResponse({
    type: AppCompanySearchResponse,
  })
  async search(
    @Body() request: AppCompanySearchRequest
  ): Promise<AppCompanySearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appCompanyImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appCompanyExport",
  })
  @ApiOkResponse({
    type: AppCompanyExportResponse,
  })
  async export(
    @Body() request: AppCompanyExportRequest
  ): Promise<AppCompanyExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appCompanySampleDownload",
  })
  @ApiOkResponse({
    type: AppCompanySampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppCompanySampleDownloadRequest
  ): Promise<AppCompanySampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appCompanyVersions",
  })
  @ApiOkResponse({
    type: AppCompanyVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppCompanyVersionsSearchRequest
  ): Promise<AppCompanyVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
