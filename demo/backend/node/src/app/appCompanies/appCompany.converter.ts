import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppCompanyCreateDto,
  AppCompanyDto,
  AppCompanyListItemDto,
  AppCompanyUpdateDto,
} from "./appCompany.dto"
import { AppCompanyEntity } from "../../database/core/entities/appCompany.entity"

@WpEntityConverter("appCompany")
export class AppCompanyConverter
  implements
    IEntityConverter<
      AppCompanyEntity,
      AppCompanyDto,
      AppCompanyListItemDto,
      AppCompanyCreateDto,
      AppCompanyUpdateDto
    >
{
  toListItemDto(entity: AppCompanyEntity): AppCompanyListItemDto {
    return {
      ...entity,
      organization: {
        id: entity.organization.id,
        name: entity.organization.name,
        uid: entity.organization.uid,
      },
    }
  }

  toEntityDto(entity: AppCompanyEntity): AppCompanyDto {
    return {
      ...entity,
      organization: {
        id: entity.organization.id,
        name: entity.organization.name,
        uid: entity.organization.uid,
      },
    }
  }

  createDtoToEntity(input: AppCompanyCreateDto): DeepPartial<AppCompanyEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppCompanyUpdateDto): DeepPartial<AppCompanyEntity> {
    return {
      ...input,
    }
  }
}
