import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppCompanyCreateDto,
  AppCompanyDto,
  AppCompanyListItemDto,
  AppCompanyUpdateDto,
} from "./appCompany.dto"
import {
  AppCompanyEntity,
  AppCompanyEntityId,
} from "../../database/core/entities/appCompany.entity"
import {
  AppCompanyCursor,
  AppCompanyFacets,
  AppCompanySorting,
} from "../../entities/appCompanies/appCompany.models"
import {
  AppCompanyDeleteParameters,
  AppCompanySearchParameters,
} from "../../entities/appCompanies/appCompany.types"

@WpEntityActions("appCompany")
export class AppCompanyActions extends NestEntityActions<
  AppCompanyEntity,
  AppCompanyEntityId,
  AppCompanyCreateDto,
  AppCompanyUpdateDto,
  AppCompanyDto,
  AppCompanyListItemDto,
  AppCompanyDeleteParameters,
  AppCompanySearchParameters,
  AppCompanySorting,
  AppCompanyCursor,
  AppCompanyFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appCompany", registry)
  }
}
