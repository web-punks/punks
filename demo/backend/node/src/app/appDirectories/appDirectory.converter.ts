import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppDirectoryCreateDto,
  AppDirectoryDto,
  AppDirectoryListItemDto,
  AppDirectoryUpdateDto,
} from "./appDirectory.dto"
import { AppDirectoryEntity } from "../../database/core/entities/appDirectory.entity"

@WpEntityConverter("appDirectory")
export class AppDirectoryConverter
  implements
    IEntityConverter<
      AppDirectoryEntity,
      AppDirectoryDto,
      AppDirectoryListItemDto,
      AppDirectoryCreateDto,
      AppDirectoryUpdateDto
    >
{
  toListItemDto(entity: AppDirectoryEntity): AppDirectoryListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppDirectoryEntity): AppDirectoryDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppDirectoryCreateDto): DeepPartial<AppDirectoryEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppDirectoryUpdateDto): DeepPartial<AppDirectoryEntity> {
    return {
      ...input,
    }
  }
}
