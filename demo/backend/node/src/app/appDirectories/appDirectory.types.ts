import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppDirectorySearchParameters,
  AppDirectorySearchResults,
  AppDirectoryVersionsSearchParameters,
  AppDirectoryVersionsSearchResults,
} from "../../entities/appDirectories/appDirectory.types"
import { AppDirectoryListItemDto } from "./appDirectory.dto"

export class AppDirectorySearchRequest {
  @ApiProperty()
  params: AppDirectorySearchParameters
}

export class AppDirectorySearchResponse extends AppDirectorySearchResults<AppDirectoryListItemDto> {
  @ApiProperty({ type: [AppDirectoryListItemDto] })
  items: AppDirectoryListItemDto[]
}

export class AppDirectoryEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppDirectoryExportRequest {
  @ApiProperty()
  options: AppDirectoryEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppDirectorySearchParameters
}

export class AppDirectoryExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppDirectorySampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppDirectorySampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppDirectoryVersionsSearchRequest {
  @ApiProperty()
  params: AppDirectoryVersionsSearchParameters
}

export class AppDirectoryVersionsSearchResponse extends AppDirectoryVersionsSearchResults<AppDirectoryListItemDto> {
  @ApiProperty({ type: [AppDirectoryListItemDto] })
  items: AppDirectoryListItemDto[]
}
