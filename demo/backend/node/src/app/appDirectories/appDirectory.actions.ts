import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppDirectoryCreateDto,
  AppDirectoryDto,
  AppDirectoryListItemDto,
  AppDirectoryUpdateDto,
} from "./appDirectory.dto"
import {
  AppDirectoryEntity,
  AppDirectoryEntityId,
} from "../../database/core/entities/appDirectory.entity"
import {
  AppDirectoryCursor,
  AppDirectoryFacets,
  AppDirectorySorting,
} from "../../entities/appDirectories/appDirectory.models"
import {
  AppDirectoryDeleteParameters,
  AppDirectorySearchParameters,
} from "../../entities/appDirectories/appDirectory.types"

@WpEntityActions("appDirectory")
export class AppDirectoryActions extends NestEntityActions<
  AppDirectoryEntity,
  AppDirectoryEntityId,
  AppDirectoryCreateDto,
  AppDirectoryUpdateDto,
  AppDirectoryDto,
  AppDirectoryListItemDto,
  AppDirectoryDeleteParameters,
  AppDirectorySearchParameters,
  AppDirectorySorting,
  AppDirectoryCursor,
  AppDirectoryFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDirectory", registry)
  }
}
