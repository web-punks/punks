import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppDirectoryEntity,
  AppDirectoryEntityId,
} from "../../database/core/entities/appDirectory.entity"
import { AppDirectorySearchParameters } from "../../entities/appDirectories/appDirectory.types"
import {
  AppDirectoryCreateData,
  AppDirectoryUpdateData,
  AppDirectorySorting,
  AppDirectoryCursor,
  AppDirectorySheetItem,
} from "../../entities/appDirectories/appDirectory.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appDirectory")
export class AppDirectorySerializer extends NestEntitySerializer<
  AppDirectoryEntity,
  AppDirectoryEntityId,
  AppDirectoryCreateData,
  AppDirectoryUpdateData,
  AppDirectorySearchParameters,
  AppDirectorySorting,
  AppDirectoryCursor,
  AppDirectorySheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDirectory", registry)
  }

  protected async loadEntities(
    filters: AppDirectorySearchParameters
  ): Promise<AppDirectoryEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppDirectoryEntity[]
  ): Promise<AppDirectorySheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppDirectorySheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppDirectoryEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
