import { Controller, Delete, Get, Query } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { TenantManagers } from "@/middleware/authentication/guards"
import { AppCacheActions } from "./appCache.actions"
import {
  AppCacheInstanceDto,
  AppCacheItemDto,
  AppCacheListItemDto,
} from "./appCache.dto"

@TenantManagers()
@Controller("v1/appCache")
export class AppCacheAdminController {
  constructor(private readonly actions: AppCacheActions) {}

  @Get("instances")
  @ApiOkResponse({
    type: [AppCacheInstanceDto],
  })
  @ApiOperation({
    operationId: "cacheInstances",
  })
  async instances(): Promise<AppCacheInstanceDto[]> {
    return await this.actions.getInstances()
  }

  @Get("getItems")
  @ApiOkResponse({
    type: [AppCacheListItemDto],
  })
  @ApiOperation({
    operationId: "cacheInstanceItems",
  })
  async getItems(
    @Query("instanceName") instanceName: string
  ): Promise<AppCacheListItemDto[]> {
    return await this.actions.getItems(instanceName)
  }

  @Get("getItem")
  @ApiOkResponse({
    type: AppCacheItemDto,
  })
  @ApiOperation({
    operationId: "cacheInstanceItem",
  })
  async getItem(
    @Query("instanceName") instanceName: string,
    @Query("key") key: string
  ): Promise<AppCacheItemDto> {
    return await this.actions.getItem(instanceName, key)
  }

  @Delete("deleteItem")
  @ApiOperation({
    operationId: "cacheInstanceItemDelete",
  })
  async deleteItem(
    @Query("instanceName") instanceName: string,
    @Query("key") key: string
  ) {
    await this.actions.deleteItem(instanceName, key)
  }

  @Delete("clearInstance")
  @ApiOperation({
    operationId: "cacheInstanceClear",
  })
  async clearInstance(@Query("instanceName") instanceName: string) {
    await this.actions.clearInstance(instanceName)
  }
}
