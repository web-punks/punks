import { Module } from "@nestjs/common"
import { SharedModule } from "@/shared/module"
import { AppCacheAdminController } from "./appCache.controller"
import { AppCacheActions } from "./appCache.actions"

@Module({
  imports: [SharedModule],
  providers: [AppCacheActions],
  controllers: [AppCacheAdminController],
})
export class AppCacheAppModule {}
