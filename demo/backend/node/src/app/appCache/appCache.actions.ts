import { Injectable } from "@nestjs/common"
import { CacheService } from "@punks/backend-entity-manager"
import {
  AppCacheInstanceDto,
  AppCacheItemDto,
  AppCacheListItemDto,
} from "./appCache.dto"

@Injectable()
export class AppCacheActions {
  constructor(private readonly cacheService: CacheService) {}

  async getInstances(): Promise<AppCacheInstanceDto[]> {
    return this.cacheService.getInstances().map((instance) => ({
      name: instance.getInstanceName(),
    }))
  }

  async getItems(instanceName: string): Promise<AppCacheListItemDto[]> {
    const items = await this.cacheService.getInstance(instanceName).getEntries()
    return items.map((item) => ({
      instance: item.instanceName,
      key: item.key,
      expiration: item.expiration,
      createdOn: item.createdOn,
      updatedOn: item.updatedOn,
    }))
  }

  async getItem(instanceName: string, key: string): Promise<AppCacheItemDto> {
    const item = await this.cacheService.getInstance(instanceName).getEntry(key)
    return {
      createdOn: item.createdOn,
      data: item.data,
      expiration: item.expiration,
      instance: item.instanceName,
      key: item.key,
      updatedOn: item.updatedOn,
    }
  }

  async deleteItem(instanceName: string, key: string) {
    await this.cacheService.getInstance(instanceName).delete(key)
  }

  async clearInstance(instanceName: string) {
    await this.cacheService.getInstance(instanceName).clear()
  }
}
