import { ApiProperty } from "@nestjs/swagger"

export class AppCacheInstanceDto {
  @ApiProperty()
  name: string
}

export class AppCacheListItemDto {
  @ApiProperty()
  instance: string

  @ApiProperty()
  key: string

  @ApiProperty()
  expiration: Date

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppCacheItemDto {
  @ApiProperty()
  instance: string

  @ApiProperty()
  key: string

  @ApiProperty()
  expiration: Date

  @ApiProperty({ type: Object })
  data: any

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}
