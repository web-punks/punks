import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppRolePermissionEntity,
  AppRolePermissionEntityId,
} from "../../database/core/entities/appRolePermission.entity"
import { AppRolePermissionSearchParameters } from "../../entities/appRolePermissions/appRolePermission.types"
import {
  AppRolePermissionCreateData,
  AppRolePermissionUpdateData,
  AppRolePermissionSorting,
  AppRolePermissionCursor,
  AppRolePermissionSheetItem,
} from "../../entities/appRolePermissions/appRolePermission.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appRolePermission")
export class AppRolePermissionSerializer extends NestEntitySerializer<
  AppRolePermissionEntity,
  AppRolePermissionEntityId,
  AppRolePermissionCreateData,
  AppRolePermissionUpdateData,
  AppRolePermissionSearchParameters,
  AppRolePermissionSorting,
  AppRolePermissionCursor,
  AppRolePermissionSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRolePermission", registry)
  }

  protected async loadEntities(
    filters: AppRolePermissionSearchParameters
  ): Promise<AppRolePermissionEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppRolePermissionEntity[]
  ): Promise<AppRolePermissionSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppRolePermissionSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppRolePermissionEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
