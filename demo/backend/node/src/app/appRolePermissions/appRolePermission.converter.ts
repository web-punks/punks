import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppRolePermissionCreateDto,
  AppRolePermissionDto,
  AppRolePermissionListItemDto,
  AppRolePermissionUpdateDto,
} from "./appRolePermission.dto"
import { AppRolePermissionEntity } from "../../database/core/entities/appRolePermission.entity"

@WpEntityConverter("appRolePermission")
export class AppRolePermissionConverter
  implements
    IEntityConverter<
      AppRolePermissionEntity,
      AppRolePermissionDto,
      AppRolePermissionListItemDto,
      AppRolePermissionCreateDto,
      AppRolePermissionUpdateDto
    >
{
  toListItemDto(entity: AppRolePermissionEntity): AppRolePermissionListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppRolePermissionEntity): AppRolePermissionDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppRolePermissionCreateDto): DeepPartial<AppRolePermissionEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppRolePermissionUpdateDto): DeepPartial<AppRolePermissionEntity> {
    return {
      ...input,
    }
  }
}
