import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppRolePermissionSearchParameters,
  AppRolePermissionSearchResults,
  AppRolePermissionVersionsSearchParameters,
  AppRolePermissionVersionsSearchResults,
} from "../../entities/appRolePermissions/appRolePermission.types"
import { AppRolePermissionListItemDto } from "./appRolePermission.dto"

export class AppRolePermissionSearchRequest {
  @ApiProperty()
  params: AppRolePermissionSearchParameters
}

export class AppRolePermissionSearchResponse extends AppRolePermissionSearchResults<AppRolePermissionListItemDto> {
  @ApiProperty({ type: [AppRolePermissionListItemDto] })
  items: AppRolePermissionListItemDto[]
}

export class AppRolePermissionEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppRolePermissionExportRequest {
  @ApiProperty()
  options: AppRolePermissionEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppRolePermissionSearchParameters
}

export class AppRolePermissionExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppRolePermissionSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppRolePermissionSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppRolePermissionVersionsSearchRequest {
  @ApiProperty()
  params: AppRolePermissionVersionsSearchParameters
}

export class AppRolePermissionVersionsSearchResponse extends AppRolePermissionVersionsSearchResults<AppRolePermissionListItemDto> {
  @ApiProperty({ type: [AppRolePermissionListItemDto] })
  items: AppRolePermissionListItemDto[]
}
