import { ApiProperty } from "@nestjs/swagger"

export class AppRolePermissionDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppRolePermissionListItemDto {
  @ApiProperty()
  id: string
}

export class AppRolePermissionCreateDto {}

export class AppRolePermissionUpdateDto {}
