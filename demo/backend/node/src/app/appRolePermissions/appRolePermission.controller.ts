import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppRolePermissionActions } from "./appRolePermission.actions"
import {
  AppRolePermissionDto,
  AppRolePermissionCreateDto,
  AppRolePermissionUpdateDto,
} from "./appRolePermission.dto"
import {
  AppRolePermissionExportRequest,
  AppRolePermissionExportResponse,
  AppRolePermissionSampleDownloadRequest,
  AppRolePermissionSampleDownloadResponse,
  AppRolePermissionSearchRequest,
  AppRolePermissionSearchResponse,
  AppRolePermissionVersionsSearchRequest,
  AppRolePermissionVersionsSearchResponse,
} from "./appRolePermission.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appRolePermission")
export class AppRolePermissionController {
  constructor(private readonly actions: AppRolePermissionActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appRolePermissionGet",
  })
  @ApiOkResponse({
    type: AppRolePermissionDto,
  })
  async item(
    @Param("id") id: string
  ): Promise<AppRolePermissionDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appRolePermissionCreate",
  })
  @ApiOkResponse({
    type: AppRolePermissionDto,
  })
  async create(
    @Body() data: AppRolePermissionCreateDto
  ): Promise<AppRolePermissionDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppRolePermissionDto,
  })
  @ApiOperation({
    operationId: "appRolePermissionUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppRolePermissionUpdateDto
  ): Promise<AppRolePermissionDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appRolePermissionDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appRolePermissionSearch",
  })
  @ApiOkResponse({
    type: AppRolePermissionSearchResponse,
  })
  async search(
    @Body() request: AppRolePermissionSearchRequest
  ): Promise<AppRolePermissionSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appRolePermissionImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appRolePermissionExport",
  })
  @ApiOkResponse({
    type: AppRolePermissionExportResponse,
  })
  async export(
    @Body() request: AppRolePermissionExportRequest
  ): Promise<AppRolePermissionExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appRolePermissionSampleDownload",
  })
  @ApiOkResponse({
    type: AppRolePermissionSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppRolePermissionSampleDownloadRequest
  ): Promise<AppRolePermissionSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appRolePermissionVersions",
  })
  @ApiOkResponse({
    type: AppRolePermissionVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppRolePermissionVersionsSearchRequest
  ): Promise<AppRolePermissionVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
