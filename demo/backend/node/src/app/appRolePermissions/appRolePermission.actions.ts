import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppRolePermissionCreateDto,
  AppRolePermissionDto,
  AppRolePermissionListItemDto,
  AppRolePermissionUpdateDto,
} from "./appRolePermission.dto"
import {
  AppRolePermissionEntity,
  AppRolePermissionEntityId,
} from "../../database/core/entities/appRolePermission.entity"
import {
  AppRolePermissionCursor,
  AppRolePermissionFacets,
  AppRolePermissionSorting,
} from "../../entities/appRolePermissions/appRolePermission.models"
import {
  AppRolePermissionDeleteParameters,
  AppRolePermissionSearchParameters,
} from "../../entities/appRolePermissions/appRolePermission.types"

@WpEntityActions("appRolePermission")
export class AppRolePermissionActions extends NestEntityActions<
  AppRolePermissionEntity,
  AppRolePermissionEntityId,
  AppRolePermissionCreateDto,
  AppRolePermissionUpdateDto,
  AppRolePermissionDto,
  AppRolePermissionListItemDto,
  AppRolePermissionDeleteParameters,
  AppRolePermissionSearchParameters,
  AppRolePermissionSorting,
  AppRolePermissionCursor,
  AppRolePermissionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRolePermission", registry)
  }
}
