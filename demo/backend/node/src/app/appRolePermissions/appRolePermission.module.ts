import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppRolePermissionActions } from "./appRolePermission.actions"
import { AppRolePermissionController } from "./appRolePermission.controller"
import { AppRolePermissionConverter } from "./appRolePermission.converter"
import { AppRolePermissionEntityModule } from "../../entities/appRolePermissions/appRolePermission.module"
import { AppRolePermissionSerializer } from "./appRolePermission.serializer"

@Module({
  imports: [SharedModule, AppRolePermissionEntityModule],
  providers: [
    AppRolePermissionActions,
    AppRolePermissionConverter,
    AppRolePermissionSerializer,
  ],
  controllers: [AppRolePermissionController],
})
export class AppRolePermissionAppModule {}
