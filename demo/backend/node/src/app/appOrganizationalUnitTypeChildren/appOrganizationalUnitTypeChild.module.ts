import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppOrganizationalUnitTypeChildActions } from "./appOrganizationalUnitTypeChild.actions"
import { AppOrganizationalUnitTypeChildController } from "./appOrganizationalUnitTypeChild.controller"
import { AppOrganizationalUnitTypeChildConverter } from "./appOrganizationalUnitTypeChild.converter"
import { AppOrganizationalUnitTypeChildEntityModule } from "../../entities/appOrganizationalUnitTypeChildren/appOrganizationalUnitTypeChild.module"
import { AppOrganizationalUnitTypeChildSerializer } from "./appOrganizationalUnitTypeChild.serializer"

@Module({
  imports: [SharedModule, AppOrganizationalUnitTypeChildEntityModule],
  providers: [
    AppOrganizationalUnitTypeChildActions,
    AppOrganizationalUnitTypeChildConverter,
    AppOrganizationalUnitTypeChildSerializer,
  ],
  controllers: [AppOrganizationalUnitTypeChildController],
})
export class AppOrganizationalUnitTypeChildAppModule {}
