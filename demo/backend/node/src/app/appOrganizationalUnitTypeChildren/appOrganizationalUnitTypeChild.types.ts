import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitTypeChildSearchParameters,
  AppOrganizationalUnitTypeChildSearchResults,
  AppOrganizationalUnitTypeChildVersionsSearchParameters,
  AppOrganizationalUnitTypeChildVersionsSearchResults,
} from "../../entities/appOrganizationalUnitTypeChildren/appOrganizationalUnitTypeChild.types"
import { AppOrganizationalUnitTypeChildListItemDto } from "./appOrganizationalUnitTypeChild.dto"

export class AppOrganizationalUnitTypeChildSearchRequest {
  @ApiProperty()
  params: AppOrganizationalUnitTypeChildSearchParameters
}

export class AppOrganizationalUnitTypeChildSearchResponse extends AppOrganizationalUnitTypeChildSearchResults<AppOrganizationalUnitTypeChildListItemDto> {
  @ApiProperty({ type: [AppOrganizationalUnitTypeChildListItemDto] })
  items: AppOrganizationalUnitTypeChildListItemDto[]
}

export class AppOrganizationalUnitTypeChildEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppOrganizationalUnitTypeChildExportRequest {
  @ApiProperty()
  options: AppOrganizationalUnitTypeChildEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppOrganizationalUnitTypeChildSearchParameters
}

export class AppOrganizationalUnitTypeChildExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppOrganizationalUnitTypeChildSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppOrganizationalUnitTypeChildSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppOrganizationalUnitTypeChildVersionsSearchRequest {
  @ApiProperty()
  params: AppOrganizationalUnitTypeChildVersionsSearchParameters
}

export class AppOrganizationalUnitTypeChildVersionsSearchResponse extends AppOrganizationalUnitTypeChildVersionsSearchResults<AppOrganizationalUnitTypeChildListItemDto> {
  @ApiProperty({ type: [AppOrganizationalUnitTypeChildListItemDto] })
  items: AppOrganizationalUnitTypeChildListItemDto[]
}
