import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitTypeChildCreateDto,
  AppOrganizationalUnitTypeChildDto,
  AppOrganizationalUnitTypeChildListItemDto,
  AppOrganizationalUnitTypeChildUpdateDto,
} from "./appOrganizationalUnitTypeChild.dto"
import {
  AppOrganizationalUnitTypeChildEntity,
  AppOrganizationalUnitTypeChildEntityId,
} from "../../database/core/entities/appOrganizationalUnitTypeChild.entity"
import {
  AppOrganizationalUnitTypeChildCursor,
  AppOrganizationalUnitTypeChildFacets,
  AppOrganizationalUnitTypeChildSorting,
} from "../../entities/appOrganizationalUnitTypeChildren/appOrganizationalUnitTypeChild.models"
import {
  AppOrganizationalUnitTypeChildDeleteParameters,
  AppOrganizationalUnitTypeChildSearchParameters,
} from "../../entities/appOrganizationalUnitTypeChildren/appOrganizationalUnitTypeChild.types"

@WpEntityActions("appOrganizationalUnitTypeChild")
export class AppOrganizationalUnitTypeChildActions extends NestEntityActions<
  AppOrganizationalUnitTypeChildEntity,
  AppOrganizationalUnitTypeChildEntityId,
  AppOrganizationalUnitTypeChildCreateDto,
  AppOrganizationalUnitTypeChildUpdateDto,
  AppOrganizationalUnitTypeChildDto,
  AppOrganizationalUnitTypeChildListItemDto,
  AppOrganizationalUnitTypeChildDeleteParameters,
  AppOrganizationalUnitTypeChildSearchParameters,
  AppOrganizationalUnitTypeChildSorting,
  AppOrganizationalUnitTypeChildCursor,
  AppOrganizationalUnitTypeChildFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganizationalUnitTypeChild", registry)
  }
}
