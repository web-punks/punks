import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitTypeChildEntity,
  AppOrganizationalUnitTypeChildEntityId,
} from "../../database/core/entities/appOrganizationalUnitTypeChild.entity"
import { AppOrganizationalUnitTypeChildSearchParameters } from "../../entities/appOrganizationalUnitTypeChildren/appOrganizationalUnitTypeChild.types"
import {
  AppOrganizationalUnitTypeChildCreateData,
  AppOrganizationalUnitTypeChildUpdateData,
  AppOrganizationalUnitTypeChildSorting,
  AppOrganizationalUnitTypeChildCursor,
  AppOrganizationalUnitTypeChildSheetItem,
} from "../../entities/appOrganizationalUnitTypeChildren/appOrganizationalUnitTypeChild.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appOrganizationalUnitTypeChild")
export class AppOrganizationalUnitTypeChildSerializer extends NestEntitySerializer<
  AppOrganizationalUnitTypeChildEntity,
  AppOrganizationalUnitTypeChildEntityId,
  AppOrganizationalUnitTypeChildCreateData,
  AppOrganizationalUnitTypeChildUpdateData,
  AppOrganizationalUnitTypeChildSearchParameters,
  AppOrganizationalUnitTypeChildSorting,
  AppOrganizationalUnitTypeChildCursor,
  AppOrganizationalUnitTypeChildSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganizationalUnitTypeChild", registry)
  }

  protected async loadEntities(
    filters: AppOrganizationalUnitTypeChildSearchParameters
  ): Promise<AppOrganizationalUnitTypeChildEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppOrganizationalUnitTypeChildEntity[]
  ): Promise<AppOrganizationalUnitTypeChildSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppOrganizationalUnitTypeChildSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<
    EntitySerializerSheetDefinition<AppOrganizationalUnitTypeChildEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
