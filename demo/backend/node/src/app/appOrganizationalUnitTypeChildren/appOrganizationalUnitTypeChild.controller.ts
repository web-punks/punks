import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppOrganizationalUnitTypeChildActions } from "./appOrganizationalUnitTypeChild.actions"
import {
  AppOrganizationalUnitTypeChildDto,
  AppOrganizationalUnitTypeChildCreateDto,
  AppOrganizationalUnitTypeChildUpdateDto,
} from "./appOrganizationalUnitTypeChild.dto"
import {
  AppOrganizationalUnitTypeChildExportRequest,
  AppOrganizationalUnitTypeChildExportResponse,
  AppOrganizationalUnitTypeChildSampleDownloadRequest,
  AppOrganizationalUnitTypeChildSampleDownloadResponse,
  AppOrganizationalUnitTypeChildSearchRequest,
  AppOrganizationalUnitTypeChildSearchResponse,
  AppOrganizationalUnitTypeChildVersionsSearchRequest,
  AppOrganizationalUnitTypeChildVersionsSearchResponse,
} from "./appOrganizationalUnitTypeChild.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appOrganizationalUnitTypeChild")
export class AppOrganizationalUnitTypeChildController {
  constructor(
    private readonly actions: AppOrganizationalUnitTypeChildActions
  ) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeChildGet",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeChildDto,
  })
  async item(
    @Param("id") id: string
  ): Promise<AppOrganizationalUnitTypeChildDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeChildCreate",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeChildDto,
  })
  async create(
    @Body() data: AppOrganizationalUnitTypeChildCreateDto
  ): Promise<AppOrganizationalUnitTypeChildDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeChildDto,
  })
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeChildUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppOrganizationalUnitTypeChildUpdateDto
  ): Promise<AppOrganizationalUnitTypeChildDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeChildDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeChildSearch",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeChildSearchResponse,
  })
  async search(
    @Body() request: AppOrganizationalUnitTypeChildSearchRequest
  ): Promise<AppOrganizationalUnitTypeChildSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeChildImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeChildExport",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeChildExportResponse,
  })
  async export(
    @Body() request: AppOrganizationalUnitTypeChildExportRequest
  ): Promise<AppOrganizationalUnitTypeChildExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeChildSampleDownload",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeChildSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppOrganizationalUnitTypeChildSampleDownloadRequest
  ): Promise<AppOrganizationalUnitTypeChildSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appOrganizationalUnitTypeChildVersions",
  })
  @ApiOkResponse({
    type: AppOrganizationalUnitTypeChildVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppOrganizationalUnitTypeChildVersionsSearchRequest
  ): Promise<AppOrganizationalUnitTypeChildVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
