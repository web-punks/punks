import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitTypeChildCreateDto,
  AppOrganizationalUnitTypeChildDto,
  AppOrganizationalUnitTypeChildListItemDto,
  AppOrganizationalUnitTypeChildUpdateDto,
} from "./appOrganizationalUnitTypeChild.dto"
import { AppOrganizationalUnitTypeChildEntity } from "../../database/core/entities/appOrganizationalUnitTypeChild.entity"

@WpEntityConverter("appOrganizationalUnitTypeChild")
export class AppOrganizationalUnitTypeChildConverter
  implements
    IEntityConverter<
      AppOrganizationalUnitTypeChildEntity,
      AppOrganizationalUnitTypeChildDto,
      AppOrganizationalUnitTypeChildListItemDto,
      AppOrganizationalUnitTypeChildCreateDto,
      AppOrganizationalUnitTypeChildUpdateDto
    >
{
  toListItemDto(entity: AppOrganizationalUnitTypeChildEntity): AppOrganizationalUnitTypeChildListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppOrganizationalUnitTypeChildEntity): AppOrganizationalUnitTypeChildDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppOrganizationalUnitTypeChildCreateDto): DeepPartial<AppOrganizationalUnitTypeChildEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppOrganizationalUnitTypeChildUpdateDto): DeepPartial<AppOrganizationalUnitTypeChildEntity> {
    return {
      ...input,
    }
  }
}
