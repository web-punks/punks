import { ApiProperty } from "@nestjs/swagger"

export class AppOrganizationalUnitTypeChildDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppOrganizationalUnitTypeChildListItemDto {
  @ApiProperty()
  id: string
}

export class AppOrganizationalUnitTypeChildCreateDto {}

export class AppOrganizationalUnitTypeChildUpdateDto {}
