import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppUserGroupActions } from "./appUserGroup.actions"
import {
  AppUserGroupDto,
  AppUserGroupCreateDto,
  AppUserGroupUpdateDto,
} from "./appUserGroup.dto"
import {
  AppUserGroupExportRequest,
  AppUserGroupExportResponse,
  AppUserGroupSampleDownloadRequest,
  AppUserGroupSampleDownloadResponse,
  AppUserGroupSearchRequest,
  AppUserGroupSearchResponse,
  AppUserGroupVersionsSearchRequest,
  AppUserGroupVersionsSearchResponse,
} from "./appUserGroup.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appUserGroup")
export class AppUserGroupController {
  constructor(private readonly actions: AppUserGroupActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appUserGroupGet",
  })
  @ApiOkResponse({
    type: AppUserGroupDto,
  })
  async item(@Param("id") id: string): Promise<AppUserGroupDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appUserGroupCreate",
  })
  @ApiOkResponse({
    type: AppUserGroupDto,
  })
  async create(@Body() data: AppUserGroupCreateDto): Promise<AppUserGroupDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppUserGroupDto,
  })
  @ApiOperation({
    operationId: "appUserGroupUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppUserGroupUpdateDto
  ): Promise<AppUserGroupDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appUserGroupDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appUserGroupSearch",
  })
  @ApiOkResponse({
    type: AppUserGroupSearchResponse,
  })
  async search(
    @Body() request: AppUserGroupSearchRequest
  ): Promise<AppUserGroupSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appUserGroupImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appUserGroupExport",
  })
  @ApiOkResponse({
    type: AppUserGroupExportResponse,
  })
  async export(
    @Body() request: AppUserGroupExportRequest
  ): Promise<AppUserGroupExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appUserGroupSampleDownload",
  })
  @ApiOkResponse({
    type: AppUserGroupSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppUserGroupSampleDownloadRequest
  ): Promise<AppUserGroupSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appUserGroupVersions",
  })
  @ApiOkResponse({
    type: AppUserGroupVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppUserGroupVersionsSearchRequest
  ): Promise<AppUserGroupVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
