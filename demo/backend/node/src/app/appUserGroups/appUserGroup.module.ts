import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppUserGroupActions } from "./appUserGroup.actions"
import { AppUserGroupController } from "./appUserGroup.controller"
import { AppUserGroupConverter } from "./appUserGroup.converter"
import { AppUserGroupEntityModule } from "../../entities/appUserGroups/appUserGroup.module"
import { AppUserGroupSerializer } from "./appUserGroup.serializer"

@Module({
  imports: [SharedModule, AppUserGroupEntityModule],
  providers: [
    AppUserGroupActions,
    AppUserGroupConverter,
    AppUserGroupSerializer,
  ],
  controllers: [AppUserGroupController],
})
export class AppUserGroupAppModule {}
