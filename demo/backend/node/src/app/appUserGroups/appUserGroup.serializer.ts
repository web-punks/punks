import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppUserGroupEntity,
  AppUserGroupEntityId,
} from "../../database/core/entities/appUserGroup.entity"
import { AppUserGroupSearchParameters } from "../../entities/appUserGroups/appUserGroup.types"
import {
  AppUserGroupCreateData,
  AppUserGroupUpdateData,
  AppUserGroupSorting,
  AppUserGroupCursor,
  AppUserGroupSheetItem,
} from "../../entities/appUserGroups/appUserGroup.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appUserGroup")
export class AppUserGroupSerializer extends NestEntitySerializer<
  AppUserGroupEntity,
  AppUserGroupEntityId,
  AppUserGroupCreateData,
  AppUserGroupUpdateData,
  AppUserGroupSearchParameters,
  AppUserGroupSorting,
  AppUserGroupCursor,
  AppUserGroupSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroup", registry)
  }

  protected async loadEntities(
    filters: AppUserGroupSearchParameters
  ): Promise<AppUserGroupEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppUserGroupEntity[]
  ): Promise<AppUserGroupSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppUserGroupSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppUserGroupEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
