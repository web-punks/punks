import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppUserGroupCreateDto,
  AppUserGroupDto,
  AppUserGroupListItemDto,
  AppUserGroupUpdateDto,
} from "./appUserGroup.dto"
import {
  AppUserGroupEntity,
  AppUserGroupEntityId,
} from "../../database/core/entities/appUserGroup.entity"
import {
  AppUserGroupCursor,
  AppUserGroupFacets,
  AppUserGroupSorting,
} from "../../entities/appUserGroups/appUserGroup.models"
import {
  AppUserGroupDeleteParameters,
  AppUserGroupSearchParameters,
} from "../../entities/appUserGroups/appUserGroup.types"

@WpEntityActions("appUserGroup")
export class AppUserGroupActions extends NestEntityActions<
  AppUserGroupEntity,
  AppUserGroupEntityId,
  AppUserGroupCreateDto,
  AppUserGroupUpdateDto,
  AppUserGroupDto,
  AppUserGroupListItemDto,
  AppUserGroupDeleteParameters,
  AppUserGroupSearchParameters,
  AppUserGroupSorting,
  AppUserGroupCursor,
  AppUserGroupFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroup", registry)
  }
}
