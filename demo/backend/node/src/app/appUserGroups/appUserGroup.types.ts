import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppUserGroupSearchParameters,
  AppUserGroupSearchResults,
  AppUserGroupVersionsSearchParameters,
  AppUserGroupVersionsSearchResults,
} from "../../entities/appUserGroups/appUserGroup.types"
import { AppUserGroupListItemDto } from "./appUserGroup.dto"

export class AppUserGroupSearchRequest {
  @ApiProperty()
  params: AppUserGroupSearchParameters
}

export class AppUserGroupSearchResponse extends AppUserGroupSearchResults<AppUserGroupListItemDto> {
  @ApiProperty({ type: [AppUserGroupListItemDto] })
  items: AppUserGroupListItemDto[]
}

export class AppUserGroupEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppUserGroupExportRequest {
  @ApiProperty()
  options: AppUserGroupEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppUserGroupSearchParameters
}

export class AppUserGroupExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppUserGroupSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppUserGroupSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppUserGroupVersionsSearchRequest {
  @ApiProperty()
  params: AppUserGroupVersionsSearchParameters
}

export class AppUserGroupVersionsSearchResponse extends AppUserGroupVersionsSearchResults<AppUserGroupListItemDto> {
  @ApiProperty({ type: [AppUserGroupListItemDto] })
  items: AppUserGroupListItemDto[]
}
