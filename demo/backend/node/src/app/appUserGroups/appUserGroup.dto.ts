import { ApiProperty } from "@nestjs/swagger"

export class AppUserGroupDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserGroupListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserGroupCreateDto {}

export class AppUserGroupUpdateDto {}
