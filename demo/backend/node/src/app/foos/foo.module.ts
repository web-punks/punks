import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { FooActions } from "./foo.actions"
import { FooController } from "./foo.controller"
import { FooConverter } from "./foo.converter"
import { FooEntityModule } from "../../entities/foos/foo.module"
import { FooSerializer } from "./foo.serializer"

@Module({
  imports: [SharedModule, FooEntityModule],
  providers: [FooActions, FooConverter, FooSerializer],
  controllers: [FooController],
})
export class FooAppModule {}
