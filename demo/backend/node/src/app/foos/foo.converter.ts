import {
  DeepPartial,
  IEntityConverter,
  WpEntityConverter,
} from "@punks/backend-entity-manager"
import { FooCreateDto, FooDto, FooListItemDto, FooUpdateDto } from "./foo.dto"
import { FooEntity } from "../../database/core/entities/foo.entity"

@WpEntityConverter("foo")
export class FooConverter
  implements
    IEntityConverter<
      FooEntity,
      FooDto,
      FooListItemDto,
      FooCreateDto,
      FooUpdateDto
    >
{
  toListItemDto(entity: FooEntity): FooListItemDto {
    return this.toEntityDto(entity)
  }

  toEntityDto(entity: FooEntity): FooDto {
    const { parent, ...data } = entity
    return {
      ...data,
      parentId: parent?.id,
    }
  }

  createDtoToEntity(input: FooCreateDto): DeepPartial<FooEntity> {
    return {
      ...input,
      parent: input.parentId
        ? {
            id: input.parentId,
          }
        : undefined,
    }
  }

  updateDtoToEntity(input: FooUpdateDto): Partial<FooEntity> {
    return {
      ...input,
    }
  }
}
