import {
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
  fieldRequiredValidator,
  NestEntitySerializer,
  WpEntitySerializer,
} from "@punks/backend-entity-manager"
import { FooEntity } from "../../database/core/entities/foo.entity"
import {
  FooCreateData,
  FooCursor,
  FooEntityId,
  FooSheetItem,
  FooSorting,
  FooUpdateData,
} from "../../entities/foos/foo.models"
import { FooSearchParameters } from "../../entities/foos/foo.types"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("foo")
export class FooSerializer extends NestEntitySerializer<
  FooEntity,
  FooEntityId,
  FooCreateData,
  FooUpdateData,
  FooSearchParameters,
  FooSorting,
  FooCursor,
  FooSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("foo", registry)
  }

  protected async loadEntities(
    filters: FooSearchParameters
  ): Promise<FooEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: FooEntity[]
  ): Promise<FooSheetItem[]> {
    return entities
  }

  protected async importItem(item: FooSheetItem, context: AppAuthContext) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<FooEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
        {
          name: "Name",
          key: "name",
          selector: "name",
          parser: (value) => (value?.toString() as string).toUpperCase(),
          converter: (value) => (value?.toString() as string).toLowerCase(),
          sampleValue: "Foo",
          validators: [
            {
              key: "required",
              fn: (item) => fieldRequiredValidator(item, (x) => x.name),
            },
          ],
        },
        {
          name: "Uid",
          key: "uid",
          selector: "uid",
          sampleValue: "foo",
          validators: [
            {
              key: "required",
              fn: (item) => fieldRequiredValidator(item, (x) => x.uid),
            },
          ],
        },
        // {
        //   name: "Age",
        //   selector: "age",
        //   parser: (value) => parseInt(value?.toString() as string),
        //   sampleValue: 18,
        // },
      ],
    }
  }
}
