import { ApiProperty } from "@nestjs/swagger"
import {
  FooSearchParameters,
  FooSearchResults,
  FooVersionsSearchParameters,
  FooVersionsSearchResults,
} from "@/entities/foos/foo.types"
import {
  EntityParseResult,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiFile } from "@/shared/interceptors/files"
import { EntitiesChildrenMap } from "@/shared/api/traverse"

export class FooDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string

  @ApiProperty({ required: false })
  other?: string

  @ApiProperty()
  type: string

  @ApiProperty()
  age: number

  @ApiProperty({ required: false })
  parentId?: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class FooListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string

  @ApiProperty({ required: false })
  other?: string

  @ApiProperty()
  type: string

  @ApiProperty()
  age: number

  @ApiProperty({ required: false })
  parentId?: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class FooCreateDto {
  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  type: string

  @ApiProperty({ required: false })
  other?: string

  @ApiProperty()
  age: number

  @ApiProperty({ required: false })
  parentId?: string
}

export class FooUpdateDto {
  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  type: string

  @ApiProperty({ required: false })
  other?: string

  @ApiProperty()
  age: number
}

export class FooSearchRequest {
  @ApiProperty()
  params: FooSearchParameters
}

export class FooSearchResponse extends FooSearchResults<FooListItemDto> {
  @ApiProperty({ type: [FooListItemDto] })
  items: FooListItemDto[]

  @ApiProperty({
    type: "object",
    required: false,
  })
  childrenMap?: EntitiesChildrenMap
}

export class FooParseRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class FooParseResponse {
  @ApiProperty({ type: [EntityParseResult] })
  entries: EntityParseResult[]
}

export class FooImportRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat

  @ApiFile()
  file: Express.Multer.File
}

export class FooEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class FooExportRequest {
  @ApiProperty()
  options: FooEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: FooSearchParameters
}

export class FooExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class FooSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class FooSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class FooVersionsSearchRequest {
  @ApiProperty()
  params: FooVersionsSearchParameters
}

export class FooVersionsSearchResponse extends FooVersionsSearchResults<FooListItemDto> {
  @ApiProperty({ type: [FooListItemDto] })
  items: FooListItemDto[]
}
