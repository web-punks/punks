import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import { FooCreateDto, FooDto, FooListItemDto, FooUpdateDto } from "./foo.dto"
import { FooEntity } from "../../database/core/entities/foo.entity"
import {
  FooCursor,
  FooEntityId,
  FooFacets,
  FooSorting,
} from "../../entities/foos/foo.models"
import {
  FooDeleteParameters,
  FooSearchParameters,
} from "../../entities/foos/foo.types"
import { FooRepository } from "@/database/core/repositories/foo.repository"

@WpEntityActions("foo")
export class FooActions extends NestEntityActions<
  FooEntity,
  FooEntityId,
  FooCreateDto,
  FooUpdateDto,
  FooDto,
  FooListItemDto,
  FooDeleteParameters,
  FooSearchParameters,
  FooSorting,
  FooCursor,
  FooFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("foo", registry)
  }
}
