import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppUserCreateDto,
  AppUserDto,
  AppUserListItemDto,
  AppUserUpdateDto,
} from "./appUser.dto"
import {
  AppUserEntity,
  AppUserEntityId,
} from "../../database/core/entities/appUser.entity"
import {
  AppUserCursor,
  AppUserFacets,
  AppUserSorting,
} from "../../entities/appUsers/appUser.models"
import {
  AppUserDeleteParameters,
  AppUserSearchParameters,
} from "../../entities/appUsers/appUser.types"

@WpEntityActions("appUser")
export class AppUserActions extends NestEntityActions<
  AppUserEntity,
  AppUserEntityId,
  AppUserCreateDto,
  AppUserUpdateDto,
  AppUserDto,
  AppUserListItemDto,
  AppUserDeleteParameters,
  AppUserSearchParameters,
  AppUserSorting,
  AppUserCursor,
  AppUserFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUser", registry)
  }
}
