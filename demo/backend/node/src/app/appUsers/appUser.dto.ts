import { ApiProperty } from "@nestjs/swagger"

export class AppUserProfileEntityDto {
  @ApiProperty()
  firstName: string

  @ApiProperty()
  lastName: string
}

export class AppUserOrganizationDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string
}

export class AppUserOrganizationalUnit {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string
}

export class AppUserDto {
  @ApiProperty()
  id: string

  @ApiProperty({ required: false })
  uid?: string

  @ApiProperty()
  userName: string

  @ApiProperty()
  email: string

  @ApiProperty()
  verified: boolean

  @ApiProperty({ required: false })
  verifiedTimestamp?: Date

  @ApiProperty({ required: false })
  passwordUpdateTimestamp?: Date

  @ApiProperty()
  disabled: boolean

  @ApiProperty()
  profile: AppUserProfileEntityDto

  @ApiProperty({ required: false })
  organization?: AppUserOrganizationDto

  @ApiProperty({ required: false })
  organizationalUnit?: AppUserOrganizationalUnit

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty({ required: false })
  uid?: string

  @ApiProperty()
  userName: string

  @ApiProperty()
  email: string

  @ApiProperty()
  verified: boolean

  @ApiProperty({ required: false })
  verifiedTimestamp?: Date

  @ApiProperty({ required: false })
  passwordUpdateTimestamp?: Date

  @ApiProperty()
  disabled: boolean

  @ApiProperty()
  profile: AppUserProfileEntityDto

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserCreateDto {}

export class AppUserUpdateDto {}
