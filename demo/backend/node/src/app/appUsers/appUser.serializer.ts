import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppUserEntity,
  AppUserEntityId,
} from "../../database/core/entities/appUser.entity"
import { AppUserSearchParameters } from "../../entities/appUsers/appUser.types"
import {
  AppUserCreateData,
  AppUserUpdateData,
  AppUserSorting,
  AppUserCursor,
  AppUserSheetItem,
} from "../../entities/appUsers/appUser.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appUser")
export class AppUserSerializer extends NestEntitySerializer<
  AppUserEntity,
  AppUserEntityId,
  AppUserCreateData,
  AppUserUpdateData,
  AppUserSearchParameters,
  AppUserSorting,
  AppUserCursor,
  AppUserSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUser", registry)
  }

  protected async loadEntities(
    filters: AppUserSearchParameters
  ): Promise<AppUserEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppUserEntity[]
  ): Promise<AppUserSheetItem[]> {
    return entities
  }

  protected async importItem(item: AppUserSheetItem, context: AppAuthContext) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppUserEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
