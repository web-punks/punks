import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppUserCreateDto,
  AppUserDto,
  AppUserListItemDto,
  AppUserUpdateDto,
} from "./appUser.dto"
import { AppUserEntity } from "../../database/core/entities/appUser.entity"

@WpEntityConverter("appUser")
export class AppUserConverter
  implements
    IEntityConverter<
      AppUserEntity,
      AppUserDto,
      AppUserListItemDto,
      AppUserCreateDto,
      AppUserUpdateDto
    >
{
  toListItemDto(entity: AppUserEntity): AppUserListItemDto {
    return this.toEntityDto(entity)
  }

  toEntityDto(entity: AppUserEntity): AppUserDto {
    const { passwordHash, organizationalUnit, ...props } = entity
    return {
      ...props,
      profile: {
        firstName: entity.profile.firstName,
        lastName: entity.profile.lastName,
      },
      organization: entity.organization
        ? {
            id: entity.organization.id,
            name: entity.organization.name,
            uid: entity.organization.uid,
          }
        : undefined,
      organizationalUnit: entity.organizationalUnit
        ? {
            id: entity.organizationalUnit.id,
            name: entity.organizationalUnit.name,
          }
        : undefined,
    }
  }

  createDtoToEntity(input: AppUserCreateDto): DeepPartial<AppUserEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppUserUpdateDto): DeepPartial<AppUserEntity> {
    return {
      ...input,
    }
  }
}
