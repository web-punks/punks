import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppUserActions } from "./appUser.actions"
import { AppUserDto, AppUserCreateDto, AppUserUpdateDto } from "./appUser.dto"
import {
  AppUserExportRequest,
  AppUserExportResponse,
  AppUserSampleDownloadRequest,
  AppUserSampleDownloadResponse,
  AppUserSearchRequest,
  AppUserSearchResponse,
  AppUserVersionsSearchRequest,
  AppUserVersionsSearchResponse,
} from "./appUser.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appUser")
export class AppUserController {
  constructor(private readonly actions: AppUserActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appUserGet",
  })
  @ApiOkResponse({
    type: AppUserDto,
  })
  async item(@Param("id") id: string): Promise<AppUserDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appUserCreate",
  })
  @ApiOkResponse({
    type: AppUserDto,
  })
  async create(@Body() data: AppUserCreateDto): Promise<AppUserDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppUserDto,
  })
  @ApiOperation({
    operationId: "appUserUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppUserUpdateDto
  ): Promise<AppUserDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appUserDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appUserSearch",
  })
  @ApiOkResponse({
    type: AppUserSearchResponse,
  })
  async search(
    @Body() request: AppUserSearchRequest
  ): Promise<AppUserSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appUserImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appUserExport",
  })
  @ApiOkResponse({
    type: AppUserExportResponse,
  })
  async export(
    @Body() request: AppUserExportRequest
  ): Promise<AppUserExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appUserSampleDownload",
  })
  @ApiOkResponse({
    type: AppUserSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppUserSampleDownloadRequest
  ): Promise<AppUserSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appUserVersions",
  })
  @ApiOkResponse({
    type: AppUserVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppUserVersionsSearchRequest
  ): Promise<AppUserVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
