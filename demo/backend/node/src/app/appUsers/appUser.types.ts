import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppUserSearchParameters,
  AppUserSearchResults,
  AppUserVersionsSearchParameters,
  AppUserVersionsSearchResults,
} from "../../entities/appUsers/appUser.types"
import { AppUserListItemDto } from "./appUser.dto"

export class AppUserSearchRequest {
  @ApiProperty()
  params: AppUserSearchParameters
}

export class AppUserSearchResponse extends AppUserSearchResults<AppUserListItemDto> {
  @ApiProperty({ type: [AppUserListItemDto] })
  items: AppUserListItemDto[]
}

export class AppUserEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppUserExportRequest {
  @ApiProperty()
  options: AppUserEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppUserSearchParameters
}

export class AppUserExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppUserSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppUserSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppUserVersionsSearchRequest {
  @ApiProperty()
  params: AppUserVersionsSearchParameters
}

export class AppUserVersionsSearchResponse extends AppUserVersionsSearchResults<AppUserListItemDto> {
  @ApiProperty({ type: [AppUserListItemDto] })
  items: AppUserListItemDto[]
}
