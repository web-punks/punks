import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppUserActions } from "./appUser.actions"
import { AppUserController } from "./appUser.controller"
import { AppUserConverter } from "./appUser.converter"
import { AppUserEntityModule } from "../../entities/appUsers/appUser.module"
import { AppUserSerializer } from "./appUser.serializer"

@Module({
  imports: [SharedModule, AppUserEntityModule],
  providers: [AppUserActions, AppUserConverter, AppUserSerializer],
  controllers: [AppUserController],
})
export class AppUserAppModule {}
