import { AppLanguageEntity } from "@/database/core/entities/appLanguage.entity"
import { AppLanguageReferenceDto } from "./appLanguage.dto"

export const toAppLanguageReferenceDto = (
  entity: AppLanguageEntity
): AppLanguageReferenceDto => ({
  id: entity.id,
  name: entity.name,
  uid: entity.uid,
})
