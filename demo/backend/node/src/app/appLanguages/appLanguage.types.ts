import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppLanguageSearchParameters,
  AppLanguageSearchResults,
  AppLanguageVersionsSearchParameters,
  AppLanguageVersionsSearchResults,
} from "../../entities/appLanguages/appLanguage.types"
import { AppLanguageListItemDto } from "./appLanguage.dto"

export class AppLanguageSearchRequest {
  @ApiProperty()
  params: AppLanguageSearchParameters
}

export class AppLanguageSearchResponse extends AppLanguageSearchResults<AppLanguageListItemDto> {
  @ApiProperty({ type: [AppLanguageListItemDto] })
  items: AppLanguageListItemDto[]
}

export class AppLanguageEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppLanguageExportRequest {
  @ApiProperty()
  options: AppLanguageEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppLanguageSearchParameters
}

export class AppLanguageExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppLanguageSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppLanguageSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppLanguageVersionsSearchRequest {
  @ApiProperty()
  params: AppLanguageVersionsSearchParameters
}

export class AppLanguageVersionsSearchResponse extends AppLanguageVersionsSearchResults<AppLanguageListItemDto> {
  @ApiProperty({ type: [AppLanguageListItemDto] })
  items: AppLanguageListItemDto[]
}
