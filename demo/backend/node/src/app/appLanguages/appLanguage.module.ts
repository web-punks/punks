import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppLanguageActions } from "./appLanguage.actions"
import { AppLanguageController } from "./appLanguage.controller"
import { AppLanguageConverter } from "./appLanguage.converter"
import { AppLanguageEntityModule } from "../../entities/appLanguages/appLanguage.module"
import { AppLanguageSerializer } from "./appLanguage.serializer"

@Module({
  imports: [SharedModule, AppLanguageEntityModule],
  providers: [
    AppLanguageActions,
    AppLanguageConverter,
    AppLanguageSerializer,
  ],
  controllers: [AppLanguageController],
})
export class AppLanguageAppModule {}
