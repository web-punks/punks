import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppLanguageCreateDto,
  AppLanguageDto,
  AppLanguageListItemDto,
  AppLanguageUpdateDto,
} from "./appLanguage.dto"
import {
  AppLanguageEntity,
  AppLanguageEntityId,
} from "../../database/core/entities/appLanguage.entity"
import {
  AppLanguageCursor,
  AppLanguageFacets,
  AppLanguageSorting,
} from "../../entities/appLanguages/appLanguage.models"
import {
  AppLanguageDeleteParameters,
  AppLanguageSearchParameters,
} from "../../entities/appLanguages/appLanguage.types"

@WpEntityActions("appLanguage")
export class AppLanguageActions extends NestEntityActions<
  AppLanguageEntity,
  AppLanguageEntityId,
  AppLanguageCreateDto,
  AppLanguageUpdateDto,
  AppLanguageDto,
  AppLanguageListItemDto,
  AppLanguageDeleteParameters,
  AppLanguageSearchParameters,
  AppLanguageSorting,
  AppLanguageCursor,
  AppLanguageFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appLanguage", registry)
  }
}
