import { ApiProperty } from "@nestjs/swagger"
import { AppOrganizationReferenceDto } from "../appOrganizations/appOrganization.dto"

export class AppLanguageReferenceDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string
}

export class AppLanguageDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  default: boolean

  @ApiProperty()
  organization: AppOrganizationReferenceDto

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppLanguageListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  default: boolean

  @ApiProperty()
  organization: AppOrganizationReferenceDto

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppLanguageCreateDto {
  @ApiProperty()
  organizationId: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string
}

export class AppLanguageUpdateDto {
  @ApiProperty()
  organizationId: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string
}
