import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppLanguageCreateDto,
  AppLanguageDto,
  AppLanguageListItemDto,
  AppLanguageUpdateDto,
} from "./appLanguage.dto"
import { toAppOrganizationReferenceDto } from "../appOrganizations/appOrganization.utils"
import { AppLanguageEntity } from "@/database/core/entities/appLanguage.entity"

@WpEntityConverter("appLanguage")
export class AppLanguageConverter
  implements
    IEntityConverter<
      AppLanguageEntity,
      AppLanguageDto,
      AppLanguageListItemDto,
      AppLanguageCreateDto,
      AppLanguageUpdateDto
    >
{
  toListItemDto(entity: AppLanguageEntity): AppLanguageListItemDto {
    return {
      ...entity,
      organization: entity.organization
        ? toAppOrganizationReferenceDto(entity.organization)
        : undefined,
    }
  }

  toEntityDto(entity: AppLanguageEntity): AppLanguageDto {
    return {
      ...entity,
      organization: entity.organization
        ? toAppOrganizationReferenceDto(entity.organization)
        : undefined,
    }
  }

  createDtoToEntity(
    input: AppLanguageCreateDto
  ): DeepPartial<AppLanguageEntity> {
    const { organizationId, ...params } = input
    return {
      ...params,
      organization: organizationId
        ? {
            id: organizationId,
          }
        : undefined,
    }
  }

  updateDtoToEntity(
    input: AppLanguageUpdateDto
  ): DeepPartial<AppLanguageEntity> {
    const { organizationId, ...params } = input
    return {
      ...params,
      organization: organizationId
        ? {
            id: organizationId,
          }
        : undefined,
    }
  }
}
