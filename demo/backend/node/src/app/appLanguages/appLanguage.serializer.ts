import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppLanguageEntity,
  AppLanguageEntityId,
} from "../../database/core/entities/appLanguage.entity"
import { AppLanguageSearchParameters } from "../../entities/appLanguages/appLanguage.types"
import {
  AppLanguageCreateData,
  AppLanguageUpdateData,
  AppLanguageSorting,
  AppLanguageCursor,
  AppLanguageSheetItem,
} from "../../entities/appLanguages/appLanguage.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appLanguage")
export class AppLanguageSerializer extends NestEntitySerializer<
  AppLanguageEntity,
  AppLanguageEntityId,
  AppLanguageCreateData,
  AppLanguageUpdateData,
  AppLanguageSearchParameters,
  AppLanguageSorting,
  AppLanguageCursor,
  AppLanguageSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appLanguage", registry)
  }

  protected async loadEntities(
    filters: AppLanguageSearchParameters
  ): Promise<AppLanguageEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppLanguageEntity[]
  ): Promise<AppLanguageSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppLanguageSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppLanguageEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
