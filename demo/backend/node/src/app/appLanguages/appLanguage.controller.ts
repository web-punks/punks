import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppLanguageActions } from "./appLanguage.actions"
import {
  AppLanguageDto,
  AppLanguageCreateDto,
  AppLanguageUpdateDto,
} from "./appLanguage.dto"
import {
  AppLanguageExportRequest,
  AppLanguageExportResponse,
  AppLanguageSampleDownloadRequest,
  AppLanguageSampleDownloadResponse,
  AppLanguageSearchRequest,
  AppLanguageSearchResponse,
  AppLanguageVersionsSearchRequest,
  AppLanguageVersionsSearchResponse,
} from "./appLanguage.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appLanguage")
export class AppLanguageController {
  constructor(private readonly actions: AppLanguageActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appLanguageGet",
  })
  @ApiOkResponse({
    type: AppLanguageDto,
  })
  async item(@Param("id") id: string): Promise<AppLanguageDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appLanguageCreate",
  })
  @ApiOkResponse({
    type: AppLanguageDto,
  })
  async create(@Body() data: AppLanguageCreateDto): Promise<AppLanguageDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppLanguageDto,
  })
  @ApiOperation({
    operationId: "appLanguageUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppLanguageUpdateDto
  ): Promise<AppLanguageDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appLanguageDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appLanguageSearch",
  })
  @ApiOkResponse({
    type: AppLanguageSearchResponse,
  })
  async search(
    @Body() request: AppLanguageSearchRequest
  ): Promise<AppLanguageSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appLanguageImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appLanguageExport",
  })
  @ApiOkResponse({
    type: AppLanguageExportResponse,
  })
  async export(
    @Body() request: AppLanguageExportRequest
  ): Promise<AppLanguageExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appLanguageSampleDownload",
  })
  @ApiOkResponse({
    type: AppLanguageSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppLanguageSampleDownloadRequest
  ): Promise<AppLanguageSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appLanguageVersions",
  })
  @ApiOkResponse({
    type: AppLanguageVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppLanguageVersionsSearchRequest
  ): Promise<AppLanguageVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
