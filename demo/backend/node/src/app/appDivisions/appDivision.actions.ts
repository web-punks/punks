import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppDivisionCreateDto,
  AppDivisionDto,
  AppDivisionListItemDto,
  AppDivisionUpdateDto,
} from "./appDivision.dto"
import {
  AppDivisionEntity,
  AppDivisionEntityId,
} from "../../database/core/entities/appDivision.entity"
import {
  AppDivisionCursor,
  AppDivisionFacets,
  AppDivisionSorting,
} from "../../entities/appDivisions/appDivision.models"
import {
  AppDivisionDeleteParameters,
  AppDivisionSearchParameters,
} from "../../entities/appDivisions/appDivision.types"

@WpEntityActions("appDivision")
export class AppDivisionActions extends NestEntityActions<
  AppDivisionEntity,
  AppDivisionEntityId,
  AppDivisionCreateDto,
  AppDivisionUpdateDto,
  AppDivisionDto,
  AppDivisionListItemDto,
  AppDivisionDeleteParameters,
  AppDivisionSearchParameters,
  AppDivisionSorting,
  AppDivisionCursor,
  AppDivisionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDivision", registry)
  }
}
