import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppDivisionCreateDto,
  AppDivisionDto,
  AppDivisionListItemDto,
  AppDivisionUpdateDto,
} from "./appDivision.dto"
import { AppDivisionEntity } from "../../database/core/entities/appDivision.entity"

@WpEntityConverter("appDivision")
export class AppDivisionConverter
  implements
    IEntityConverter<
      AppDivisionEntity,
      AppDivisionDto,
      AppDivisionListItemDto,
      AppDivisionCreateDto,
      AppDivisionUpdateDto
    >
{
  toListItemDto(entity: AppDivisionEntity): AppDivisionListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppDivisionEntity): AppDivisionDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppDivisionCreateDto): DeepPartial<AppDivisionEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppDivisionUpdateDto): DeepPartial<AppDivisionEntity> {
    return {
      ...input,
    }
  }
}
