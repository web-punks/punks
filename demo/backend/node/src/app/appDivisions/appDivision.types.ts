import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppDivisionSearchParameters,
  AppDivisionSearchResults,
  AppDivisionVersionsSearchParameters,
  AppDivisionVersionsSearchResults,
} from "../../entities/appDivisions/appDivision.types"
import { AppDivisionListItemDto } from "./appDivision.dto"

export class AppDivisionSearchRequest {
  @ApiProperty()
  params: AppDivisionSearchParameters
}

export class AppDivisionSearchResponse extends AppDivisionSearchResults<AppDivisionListItemDto> {
  @ApiProperty({ type: [AppDivisionListItemDto] })
  items: AppDivisionListItemDto[]
}

export class AppDivisionEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppDivisionExportRequest {
  @ApiProperty()
  options: AppDivisionEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppDivisionSearchParameters
}

export class AppDivisionExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppDivisionSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppDivisionSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppDivisionVersionsSearchRequest {
  @ApiProperty()
  params: AppDivisionVersionsSearchParameters
}

export class AppDivisionVersionsSearchResponse extends AppDivisionVersionsSearchResults<AppDivisionListItemDto> {
  @ApiProperty({ type: [AppDivisionListItemDto] })
  items: AppDivisionListItemDto[]
}
