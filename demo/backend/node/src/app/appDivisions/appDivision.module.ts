import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppDivisionActions } from "./appDivision.actions"
import { AppDivisionController } from "./appDivision.controller"
import { AppDivisionConverter } from "./appDivision.converter"
import { AppDivisionEntityModule } from "../../entities/appDivisions/appDivision.module"
import { AppDivisionSerializer } from "./appDivision.serializer"

@Module({
  imports: [SharedModule, AppDivisionEntityModule],
  providers: [
    AppDivisionActions,
    AppDivisionConverter,
    AppDivisionSerializer,
  ],
  controllers: [AppDivisionController],
})
export class AppDivisionAppModule {}
