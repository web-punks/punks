import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppDivisionEntity,
  AppDivisionEntityId,
} from "../../database/core/entities/appDivision.entity"
import { AppDivisionSearchParameters } from "../../entities/appDivisions/appDivision.types"
import {
  AppDivisionCreateData,
  AppDivisionUpdateData,
  AppDivisionSorting,
  AppDivisionCursor,
  AppDivisionSheetItem,
} from "../../entities/appDivisions/appDivision.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appDivision")
export class AppDivisionSerializer extends NestEntitySerializer<
  AppDivisionEntity,
  AppDivisionEntityId,
  AppDivisionCreateData,
  AppDivisionUpdateData,
  AppDivisionSearchParameters,
  AppDivisionSorting,
  AppDivisionCursor,
  AppDivisionSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDivision", registry)
  }

  protected async loadEntities(
    filters: AppDivisionSearchParameters
  ): Promise<AppDivisionEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppDivisionEntity[]
  ): Promise<AppDivisionSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppDivisionSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppDivisionEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
