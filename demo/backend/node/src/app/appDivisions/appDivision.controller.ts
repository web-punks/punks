import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppDivisionActions } from "./appDivision.actions"
import {
  AppDivisionDto,
  AppDivisionCreateDto,
  AppDivisionUpdateDto,
} from "./appDivision.dto"
import {
  AppDivisionExportRequest,
  AppDivisionExportResponse,
  AppDivisionSampleDownloadRequest,
  AppDivisionSampleDownloadResponse,
  AppDivisionSearchRequest,
  AppDivisionSearchResponse,
  AppDivisionVersionsSearchRequest,
  AppDivisionVersionsSearchResponse,
} from "./appDivision.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appDivision")
export class AppDivisionController {
  constructor(private readonly actions: AppDivisionActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appDivisionGet",
  })
  @ApiOkResponse({
    type: AppDivisionDto,
  })
  async item(@Param("id") id: string): Promise<AppDivisionDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appDivisionCreate",
  })
  @ApiOkResponse({
    type: AppDivisionDto,
  })
  async create(@Body() data: AppDivisionCreateDto): Promise<AppDivisionDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppDivisionDto,
  })
  @ApiOperation({
    operationId: "appDivisionUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppDivisionUpdateDto
  ): Promise<AppDivisionDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appDivisionDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appDivisionSearch",
  })
  @ApiOkResponse({
    type: AppDivisionSearchResponse,
  })
  async search(
    @Body() request: AppDivisionSearchRequest
  ): Promise<AppDivisionSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appDivisionImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appDivisionExport",
  })
  @ApiOkResponse({
    type: AppDivisionExportResponse,
  })
  async export(
    @Body() request: AppDivisionExportRequest
  ): Promise<AppDivisionExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appDivisionSampleDownload",
  })
  @ApiOkResponse({
    type: AppDivisionSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppDivisionSampleDownloadRequest
  ): Promise<AppDivisionSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appDivisionVersions",
  })
  @ApiOkResponse({
    type: AppDivisionVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppDivisionVersionsSearchRequest
  ): Promise<AppDivisionVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
