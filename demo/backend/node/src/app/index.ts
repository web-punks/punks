import { AppAuthAppModule } from "./appAuth/appAuth.module"
import { AppAdminAppModule } from "./appAdmin/appAdmin.module"
import { AppCompanyAppModule } from "./appCompanies/appCompany.module"
import { AppDivisionAppModule } from "./appDivisions/appDivision.module"
import { AppEmailLogAppModule } from "./appEmailLogs/appEmailLog.module"
import { AppEventLogAppModule } from "./appEventLogs/appEventLog.module"
import { AppEntityVersionAppModule } from "./appEntityVersions/appEntityVersion.module"
import { AppFileAppModule } from "./appFiles/appFile.module"
import { AppFileReferenceAppModule } from "./appFileReferences/appFileReference.module"
import { AppLanguageAppModule } from "./appLanguages/appLanguage.module"
import { AppOrganizationAppModule } from "./appOrganizations/appOrganization.module"
import { AppRoleAppModule } from "./appRoles/appRole.module"
import { AppTenantAppModule } from "./appTenants/appTenant.module"
import { AppUserGroupMemberAppModule } from "./appUserGroupMembers/appUserGroupMember.module"
import { AppUserGroupAppModule } from "./appUserGroups/appUserGroup.module"
import { AppUserProfileAppModule } from "./appUserProfiles/appUserProfile.module"
import { AppUserRoleAppModule } from "./appUserRoles/appUserRole.module"
import { AppUserAppModule } from "./appUsers/appUser.module"
import { AppDirectoryAppModule } from "./appDirectories/appDirectory.module"
import { AppOrganizationalUnitAppModule } from "./appOrganizationalUnits/appOrganizationalUnit.module"
import { AppOrganizationalUnitTypeAppModule } from "./appOrganizationalUnitTypes/appOrganizationalUnitType.module"
import { AppOrganizationalUnitTypeChildAppModule } from "./appOrganizationalUnitTypeChildren/appOrganizationalUnitTypeChild.module"
import { AppPermissionAppModule } from "./appPermissions/appPermission.module"
import { AppCacheAppModule } from "./appCache/appCache.module"
import { CrmContactAppModule } from "./crmContacts/crmContact.module"
import { FooAppModule } from "./foos/foo.module"
import { AppOperationLockAppModule } from "./appOperationLocks/appOperationLock.module"

export const AppModules = [
  AppAuthAppModule,
  AppAdminAppModule,
  AppCacheAppModule,
  AppCompanyAppModule,
  AppDirectoryAppModule,
  AppDivisionAppModule,
  AppEmailLogAppModule,
  AppEventLogAppModule,
  AppEntityVersionAppModule,
  AppFileAppModule,
  AppFileReferenceAppModule,
  AppLanguageAppModule,
  AppOperationLockAppModule,
  AppOrganizationAppModule,
  AppOrganizationalUnitAppModule,
  AppOrganizationalUnitTypeAppModule,
  AppOrganizationalUnitTypeChildAppModule,
  AppPermissionAppModule,
  AppRoleAppModule,
  AppTenantAppModule,
  AppUserGroupMemberAppModule,
  AppUserGroupAppModule,
  AppUserProfileAppModule,
  AppUserRoleAppModule,
  AppUserAppModule,
  CrmContactAppModule,
  FooAppModule,
]
