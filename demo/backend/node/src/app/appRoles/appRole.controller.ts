import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppRoleActions } from "./appRole.actions"
import { AppRoleDto, AppRoleCreateDto, AppRoleUpdateDto } from "./appRole.dto"
import {
  AppRoleExportRequest,
  AppRoleExportResponse,
  AppRoleSampleDownloadRequest,
  AppRoleSampleDownloadResponse,
  AppRoleSearchRequest,
  AppRoleSearchResponse,
  AppRoleVersionsSearchRequest,
  AppRoleVersionsSearchResponse,
} from "./appRole.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appRole")
export class AppRoleController {
  constructor(private readonly actions: AppRoleActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appRoleGet",
  })
  @ApiOkResponse({
    type: AppRoleDto,
  })
  async item(@Param("id") id: string): Promise<AppRoleDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appRoleCreate",
  })
  @ApiOkResponse({
    type: AppRoleDto,
  })
  async create(@Body() data: AppRoleCreateDto): Promise<AppRoleDto> {
    return await this.actions.create(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppRoleDto,
  })
  @ApiOperation({
    operationId: "appRoleUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppRoleUpdateDto
  ): Promise<AppRoleDto> {
    return await this.actions.update(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appRoleDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appRoleSearch",
  })
  @ApiOkResponse({
    type: AppRoleSearchResponse,
  })
  async search(
    @Body() request: AppRoleSearchRequest
  ): Promise<AppRoleSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appRoleImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appRoleExport",
  })
  @ApiOkResponse({
    type: AppRoleExportResponse,
  })
  async export(
    @Body() request: AppRoleExportRequest
  ): Promise<AppRoleExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appRoleSampleDownload",
  })
  @ApiOkResponse({
    type: AppRoleSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppRoleSampleDownloadRequest
  ): Promise<AppRoleSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appRoleVersions",
  })
  @ApiOkResponse({
    type: AppRoleVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppRoleVersionsSearchRequest
  ): Promise<AppRoleVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
