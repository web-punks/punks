import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppRoleCreateDto,
  AppRoleDto,
  AppRoleListItemDto,
  AppRoleUpdateDto,
} from "./appRole.dto"
import { AppRoleEntity } from "../../database/core/entities/appRole.entity"

@WpEntityConverter("appRole")
export class AppRoleConverter
  implements
    IEntityConverter<
      AppRoleEntity,
      AppRoleDto,
      AppRoleListItemDto,
      AppRoleCreateDto,
      AppRoleUpdateDto
    >
{
  toListItemDto(entity: AppRoleEntity): AppRoleListItemDto {
    return {
      id: entity.id,
      name: entity.name,
      enabled: entity.enabled,
      uid: entity.uid,
      allowAsRoot: entity.allowAsRoot,
      organizationalUnitTypes: entity.allowedOrganizationalUnitTypes.map(
        (item) => ({
          id: item.type.id,
          name: item.type.name,
          uid: item.type.uid,
        })
      ),
      assignableRoles: entity.assignableRoles.map((item) => ({
        id: item.assignableRole.id,
        name: item.assignableRole.name,
        uid: item.assignableRole.uid,
      })),
      permissions: entity.permissions.map((item) => ({
        id: item.permission.id,
        name: item.permission.name,
        uid: item.permission.uid,
      })),
      createdOn: entity.createdOn,
      updatedOn: entity.updatedOn,
    }
  }

  toEntityDto(entity: AppRoleEntity): AppRoleDto {
    return this.toListItemDto(entity)
  }

  createDtoToEntity(input: AppRoleCreateDto): DeepPartial<AppRoleEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppRoleUpdateDto): DeepPartial<AppRoleEntity> {
    return {
      ...input,
    }
  }
}
