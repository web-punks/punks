import { AppRoleUpdateDto } from "../../appRole.dto"

export type AppRoleUpdateInput = {
  id: string
  data: AppRoleUpdateDto
}
