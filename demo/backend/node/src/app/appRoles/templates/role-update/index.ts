import { difference } from "fp-ts/Array"
import * as S from "fp-ts/string"
import {
  EntityReference,
  IPipelineTemplateBuilder,
  NestPipelineTemplate,
  PipelineDefinition,
  WpPipeline,
} from "@punks/backend-entity-manager"
import { AppAuthContext } from "@/infrastructure/authentication"
import { AppRoleUpdateInput } from "./models"
import {
  AppRoleEntity,
  AppRoleEntityId,
} from "@/database/core/entities/appRole.entity"
import { AppRoleEntityManager } from "@/entities/appRoles/appRole.manager"
import { AppRoleOrganizationalUnitTypeEntityManager } from "@/entities/appRoleOrganizationalUnitTypes/appRoleOrganizationalUnitType.manager"
import { AppRoleAssignableRoleEntityManager } from "@/entities/appRoleAssignableRoles/appRoleAssignableRole.manager"
import { AppRolePermissionEntityManager } from "@/entities/appRolePermissions/appRolePermission.manager"

@WpPipeline("roleUpdate")
export class AppRoleUpdateTemplate extends NestPipelineTemplate<
  AppRoleUpdateInput,
  AppRoleEntity,
  AppAuthContext
> {
  constructor(
    private readonly roles: AppRoleEntityManager,
    private readonly rolesEntityManager: AppRoleOrganizationalUnitTypeEntityManager,
    private readonly assignableRolesManager: AppRoleAssignableRoleEntityManager,
    private readonly rolePermissionsManager: AppRolePermissionEntityManager
  ) {
    super()
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<AppRoleUpdateInput, AppAuthContext>
  ): PipelineDefinition<AppRoleUpdateInput, AppRoleEntity, AppAuthContext> {
    return builder
      .addStep<EntityReference<AppRoleEntityId>>((step) => {
        step.addOperation({
          name: "Role update",
          action: async (input) => {
            this.logger.info("Role update", {
              data: input.data,
            })
            return await this.roles.manager.update.execute(input.id, {
              allowAsRoot: input.data.allowAsRoot,
              name: input.data.name,
              enabled: input.data.enabled,
            })
          },
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Role OU type update",
          action: async (input, state) => {
            const currentOuTypes =
              await this.rolesEntityManager.manager.search.execute({
                filters: {
                  roleIds: [state.pipelineInput.id],
                },
                relations: {
                  type: true,
                },
              })

            const currentOuTypeIds = currentOuTypes.items.map((x) => x.type.id)
            const newOuTypeIds =
              state.pipelineInput.data.organizationalUnityTypes.map((x) => x.id)

            const missingOuTypesIds = difference(S.Eq)(
              newOuTypeIds,
              currentOuTypeIds
            )

            for (const missingOuTypeId of missingOuTypesIds) {
              this.logger.info("Role OU type update add", {
                contactId: input.id,
                missingChildId: missingOuTypeId,
              })
              await this.rolesEntityManager.manager.create.execute({
                role: {
                  id: input.id,
                },
                type: {
                  id: missingOuTypeId,
                },
              })
            }

            const exceedingOuTypeIds = difference(S.Eq)(
              currentOuTypeIds,
              newOuTypeIds
            )
            for (const exceedingOuTypeId of exceedingOuTypeIds) {
              this.logger.info("Role OU type children update remove", {
                contactId: input.id,
                exceedingChildrenId: exceedingOuTypeId,
              })
              await this.rolesEntityManager.manager.delete.execute(
                currentOuTypes.items.find(
                  (x) => x.type.id === exceedingOuTypeId
                ).id
              )
            }
          },
          skipIf: (_, state) =>
            !state.pipelineInput.data.organizationalUnityTypes,
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Assignable role update",
          action: async (input, state) => {
            const currentAssignableRoles =
              await this.assignableRolesManager.manager.search.execute({
                filters: {
                  userRoleIds: [state.pipelineInput.id],
                },
                relations: {
                  assignableRole: true,
                },
              })

            const currentAssignableRoleIds = currentAssignableRoles.items.map(
              (x) => x.assignableRole.id
            )
            const newAssignableRoleIds =
              state.pipelineInput.data.assignableRoles.map((x) => x.id)

            const missingRoleIds = difference(S.Eq)(
              newAssignableRoleIds,
              currentAssignableRoleIds
            )

            const id = this.utils.getStepOutput<
              EntityReference<AppRoleEntityId>
            >(state, 0).id

            for (const missingAssignableRoleId of missingRoleIds) {
              this.logger.info("Role ids add", {
                id,
                missingAssignableRoleId,
              })
              await this.assignableRolesManager.manager.create.execute({
                userRole: {
                  id,
                },
                assignableRole: {
                  id: missingAssignableRoleId,
                },
              })
            }

            const exceedingRoleIds = difference(S.Eq)(
              currentAssignableRoleIds,
              newAssignableRoleIds
            )
            for (const exceedingAssignableRoleId of exceedingRoleIds) {
              this.logger.info("Role id update remove", {
                id,
                exceedingAssignableRoleId,
              })
              await this.assignableRolesManager.manager.delete.execute(
                currentAssignableRoles.items.find(
                  (x) => x.assignableRole.id === exceedingAssignableRoleId
                ).id
              )
            }
          },
          skipIf: (_, state) => !state.pipelineInput.data.assignableRoles,
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Permissions update",
          action: async (_, state) => {
            const currentPermissions =
              await this.rolePermissionsManager.manager.search.execute({
                filters: {
                  roleId: {
                    eq: state.pipelineInput.id,
                  },
                },
                relations: {
                  permission: true,
                },
              })

            const currentPermissionIds = currentPermissions.items.map(
              (x) => x.permission.id
            )
            const newPermissionIds = state.pipelineInput.data.permissions.map(
              (x) => x.id
            )

            const missingPermissionIds = difference(S.Eq)(
              newPermissionIds,
              currentPermissionIds
            )

            const id = this.utils.getStepOutput<
              EntityReference<AppRoleEntityId>
            >(state, 0).id

            for (const missingPermissionId of missingPermissionIds) {
              this.logger.info("Permission ids add", {
                id,
                missingPermissionId,
              })
              await this.rolePermissionsManager.manager.create.execute({
                role: {
                  id,
                },
                permission: {
                  id: missingPermissionId,
                },
              })
            }

            const exceedingPermissionIds = difference(S.Eq)(
              currentPermissionIds,
              newPermissionIds
            )
            for (const exceedingPermissionId of exceedingPermissionIds) {
              this.logger.info("Permission id update remove", {
                id,
                exceedingAssignableRoleId: exceedingPermissionId,
              })
              await this.rolePermissionsManager.manager.delete.execute(
                currentPermissions.items.find(
                  (x) => x.permission.id === exceedingPermissionId
                ).id
              )
            }
          },
          skipIf: (_, state) => !state.pipelineInput.data.permissions,
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Map output",
          action: async (input, state) => {
            return await this.roles.manager.get.execute(
              this.utils.getStepOutput<EntityReference<AppRoleEntityId>>(
                state,
                0
              ).id
            )
          },
        })
      })
      .complete()
  }
}
