import { AppRoleCreateTemplate } from "./role-create"
import { AppRoleUpdateTemplate } from "./role-update"

export const AppRolesTemplates = [AppRoleCreateTemplate, AppRoleUpdateTemplate]
