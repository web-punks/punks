import { AppRoleCreateDto } from "../../appRole.dto"

export type AppRoleCreateInput = {
  data: AppRoleCreateDto
}
