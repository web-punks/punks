import {
  EntityReference,
  IPipelineTemplateBuilder,
  NestPipelineTemplate,
  PipelineDefinition,
  WpPipeline,
} from "@punks/backend-entity-manager"
import { AppAuthContext } from "@/infrastructure/authentication"
import { AppRoleCreateInput } from "./models"
import {
  AppRoleEntity,
  AppRoleEntityId,
} from "@/database/core/entities/appRole.entity"
import { AppRoleEntityManager } from "@/entities/appRoles/appRole.manager"
import { AppRoleOrganizationalUnitTypeEntityManager } from "@/entities/appRoleOrganizationalUnitTypes/appRoleOrganizationalUnitType.manager"
import { AppRoleAssignableRoleEntityManager } from "@/entities/appRoleAssignableRoles/appRoleAssignableRole.manager"
import { AppRolePermissionEntityManager } from "@/entities/appRolePermissions/appRolePermission.manager"

@WpPipeline("roleCreate")
export class AppRoleCreateTemplate extends NestPipelineTemplate<
  AppRoleCreateInput,
  AppRoleEntity,
  AppAuthContext
> {
  constructor(
    private readonly roles: AppRoleEntityManager,
    private readonly rolesEntityManager: AppRoleOrganizationalUnitTypeEntityManager,
    private readonly assignableRolesManager: AppRoleAssignableRoleEntityManager,
    private readonly rolePermissionsManager: AppRolePermissionEntityManager
  ) {
    super()
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<AppRoleCreateInput, AppAuthContext>
  ): PipelineDefinition<AppRoleCreateInput, AppRoleEntity, AppAuthContext> {
    return builder
      .addStep<EntityReference<AppRoleEntityId>>((step) => {
        step.addOperation({
          name: "Role create",
          action: async (input) => {
            this.logger.info("Role create", {
              data: input.data,
            })
            return await this.roles.manager.create.execute({
              allowAsRoot: input.data.allowAsRoot,
              name: input.data.name,
              uid: input.data.uid,
              enabled: input.data.enabled,
            })
          },
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Role ou types insert",
          action: async (input, state) => {
            for (const ouType of state.pipelineInput.data
              .organizationalUnityTypes ?? []) {
              this.logger.info("Role ou types add", {
                id: input.id,
                type: ouType,
              })
              await this.rolesEntityManager.manager.create.execute({
                role: {
                  id: input.id,
                },
                type: {
                  id: ouType.id,
                },
              })
            }
          },
          skipIf: (_, state) =>
            !state.pipelineInput.data.organizationalUnityTypes,
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Assignable roles insert",
          action: async (input, state) => {
            const id = this.utils.getStepOutput<
              EntityReference<AppRoleEntityId>
            >(state, 0).id
            for (const role of state.pipelineInput.data.assignableRoles ?? []) {
              this.logger.info("Assignable role add", {
                id,
                role,
              })
              await this.assignableRolesManager.manager.create.execute({
                userRole: {
                  id,
                },
                assignableRole: {
                  id: role.id,
                },
              })
            }
          },
          skipIf: (_, state) => !state.pipelineInput.data.assignableRoles,
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Permissions insert",
          action: async (input, state) => {
            const id = this.utils.getStepOutput<
              EntityReference<AppRoleEntityId>
            >(state, 0).id
            for (const permission of state.pipelineInput.data.permissions ??
              []) {
              this.logger.info("Permission add", {
                id,
                permission,
              })
              await this.rolePermissionsManager.manager.create.execute({
                role: {
                  id,
                },
                permission: {
                  id: permission.id,
                },
              })
            }
          },
          skipIf: (_, state) => !state.pipelineInput.data.permissions,
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Map output",
          action: async (input, state) => {
            return await this.roles.manager.get.execute(
              this.utils.getStepOutput<EntityReference<AppRoleEntityId>>(
                state,
                0
              ).id
            )
          },
        })
      })
      .complete()
  }
}
