import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppRoleCreateDto,
  AppRoleDto,
  AppRoleListItemDto,
  AppRoleUpdateDto,
} from "./appRole.dto"
import {
  AppRoleEntity,
  AppRoleEntityId,
} from "../../database/core/entities/appRole.entity"
import {
  AppRoleCursor,
  AppRoleFacets,
  AppRoleSorting,
} from "../../entities/appRoles/appRole.models"
import {
  AppRoleDeleteParameters,
  AppRoleSearchParameters,
} from "../../entities/appRoles/appRole.types"
import { AppRoleCreateTemplate } from "./templates/role-create"
import { AppRoleUpdateTemplate } from "./templates/role-update"

@WpEntityActions("appRole")
export class AppRoleActions extends NestEntityActions<
  AppRoleEntity,
  AppRoleEntityId,
  AppRoleCreateDto,
  AppRoleUpdateDto,
  AppRoleDto,
  AppRoleListItemDto,
  AppRoleDeleteParameters,
  AppRoleSearchParameters,
  AppRoleSorting,
  AppRoleCursor,
  AppRoleFacets
> {
  constructor(
    registry: EntityManagerRegistry,
    private readonly createPipeline: AppRoleCreateTemplate,
    private readonly updatePipeline: AppRoleUpdateTemplate
  ) {
    super("appRole", registry)
  }

  async create(data: AppRoleCreateDto): Promise<AppRoleDto> {
    const entity = await this.createPipeline.invoke({
      data,
    })
    return this.converter.toEntityDto(entity)
  }

  async update(
    id: AppRoleEntityId,
    data: AppRoleUpdateDto
  ): Promise<AppRoleDto> {
    const entity = await this.updatePipeline.invoke({
      id,
      data,
    })
    return this.converter.toEntityDto(entity)
  }
}
