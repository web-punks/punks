import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppRoleActions } from "./appRole.actions"
import { AppRoleController } from "./appRole.controller"
import { AppRoleConverter } from "./appRole.converter"
import { AppRoleEntityModule } from "../../entities/appRoles/appRole.module"
import { AppRoleSerializer } from "./appRole.serializer"
import { AppRolesTemplates } from "./templates"

@Module({
  imports: [SharedModule, AppRoleEntityModule],
  providers: [
    AppRoleActions,
    AppRoleConverter,
    AppRoleSerializer,
    ...AppRolesTemplates,
  ],
  controllers: [AppRoleController],
})
export class AppRoleAppModule {}
