import { ApiProperty } from "@nestjs/swagger"

export class AppRoleOrganizationalUnitType {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string
}

export class AppRoleAssignableRole {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string
}

export class AppRolePermission {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string
}

export class AppRoleDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string

  @ApiProperty()
  enabled: boolean

  @ApiProperty()
  allowAsRoot: boolean

  @ApiProperty({ type: [AppRoleOrganizationalUnitType] })
  organizationalUnitTypes: AppRoleOrganizationalUnitType[]

  @ApiProperty({ type: [AppRoleAssignableRole] })
  assignableRoles: AppRoleAssignableRole[]

  @ApiProperty({ type: [AppRolePermission] })
  permissions: AppRolePermission[]

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppRoleListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string

  @ApiProperty()
  enabled: boolean

  @ApiProperty()
  allowAsRoot: boolean

  @ApiProperty({ type: [AppRoleOrganizationalUnitType] })
  organizationalUnitTypes: AppRoleOrganizationalUnitType[]

  @ApiProperty({ type: [AppRoleAssignableRole] })
  assignableRoles: AppRoleAssignableRole[]

  @ApiProperty({ type: [AppRolePermission] })
  permissions: AppRolePermission[]

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppRoleAllowedOrganizationalUnitTypeInput {
  @ApiProperty()
  id: string
}

export class AppRoleAssignableRoleInput {
  @ApiProperty()
  id: string
}

export class AppRolePermissionInput {
  @ApiProperty()
  id: string
}

export class AppRoleCreateDto {
  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string

  @ApiProperty()
  enabled: boolean

  @ApiProperty()
  allowAsRoot: boolean

  @ApiProperty({
    type: [AppRoleAllowedOrganizationalUnitTypeInput],
    required: false,
  })
  organizationalUnityTypes?: AppRoleAllowedOrganizationalUnitTypeInput[]

  @ApiProperty({
    type: [AppRoleAssignableRoleInput],
    required: false,
  })
  assignableRoles?: AppRoleAssignableRoleInput[]

  @ApiProperty({
    type: [AppRolePermissionInput],
    required: false,
  })
  permissions?: AppRolePermissionInput[]
}

export class AppRoleUpdateDto {
  @ApiProperty()
  name: string

  @ApiProperty()
  enabled: boolean

  @ApiProperty()
  allowAsRoot: boolean

  @ApiProperty({
    type: [AppRoleAllowedOrganizationalUnitTypeInput],
    required: false,
  })
  organizationalUnityTypes?: AppRoleAllowedOrganizationalUnitTypeInput[]

  @ApiProperty({
    type: [AppRoleAssignableRoleInput],
    required: false,
  })
  assignableRoles?: AppRoleAssignableRoleInput[]

  @ApiProperty({
    type: [AppRolePermissionInput],
    required: false,
  })
  permissions?: AppRolePermissionInput[]
}
