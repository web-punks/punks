import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppRoleEntity,
  AppRoleEntityId,
} from "../../database/core/entities/appRole.entity"
import { AppRoleSearchParameters } from "../../entities/appRoles/appRole.types"
import {
  AppRoleCreateData,
  AppRoleUpdateData,
  AppRoleSorting,
  AppRoleCursor,
  AppRoleSheetItem,
} from "../../entities/appRoles/appRole.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appRole")
export class AppRoleSerializer extends NestEntitySerializer<
  AppRoleEntity,
  AppRoleEntityId,
  AppRoleCreateData,
  AppRoleUpdateData,
  AppRoleSearchParameters,
  AppRoleSorting,
  AppRoleCursor,
  AppRoleSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRole", registry)
  }

  protected async loadEntities(
    filters: AppRoleSearchParameters
  ): Promise<AppRoleEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppRoleEntity[]
  ): Promise<AppRoleSheetItem[]> {
    return entities
  }

  protected async importItem(item: AppRoleSheetItem, context: AppAuthContext) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppRoleEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
