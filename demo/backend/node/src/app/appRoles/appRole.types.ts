import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppRoleSearchParameters,
  AppRoleSearchResults,
  AppRoleVersionsSearchParameters,
  AppRoleVersionsSearchResults,
} from "../../entities/appRoles/appRole.types"
import { AppRoleListItemDto } from "./appRole.dto"

export class AppRoleSearchRequest {
  @ApiProperty()
  params: AppRoleSearchParameters
}

export class AppRoleSearchResponse extends AppRoleSearchResults<AppRoleListItemDto> {
  @ApiProperty({ type: [AppRoleListItemDto] })
  items: AppRoleListItemDto[]
}

export class AppRoleEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppRoleExportRequest {
  @ApiProperty()
  options: AppRoleEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppRoleSearchParameters
}

export class AppRoleExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppRoleSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppRoleSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppRoleVersionsSearchRequest {
  @ApiProperty()
  params: AppRoleVersionsSearchParameters
}

export class AppRoleVersionsSearchResponse extends AppRoleVersionsSearchResults<AppRoleListItemDto> {
  @ApiProperty({ type: [AppRoleListItemDto] })
  items: AppRoleListItemDto[]
}
