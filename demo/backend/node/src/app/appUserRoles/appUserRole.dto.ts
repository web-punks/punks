import { ApiProperty } from "@nestjs/swagger"

export class AppUserRoleDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserRoleListItemDto {
  @ApiProperty()
  id: string
}

export class AppUserRoleCreateDto {}

export class AppUserRoleUpdateDto {}
