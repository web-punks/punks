import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppUserRoleCreateDto,
  AppUserRoleDto,
  AppUserRoleListItemDto,
  AppUserRoleUpdateDto,
} from "./appUserRole.dto"
import { AppUserRoleEntity } from "../../database/core/entities/appUserRole.entity"

@WpEntityConverter("appUserRole")
export class AppUserRoleConverter
  implements
    IEntityConverter<
      AppUserRoleEntity,
      AppUserRoleDto,
      AppUserRoleListItemDto,
      AppUserRoleCreateDto,
      AppUserRoleUpdateDto
    >
{
  toListItemDto(entity: AppUserRoleEntity): AppUserRoleListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppUserRoleEntity): AppUserRoleDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppUserRoleCreateDto): DeepPartial<AppUserRoleEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppUserRoleUpdateDto): DeepPartial<AppUserRoleEntity> {
    return {
      ...input,
    }
  }
}
