import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppUserRoleSearchParameters,
  AppUserRoleSearchResults,
  AppUserRoleVersionsSearchParameters,
  AppUserRoleVersionsSearchResults,
} from "../../entities/appUserRoles/appUserRole.types"
import { AppUserRoleListItemDto } from "./appUserRole.dto"

export class AppUserRoleSearchRequest {
  @ApiProperty()
  params: AppUserRoleSearchParameters
}

export class AppUserRoleSearchResponse extends AppUserRoleSearchResults<AppUserRoleListItemDto> {
  @ApiProperty({ type: [AppUserRoleListItemDto] })
  items: AppUserRoleListItemDto[]
}

export class AppUserRoleEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppUserRoleExportRequest {
  @ApiProperty()
  options: AppUserRoleEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppUserRoleSearchParameters
}

export class AppUserRoleExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppUserRoleSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppUserRoleSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppUserRoleVersionsSearchRequest {
  @ApiProperty()
  params: AppUserRoleVersionsSearchParameters
}

export class AppUserRoleVersionsSearchResponse extends AppUserRoleVersionsSearchResults<AppUserRoleListItemDto> {
  @ApiProperty({ type: [AppUserRoleListItemDto] })
  items: AppUserRoleListItemDto[]
}
