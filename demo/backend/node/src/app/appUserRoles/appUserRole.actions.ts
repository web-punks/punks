import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppUserRoleCreateDto,
  AppUserRoleDto,
  AppUserRoleListItemDto,
  AppUserRoleUpdateDto,
} from "./appUserRole.dto"
import {
  AppUserRoleEntity,
  AppUserRoleEntityId,
} from "../../database/core/entities/appUserRole.entity"
import {
  AppUserRoleCursor,
  AppUserRoleFacets,
  AppUserRoleSorting,
} from "../../entities/appUserRoles/appUserRole.models"
import {
  AppUserRoleDeleteParameters,
  AppUserRoleSearchParameters,
} from "../../entities/appUserRoles/appUserRole.types"

@WpEntityActions("appUserRole")
export class AppUserRoleActions extends NestEntityActions<
  AppUserRoleEntity,
  AppUserRoleEntityId,
  AppUserRoleCreateDto,
  AppUserRoleUpdateDto,
  AppUserRoleDto,
  AppUserRoleListItemDto,
  AppUserRoleDeleteParameters,
  AppUserRoleSearchParameters,
  AppUserRoleSorting,
  AppUserRoleCursor,
  AppUserRoleFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserRole", registry)
  }
}
