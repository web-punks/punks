import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppUserRoleActions } from "./appUserRole.actions"
import { AppUserRoleController } from "./appUserRole.controller"
import { AppUserRoleConverter } from "./appUserRole.converter"
import { AppUserRoleEntityModule } from "../../entities/appUserRoles/appUserRole.module"
import { AppUserRoleSerializer } from "./appUserRole.serializer"

@Module({
  imports: [SharedModule, AppUserRoleEntityModule],
  providers: [
    AppUserRoleActions,
    AppUserRoleConverter,
    AppUserRoleSerializer,
  ],
  controllers: [AppUserRoleController],
})
export class AppUserRoleAppModule {}
