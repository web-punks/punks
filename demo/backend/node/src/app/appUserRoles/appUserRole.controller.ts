import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppUserRoleActions } from "./appUserRole.actions"
import {
  AppUserRoleDto,
  AppUserRoleCreateDto,
  AppUserRoleUpdateDto,
} from "./appUserRole.dto"
import {
  AppUserRoleExportRequest,
  AppUserRoleExportResponse,
  AppUserRoleSampleDownloadRequest,
  AppUserRoleSampleDownloadResponse,
  AppUserRoleSearchRequest,
  AppUserRoleSearchResponse,
  AppUserRoleVersionsSearchRequest,
  AppUserRoleVersionsSearchResponse,
} from "./appUserRole.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appUserRole")
export class AppUserRoleController {
  constructor(private readonly actions: AppUserRoleActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appUserRoleGet",
  })
  @ApiOkResponse({
    type: AppUserRoleDto,
  })
  async item(@Param("id") id: string): Promise<AppUserRoleDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appUserRoleCreate",
  })
  @ApiOkResponse({
    type: AppUserRoleDto,
  })
  async create(@Body() data: AppUserRoleCreateDto): Promise<AppUserRoleDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppUserRoleDto,
  })
  @ApiOperation({
    operationId: "appUserRoleUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppUserRoleUpdateDto
  ): Promise<AppUserRoleDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appUserRoleDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appUserRoleSearch",
  })
  @ApiOkResponse({
    type: AppUserRoleSearchResponse,
  })
  async search(
    @Body() request: AppUserRoleSearchRequest
  ): Promise<AppUserRoleSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appUserRoleImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appUserRoleExport",
  })
  @ApiOkResponse({
    type: AppUserRoleExportResponse,
  })
  async export(
    @Body() request: AppUserRoleExportRequest
  ): Promise<AppUserRoleExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appUserRoleSampleDownload",
  })
  @ApiOkResponse({
    type: AppUserRoleSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppUserRoleSampleDownloadRequest
  ): Promise<AppUserRoleSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appUserRoleVersions",
  })
  @ApiOkResponse({
    type: AppUserRoleVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppUserRoleVersionsSearchRequest
  ): Promise<AppUserRoleVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
