import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppUserRoleEntity,
  AppUserRoleEntityId,
} from "../../database/core/entities/appUserRole.entity"
import { AppUserRoleSearchParameters } from "../../entities/appUserRoles/appUserRole.types"
import {
  AppUserRoleCreateData,
  AppUserRoleUpdateData,
  AppUserRoleSorting,
  AppUserRoleCursor,
  AppUserRoleSheetItem,
} from "../../entities/appUserRoles/appUserRole.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appUserRole")
export class AppUserRoleSerializer extends NestEntitySerializer<
  AppUserRoleEntity,
  AppUserRoleEntityId,
  AppUserRoleCreateData,
  AppUserRoleUpdateData,
  AppUserRoleSearchParameters,
  AppUserRoleSorting,
  AppUserRoleCursor,
  AppUserRoleSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserRole", registry)
  }

  protected async loadEntities(
    filters: AppUserRoleSearchParameters
  ): Promise<AppUserRoleEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppUserRoleEntity[]
  ): Promise<AppUserRoleSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppUserRoleSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppUserRoleEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
