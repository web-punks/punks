import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppUserProfileActions } from "./appUserProfile.actions"
import {
  AppUserProfileDto,
  AppUserProfileCreateDto,
  AppUserProfileUpdateDto,
} from "./appUserProfile.dto"
import {
  AppUserProfileExportRequest,
  AppUserProfileExportResponse,
  AppUserProfileSampleDownloadRequest,
  AppUserProfileSampleDownloadResponse,
  AppUserProfileSearchRequest,
  AppUserProfileSearchResponse,
  AppUserProfileVersionsSearchRequest,
  AppUserProfileVersionsSearchResponse,
} from "./appUserProfile.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appUserProfile")
export class AppUserProfileController {
  constructor(private readonly actions: AppUserProfileActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appUserProfileGet",
  })
  @ApiOkResponse({
    type: AppUserProfileDto,
  })
  async item(@Param("id") id: string): Promise<AppUserProfileDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appUserProfileCreate",
  })
  @ApiOkResponse({
    type: AppUserProfileDto,
  })
  async create(
    @Body() data: AppUserProfileCreateDto
  ): Promise<AppUserProfileDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppUserProfileDto,
  })
  @ApiOperation({
    operationId: "appUserProfileUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppUserProfileUpdateDto
  ): Promise<AppUserProfileDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appUserProfileDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appUserProfileSearch",
  })
  @ApiOkResponse({
    type: AppUserProfileSearchResponse,
  })
  async search(
    @Body() request: AppUserProfileSearchRequest
  ): Promise<AppUserProfileSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appUserProfileImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appUserProfileExport",
  })
  @ApiOkResponse({
    type: AppUserProfileExportResponse,
  })
  async export(
    @Body() request: AppUserProfileExportRequest
  ): Promise<AppUserProfileExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appUserProfileSampleDownload",
  })
  @ApiOkResponse({
    type: AppUserProfileSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppUserProfileSampleDownloadRequest
  ): Promise<AppUserProfileSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appUserProfileVersions",
  })
  @ApiOkResponse({
    type: AppUserProfileVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppUserProfileVersionsSearchRequest
  ): Promise<AppUserProfileVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
