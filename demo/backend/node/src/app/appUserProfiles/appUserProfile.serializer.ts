import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppUserProfileEntity,
  AppUserProfileEntityId,
} from "../../database/core/entities/appUserProfile.entity"
import { AppUserProfileSearchParameters } from "../../entities/appUserProfiles/appUserProfile.types"
import {
  AppUserProfileCreateData,
  AppUserProfileUpdateData,
  AppUserProfileSorting,
  AppUserProfileCursor,
  AppUserProfileSheetItem,
} from "../../entities/appUserProfiles/appUserProfile.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appUserProfile")
export class AppUserProfileSerializer extends NestEntitySerializer<
  AppUserProfileEntity,
  AppUserProfileEntityId,
  AppUserProfileCreateData,
  AppUserProfileUpdateData,
  AppUserProfileSearchParameters,
  AppUserProfileSorting,
  AppUserProfileCursor,
  AppUserProfileSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserProfile", registry)
  }

  protected async loadEntities(
    filters: AppUserProfileSearchParameters
  ): Promise<AppUserProfileEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppUserProfileEntity[]
  ): Promise<AppUserProfileSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppUserProfileSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppUserProfileEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
