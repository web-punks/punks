import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppUserProfileCreateDto,
  AppUserProfileDto,
  AppUserProfileListItemDto,
  AppUserProfileUpdateDto,
} from "./appUserProfile.dto"
import {
  AppUserProfileEntity,
  AppUserProfileEntityId,
} from "../../database/core/entities/appUserProfile.entity"
import {
  AppUserProfileCursor,
  AppUserProfileFacets,
  AppUserProfileSorting,
} from "../../entities/appUserProfiles/appUserProfile.models"
import {
  AppUserProfileDeleteParameters,
  AppUserProfileSearchParameters,
} from "../../entities/appUserProfiles/appUserProfile.types"

@WpEntityActions("appUserProfile")
export class AppUserProfileActions extends NestEntityActions<
  AppUserProfileEntity,
  AppUserProfileEntityId,
  AppUserProfileCreateDto,
  AppUserProfileUpdateDto,
  AppUserProfileDto,
  AppUserProfileListItemDto,
  AppUserProfileDeleteParameters,
  AppUserProfileSearchParameters,
  AppUserProfileSorting,
  AppUserProfileCursor,
  AppUserProfileFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserProfile", registry)
  }
}
