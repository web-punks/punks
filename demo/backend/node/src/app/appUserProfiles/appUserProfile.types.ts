import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppUserProfileSearchParameters,
  AppUserProfileSearchResults,
  AppUserProfileVersionsSearchParameters,
  AppUserProfileVersionsSearchResults,
} from "../../entities/appUserProfiles/appUserProfile.types"
import { AppUserProfileListItemDto } from "./appUserProfile.dto"

export class AppUserProfileSearchRequest {
  @ApiProperty()
  params: AppUserProfileSearchParameters
}

export class AppUserProfileSearchResponse extends AppUserProfileSearchResults<AppUserProfileListItemDto> {
  @ApiProperty({ type: [AppUserProfileListItemDto] })
  items: AppUserProfileListItemDto[]
}

export class AppUserProfileEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppUserProfileExportRequest {
  @ApiProperty()
  options: AppUserProfileEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppUserProfileSearchParameters
}

export class AppUserProfileExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppUserProfileSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppUserProfileSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppUserProfileVersionsSearchRequest {
  @ApiProperty()
  params: AppUserProfileVersionsSearchParameters
}

export class AppUserProfileVersionsSearchResponse extends AppUserProfileVersionsSearchResults<AppUserProfileListItemDto> {
  @ApiProperty({ type: [AppUserProfileListItemDto] })
  items: AppUserProfileListItemDto[]
}
