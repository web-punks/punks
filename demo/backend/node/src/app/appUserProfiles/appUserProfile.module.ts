import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppUserProfileActions } from "./appUserProfile.actions"
import { AppUserProfileController } from "./appUserProfile.controller"
import { AppUserProfileConverter } from "./appUserProfile.converter"
import { AppUserProfileEntityModule } from "../../entities/appUserProfiles/appUserProfile.module"
import { AppUserProfileSerializer } from "./appUserProfile.serializer"

@Module({
  imports: [SharedModule, AppUserProfileEntityModule],
  providers: [
    AppUserProfileActions,
    AppUserProfileConverter,
    AppUserProfileSerializer,
  ],
  controllers: [AppUserProfileController],
})
export class AppUserProfileAppModule {}
