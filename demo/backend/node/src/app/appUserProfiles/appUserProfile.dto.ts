import { ApiProperty } from "@nestjs/swagger"

export class AppUserProfileDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserProfileListItemDto {
  @ApiProperty()
  id: string
}

export class AppUserProfileCreateDto {}

export class AppUserProfileUpdateDto {}
