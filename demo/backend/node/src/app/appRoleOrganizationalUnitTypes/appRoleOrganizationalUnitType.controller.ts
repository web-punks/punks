import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "@punks/backend-entity-manager"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { AppRoleOrganizationalUnitTypeActions } from "./appRoleOrganizationalUnitType.actions"
import {
  AppRoleOrganizationalUnitTypeDto,
  AppRoleOrganizationalUnitTypeCreateDto,
  AppRoleOrganizationalUnitTypeUpdateDto,
} from "./appRoleOrganizationalUnitType.dto"
import {
  AppRoleOrganizationalUnitTypeExportRequest,
  AppRoleOrganizationalUnitTypeExportResponse,
  AppRoleOrganizationalUnitTypeSampleDownloadRequest,
  AppRoleOrganizationalUnitTypeSampleDownloadResponse,
  AppRoleOrganizationalUnitTypeSearchRequest,
  AppRoleOrganizationalUnitTypeSearchResponse,
  AppRoleOrganizationalUnitTypeVersionsSearchRequest,
  AppRoleOrganizationalUnitTypeVersionsSearchResponse,
} from "./appRoleOrganizationalUnitType.types"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("v1/appRoleOrganizationalUnitType")
export class AppRoleOrganizationalUnitTypeController {
  constructor(private readonly actions: AppRoleOrganizationalUnitTypeActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appRoleOrganizationalUnitTypeGet",
  })
  @ApiOkResponse({
    type: AppRoleOrganizationalUnitTypeDto,
  })
  async item(
    @Param("id") id: string
  ): Promise<AppRoleOrganizationalUnitTypeDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appRoleOrganizationalUnitTypeCreate",
  })
  @ApiOkResponse({
    type: AppRoleOrganizationalUnitTypeDto,
  })
  async create(
    @Body() data: AppRoleOrganizationalUnitTypeCreateDto
  ): Promise<AppRoleOrganizationalUnitTypeDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppRoleOrganizationalUnitTypeDto,
  })
  @ApiOperation({
    operationId: "appRoleOrganizationalUnitTypeUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppRoleOrganizationalUnitTypeUpdateDto
  ): Promise<AppRoleOrganizationalUnitTypeDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appRoleOrganizationalUnitTypeDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appRoleOrganizationalUnitTypeSearch",
  })
  @ApiOkResponse({
    type: AppRoleOrganizationalUnitTypeSearchResponse,
  })
  async search(
    @Body() request: AppRoleOrganizationalUnitTypeSearchRequest
  ): Promise<AppRoleOrganizationalUnitTypeSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appRoleOrganizationalUnitTypeImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appRoleOrganizationalUnitTypeExport",
  })
  @ApiOkResponse({
    type: AppRoleOrganizationalUnitTypeExportResponse,
  })
  async export(
    @Body() request: AppRoleOrganizationalUnitTypeExportRequest
  ): Promise<AppRoleOrganizationalUnitTypeExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appRoleOrganizationalUnitTypeSampleDownload",
  })
  @ApiOkResponse({
    type: AppRoleOrganizationalUnitTypeSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppRoleOrganizationalUnitTypeSampleDownloadRequest
  ): Promise<AppRoleOrganizationalUnitTypeSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appRoleOrganizationalUnitTypeVersions",
  })
  @ApiOkResponse({
    type: AppRoleOrganizationalUnitTypeVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppRoleOrganizationalUnitTypeVersionsSearchRequest
  ): Promise<AppRoleOrganizationalUnitTypeVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
