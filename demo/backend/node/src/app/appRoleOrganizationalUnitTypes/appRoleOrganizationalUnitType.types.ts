import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "@punks/backend-entity-manager"
import {
  AppRoleOrganizationalUnitTypeSearchParameters,
  AppRoleOrganizationalUnitTypeSearchResults,
  AppRoleOrganizationalUnitTypeVersionsSearchParameters,
  AppRoleOrganizationalUnitTypeVersionsSearchResults,
} from "../../entities/appRoleOrganizationalUnitTypes/appRoleOrganizationalUnitType.types"
import { AppRoleOrganizationalUnitTypeListItemDto } from "./appRoleOrganizationalUnitType.dto"

export class AppRoleOrganizationalUnitTypeSearchRequest {
  @ApiProperty()
  params: AppRoleOrganizationalUnitTypeSearchParameters
}

export class AppRoleOrganizationalUnitTypeSearchResponse extends AppRoleOrganizationalUnitTypeSearchResults<AppRoleOrganizationalUnitTypeListItemDto> {
  @ApiProperty({ type: [AppRoleOrganizationalUnitTypeListItemDto] })
  items: AppRoleOrganizationalUnitTypeListItemDto[]
}

export class AppRoleOrganizationalUnitTypeEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppRoleOrganizationalUnitTypeExportRequest {
  @ApiProperty()
  options: AppRoleOrganizationalUnitTypeEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppRoleOrganizationalUnitTypeSearchParameters
}

export class AppRoleOrganizationalUnitTypeExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppRoleOrganizationalUnitTypeSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppRoleOrganizationalUnitTypeSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppRoleOrganizationalUnitTypeVersionsSearchRequest {
  @ApiProperty()
  params: AppRoleOrganizationalUnitTypeVersionsSearchParameters
}

export class AppRoleOrganizationalUnitTypeVersionsSearchResponse extends AppRoleOrganizationalUnitTypeVersionsSearchResults<AppRoleOrganizationalUnitTypeListItemDto> {
  @ApiProperty({ type: [AppRoleOrganizationalUnitTypeListItemDto] })
  items: AppRoleOrganizationalUnitTypeListItemDto[]
}
