import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "@punks/backend-entity-manager"
import {
  AppRoleOrganizationalUnitTypeEntity,
  AppRoleOrganizationalUnitTypeEntityId,
} from "../../database/core/entities/appRoleOrganizationalUnitType.entity"
import { AppRoleOrganizationalUnitTypeSearchParameters } from "../../entities/appRoleOrganizationalUnitTypes/appRoleOrganizationalUnitType.types"
import {
  AppRoleOrganizationalUnitTypeCreateData,
  AppRoleOrganizationalUnitTypeUpdateData,
  AppRoleOrganizationalUnitTypeSorting,
  AppRoleOrganizationalUnitTypeCursor,
  AppRoleOrganizationalUnitTypeSheetItem,
} from "../../entities/appRoleOrganizationalUnitTypes/appRoleOrganizationalUnitType.models"
import { AppAuthContext } from "@/infrastructure/authentication"

@WpEntitySerializer("appRoleOrganizationalUnitType")
export class AppRoleOrganizationalUnitTypeSerializer extends NestEntitySerializer<
  AppRoleOrganizationalUnitTypeEntity,
  AppRoleOrganizationalUnitTypeEntityId,
  AppRoleOrganizationalUnitTypeCreateData,
  AppRoleOrganizationalUnitTypeUpdateData,
  AppRoleOrganizationalUnitTypeSearchParameters,
  AppRoleOrganizationalUnitTypeSorting,
  AppRoleOrganizationalUnitTypeCursor,
  AppRoleOrganizationalUnitTypeSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRoleOrganizationalUnitType", registry)
  }

  protected async loadEntities(
    filters: AppRoleOrganizationalUnitTypeSearchParameters
  ): Promise<AppRoleOrganizationalUnitTypeEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppRoleOrganizationalUnitTypeEntity[]
  ): Promise<AppRoleOrganizationalUnitTypeSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppRoleOrganizationalUnitTypeSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<
    EntitySerializerSheetDefinition<AppRoleOrganizationalUnitTypeEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
