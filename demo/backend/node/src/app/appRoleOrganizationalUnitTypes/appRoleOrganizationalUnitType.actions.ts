import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "@punks/backend-entity-manager"
import {
  AppRoleOrganizationalUnitTypeCreateDto,
  AppRoleOrganizationalUnitTypeDto,
  AppRoleOrganizationalUnitTypeListItemDto,
  AppRoleOrganizationalUnitTypeUpdateDto,
} from "./appRoleOrganizationalUnitType.dto"
import {
  AppRoleOrganizationalUnitTypeEntity,
  AppRoleOrganizationalUnitTypeEntityId,
} from "../../database/core/entities/appRoleOrganizationalUnitType.entity"
import {
  AppRoleOrganizationalUnitTypeCursor,
  AppRoleOrganizationalUnitTypeFacets,
  AppRoleOrganizationalUnitTypeSorting,
} from "../../entities/appRoleOrganizationalUnitTypes/appRoleOrganizationalUnitType.models"
import {
  AppRoleOrganizationalUnitTypeDeleteParameters,
  AppRoleOrganizationalUnitTypeSearchParameters,
} from "../../entities/appRoleOrganizationalUnitTypes/appRoleOrganizationalUnitType.types"

@WpEntityActions("appRoleOrganizationalUnitType")
export class AppRoleOrganizationalUnitTypeActions extends NestEntityActions<
  AppRoleOrganizationalUnitTypeEntity,
  AppRoleOrganizationalUnitTypeEntityId,
  AppRoleOrganizationalUnitTypeCreateDto,
  AppRoleOrganizationalUnitTypeUpdateDto,
  AppRoleOrganizationalUnitTypeDto,
  AppRoleOrganizationalUnitTypeListItemDto,
  AppRoleOrganizationalUnitTypeDeleteParameters,
  AppRoleOrganizationalUnitTypeSearchParameters,
  AppRoleOrganizationalUnitTypeSorting,
  AppRoleOrganizationalUnitTypeCursor,
  AppRoleOrganizationalUnitTypeFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRoleOrganizationalUnitType", registry)
  }
}
