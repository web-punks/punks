import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppRoleOrganizationalUnitTypeActions } from "./appRoleOrganizationalUnitType.actions"
import { AppRoleOrganizationalUnitTypeController } from "./appRoleOrganizationalUnitType.controller"
import { AppRoleOrganizationalUnitTypeConverter } from "./appRoleOrganizationalUnitType.converter"
import { AppRoleOrganizationalUnitTypeEntityModule } from "../../entities/appRoleOrganizationalUnitTypes/appRoleOrganizationalUnitType.module"
import { AppRoleOrganizationalUnitTypeSerializer } from "./appRoleOrganizationalUnitType.serializer"

@Module({
  imports: [SharedModule, AppRoleOrganizationalUnitTypeEntityModule],
  providers: [
    AppRoleOrganizationalUnitTypeActions,
    AppRoleOrganizationalUnitTypeConverter,
    AppRoleOrganizationalUnitTypeSerializer,
  ],
  controllers: [AppRoleOrganizationalUnitTypeController],
})
export class AppRoleOrganizationalUnitTypeAppModule {}
