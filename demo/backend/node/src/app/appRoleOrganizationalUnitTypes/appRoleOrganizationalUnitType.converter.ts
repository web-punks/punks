import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppRoleOrganizationalUnitTypeCreateDto,
  AppRoleOrganizationalUnitTypeDto,
  AppRoleOrganizationalUnitTypeListItemDto,
  AppRoleOrganizationalUnitTypeUpdateDto,
} from "./appRoleOrganizationalUnitType.dto"
import { AppRoleOrganizationalUnitTypeEntity } from "../../database/core/entities/appRoleOrganizationalUnitType.entity"

@WpEntityConverter("appRoleOrganizationalUnitType")
export class AppRoleOrganizationalUnitTypeConverter
  implements
    IEntityConverter<
      AppRoleOrganizationalUnitTypeEntity,
      AppRoleOrganizationalUnitTypeDto,
      AppRoleOrganizationalUnitTypeListItemDto,
      AppRoleOrganizationalUnitTypeCreateDto,
      AppRoleOrganizationalUnitTypeUpdateDto
    >
{
  toListItemDto(entity: AppRoleOrganizationalUnitTypeEntity): AppRoleOrganizationalUnitTypeListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppRoleOrganizationalUnitTypeEntity): AppRoleOrganizationalUnitTypeDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppRoleOrganizationalUnitTypeCreateDto): DeepPartial<AppRoleOrganizationalUnitTypeEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppRoleOrganizationalUnitTypeUpdateDto): DeepPartial<AppRoleOrganizationalUnitTypeEntity> {
    return {
      ...input,
    }
  }
}
