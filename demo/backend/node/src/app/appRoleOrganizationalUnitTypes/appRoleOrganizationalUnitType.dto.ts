import { ApiProperty } from "@nestjs/swagger"

export class AppRoleOrganizationalUnitTypeDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppRoleOrganizationalUnitTypeListItemDto {
  @ApiProperty()
  id: string
}

export class AppRoleOrganizationalUnitTypeCreateDto {}

export class AppRoleOrganizationalUnitTypeUpdateDto {}
