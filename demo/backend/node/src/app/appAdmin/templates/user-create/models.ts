import { ApiProperty } from "@nestjs/swagger"

export class UserCreateRequest {
  @ApiProperty()
  email: string

  @ApiProperty({ required: false })
  userName?: string

  @ApiProperty()
  fistName: string

  @ApiProperty()
  lastName: string

  @ApiProperty()
  roleUid: string

  @ApiProperty()
  tenantUid: string

  @ApiProperty({ required: false })
  organizationUid?: string

  @ApiProperty({ required: false })
  organizationalUnitId?: string

  @ApiProperty({ required: false })
  birthDate?: string
}

export class UserCreateResponse {
  @ApiProperty()
  userId: string
}
