import {
  AuthenticationService,
  IPipelineTemplateBuilder,
  NestPipelineTemplate,
  PipelineDefinition,
  UserCreationResult,
  WpPipeline,
} from "@punks/backend-entity-manager"
import { AppAuthContext } from "@/infrastructure/authentication"
import { UserCreateRequest, UserCreateResponse } from "./models"
import { newUuid } from "@punks/backend-core"
import {
  AuthUserContext,
  AuthUserRegistrationData,
} from "@/infrastructure/authentication/models"
import { AppUserEntityManager } from "@/entities/appUsers/appUser.manager"

@WpPipeline("UserCreate")
export class UserCreateTemplate extends NestPipelineTemplate<
  UserCreateRequest,
  UserCreateResponse,
  AppAuthContext
> {
  constructor(
    private readonly auth: AuthenticationService,
    private readonly users: AppUserEntityManager
  ) {
    super()
  }

  protected isAuthorized(context: AppAuthContext) {
    return true
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<UserCreateRequest, AppAuthContext>
  ): PipelineDefinition<UserCreateRequest, UserCreateResponse, AppAuthContext> {
    return builder
      .addStep<UserCreationResult>((step) => {
        step
          .addOperation({
            name: "User create",
            action: async (input) =>
              this.auth.userCreate<AuthUserRegistrationData, AuthUserContext>({
                email: input.email,
                password: newUuid(),
                userName: input.userName ?? input.email,
                registrationInfo: {
                  firstName: input.fistName,
                  lastName: input.lastName,
                  birthDate: input.birthDate,
                },
                context: {
                  tenantUid: input.tenantUid,
                  organizationUid: input.organizationUid,
                },
              }),
            // precondition: (input, state) =>
            //   state.context.userRoles?.some((role) =>
            //     isAllowedToAssignRole(role.uid, input.roleUid)
            //   ) ?? false,
          })
          .withRollback({
            name: "User create rollback",
            action: async (_, output) =>
              output?.userId &&
              this.auth.userDelete({
                userId: output.userId,
              }),
          })
      })
      .addStep((step) => {
        step.addOperation({
          name: "OU assign",
          action: async (input, state) =>
            this.users.manager.update.execute(input.userId, {
              organizationalUnit: state.pipelineInput.organizationalUnitId
                ? {
                    id: state.pipelineInput.organizationalUnitId,
                  }
                : null,
            }),
          precondition: (input) => input.success,
          skipIf: (input, state) => !state.pipelineInput.organizationalUnitId,
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Role assign",
          action: async (input, state) =>
            this.auth.userRolesService.addUserToRoleByUid(
              this.utils.getStepOutput<UserCreationResult>(state, 0).userId,
              state.pipelineInput.roleUid
            ),
        })
      })
      .addStep<UserCreateResponse>((step) => {
        step.addOperation({
          name: "Map result",
          action: (_, state) => ({
            userId: this.utils.getStepOutput<UserCreationResult>(state, 0)
              .userId,
          }),
        })
      })
      .complete()
  }
}
