import { Repository } from "typeorm"
import { createTestServer } from "@/testing"
import { UserCreateTemplate } from "."
import { AppRoleType } from "@/infrastructure/authentication/roles"
import { AppUserEntity } from "@/database/core/entities/appUser.entity"
import { createAuthenticationContext } from "@/testing/mocks/authentication"
import { ApplicationAuthenticationContext } from "@/testing/mocks/authentication/types"
import {
  getRole,
  getRoleUid,
} from "@/infrastructure/authentication/roles/utils"

describe("Test User Create Pipeline", () => {
  let context: ApplicationAuthenticationContext
  let usersRepo: Repository<AppUserEntity>
  let template: UserCreateTemplate

  beforeEach(async () => {
    const { dataSource, app } = await createTestServer({})
    usersRepo = dataSource.getRepository(AppUserEntity)
    template = app.get(UserCreateTemplate)
    context = await createAuthenticationContext(app, dataSource)
  })

  it("a tenant admin should create another tenant admin", async () => {
    const result = await template.execute({
      input: {
        email: "user@test.it",
        userName: "user@test.it",
        fistName: "user",
        lastName: "test",
        roleUid: getRoleUid(AppRoleType.TenantAdmin),
        tenantUid: context.tenant.uid,
        organizationUid: context.organization.uid,
      },
      context: {
        isAnonymous: false,
        isAuthenticated: true,
        userRoles: [getRole(AppRoleType.TenantAdmin)],
      },
    })

    expect(result.type).toBe("success")

    const current = await usersRepo.findOneOrFail({
      where: {
        id: result.output.userId,
      },
      relations: ["userRoles", "userRoles.role"],
    })

    expect(current.userRoles).toHaveLength(1)
  })
})
