import * as request from "supertest"
import { Repository } from "typeorm"
import { createTestServer, NestTestServerPorts } from "@/testing"
import { AppTenantEntity } from "@/database/core/entities/appTenant.entity"
import { AppUserEntity } from "@/database/core/entities/appUser.entity"
import { AppRoles } from "@/infrastructure/authentication/roles"
import { TenantInitializeTemplate } from "."
import { TenantInitializeRequest } from "./models"
import { InstanceInitializeTemplate } from "../instance-initialize"
import { AppAdminAppModule } from "../../appAdmin.module"

describe("Test Tenant Initialize Pipeline", () => {
  let httpServer: any
  let tenantsRepo: Repository<AppTenantEntity>
  let usersRepo: Repository<AppUserEntity>
  let template: TenantInitializeTemplate
  let instanceInit: InstanceInitializeTemplate
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { dataSource, app, terminate } = await createTestServer({
      createWebServer: true,
      serverStartPort: NestTestServerPorts.TenantInitPipeline,
      extraModules: [AppAdminAppModule],
    })
    httpServer = app.getHttpServer()
    tenantsRepo = dataSource.getRepository(AppTenantEntity)
    usersRepo = dataSource.getRepository(AppUserEntity)
    template = app.get(TenantInitializeTemplate)
    instanceInit = app.get(InstanceInitializeTemplate)
    terminateServer = terminate
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should run the pipeline template successfully", async () => {
    await instanceInit.invoke({
      roles: Object.values(AppRoles),
    })
    const result = await template.execute({
      input: {
        tenant: {
          name: "Test Tenant",
          uid: "test-tenant",
        },
        directory: {
          name: "Global",
          uid: "global",
        },
        defaultAdmin: {
          email: "prova@prova.it",
          firstName: "Test",
          lastName: "Test2",
          password: "Prova123!",
        },
      },
      context: {
        isAnonymous: true,
        isAuthenticated: false,
      },
    })

    expect(result.type).toBe("success")
  })

  it("should run the pipeline api successfully", async () => {
    const response = await request(httpServer)
      .post("/v1/appAdmin/tenantInitialize")
      .send({
        tenant: {
          name: "Test Tenant",
          uid: "test-tenant",
        },
        directory: {
          name: "Global",
          uid: "global",
        },
        defaultAdmin: {
          email: "prova@prova.it",
          firstName: "Test",
          lastName: "Test2",
          password: "Prova123!",
        },
      } as TenantInitializeRequest)
      .expect(201)

    expect(response.body).toMatchObject({
      tenantId: expect.any(String),
    })

    const tenant = await tenantsRepo.findOne({
      where: {
        id: response.body.tenantId,
      },
    })
    expect(tenant).toMatchObject({
      id: expect.any(String),
      name: "Test Tenant",
      uid: "test-tenant",
    })

    const user = await usersRepo.findOne({
      where: {
        email: "prova@prova.it",
      },
      relations: ["tenant", "profile", "userRoles", "userRoles.role"],
    })

    expect(user).toMatchObject({
      id: expect.any(String),
      tenant: {
        id: tenant!.id,
      },
      profile: {
        firstName: "Test",
        lastName: "Test2",
      },
      userRoles: [
        {
          role: {
            uid: "tenant-admin",
          },
        },
      ],
    })
  })
})
