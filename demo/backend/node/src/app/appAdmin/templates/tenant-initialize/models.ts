import { ApiProperty } from "@nestjs/swagger"

export class TenantInitializeInfo {
  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string
}

export class DirectoryInitializeInfo {
  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string
}

export class TenantAdmin {
  @ApiProperty()
  email: string

  @ApiProperty()
  firstName: string

  @ApiProperty()
  lastName: string

  @ApiProperty()
  password: string
}

export class TenantInitializeRequest {
  @ApiProperty()
  tenant: TenantInitializeInfo

  @ApiProperty()
  directory: DirectoryInitializeInfo

  @ApiProperty()
  defaultAdmin: TenantAdmin
}

export class TenantInitializeResponse {
  @ApiProperty()
  tenantId: string
}
