import {
  AuthenticationService,
  IPipelineTemplateBuilder,
  NestPipelineTemplate,
  PipelineDefinition,
  WpPipeline,
} from "@punks/backend-entity-manager"
import { AppAuthContext } from "@/infrastructure/authentication"
import { UserDeletePipelineInput } from "./models"
import { AppUserEntityManager } from "@/entities/appUsers/appUser.manager"

@WpPipeline("UserDelete")
export class UserDeleteTemplate extends NestPipelineTemplate<
  UserDeletePipelineInput,
  void,
  AppAuthContext
> {
  constructor(
    private readonly auth: AuthenticationService,
    private readonly users: AppUserEntityManager
  ) {
    super()
  }

  protected isAuthorized(context: AppAuthContext) {
    return true
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<UserDeletePipelineInput, AppAuthContext>
  ): PipelineDefinition<UserDeletePipelineInput, void, AppAuthContext> {
    return builder
      .addStep((step) => {
        step.addOperation({
          name: "User roles remove",
          action: async (input) =>
            this.auth.userRolesService.clearUserRoles(input.userId),
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "User delete",
          action: async (_, state) =>
            this.users.manager.delete.execute(state.pipelineInput.userId),
        })
      })
      .complete()
  }
}
