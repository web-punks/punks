import { createTestServer } from "@/testing"
import { AppRoleType } from "@/infrastructure/authentication/roles"
import { UserDeleteTemplate } from "."
import {
  createAuthenticationContext,
  createUser,
} from "@/testing/mocks/authentication"
import { getRole } from "@/infrastructure/authentication/roles/utils"
import { INestApplication } from "@nestjs/common"
import { DataSource } from "typeorm"
import { AppUserRepository } from "@/database/core/repositories/appUser.repository"

describe("Test User Delete Pipeline", () => {
  let app: INestApplication<any>
  let dataSource: DataSource
  let template: UserDeleteTemplate
  let users: AppUserRepository

  beforeEach(async () => {
    const server = await createTestServer({})
    app = server.app
    dataSource = server.dataSource
    users = app.get(AppUserRepository)
    template = app.get(UserDeleteTemplate)
  })

  it("a tenant admin should delete a user", async () => {
    const context = await createAuthenticationContext(app, dataSource)
    const user = await createUser(app, context, {
      roleUid: "company-user",
    })

    let current = await users.get(user.id)
    expect(current).toBeDefined()

    const result = await template.execute({
      input: {
        userId: user.id,
      },
      context: {
        isAnonymous: false,
        isAuthenticated: true,
        userRoles: [getRole(AppRoleType.TenantAdmin)],
      },
    })

    expect(result.type).toBe("success")

    current = await users.get(user.id)
    expect(current).toBeUndefined()
  })
})
