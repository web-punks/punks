import {
  AuthenticationService,
  IPipelineTemplateBuilder,
  NestPipelineTemplate,
  PipelineDefinition,
  WpPipeline,
} from "@punks/backend-entity-manager"
import { AppAuthContext } from "@/infrastructure/authentication"
import { InstanceInitializeInput } from "./models"
import { AppPermissionEntityManager } from "@/entities/appPermissions/appPermission.manager"
import { subArrays } from "@/utils/arrays"

@WpPipeline("InstanceInitialize")
export class InstanceInitializeTemplate extends NestPipelineTemplate<
  InstanceInitializeInput,
  void,
  AppAuthContext
> {
  constructor(
    private readonly auth: AuthenticationService,
    private readonly permissions: AppPermissionEntityManager
  ) {
    super()
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<InstanceInitializeInput, AppAuthContext>
  ): PipelineDefinition<InstanceInitializeInput, void, AppAuthContext> {
    return builder
      .addStep((step) => {
        step.addOperation({
          name: "Permissions ensure",
          action: async (_, state) => {
            for (const permission of state.pipelineInput.permission) {
              const item = await this.permissions.manager.find.execute({
                filters: {
                  uid: {
                    eq: permission.uid,
                  },
                },
              })
              if (!item) {
                await this.permissions.manager.create.execute({
                  name: permission.name,
                  uid: permission.uid,
                })
              }
            }
          },
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Roles ensure",
          action: async (_, state) => {
            for (const role of state.pipelineInput.roles) {
              await this.auth.rolesService.ensure(role.uid, {
                name: role.name,
              })

              const missingPermissions = subArrays(
                role.permissions,
                await this.auth.rolesService.getPermissions(role.uid),
                (a, b) => a.uid === b.uid
              )
              for (const rolePermission of missingPermissions) {
                await this.auth.rolesService.addPermission(
                  role.uid,
                  rolePermission.uid
                )
              }
            }
          },
        })
      })
      .complete()
  }
}
