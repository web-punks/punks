import {
  AppPermissionItem,
  AppRoleItem,
} from "@/infrastructure/authentication/roles/types"

export interface InstanceInitializeInput {
  roles: AppRoleItem[]
  permission: AppPermissionItem[]
}
