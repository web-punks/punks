import { InstanceInitializeTemplate } from "./instance-initialize"
import { OrganizationRegisterTemplate } from "./organization-register"
import { TenantInitializeTemplate } from "./tenant-initialize"
import { UserCreateTemplate } from "./user-create"
import { UserDeleteTemplate } from "./user-delete"
import { UserUpdateTemplate } from "./user-update"

export const AppAdminTemplates = [
  InstanceInitializeTemplate,
  OrganizationRegisterTemplate,
  TenantInitializeTemplate,
  UserCreateTemplate,
  UserUpdateTemplate,
  UserDeleteTemplate,
]
