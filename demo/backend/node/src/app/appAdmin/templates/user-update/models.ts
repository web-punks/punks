import { ApiProperty } from "@nestjs/swagger"

export class UserUpdateRequest {
  @ApiProperty()
  email: string

  @ApiProperty({ required: false })
  userName?: string

  @ApiProperty()
  fistName: string

  @ApiProperty()
  lastName: string

  @ApiProperty()
  roleUid: string

  @ApiProperty({ required: false })
  organizationalUnitId?: string

  @ApiProperty({ required: false })
  birthDate?: string
}

export type UserUpdatePipelineInput = {
  userId: string
  data: UserUpdateRequest
}
