import { Body, Controller, Delete, Param, Post } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { Public } from "@punks/backend-entity-manager"
import {
  TenantInitializeRequest,
  TenantInitializeResponse,
} from "./templates/tenant-initialize/models"
import { TenantInitializeTemplate } from "./templates/tenant-initialize"
import { InstanceInitializeTemplate } from "./templates/instance-initialize"
import {
  OrganizationInitializeRequest,
  OrganizationInitializeResponse,
} from "./templates/organization-register/models"
import { OrganizationRegisterTemplate } from "./templates/organization-register"
import { UserCreateTemplate } from "./templates/user-create"
import {
  UserCreateRequest,
  UserCreateResponse,
} from "./templates/user-create/models"
import { UserUpdateTemplate } from "./templates/user-update"
import { UserDeleteTemplate } from "./templates/user-delete"
import { UserUpdateRequest } from "./templates/user-update/models"
import { AppPermissions } from "@/middleware/authentication/permissions"
import {
  OrganizationManagers,
  TenantManagers,
} from "@/middleware/authentication/guards"
import { AppRoles } from "@/middleware/authentication/roles"

@OrganizationManagers()
@Controller("v1/appAdmin")
export class AppAdminController {
  constructor(
    private readonly tenantInit: TenantInitializeTemplate,
    private readonly instanceInit: InstanceInitializeTemplate,
    private readonly organizationInit: OrganizationRegisterTemplate,
    private readonly userCreateTemplate: UserCreateTemplate,
    private readonly userUpdateTemplate: UserUpdateTemplate,
    private readonly userDeleteTemplate: UserDeleteTemplate
  ) {}

  @Public()
  @Post("instanceInitialize")
  @ApiOperation({
    operationId: "instanceInitialize",
  })
  async instanceInitialize() {
    await this.instanceInit.invoke({
      roles: Object.values(AppRoles),
      permission: Object.values(AppPermissions),
    })
  }

  @Public()
  @Post("tenantInitialize")
  @ApiOkResponse({
    type: TenantInitializeResponse,
  })
  @ApiOperation({
    operationId: "tenantInitialize",
  })
  async tenantInitialize(
    @Body() request: TenantInitializeRequest
  ): Promise<TenantInitializeResponse> {
    await this.instanceInit.invoke({
      roles: Object.values(AppRoles),
      permission: Object.values(AppPermissions),
    })
    return await this.tenantInit.invoke(request)
  }

  @TenantManagers()
  @Post("organizationInitialize")
  @ApiOkResponse({
    type: OrganizationInitializeResponse,
  })
  @ApiOperation({
    operationId: "organizationInitialize",
  })
  async organizationInitialize(
    @Body() request: OrganizationInitializeRequest
  ): Promise<OrganizationInitializeResponse> {
    return await this.organizationInit.invoke(request)
  }

  @Post("userCreate")
  @ApiOkResponse({
    type: OrganizationInitializeResponse,
  })
  @ApiOperation({
    operationId: "userCreate",
  })
  async userCreate(
    @Body() request: UserCreateRequest
  ): Promise<UserCreateResponse> {
    return await this.userCreateTemplate.invoke(request)
  }

  @Post("userUpdate/:id")
  @ApiOkResponse({})
  @ApiOperation({
    operationId: "userUpdate",
  })
  async userUpdate(
    @Param("id") id: string,
    @Body() request: UserUpdateRequest
  ): Promise<void> {
    await this.userUpdateTemplate.invoke({
      userId: id,
      data: request,
    })
  }

  @Delete("userDelete/:id")
  @ApiOkResponse({})
  @ApiOperation({
    operationId: "userDelete",
  })
  async userDelete(@Param("id") id: string): Promise<void> {
    await this.userDeleteTemplate.invoke({
      userId: id,
    })
  }
}
