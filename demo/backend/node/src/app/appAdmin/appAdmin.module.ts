import { Module } from "@nestjs/common"
import { SharedModule } from "@/shared/module"
import { AppAdminTemplates } from "./templates/index"
import { AppAdminController } from "./appAdmin.controller"

@Module({
  imports: [SharedModule],
  providers: [...AppAdminTemplates],
  controllers: [AppAdminController],
})
export class AppAdminAppModule {}
