import {
  AwsBucketSettings,
  AuthenticationModuleSettings,
  EntityManagerSettings,
  SendgridSettings,
  AwsSecretsSettings,
} from "@punks/backend-entity-manager"

export const getSendgridSettings = (): SendgridSettings => ({
  apiKey: process.env.SENDGRID_API_KEY,
  defaultSender: process.env.SENDGRID_DEFAULT_SENDER,
  defaultReplyTo: process.env.SENDGRID_DEFAULT_REPLY_TO,
})

export const getAuthSettings = (): AuthenticationModuleSettings => ({
  jwtSecret: process.env.AUTH_JWT_SECRET,
  passwordSalt: process.env.AUTH_PASSWORD_SALT,
})

export const getAwsBucketSettings = (): AwsBucketSettings => ({
  awsAccessKeyId: process.env.AWS_ACCESS_KEY_ID,
  awsSecretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
  defaultBucket: process.env.AWS_DEFAULT_BUCKET,
  paths: {
    filesUpload: process.env.AWS_PATH_FILES_UPLOAD,
  },
  publicLinksExpirationMinutes: 60,
})

export const getAwsSecretsSettings = (): AwsSecretsSettings => ({
  awsAccessKeyId: process.env.AWS_ACCESS_KEY_ID,
  awsSecretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
  secretsRootPath: process.env.AWS_SECRETS_ROOT_PATH,
})

export const getEntityManagerSettings = (): EntityManagerSettings => ({
  importExport: {
    exportBucket: {
      bucket: process.env.ENTITY_MANAGER_IMPORT_EXPORT_BUCKET,
      publicLinksExpirationMinutes: 60,
      rootFolderPath: process.env.ENTITY_MANAGER_IMPORT_EXPORT_PATH,
    },
  },
  defaultBucketProvider: process.env.ENTITY_MANAGER_DEFAULT_BUCKET_PROVIDER,
  defaultFilesProvider: process.env.ENTITY_MANAGER_DEFAULT_FILES_PROVIDER,
  defaultMediaProvider: process.env.ENTITY_MANAGER_DEFAULT_MEDIA_PROVIDER,
})
