import { Module } from "@nestjs/common"
import { EntitiesModule } from "../entities"
import { SharedModule } from "../shared/module"
import { AuthenticationEventsHandler } from "./authentication"
import { PlatformEventsHandler } from "./platform"

@Module({
  imports: [SharedModule, EntitiesModule],
  providers: [AuthenticationEventsHandler, PlatformEventsHandler],
})
export class AppEventHandlersModule {}
