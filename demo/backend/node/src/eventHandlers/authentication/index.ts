import { Injectable } from "@nestjs/common"
import { OnEvent } from "@nestjs/event-emitter"
import { IEventLog, TrackingService } from "@punks/backend-entity-manager"

@Injectable()
export class AuthenticationEventsHandler {
  constructor(private readonly tracker: TrackingService) {}

  @OnEvent("authentication:*")
  async execute(event: IEventLog<unknown>) {
    await this.tracker.trackEvent(event)
  }
}
