import { Controller, Get, Post, Res } from "@nestjs/common"
import { ApiOperation } from "@nestjs/swagger"
import { Public } from "@punks/backend-entity-manager"
import { runDatabaseMigrations } from "./shared/database/migrations"
import { ormConnectionOptions } from "./app.ormconfig"

@Public()
@Controller()
export class AppController {
  @Get()
  index(@Res() res: any) {
    return res.redirect("swagger")
  }

  @Get("health")
  @ApiOperation({
    operationId: "healthCheck",
  })
  health() {
    return {
      healthy: true,
    }
  }

  @Post("migrate")
  async migrate() {
    await runDatabaseMigrations(ormConnectionOptions.core(), "core")
  }
}
