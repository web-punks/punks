import { NestFactory } from "@nestjs/core"
import { Log } from "@punks/backend-core"
import { AppModule } from "./app.module"
import { setupServer } from "./server"

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true })
  await setupServer(app)
  const port = process.env.HTTP_PORT || 3000
  await app.listen(port)
  Log.getLogger("Main").info(`App started -> http://localhost:${port}/`)
}
bootstrap()
