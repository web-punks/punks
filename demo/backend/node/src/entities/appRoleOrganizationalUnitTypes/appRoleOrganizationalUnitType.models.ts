import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppRoleOrganizationalUnitTypeEntity } from "../../database/core/entities/appRoleOrganizationalUnitType.entity"

export type AppRoleOrganizationalUnitTypeCreateData = DeepPartial<
  Omit<AppRoleOrganizationalUnitTypeEntity, "id">
>
export type AppRoleOrganizationalUnitTypeUpdateData = DeepPartial<
  Omit<AppRoleOrganizationalUnitTypeEntity, "id">
>

export enum AppRoleOrganizationalUnitTypeSorting {
  Name = "Name",
}

export type AppRoleOrganizationalUnitTypeCursor = number

export class AppRoleOrganizationalUnitTypeSearchFilters {
  @ApiProperty({ required: false })
  roleIds?: string[]

  @ApiProperty({ required: false })
  typeIds?: string[]
}

export class AppRoleOrganizationalUnitTypeFacets {}

export type AppRoleOrganizationalUnitTypeSheetItem =
  AppRoleOrganizationalUnitTypeEntity
