import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppRoleOrganizationalUnitTypeEntity } from "../../database/core/entities/appRoleOrganizationalUnitType.entity"
import {
  AppRoleOrganizationalUnitTypeSorting,
  AppRoleOrganizationalUnitTypeCursor,
  AppRoleOrganizationalUnitTypeSearchFilters,
  AppRoleOrganizationalUnitTypeFacets,
} from "./appRoleOrganizationalUnitType.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppRoleOrganizationalUnitTypeSearchSortingField
  implements ISearchSortingField<AppRoleOrganizationalUnitTypeSorting>
{
  @ApiProperty({ enum: AppRoleOrganizationalUnitTypeSorting })
  field: AppRoleOrganizationalUnitTypeSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppRoleOrganizationalUnitTypeQuerySorting
  implements ISearchSorting<AppRoleOrganizationalUnitTypeSorting>
{
  @ApiProperty({
    required: false,
    type: [AppRoleOrganizationalUnitTypeSearchSortingField],
  })
  fields: AppRoleOrganizationalUnitTypeSearchSortingField[]
}

export class AppRoleOrganizationalUnitTypeQueryPaging
  implements ISearchRequestPaging<AppRoleOrganizationalUnitTypeCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppRoleOrganizationalUnitTypeCursor

  @ApiProperty()
  pageSize: number
}

export class AppRoleOrganizationalUnitTypeSearchOptions
  implements ISearchOptions
{
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppRoleOrganizationalUnitTypeFullTextQuery
  implements IFullTextQuery
{
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppRoleOrganizationalUnitTypeSearchParameters
  implements
    IEntitySearchParameters<
      AppRoleOrganizationalUnitTypeEntity,
      AppRoleOrganizationalUnitTypeSorting,
      AppRoleOrganizationalUnitTypeCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppRoleOrganizationalUnitTypeFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppRoleOrganizationalUnitTypeSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppRoleOrganizationalUnitTypeQuerySorting

  @ApiProperty({ required: false })
  paging?: AppRoleOrganizationalUnitTypeQueryPaging

  @ApiProperty({ required: false })
  options?: AppRoleOrganizationalUnitTypeSearchOptions

  relations?: ISearchQueryRelations<AppRoleOrganizationalUnitTypeEntity>
}

export class AppRoleOrganizationalUnitTypeSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppRoleOrganizationalUnitTypeCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppRoleOrganizationalUnitTypeCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppRoleOrganizationalUnitTypeCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppRoleOrganizationalUnitTypeCursor
}

export class AppRoleOrganizationalUnitTypeSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppRoleOrganizationalUnitTypeEntity,
      AppRoleOrganizationalUnitTypeSearchParameters,
      TResult,
      AppRoleOrganizationalUnitTypeSorting,
      AppRoleOrganizationalUnitTypeCursor,
      AppRoleOrganizationalUnitTypeFacets
    >
{
  @ApiProperty()
  request: AppRoleOrganizationalUnitTypeSearchParameters

  @ApiProperty()
  facets?: AppRoleOrganizationalUnitTypeFacets

  @ApiProperty()
  paging?: AppRoleOrganizationalUnitTypeSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppRoleOrganizationalUnitTypeDeleteParameters
  implements IEntitiesDeleteParameters<AppRoleOrganizationalUnitTypeSorting>
{
  @ApiProperty({ required: false })
  filters?: AppRoleOrganizationalUnitTypeSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppRoleOrganizationalUnitTypeQuerySorting
}

export class AppRoleOrganizationalUnitTypeVersionsSearchParameters
  implements
    IEntityVersionsSearchParameters<AppRoleOrganizationalUnitTypeCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppRoleOrganizationalUnitTypeCursor>
}

export class AppRoleOrganizationalUnitTypeVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppRoleOrganizationalUnitTypeCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppRoleOrganizationalUnitTypeCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppRoleOrganizationalUnitTypeCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppRoleOrganizationalUnitTypeCursor
}

export class AppRoleOrganizationalUnitTypeVersionsSearchResults<TResult>
  implements
    IEntityVersionsSearchResults<TResult, AppRoleOrganizationalUnitTypeCursor>
{
  @ApiProperty()
  paging?: AppRoleOrganizationalUnitTypeVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
