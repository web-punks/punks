import { FindOptionsOrder, FindOptionsWhere, In } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppRoleOrganizationalUnitTypeEntity,
  AppRoleOrganizationalUnitTypeEntityId,
} from "../../database/core/entities/appRoleOrganizationalUnitType.entity"
import {
  AppRoleOrganizationalUnitTypeFacets,
  AppRoleOrganizationalUnitTypeSorting,
} from "./appRoleOrganizationalUnitType.models"
import { AppRoleOrganizationalUnitTypeSearchParameters } from "./appRoleOrganizationalUnitType.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appRoleOrganizationalUnitType")
export class AppRoleOrganizationalUnitTypeQueryBuilder extends NestTypeOrmQueryBuilder<
  AppRoleOrganizationalUnitTypeEntity,
  AppRoleOrganizationalUnitTypeEntityId,
  AppRoleOrganizationalUnitTypeSearchParameters,
  AppRoleOrganizationalUnitTypeSorting,
  AppRoleOrganizationalUnitTypeFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRoleOrganizationalUnitType", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ):
    | FindOptionsWhere<AppRoleOrganizationalUnitTypeEntity>
    | FindOptionsWhere<AppRoleOrganizationalUnitTypeEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppRoleOrganizationalUnitTypeSearchParameters
  ): FindOptionsOrder<AppRoleOrganizationalUnitTypeEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppRoleOrganizationalUnitTypeSearchParameters
  ):
    | FindOptionsWhere<AppRoleOrganizationalUnitTypeEntity>
    | FindOptionsWhere<AppRoleOrganizationalUnitTypeEntity>[] {
    return {
      ...(request.filters?.roleIds && {
        role: {
          id: In(request.filters.roleIds),
        },
      }),
      ...(request.filters?.typeIds && {
        type: {
          id: In(request.filters.typeIds),
        },
      }),
    }
  }

  protected calculateFacets(
    request: AppRoleOrganizationalUnitTypeSearchParameters,
    context?: AppAuthContext
  ): Promise<AppRoleOrganizationalUnitTypeFacets> {
    return Promise.resolve({})
  }
}
