import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppRoleOrganizationalUnitTypeCreateData,
  AppRoleOrganizationalUnitTypeCursor,
  AppRoleOrganizationalUnitTypeFacets,
  AppRoleOrganizationalUnitTypeSorting,
  AppRoleOrganizationalUnitTypeUpdateData,
} from "./appRoleOrganizationalUnitType.models"
import {
  AppRoleOrganizationalUnitTypeSearchParameters,
  AppRoleOrganizationalUnitTypeDeleteParameters,
} from "./appRoleOrganizationalUnitType.types"
import {
  AppRoleOrganizationalUnitTypeEntity,
  AppRoleOrganizationalUnitTypeEntityId,
} from "../../database/core/entities/appRoleOrganizationalUnitType.entity"

@WpEntityManager("appRoleOrganizationalUnitType")
export class AppRoleOrganizationalUnitTypeEntityManager extends NestEntityManager<
  AppRoleOrganizationalUnitTypeEntity,
  AppRoleOrganizationalUnitTypeEntityId,
  AppRoleOrganizationalUnitTypeCreateData,
  AppRoleOrganizationalUnitTypeUpdateData,
  AppRoleOrganizationalUnitTypeDeleteParameters,
  AppRoleOrganizationalUnitTypeSearchParameters,
  AppRoleOrganizationalUnitTypeSorting,
  AppRoleOrganizationalUnitTypeCursor,
  AppRoleOrganizationalUnitTypeFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRoleOrganizationalUnitType", registry)
  }
}
