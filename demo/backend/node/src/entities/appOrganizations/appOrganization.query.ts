import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationEntity,
  AppOrganizationEntityId,
} from "../../database/core/entities/appOrganization.entity"
import {
  AppOrganizationFacets,
  AppOrganizationSorting
} from "./appOrganization.models"
import { AppOrganizationSearchParameters } from "./appOrganization.types"
import {
  AppUserContext,
  AppAuthContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appOrganization")
export class AppOrganizationQueryBuilder extends NestTypeOrmQueryBuilder<
  AppOrganizationEntity,
  AppOrganizationEntityId,
  AppOrganizationSearchParameters,
  AppOrganizationSorting,
  AppOrganizationFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganization", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ):
    | FindOptionsWhere<AppOrganizationEntity>
    | FindOptionsWhere<AppOrganizationEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppOrganizationSearchParameters
  ): FindOptionsOrder<AppOrganizationEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppOrganizationSearchParameters
  ):
    | FindOptionsWhere<AppOrganizationEntity>
    | FindOptionsWhere<AppOrganizationEntity>[] {
    return {
      ...(request.filters?.uid ? { uid: request.filters.uid } : {}),
    }
  }

  protected calculateFacets(
    request: AppOrganizationSearchParameters,
    context?: AppAuthContext
  ): Promise<AppOrganizationFacets> {
    return Promise.resolve({})
  }
}
