import {
  DeepPartial,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppOrganizationEntity } from "../../database/core/entities/appOrganization.entity"

export type AppOrganizationCreateData = DeepPartial<Omit<AppOrganizationEntity, "id">>
export type AppOrganizationUpdateData = DeepPartial<Omit<AppOrganizationEntity, "id">>

export enum AppOrganizationSorting {
  Name = "Name",
}

export type AppOrganizationCursor = number

export class AppOrganizationFacets {}

export class AppOrganizationSearchFilters {
  uid?: string
}

export type AppOrganizationSheetItem = AppOrganizationEntity
