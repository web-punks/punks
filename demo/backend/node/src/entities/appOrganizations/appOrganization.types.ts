import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppOrganizationEntity } from "../../database/core/entities/appOrganization.entity"
import {
  AppOrganizationSorting,
  AppOrganizationCursor,
  AppOrganizationSearchFilters,
  AppOrganizationFacets,
} from "./appOrganization.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppOrganizationSearchSortingField
  implements ISearchSortingField<AppOrganizationSorting>
{
  @ApiProperty({ enum: AppOrganizationSorting })
  field: AppOrganizationSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppOrganizationQuerySorting
  implements ISearchSorting<AppOrganizationSorting>
{
  @ApiProperty({ required: false, type: [AppOrganizationSearchSortingField] })
  fields: AppOrganizationSearchSortingField[]
}

export class AppOrganizationQueryPaging
  implements ISearchRequestPaging<AppOrganizationCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppOrganizationCursor

  @ApiProperty()
  pageSize: number
}

export class AppOrganizationSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppOrganizationFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppOrganizationSearchParameters
  implements
    IEntitySearchParameters<
      AppOrganizationEntity,
      AppOrganizationSorting,
      AppOrganizationCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppOrganizationFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppOrganizationSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppOrganizationQuerySorting

  @ApiProperty({ required: false })
  paging?: AppOrganizationQueryPaging

  @ApiProperty({ required: false })
  options?: AppOrganizationSearchOptions

  relations?: ISearchQueryRelations<AppOrganizationEntity>
}

export class AppOrganizationSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppOrganizationCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppOrganizationCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppOrganizationCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppOrganizationCursor
}

export class AppOrganizationSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppOrganizationEntity,
      AppOrganizationSearchParameters,
      TResult,
      AppOrganizationSorting,
      AppOrganizationCursor,
      AppOrganizationFacets
    >
{
  @ApiProperty()
  request: AppOrganizationSearchParameters

  @ApiProperty()
  facets?: AppOrganizationFacets

  @ApiProperty()
  paging?: AppOrganizationSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppOrganizationDeleteParameters
  implements IEntitiesDeleteParameters<AppOrganizationSorting>
{
  @ApiProperty({ required: false })
  filters?: AppOrganizationSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppOrganizationQuerySorting
}

export class AppOrganizationVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppOrganizationCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppOrganizationCursor>
}

export class AppOrganizationVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppOrganizationCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppOrganizationCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppOrganizationCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppOrganizationCursor
}

export class AppOrganizationVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppOrganizationCursor>
{
  @ApiProperty()
  paging?: AppOrganizationVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
