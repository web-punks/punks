import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "../../database/core/entities/appOrganization.entity"
import { AppOrganizationCreateData, AppOrganizationUpdateData } from "./appOrganization.models"

@WpEntityAdapter("appOrganization")
export class AppOrganizationAdapter
  implements
    IEntityAdapter<
      AppOrganizationEntity,
      AppOrganizationCreateData,
      AppOrganizationUpdateData
    >
{
  createDataToEntity(data: AppOrganizationCreateData): DeepPartial<AppOrganizationEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppOrganizationUpdateData): DeepPartial<AppOrganizationEntity> {
    return {
      ...data,
    }
  }
}
