import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationCreateData,
  AppOrganizationCursor,
  AppOrganizationFacets,
  AppOrganizationSorting,
  AppOrganizationUpdateData,
} from "./appOrganization.models"
import {
  AppOrganizationSearchParameters,
  AppOrganizationDeleteParameters,
} from "./appOrganization.types"
import {
  AppOrganizationEntity,
  AppOrganizationEntityId,
} from "../../database/core/entities/appOrganization.entity"

@WpEntityManager("appOrganization")
export class AppOrganizationEntityManager extends NestEntityManager<
  AppOrganizationEntity,
  AppOrganizationEntityId,
  AppOrganizationCreateData,
  AppOrganizationUpdateData,
  AppOrganizationDeleteParameters,
  AppOrganizationSearchParameters,
  AppOrganizationSorting,
  AppOrganizationCursor,
  AppOrganizationFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganization", registry)
  }
}
