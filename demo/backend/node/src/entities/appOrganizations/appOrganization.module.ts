import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppOrganizationAdapter } from "./appOrganization.adapter"
import { AppOrganizationAuthMiddleware } from "./appOrganization.authentication"
import { AppOrganizationEntityManager } from "./appOrganization.manager"
import { AppOrganizationQueryBuilder } from "./appOrganization.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppOrganizationAdapter,
    AppOrganizationAuthMiddleware,
    AppOrganizationEntityManager,
    AppOrganizationQueryBuilder,
  ],
  exports: [AppOrganizationEntityManager],
})
export class AppOrganizationEntityModule {}
