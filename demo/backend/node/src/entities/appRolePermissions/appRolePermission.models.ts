import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppRolePermissionEntity } from "../../database/core/entities/appRolePermission.entity"
import { StringFilter } from "@/shared/api/fields"

export type AppRolePermissionCreateData = DeepPartial<
  Omit<AppRolePermissionEntity, "id">
>
export type AppRolePermissionUpdateData = DeepPartial<
  Omit<AppRolePermissionEntity, "id">
>

export enum AppRolePermissionSorting {
  Name = "Name",
}

export type AppRolePermissionCursor = number

export class AppRolePermissionSearchFilters {
  @ApiProperty({ required: false })
  roleId?: StringFilter

  @ApiProperty({ required: false })
  roleUid?: StringFilter

  @ApiProperty({ required: false })
  permissionId?: StringFilter

  @ApiProperty({ required: false })
  permissionUid?: StringFilter
}

export class AppRolePermissionFacets {}

export type AppRolePermissionSheetItem = AppRolePermissionEntity
