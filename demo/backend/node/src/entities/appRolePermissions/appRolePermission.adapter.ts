import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppRolePermissionEntity } from "../../database/core/entities/appRolePermission.entity"
import { AppRolePermissionCreateData, AppRolePermissionUpdateData } from "./appRolePermission.models"

@WpEntityAdapter("appRolePermission")
export class AppRolePermissionAdapter
  implements
    IEntityAdapter<
      AppRolePermissionEntity,
      AppRolePermissionCreateData,
      AppRolePermissionUpdateData
    >
{
  createDataToEntity(data: AppRolePermissionCreateData): DeepPartial<AppRolePermissionEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppRolePermissionUpdateData): DeepPartial<AppRolePermissionEntity> {
    return {
      ...data,
    }
  }
}
