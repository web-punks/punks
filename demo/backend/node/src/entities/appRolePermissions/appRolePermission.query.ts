import { FindOptionsOrder, FindOptionsWhere, In } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppRolePermissionEntity,
  AppRolePermissionEntityId,
} from "../../database/core/entities/appRolePermission.entity"
import {
  AppRolePermissionFacets,
  AppRolePermissionSorting,
} from "./appRolePermission.models"
import { AppRolePermissionSearchParameters } from "./appRolePermission.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appRolePermission")
export class AppRolePermissionQueryBuilder extends NestTypeOrmQueryBuilder<
  AppRolePermissionEntity,
  AppRolePermissionEntityId,
  AppRolePermissionSearchParameters,
  AppRolePermissionSorting,
  AppRolePermissionFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRolePermission", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ):
    | FindOptionsWhere<AppRolePermissionEntity>
    | FindOptionsWhere<AppRolePermissionEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppRolePermissionSearchParameters
  ): FindOptionsOrder<AppRolePermissionEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppRolePermissionSearchParameters
  ):
    | FindOptionsWhere<AppRolePermissionEntity>
    | FindOptionsWhere<AppRolePermissionEntity>[] {
    return {
      ...(request.filters?.roleId
        ? {
            role: {
              id: this.clause.stringFilter(request.filters.roleId),
            },
          }
        : {}),
      ...(request.filters?.roleUid
        ? {
            role: {
              uid: this.clause.stringFilter(request.filters.roleUid),
            },
          }
        : {}),
      ...(request.filters?.permissionId
        ? {
            permission: {
              id: this.clause.stringFilter(request.filters.permissionId),
            },
          }
        : {}),
      ...(request.filters?.permissionUid
        ? {
            permission: {
              uid: this.clause.stringFilter(request.filters.permissionUid),
            },
          }
        : {}),
    }
  }

  protected calculateFacets(
    request: AppRolePermissionSearchParameters,
    context?: AppAuthContext
  ): Promise<AppRolePermissionFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
