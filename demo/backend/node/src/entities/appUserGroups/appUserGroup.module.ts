import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppUserGroupAdapter } from "./appUserGroup.adapter"
import { AppUserGroupAuthMiddleware } from "./appUserGroup.authentication"
import { AppUserGroupEntityManager } from "./appUserGroup.manager"
import { AppUserGroupQueryBuilder } from "./appUserGroup.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppUserGroupAdapter,
    AppUserGroupAuthMiddleware,
    AppUserGroupEntityManager,
    AppUserGroupQueryBuilder,
  ],
  exports: [AppUserGroupEntityManager],
})
export class AppUserGroupEntityModule {}
