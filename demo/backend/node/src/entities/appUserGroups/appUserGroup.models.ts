import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppUserGroupEntity } from "../../database/core/entities/appUserGroup.entity"

export type AppUserGroupCreateData = DeepPartial<Omit<AppUserGroupEntity, "id">>
export type AppUserGroupUpdateData = DeepPartial<Omit<AppUserGroupEntity, "id">>

export enum AppUserGroupSorting {
  Name = "Name",
}

export type AppUserGroupCursor = number

export class AppUserGroupSearchFilters {}

export class AppUserGroupFacets {}

export type AppUserGroupSheetItem = AppUserGroupEntity
