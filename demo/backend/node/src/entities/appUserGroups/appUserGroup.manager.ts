import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppUserGroupCreateData,
  AppUserGroupCursor,
  AppUserGroupFacets,
  AppUserGroupSorting,
  AppUserGroupUpdateData,
} from "./appUserGroup.models"
import {
  AppUserGroupSearchParameters,
  AppUserGroupDeleteParameters,
} from "./appUserGroup.types"
import {
  AppUserGroupEntity,
  AppUserGroupEntityId,
} from "../../database/core/entities/appUserGroup.entity"

@WpEntityManager("appUserGroup")
export class AppUserGroupEntityManager extends NestEntityManager<
  AppUserGroupEntity,
  AppUserGroupEntityId,
  AppUserGroupCreateData,
  AppUserGroupUpdateData,
  AppUserGroupDeleteParameters,
  AppUserGroupSearchParameters,
  AppUserGroupSorting,
  AppUserGroupCursor,
  AppUserGroupFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroup", registry)
  }
}
