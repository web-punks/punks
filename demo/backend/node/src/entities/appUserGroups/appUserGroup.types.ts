import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppUserGroupEntity } from "../../database/core/entities/appUserGroup.entity"
import {
  AppUserGroupSorting,
  AppUserGroupCursor,
  AppUserGroupSearchFilters,
  AppUserGroupFacets,
} from "./appUserGroup.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppUserGroupSearchSortingField
  implements ISearchSortingField<AppUserGroupSorting>
{
  @ApiProperty({ enum: AppUserGroupSorting })
  field: AppUserGroupSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppUserGroupQuerySorting
  implements ISearchSorting<AppUserGroupSorting>
{
  @ApiProperty({ required: false, type: [AppUserGroupSearchSortingField] })
  fields: AppUserGroupSearchSortingField[]
}

export class AppUserGroupQueryPaging
  implements ISearchRequestPaging<AppUserGroupCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppUserGroupCursor

  @ApiProperty()
  pageSize: number
}

export class AppUserGroupSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppUserGroupFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppUserGroupSearchParameters
  implements
    IEntitySearchParameters<
      AppUserGroupEntity,
      AppUserGroupSorting,
      AppUserGroupCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppUserGroupFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppUserGroupSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserGroupQuerySorting

  @ApiProperty({ required: false })
  paging?: AppUserGroupQueryPaging

  @ApiProperty({ required: false })
  options?: AppUserGroupSearchOptions

  relations?: ISearchQueryRelations<AppUserGroupEntity>
}

export class AppUserGroupSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppUserGroupCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserGroupCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserGroupCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserGroupCursor
}

export class AppUserGroupSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppUserGroupEntity,
      AppUserGroupSearchParameters,
      TResult,
      AppUserGroupSorting,
      AppUserGroupCursor,
      AppUserGroupFacets
    >
{
  @ApiProperty()
  request: AppUserGroupSearchParameters

  @ApiProperty()
  facets?: AppUserGroupFacets

  @ApiProperty()
  paging?: AppUserGroupSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppUserGroupDeleteParameters
  implements IEntitiesDeleteParameters<AppUserGroupSorting>
{
  @ApiProperty({ required: false })
  filters?: AppUserGroupSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserGroupQuerySorting
}

export class AppUserGroupVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppUserGroupCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppUserGroupCursor>
}

export class AppUserGroupVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppUserGroupCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserGroupCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserGroupCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserGroupCursor
}

export class AppUserGroupVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppUserGroupCursor>
{
  @ApiProperty()
  paging?: AppUserGroupVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
