import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppEntityVersionCreateData,
  AppEntityVersionCursor,
  AppEntityVersionFacets,
  AppEntityVersionSorting,
  AppEntityVersionUpdateData,
} from "./appEntityVersion.models"
import {
  AppEntityVersionSearchParameters,
  AppEntityVersionDeleteParameters,
} from "./appEntityVersion.types"
import {
  AppEntityVersionEntity,
  AppEntityVersionEntityId,
} from "../../database/core/entities/appEntityVersion.entity"

@WpEntityManager("appEntityVersion")
export class AppEntityVersionEntityManager extends NestEntityManager<
  AppEntityVersionEntity,
  AppEntityVersionEntityId,
  AppEntityVersionCreateData,
  AppEntityVersionUpdateData,
  AppEntityVersionDeleteParameters,
  AppEntityVersionSearchParameters,
  AppEntityVersionSorting,
  AppEntityVersionCursor,
  AppEntityVersionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEntityVersion", registry)
  }
}
