import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppEntityVersionEntity,
  AppEntityVersionEntityId,
} from "../../database/core/entities/appEntityVersion.entity"
import {
  AppEntityVersionFacets,
  AppEntityVersionSorting,
} from "./appEntityVersion.models"
import { AppEntityVersionSearchParameters } from "./appEntityVersion.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appEntityVersion")
export class AppEntityVersionQueryBuilder extends NestTypeOrmQueryBuilder<
  AppEntityVersionEntity,
  AppEntityVersionEntityId,
  AppEntityVersionSearchParameters,
  AppEntityVersionSorting,
  AppEntityVersionFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEntityVersion", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppEntityVersionEntity> | FindOptionsWhere<AppEntityVersionEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppEntityVersionSearchParameters
  ): FindOptionsOrder<AppEntityVersionEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppEntityVersionSearchParameters
  ): FindOptionsWhere<AppEntityVersionEntity> | FindOptionsWhere<AppEntityVersionEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppEntityVersionSearchParameters,
    context?: AppAuthContext
  ): Promise<AppEntityVersionFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
