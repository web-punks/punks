import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppEntityVersionEntity } from "../../database/core/entities/appEntityVersion.entity"
import { AppEntityVersionCreateData, AppEntityVersionUpdateData } from "./appEntityVersion.models"

@WpEntityAdapter("appEntityVersion")
export class AppEntityVersionAdapter
  implements
    IEntityAdapter<
      AppEntityVersionEntity,
      AppEntityVersionCreateData,
      AppEntityVersionUpdateData
    >
{
  createDataToEntity(data: AppEntityVersionCreateData): DeepPartial<AppEntityVersionEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppEntityVersionUpdateData): DeepPartial<AppEntityVersionEntity> {
    return {
      ...data,
    }
  }
}
