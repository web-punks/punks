import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppEntityVersionAdapter } from "./appEntityVersion.adapter"
import { AppEntityVersionAuthMiddleware } from "./appEntityVersion.authentication"
import { AppEntityVersionEntityManager } from "./appEntityVersion.manager"
import { AppEntityVersionQueryBuilder } from "./appEntityVersion.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppEntityVersionAdapter,
    AppEntityVersionAuthMiddleware,
    AppEntityVersionEntityManager,
    AppEntityVersionQueryBuilder,
  ],
  exports: [AppEntityVersionEntityManager],
})
export class AppEntityVersionEntityModule {}
