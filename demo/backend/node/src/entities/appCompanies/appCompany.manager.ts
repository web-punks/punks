import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppCompanyCreateData,
  AppCompanyCursor,
  AppCompanyFacets,
  AppCompanySorting,
  AppCompanyUpdateData,
} from "./appCompany.models"
import {
  AppCompanySearchParameters,
  AppCompanyDeleteParameters,
} from "./appCompany.types"
import {
  AppCompanyEntity,
  AppCompanyEntityId,
} from "../../database/core/entities/appCompany.entity"

@WpEntityManager("appCompany")
export class AppCompanyEntityManager extends NestEntityManager<
  AppCompanyEntity,
  AppCompanyEntityId,
  AppCompanyCreateData,
  AppCompanyUpdateData,
  AppCompanyDeleteParameters,
  AppCompanySearchParameters,
  AppCompanySorting,
  AppCompanyCursor,
  AppCompanyFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appCompany", registry)
  }
}
