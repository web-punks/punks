import {
  FindOptionsOrder,
  FindOptionsRelations,
  FindOptionsWhere,
  In,
} from "typeorm"
import {
  EntityManagerRegistry,
  IAuthenticationContext,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppCompanyEntity,
  AppCompanyEntityId,
} from "../../database/core/entities/appCompany.entity"
import { AppCompanyFacets, AppCompanySorting } from "./appCompany.models"
import { AppCompanySearchParameters } from "./appCompany.types"
import {
  AppUserContext,
  AppAuthContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appCompany")
export class AppCompanyQueryBuilder extends NestTypeOrmQueryBuilder<
  AppCompanyEntity,
  AppCompanyEntityId,
  AppCompanySearchParameters,
  AppCompanySorting,
  AppCompanyFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appCompany", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppCompanyEntity> | FindOptionsWhere<AppCompanyEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppCompanySearchParameters
  ): FindOptionsOrder<AppCompanyEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppCompanySearchParameters
  ): FindOptionsWhere<AppCompanyEntity> | FindOptionsWhere<AppCompanyEntity>[] {
    return {
      ...(request.filters?.id ? { id: In(request.filters.id) } : {}),
      ...(request.filters?.uid ? { uid: In(request.filters.uid) } : {}),
      ...(request.filters?.organizationId
        ? { uid: In(request.filters.organizationId) }
        : {}),
      ...(request.filters?.tenantId
        ? { uid: In(request.filters.tenantId) }
        : {}),
    }
  }

  protected calculateFacets(
    request: AppCompanySearchParameters,
    context?: AppAuthContext
  ): Promise<AppCompanyFacets> {
    return Promise.resolve({})
  }

   protected getRelationsToLoad(
    request: AppCompanySearchParameters,
    context: IAuthenticationContext<AppUserContext>
  ): FindOptionsRelations<AppCompanyEntity> {
    return {
      organization: true,
    }
  }
}
