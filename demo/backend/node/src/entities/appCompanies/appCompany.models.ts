import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppCompanyEntity } from "../../database/core/entities/appCompany.entity"

export type AppCompanyCreateData = DeepPartial<Omit<AppCompanyEntity, "id">>
export type AppCompanyUpdateData = DeepPartial<Omit<AppCompanyEntity, "id">>

export enum AppCompanySorting {
  Name = "Name",
}

export type AppCompanyCursor = number

export class AppCompanySearchFilters {
  @ApiProperty({ required: false, type: [String] })
  id?: string[]

  @ApiProperty({ required: false, type: [String] })
  uid?: string[]

  @ApiProperty({ required: false, type: [String] })
  organizationId?: string[]

  @ApiProperty({ required: false, type: [String] })
  tenantId?: string[]
}

export class AppCompanyFacets {}

export type AppCompanySheetItem = AppCompanyEntity
