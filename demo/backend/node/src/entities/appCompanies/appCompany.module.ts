import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppCompanyAdapter } from "./appCompany.adapter"
import { AppCompanyAuthMiddleware } from "./appCompany.authentication"
import { AppCompanyEntityManager } from "./appCompany.manager"
import { AppCompanyQueryBuilder } from "./appCompany.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppCompanyAdapter,
    AppCompanyAuthMiddleware,
    AppCompanyEntityManager,
    AppCompanyQueryBuilder,
  ],
  exports: [AppCompanyEntityManager],
})
export class AppCompanyEntityModule {}
