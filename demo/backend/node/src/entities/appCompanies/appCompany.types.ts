import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppCompanyEntity } from "../../database/core/entities/appCompany.entity"
import {
  AppCompanySorting,
  AppCompanyCursor,
  AppCompanySearchFilters,
  AppCompanyFacets,
} from "./appCompany.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppCompanySearchSortingField
  implements ISearchSortingField<AppCompanySorting>
{
  @ApiProperty({ enum: AppCompanySorting })
  field: AppCompanySorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppCompanyQuerySorting
  implements ISearchSorting<AppCompanySorting>
{
  @ApiProperty({ required: false, type: [AppCompanySearchSortingField] })
  fields: AppCompanySearchSortingField[]
}

export class AppCompanyQueryPaging
  implements ISearchRequestPaging<AppCompanyCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppCompanyCursor

  @ApiProperty()
  pageSize: number
}

export class AppCompanySearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppCompanyFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppCompanySearchParameters
  implements
    IEntitySearchParameters<
      AppCompanyEntity,
      AppCompanySorting,
      AppCompanyCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppCompanyFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppCompanySearchFilters

  @ApiProperty({ required: false })
  sorting?: AppCompanyQuerySorting

  @ApiProperty({ required: false })
  paging?: AppCompanyQueryPaging

  @ApiProperty({ required: false })
  options?: AppCompanySearchOptions

  relations?: ISearchQueryRelations<AppCompanyEntity>
}

export class AppCompanySearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppCompanyCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppCompanyCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppCompanyCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppCompanyCursor
}

export class AppCompanySearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppCompanyEntity,
      AppCompanySearchParameters,
      TResult,
      AppCompanySorting,
      AppCompanyCursor,
      AppCompanyFacets
    >
{
  @ApiProperty()
  request: AppCompanySearchParameters

  @ApiProperty()
  facets?: AppCompanyFacets

  @ApiProperty()
  paging?: AppCompanySearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppCompanyDeleteParameters
  implements IEntitiesDeleteParameters<AppCompanySorting>
{
  @ApiProperty({ required: false })
  filters?: AppCompanySearchFilters

  @ApiProperty({ required: false })
  sorting?: AppCompanyQuerySorting
}

export class AppCompanyVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppCompanyCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppCompanyCursor>
}

export class AppCompanyVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppCompanyCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppCompanyCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppCompanyCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppCompanyCursor
}

export class AppCompanyVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppCompanyCursor>
{
  @ApiProperty()
  paging?: AppCompanyVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
