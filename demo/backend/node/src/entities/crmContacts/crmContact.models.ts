import { DeepPartial } from "@punks/backend-entity-manager"
import { CrmContactEntity } from "../../database/core/entities/crmContact.entity"

export type CrmContactEntityId = string

export type CrmContactCreateData = DeepPartial<Omit<CrmContactEntity, "id">>
export type CrmContactUpdateData = DeepPartial<Omit<CrmContactEntity, "id">>

export enum CrmContactSorting {
  Name = "Name",
}

export type CrmContactCursor = number

export class CrmContactSearchFilters {}

export class CrmContactFacets {}

export type CrmContactSheetItem = CrmContactEntity
