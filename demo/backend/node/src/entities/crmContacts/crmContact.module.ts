import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { CrmContactAdapter } from "./crmContact.adapter"
import { CrmContactAuthMiddleware } from "./crmContact.authentication"
import { CrmContactEntityManager } from "./crmContact.manager"
import { CrmContactQueryBuilder } from "./crmContact.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    CrmContactAdapter,
    CrmContactAuthMiddleware,
    CrmContactEntityManager,
    CrmContactQueryBuilder,
  ],
  exports: [CrmContactEntityManager],
})
export class CrmContactEntityModule {}
