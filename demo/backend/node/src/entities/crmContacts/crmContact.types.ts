import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import {
  CrmContactSorting,
  CrmContactCursor,
  CrmContactSearchFilters,
  CrmContactFacets,
} from "./crmContact.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"
import { CrmContactEntity } from "@/database/core/entities/crmContact.entity"

export class CrmContactSearchSortingField
  implements ISearchSortingField<CrmContactSorting>
{
  @ApiProperty({ enum: CrmContactSorting })
  field: CrmContactSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class CrmContactQuerySorting
  implements ISearchSorting<CrmContactSorting>
{
  @ApiProperty({ required: false, type: [CrmContactSearchSortingField] })
  fields: CrmContactSearchSortingField[]
}

export class CrmContactQueryPaging
  implements ISearchRequestPaging<CrmContactCursor>
{
  @ApiProperty({ required: false })
  cursor?: CrmContactCursor

  @ApiProperty()
  pageSize: number
}

export class CrmContactSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class CrmContactFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class CrmContactSearchParameters
  implements
    IEntitySearchParameters<
      CrmContactEntity,
      CrmContactSorting,
      CrmContactCursor
    >
{
  @ApiProperty({ required: false })
  query?: CrmContactFullTextQuery

  @ApiProperty({ required: false })
  filters?: CrmContactSearchFilters

  @ApiProperty({ required: false })
  sorting?: CrmContactQuerySorting

  @ApiProperty({ required: false })
  paging?: CrmContactQueryPaging

  @ApiProperty({ required: false })
  options?: CrmContactSearchOptions
}

export class CrmContactSearchResultsPaging
  implements IEntitiesSearchResultsPaging<CrmContactCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: CrmContactCursor

  @ApiProperty({ required: false })
  currentPageCursor?: CrmContactCursor

  @ApiProperty({ required: false })
  prevPageCursor?: CrmContactCursor
}

export class CrmContactSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      CrmContactEntity,
      CrmContactSearchParameters,
      TResult,
      CrmContactSorting,
      CrmContactCursor,
      CrmContactFacets
    >
{
  @ApiProperty()
  request: CrmContactSearchParameters

  @ApiProperty()
  facets?: CrmContactFacets

  @ApiProperty()
  paging?: CrmContactSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class CrmContactDeleteParameters
  implements IEntitiesDeleteParameters<CrmContactSorting>
{
  @ApiProperty({ required: false })
  filters?: CrmContactSearchFilters

  @ApiProperty({ required: false })
  sorting?: CrmContactQuerySorting
}

export class CrmContactVersionsSearchParameters
  implements IEntityVersionsSearchParameters<CrmContactCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<CrmContactCursor>
}

export class CrmContactVersionsResultsPaging
  implements IEntityVersionsResultsPaging<CrmContactCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: CrmContactCursor

  @ApiProperty({ required: false })
  currentPageCursor?: CrmContactCursor

  @ApiProperty({ required: false })
  prevPageCursor?: CrmContactCursor
}

export class CrmContactVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, CrmContactCursor>
{
  @ApiProperty()
  paging?: CrmContactVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
