import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppUserProfileEntity } from "../../database/core/entities/appUserProfile.entity"
import {
  AppUserProfileSorting,
  AppUserProfileCursor,
  AppUserProfileSearchFilters,
  AppUserProfileFacets,
} from "./appUserProfile.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppUserProfileSearchSortingField
  implements ISearchSortingField<AppUserProfileSorting>
{
  @ApiProperty({ enum: AppUserProfileSorting })
  field: AppUserProfileSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppUserProfileQuerySorting
  implements ISearchSorting<AppUserProfileSorting>
{
  @ApiProperty({ required: false, type: [AppUserProfileSearchSortingField] })
  fields: AppUserProfileSearchSortingField[]
}

export class AppUserProfileQueryPaging
  implements ISearchRequestPaging<AppUserProfileCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppUserProfileCursor

  @ApiProperty()
  pageSize: number
}

export class AppUserProfileSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppUserProfileFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppUserProfileSearchParameters
  implements
    IEntitySearchParameters<
      AppUserProfileEntity,
      AppUserProfileSorting,
      AppUserProfileCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppUserProfileFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppUserProfileSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserProfileQuerySorting

  @ApiProperty({ required: false })
  paging?: AppUserProfileQueryPaging

  @ApiProperty({ required: false })
  options?: AppUserProfileSearchOptions

  relations?: ISearchQueryRelations<AppUserProfileEntity>
}

export class AppUserProfileSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppUserProfileCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserProfileCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserProfileCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserProfileCursor
}

export class AppUserProfileSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppUserProfileEntity,
      AppUserProfileSearchParameters,
      TResult,
      AppUserProfileSorting,
      AppUserProfileCursor,
      AppUserProfileFacets
    >
{
  @ApiProperty()
  request: AppUserProfileSearchParameters

  @ApiProperty()
  facets?: AppUserProfileFacets

  @ApiProperty()
  paging?: AppUserProfileSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppUserProfileDeleteParameters
  implements IEntitiesDeleteParameters<AppUserProfileSorting>
{
  @ApiProperty({ required: false })
  filters?: AppUserProfileSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserProfileQuerySorting
}

export class AppUserProfileVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppUserProfileCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppUserProfileCursor>
}

export class AppUserProfileVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppUserProfileCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserProfileCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserProfileCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserProfileCursor
}

export class AppUserProfileVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppUserProfileCursor>
{
  @ApiProperty()
  paging?: AppUserProfileVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
