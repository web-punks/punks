import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppUserProfileEntity } from "../../database/core/entities/appUserProfile.entity"
import { AppUserProfileCreateData, AppUserProfileUpdateData } from "./appUserProfile.models"

@WpEntityAdapter("appUserProfile")
export class AppUserProfileAdapter
  implements
    IEntityAdapter<
      AppUserProfileEntity,
      AppUserProfileCreateData,
      AppUserProfileUpdateData
    >
{
  createDataToEntity(data: AppUserProfileCreateData): DeepPartial<AppUserProfileEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppUserProfileUpdateData): DeepPartial<AppUserProfileEntity> {
    return {
      ...data,
    }
  }
}
