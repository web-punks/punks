import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppUserProfileEntity,
  AppUserProfileEntityId,
} from "../../database/core/entities/appUserProfile.entity"
import {
  AppUserProfileFacets,
  AppUserProfileSorting,
} from "./appUserProfile.models"
import { AppUserProfileSearchParameters } from "./appUserProfile.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appUserProfile")
export class AppUserProfileQueryBuilder extends NestTypeOrmQueryBuilder<
  AppUserProfileEntity,
  AppUserProfileEntityId,
  AppUserProfileSearchParameters,
  AppUserProfileSorting,
  AppUserProfileFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserProfile", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppUserProfileEntity> | FindOptionsWhere<AppUserProfileEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppUserProfileSearchParameters
  ): FindOptionsOrder<AppUserProfileEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppUserProfileSearchParameters
  ): FindOptionsWhere<AppUserProfileEntity> | FindOptionsWhere<AppUserProfileEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppUserProfileSearchParameters,
    context?: AppAuthContext
  ): Promise<AppUserProfileFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
