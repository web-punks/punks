import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppUserProfileCreateData,
  AppUserProfileCursor,
  AppUserProfileFacets,
  AppUserProfileSorting,
  AppUserProfileUpdateData,
} from "./appUserProfile.models"
import {
  AppUserProfileSearchParameters,
  AppUserProfileDeleteParameters,
} from "./appUserProfile.types"
import {
  AppUserProfileEntity,
  AppUserProfileEntityId,
} from "../../database/core/entities/appUserProfile.entity"

@WpEntityManager("appUserProfile")
export class AppUserProfileEntityManager extends NestEntityManager<
  AppUserProfileEntity,
  AppUserProfileEntityId,
  AppUserProfileCreateData,
  AppUserProfileUpdateData,
  AppUserProfileDeleteParameters,
  AppUserProfileSearchParameters,
  AppUserProfileSorting,
  AppUserProfileCursor,
  AppUserProfileFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserProfile", registry)
  }
}
