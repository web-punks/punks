import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppUserProfileEntity } from "../../database/core/entities/appUserProfile.entity"

export type AppUserProfileCreateData = DeepPartial<Omit<AppUserProfileEntity, "id">>
export type AppUserProfileUpdateData = DeepPartial<Omit<AppUserProfileEntity, "id">>

export enum AppUserProfileSorting {
  Name = "Name",
}

export type AppUserProfileCursor = number

export class AppUserProfileSearchFilters {}

export class AppUserProfileFacets {}

export type AppUserProfileSheetItem = AppUserProfileEntity
