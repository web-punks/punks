import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppUserEntity } from "../../database/core/entities/appUser.entity"
import { AppUserCreateData, AppUserUpdateData } from "./appUser.models"

@WpEntityAdapter("appUser")
export class AppUserAdapter
  implements
    IEntityAdapter<
      AppUserEntity,
      AppUserCreateData,
      AppUserUpdateData
    >
{
  createDataToEntity(data: AppUserCreateData): DeepPartial<AppUserEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppUserUpdateData): DeepPartial<AppUserEntity> {
    return {
      ...data,
    }
  }
}
