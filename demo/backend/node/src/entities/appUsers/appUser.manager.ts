import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppUserCreateData,
  AppUserCursor,
  AppUserFacets,
  AppUserSorting,
  AppUserUpdateData,
} from "./appUser.models"
import {
  AppUserSearchParameters,
  AppUserDeleteParameters,
} from "./appUser.types"
import {
  AppUserEntity,
  AppUserEntityId,
} from "../../database/core/entities/appUser.entity"

@WpEntityManager("appUser")
export class AppUserEntityManager extends NestEntityManager<
  AppUserEntity,
  AppUserEntityId,
  AppUserCreateData,
  AppUserUpdateData,
  AppUserDeleteParameters,
  AppUserSearchParameters,
  AppUserSorting,
  AppUserCursor,
  AppUserFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUser", registry)
  }
}
