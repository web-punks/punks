import {
  FindOptionsOrder,
  FindOptionsWhere,
  FindOptionsRelations,
} from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppUserEntity,
  AppUserEntityId,
} from "../../database/core/entities/appUser.entity"
import { AppUserFacets, AppUserSorting } from "./appUser.models"
import { AppUserSearchParameters } from "./appUser.types"
import {
  AppUserContext,
  AppAuthContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appUser")
export class AppUserQueryBuilder extends NestTypeOrmQueryBuilder<
  AppUserEntity,
  AppUserEntityId,
  AppUserSearchParameters,
  AppUserSorting,
  AppUserFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUser", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppUserEntity> | FindOptionsWhere<AppUserEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppUserSearchParameters
  ): FindOptionsOrder<AppUserEntity> {
    switch (request.sorting?.fields[0]?.field) {
      case AppUserSorting.Name:
        return {
          email: request.sorting?.fields[0].direction ?? "ASC",
        }
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppUserSearchParameters
  ): FindOptionsWhere<AppUserEntity> | FindOptionsWhere<AppUserEntity>[] {
    return {
      ...(request.filters?.id ? { id: request.filters.id } : {}),
      ...(request.filters?.email ? { email: request.filters.email } : {}),
      ...(request.filters?.userName
        ? { userName: request.filters.userName }
        : {}),
      ...(request.filters?.organizationId
        ? {
            organization: {
              id: request.filters.organizationId,
            },
          }
        : {}),
      ...(request.filters?.organizationUid
        ? {
            organization: {
              uid: request.filters.organizationUid,
            },
          }
        : {}),
    }
  }

  protected calculateFacets(
    request: AppUserSearchParameters,
    context?: AppAuthContext
  ): Promise<AppUserFacets | undefined> {
    return Promise.resolve(undefined)
  }

  protected getRelationsToLoad(
    request: AppUserSearchParameters,
    context?: AppAuthContext | undefined
  ): FindOptionsRelations<AppUserEntity> | undefined {
    return {
      organizationalUnit: true,
    }
  }
}
