import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppUserEntity } from "../../database/core/entities/appUser.entity"

export type AppUserEntityId = string

export type AppUserCreateData = DeepPartial<Omit<AppUserEntity, "id">>
export type AppUserUpdateData = DeepPartial<Omit<AppUserEntity, "id">>

export enum AppUserSorting {
  Name = "Name",
}

export class AppUserFacets {}

export class AppUserSearchFilters {
  @ApiProperty({ required: false })
  id?: string

  @ApiProperty({ required: false })
  email?: string

  @ApiProperty({ required: false })
  userName?: string

  @ApiProperty({ required: false })
  organizationId?: string

  @ApiProperty({ required: false })
  organizationUid?: string

  @ApiProperty({ required: false })
  tenantId?: string

  @ApiProperty({ required: false })
  tenantUid?: string

  @ApiProperty({ required: false })
  directoryId?: string

  @ApiProperty({ required: false })
  directoryUid?: string
}

export type AppUserCursor = number

export type AppUserSheetItem = AppUserEntity
