import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppUserEntity } from "../../database/core/entities/appUser.entity"
import {
  AppUserSorting,
  AppUserCursor,
  AppUserSearchFilters,
  AppUserFacets,
} from "./appUser.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppUserSearchSortingField
  implements ISearchSortingField<AppUserSorting>
{
  @ApiProperty({ enum: AppUserSorting })
  field: AppUserSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppUserQuerySorting implements ISearchSorting<AppUserSorting> {
  @ApiProperty({ required: false, type: [AppUserSearchSortingField] })
  fields: AppUserSearchSortingField[]
}

export class AppUserQueryPaging implements ISearchRequestPaging<AppUserCursor> {
  @ApiProperty({ required: false })
  cursor?: AppUserCursor

  @ApiProperty()
  pageSize: number
}

export class AppUserSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean

  @ApiProperty({ required: false })
  facetsFilters?: AppUserSearchFilters
}

export class AppUserFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppUserSearchParameters
  implements
    IEntitySearchParameters<AppUserEntity, AppUserSorting, AppUserCursor>
{
  @ApiProperty({ required: false })
  query?: AppUserFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppUserSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserQuerySorting

  @ApiProperty({ required: false })
  paging?: AppUserQueryPaging

  @ApiProperty({ required: false })
  options?: AppUserSearchOptions

  relations?: ISearchQueryRelations<AppUserEntity>
}

export class AppUserSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppUserCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserCursor
}

export class AppUserSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppUserEntity,
      AppUserSearchParameters,
      TResult,
      AppUserSorting,
      AppUserCursor,
      AppUserFacets
    >
{
  @ApiProperty()
  request: AppUserSearchParameters

  @ApiProperty()
  facets?: AppUserFacets

  @ApiProperty()
  paging?: AppUserSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppUserDeleteParameters
  implements IEntitiesDeleteParameters<AppUserSorting>
{
  @ApiProperty({ required: false })
  filters?: AppUserSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserQuerySorting
}

export class AppUserVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppUserCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppUserCursor>
}

export class AppUserVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppUserCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserCursor
}

export class AppUserVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppUserCursor>
{
  @ApiProperty()
  paging?: AppUserVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
