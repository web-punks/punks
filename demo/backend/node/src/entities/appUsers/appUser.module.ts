import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppUserAdapter } from "./appUser.adapter"
import { AppUserAuthMiddleware } from "./appUser.authentication"
import { AppUserEntityManager } from "./appUser.manager"
import { AppUserQueryBuilder } from "./appUser.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppUserAdapter,
    AppUserAuthMiddleware,
    AppUserEntityManager,
    AppUserQueryBuilder,
  ],
  exports: [AppUserEntityManager],
})
export class AppUserEntityModule {}
