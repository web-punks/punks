import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppTenantAdapter } from "./appTenant.adapter"
import { AppTenantAuthMiddleware } from "./appTenant.authentication"
import { AppTenantEntityManager } from "./appTenant.manager"
import { AppTenantQueryBuilder } from "./appTenant.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppTenantAdapter,
    AppTenantAuthMiddleware,
    AppTenantEntityManager,
    AppTenantQueryBuilder,
  ],
  exports: [AppTenantEntityManager],
})
export class AppTenantEntityModule {}
