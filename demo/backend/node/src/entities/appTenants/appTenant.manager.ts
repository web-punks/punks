import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppTenantCreateData,
  AppTenantCursor,
  AppTenantFacets,
  AppTenantSorting,
  AppTenantUpdateData,
} from "./appTenant.models"
import {
  AppTenantSearchParameters,
  AppTenantDeleteParameters,
} from "./appTenant.types"
import {
  AppTenantEntity,
  AppTenantEntityId,
} from "../../database/core/entities/appTenant.entity"

@WpEntityManager("appTenant")
export class AppTenantEntityManager extends NestEntityManager<
  AppTenantEntity,
  AppTenantEntityId,
  AppTenantCreateData,
  AppTenantUpdateData,
  AppTenantDeleteParameters,
  AppTenantSearchParameters,
  AppTenantSorting,
  AppTenantCursor,
  AppTenantFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appTenant", registry)
  }
}
