import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppTenantEntity } from "../../database/core/entities/appTenant.entity"
import {
  AppTenantSorting,
  AppTenantCursor,
  AppTenantSearchFilters,
  AppTenantFacets,
} from "./appTenant.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppTenantSearchSortingField
  implements ISearchSortingField<AppTenantSorting>
{
  @ApiProperty({ enum: AppTenantSorting })
  field: AppTenantSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppTenantQuerySorting implements ISearchSorting<AppTenantSorting> {
  @ApiProperty({ required: false, type: [AppTenantSearchSortingField] })
  fields: AppTenantSearchSortingField[]
}

export class AppTenantQueryPaging
  implements ISearchRequestPaging<AppTenantCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppTenantCursor

  @ApiProperty()
  pageSize: number
}

export class AppTenantSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppTenantFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppTenantSearchParameters
  implements
    IEntitySearchParameters<AppTenantEntity, AppTenantSorting, AppTenantCursor>
{
  @ApiProperty({ required: false })
  query?: AppTenantFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppTenantSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppTenantQuerySorting

  @ApiProperty({ required: false })
  paging?: AppTenantQueryPaging

  @ApiProperty({ required: false })
  options?: AppTenantSearchOptions

  relations?: ISearchQueryRelations<AppTenantEntity>
}

export class AppTenantSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppTenantCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppTenantCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppTenantCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppTenantCursor
}

export class AppTenantSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppTenantEntity,
      AppTenantSearchParameters,
      TResult,
      AppTenantSorting,
      AppTenantCursor,
      AppTenantFacets
    >
{
  @ApiProperty()
  request: AppTenantSearchParameters

  @ApiProperty()
  facets?: AppTenantFacets

  @ApiProperty()
  paging?: AppTenantSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppTenantDeleteParameters
  implements IEntitiesDeleteParameters<AppTenantSorting>
{
  @ApiProperty({ required: false })
  filters?: AppTenantSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppTenantQuerySorting
}

export class AppTenantVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppTenantCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppTenantCursor>
}

export class AppTenantVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppTenantCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppTenantCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppTenantCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppTenantCursor
}

export class AppTenantVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppTenantCursor>
{
  @ApiProperty()
  paging?: AppTenantVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
