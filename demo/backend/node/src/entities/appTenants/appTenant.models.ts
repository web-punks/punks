import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppTenantEntity } from "../../database/core/entities/appTenant.entity"

export type AppTenantCreateData = DeepPartial<Omit<AppTenantEntity, "id">>
export type AppTenantUpdateData = DeepPartial<Omit<AppTenantEntity, "id">>

export enum AppTenantSorting {
  Name = "Name",
}

export type AppTenantCursor = number

export class AppTenantSearchFilters {}

export class AppTenantFacets {}

export type AppTenantSheetItem = AppTenantEntity
