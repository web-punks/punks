import { FindOptionsOrder, FindOptionsWhere, In, IsNull } from "typeorm"
import {
  IAuthenticationContext,
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import { FooEntity } from "../../database/core/entities/foo.entity"
import { FooEntityId, FooFacets, FooSorting } from "./foo.models"
import { FooSearchParameters } from "./foo.types"
import { AppAuthContext } from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("foo")
export class FooQueryBuilder extends NestTypeOrmQueryBuilder<
  FooEntity,
  FooEntityId,
  FooSearchParameters,
  FooSorting,
  FooFacets,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("foo", registry)
  }

  protected buildContextFilter(
    context?: IAuthenticationContext<AppAuthContext>
  ): FindOptionsWhere<FooEntity> | FindOptionsWhere<FooEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: FooSearchParameters
  ): FindOptionsOrder<FooEntity> {
    switch (request.sorting?.fields[0]?.field) {
      case FooSorting.Name:
      default:
        return {
          name: {
            direction: request.sorting?.fields[0].direction ?? "ASC",
            modifier: "lower",
          },
        }
    }
  }

  protected buildWhereClause(
    request: FooSearchParameters
  ): FindOptionsWhere<FooEntity> | FindOptionsWhere<FooEntity>[] {
    return {
      ...(!!request.traverse?.rootOnly
        ? {
            parent: IsNull(),
          }
        : {}),
      ...(!!request.traverse?.parentIds
        ? {
            parent: {
              id: In(request.traverse.parentIds),
            },
          }
        : {}),
      ...(!!request.filters?.age
        ? {
            age: this.clause.numericFilter(request.filters.age),
          }
        : {}),
      ...(!!request.filters?.name
        ? {
            name: this.clause.stringFilter(request.filters.name),
          }
        : {}),
      ...(!!request.filters?.uid
        ? {
            uid: this.clause.stringFilter(request.filters.uid),
          }
        : {}),
      ...(!!request.filters?.other
        ? {
            other: this.clause.stringFilter(request.filters.other),
          }
        : {}),
      ...(!!request.filters?.enabled
        ? {
            enabled: this.clause.boolFilter(request.filters.enabled),
          }
        : {}),
    }
  }

  protected async calculateFacets(
    request: FooSearchParameters,
    context?: IAuthenticationContext<AppAuthContext>
  ): Promise<FooFacets> {
    return {
      age: await this.calculateFacet({ field: "age", request, context }),
      type: await this.calculateFacet({ field: "type", request, context }),
      enabled: await this.calculateFacet({
        field: "enabled",
        request,
        context,
      }),
    }
  }

  protected async calculateChildrenMap(
    items: FooEntity[]
  ): Promise<{ [x: string]: { count: number } }> {
    return await this.queryChildrenMap({
      idField: "id",
      parentField: "parent",
      nodeIds: items.map((item) => item.id),
    })
  }
}
