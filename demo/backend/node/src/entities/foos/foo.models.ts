import { DeepPartial } from "@punks/backend-entity-manager"
import { FooEntity } from "../../database/core/entities/foo.entity"
import { ApiProperty } from "@nestjs/swagger"
import {
  BooleanFilter,
  IdFilter,
  NumericFilter,
  StringFilter,
} from "@/shared/api/fields"
import { BooleanFacet, NumericFacet, StringFacet } from "@/shared/api/facets"

export type FooEntityId = string

export type FooCreateData = DeepPartial<Omit<FooEntity, "id">>
export type FooUpdateData = DeepPartial<Omit<FooEntity, "id">>

export enum FooSorting {
  Name = "Name",
}

export type FooCursor = number

export class FooSearchFilters {
  @ApiProperty({ required: false })
  age?: NumericFilter

  @ApiProperty({ required: false })
  name?: StringFilter

  @ApiProperty({ required: false })
  uid?: StringFilter

  @ApiProperty({ required: false })
  type?: IdFilter

  @ApiProperty({ required: false })
  other?: StringFilter

  @ApiProperty({ required: false })
  enabled?: BooleanFilter
}

export class FooFacets {
  @ApiProperty()
  age: NumericFacet

  @ApiProperty()
  type: StringFacet

  @ApiProperty()
  enabled: BooleanFacet
}

export type FooSheetItem = FooEntity
