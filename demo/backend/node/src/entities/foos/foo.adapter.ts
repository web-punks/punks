import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { FooEntity } from "../../database/core/entities/foo.entity"
import { FooCreateData, FooUpdateData } from "./foo.models"

@WpEntityAdapter("foo")
export class FooAdapter
  implements IEntityAdapter<FooEntity, FooCreateData, FooUpdateData>
{
  createDataToEntity(data: FooCreateData): DeepPartial<FooEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: FooUpdateData): DeepPartial<FooEntity> {
    return {
      ...data,
    }
  }
}
