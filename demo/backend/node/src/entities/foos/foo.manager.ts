import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  FooCreateData,
  FooCursor,
  FooEntityId,
  FooFacets,
  FooSorting,
  FooUpdateData,
} from "./foo.models"
import {
  FooSearchParameters,
  FooDeleteParameters,
} from "./foo.types"
import { FooEntity } from "../../database/core/entities/foo.entity"

@WpEntityManager("foo")
export class FooEntityManager extends NestEntityManager<
  FooEntity,
  FooEntityId,
  FooCreateData,
  FooUpdateData,
  FooDeleteParameters,
  FooSearchParameters,
  FooSorting,
  FooCursor,
  FooFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("foo", registry)
  }
}
