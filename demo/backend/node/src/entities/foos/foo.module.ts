import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { FooAdapter } from "./foo.adapter"
import { FooAuthMiddleware } from "./foo.authentication"
import { FooEntityManager } from "./foo.manager"
import { FooQueryBuilder } from "./foo.query"

@Module({
  imports: [EntityManagerModule],
  providers: [FooAdapter, FooAuthMiddleware, FooEntityManager, FooQueryBuilder],
  exports: [FooEntityManager],
})
export class FooEntityModule {}
