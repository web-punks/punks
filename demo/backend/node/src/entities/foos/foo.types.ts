import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchParameters,
  IEntityVersionsSearchResults,
  IFullTextQuery,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import {
  FooSorting,
  FooCursor,
  FooSearchFilters,
  FooFacets,
} from "./foo.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"
import { FooEntity } from "@/database/core/entities/foo.entity"
import { EntitiesTraverseFilters } from "@/shared/api/traverse"

export class FooSearchSortingField implements ISearchSortingField<FooSorting> {
  @ApiProperty({ enum: FooSorting })
  field: FooSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class FooQuerySorting implements ISearchSorting<FooSorting> {
  @ApiProperty({ required: false, type: [FooSearchSortingField] })
  fields: FooSearchSortingField[]
}

export class FooQueryPaging implements ISearchRequestPaging<FooCursor> {
  @ApiProperty({ required: false })
  cursor?: FooCursor

  @ApiProperty()
  pageSize: number
}

export class FooSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean

  @ApiProperty({ required: false })
  includeChildrenMap?: boolean

  @ApiProperty({ required: false })
  facetsFilters?: FooSearchFilters
}

export class FooFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class FooSearchParameters
  implements IEntitySearchParameters<FooEntity, FooSorting, FooCursor>
{
  @ApiProperty({ required: false })
  query?: FooFullTextQuery

  @ApiProperty({ required: false })
  filters?: FooSearchFilters

  @ApiProperty({ required: false })
  traverse?: EntitiesTraverseFilters

  @ApiProperty({ required: false })
  sorting?: FooQuerySorting

  @ApiProperty({ required: false })
  paging?: FooQueryPaging

  @ApiProperty({ required: false })
  options?: FooSearchOptions
}

export class FooSearchResultsPaging
  implements IEntitiesSearchResultsPaging<FooCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: FooCursor

  @ApiProperty({ required: false })
  currentPageCursor?: FooCursor

  @ApiProperty({ required: false })
  prevPageCursor?: FooCursor
}

export class FooSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      FooEntity,
      FooSearchParameters,
      TResult,
      FooSorting,
      FooCursor,
      FooFacets
    >
{
  @ApiProperty()
  request: FooSearchParameters

  @ApiProperty()
  facets?: FooFacets

  @ApiProperty()
  paging?: FooSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class FooDeleteParameters
  implements IEntitiesDeleteParameters<FooSorting>
{
  @ApiProperty({ required: false })
  filters?: FooSearchFilters

  @ApiProperty({ required: false })
  sorting?: FooQuerySorting
}

export class FooVersionsSearchParameters
  implements IEntityVersionsSearchParameters<FooCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<FooCursor>
}

export class FooVersionsResultsPaging
  implements IEntityVersionsResultsPaging<FooCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: FooCursor

  @ApiProperty({ required: false })
  currentPageCursor?: FooCursor

  @ApiProperty({ required: false })
  prevPageCursor?: FooCursor
}

export class FooVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, FooCursor>
{
  @ApiProperty()
  paging?: FooVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
