import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppLanguageAdapter } from "./appLanguage.adapter"
import { AppLanguageAuthMiddleware } from "./appLanguage.authentication"
import { AppLanguageEntityManager } from "./appLanguage.manager"
import { AppLanguageQueryBuilder } from "./appLanguage.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppLanguageAdapter,
    AppLanguageAuthMiddleware,
    AppLanguageEntityManager,
    AppLanguageQueryBuilder,
  ],
  exports: [AppLanguageEntityManager],
})
export class AppLanguageEntityModule {}
