import { DeepPartial } from "@punks/backend-entity-manager"
import { AppLanguageEntity } from "@/database/core/entities/appLanguage.entity"
import { StringFilter } from "@/shared/api/fields"
import { ApiProperty } from "@nestjs/swagger"

export type AppLanguageCreateData = DeepPartial<Omit<AppLanguageEntity, "id">>
export type AppLanguageUpdateData = DeepPartial<Omit<AppLanguageEntity, "id">>

export enum AppLanguageSorting {
  Name = "Name",
}

export type AppLanguageCursor = number

export class AppLanguageSearchFilters {
  @ApiProperty({ required: false })
  uid?: StringFilter
}

export class AppLanguageFacets {}

export type AppLanguageSheetItem = AppLanguageEntity
