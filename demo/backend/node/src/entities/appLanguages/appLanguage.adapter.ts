import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppLanguageEntity } from "../../database/core/entities/appLanguage.entity"
import { AppLanguageCreateData, AppLanguageUpdateData } from "./appLanguage.models"

@WpEntityAdapter("appLanguage")
export class AppLanguageAdapter
  implements
    IEntityAdapter<
      AppLanguageEntity,
      AppLanguageCreateData,
      AppLanguageUpdateData
    >
{
  createDataToEntity(data: AppLanguageCreateData): DeepPartial<AppLanguageEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppLanguageUpdateData): DeepPartial<AppLanguageEntity> {
    return {
      ...data,
    }
  }
}
