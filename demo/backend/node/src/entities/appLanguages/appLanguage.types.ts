import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppLanguageEntity } from "../../database/core/entities/appLanguage.entity"
import {
  AppLanguageSorting,
  AppLanguageCursor,
  AppLanguageSearchFilters,
  AppLanguageFacets,
} from "./appLanguage.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppLanguageSearchSortingField
  implements ISearchSortingField<AppLanguageSorting>
{
  @ApiProperty({ enum: AppLanguageSorting })
  field: AppLanguageSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppLanguageQuerySorting
  implements ISearchSorting<AppLanguageSorting>
{
  @ApiProperty({ required: false, type: [AppLanguageSearchSortingField] })
  fields: AppLanguageSearchSortingField[]
}

export class AppLanguageQueryPaging
  implements ISearchRequestPaging<AppLanguageCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppLanguageCursor

  @ApiProperty()
  pageSize: number
}

export class AppLanguageSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppLanguageFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppLanguageSearchParameters
  implements
    IEntitySearchParameters<
      AppLanguageEntity,
      AppLanguageSorting,
      AppLanguageCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppLanguageFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppLanguageSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppLanguageQuerySorting

  @ApiProperty({ required: false })
  paging?: AppLanguageQueryPaging

  @ApiProperty({ required: false })
  options?: AppLanguageSearchOptions

  relations?: ISearchQueryRelations<AppLanguageEntity>
}

export class AppLanguageSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppLanguageCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppLanguageCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppLanguageCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppLanguageCursor
}

export class AppLanguageSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppLanguageEntity,
      AppLanguageSearchParameters,
      TResult,
      AppLanguageSorting,
      AppLanguageCursor,
      AppLanguageFacets
    >
{
  @ApiProperty()
  request: AppLanguageSearchParameters

  @ApiProperty()
  facets?: AppLanguageFacets

  @ApiProperty()
  paging?: AppLanguageSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppLanguageDeleteParameters
  implements IEntitiesDeleteParameters<AppLanguageSorting>
{
  @ApiProperty({ required: false })
  filters?: AppLanguageSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppLanguageQuerySorting
}

export class AppLanguageVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppLanguageCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppLanguageCursor>
}

export class AppLanguageVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppLanguageCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppLanguageCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppLanguageCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppLanguageCursor
}

export class AppLanguageVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppLanguageCursor>
{
  @ApiProperty()
  paging?: AppLanguageVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
