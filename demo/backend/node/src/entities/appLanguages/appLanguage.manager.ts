import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppLanguageCreateData,
  AppLanguageCursor,
  AppLanguageFacets,
  AppLanguageSorting,
  AppLanguageUpdateData,
} from "./appLanguage.models"
import {
  AppLanguageSearchParameters,
  AppLanguageDeleteParameters,
} from "./appLanguage.types"
import {
  AppLanguageEntity,
  AppLanguageEntityId,
} from "../../database/core/entities/appLanguage.entity"

@WpEntityManager("appLanguage")
export class AppLanguageEntityManager extends NestEntityManager<
  AppLanguageEntity,
  AppLanguageEntityId,
  AppLanguageCreateData,
  AppLanguageUpdateData,
  AppLanguageDeleteParameters,
  AppLanguageSearchParameters,
  AppLanguageSorting,
  AppLanguageCursor,
  AppLanguageFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appLanguage", registry)
  }
}
