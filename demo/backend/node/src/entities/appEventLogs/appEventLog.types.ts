import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppEventLogEntity } from "../../database/core/entities/appEventLog.entity"
import {
  AppEventLogSorting,
  AppEventLogCursor,
  AppEventLogSearchFilters,
  AppEventLogFacets,
} from "./appEventLog.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppEventLogSearchSortingField
  implements ISearchSortingField<AppEventLogSorting>
{
  @ApiProperty({ enum: AppEventLogSorting })
  field: AppEventLogSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppEventLogQuerySorting
  implements ISearchSorting<AppEventLogSorting>
{
  @ApiProperty({ required: false, type: [AppEventLogSearchSortingField] })
  fields: AppEventLogSearchSortingField[]
}

export class AppEventLogQueryPaging
  implements ISearchRequestPaging<AppEventLogCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppEventLogCursor

  @ApiProperty()
  pageSize: number
}

export class AppEventLogSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppEventLogFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppEventLogSearchParameters
  implements
    IEntitySearchParameters<
      AppEventLogEntity,
      AppEventLogSorting,
      AppEventLogCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppEventLogFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppEventLogSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppEventLogQuerySorting

  @ApiProperty({ required: false })
  paging?: AppEventLogQueryPaging

  @ApiProperty({ required: false })
  options?: AppEventLogSearchOptions

  relations?: ISearchQueryRelations<AppEventLogEntity>
}

export class AppEventLogSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppEventLogCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppEventLogCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppEventLogCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppEventLogCursor
}

export class AppEventLogSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppEventLogEntity,
      AppEventLogSearchParameters,
      TResult,
      AppEventLogSorting,
      AppEventLogCursor,
      AppEventLogFacets
    >
{
  @ApiProperty()
  request: AppEventLogSearchParameters

  @ApiProperty()
  facets?: AppEventLogFacets

  @ApiProperty()
  paging?: AppEventLogSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppEventLogDeleteParameters
  implements IEntitiesDeleteParameters<AppEventLogSorting>
{
  @ApiProperty({ required: false })
  filters?: AppEventLogSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppEventLogQuerySorting
}

export class AppEventLogVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppEventLogCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppEventLogCursor>
}

export class AppEventLogVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppEventLogCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppEventLogCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppEventLogCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppEventLogCursor
}

export class AppEventLogVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppEventLogCursor>
{
  @ApiProperty()
  paging?: AppEventLogVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
