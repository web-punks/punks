import {
  IAuthorizationResult,
  WpEntityAuthMiddleware,
} from "@punks/backend-entity-manager"
import {
  AppAuthContext,
  AppEntityAuthorizationMiddlewareBase,
} from "../../infrastructure/authentication/index"
import { AppEventLogEntity } from "../../database/core/entities/appEventLog.entity"

@WpEntityAuthMiddleware("appEventLog")
export class AppEventLogAuthMiddleware extends AppEntityAuthorizationMiddlewareBase<AppEventLogEntity> {
  async canSearch(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.defaultCanSearch(context)
  }

  async canRead(
    entity: Partial<AppEventLogEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanRead(entity, context)
  }

  async canCreate(
    entity: Partial<AppEventLogEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanCreate(entity, context)
  }

  async canUpdate(
    entity: Partial<AppEventLogEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanUpdate(entity, context)
  }

  async canDelete(
    entity: Partial<AppEventLogEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanDelete(entity, context)
  }

  async canDeleteItems(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.defaultCanDeleteItems(context)
  }
}
