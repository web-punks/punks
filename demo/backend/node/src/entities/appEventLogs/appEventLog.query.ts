import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppEventLogEntity,
  AppEventLogEntityId,
} from "../../database/core/entities/appEventLog.entity"
import {
  AppEventLogFacets,
  AppEventLogSorting,
} from "./appEventLog.models"
import { AppEventLogSearchParameters } from "./appEventLog.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appEventLog")
export class AppEventLogQueryBuilder extends NestTypeOrmQueryBuilder<
  AppEventLogEntity,
  AppEventLogEntityId,
  AppEventLogSearchParameters,
  AppEventLogSorting,
  AppEventLogFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEventLog", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppEventLogEntity> | FindOptionsWhere<AppEventLogEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppEventLogSearchParameters
  ): FindOptionsOrder<AppEventLogEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppEventLogSearchParameters
  ): FindOptionsWhere<AppEventLogEntity> | FindOptionsWhere<AppEventLogEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppEventLogSearchParameters,
    context?: AppAuthContext
  ): Promise<AppEventLogFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
