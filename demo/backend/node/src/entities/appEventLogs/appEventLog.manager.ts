import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppEventLogCreateData,
  AppEventLogCursor,
  AppEventLogFacets,
  AppEventLogSorting,
  AppEventLogUpdateData,
} from "./appEventLog.models"
import {
  AppEventLogSearchParameters,
  AppEventLogDeleteParameters,
} from "./appEventLog.types"
import {
  AppEventLogEntity,
  AppEventLogEntityId,
} from "../../database/core/entities/appEventLog.entity"

@WpEntityManager("appEventLog")
export class AppEventLogEntityManager extends NestEntityManager<
  AppEventLogEntity,
  AppEventLogEntityId,
  AppEventLogCreateData,
  AppEventLogUpdateData,
  AppEventLogDeleteParameters,
  AppEventLogSearchParameters,
  AppEventLogSorting,
  AppEventLogCursor,
  AppEventLogFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEventLog", registry)
  }
}
