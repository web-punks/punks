import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppEventLogEntity } from "../../database/core/entities/appEventLog.entity"

export type AppEventLogCreateData = DeepPartial<Omit<AppEventLogEntity, "id">>
export type AppEventLogUpdateData = DeepPartial<Omit<AppEventLogEntity, "id">>

export enum AppEventLogSorting {
  Name = "Name",
}

export type AppEventLogCursor = number

export class AppEventLogSearchFilters {}

export class AppEventLogFacets {}

export type AppEventLogSheetItem = AppEventLogEntity
