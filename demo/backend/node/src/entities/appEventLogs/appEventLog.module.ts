import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppEventLogAdapter } from "./appEventLog.adapter"
import { AppEventLogAuthMiddleware } from "./appEventLog.authentication"
import { AppEventLogEntityManager } from "./appEventLog.manager"
import { AppEventLogQueryBuilder } from "./appEventLog.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppEventLogAdapter,
    AppEventLogAuthMiddleware,
    AppEventLogEntityManager,
    AppEventLogQueryBuilder,
  ],
  exports: [AppEventLogEntityManager],
})
export class AppEventLogEntityModule {}
