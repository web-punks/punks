import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppEventLogEntity } from "../../database/core/entities/appEventLog.entity"
import { AppEventLogCreateData, AppEventLogUpdateData } from "./appEventLog.models"

@WpEntityAdapter("appEventLog")
export class AppEventLogAdapter
  implements
    IEntityAdapter<
      AppEventLogEntity,
      AppEventLogCreateData,
      AppEventLogUpdateData
    >
{
  createDataToEntity(data: AppEventLogCreateData): DeepPartial<AppEventLogEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppEventLogUpdateData): DeepPartial<AppEventLogEntity> {
    return {
      ...data,
    }
  }
}
