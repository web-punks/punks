import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppJobDefinitionEntity,
  AppJobDefinitionEntityId,
} from "../../database/core/entities/appJobDefinition.entity"
import {
  AppJobDefinitionFacets,
  AppJobDefinitionSorting,
} from "./appJobDefinition.models"
import { AppJobDefinitionSearchParameters } from "./appJobDefinition.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appJobDefinition")
export class AppJobDefinitionQueryBuilder extends NestTypeOrmQueryBuilder<
  AppJobDefinitionEntity,
  AppJobDefinitionEntityId,
  AppJobDefinitionSearchParameters,
  AppJobDefinitionSorting,
  AppJobDefinitionFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appJobDefinition", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppJobDefinitionEntity> | FindOptionsWhere<AppJobDefinitionEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppJobDefinitionSearchParameters
  ): FindOptionsOrder<AppJobDefinitionEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppJobDefinitionSearchParameters
  ): FindOptionsWhere<AppJobDefinitionEntity> | FindOptionsWhere<AppJobDefinitionEntity>[] {
    // todo: implement query filters
    return {
      ...(request.filters?.id && {
        id: this.clause.idFilter(request.filters.id),
      }),
    }
  }

  protected calculateFacets(
    request: AppJobDefinitionSearchParameters,
    context?: AppAuthContext
  ): Promise<AppJobDefinitionFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
