import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppJobDefinitionEntity } from "../../database/core/entities/appJobDefinition.entity"
import { IdFilter } from "@/shared/api/fields"

export type AppJobDefinitionCreateData = DeepPartial<Omit<AppJobDefinitionEntity, "id">>
export type AppJobDefinitionUpdateData = DeepPartial<Omit<AppJobDefinitionEntity, "id">>

export enum AppJobDefinitionSorting {
  Name = "Name",
}

export type AppJobDefinitionCursor = number

export class AppJobDefinitionSearchFilters {
  @ApiProperty({ required: false })
  id?: IdFilter
}

export class AppJobDefinitionFacets {}

export type AppJobDefinitionSheetItem = AppJobDefinitionEntity
