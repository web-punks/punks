import {
  DeepPartial,
  IAuthenticationContext,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppJobDefinitionEntity } from "../../database/core/entities/appJobDefinition.entity"
import { AppJobDefinitionCreateData, AppJobDefinitionUpdateData } from "./appJobDefinition.models"

@WpEntityAdapter("appJobDefinition")
export class AppJobDefinitionAdapter
  implements
    IEntityAdapter<
      AppJobDefinitionEntity,
      AppJobDefinitionCreateData,
      AppJobDefinitionUpdateData
    >
{
  createDataToEntity(
    data: AppJobDefinitionCreateData, 
    context: IAuthenticationContext<unknown>
  ): DeepPartial<AppJobDefinitionEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppJobDefinitionUpdateData,
    context: IAuthenticationContext<unknown>
  ): DeepPartial<AppJobDefinitionEntity> {
    return {
      ...data,
    }
  }
}
