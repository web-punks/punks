import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppJobDefinitionEntity } from "../../database/core/entities/appJobDefinition.entity"
import {
  AppJobDefinitionSorting,
  AppJobDefinitionCursor,
  AppJobDefinitionSearchFilters,
  AppJobDefinitionFacets,
} from "./appJobDefinition.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppJobDefinitionSearchSortingField
  implements ISearchSortingField<AppJobDefinitionSorting>
{
  @ApiProperty({ enum: AppJobDefinitionSorting })
  field: AppJobDefinitionSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppJobDefinitionQuerySorting
  implements ISearchSorting<AppJobDefinitionSorting>
{
  @ApiProperty({ required: false, type: [AppJobDefinitionSearchSortingField] })
  fields: AppJobDefinitionSearchSortingField[]
}

export class AppJobDefinitionQueryPaging
  implements ISearchRequestPaging<AppJobDefinitionCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppJobDefinitionCursor

  @ApiProperty()
  pageSize: number
}

export class AppJobDefinitionSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppJobDefinitionFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppJobDefinitionSearchParameters
  implements
    IEntitySearchParameters<
      AppJobDefinitionEntity,
      AppJobDefinitionSorting,
      AppJobDefinitionCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppJobDefinitionFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppJobDefinitionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppJobDefinitionQuerySorting

  @ApiProperty({ required: false })
  paging?: AppJobDefinitionQueryPaging

  @ApiProperty({ required: false })
  options?: AppJobDefinitionSearchOptions

  relations?: ISearchQueryRelations<AppJobDefinitionEntity>
}

export class AppJobDefinitionSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppJobDefinitionCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppJobDefinitionCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppJobDefinitionCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppJobDefinitionCursor
}

export class AppJobDefinitionSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppJobDefinitionEntity,
      AppJobDefinitionSearchParameters,
      TResult,
      AppJobDefinitionSorting,
      AppJobDefinitionCursor,
      AppJobDefinitionFacets
    >
{
  @ApiProperty()
  request: AppJobDefinitionSearchParameters

  @ApiProperty()
  facets?: AppJobDefinitionFacets

  @ApiProperty()
  paging?: AppJobDefinitionSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppJobDefinitionDeleteParameters
  implements IEntitiesDeleteParameters<AppJobDefinitionSorting>
{
  @ApiProperty({ required: false })
  filters?: AppJobDefinitionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppJobDefinitionQuerySorting
}

export class AppJobDefinitionVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppJobDefinitionCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppJobDefinitionCursor>
}

export class AppJobDefinitionVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppJobDefinitionCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppJobDefinitionCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppJobDefinitionCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppJobDefinitionCursor
}

export class AppJobDefinitionVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppJobDefinitionCursor>
{
  @ApiProperty()
  paging?: AppJobDefinitionVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
