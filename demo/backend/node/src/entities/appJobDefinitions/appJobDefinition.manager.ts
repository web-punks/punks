import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppJobDefinitionCreateData,
  AppJobDefinitionCursor,
  AppJobDefinitionFacets,
  AppJobDefinitionSorting,
  AppJobDefinitionUpdateData,
} from "./appJobDefinition.models"
import {
  AppJobDefinitionSearchParameters,
  AppJobDefinitionDeleteParameters,
} from "./appJobDefinition.types"
import {
  AppJobDefinitionEntity,
  AppJobDefinitionEntityId,
} from "../../database/core/entities/appJobDefinition.entity"

@WpEntityManager("appJobDefinition")
export class AppJobDefinitionEntityManager extends NestEntityManager<
  AppJobDefinitionEntity,
  AppJobDefinitionEntityId,
  AppJobDefinitionCreateData,
  AppJobDefinitionUpdateData,
  AppJobDefinitionDeleteParameters,
  AppJobDefinitionSearchParameters,
  AppJobDefinitionSorting,
  AppJobDefinitionCursor,
  AppJobDefinitionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appJobDefinition", registry)
  }
}
