import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppJobDefinitionAdapter } from "./appJobDefinition.adapter"
import { AppJobDefinitionAuthMiddleware } from "./appJobDefinition.authentication"
import { AppJobDefinitionEntityManager } from "./appJobDefinition.manager"
import { AppJobDefinitionQueryBuilder } from "./appJobDefinition.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppJobDefinitionAdapter,
    AppJobDefinitionAuthMiddleware,
    AppJobDefinitionEntityManager,
    AppJobDefinitionQueryBuilder,
  ],
  exports: [AppJobDefinitionEntityManager],
})
export class AppJobDefinitionEntityModule {}
