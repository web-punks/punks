import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppUserGroupMemberEntity } from "../../database/core/entities/appUserGroupMember.entity"
import {
  AppUserGroupMemberSorting,
  AppUserGroupMemberCursor,
  AppUserGroupMemberSearchFilters,
  AppUserGroupMemberFacets,
} from "./appUserGroupMember.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppUserGroupMemberSearchSortingField
  implements ISearchSortingField<AppUserGroupMemberSorting>
{
  @ApiProperty({ enum: AppUserGroupMemberSorting })
  field: AppUserGroupMemberSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppUserGroupMemberQuerySorting
  implements ISearchSorting<AppUserGroupMemberSorting>
{
  @ApiProperty({
    required: false,
    type: [AppUserGroupMemberSearchSortingField],
  })
  fields: AppUserGroupMemberSearchSortingField[]
}

export class AppUserGroupMemberQueryPaging
  implements ISearchRequestPaging<AppUserGroupMemberCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppUserGroupMemberCursor

  @ApiProperty()
  pageSize: number
}

export class AppUserGroupMemberSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppUserGroupMemberFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppUserGroupMemberSearchParameters
  implements
    IEntitySearchParameters<
      AppUserGroupMemberEntity,
      AppUserGroupMemberSorting,
      AppUserGroupMemberCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppUserGroupMemberFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppUserGroupMemberSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserGroupMemberQuerySorting

  @ApiProperty({ required: false })
  paging?: AppUserGroupMemberQueryPaging

  @ApiProperty({ required: false })
  options?: AppUserGroupMemberSearchOptions

  relations?: ISearchQueryRelations<AppUserGroupMemberEntity>
}

export class AppUserGroupMemberSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppUserGroupMemberCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserGroupMemberCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserGroupMemberCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserGroupMemberCursor
}

export class AppUserGroupMemberSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppUserGroupMemberEntity,
      AppUserGroupMemberSearchParameters,
      TResult,
      AppUserGroupMemberSorting,
      AppUserGroupMemberCursor,
      AppUserGroupMemberFacets
    >
{
  @ApiProperty()
  request: AppUserGroupMemberSearchParameters

  @ApiProperty()
  facets?: AppUserGroupMemberFacets

  @ApiProperty()
  paging?: AppUserGroupMemberSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppUserGroupMemberDeleteParameters
  implements IEntitiesDeleteParameters<AppUserGroupMemberSorting>
{
  @ApiProperty({ required: false })
  filters?: AppUserGroupMemberSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserGroupMemberQuerySorting
}

export class AppUserGroupMemberVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppUserGroupMemberCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppUserGroupMemberCursor>
}

export class AppUserGroupMemberVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppUserGroupMemberCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserGroupMemberCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserGroupMemberCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserGroupMemberCursor
}

export class AppUserGroupMemberVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppUserGroupMemberCursor>
{
  @ApiProperty()
  paging?: AppUserGroupMemberVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
