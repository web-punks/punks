import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppUserGroupMemberEntity } from "../../database/core/entities/appUserGroupMember.entity"

export type AppUserGroupMemberCreateData = DeepPartial<Omit<AppUserGroupMemberEntity, "id">>
export type AppUserGroupMemberUpdateData = DeepPartial<Omit<AppUserGroupMemberEntity, "id">>

export enum AppUserGroupMemberSorting {
  Name = "Name",
}

export type AppUserGroupMemberCursor = number

export class AppUserGroupMemberSearchFilters {}

export class AppUserGroupMemberFacets {}

export type AppUserGroupMemberSheetItem = AppUserGroupMemberEntity
