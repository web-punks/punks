import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
} from "../../database/core/entities/appUserGroupMember.entity"
import {
  AppUserGroupMemberFacets,
  AppUserGroupMemberSorting,
} from "./appUserGroupMember.models"
import { AppUserGroupMemberSearchParameters } from "./appUserGroupMember.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appUserGroupMember")
export class AppUserGroupMemberQueryBuilder extends NestTypeOrmQueryBuilder<
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSorting,
  AppUserGroupMemberFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroupMember", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppUserGroupMemberEntity> | FindOptionsWhere<AppUserGroupMemberEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppUserGroupMemberSearchParameters
  ): FindOptionsOrder<AppUserGroupMemberEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppUserGroupMemberSearchParameters
  ): FindOptionsWhere<AppUserGroupMemberEntity> | FindOptionsWhere<AppUserGroupMemberEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppUserGroupMemberSearchParameters,
    context?: AppAuthContext
  ): Promise<AppUserGroupMemberFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
