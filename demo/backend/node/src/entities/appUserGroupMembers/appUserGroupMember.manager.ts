import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppUserGroupMemberCreateData,
  AppUserGroupMemberCursor,
  AppUserGroupMemberFacets,
  AppUserGroupMemberSorting,
  AppUserGroupMemberUpdateData,
} from "./appUserGroupMember.models"
import {
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberDeleteParameters,
} from "./appUserGroupMember.types"
import {
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
} from "../../database/core/entities/appUserGroupMember.entity"

@WpEntityManager("appUserGroupMember")
export class AppUserGroupMemberEntityManager extends NestEntityManager<
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
  AppUserGroupMemberCreateData,
  AppUserGroupMemberUpdateData,
  AppUserGroupMemberDeleteParameters,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSorting,
  AppUserGroupMemberCursor,
  AppUserGroupMemberFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroupMember", registry)
  }
}
