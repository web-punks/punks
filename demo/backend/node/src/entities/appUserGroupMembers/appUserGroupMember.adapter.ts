import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppUserGroupMemberEntity } from "../../database/core/entities/appUserGroupMember.entity"
import { AppUserGroupMemberCreateData, AppUserGroupMemberUpdateData } from "./appUserGroupMember.models"

@WpEntityAdapter("appUserGroupMember")
export class AppUserGroupMemberAdapter
  implements
    IEntityAdapter<
      AppUserGroupMemberEntity,
      AppUserGroupMemberCreateData,
      AppUserGroupMemberUpdateData
    >
{
  createDataToEntity(data: AppUserGroupMemberCreateData): DeepPartial<AppUserGroupMemberEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppUserGroupMemberUpdateData): DeepPartial<AppUserGroupMemberEntity> {
    return {
      ...data,
    }
  }
}
