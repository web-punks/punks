import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppUserGroupMemberAdapter } from "./appUserGroupMember.adapter"
import { AppUserGroupMemberAuthMiddleware } from "./appUserGroupMember.authentication"
import { AppUserGroupMemberEntityManager } from "./appUserGroupMember.manager"
import { AppUserGroupMemberQueryBuilder } from "./appUserGroupMember.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppUserGroupMemberAdapter,
    AppUserGroupMemberAuthMiddleware,
    AppUserGroupMemberEntityManager,
    AppUserGroupMemberQueryBuilder,
  ],
  exports: [AppUserGroupMemberEntityManager],
})
export class AppUserGroupMemberEntityModule {}
