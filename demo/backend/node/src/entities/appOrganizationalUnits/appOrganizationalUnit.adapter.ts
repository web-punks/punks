import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppOrganizationalUnitEntity } from "../../database/core/entities/appOrganizationalUnit.entity"
import {
  AppOrganizationalUnitCreateData,
  AppOrganizationalUnitUpdateData,
} from "./appOrganizationalUnit.models"

@WpEntityAdapter("appOrganizationalUnit")
export class AppOrganizationalUnitAdapter
  implements
    IEntityAdapter<
      AppOrganizationalUnitEntity,
      AppOrganizationalUnitCreateData,
      AppOrganizationalUnitUpdateData
    >
{
  createDataToEntity(
    data: AppOrganizationalUnitCreateData
  ): DeepPartial<AppOrganizationalUnitEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppOrganizationalUnitUpdateData
  ): DeepPartial<AppOrganizationalUnitEntity> {
    return {
      ...data,
    }
  }
}
