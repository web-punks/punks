import { AppOrganizationalUnitEntity } from "@/database/core/entities/appOrganizationalUnit.entity"
import {
  AppOrganizationalUnitTree,
  AppOrganizationalUnitTreeNode,
  AppOrganizationalUnitsDescendantsMap,
} from "../appOrganizationalUnit.models"
import { ITreeNode } from "@punks/backend-core"

const expandNodes = (
  node: ITreeNode<string, AppOrganizationalUnitEntity>
): ITreeNode<string, AppOrganizationalUnitEntity>[] => {
  return [node, ...node.children.flatMap((child) => expandNodes(child))]
}

export const expandTree = (tree: AppOrganizationalUnitTree) => {
  return tree.root.flatMap((node) => expandNodes(node))
}

const getDescendants = (
  node: ITreeNode<string, AppOrganizationalUnitEntity>
): AppOrganizationalUnitTreeNode[] => {
  return [
    node,
    ...node.children.flatMap((child) => getDescendants(child)),
  ] as AppOrganizationalUnitTreeNode[]
}

export const buildDescendantsMap = (
  tree: AppOrganizationalUnitTree
): AppOrganizationalUnitsDescendantsMap => {
  const nodes = expandTree(tree)
  const map: AppOrganizationalUnitsDescendantsMap = {}
  nodes.forEach((node) => {
    const descendants = getDescendants(node)
    map[node.id] = descendants.map((x) => x.value)
  })
  return map
}
