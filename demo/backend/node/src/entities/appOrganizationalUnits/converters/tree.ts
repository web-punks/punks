import { AppOrganizationalUnitEntity } from "@/database/core/entities/appOrganizationalUnit.entity"
import { buildTree } from "@punks/backend-core"
import {
  AppOrganizationalUnitTree,
  AppOrganizationalUnitTreeNode,
} from "../appOrganizationalUnit.models"

export const toAppOrganizationalUnitTree = (
  items: AppOrganizationalUnitEntity[]
) => {
  return buildTree<
    string,
    AppOrganizationalUnitEntity,
    AppOrganizationalUnitTreeNode,
    AppOrganizationalUnitTree
  >(items, {
    idSelector: (record) => record.id,
    parentIdSelector: (record) => record.parent?.id,
    additionalPropsBuilder: async (record, ancestors) => ({
      paths: {
        idPath: [...ancestors.map((ancestor) => ancestor.id), record.id],
        namePath: [...ancestors.map((ancestor) => ancestor.name), record.name],
      },
    }),
  })
}
