import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppOrganizationalUnitAdapter } from "./appOrganizationalUnit.adapter"
import { AppOrganizationalUnitAuthMiddleware } from "./appOrganizationalUnit.authentication"
import { AppOrganizationalUnitEntityManager } from "./appOrganizationalUnit.manager"
import { AppOrganizationalUnitQueryBuilder } from "./appOrganizationalUnit.query"
import { AppOrganizationalUnitService } from "./appOrganizationalUnit.service"
import { AppOrganizationalUnitQueries } from "./appOrganizationalUnit.queries"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppOrganizationalUnitAdapter,
    AppOrganizationalUnitAuthMiddleware,
    AppOrganizationalUnitEntityManager,
    AppOrganizationalUnitQueryBuilder,
    AppOrganizationalUnitQueries,
    AppOrganizationalUnitService,
  ],
  exports: [AppOrganizationalUnitEntityManager, AppOrganizationalUnitService],
})
export class AppOrganizationalUnitEntityModule {}
