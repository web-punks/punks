import { Injectable } from "@nestjs/common"
import * as A from "fp-ts/Array"
import { AppOrganizationalUnitQueries } from "./appOrganizationalUnit.queries"
import { AppOrganizationalUnitSearchParameters } from "./appOrganizationalUnit.types"
import {
  AppOrganizationalUnitTree,
  AppOrganizationalUnitsDescendantsMap,
} from "./appOrganizationalUnit.models"
import { OrganizationCacheService } from "@/middleware/cache/organization"
import { IAuthOrganizationalUnit } from "@punks/backend-entity-manager"

@Injectable()
export class AppOrganizationalUnitService {
  constructor(
    private readonly queries: AppOrganizationalUnitQueries,
    private readonly cache: OrganizationCacheService
  ) {}

  async buildTree(
    searchParameters: AppOrganizationalUnitSearchParameters
  ): Promise<AppOrganizationalUnitTree> {
    return await this.queries.buildTree(searchParameters)
  }

  async getDescendantsMapCached(): Promise<AppOrganizationalUnitsDescendantsMap> {
    return await this.cache.retrieveOrganizationalUnitDescendantsMap(() =>
      this.queries.buildDescendantsMap()
    )
  }

  async getOrganizationalUnitsDescendants(
    organizationalUnits: IAuthOrganizationalUnit[]
  ) {
    const descendantsMap = await this.getDescendantsMapCached()
    return A.uniq<IAuthOrganizationalUnit>({
      equals: (a, b) => a.id === b.id,
    })(organizationalUnits.flatMap((ou) => descendantsMap[ou.id]))
  }
}
