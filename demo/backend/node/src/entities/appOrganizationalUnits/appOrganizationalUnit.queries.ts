import { Injectable } from "@nestjs/common"
import { AppOrganizationalUnitEntityManager } from "./appOrganizationalUnit.manager"
import { AppOrganizationalUnitSearchParameters } from "./appOrganizationalUnit.types"
import {
  AppOrganizationalUnitTree,
  AppOrganizationalUnitsDescendantsMap,
} from "./appOrganizationalUnit.models"
import { toAppOrganizationalUnitTree } from "./converters/tree"
import { buildDescendantsMap } from "./converters/treeDescendants"
import { AppOrganizationalUnitRepository } from "@/database/core/repositories/appOrganizationalUnit.repository"

@Injectable()
export class AppOrganizationalUnitQueries {
  constructor(private readonly manager: AppOrganizationalUnitEntityManager) {}

  async buildDescendantsMap(): Promise<AppOrganizationalUnitsDescendantsMap> {
    const tree = await this.buildGlobalTree()
    return buildDescendantsMap(tree)
  }

  async buildGlobalTree(): Promise<AppOrganizationalUnitTree> {
    const items = await this.repository.find({
      relations: {
        parent: true,
      },
    })
    return toAppOrganizationalUnitTree(items)
  }

  async buildTree(
    searchParameters: AppOrganizationalUnitSearchParameters
  ): Promise<AppOrganizationalUnitTree> {
    const data = await this.manager.manager.search.execute(searchParameters)
    return toAppOrganizationalUnitTree(data.items)
  }

  private get repository() {
    return this.manager.getRepository() as AppOrganizationalUnitRepository
  }
}
