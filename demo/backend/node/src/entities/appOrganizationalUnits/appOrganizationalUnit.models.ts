import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppOrganizationalUnitEntity } from "@/database/core/entities/appOrganizationalUnit.entity"
import { ITree, ITreeNode } from "@punks/backend-core"
import { EntityTreeNodePaths } from "@/shared/api/trees"

export type AppOrganizationalUnitCreateData = DeepPartial<
  Omit<AppOrganizationalUnitEntity, "id">
>
export type AppOrganizationalUnitUpdateData = DeepPartial<
  Omit<AppOrganizationalUnitEntity, "id">
>

export enum AppOrganizationalUnitSorting {
  Name = "Name",
}

export type AppOrganizationalUnitCursor = number

export class AppOrganizationalUnitSearchFilters {
  @ApiProperty({ type: [String], required: false })
  typeIds?: string[]
}

export class AppOrganizationalUnitFacets {}

export type AppOrganizationalUnitSheetItem = AppOrganizationalUnitEntity

export type AppOrganizationalUnitTreeNode = ITreeNode<
  string,
  AppOrganizationalUnitEntity
> & {
  paths: EntityTreeNodePaths
}

export type AppOrganizationalUnitTree = ITree<
  string,
  AppOrganizationalUnitEntity
>

export class AppOrganizationalUnitsDescendantsMap {
  [parentId: string]: AppOrganizationalUnitEntity[]
}
