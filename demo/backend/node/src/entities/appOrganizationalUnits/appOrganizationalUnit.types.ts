import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppOrganizationalUnitEntity } from "../../database/core/entities/appOrganizationalUnit.entity"
import {
  AppOrganizationalUnitSorting,
  AppOrganizationalUnitCursor,
  AppOrganizationalUnitSearchFilters,
  AppOrganizationalUnitFacets,
} from "./appOrganizationalUnit.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"
import { EntitiesTraverseFilters } from "@/shared/api/traverse"

export class AppOrganizationalUnitSearchSortingField
  implements ISearchSortingField<AppOrganizationalUnitSorting>
{
  @ApiProperty({ enum: AppOrganizationalUnitSorting })
  field: AppOrganizationalUnitSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppOrganizationalUnitQuerySorting
  implements ISearchSorting<AppOrganizationalUnitSorting>
{
  @ApiProperty({
    required: false,
    type: [AppOrganizationalUnitSearchSortingField],
  })
  fields: AppOrganizationalUnitSearchSortingField[]
}

export class AppOrganizationalUnitQueryPaging
  implements ISearchRequestPaging<AppOrganizationalUnitCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppOrganizationalUnitCursor

  @ApiProperty()
  pageSize: number
}

export class AppOrganizationalUnitSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean

  @ApiProperty({ required: false })
  includeChildrenMap?: boolean
}

export class AppOrganizationalUnitFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppOrganizationalUnitSearchParameters
  implements
    IEntitySearchParameters<
      AppOrganizationalUnitEntity,
      AppOrganizationalUnitSorting,
      AppOrganizationalUnitCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppOrganizationalUnitFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppOrganizationalUnitSearchFilters

  @ApiProperty({ required: false })
  traverse?: EntitiesTraverseFilters

  @ApiProperty({ required: false })
  sorting?: AppOrganizationalUnitQuerySorting

  @ApiProperty({ required: false })
  paging?: AppOrganizationalUnitQueryPaging

  @ApiProperty({ required: false })
  options?: AppOrganizationalUnitSearchOptions

  relations?: ISearchQueryRelations<AppOrganizationalUnitEntity>
}

export class AppOrganizationalUnitSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppOrganizationalUnitCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppOrganizationalUnitCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppOrganizationalUnitCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppOrganizationalUnitCursor
}

export class AppOrganizationalUnitSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppOrganizationalUnitEntity,
      AppOrganizationalUnitSearchParameters,
      TResult,
      AppOrganizationalUnitSorting,
      AppOrganizationalUnitCursor,
      AppOrganizationalUnitFacets
    >
{
  @ApiProperty()
  request: AppOrganizationalUnitSearchParameters

  @ApiProperty()
  facets?: AppOrganizationalUnitFacets

  @ApiProperty()
  paging?: AppOrganizationalUnitSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppOrganizationalUnitDeleteParameters
  implements IEntitiesDeleteParameters<AppOrganizationalUnitSorting>
{
  @ApiProperty({ required: false })
  filters?: AppOrganizationalUnitSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppOrganizationalUnitQuerySorting
}

export class AppOrganizationalUnitVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppOrganizationalUnitCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppOrganizationalUnitCursor>
}

export class AppOrganizationalUnitVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppOrganizationalUnitCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppOrganizationalUnitCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppOrganizationalUnitCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppOrganizationalUnitCursor
}

export class AppOrganizationalUnitVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppOrganizationalUnitCursor>
{
  @ApiProperty()
  paging?: AppOrganizationalUnitVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
