import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppOrganizationalUnitTypeEntity } from "../../database/core/entities/appOrganizationalUnitType.entity"
import {
  AppOrganizationalUnitTypeSorting,
  AppOrganizationalUnitTypeCursor,
  AppOrganizationalUnitTypeSearchFilters,
  AppOrganizationalUnitTypeFacets,
} from "./appOrganizationalUnitType.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppOrganizationalUnitTypeSearchSortingField
  implements ISearchSortingField<AppOrganizationalUnitTypeSorting>
{
  @ApiProperty({ enum: AppOrganizationalUnitTypeSorting })
  field: AppOrganizationalUnitTypeSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppOrganizationalUnitTypeQuerySorting
  implements ISearchSorting<AppOrganizationalUnitTypeSorting>
{
  @ApiProperty({ required: false, type: [AppOrganizationalUnitTypeSearchSortingField] })
  fields: AppOrganizationalUnitTypeSearchSortingField[]
}

export class AppOrganizationalUnitTypeQueryPaging
  implements ISearchRequestPaging<AppOrganizationalUnitTypeCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppOrganizationalUnitTypeCursor

  @ApiProperty()
  pageSize: number
}

export class AppOrganizationalUnitTypeSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppOrganizationalUnitTypeFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppOrganizationalUnitTypeSearchParameters
  implements
    IEntitySearchParameters<
      AppOrganizationalUnitTypeEntity,
      AppOrganizationalUnitTypeSorting,
      AppOrganizationalUnitTypeCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppOrganizationalUnitTypeFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppOrganizationalUnitTypeSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppOrganizationalUnitTypeQuerySorting

  @ApiProperty({ required: false })
  paging?: AppOrganizationalUnitTypeQueryPaging

  @ApiProperty({ required: false })
  options?: AppOrganizationalUnitTypeSearchOptions

  relations?: ISearchQueryRelations<AppOrganizationalUnitTypeEntity>
}

export class AppOrganizationalUnitTypeSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppOrganizationalUnitTypeCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppOrganizationalUnitTypeCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppOrganizationalUnitTypeCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppOrganizationalUnitTypeCursor
}

export class AppOrganizationalUnitTypeSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppOrganizationalUnitTypeEntity,
      AppOrganizationalUnitTypeSearchParameters,
      TResult,
      AppOrganizationalUnitTypeSorting,
      AppOrganizationalUnitTypeCursor,
      AppOrganizationalUnitTypeFacets
    >
{
  @ApiProperty()
  request: AppOrganizationalUnitTypeSearchParameters

  @ApiProperty()
  facets?: AppOrganizationalUnitTypeFacets

  @ApiProperty()
  paging?: AppOrganizationalUnitTypeSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppOrganizationalUnitTypeDeleteParameters
  implements IEntitiesDeleteParameters<AppOrganizationalUnitTypeSorting>
{
  @ApiProperty({ required: false })
  filters?: AppOrganizationalUnitTypeSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppOrganizationalUnitTypeQuerySorting
}

export class AppOrganizationalUnitTypeVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppOrganizationalUnitTypeCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppOrganizationalUnitTypeCursor>
}

export class AppOrganizationalUnitTypeVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppOrganizationalUnitTypeCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppOrganizationalUnitTypeCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppOrganizationalUnitTypeCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppOrganizationalUnitTypeCursor
}

export class AppOrganizationalUnitTypeVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppOrganizationalUnitTypeCursor>
{
  @ApiProperty()
  paging?: AppOrganizationalUnitTypeVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
