import {
  FindOptionsOrder,
  FindOptionsRelations,
  FindOptionsWhere,
} from "typeorm"
import {
  EntityManagerRegistry,
  IAuthenticationContext,
  NestTypeOrmQueryBuilder,
  QueryBuilderOperation,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId,
} from "../../database/core/entities/appOrganizationalUnitType.entity"
import {
  AppOrganizationalUnitTypeFacets,
  AppOrganizationalUnitTypeSorting,
} from "./appOrganizationalUnitType.models"
import { AppOrganizationalUnitTypeSearchParameters } from "./appOrganizationalUnitType.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appOrganizationalUnitType")
export class AppOrganizationalUnitTypeQueryBuilder extends NestTypeOrmQueryBuilder<
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId,
  AppOrganizationalUnitTypeSearchParameters,
  AppOrganizationalUnitTypeSorting,
  AppOrganizationalUnitTypeFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganizationalUnitType", registry)
  }

  protected getRelationsToLoad(
    request: AppOrganizationalUnitTypeSearchParameters,
    context: IAuthenticationContext<AppUserContext>,
    operation: QueryBuilderOperation
  ): FindOptionsRelations<AppOrganizationalUnitTypeEntity> {
    return {
      allowedChildrenTypes: {
        childType: true,
      },
    }
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ):
    | FindOptionsWhere<AppOrganizationalUnitTypeEntity>
    | FindOptionsWhere<AppOrganizationalUnitTypeEntity>[] {
    return {
      ...(context?.userContext?.organizationId
        ? {
            organization: {
              id: context?.userContext?.organizationId,
            },
          }
        : {}),
    }
  }

  protected buildSortingClause(
    request: AppOrganizationalUnitTypeSearchParameters
  ): FindOptionsOrder<AppOrganizationalUnitTypeEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppOrganizationalUnitTypeSearchParameters
  ):
    | FindOptionsWhere<AppOrganizationalUnitTypeEntity>
    | FindOptionsWhere<AppOrganizationalUnitTypeEntity>[] {
    return {}
  }

  protected calculateFacets(
    request: AppOrganizationalUnitTypeSearchParameters,
    context?: AppAuthContext
  ): Promise<AppOrganizationalUnitTypeFacets> {
    return Promise.resolve({})
  }
}
