import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppOrganizationalUnitTypeEntity } from "../../database/core/entities/appOrganizationalUnitType.entity"
import { AppOrganizationalUnitTypeCreateData, AppOrganizationalUnitTypeUpdateData } from "./appOrganizationalUnitType.models"

@WpEntityAdapter("appOrganizationalUnitType")
export class AppOrganizationalUnitTypeAdapter
  implements
    IEntityAdapter<
      AppOrganizationalUnitTypeEntity,
      AppOrganizationalUnitTypeCreateData,
      AppOrganizationalUnitTypeUpdateData
    >
{
  createDataToEntity(data: AppOrganizationalUnitTypeCreateData): DeepPartial<AppOrganizationalUnitTypeEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppOrganizationalUnitTypeUpdateData): DeepPartial<AppOrganizationalUnitTypeEntity> {
    return {
      ...data,
    }
  }
}
