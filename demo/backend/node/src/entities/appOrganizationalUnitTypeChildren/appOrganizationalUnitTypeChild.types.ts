import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppOrganizationalUnitTypeChildEntity } from "../../database/core/entities/appOrganizationalUnitTypeChild.entity"
import {
  AppOrganizationalUnitTypeChildSorting,
  AppOrganizationalUnitTypeChildCursor,
  AppOrganizationalUnitTypeChildSearchFilters,
  AppOrganizationalUnitTypeChildFacets,
} from "./appOrganizationalUnitTypeChild.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppOrganizationalUnitTypeChildSearchSortingField
  implements ISearchSortingField<AppOrganizationalUnitTypeChildSorting>
{
  @ApiProperty({ enum: AppOrganizationalUnitTypeChildSorting })
  field: AppOrganizationalUnitTypeChildSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppOrganizationalUnitTypeChildQuerySorting
  implements ISearchSorting<AppOrganizationalUnitTypeChildSorting>
{
  @ApiProperty({
    required: false,
    type: [AppOrganizationalUnitTypeChildSearchSortingField],
  })
  fields: AppOrganizationalUnitTypeChildSearchSortingField[]
}

export class AppOrganizationalUnitTypeChildQueryPaging
  implements ISearchRequestPaging<AppOrganizationalUnitTypeChildCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppOrganizationalUnitTypeChildCursor

  @ApiProperty()
  pageSize: number
}

export class AppOrganizationalUnitTypeChildSearchOptions
  implements ISearchOptions
{
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppOrganizationalUnitTypeChildFullTextQuery
  implements IFullTextQuery
{
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppOrganizationalUnitTypeChildSearchParameters
  implements
    IEntitySearchParameters<
      AppOrganizationalUnitTypeChildEntity,
      AppOrganizationalUnitTypeChildSorting,
      AppOrganizationalUnitTypeChildCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppOrganizationalUnitTypeChildFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppOrganizationalUnitTypeChildSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppOrganizationalUnitTypeChildQuerySorting

  @ApiProperty({ required: false })
  paging?: AppOrganizationalUnitTypeChildQueryPaging

  @ApiProperty({ required: false })
  options?: AppOrganizationalUnitTypeChildSearchOptions

  relations?: ISearchQueryRelations<AppOrganizationalUnitTypeChildEntity>
}

export class AppOrganizationalUnitTypeChildSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppOrganizationalUnitTypeChildCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppOrganizationalUnitTypeChildCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppOrganizationalUnitTypeChildCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppOrganizationalUnitTypeChildCursor
}

export class AppOrganizationalUnitTypeChildSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppOrganizationalUnitTypeChildEntity,
      AppOrganizationalUnitTypeChildSearchParameters,
      TResult,
      AppOrganizationalUnitTypeChildSorting,
      AppOrganizationalUnitTypeChildCursor,
      AppOrganizationalUnitTypeChildFacets
    >
{
  @ApiProperty()
  request: AppOrganizationalUnitTypeChildSearchParameters

  @ApiProperty()
  facets?: AppOrganizationalUnitTypeChildFacets

  @ApiProperty()
  paging?: AppOrganizationalUnitTypeChildSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppOrganizationalUnitTypeChildDeleteParameters
  implements IEntitiesDeleteParameters<AppOrganizationalUnitTypeChildSorting>
{
  @ApiProperty({ required: false })
  filters?: AppOrganizationalUnitTypeChildSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppOrganizationalUnitTypeChildQuerySorting
}

export class AppOrganizationalUnitTypeChildVersionsSearchParameters
  implements
    IEntityVersionsSearchParameters<AppOrganizationalUnitTypeChildCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppOrganizationalUnitTypeChildCursor>
}

export class AppOrganizationalUnitTypeChildVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppOrganizationalUnitTypeChildCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppOrganizationalUnitTypeChildCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppOrganizationalUnitTypeChildCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppOrganizationalUnitTypeChildCursor
}

export class AppOrganizationalUnitTypeChildVersionsSearchResults<TResult>
  implements
    IEntityVersionsSearchResults<TResult, AppOrganizationalUnitTypeChildCursor>
{
  @ApiProperty()
  paging?: AppOrganizationalUnitTypeChildVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
