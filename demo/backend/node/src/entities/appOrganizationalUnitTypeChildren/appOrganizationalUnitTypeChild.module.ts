import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppOrganizationalUnitTypeChildAdapter } from "./appOrganizationalUnitTypeChild.adapter"
import { AppOrganizationalUnitTypeChildAuthMiddleware } from "./appOrganizationalUnitTypeChild.authentication"
import { AppOrganizationalUnitTypeChildEntityManager } from "./appOrganizationalUnitTypeChild.manager"
import { AppOrganizationalUnitTypeChildQueryBuilder } from "./appOrganizationalUnitTypeChild.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppOrganizationalUnitTypeChildAdapter,
    AppOrganizationalUnitTypeChildAuthMiddleware,
    AppOrganizationalUnitTypeChildEntityManager,
    AppOrganizationalUnitTypeChildQueryBuilder,
  ],
  exports: [AppOrganizationalUnitTypeChildEntityManager],
})
export class AppOrganizationalUnitTypeChildEntityModule {}
