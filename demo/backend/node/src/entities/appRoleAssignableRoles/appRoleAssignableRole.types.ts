import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppRoleAssignableRoleEntity } from "../../database/core/entities/appRoleAssignableRole.entity"
import {
  AppRoleAssignableRoleSorting,
  AppRoleAssignableRoleCursor,
  AppRoleAssignableRoleSearchFilters,
  AppRoleAssignableRoleFacets,
} from "./appRoleAssignableRole.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppRoleAssignableRoleSearchSortingField
  implements ISearchSortingField<AppRoleAssignableRoleSorting>
{
  @ApiProperty({ enum: AppRoleAssignableRoleSorting })
  field: AppRoleAssignableRoleSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppRoleAssignableRoleQuerySorting
  implements ISearchSorting<AppRoleAssignableRoleSorting>
{
  @ApiProperty({ required: false, type: [AppRoleAssignableRoleSearchSortingField] })
  fields: AppRoleAssignableRoleSearchSortingField[]
}

export class AppRoleAssignableRoleQueryPaging
  implements ISearchRequestPaging<AppRoleAssignableRoleCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppRoleAssignableRoleCursor

  @ApiProperty()
  pageSize: number
}

export class AppRoleAssignableRoleSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppRoleAssignableRoleFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppRoleAssignableRoleSearchParameters
  implements
    IEntitySearchParameters<
      AppRoleAssignableRoleEntity,
      AppRoleAssignableRoleSorting,
      AppRoleAssignableRoleCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppRoleAssignableRoleFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppRoleAssignableRoleSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppRoleAssignableRoleQuerySorting

  @ApiProperty({ required: false })
  paging?: AppRoleAssignableRoleQueryPaging

  @ApiProperty({ required: false })
  options?: AppRoleAssignableRoleSearchOptions

  relations?: ISearchQueryRelations<AppRoleAssignableRoleEntity>
}

export class AppRoleAssignableRoleSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppRoleAssignableRoleCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppRoleAssignableRoleCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppRoleAssignableRoleCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppRoleAssignableRoleCursor
}

export class AppRoleAssignableRoleSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppRoleAssignableRoleEntity,
      AppRoleAssignableRoleSearchParameters,
      TResult,
      AppRoleAssignableRoleSorting,
      AppRoleAssignableRoleCursor,
      AppRoleAssignableRoleFacets
    >
{
  @ApiProperty()
  request: AppRoleAssignableRoleSearchParameters

  @ApiProperty()
  facets?: AppRoleAssignableRoleFacets

  @ApiProperty()
  paging?: AppRoleAssignableRoleSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppRoleAssignableRoleDeleteParameters
  implements IEntitiesDeleteParameters<AppRoleAssignableRoleSorting>
{
  @ApiProperty({ required: false })
  filters?: AppRoleAssignableRoleSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppRoleAssignableRoleQuerySorting
}

export class AppRoleAssignableRoleVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppRoleAssignableRoleCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppRoleAssignableRoleCursor>
}

export class AppRoleAssignableRoleVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppRoleAssignableRoleCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppRoleAssignableRoleCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppRoleAssignableRoleCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppRoleAssignableRoleCursor
}

export class AppRoleAssignableRoleVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppRoleAssignableRoleCursor>
{
  @ApiProperty()
  paging?: AppRoleAssignableRoleVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
