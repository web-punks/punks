import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppRoleAssignableRoleEntity } from "../../database/core/entities/appRoleAssignableRole.entity"
import { AppRoleAssignableRoleCreateData, AppRoleAssignableRoleUpdateData } from "./appRoleAssignableRole.models"

@WpEntityAdapter("appRoleAssignableRole")
export class AppRoleAssignableRoleAdapter
  implements
    IEntityAdapter<
      AppRoleAssignableRoleEntity,
      AppRoleAssignableRoleCreateData,
      AppRoleAssignableRoleUpdateData
    >
{
  createDataToEntity(data: AppRoleAssignableRoleCreateData): DeepPartial<AppRoleAssignableRoleEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppRoleAssignableRoleUpdateData): DeepPartial<AppRoleAssignableRoleEntity> {
    return {
      ...data,
    }
  }
}
