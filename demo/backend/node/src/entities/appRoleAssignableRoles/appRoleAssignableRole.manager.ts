import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppRoleAssignableRoleCreateData,
  AppRoleAssignableRoleCursor,
  AppRoleAssignableRoleFacets,
  AppRoleAssignableRoleSorting,
  AppRoleAssignableRoleUpdateData,
} from "./appRoleAssignableRole.models"
import {
  AppRoleAssignableRoleSearchParameters,
  AppRoleAssignableRoleDeleteParameters,
} from "./appRoleAssignableRole.types"
import {
  AppRoleAssignableRoleEntity,
  AppRoleAssignableRoleEntityId,
} from "../../database/core/entities/appRoleAssignableRole.entity"

@WpEntityManager("appRoleAssignableRole")
export class AppRoleAssignableRoleEntityManager extends NestEntityManager<
  AppRoleAssignableRoleEntity,
  AppRoleAssignableRoleEntityId,
  AppRoleAssignableRoleCreateData,
  AppRoleAssignableRoleUpdateData,
  AppRoleAssignableRoleDeleteParameters,
  AppRoleAssignableRoleSearchParameters,
  AppRoleAssignableRoleSorting,
  AppRoleAssignableRoleCursor,
  AppRoleAssignableRoleFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRoleAssignableRole", registry)
  }
}
