import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppRoleAssignableRoleEntity } from "../../database/core/entities/appRoleAssignableRole.entity"

export type AppRoleAssignableRoleCreateData = DeepPartial<
  Omit<AppRoleAssignableRoleEntity, "id">
>
export type AppRoleAssignableRoleUpdateData = DeepPartial<
  Omit<AppRoleAssignableRoleEntity, "id">
>

export enum AppRoleAssignableRoleSorting {
  Name = "Name",
}

export type AppRoleAssignableRoleCursor = number

export class AppRoleAssignableRoleSearchFilters {
  @ApiProperty({ required: false })
  userRoleIds?: string[]

  @ApiProperty({ required: false })
  assignableRoleIds?: string[]
}

export class AppRoleAssignableRoleFacets {}

export type AppRoleAssignableRoleSheetItem = AppRoleAssignableRoleEntity
