import { FindOptionsOrder, FindOptionsWhere, In } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppRoleAssignableRoleEntity,
  AppRoleAssignableRoleEntityId,
} from "../../database/core/entities/appRoleAssignableRole.entity"
import {
  AppRoleAssignableRoleFacets,
  AppRoleAssignableRoleSorting,
} from "./appRoleAssignableRole.models"
import { AppRoleAssignableRoleSearchParameters } from "./appRoleAssignableRole.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appRoleAssignableRole")
export class AppRoleAssignableRoleQueryBuilder extends NestTypeOrmQueryBuilder<
  AppRoleAssignableRoleEntity,
  AppRoleAssignableRoleEntityId,
  AppRoleAssignableRoleSearchParameters,
  AppRoleAssignableRoleSorting,
  AppRoleAssignableRoleFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRoleAssignableRole", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ):
    | FindOptionsWhere<AppRoleAssignableRoleEntity>
    | FindOptionsWhere<AppRoleAssignableRoleEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppRoleAssignableRoleSearchParameters
  ): FindOptionsOrder<AppRoleAssignableRoleEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppRoleAssignableRoleSearchParameters
  ):
    | FindOptionsWhere<AppRoleAssignableRoleEntity>
    | FindOptionsWhere<AppRoleAssignableRoleEntity>[] {
    return {
      ...(request.filters?.userRoleIds && {
        userRole: {
          id: In(request.filters.userRoleIds),
        },
      }),
      ...(request.filters?.assignableRoleIds && {
        assignableRole: {
          id: In(request.filters.assignableRoleIds),
        },
      }),
    }
  }

  protected calculateFacets(
    request: AppRoleAssignableRoleSearchParameters,
    context?: AppAuthContext
  ): Promise<AppRoleAssignableRoleFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
