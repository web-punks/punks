import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppRoleAssignableRoleAdapter } from "./appRoleAssignableRole.adapter"
import { AppRoleAssignableRoleAuthMiddleware } from "./appRoleAssignableRole.authentication"
import { AppRoleAssignableRoleEntityManager } from "./appRoleAssignableRole.manager"
import { AppRoleAssignableRoleQueryBuilder } from "./appRoleAssignableRole.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppRoleAssignableRoleAdapter,
    AppRoleAssignableRoleAuthMiddleware,
    AppRoleAssignableRoleEntityManager,
    AppRoleAssignableRoleQueryBuilder,
  ],
  exports: [AppRoleAssignableRoleEntityManager],
})
export class AppRoleAssignableRoleEntityModule {}
