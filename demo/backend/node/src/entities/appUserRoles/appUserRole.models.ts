import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppUserRoleEntity } from "../../database/core/entities/appUserRole.entity"

export type AppUserRoleEntityId = string

export type AppUserRoleCreateData = DeepPartial<Omit<AppUserRoleEntity, "id">>
export type AppUserRoleUpdateData = DeepPartial<Omit<AppUserRoleEntity, "id">>

export enum AppUserRoleSorting {
  Name = "Name",
}

export type AppUserRoleCursor = number

export class AppUserRoleSearchFilters {
  @ApiProperty({ required: false })
  userId?: string

  @ApiProperty({ required: false, type: [String] })
  roleIds?: string[]
}

export class AppUserRoleFacets {}

export type AppUserRoleSheetItem = AppUserRoleEntity
