import {
  FindOptionsOrder,
  FindOptionsRelations,
  FindOptionsWhere,
  In,
} from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppUserRoleEntity,
  AppUserRoleEntityId,
} from "../../database/core/entities/appUserRole.entity"
import {
  AppUserRoleFacets,
  AppUserRoleSorting,
} from "./appUserRole.models"
import { AppUserRoleSearchParameters } from "./appUserRole.types"
import {
  AppUserContext,
  AppAuthContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appUserRole")
export class AppUserRoleQueryBuilder extends NestTypeOrmQueryBuilder<
  AppUserRoleEntity,
  AppUserRoleEntityId,
  AppUserRoleSearchParameters,
  AppUserRoleSorting,
  AppUserRoleFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserRole", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext | undefined
  ): FindOptionsWhere<AppUserRoleEntity> | FindOptionsWhere<AppUserRoleEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppUserRoleSearchParameters
  ): FindOptionsOrder<AppUserRoleEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppUserRoleSearchParameters
  ): FindOptionsWhere<AppUserRoleEntity> | FindOptionsWhere<AppUserRoleEntity>[] {
    return {
      ...(request.filters?.userId
        ? {
            user: {
              id: request.filters.userId,
            },
          }
        : {}),
      ...(request.filters?.roleIds
        ? {
            role: {
              id: In(request.filters.roleIds),
            },
          }
        : undefined),
    }
  }

  protected calculateFacets(
    request: AppUserRoleSearchParameters
  ): Promise<AppUserRoleFacets | undefined> {
    return Promise.resolve(undefined)
  }

  protected getRelationsToLoad(
    request: AppUserRoleSearchParameters,
    context: AppAuthContext | undefined
  ): FindOptionsRelations<AppUserRoleEntity> | undefined {
    return {
      role: true,
      user: true,
    }
  }
}
