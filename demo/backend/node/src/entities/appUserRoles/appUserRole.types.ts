import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppUserRoleEntity } from "../../database/core/entities/appUserRole.entity"
import {
  AppUserRoleSorting,
  AppUserRoleCursor,
  AppUserRoleSearchFilters,
  AppUserRoleFacets,
} from "./appUserRole.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppUserRoleSearchSortingField
  implements ISearchSortingField<AppUserRoleSorting>
{
  @ApiProperty({ enum: AppUserRoleSorting })
  field: AppUserRoleSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppUserRoleQuerySorting
  implements ISearchSorting<AppUserRoleSorting>
{
  @ApiProperty({ required: false, type: [AppUserRoleSearchSortingField] })
  fields: AppUserRoleSearchSortingField[]
}

export class AppUserRoleQueryPaging
  implements ISearchRequestPaging<AppUserRoleCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppUserRoleCursor

  @ApiProperty()
  pageSize: number
}

export class AppUserRoleSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppUserRoleFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppUserRoleSearchParameters
  implements
    IEntitySearchParameters<
      AppUserRoleEntity,
      AppUserRoleSorting,
      AppUserRoleCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppUserRoleFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppUserRoleSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserRoleQuerySorting

  @ApiProperty({ required: false })
  paging?: AppUserRoleQueryPaging

  @ApiProperty({ required: false })
  options?: AppUserRoleSearchOptions

  relations?: ISearchQueryRelations<AppUserRoleEntity>
}

export class AppUserRoleSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppUserRoleCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserRoleCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserRoleCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserRoleCursor
}

export class AppUserRoleSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppUserRoleEntity,
      AppUserRoleSearchParameters,
      TResult,
      AppUserRoleSorting,
      AppUserRoleCursor,
      AppUserRoleFacets
    >
{
  @ApiProperty()
  request: AppUserRoleSearchParameters

  @ApiProperty()
  facets?: AppUserRoleFacets

  @ApiProperty()
  paging?: AppUserRoleSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppUserRoleDeleteParameters
  implements IEntitiesDeleteParameters<AppUserRoleSorting>
{
  @ApiProperty({ required: false })
  filters?: AppUserRoleSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserRoleQuerySorting
}

export class AppUserRoleVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppUserRoleCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppUserRoleCursor>
}

export class AppUserRoleVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppUserRoleCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserRoleCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserRoleCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserRoleCursor
}

export class AppUserRoleVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppUserRoleCursor>
{
  @ApiProperty()
  paging?: AppUserRoleVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
