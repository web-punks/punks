import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppUserRoleCreateData,
  AppUserRoleCursor,
  AppUserRoleFacets,
  AppUserRoleSorting,
  AppUserRoleUpdateData,
} from "./appUserRole.models"
import {
  AppUserRoleSearchParameters,
  AppUserRoleDeleteParameters,
} from "./appUserRole.types"
import {
  AppUserRoleEntity,
  AppUserRoleEntityId,
} from "../../database/core/entities/appUserRole.entity"

@WpEntityManager("appUserRole")
export class AppUserRoleEntityManager extends NestEntityManager<
  AppUserRoleEntity,
  AppUserRoleEntityId,
  AppUserRoleCreateData,
  AppUserRoleUpdateData,
  AppUserRoleDeleteParameters,
  AppUserRoleSearchParameters,
  AppUserRoleSorting,
  AppUserRoleCursor,
  AppUserRoleFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserRole", registry)
  }
}
