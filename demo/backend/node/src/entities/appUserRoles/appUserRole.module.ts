import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppUserRoleAdapter } from "./appUserRole.adapter"
import { AppUserRoleAuthMiddleware } from "./appUserRole.authentication"
import { AppUserRoleEntityManager } from "./appUserRole.manager"
import { AppUserRoleQueryBuilder } from "./appUserRole.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppUserRoleAdapter,
    AppUserRoleAuthMiddleware,
    AppUserRoleEntityManager,
    AppUserRoleQueryBuilder,
  ],
  exports: [AppUserRoleEntityManager],
})
export class AppUserRoleEntityModule {}
