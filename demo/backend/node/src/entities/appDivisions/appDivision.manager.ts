import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppDivisionCreateData,
  AppDivisionCursor,
  AppDivisionFacets,
  AppDivisionSorting,
  AppDivisionUpdateData,
} from "./appDivision.models"
import {
  AppDivisionSearchParameters,
  AppDivisionDeleteParameters,
} from "./appDivision.types"
import {
  AppDivisionEntity,
  AppDivisionEntityId,
} from "../../database/core/entities/appDivision.entity"

@WpEntityManager("appDivision")
export class AppDivisionEntityManager extends NestEntityManager<
  AppDivisionEntity,
  AppDivisionEntityId,
  AppDivisionCreateData,
  AppDivisionUpdateData,
  AppDivisionDeleteParameters,
  AppDivisionSearchParameters,
  AppDivisionSorting,
  AppDivisionCursor,
  AppDivisionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDivision", registry)
  }
}
