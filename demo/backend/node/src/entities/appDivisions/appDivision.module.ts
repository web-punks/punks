import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppDivisionAdapter } from "./appDivision.adapter"
import { AppDivisionAuthMiddleware } from "./appDivision.authentication"
import { AppDivisionEntityManager } from "./appDivision.manager"
import { AppDivisionQueryBuilder } from "./appDivision.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppDivisionAdapter,
    AppDivisionAuthMiddleware,
    AppDivisionEntityManager,
    AppDivisionQueryBuilder,
  ],
  exports: [AppDivisionEntityManager],
})
export class AppDivisionEntityModule {}
