import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppDivisionEntity } from "../../database/core/entities/appDivision.entity"
import {
  AppDivisionSorting,
  AppDivisionCursor,
  AppDivisionSearchFilters,
  AppDivisionFacets,
} from "./appDivision.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppDivisionSearchSortingField
  implements ISearchSortingField<AppDivisionSorting>
{
  @ApiProperty({ enum: AppDivisionSorting })
  field: AppDivisionSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppDivisionQuerySorting
  implements ISearchSorting<AppDivisionSorting>
{
  @ApiProperty({ required: false, type: [AppDivisionSearchSortingField] })
  fields: AppDivisionSearchSortingField[]
}

export class AppDivisionQueryPaging
  implements ISearchRequestPaging<AppDivisionCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppDivisionCursor

  @ApiProperty()
  pageSize: number
}

export class AppDivisionSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppDivisionFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppDivisionSearchParameters
  implements
    IEntitySearchParameters<
      AppDivisionEntity,
      AppDivisionSorting,
      AppDivisionCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppDivisionFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppDivisionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppDivisionQuerySorting

  @ApiProperty({ required: false })
  paging?: AppDivisionQueryPaging

  @ApiProperty({ required: false })
  options?: AppDivisionSearchOptions

  relations?: ISearchQueryRelations<AppDivisionEntity>
}

export class AppDivisionSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppDivisionCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppDivisionCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppDivisionCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppDivisionCursor
}

export class AppDivisionSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppDivisionEntity,
      AppDivisionSearchParameters,
      TResult,
      AppDivisionSorting,
      AppDivisionCursor,
      AppDivisionFacets
    >
{
  @ApiProperty()
  request: AppDivisionSearchParameters

  @ApiProperty()
  facets?: AppDivisionFacets

  @ApiProperty()
  paging?: AppDivisionSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppDivisionDeleteParameters
  implements IEntitiesDeleteParameters<AppDivisionSorting>
{
  @ApiProperty({ required: false })
  filters?: AppDivisionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppDivisionQuerySorting
}

export class AppDivisionVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppDivisionCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppDivisionCursor>
}

export class AppDivisionVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppDivisionCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppDivisionCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppDivisionCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppDivisionCursor
}

export class AppDivisionVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppDivisionCursor>
{
  @ApiProperty()
  paging?: AppDivisionVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
