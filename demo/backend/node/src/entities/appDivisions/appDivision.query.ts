import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppDivisionEntity,
  AppDivisionEntityId,
} from "../../database/core/entities/appDivision.entity"
import {
  AppDivisionFacets,
  AppDivisionSorting,
} from "./appDivision.models"
import { AppDivisionSearchParameters } from "./appDivision.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appDivision")
export class AppDivisionQueryBuilder extends NestTypeOrmQueryBuilder<
  AppDivisionEntity,
  AppDivisionEntityId,
  AppDivisionSearchParameters,
  AppDivisionSorting,
  AppDivisionFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDivision", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppDivisionEntity> | FindOptionsWhere<AppDivisionEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppDivisionSearchParameters
  ): FindOptionsOrder<AppDivisionEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppDivisionSearchParameters
  ): FindOptionsWhere<AppDivisionEntity> | FindOptionsWhere<AppDivisionEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppDivisionSearchParameters,
    context?: AppAuthContext
  ): Promise<AppDivisionFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
