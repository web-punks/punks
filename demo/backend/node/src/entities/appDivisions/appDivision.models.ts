import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppDivisionEntity } from "../../database/core/entities/appDivision.entity"

export type AppDivisionCreateData = DeepPartial<Omit<AppDivisionEntity, "id">>
export type AppDivisionUpdateData = DeepPartial<Omit<AppDivisionEntity, "id">>

export enum AppDivisionSorting {
  Name = "Name",
}

export type AppDivisionCursor = number

export class AppDivisionSearchFilters {}

export class AppDivisionFacets {}

export type AppDivisionSheetItem = AppDivisionEntity
