import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppEmailLogAdapter } from "./appEmailLog.adapter"
import { AppEmailLogAuthMiddleware } from "./appEmailLog.authentication"
import { AppEmailLogEntityManager } from "./appEmailLog.manager"
import { AppEmailLogQueryBuilder } from "./appEmailLog.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppEmailLogAdapter,
    AppEmailLogAuthMiddleware,
    AppEmailLogEntityManager,
    AppEmailLogQueryBuilder,
  ],
  exports: [AppEmailLogEntityManager],
})
export class AppEmailLogEntityModule {}
