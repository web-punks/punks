import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppEmailLogEntity } from "../../database/core/entities/appEmailLog.entity"
import {
  AppEmailLogSorting,
  AppEmailLogCursor,
  AppEmailLogSearchFilters,
  AppEmailLogFacets,
} from "./appEmailLog.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppEmailLogSearchSortingField
  implements ISearchSortingField<AppEmailLogSorting>
{
  @ApiProperty({ enum: AppEmailLogSorting })
  field: AppEmailLogSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppEmailLogQuerySorting
  implements ISearchSorting<AppEmailLogSorting>
{
  @ApiProperty({ required: false, type: [AppEmailLogSearchSortingField] })
  fields: AppEmailLogSearchSortingField[]
}

export class AppEmailLogQueryPaging
  implements ISearchRequestPaging<AppEmailLogCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppEmailLogCursor

  @ApiProperty()
  pageSize: number
}

export class AppEmailLogSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppEmailLogFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppEmailLogSearchParameters
  implements
    IEntitySearchParameters<
      AppEmailLogEntity,
      AppEmailLogSorting,
      AppEmailLogCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppEmailLogFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppEmailLogSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppEmailLogQuerySorting

  @ApiProperty({ required: false })
  paging?: AppEmailLogQueryPaging

  @ApiProperty({ required: false })
  options?: AppEmailLogSearchOptions

  relations?: ISearchQueryRelations<AppEmailLogEntity>
}

export class AppEmailLogSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppEmailLogCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppEmailLogCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppEmailLogCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppEmailLogCursor
}

export class AppEmailLogSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppEmailLogEntity,
      AppEmailLogSearchParameters,
      TResult,
      AppEmailLogSorting,
      AppEmailLogCursor,
      AppEmailLogFacets
    >
{
  @ApiProperty()
  request: AppEmailLogSearchParameters

  @ApiProperty()
  facets?: AppEmailLogFacets

  @ApiProperty()
  paging?: AppEmailLogSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppEmailLogDeleteParameters
  implements IEntitiesDeleteParameters<AppEmailLogSorting>
{
  @ApiProperty({ required: false })
  filters?: AppEmailLogSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppEmailLogQuerySorting
}

export class AppEmailLogVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppEmailLogCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppEmailLogCursor>
}

export class AppEmailLogVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppEmailLogCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppEmailLogCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppEmailLogCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppEmailLogCursor
}

export class AppEmailLogVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppEmailLogCursor>
{
  @ApiProperty()
  paging?: AppEmailLogVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
