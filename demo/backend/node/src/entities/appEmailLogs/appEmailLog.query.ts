import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppEmailLogEntity,
  AppEmailLogEntityId,
} from "@/database/core/entities/appEmailLog.entity"
import { AppEmailLogFacets, AppEmailLogSorting } from "./appEmailLog.models"
import { AppEmailLogSearchParameters } from "./appEmailLog.types"
import {
  AppAuthContext,
  AppUserContext,
} from "@/infrastructure/authentication/types"

@WpEntityQueryBuilder("appEmailLog")
export class AppEmailLogQueryBuilder extends NestTypeOrmQueryBuilder<
  AppEmailLogEntity,
  AppEmailLogEntityId,
  AppEmailLogSearchParameters,
  AppEmailLogSorting,
  AppEmailLogFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEmailLog", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ):
    | FindOptionsWhere<AppEmailLogEntity>
    | FindOptionsWhere<AppEmailLogEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppEmailLogSearchParameters
  ): FindOptionsOrder<AppEmailLogEntity> {
    switch (request.sorting?.fields[0]?.field) {
      case AppEmailLogSorting.EmailType:
        return {
          emailType: request.sorting?.fields[0]?.direction,
        }
      case AppEmailLogSorting.Timestamp:
      default:
        return {
          createdOn: request.sorting?.fields[0]?.direction,
        }
    }
  }

  protected buildWhereClause(
    request: AppEmailLogSearchParameters
  ):
    | FindOptionsWhere<AppEmailLogEntity>
    | FindOptionsWhere<AppEmailLogEntity>[] {
    return {}
  }

  protected calculateFacets(
    request: AppEmailLogSearchParameters,
    context?: AppAuthContext
  ): Promise<AppEmailLogFacets> {
    return Promise.resolve({})
  }
}
