import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppEmailLogCreateData,
  AppEmailLogCursor,
  AppEmailLogFacets,
  AppEmailLogSorting,
  AppEmailLogUpdateData,
} from "./appEmailLog.models"
import {
  AppEmailLogSearchParameters,
  AppEmailLogDeleteParameters,
} from "./appEmailLog.types"
import {
  AppEmailLogEntity,
  AppEmailLogEntityId,
} from "../../database/core/entities/appEmailLog.entity"

@WpEntityManager("appEmailLog")
export class AppEmailLogEntityManager extends NestEntityManager<
  AppEmailLogEntity,
  AppEmailLogEntityId,
  AppEmailLogCreateData,
  AppEmailLogUpdateData,
  AppEmailLogDeleteParameters,
  AppEmailLogSearchParameters,
  AppEmailLogSorting,
  AppEmailLogCursor,
  AppEmailLogFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEmailLog", registry)
  }
}
