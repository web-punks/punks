import { AppCompanyEntityModule } from "./appCompanies/appCompany.module"
import { AppDivisionEntityModule } from "./appDivisions/appDivision.module"
import { AppEmailLogEntityModule } from "./appEmailLogs/appEmailLog.module"
import { AppEventLogEntityModule } from "./appEventLogs/appEventLog.module"
import { AppEntityVersionEntityModule } from "./appEntityVersions/appEntityVersion.module"
import { AppFileReferenceEntityModule } from "./appFileReferences/appFileReference.module"
import { AppOrganizationEntityModule } from "./appOrganizations/appOrganization.module"
import { AppLanguageEntityModule } from "./appLanguages/appLanguage.module"
import { AppRoleEntityModule } from "./appRoles/appRole.module"
import { AppTenantEntityModule } from "./appTenants/appTenant.module"
import { AppUserGroupMemberEntityModule } from "./appUserGroupMembers/appUserGroupMember.module"
import { AppUserGroupEntityModule } from "./appUserGroups/appUserGroup.module"
import { AppUserProfileEntityModule } from "./appUserProfiles/appUserProfile.module"
import { AppUserRoleEntityModule } from "./appUserRoles/appUserRole.module"
import { AppUserEntityModule } from "./appUsers/appUser.module"
import { AppDirectoryEntityModule } from "./appDirectories/appDirectory.module"
import { AppRoleOrganizationalUnitTypeEntityModule } from "./appRoleOrganizationalUnitTypes/appRoleOrganizationalUnitType.module"
import { AppRoleAssignableRoleEntityModule } from "./appRoleAssignableRoles/appRoleAssignableRole.module"
import { AppPermissionEntityModule } from "./appPermissions/appPermission.module"
import { AppRolePermissionEntityModule } from "./appRolePermissions/appRolePermission.module"
import { AppCacheEntryEntityModule } from "./appCacheEntries/appCacheEntry.module"
import { CrmContactEntityModule } from "./crmContacts/crmContact.module"
import { FooEntityModule } from "./foos/foo.module"
import { AppOperationLockEntityModule } from "./appOperationLocks/appOperationLock.module"
import { AppJobDefinitionEntityModule } from "./appJobDefinitions/appJobDefinition.module"
import { AppJobInstanceEntityModule } from "./appJobInstances/appJobInstance.module"

export const EntityModules = [
  AppCacheEntryEntityModule,
  AppCompanyEntityModule,
  AppDivisionEntityModule,
  AppDirectoryEntityModule,
  AppEmailLogEntityModule,
  AppEventLogEntityModule,
  AppEntityVersionEntityModule,
  AppFileReferenceEntityModule,
  AppJobDefinitionEntityModule,
  AppJobInstanceEntityModule,
  AppLanguageEntityModule,
  AppOperationLockEntityModule,
  AppOrganizationEntityModule,
  AppRoleOrganizationalUnitTypeEntityModule,
  AppRoleEntityModule,
  AppRoleAssignableRoleEntityModule,
  AppRolePermissionEntityModule,
  AppPermissionEntityModule,
  AppTenantEntityModule,
  AppUserGroupMemberEntityModule,
  AppUserGroupEntityModule,
  AppUserProfileEntityModule,
  AppUserRoleEntityModule,
  AppUserEntityModule,
  CrmContactEntityModule,
  FooEntityModule,
]
