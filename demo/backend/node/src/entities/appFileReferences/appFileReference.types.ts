import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppFileReferenceEntity } from "../../database/core/entities/appFileReference.entity"
import {
  AppFileReferenceSorting,
  AppFileReferenceCursor,
  AppFileReferenceSearchFilters,
  AppFileReferenceFacets,
} from "./appFileReference.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppFileReferenceSearchSortingField
  implements ISearchSortingField<AppFileReferenceSorting>
{
  @ApiProperty({ enum: AppFileReferenceSorting })
  field: AppFileReferenceSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppFileReferenceQuerySorting
  implements ISearchSorting<AppFileReferenceSorting>
{
  @ApiProperty({ required: false, type: [AppFileReferenceSearchSortingField] })
  fields: AppFileReferenceSearchSortingField[]
}

export class AppFileReferenceQueryPaging
  implements ISearchRequestPaging<AppFileReferenceCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppFileReferenceCursor

  @ApiProperty()
  pageSize: number
}

export class AppFileReferenceSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppFileReferenceFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppFileReferenceSearchParameters
  implements
    IEntitySearchParameters<
      AppFileReferenceEntity,
      AppFileReferenceSorting,
      AppFileReferenceCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppFileReferenceFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppFileReferenceSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppFileReferenceQuerySorting

  @ApiProperty({ required: false })
  paging?: AppFileReferenceQueryPaging

  @ApiProperty({ required: false })
  options?: AppFileReferenceSearchOptions

  relations?: ISearchQueryRelations<AppFileReferenceEntity>
}

export class AppFileReferenceSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppFileReferenceCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppFileReferenceCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppFileReferenceCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppFileReferenceCursor
}

export class AppFileReferenceSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppFileReferenceEntity,
      AppFileReferenceSearchParameters,
      TResult,
      AppFileReferenceSorting,
      AppFileReferenceCursor,
      AppFileReferenceFacets
    >
{
  @ApiProperty()
  request: AppFileReferenceSearchParameters

  @ApiProperty()
  facets?: AppFileReferenceFacets

  @ApiProperty()
  paging?: AppFileReferenceSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppFileReferenceDeleteParameters
  implements IEntitiesDeleteParameters<AppFileReferenceSorting>
{
  @ApiProperty({ required: false })
  filters?: AppFileReferenceSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppFileReferenceQuerySorting
}

export class AppFileReferenceVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppFileReferenceCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppFileReferenceCursor>
}

export class AppFileReferenceVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppFileReferenceCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppFileReferenceCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppFileReferenceCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppFileReferenceCursor
}

export class AppFileReferenceVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppFileReferenceCursor>
{
  @ApiProperty()
  paging?: AppFileReferenceVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
