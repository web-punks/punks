import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppFileReferenceEntity } from "../../database/core/entities/appFileReference.entity"
import { AppFileReferenceCreateData, AppFileReferenceUpdateData } from "./appFileReference.models"

@WpEntityAdapter("appFileReference")
export class AppFileReferenceAdapter
  implements
    IEntityAdapter<
      AppFileReferenceEntity,
      AppFileReferenceCreateData,
      AppFileReferenceUpdateData
    >
{
  createDataToEntity(data: AppFileReferenceCreateData): DeepPartial<AppFileReferenceEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppFileReferenceUpdateData): DeepPartial<AppFileReferenceEntity> {
    return {
      ...data,
    }
  }
}
