import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppFileReferenceCreateData,
  AppFileReferenceCursor,
  AppFileReferenceFacets,
  AppFileReferenceSorting,
  AppFileReferenceUpdateData,
} from "./appFileReference.models"
import {
  AppFileReferenceSearchParameters,
  AppFileReferenceDeleteParameters,
} from "./appFileReference.types"
import {
  AppFileReferenceEntity,
  AppFileReferenceEntityId,
} from "../../database/core/entities/appFileReference.entity"

@WpEntityManager("appFileReference")
export class AppFileReferenceEntityManager extends NestEntityManager<
  AppFileReferenceEntity,
  AppFileReferenceEntityId,
  AppFileReferenceCreateData,
  AppFileReferenceUpdateData,
  AppFileReferenceDeleteParameters,
  AppFileReferenceSearchParameters,
  AppFileReferenceSorting,
  AppFileReferenceCursor,
  AppFileReferenceFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appFileReference", registry)
  }
}
