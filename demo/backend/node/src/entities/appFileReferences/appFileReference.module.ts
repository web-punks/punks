import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppFileReferenceAdapter } from "./appFileReference.adapter"
import { AppFileReferenceAuthMiddleware } from "./appFileReference.authentication"
import { AppFileReferenceEntityManager } from "./appFileReference.manager"
import { AppFileReferenceQueryBuilder } from "./appFileReference.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppFileReferenceAdapter,
    AppFileReferenceAuthMiddleware,
    AppFileReferenceEntityManager,
    AppFileReferenceQueryBuilder,
  ],
  exports: [AppFileReferenceEntityManager],
})
export class AppFileReferenceEntityModule {}
