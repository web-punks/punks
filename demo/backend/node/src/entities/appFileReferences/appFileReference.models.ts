import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppFileReferenceEntity } from "../../database/core/entities/appFileReference.entity"

export type AppFileReferenceCreateData = DeepPartial<Omit<AppFileReferenceEntity, "id">>
export type AppFileReferenceUpdateData = DeepPartial<Omit<AppFileReferenceEntity, "id">>

export enum AppFileReferenceSorting {
  Name = "Name",
}

export type AppFileReferenceCursor = number

export class AppFileReferenceSearchFilters {}

export class AppFileReferenceFacets {}

export type AppFileReferenceSheetItem = AppFileReferenceEntity
