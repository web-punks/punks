import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppFileReferenceEntity,
  AppFileReferenceEntityId,
} from "../../database/core/entities/appFileReference.entity"
import {
  AppFileReferenceFacets,
  AppFileReferenceSorting,
} from "./appFileReference.models"
import { AppFileReferenceSearchParameters } from "./appFileReference.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appFileReference")
export class AppFileReferenceQueryBuilder extends NestTypeOrmQueryBuilder<
  AppFileReferenceEntity,
  AppFileReferenceEntityId,
  AppFileReferenceSearchParameters,
  AppFileReferenceSorting,
  AppFileReferenceFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appFileReference", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppFileReferenceEntity> | FindOptionsWhere<AppFileReferenceEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppFileReferenceSearchParameters
  ): FindOptionsOrder<AppFileReferenceEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppFileReferenceSearchParameters
  ): FindOptionsWhere<AppFileReferenceEntity> | FindOptionsWhere<AppFileReferenceEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppFileReferenceSearchParameters,
    context?: AppAuthContext
  ): Promise<AppFileReferenceFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
