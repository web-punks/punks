import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppRoleAdapter } from "./appRole.adapter"
import { AppRoleAuthMiddleware } from "./appRole.authentication"
import { AppRoleEntityManager } from "./appRole.manager"
import { AppRoleQueryBuilder } from "./appRole.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppRoleAdapter,
    AppRoleAuthMiddleware,
    AppRoleEntityManager,
    AppRoleQueryBuilder,
  ],
  exports: [AppRoleEntityManager],
})
export class AppRoleEntityModule {}
