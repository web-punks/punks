import {
  FindOptionsOrder,
  FindOptionsWhere,
  FindOptionsRelations,
  In,
} from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppRoleEntity,
  AppRoleEntityId,
} from "../../database/core/entities/appRole.entity"
import { AppRoleFacets, AppRoleSorting } from "./appRole.models"
import { AppRoleSearchParameters } from "./appRole.types"
import {
  AppUserContext,
  AppAuthContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appRole")
export class AppRoleQueryBuilder extends NestTypeOrmQueryBuilder<
  AppRoleEntity,
  AppRoleEntityId,
  AppRoleSearchParameters,
  AppRoleSorting,
  AppRoleFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRole", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext | undefined
  ): FindOptionsWhere<AppRoleEntity> | FindOptionsWhere<AppRoleEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppRoleSearchParameters
  ): FindOptionsOrder<AppRoleEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {
          name: "ASC",
        }
    }
  }

  protected buildWhereClause(
    request: AppRoleSearchParameters
  ): FindOptionsWhere<AppRoleEntity> | FindOptionsWhere<AppRoleEntity>[] {
    return {
      ...(request.filters?.uid ? { uid: In(request.filters.uid) } : {}),
    }
  }

  protected calculateFacets(
    request: AppRoleSearchParameters,
    context?: AppAuthContext | undefined
  ): Promise<AppRoleFacets | undefined> {
    return Promise.resolve(undefined)
  }

  protected getRelationsToLoad(
    request: AppRoleSearchParameters,
    context?: AppAuthContext | undefined
  ): FindOptionsRelations<AppRoleEntity> | undefined {
    return {
      allowedOrganizationalUnitTypes: {
        type: true,
      },
      assignableRoles: {
        assignableRole: true,
      },
      permissions: {
        permission: true,
      },
    }
  }
}
