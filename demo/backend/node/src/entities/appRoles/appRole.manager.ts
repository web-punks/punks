import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppRoleCreateData,
  AppRoleCursor,
  AppRoleFacets,
  AppRoleSorting,
  AppRoleUpdateData,
} from "./appRole.models"
import {
  AppRoleSearchParameters,
  AppRoleDeleteParameters,
} from "./appRole.types"
import {
  AppRoleEntity,
  AppRoleEntityId,
} from "../../database/core/entities/appRole.entity"

@WpEntityManager("appRole")
export class AppRoleEntityManager extends NestEntityManager<
  AppRoleEntity,
  AppRoleEntityId,
  AppRoleCreateData,
  AppRoleUpdateData,
  AppRoleDeleteParameters,
  AppRoleSearchParameters,
  AppRoleSorting,
  AppRoleCursor,
  AppRoleFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRole", registry)
  }

  async getByUid(uid: string): Promise<AppRoleEntity | undefined> {
    const { items } = await this.manager.search.execute({
      filters: { uid: [uid] },
    })
    return items[0]
  }
}
