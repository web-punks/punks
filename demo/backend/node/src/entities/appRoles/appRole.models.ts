import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppRoleEntity } from "../../database/core/entities/appRole.entity"

export type AppRoleCreateData = DeepPartial<Omit<AppRoleEntity, "id">>
export type AppRoleUpdateData = DeepPartial<Omit<AppRoleEntity, "id">>

export enum AppRoleSorting {
  Name = "Name",
}

export type AppRoleCursor = number

export class AppRoleSearchFilters {
  @ApiProperty({ required: false })
  uid?: string[]
}

export class AppRoleFacets {}

export type AppRoleSheetItem = AppRoleEntity
