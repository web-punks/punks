import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppRoleEntity } from "../../database/core/entities/appRole.entity"
import {
  AppRoleSorting,
  AppRoleCursor,
  AppRoleSearchFilters,
  AppRoleFacets,
} from "./appRole.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppRoleSearchSortingField
  implements ISearchSortingField<AppRoleSorting>
{
  @ApiProperty({ enum: AppRoleSorting })
  field: AppRoleSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppRoleQuerySorting implements ISearchSorting<AppRoleSorting> {
  @ApiProperty({ required: false, type: [AppRoleSearchSortingField] })
  fields: AppRoleSearchSortingField[]
}

export class AppRoleQueryPaging implements ISearchRequestPaging<AppRoleCursor> {
  @ApiProperty({ required: false })
  cursor?: AppRoleCursor

  @ApiProperty()
  pageSize: number
}

export class AppRoleSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppRoleFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppRoleSearchParameters
  implements
    IEntitySearchParameters<AppRoleEntity, AppRoleSorting, AppRoleCursor>
{
  @ApiProperty({ required: false })
  query?: AppRoleFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppRoleSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppRoleQuerySorting

  @ApiProperty({ required: false })
  paging?: AppRoleQueryPaging

  @ApiProperty({ required: false })
  options?: AppRoleSearchOptions

  relations?: ISearchQueryRelations<AppRoleEntity>
}

export class AppRoleSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppRoleCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppRoleCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppRoleCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppRoleCursor
}

export class AppRoleSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppRoleEntity,
      AppRoleSearchParameters,
      TResult,
      AppRoleSorting,
      AppRoleCursor,
      AppRoleFacets
    >
{
  @ApiProperty()
  request: AppRoleSearchParameters

  @ApiProperty()
  facets?: AppRoleFacets

  @ApiProperty()
  paging?: AppRoleSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppRoleDeleteParameters
  implements IEntitiesDeleteParameters<AppRoleSorting>
{
  @ApiProperty({ required: false })
  filters?: AppRoleSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppRoleQuerySorting
}

export class AppRoleVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppRoleCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppRoleCursor>
}

export class AppRoleVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppRoleCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppRoleCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppRoleCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppRoleCursor
}

export class AppRoleVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppRoleCursor>
{
  @ApiProperty()
  paging?: AppRoleVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
