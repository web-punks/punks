import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppPermissionEntity } from "../../database/core/entities/appPermission.entity"
import { StringFilter } from "@/shared/api/fields"

export type AppPermissionCreateData = DeepPartial<
  Omit<AppPermissionEntity, "id">
>
export type AppPermissionUpdateData = DeepPartial<
  Omit<AppPermissionEntity, "id">
>

export enum AppPermissionSorting {
  Name = "Name",
}

export type AppPermissionCursor = number

export class AppPermissionSearchFilters {
  @ApiProperty({ required: false })
  id?: StringFilter

  @ApiProperty({ required: false })
  uid?: StringFilter
}

export class AppPermissionFacets {}

export type AppPermissionSheetItem = AppPermissionEntity
