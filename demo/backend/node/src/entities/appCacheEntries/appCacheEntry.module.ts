import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppCacheEntryAdapter } from "./appCacheEntry.adapter"
import { AppCacheEntryAuthMiddleware } from "./appCacheEntry.authentication"
import { AppCacheEntryEntityManager } from "./appCacheEntry.manager"
import { AppCacheEntryQueryBuilder } from "./appCacheEntry.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppCacheEntryAdapter,
    AppCacheEntryAuthMiddleware,
    AppCacheEntryEntityManager,
    AppCacheEntryQueryBuilder,
  ],
  exports: [AppCacheEntryEntityManager],
})
export class AppCacheEntryEntityModule {}
