import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppCacheEntryEntity,
  AppCacheEntryEntityId,
} from "../../database/core/entities/appCacheEntry.entity"
import {
  AppCacheEntryFacets,
  AppCacheEntrySorting,
} from "./appCacheEntry.models"
import { AppCacheEntrySearchParameters } from "./appCacheEntry.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appCacheEntry")
export class AppCacheEntryQueryBuilder extends NestTypeOrmQueryBuilder<
  AppCacheEntryEntity,
  AppCacheEntryEntityId,
  AppCacheEntrySearchParameters,
  AppCacheEntrySorting,
  AppCacheEntryFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appCacheEntry", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppCacheEntryEntity> | FindOptionsWhere<AppCacheEntryEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppCacheEntrySearchParameters
  ): FindOptionsOrder<AppCacheEntryEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppCacheEntrySearchParameters
  ): FindOptionsWhere<AppCacheEntryEntity> | FindOptionsWhere<AppCacheEntryEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppCacheEntrySearchParameters,
    context?: AppAuthContext
  ): Promise<AppCacheEntryFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
