import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppCacheEntryEntity } from "../../database/core/entities/appCacheEntry.entity"
import {
  AppCacheEntrySorting,
  AppCacheEntryCursor,
  AppCacheEntrySearchFilters,
  AppCacheEntryFacets,
} from "./appCacheEntry.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppCacheEntrySearchSortingField
  implements ISearchSortingField<AppCacheEntrySorting>
{
  @ApiProperty({ enum: AppCacheEntrySorting })
  field: AppCacheEntrySorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppCacheEntryQuerySorting
  implements ISearchSorting<AppCacheEntrySorting>
{
  @ApiProperty({ required: false, type: [AppCacheEntrySearchSortingField] })
  fields: AppCacheEntrySearchSortingField[]
}

export class AppCacheEntryQueryPaging
  implements ISearchRequestPaging<AppCacheEntryCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppCacheEntryCursor

  @ApiProperty()
  pageSize: number
}

export class AppCacheEntrySearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppCacheEntryFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppCacheEntrySearchParameters
  implements
    IEntitySearchParameters<
      AppCacheEntryEntity,
      AppCacheEntrySorting,
      AppCacheEntryCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppCacheEntryFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppCacheEntrySearchFilters

  @ApiProperty({ required: false })
  sorting?: AppCacheEntryQuerySorting

  @ApiProperty({ required: false })
  paging?: AppCacheEntryQueryPaging

  @ApiProperty({ required: false })
  options?: AppCacheEntrySearchOptions

  relations?: ISearchQueryRelations<AppCacheEntryEntity>
}

export class AppCacheEntrySearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppCacheEntryCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppCacheEntryCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppCacheEntryCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppCacheEntryCursor
}

export class AppCacheEntrySearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppCacheEntryEntity,
      AppCacheEntrySearchParameters,
      TResult,
      AppCacheEntrySorting,
      AppCacheEntryCursor,
      AppCacheEntryFacets
    >
{
  @ApiProperty()
  request: AppCacheEntrySearchParameters

  @ApiProperty()
  facets?: AppCacheEntryFacets

  @ApiProperty()
  paging?: AppCacheEntrySearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppCacheEntryDeleteParameters
  implements IEntitiesDeleteParameters<AppCacheEntrySorting>
{
  @ApiProperty({ required: false })
  filters?: AppCacheEntrySearchFilters

  @ApiProperty({ required: false })
  sorting?: AppCacheEntryQuerySorting
}

export class AppCacheEntryVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppCacheEntryCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppCacheEntryCursor>
}

export class AppCacheEntryVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppCacheEntryCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppCacheEntryCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppCacheEntryCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppCacheEntryCursor
}

export class AppCacheEntryVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppCacheEntryCursor>
{
  @ApiProperty()
  paging?: AppCacheEntryVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
