import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppCacheEntryEntity } from "../../database/core/entities/appCacheEntry.entity"
import { AppCacheEntryCreateData, AppCacheEntryUpdateData } from "./appCacheEntry.models"

@WpEntityAdapter("appCacheEntry")
export class AppCacheEntryAdapter
  implements
    IEntityAdapter<
      AppCacheEntryEntity,
      AppCacheEntryCreateData,
      AppCacheEntryUpdateData
    >
{
  createDataToEntity(data: AppCacheEntryCreateData): DeepPartial<AppCacheEntryEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppCacheEntryUpdateData): DeepPartial<AppCacheEntryEntity> {
    return {
      ...data,
    }
  }
}
