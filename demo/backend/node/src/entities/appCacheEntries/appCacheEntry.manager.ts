import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppCacheEntryCreateData,
  AppCacheEntryCursor,
  AppCacheEntryFacets,
  AppCacheEntrySorting,
  AppCacheEntryUpdateData,
} from "./appCacheEntry.models"
import {
  AppCacheEntrySearchParameters,
  AppCacheEntryDeleteParameters,
} from "./appCacheEntry.types"
import {
  AppCacheEntryEntity,
  AppCacheEntryEntityId,
} from "../../database/core/entities/appCacheEntry.entity"

@WpEntityManager("appCacheEntry")
export class AppCacheEntryEntityManager extends NestEntityManager<
  AppCacheEntryEntity,
  AppCacheEntryEntityId,
  AppCacheEntryCreateData,
  AppCacheEntryUpdateData,
  AppCacheEntryDeleteParameters,
  AppCacheEntrySearchParameters,
  AppCacheEntrySorting,
  AppCacheEntryCursor,
  AppCacheEntryFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appCacheEntry", registry)
  }
}
