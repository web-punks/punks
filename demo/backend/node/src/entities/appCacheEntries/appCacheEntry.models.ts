import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppCacheEntryEntity } from "../../database/core/entities/appCacheEntry.entity"

export type AppCacheEntryCreateData = DeepPartial<Omit<AppCacheEntryEntity, "id">>
export type AppCacheEntryUpdateData = DeepPartial<Omit<AppCacheEntryEntity, "id">>

export enum AppCacheEntrySorting {
  Name = "Name",
}

export type AppCacheEntryCursor = number

export class AppCacheEntrySearchFilters {}

export class AppCacheEntryFacets {}

export type AppCacheEntrySheetItem = AppCacheEntryEntity
