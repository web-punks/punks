import {
  IAuthorizationResult,
  WpEntityAuthMiddleware,
} from "@punks/backend-entity-manager"
import {
  AppAuthContext,
  AppEntityAuthorizationMiddlewareBase,
} from "../../infrastructure/authentication/index"
import { AppOperationLockEntity } from "../../database/core/entities/appOperationLock.entity"

@WpEntityAuthMiddleware("appOperationLock")
export class AppOperationLockAuthMiddleware extends AppEntityAuthorizationMiddlewareBase<AppOperationLockEntity> {
  async canSearch(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.defaultCanSearch(context)
  }

  async canRead(
    entity: Partial<AppOperationLockEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanRead(entity, context)
  }

  async canCreate(
    entity: Partial<AppOperationLockEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanCreate(entity, context)
  }

  async canUpdate(
    entity: Partial<AppOperationLockEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanUpdate(entity, context)
  }

  async canDelete(
    entity: Partial<AppOperationLockEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanDelete(entity, context)
  }

  async canDeleteItems(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.defaultCanDeleteItems(context)
  }
}
