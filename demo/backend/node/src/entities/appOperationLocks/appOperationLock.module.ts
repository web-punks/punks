import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppOperationLockAdapter } from "./appOperationLock.adapter"
import { AppOperationLockAuthMiddleware } from "./appOperationLock.authentication"
import { AppOperationLockEntityManager } from "./appOperationLock.manager"
import { AppOperationLockQueryBuilder } from "./appOperationLock.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppOperationLockAdapter,
    AppOperationLockAuthMiddleware,
    AppOperationLockEntityManager,
    AppOperationLockQueryBuilder,
  ],
  exports: [AppOperationLockEntityManager],
})
export class AppOperationLockEntityModule {}
