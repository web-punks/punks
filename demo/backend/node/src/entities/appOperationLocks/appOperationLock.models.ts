import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppOperationLockEntity } from "../../database/core/entities/appOperationLock.entity"
import { IdFilter } from "@/shared/api/fields"

export type AppOperationLockCreateData = DeepPartial<Omit<AppOperationLockEntity, "id">>
export type AppOperationLockUpdateData = DeepPartial<Omit<AppOperationLockEntity, "id">>

export enum AppOperationLockSorting {
  Name = "Name",
}

export type AppOperationLockCursor = number

export class AppOperationLockSearchFilters {
  @ApiProperty({ required: false })
  id?: IdFilter
}

export class AppOperationLockFacets {}

export type AppOperationLockSheetItem = AppOperationLockEntity
