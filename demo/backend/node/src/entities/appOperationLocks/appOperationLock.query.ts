import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppOperationLockEntity,
  AppOperationLockEntityId,
} from "../../database/core/entities/appOperationLock.entity"
import {
  AppOperationLockFacets,
  AppOperationLockSorting,
} from "./appOperationLock.models"
import { AppOperationLockSearchParameters } from "./appOperationLock.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appOperationLock")
export class AppOperationLockQueryBuilder extends NestTypeOrmQueryBuilder<
  AppOperationLockEntity,
  AppOperationLockEntityId,
  AppOperationLockSearchParameters,
  AppOperationLockSorting,
  AppOperationLockFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOperationLock", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppOperationLockEntity> | FindOptionsWhere<AppOperationLockEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppOperationLockSearchParameters
  ): FindOptionsOrder<AppOperationLockEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppOperationLockSearchParameters
  ): FindOptionsWhere<AppOperationLockEntity> | FindOptionsWhere<AppOperationLockEntity>[] {
    // todo: implement query filters
    return {
      ...(request.filters?.id && {
        id: this.clause.idFilter(request.filters.id),
      }),
    }
  }

  protected calculateFacets(
    request: AppOperationLockSearchParameters,
    context?: AppAuthContext
  ): Promise<AppOperationLockFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
