import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppOperationLockCreateData,
  AppOperationLockCursor,
  AppOperationLockFacets,
  AppOperationLockSorting,
  AppOperationLockUpdateData,
} from "./appOperationLock.models"
import {
  AppOperationLockSearchParameters,
  AppOperationLockDeleteParameters,
} from "./appOperationLock.types"
import {
  AppOperationLockEntity,
  AppOperationLockEntityId,
} from "../../database/core/entities/appOperationLock.entity"

@WpEntityManager("appOperationLock")
export class AppOperationLockEntityManager extends NestEntityManager<
  AppOperationLockEntity,
  AppOperationLockEntityId,
  AppOperationLockCreateData,
  AppOperationLockUpdateData,
  AppOperationLockDeleteParameters,
  AppOperationLockSearchParameters,
  AppOperationLockSorting,
  AppOperationLockCursor,
  AppOperationLockFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOperationLock", registry)
  }
}
