import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppOperationLockEntity } from "../../database/core/entities/appOperationLock.entity"
import {
  AppOperationLockSorting,
  AppOperationLockCursor,
  AppOperationLockSearchFilters,
  AppOperationLockFacets,
} from "./appOperationLock.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppOperationLockSearchSortingField
  implements ISearchSortingField<AppOperationLockSorting>
{
  @ApiProperty({ enum: AppOperationLockSorting })
  field: AppOperationLockSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppOperationLockQuerySorting
  implements ISearchSorting<AppOperationLockSorting>
{
  @ApiProperty({ required: false, type: [AppOperationLockSearchSortingField] })
  fields: AppOperationLockSearchSortingField[]
}

export class AppOperationLockQueryPaging
  implements ISearchRequestPaging<AppOperationLockCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppOperationLockCursor

  @ApiProperty()
  pageSize: number
}

export class AppOperationLockSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppOperationLockFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppOperationLockSearchParameters
  implements
    IEntitySearchParameters<
      AppOperationLockEntity,
      AppOperationLockSorting,
      AppOperationLockCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppOperationLockFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppOperationLockSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppOperationLockQuerySorting

  @ApiProperty({ required: false })
  paging?: AppOperationLockQueryPaging

  @ApiProperty({ required: false })
  options?: AppOperationLockSearchOptions

  relations?: ISearchQueryRelations<AppOperationLockEntity>
}

export class AppOperationLockSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppOperationLockCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppOperationLockCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppOperationLockCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppOperationLockCursor
}

export class AppOperationLockSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppOperationLockEntity,
      AppOperationLockSearchParameters,
      TResult,
      AppOperationLockSorting,
      AppOperationLockCursor,
      AppOperationLockFacets
    >
{
  @ApiProperty()
  request: AppOperationLockSearchParameters

  @ApiProperty()
  facets?: AppOperationLockFacets

  @ApiProperty()
  paging?: AppOperationLockSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppOperationLockDeleteParameters
  implements IEntitiesDeleteParameters<AppOperationLockSorting>
{
  @ApiProperty({ required: false })
  filters?: AppOperationLockSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppOperationLockQuerySorting
}

export class AppOperationLockVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppOperationLockCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppOperationLockCursor>
}

export class AppOperationLockVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppOperationLockCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppOperationLockCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppOperationLockCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppOperationLockCursor
}

export class AppOperationLockVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppOperationLockCursor>
{
  @ApiProperty()
  paging?: AppOperationLockVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
