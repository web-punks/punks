import {
  DeepPartial,
  IAuthenticationContext,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppOperationLockEntity } from "../../database/core/entities/appOperationLock.entity"
import { AppOperationLockCreateData, AppOperationLockUpdateData } from "./appOperationLock.models"

@WpEntityAdapter("appOperationLock")
export class AppOperationLockAdapter
  implements
    IEntityAdapter<
      AppOperationLockEntity,
      AppOperationLockCreateData,
      AppOperationLockUpdateData
    >
{
  createDataToEntity(
    data: AppOperationLockCreateData, 
    context: IAuthenticationContext<unknown>
  ): DeepPartial<AppOperationLockEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppOperationLockUpdateData,
    context: IAuthenticationContext<unknown>
  ): DeepPartial<AppOperationLockEntity> {
    return {
      ...data,
    }
  }
}
