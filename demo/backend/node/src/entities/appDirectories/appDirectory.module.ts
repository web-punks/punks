import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppDirectoryAdapter } from "./appDirectory.adapter"
import { AppDirectoryAuthMiddleware } from "./appDirectory.authentication"
import { AppDirectoryEntityManager } from "./appDirectory.manager"
import { AppDirectoryQueryBuilder } from "./appDirectory.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppDirectoryAdapter,
    AppDirectoryAuthMiddleware,
    AppDirectoryEntityManager,
    AppDirectoryQueryBuilder,
  ],
  exports: [AppDirectoryEntityManager],
})
export class AppDirectoryEntityModule {}
