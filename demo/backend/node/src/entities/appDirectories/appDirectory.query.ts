import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import { isNullOrUndefined } from "@punks/backend-core"
import {
  AppDirectoryEntity,
  AppDirectoryEntityId,
} from "../../database/core/entities/appDirectory.entity"
import { AppDirectoryFacets, AppDirectorySorting } from "./appDirectory.models"
import { AppDirectorySearchParameters } from "./appDirectory.types"
import {
  AppUserContext,
  AppAuthContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appDirectory")
export class AppDirectoryQueryBuilder extends NestTypeOrmQueryBuilder<
  AppDirectoryEntity,
  AppDirectoryEntityId,
  AppDirectorySearchParameters,
  AppDirectorySorting,
  AppDirectoryFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDirectory", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ):
    | FindOptionsWhere<AppDirectoryEntity>
    | FindOptionsWhere<AppDirectoryEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppDirectorySearchParameters
  ): FindOptionsOrder<AppDirectoryEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppDirectorySearchParameters
  ):
    | FindOptionsWhere<AppDirectoryEntity>
    | FindOptionsWhere<AppDirectoryEntity>[] {
    return {
      ...(request.filters?.uid && { uid: request.filters.uid }),
      ...(!isNullOrUndefined(request.filters?.default) && {
        default: request.filters?.default,
      }),
      ...(request.filters?.tenantUid && {
        tenant: { uid: request.filters.tenantUid },
      }),
    }
  }

  protected calculateFacets(
    request: AppDirectorySearchParameters,
    context?: AppAuthContext
  ): Promise<AppDirectoryFacets> {
    return Promise.resolve({})
  }
}
