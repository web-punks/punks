import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppDirectoryCreateData,
  AppDirectoryCursor,
  AppDirectoryFacets,
  AppDirectorySorting,
  AppDirectoryUpdateData,
} from "./appDirectory.models"
import {
  AppDirectorySearchParameters,
  AppDirectoryDeleteParameters,
} from "./appDirectory.types"
import {
  AppDirectoryEntity,
  AppDirectoryEntityId,
} from "../../database/core/entities/appDirectory.entity"

@WpEntityManager("appDirectory")
export class AppDirectoryEntityManager extends NestEntityManager<
  AppDirectoryEntity,
  AppDirectoryEntityId,
  AppDirectoryCreateData,
  AppDirectoryUpdateData,
  AppDirectoryDeleteParameters,
  AppDirectorySearchParameters,
  AppDirectorySorting,
  AppDirectoryCursor,
  AppDirectoryFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDirectory", registry)
  }
}
