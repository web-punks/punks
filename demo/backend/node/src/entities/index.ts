import { Module } from "@nestjs/common"
import { EntityModules } from "./modules"

@Module({
  imports: [...EntityModules],
  exports: [...EntityModules],
})
export class EntitiesModule {}
