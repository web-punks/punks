import {
  IAuthorizationResult,
  WpEntityAuthMiddleware,
} from "@punks/backend-entity-manager"
import {
  AppAuthContext,
  AppEntityAuthorizationMiddlewareBase,
} from "../../infrastructure/authentication/index"
import { AppJobInstanceEntity } from "../../database/core/entities/appJobInstance.entity"

@WpEntityAuthMiddleware("appJobInstance")
export class AppJobInstanceAuthMiddleware extends AppEntityAuthorizationMiddlewareBase<AppJobInstanceEntity> {
  async canSearch(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.defaultCanSearch(context)
  }

  async canRead(
    entity: Partial<AppJobInstanceEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanRead(entity, context)
  }

  async canCreate(
    entity: Partial<AppJobInstanceEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanCreate(entity, context)
  }

  async canUpdate(
    entity: Partial<AppJobInstanceEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanUpdate(entity, context)
  }

  async canDelete(
    entity: Partial<AppJobInstanceEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanDelete(entity, context)
  }

  async canDeleteItems(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.defaultCanDeleteItems(context)
  }
}
