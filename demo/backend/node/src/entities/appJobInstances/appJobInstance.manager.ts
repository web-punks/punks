import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "@punks/backend-entity-manager"
import {
  AppJobInstanceCreateData,
  AppJobInstanceCursor,
  AppJobInstanceFacets,
  AppJobInstanceSorting,
  AppJobInstanceUpdateData,
} from "./appJobInstance.models"
import {
  AppJobInstanceSearchParameters,
  AppJobInstanceDeleteParameters,
} from "./appJobInstance.types"
import {
  AppJobInstanceEntity,
  AppJobInstanceEntityId,
} from "../../database/core/entities/appJobInstance.entity"

@WpEntityManager("appJobInstance")
export class AppJobInstanceEntityManager extends NestEntityManager<
  AppJobInstanceEntity,
  AppJobInstanceEntityId,
  AppJobInstanceCreateData,
  AppJobInstanceUpdateData,
  AppJobInstanceDeleteParameters,
  AppJobInstanceSearchParameters,
  AppJobInstanceSorting,
  AppJobInstanceCursor,
  AppJobInstanceFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appJobInstance", registry)
  }
}
