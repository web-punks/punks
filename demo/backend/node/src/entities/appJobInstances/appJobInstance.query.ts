import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppJobInstanceEntity,
  AppJobInstanceEntityId,
} from "../../database/core/entities/appJobInstance.entity"
import {
  AppJobInstanceFacets,
  AppJobInstanceSorting,
} from "./appJobInstance.models"
import { AppJobInstanceSearchParameters } from "./appJobInstance.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appJobInstance")
export class AppJobInstanceQueryBuilder extends NestTypeOrmQueryBuilder<
  AppJobInstanceEntity,
  AppJobInstanceEntityId,
  AppJobInstanceSearchParameters,
  AppJobInstanceSorting,
  AppJobInstanceFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appJobInstance", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppJobInstanceEntity> | FindOptionsWhere<AppJobInstanceEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppJobInstanceSearchParameters
  ): FindOptionsOrder<AppJobInstanceEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppJobInstanceSearchParameters
  ): FindOptionsWhere<AppJobInstanceEntity> | FindOptionsWhere<AppJobInstanceEntity>[] {
    // todo: implement query filters
    return {
      ...(request.filters?.id && {
        id: this.clause.idFilter(request.filters.id),
      }),
    }
  }

  protected calculateFacets(
    request: AppJobInstanceSearchParameters,
    context?: AppAuthContext
  ): Promise<AppJobInstanceFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
