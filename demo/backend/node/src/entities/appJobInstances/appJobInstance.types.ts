import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { AppJobInstanceEntity } from "../../database/core/entities/appJobInstance.entity"
import {
  AppJobInstanceSorting,
  AppJobInstanceCursor,
  AppJobInstanceSearchFilters,
  AppJobInstanceFacets,
} from "./appJobInstance.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "@/shared/api/versioning"

export class AppJobInstanceSearchSortingField
  implements ISearchSortingField<AppJobInstanceSorting>
{
  @ApiProperty({ enum: AppJobInstanceSorting })
  field: AppJobInstanceSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppJobInstanceQuerySorting
  implements ISearchSorting<AppJobInstanceSorting>
{
  @ApiProperty({ required: false, type: [AppJobInstanceSearchSortingField] })
  fields: AppJobInstanceSearchSortingField[]
}

export class AppJobInstanceQueryPaging
  implements ISearchRequestPaging<AppJobInstanceCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppJobInstanceCursor

  @ApiProperty()
  pageSize: number
}

export class AppJobInstanceSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppJobInstanceFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppJobInstanceSearchParameters
  implements
    IEntitySearchParameters<
      AppJobInstanceEntity,
      AppJobInstanceSorting,
      AppJobInstanceCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppJobInstanceFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppJobInstanceSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppJobInstanceQuerySorting

  @ApiProperty({ required: false })
  paging?: AppJobInstanceQueryPaging

  @ApiProperty({ required: false })
  options?: AppJobInstanceSearchOptions

  relations?: ISearchQueryRelations<AppJobInstanceEntity>
}

export class AppJobInstanceSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppJobInstanceCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppJobInstanceCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppJobInstanceCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppJobInstanceCursor
}

export class AppJobInstanceSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppJobInstanceEntity,
      AppJobInstanceSearchParameters,
      TResult,
      AppJobInstanceSorting,
      AppJobInstanceCursor,
      AppJobInstanceFacets
    >
{
  @ApiProperty()
  request: AppJobInstanceSearchParameters

  @ApiProperty()
  facets?: AppJobInstanceFacets

  @ApiProperty()
  paging?: AppJobInstanceSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppJobInstanceDeleteParameters
  implements IEntitiesDeleteParameters<AppJobInstanceSorting>
{
  @ApiProperty({ required: false })
  filters?: AppJobInstanceSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppJobInstanceQuerySorting
}

export class AppJobInstanceVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppJobInstanceCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppJobInstanceCursor>
}

export class AppJobInstanceVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppJobInstanceCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppJobInstanceCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppJobInstanceCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppJobInstanceCursor
}

export class AppJobInstanceVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppJobInstanceCursor>
{
  @ApiProperty()
  paging?: AppJobInstanceVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
