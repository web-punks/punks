import {
  DeepPartial,
  IAuthenticationContext,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "@punks/backend-entity-manager"
import { AppJobInstanceEntity } from "../../database/core/entities/appJobInstance.entity"
import { AppJobInstanceCreateData, AppJobInstanceUpdateData } from "./appJobInstance.models"

@WpEntityAdapter("appJobInstance")
export class AppJobInstanceAdapter
  implements
    IEntityAdapter<
      AppJobInstanceEntity,
      AppJobInstanceCreateData,
      AppJobInstanceUpdateData
    >
{
  createDataToEntity(
    data: AppJobInstanceCreateData, 
    context: IAuthenticationContext<unknown>
  ): DeepPartial<AppJobInstanceEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppJobInstanceUpdateData,
    context: IAuthenticationContext<unknown>
  ): DeepPartial<AppJobInstanceEntity> {
    return {
      ...data,
    }
  }
}
