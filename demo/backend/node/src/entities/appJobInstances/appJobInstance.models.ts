import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppJobInstanceEntity } from "../../database/core/entities/appJobInstance.entity"
import { IdFilter } from "@/shared/api/fields"

export type AppJobInstanceCreateData = DeepPartial<Omit<AppJobInstanceEntity, "id">>
export type AppJobInstanceUpdateData = DeepPartial<Omit<AppJobInstanceEntity, "id">>

export enum AppJobInstanceSorting {
  Name = "Name",
}

export type AppJobInstanceCursor = number

export class AppJobInstanceSearchFilters {
  @ApiProperty({ required: false })
  id?: IdFilter
}

export class AppJobInstanceFacets {}

export type AppJobInstanceSheetItem = AppJobInstanceEntity
