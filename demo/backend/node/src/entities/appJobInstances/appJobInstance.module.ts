import { Module } from "@nestjs/common"
import { EntityManagerModule } from "@punks/backend-entity-manager"
import { AppJobInstanceAdapter } from "./appJobInstance.adapter"
import { AppJobInstanceAuthMiddleware } from "./appJobInstance.authentication"
import { AppJobInstanceEntityManager } from "./appJobInstance.manager"
import { AppJobInstanceQueryBuilder } from "./appJobInstance.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppJobInstanceAdapter,
    AppJobInstanceAuthMiddleware,
    AppJobInstanceEntityManager,
    AppJobInstanceQueryBuilder,
  ],
  exports: [AppJobInstanceEntityManager],
})
export class AppJobInstanceEntityModule {}
