import { SharedModule } from "@/shared/module"
import { Module } from "@nestjs/common"
import { AwsSecretsModule } from "@punks/backend-entity-manager"
import { SettingsServices } from "./services"

@Module({
  imports: [SharedModule, AwsSecretsModule],
  providers: [...SettingsServices],
  exports: [...SettingsServices],
})
export class SettingsMiddlewareModule {}
