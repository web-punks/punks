import { AppSecretDefinition } from "@punks/backend-entity-manager"

type DefaultSettingsMap = {
  [key: string]: Omit<AppSecretDefinition, "key">
}

export const DefaultSettings: DefaultSettingsMap = {
  MY_API_KEY: {
    type: "string",
    hidden: true,
  },
  OTHER_SECRET: {
    type: "string",
    hidden: true,
  },
  MAGIC_NUMBER: {
    type: "number",
    hidden: false,
    defaultValue: 42,
  },
}
