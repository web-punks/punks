import { AuthenticationMiddlewareModule } from "./authentication/module"
import { CacheMiddlewareModule } from "./cache/module"
import { SettingsMiddlewareModule } from "./settings/module"

export const MiddlewareModules = [
  AuthenticationMiddlewareModule,
  CacheMiddlewareModule,
  SettingsMiddlewareModule,
]
