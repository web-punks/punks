import { SharedModule } from "@/shared/module"
import { Global, Module } from "@nestjs/common"
import { CacheProviders } from "./providers"
import { CacheInfrastructureModule } from "@/infrastructure/cache/module"

@Global()
@Module({
  imports: [SharedModule, CacheInfrastructureModule],
  providers: [...CacheProviders],
  exports: [...CacheProviders],
})
export class CacheMiddlewareModule {}
