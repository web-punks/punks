import {
  AppOrganizationalUnitTree,
  AppOrganizationalUnitsDescendantsMap,
} from "@/entities/appOrganizationalUnits/appOrganizationalUnit.models"
import { DefaultCacheInstance } from "@/infrastructure/cache/providers/default-instance"
import { Injectable } from "@nestjs/common"

@Injectable()
export class OrganizationCacheService {
  constructor(private readonly instance: DefaultCacheInstance) {}

  async retrieveOrganizationalUnitsTree(
    valueFactory: () => Promise<AppOrganizationalUnitTree>
  ) {
    return await this.instance.retrieve<AppOrganizationalUnitTree>(
      "organizational-unit-tree",
      {
        valueFactory,
        ttl: {
          value: 1,
          unit: "minutes",
        },
      }
    )
  }

  async retrieveOrganizationalUnitDescendantsMap(
    valueFactory: () => Promise<AppOrganizationalUnitsDescendantsMap>
  ) {
    return await this.instance.retrieve<AppOrganizationalUnitsDescendantsMap>(
      "organizational-unit-descendants-map",
      {
        valueFactory,
        ttl: {
          value: 1,
          unit: "minutes",
        },
      }
    )
  }
}
