import { AppPermissions } from "../permissions"

export enum AppRoleType {
  TenantAdmin = "tenant-admin",
  OrganizationAdmin = "organization-admin",
  OrganizationManager = "organization-manager",
  Teacher = "teacher",
  Principal = "principal",
  Partner = "partner",
}

export const AppRoles = {
  TenantAdmin: {
    uid: AppRoleType.TenantAdmin,
    name: "Tenant Admin",
    permissions: [
      AppPermissions.AuditFullAccess,
      AppPermissions.CalendarsFullAccess,
      AppPermissions.CrmFullAccess,
      AppPermissions.JsonInspect,
      AppPermissions.OrganizationFullAccess,
      AppPermissions.ProjectsFullAccess,
      AppPermissions.TenantFullAccess,
      AppPermissions.UserImpersonate,
    ],
  },
  OrganizationAdmin: {
    uid: AppRoleType.OrganizationAdmin,
    name: "Organization Admin",
    permissions: [
      AppPermissions.AuditFullAccess,
      AppPermissions.CalendarsFullAccess,
      AppPermissions.CrmFullAccess,
      AppPermissions.JsonInspect,
      AppPermissions.OrganizationFullAccess,
      AppPermissions.ProjectsFullAccess,
    ],
  },
  OrganizationManager: {
    uid: AppRoleType.OrganizationManager,
    name: "Organization Manager",
    permissions: [
      AppPermissions.AuditFullAccess,
      AppPermissions.CalendarsFullAccess,
      AppPermissions.CrmFullAccess,
      AppPermissions.OrganizationFullAccess,
      AppPermissions.ProjectsFullAccess,
    ],
  },
  Teacher: {
    uid: AppRoleType.Teacher,
    name: "Teacher",
    permissions: [
      AppPermissions.CalendarsScopedAccess,
      AppPermissions.CrmScopedAccess,
    ],
  },
  Principal: {
    uid: AppRoleType.Principal,
    name: "Principal",
    permissions: [
      AppPermissions.CalendarsScopedAccess,
      AppPermissions.CrmScopedAccess,
    ],
  },
  Partner: {
    uid: AppRoleType.Partner,
    name: "Partner",
    permissions: [AppPermissions.CalendarsScopedAccess],
  },
}
