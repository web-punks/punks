export enum AppPermissionType {
  TenantFullAccess = "tenant-full-access",
  OrganizationFullAccess = "organization-full-access",
  CalendarsFullAccess = "calendars-full-access",
  CalendarsScopedAccess = "calendars-scoped-access",
  CrmFullAccess = "crm-full-access",
  CrmScopedAccess = "crm-scoped-access",
  ProjectsFullAccess = "projects-full-access",
  AuditFullAccess = "audit-full-access",
  JsonInspect = "json-inspect",
  UserImpersonate = "user-impersonate",
}

export const AppPermissions = {
  TenantFullAccess: {
    uid: AppPermissionType.TenantFullAccess,
    name: "Tenant Full Access",
    description: "Manage tenant settings",
  },
  OrganizationFullAccess: {
    uid: AppPermissionType.OrganizationFullAccess,
    name: "Organization Full Access",
    description: "Manage organization settings",
  },
  CalendarsFullAccess: {
    uid: AppPermissionType.CalendarsFullAccess,
    name: "Calendars Full Access",
    description: "Manage all calendars",
  },
  CalendarsScopedAccess: {
    uid: AppPermissionType.CalendarsScopedAccess,
    name: "Calendars Scoped Access",
    description: "Manage calendars limited to the user organizational unit",
  },
  CrmFullAccess: {
    uid: AppPermissionType.CrmFullAccess,
    name: "Crm Full Access",
    description: "Manage all crm data",
  },
  CrmScopedAccess: {
    uid: AppPermissionType.CrmScopedAccess,
    name: "Crm Scoped Access",
    description: "Manage crm data limited to the user organizational unit",
  },
  ProjectsFullAccess: {
    uid: AppPermissionType.ProjectsFullAccess,
    name: "Projects Full Access",
    description: "Manage all projects",
  },
  AuditFullAccess: {
    uid: AppPermissionType.AuditFullAccess,
    name: "Audit Full Access",
  },
  JsonInspect: {
    uid: AppPermissionType.JsonInspect,
    name: "Json Inspect",
  },
  UserImpersonate: {
    uid: AppPermissionType.UserImpersonate,
    name: "User Impersonate",
  },
}
