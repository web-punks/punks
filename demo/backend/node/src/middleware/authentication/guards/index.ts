import { buildPermissionsGuard } from "@punks/backend-entity-manager"
import { AppPermissions } from "../permissions"

export const UserImpersonate = () =>
  buildPermissionsGuard({
    permissions: [AppPermissions.UserImpersonate],
  })

export const TenantManagers = () =>
  buildPermissionsGuard({
    permissions: [AppPermissions.TenantFullAccess],
  })

export const OrganizationManagers = () =>
  buildPermissionsGuard({
    permissions: [AppPermissions.OrganizationFullAccess],
  })

export const CalendarsManagers = () =>
  buildPermissionsGuard({
    permissions: [
      AppPermissions.CalendarsFullAccess,
      AppPermissions.CalendarsScopedAccess,
    ],
  })

export const CrmManagers = () =>
  buildPermissionsGuard({
    permissions: [AppPermissions.CrmFullAccess, AppPermissions.CrmScopedAccess],
  })

export const ProjectManagers = () =>
  buildPermissionsGuard({
    permissions: [AppPermissions.ProjectsFullAccess],
  })

export const AuditManagers = () =>
  buildPermissionsGuard({
    permissions: [AppPermissions.AuditFullAccess],
  })
