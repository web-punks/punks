import * as request from "supertest"
import { createTestServer, NestTestServerPorts } from "@/testing"

describe("AppController", () => {
  let httpServer: any
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { app, terminate } = await createTestServer({
      createWebServer: true,
      serverStartPort: NestTestServerPorts.AppController,
    })
    httpServer = app.getHttpServer()
    terminateServer = terminate
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should return healthy", async () => {
    const response = await request(httpServer).get("/health").send().expect(200)

    expect(response.body).toMatchObject({
      healthy: true,
    })
  })
})
