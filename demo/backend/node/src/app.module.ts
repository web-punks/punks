import { Module } from "@nestjs/common"
import { TypeOrmModule } from "@nestjs/typeorm"
import { ConfigModule } from "@nestjs/config"
import { APP_FILTER, APP_GUARD } from "@nestjs/core"
import { EventEmitterModule } from "@nestjs/event-emitter"
import {
  AuthenticationModule,
  AwsBucketModule,
  AwsSecretsModule,
  SendgridEmailModule,
  AuthGuard,
} from "@punks/backend-entity-manager"
import { AppController } from "./app.controller"
import { ormConnectionOptions } from "./app.ormconfig"
import { SharedModule } from "./shared/module"
import { AppExceptionsFilter } from "./shared/errors/middleware"
import { AppModules } from "./app"
import { DatabaseModules } from "./database"
import { EntitiesModule } from "./entities"
import { FeatureModules } from "./features"
import { InfrastructureModules } from "./infrastructure"
import {
  getAuthSettings,
  getSendgridSettings,
  getAwsBucketSettings,
  getAwsSecretsSettings,
} from "./settings"
import { MessagingModule } from "./messaging/module"
import { AppEventHandlersModule } from "./eventHandlers/module"
import { MiddlewareModules } from "./middleware"

@Module({
  imports: [
    SharedModule,
    ConfigModule.forRoot(),
    EventEmitterModule.forRoot({
      delimiter: ":",
      wildcard: true,
    }),
    TypeOrmModule.forRoot(ormConnectionOptions.core()),
    SendgridEmailModule.forRoot(getSendgridSettings()),
    AwsBucketModule.forRoot(getAwsBucketSettings()),
    AwsSecretsModule.forRoot(getAwsSecretsSettings()),
    AuthenticationModule.forRoot(getAuthSettings()),
    EntitiesModule,
    MessagingModule,
    AppEventHandlersModule,
    ...MiddlewareModules,
    ...AppModules,
    ...FeatureModules,
    ...InfrastructureModules,
    ...DatabaseModules,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AppExceptionsFilter,
    },
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {}
