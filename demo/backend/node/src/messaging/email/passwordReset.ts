import {
  AuthenticationEmailTemplates,
  PasswordResetEmailPayload,
  SendgridEmailTemplate,
  WpSendgridEmailTemplate,
} from "@punks/backend-entity-manager"

@WpSendgridEmailTemplate(AuthenticationEmailTemplates.PasswordReset, {
  type: "html",
  subjectTemplate: "Reimposta password",
  bodyTemplate: `
<div style="background-color: #f2f2f2; margin:0; padding:70px 0; width:100%">
  <div style="max-width: 600px; margin-left: auto; margin-right: auto; color: #333 !important; border: 1px solid #dedede; background-color: #ffffff;">
    <div style="background-color: #333; color: #fff; padding:36px 48px;display:block; text-align: center;">
      <img width="600px" style="max-width: 200px;" src="{{ siteLogo }}"/>
    </div>
    <div style="padding: 32px 48px 56px; text-align: center; font-size: 16px;">
      Ciao {{ firstName }},<br/>
      clicca <a href="{{ callbackUrl }}" style="text-decoration: none;">questo link</a> per reimpostare la tua password.
    </div>
  </div>
  <div style="padding: 24px 0; text-align: center; font-size: 16px;">
    <a href="https://{{ siteUrl }}" target="blank">{{ siteUrl }}</a>
  </div>
</div>
  `,
})
export class PasswordResetEmailTemplate extends SendgridEmailTemplate<PasswordResetEmailPayload> {}
