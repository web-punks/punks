import {
  AuthenticationEmailTemplates,
  RegistrationEmailPayload,
  SendgridEmailTemplate,
  WpSendgridEmailTemplate,
} from "@punks/backend-entity-manager"

@WpSendgridEmailTemplate(AuthenticationEmailTemplates.Registration, {
  type: "html",
  subjectTemplate: "Ci siamo quasi!",
  bodyTemplate: `
<div style="background-color: #f2f2f2; margin:0; padding:70px 0; width:100%">
  <div style="max-width: 600px; margin-left: auto; margin-right: auto; color: #333 !important; border: 1px solid #dedede; background-color: #ffffff;">
    <div style="background-color: #333; color: #fff; padding:36px 48px;display:block; text-align: center;">
      <img width="600px" style="max-width: 200px;" src="{{ siteLogo }}"/>
    </div>
    <div style="padding: 32px 48px 56px; text-align: center; font-size: 16px;">
      {{ firstName }} ci siamo quasi,<br/>
      clicca <a href="{{ callbackUrl }}" style="text-decoration: none;">questo link</a> per verificare il tuo indirizzo mail.
    </div>
  </div>
  <div style="padding: 24px 0; text-align: center; font-size: 16px;">
    <a href="https://{{ siteUrl }}" target="blank">{{ siteUrl }}</a>
  </div>
</div>
  `,
})
export class RegistrationEmailTemplate extends SendgridEmailTemplate<RegistrationEmailPayload> {}
