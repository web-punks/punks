import { EmilVerifyEmailTemplate } from "./emailVerify"
import { PasswordResetEmailTemplate } from "./passwordReset"
import { RegistrationEmailTemplate } from "./registration"

export const AuthEmailTemplates = [
  EmilVerifyEmailTemplate,
  RegistrationEmailTemplate,
  PasswordResetEmailTemplate,
]
