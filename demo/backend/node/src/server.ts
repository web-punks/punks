import { INestApplication } from "@nestjs/common"
import {
  ConsoleLogger,
  Log,
  LogLevel,
  MetaSerializationType,
} from "@punks/backend-core"
import { ModulesContainer } from "@nestjs/core"
import { EntityManagerInitializer } from "@punks/backend-entity-manager"
import { setupSwagger } from "./infrastructure/swagger/initialize"
import { getEntityManagerSettings } from "./settings"
import { AppAuthContextProvider } from "./app/appAuth/context"

export const setupServer = async (app: INestApplication) => {
  Log.configure({
    provider: new ConsoleLogger(),
    options: {
      enabled: true,
      level: LogLevel.Debug,
      serialization: MetaSerializationType.None,
    },
  })

  if (process.env.SWAGGER_ENABLED?.toString() === "true") {
    Log.getLogger("Main").info("Setting up swagger")
    setupSwagger(app, {
      apiPath: "swagger",
      description: "WebPunks Demo Api",
      title: "WebPunks Demo Api",
      version: "1.0",
    })
  } else {
    Log.getLogger("Main").info("Swagger is disabled")
  }

  await app.get(EntityManagerInitializer).initialize(app, {
    modulesContainer: app.get(ModulesContainer),
    authenticationProvider: app.get(AppAuthContextProvider),
    settings: getEntityManagerSettings(),
  })
  await app.init()
}
