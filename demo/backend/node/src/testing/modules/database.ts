import { getRepositoryToken } from "@nestjs/typeorm"
import { DataSource } from "typeorm"
import { DynamicModule, Module } from "@nestjs/common"
import { CoreDatabaseEntities } from "@/database/core/entities"
import { CoreDatabaseRepositories } from "@/database/core/repositories"

export const mockDatabaseProviders = (dataSource: DataSource) => {
  return CoreDatabaseEntities.map((entity) => ({
    provide: getRepositoryToken(entity, "core"),
    useValue: dataSource.getRepository(entity),
  }))
}

@Module({})
export class CoreDatabaseTestingModule {
  static forRoot(dataSource: DataSource): DynamicModule {
    const repositories = mockDatabaseProviders(dataSource)
    return {
      global: true,
      module: CoreDatabaseTestingModule,
      providers: [
        ...repositories,
        ...CoreDatabaseEntities,
        ...CoreDatabaseRepositories,
      ],
      exports: [
        ...repositories,
        ...CoreDatabaseEntities,
        ...CoreDatabaseRepositories,
      ],
    }
  }
}
