
export const NestTestServerPorts = {
  AppController: 50000,
  TenantInitPipeline: 50100,
  OrganizationRegisterPipeline: 50200,
}
