import { AppDirectoryEntity } from "@/database/core/entities/appDirectory.entity"
import { AppOrganizationEntity } from "@/database/core/entities/appOrganization.entity"
import { AppTenantEntity } from "@/database/core/entities/appTenant.entity"

export type ApplicationAuthenticationContext = {
  tenant: AppTenantEntity
  organization: AppOrganizationEntity
  directory: AppDirectoryEntity
}
