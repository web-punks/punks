import { DataSource } from "typeorm"
import { newDb } from "pg-mem"
import { newUuid } from "@punks/backend-entity-manager"

export const mockPostgresDatabase = async ({
  entities,
}: {
  entities: any[]
}) => {
  const db = newDb({
    autoCreateForeignKeyIndices: true,
  })
  db.public.registerFunction({
    implementation: () => "test",
    name: "current_database",
  })
  db.public.registerFunction({
    implementation: () => "15.3",
    name: "version",
  })
  const dataSource: DataSource = db.adapters.createTypeormDataSource({
    name: newUuid(),
    type: "postgres",
    entities,
  })
  await dataSource.initialize()
  await dataSource.synchronize()
  return {
    db,
    dataSource,
  }
}
