import * as express from "express"
import { APP_FILTER, APP_GUARD, ModulesContainer } from "@nestjs/core"
import { Test } from "@nestjs/testing"
import { ExpressAdapter } from "@nestjs/platform-express"
import { EventEmitterModule } from "@nestjs/event-emitter"
import {
  ConsoleLogger,
  Log,
  LogLevel,
  MetaSerializationType,
} from "@punks/backend-core"
import {
  AuthenticationModule,
  AuthGuard,
  EntityManagerInitializer,
  InMemoryBucketProvider,
  InMemoryEmailProvider,
} from "@punks/backend-entity-manager"
import { AppExceptionsFilter } from "@/shared/errors/middleware"
import { AuthenticationInfrastructureModule } from "@/infrastructure/authentication/module"
import { EventsInfrastructureModule } from "@/infrastructure/events/module"
import { EmailInfrastructureModule } from "@/infrastructure/email/module"
import { EntityVersioningProvider } from "@/infrastructure/versioning/providers/entity-versioning"
import { CoreDatabaseEntities } from "@/database/core/entities"
import { CoreDatabaseRepositories } from "@/database/core/repositories"
import { MessagingModule } from "@/messaging/module"
import { EntitiesModule } from "@/entities"
import { AppAdminAppModule } from "@/app/appAdmin/appAdmin.module"
import { AppController } from "@/app.controller"
import { mockDatabaseProviders } from "./modules/database"
import { mockPostgresDatabase } from "./mocks/postgres"
import { AppAuthContextProvider } from "@/app/appAuth/context"

const startPort = 9000
let currentPort = startPort

const getAvailablePort = () => {
  return currentPort++
}

export const createTestServer = async (options?: {
  createWebServer?: boolean
  serverStartPort?: number
  extraModules?: any[]
  extraControllers?: any[]
  extraProviders?: any[]
  enableLogging?: boolean
  disableAuthentication?: boolean
  disableAuthorization?: boolean
}) => {
  if (options?.enableLogging) {
    Log.configure({
      provider: new ConsoleLogger(),
      options: {
        enabled: true,
        level: LogLevel.Debug,
        serialization: MetaSerializationType.None,
      },
    })
  }
  if (options?.serverStartPort) {
    currentPort = options?.serverStartPort
  }
  const { dataSource } = await mockPostgresDatabase({
    entities: CoreDatabaseEntities,
  })
  const server = await Test.createTestingModule({
    imports: [
      EventEmitterModule.forRoot({
        delimiter: ":",
        wildcard: true,
      }),
      AuthenticationModule.forRoot({
        jwtSecret: "$2a$10$ZWHlR0hxcjwSnNoziP66lO",
        passwordSalt: "$2a$10$MLrSVgluidIHp8IXN0XJNu",
      }),
      AuthenticationInfrastructureModule,
      EventsInfrastructureModule,
      EmailInfrastructureModule,
      EntitiesModule,
      MessagingModule,
      AppAdminAppModule,
      ...(options?.extraModules ? options.extraModules : []),
    ],
    controllers: [AppController, ...(options?.extraControllers ?? [])],
    providers: [
      ...(!options?.disableAuthentication
        ? [
            {
              provide: APP_FILTER,
              useClass: AppExceptionsFilter,
            },
          ]
        : []),
      ...(!options?.disableAuthentication && !options?.disableAuthorization
        ? [
            {
              provide: APP_GUARD,
              useClass: AuthGuard,
            },
          ]
        : []),
      ...mockDatabaseProviders(dataSource),
      ...CoreDatabaseEntities,
      ...CoreDatabaseRepositories,
      EntityVersioningProvider,
      InMemoryEmailProvider,
      InMemoryBucketProvider,
      ...(options?.extraProviders ?? []),
    ],
  }).compile()

  const expressServer = express()
  const app = server.createNestApplication(new ExpressAdapter(expressServer))

  if (options?.createWebServer) {
    await app.listen(getAvailablePort())
  }

  await server.init()

  const initializer = app.get(EntityManagerInitializer)
  await initializer.initialize(app, {
    modulesContainer: app.get(ModulesContainer),
    authenticationProvider: app.get(AppAuthContextProvider),
    settings: {
      importExport: {
        exportBucket: {
          bucket: process.env.ENTITY_MANAGER_IMPORT_EXPORT_BUCKET,
          rootFolderPath:
            process.env.ENTITY_MANAGER_IMPORT_EXPORT_BUCKET_ROOT_FOLDER_PATH,
          publicLinksExpirationMinutes: 60,
        },
      },
      defaultBucketProvider: "inMemory",
      defaultFilesProvider: "inMemory",
      defaultMediaProvider: "inMemory",
    },
  })

  return {
    app,
    server,
    dataSource,
    terminate: async () => {
      await app.close()
    },
  }
}
