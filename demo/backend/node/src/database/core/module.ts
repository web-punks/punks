import { Module, Global } from "@nestjs/common"
import { TypeOrmModule } from "@nestjs/typeorm"
import { CoreDatabaseEntities } from "./entities"
import { CoreDatabaseRepositories } from "./repositories"
import { getEntityManagerProviderToken } from "@punks/backend-entity-manager"
import { AppOperationLockRepository } from "./repositories/appOperationLock.repository"
import { AppJobInstanceRepository } from "./repositories/appJobInstance.repository"
import { AppJobDefinitionRepository } from "./repositories/appJobDefinition.repository"

export const PublicProviders = [
  {
    provide: getEntityManagerProviderToken("OperationsLockRepository"),
    useClass: AppOperationLockRepository,
  },
  {
    provide: getEntityManagerProviderToken("JobInstancesRepository"),
    useClass: AppJobInstanceRepository,
  },
  {
    provide: getEntityManagerProviderToken("JobDefinitionsRepository"),
    useClass: AppJobDefinitionRepository,
  },
]

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([...CoreDatabaseEntities], "core")],
  providers: [
    ...CoreDatabaseRepositories,
    ...CoreDatabaseEntities,
    ...PublicProviders,
  ],
  exports: [...CoreDatabaseRepositories, ...PublicProviders],
})
export class CoreDatabaseModule {}
