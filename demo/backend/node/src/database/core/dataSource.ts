import { DataSource } from "typeorm"
import { config } from "dotenv"

config()

export const coreDataSource = new DataSource({
  type: "postgres",
  url: process.env.CORE_DATABASE_URL,
  synchronize: false,
  logging: true,
  entities: ["src/database/core/entities/**/*.entity.ts"],
  migrations: ["src/database/core/migrations/**/*.ts"],
  subscribers: ["src/database/core/subscriber/**/*.ts"],
  ssl:
    process.env.TYPEORM_USE_SSL === "true"
      ? {
          rejectUnauthorized: false,
        }
      : false,
})
