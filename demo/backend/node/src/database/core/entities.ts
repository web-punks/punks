import { AppCompanyEntity } from "./entities/appCompany.entity"
import { AppDivisionEntity } from "./entities/appDivision.entity"
import { AppEmailLogEntity } from "./entities/appEmailLog.entity"
import { AppEventLogEntity } from "./entities/appEventLog.entity"
import { AppFileReferenceEntity } from "./entities/appFileReference.entity"
import { AppLanguageEntity } from "./entities/appLanguage.entity"
import { AppEntityVersionEntity } from "./entities/appEntityVersion.entity"
import { AppOrganizationEntity } from "./entities/appOrganization.entity"
import { AppRoleEntity } from "./entities/appRole.entity"
import { AppTenantEntity } from "./entities/appTenant.entity"
import { AppUserEntity } from "./entities/appUser.entity"
import { AppUserGroupEntity } from "./entities/appUserGroup.entity"
import { AppUserGroupMemberEntity } from "./entities/appUserGroupMember.entity"
import { AppUserProfileEntity } from "./entities/appUserProfile.entity"
import { AppUserRoleEntity } from "./entities/appUserRole.entity"
import { AppDirectoryEntity } from "./entities/appDirectory.entity"
import { AppOrganizationalUnitEntity } from "./entities/appOrganizationalUnit.entity"
import { AppOrganizationalUnitTypeEntity } from "./entities/appOrganizationalUnitType.entity"
import { AppOrganizationalUnitTypeChildEntity } from "./entities/appOrganizationalUnitTypeChild.entity"
import { AppRoleOrganizationalUnitTypeEntity } from "./entities/appRoleOrganizationalUnitType.entity"
import { AppRoleAssignableRoleEntity } from "./entities/appRoleAssignableRole.entity"
import { AppPermissionEntity } from "./entities/appPermission.entity"
import { AppRolePermissionEntity } from "./entities/appRolePermission.entity"
import { AppCacheEntryEntity } from "./entities/appCacheEntry.entity"
import { CrmContactEntity } from "./entities/crmContact.entity"
import { FooEntity } from "./entities/foo.entity"
import { AppOperationLockEntity } from "./entities/appOperationLock.entity"
import { AppJobDefinitionEntity } from "./entities/appJobDefinition.entity"
import { AppJobInstanceEntity } from "./entities/appJobInstance.entity"

export const CoreDatabaseEntities = [
  AppCacheEntryEntity,
  AppCompanyEntity,
  AppDirectoryEntity,
  AppDivisionEntity,
  AppEntityVersionEntity,
  AppEmailLogEntity,
  AppEventLogEntity,
  AppFileReferenceEntity,
  AppJobDefinitionEntity,
  AppJobInstanceEntity,
  AppLanguageEntity,
  AppOperationLockEntity,
  AppOrganizationEntity,
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeChildEntity,
  AppPermissionEntity,
  AppRoleEntity,
  AppRoleAssignableRoleEntity,
  AppRoleOrganizationalUnitTypeEntity,
  AppRolePermissionEntity,
  AppTenantEntity,
  AppUserEntity,
  AppUserGroupEntity,
  AppUserGroupMemberEntity,
  AppUserProfileEntity,
  AppUserRoleEntity,
  CrmContactEntity,
  FooEntity,
]
