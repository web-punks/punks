import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitTypeChildEntity,
  AppOrganizationalUnitTypeChildEntityId,
} from "../entities/appOrganizationalUnitTypeChild.entity"

@WpEntityRepository("appOrganizationalUnitTypeChild")
export class AppOrganizationalUnitTypeChildRepository extends NestTypeOrmRepository<
  AppOrganizationalUnitTypeChildEntity,
  AppOrganizationalUnitTypeChildEntityId
> {
  constructor(
    @InjectRepository(AppOrganizationalUnitTypeChildEntity, "core")
    repository: Repository<AppOrganizationalUnitTypeChildEntity>
  ) {
    super(repository)
  }
}
