import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppRolePermissionEntity,
  AppRolePermissionEntityId,
} from "../entities/appRolePermission.entity"

@WpEntityRepository("appRolePermission")
export class AppRolePermissionRepository extends NestTypeOrmRepository<
  AppRolePermissionEntity,
  AppRolePermissionEntityId
> {
  constructor(
    @InjectRepository(AppRolePermissionEntity, "core")
    repository: Repository<AppRolePermissionEntity>
  ) {
    super(repository)
  }
}
