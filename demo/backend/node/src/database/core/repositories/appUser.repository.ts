import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import { AppUserEntity, AppUserEntityId } from "../entities/appUser.entity"

@WpEntityRepository("appUser")
export class AppUserRepository extends NestTypeOrmRepository<
  AppUserEntity,
  AppUserEntityId
> {
  constructor(
    @InjectRepository(AppUserEntity, "core")
    repository: Repository<AppUserEntity>
  ) {
    super(repository)
  }

  async getUserDetails(id: string) {
    return await this.innerRepository.findOne({
      where: { id },
      relations: {
        organization: true,
        profile: true,
        tenant: true,
        directory: true,
      },
    })
  }
}
