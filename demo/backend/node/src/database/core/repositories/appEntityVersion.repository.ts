import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppEntityVersionEntity,
  AppEntityVersionEntityId,
} from "../entities/appEntityVersion.entity"

@WpEntityRepository("appEntityVersion")
export class AppEntityVersionRepository extends NestTypeOrmRepository<
  AppEntityVersionEntity,
  AppEntityVersionEntityId
> {
  constructor(
    @InjectRepository(AppEntityVersionEntity, "core")
    repository: Repository<AppEntityVersionEntity>
  ) {
    super(repository)
  }
}
