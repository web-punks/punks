import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import { CrmContactEntity } from "../entities/crmContact.entity"
import { CrmContactEntityId } from "../../../entities/crmContacts/crmContact.models"

@WpEntityRepository("crmContact")
export class CrmContactRepository extends NestTypeOrmRepository<
  CrmContactEntity,
  CrmContactEntityId
> {
  constructor(
    @InjectRepository(CrmContactEntity, "core")
    repository: Repository<CrmContactEntity>
  ) {
    super(repository)
  }
}
