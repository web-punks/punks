import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppCompanyEntity,
  AppCompanyEntityId,
} from "../entities/appCompany.entity"

@WpEntityRepository("appCompany")
export class AppCompanyRepository extends NestTypeOrmRepository<
  AppCompanyEntity,
  AppCompanyEntityId
> {
  constructor(
    @InjectRepository(AppCompanyEntity, "core")
    repository: Repository<AppCompanyEntity>
  ) {
    super(repository)
  }
}
