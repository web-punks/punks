import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppRoleAssignableRoleEntity,
  AppRoleAssignableRoleEntityId,
} from "../entities/appRoleAssignableRole.entity"

@WpEntityRepository("appRoleAssignableRole")
export class AppRoleAssignableRoleRepository extends NestTypeOrmRepository<
  AppRoleAssignableRoleEntity,
  AppRoleAssignableRoleEntityId
> {
  constructor(
    @InjectRepository(AppRoleAssignableRoleEntity, "core")
    repository: Repository<AppRoleAssignableRoleEntity>
  ) {
    super(repository)
  }
}
