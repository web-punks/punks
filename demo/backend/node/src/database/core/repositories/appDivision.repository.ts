import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppDivisionEntity,
  AppDivisionEntityId,
} from "../entities/appDivision.entity"

@WpEntityRepository("appDivision")
export class AppDivisionRepository extends NestTypeOrmRepository<
  AppDivisionEntity,
  AppDivisionEntityId
> {
  constructor(
    @InjectRepository(AppDivisionEntity, "core")
    repository: Repository<AppDivisionEntity>
  ) {
    super(repository)
  }
}
