import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppCacheEntryEntity,
  AppCacheEntryEntityId,
} from "../entities/appCacheEntry.entity"

@WpEntityRepository("appCacheEntry")
export class AppCacheEntryRepository extends NestTypeOrmRepository<
  AppCacheEntryEntity,
  AppCacheEntryEntityId
> {
  constructor(
    @InjectRepository(AppCacheEntryEntity, "core")
    repository: Repository<AppCacheEntryEntity>
  ) {
    super(repository)
  }
}
