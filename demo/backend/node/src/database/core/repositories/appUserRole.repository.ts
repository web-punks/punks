import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppUserRoleEntity,
  AppUserRoleEntityId,
} from "../entities/appUserRole.entity"

@WpEntityRepository("appUserRole")
export class AppUserRoleRepository extends NestTypeOrmRepository<
  AppUserRoleEntity,
  AppUserRoleEntityId
> {
  constructor(
    @InjectRepository(AppUserRoleEntity, "core")
    repository: Repository<AppUserRoleEntity>
  ) {
    super(repository)
  }
}
