import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppDirectoryEntity,
  AppDirectoryEntityId,
} from "../entities/appDirectory.entity"

@WpEntityRepository("appDirectory")
export class AppDirectoryRepository extends NestTypeOrmRepository<
  AppDirectoryEntity,
  AppDirectoryEntityId
> {
  constructor(
    @InjectRepository(AppDirectoryEntity, "core")
    repository: Repository<AppDirectoryEntity>
  ) {
    super(repository)
  }
}
