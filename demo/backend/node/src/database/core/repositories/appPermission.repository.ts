import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppPermissionEntity,
  AppPermissionEntityId,
} from "../entities/appPermission.entity"

@WpEntityRepository("appPermission")
export class AppPermissionRepository extends NestTypeOrmRepository<
  AppPermissionEntity,
  AppPermissionEntityId
> {
  constructor(
    @InjectRepository(AppPermissionEntity, "core")
    repository: Repository<AppPermissionEntity>
  ) {
    super(repository)
  }
}
