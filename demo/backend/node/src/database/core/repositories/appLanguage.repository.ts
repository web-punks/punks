import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppLanguageEntity,
  AppLanguageEntityId,
} from "../entities/appLanguage.entity"

@WpEntityRepository("appLanguage")
export class AppLanguageRepository extends NestTypeOrmRepository<
  AppLanguageEntity,
  AppLanguageEntityId
> {
  constructor(
    @InjectRepository(AppLanguageEntity, "core")
    repository: Repository<AppLanguageEntity>
  ) {
    super(repository)
  }
}
