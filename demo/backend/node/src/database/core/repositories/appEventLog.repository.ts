import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppEventLogEntity,
  AppEventLogEntityId,
} from "../entities/appEventLog.entity"

@WpEntityRepository("appEventLog")
export class AppEventLogRepository extends NestTypeOrmRepository<
  AppEventLogEntity,
  AppEventLogEntityId
> {
  constructor(
    @InjectRepository(AppEventLogEntity, "core")
    repository: Repository<AppEventLogEntity>
  ) {
    super(repository)
  }
}
