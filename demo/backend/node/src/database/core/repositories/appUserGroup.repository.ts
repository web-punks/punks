import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppUserGroupEntity,
  AppUserGroupEntityId,
} from "../entities/appUserGroup.entity"

@WpEntityRepository("appUserGroup")
export class AppUserGroupRepository extends NestTypeOrmRepository<
  AppUserGroupEntity,
  AppUserGroupEntityId
> {
  constructor(
    @InjectRepository(AppUserGroupEntity, "core")
    repository: Repository<AppUserGroupEntity>
  ) {
    super(repository)
  }
}
