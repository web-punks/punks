import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
} from "../entities/appUserGroupMember.entity"

@WpEntityRepository("appUserGroupMember")
export class AppUserGroupMemberRepository extends NestTypeOrmRepository<
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId
> {
  constructor(
    @InjectRepository(AppUserGroupMemberEntity, "core")
    repository: Repository<AppUserGroupMemberEntity>
  ) {
    super(repository)
  }
}
