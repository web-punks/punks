import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppTenantEntity,
  AppTenantEntityId,
} from "../entities/appTenant.entity"

@WpEntityRepository("appTenant")
export class AppTenantRepository extends NestTypeOrmRepository<
  AppTenantEntity,
  AppTenantEntityId
> {
  constructor(
    @InjectRepository(AppTenantEntity, "core")
    repository: Repository<AppTenantEntity>
  ) {
    super(repository)
  }
}
