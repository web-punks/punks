import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId,
} from "../entities/appOrganizationalUnitType.entity"

@WpEntityRepository("appOrganizationalUnitType")
export class AppOrganizationalUnitTypeRepository extends NestTypeOrmRepository<
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId
> {
  constructor(
    @InjectRepository(AppOrganizationalUnitTypeEntity, "core")
    repository: Repository<AppOrganizationalUnitTypeEntity>
  ) {
    super(repository)
  }
}
