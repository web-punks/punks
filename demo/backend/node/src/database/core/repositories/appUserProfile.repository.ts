import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppUserProfileEntity,
  AppUserProfileEntityId,
} from "../entities/appUserProfile.entity"

@WpEntityRepository("appUserProfile")
export class AppUserProfileRepository extends NestTypeOrmRepository<
  AppUserProfileEntity,
  AppUserProfileEntityId
> {
  constructor(
    @InjectRepository(AppUserProfileEntity, "core")
    repository: Repository<AppUserProfileEntity>
  ) {
    super(repository)
  }
}
