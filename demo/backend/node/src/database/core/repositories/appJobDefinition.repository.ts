import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
  IJobDefinitionsRepository,
  JobDefinition,
  newUuid,
} from "@punks/backend-entity-manager"
import {
  AppJobDefinitionEntity,
  AppJobDefinitionEntityId,
} from "../entities/appJobDefinition.entity"

type JobData = Omit<JobDefinition<any, any>, "id" | "createdOn" | "updatedOn">

@WpEntityRepository("appJobDefinition")
export class AppJobDefinitionRepository
  extends NestTypeOrmRepository<
    AppJobDefinitionEntity,
    AppJobDefinitionEntityId
  >
  implements IJobDefinitionsRepository
{
  constructor(
    @InjectRepository(AppJobDefinitionEntity, "core")
    repository: Repository<AppJobDefinitionEntity>
  ) {
    super(repository)
  }

  async getAllDefinitions(): Promise<JobDefinition<unknown, unknown>[]> {
    return await this.innerRepository.find()
  }

  async getDefinitionByUid(
    uid: string
  ): Promise<JobDefinition<unknown, unknown>> {
    return await this.innerRepository.findOne({
      where: {
        uid,
      },
    })
  }

  async ensureDefinition(
    data: JobData
  ): Promise<JobDefinition<unknown, unknown>> {
    const current = await this.getDefinitionByUid(data.uid)
    if (current) {
      return await this.updateDefinition(data.uid, data)
    } else {
      return await this.createDefinition(data)
    }
  }

  async createDefinition(
    data: JobData
  ): Promise<JobDefinition<unknown, unknown>> {
    await this.innerRepository.insert({
      id: newUuid(),
      ...data,
    })
    return await this.getDefinitionByUid(data.uid)
  }

  async updateDefinition(
    uid: string,
    data: JobData
  ): Promise<JobDefinition<unknown, unknown>> {
    await this.innerRepository.update(
      {
        uid,
      },
      {
        ...data,
      }
    )
    return await this.getDefinitionByUid(uid)
  }
}
