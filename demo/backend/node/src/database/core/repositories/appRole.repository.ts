import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import {
  AppRoleEntity,
  AppRoleEntityId,
} from "../entities/appRole.entity"

@WpEntityRepository("appRole")
export class AppRoleRepository extends NestTypeOrmRepository<
  AppRoleEntity,
  AppRoleEntityId
> {
  constructor(
    @InjectRepository(AppRoleEntity, "core")
    repository: Repository<AppRoleEntity>
  ) {
    super(repository)
  }
}
