import { In, Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
  JobInstance,
  JobStatus,
  newUuid,
  JobDefinition,
} from "@punks/backend-entity-manager"
import {
  AppJobInstanceEntity,
  AppJobInstanceEntityId,
} from "../entities/appJobInstance.entity"

@WpEntityRepository("appJobInstance")
export class AppJobInstanceRepository extends NestTypeOrmRepository<
  AppJobInstanceEntity,
  AppJobInstanceEntityId
> {
  constructor(
    @InjectRepository(AppJobInstanceEntity, "core")
    repository: Repository<AppJobInstanceEntity>
  ) {
    super(repository)
  }

  async getById(id: string): Promise<JobInstance> {
    return await this.innerRepository.findOne({
      where: {
        id,
      },
    })
  }

  async getByStatus(...status: JobStatus[]): Promise<JobInstance[]> {
    return await this.innerRepository.find({
      where: {
        status: In(status),
      },
      order: {
        createdOn: "ASC",
      },
    })
  }

  async getLastInstance(
    jobUid: string,
    scheduleUid?: string
  ): Promise<JobInstance> {
    return await this.innerRepository.findOne({
      where: { jobUid, scheduleUid },
      order: {
        insertTime: "DESC",
      },
    })
  }

  async appendInstance(
    job: JobDefinition<unknown, unknown>,
    data: Pick<JobInstance, "id" | "insertTime" | "scheduleUid" | "runType">
  ): Promise<JobInstance> {
    const instanceId = newUuid()
    await this.innerRepository.insert({
      id: instanceId,
      jobUid: job.uid,
      status: JobStatus.Created,
      ...data,
    })
    return await this.getById(instanceId)
  }

  async updateInstance(
    instanceId: string,
    data: Pick<
      JobInstance,
      "error" | "startTime" | "endTime" | "status" | "result"
    >
  ): Promise<JobInstance> {
    await this.innerRepository.update(
      {
        id: instanceId,
      },
      data
    )
    return await this.getById(instanceId)
  }
}
