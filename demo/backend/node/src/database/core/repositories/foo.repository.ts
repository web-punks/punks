import { In, Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
} from "@punks/backend-entity-manager"
import { FooEntity } from "../entities/foo.entity"
import { FooEntityId } from "../../../entities/foos/foo.models"

@WpEntityRepository("foo")
export class FooRepository extends NestTypeOrmRepository<
  FooEntity,
  FooEntityId
> {
  constructor(
    @InjectRepository(FooEntity, "core")
    repository: Repository<FooEntity>
  ) {
    super(repository)
  }

  // getChildrenMap(nodeIds: string[]) {
  //   return this.getInnerRepository()
  //     .createQueryBuilder("foo")
  //     .where({
  //       parent: {
  //         id: In(nodeIds),
  //       },
  //     })
  //     .select("foo.parent.id", "parentId")
  //     .addSelect("COUNT(foo.id)", "count")
  //     .groupBy("foo.parent.id")
  //     .getRawMany()
  // }
}
