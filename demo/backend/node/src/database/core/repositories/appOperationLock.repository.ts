import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  WpEntityRepository,
  NestTypeOrmRepository,
  LockAcquireInput,
  LockAcquireResult,
  newUuid,
  LockReleaseInput,
  LockNotFoundError,
  LockItem,
} from "@punks/backend-entity-manager"
import {
  AppOperationLockEntity,
  AppOperationLockEntityId,
} from "../entities/appOperationLock.entity"

@WpEntityRepository("appOperationLock")
export class AppOperationLockRepository extends NestTypeOrmRepository<
  AppOperationLockEntity,
  AppOperationLockEntityId
> {
  constructor(
    @InjectRepository(AppOperationLockEntity, "core")
    repository: Repository<AppOperationLockEntity>
  ) {
    super(repository)
  }

  async acquireLock(input: LockAcquireInput): Promise<LockAcquireResult> {
    return await this.innerRepository.manager.transaction(
      async (manager): Promise<LockAcquireResult> => {
        const currentLock = await manager.findOne(AppOperationLockEntity, {
          where: [
            {
              uid: input.lockUid,
            },
          ],
        })

        if (currentLock) {
          return {
            available: false,
            lockItem: currentLock,
          }
        }

        const newLock = manager.create(AppOperationLockEntity, {
          id: newUuid(),
          uid: input.lockUid,
          lockedBy: input.requestedBy,
        })
        await manager.save([newLock])

        return {
          available: true,
          lockItem: newLock,
        }
      }
    )
  }

  async releaseLock(input: LockReleaseInput): Promise<void> {
    const result = await this.innerRepository.delete({
      uid: input.lockUid,
    })
    if (result.affected === 0) {
      throw new LockNotFoundError(`Lock ${input.lockUid} was not found`)
    }
  }

  async getLock(lockUid: string): Promise<LockItem> {
    return await this.innerRepository.findOne({
      where: {
        uid: lockUid,
      },
    })
  }
}
