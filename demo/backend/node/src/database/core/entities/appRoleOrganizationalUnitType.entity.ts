import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Unique,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationalUnitTypeEntity } from "./appOrganizationalUnitType.entity"
import { AppRoleEntity } from "./appRole.entity"

export type AppRoleOrganizationalUnitTypeEntityId = string

@Entity("appRoleOrganizationalUnitTypes")
@WpEntity("appRoleOrganizationalUnitType")
@Unique(["role", "type"])
export class AppRoleOrganizationalUnitTypeEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppRoleOrganizationalUnitTypeEntityId

  @ManyToOne(() => AppRoleEntity, {
    nullable: false,
  })
  role: AppRoleEntity

  @ManyToOne(() => AppOrganizationalUnitTypeEntity, {
    nullable: false,
  })
  type: AppOrganizationalUnitTypeEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
