import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Column,
  Unique,
  OneToMany,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { AppOrganizationalUnitEntity } from "./appOrganizationalUnit.entity"
import { AppOrganizationalUnitTypeChildEntity } from "./appOrganizationalUnitTypeChild.entity"
import { AppRoleOrganizationalUnitTypeEntity } from "./appRoleOrganizationalUnitType.entity"

export type AppOrganizationalUnitTypeEntityId = string

@Entity("appOrganizationalUnitTypes")
@Unique("organizationalUnitUid", ["organization", "uid"])
@Unique("organizationalUnitName", ["organization", "name"])
@WpEntity("appOrganizationalUnitType")
export class AppOrganizationalUnitTypeEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppOrganizationalUnitTypeEntityId

  @Column({ type: "varchar", length: 255 })
  uid: string

  @Column({ type: "varchar", length: 255 })
  name: string

  @Column({ type: "boolean" })
  allowAsRoot: boolean

  @OneToMany(
    () => AppOrganizationalUnitTypeChildEntity,
    (child) => child.parentType
  )
  allowedChildrenTypes?: AppOrganizationalUnitTypeChildEntity[]

  @OneToMany(() => AppOrganizationalUnitEntity, (unit) => unit.type)
  organizationalUnits?: AppOrganizationalUnitEntity[]

  @OneToMany(() => AppRoleOrganizationalUnitTypeEntity, (type) => type.role)
  roles?: AppRoleOrganizationalUnitTypeEntity[]

  @ManyToOne(() => AppOrganizationEntity, {
    eager: true,
    nullable: false,
  })
  organization: AppOrganizationEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
