import {
  Entity,
  Column,
  CreateDateColumn,
  ManyToOne,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
  Index,
} from "typeorm"
import { IAppDivision, WpEntity } from "@punks/backend-entity-manager"
import { AppCompanyEntity } from "./appCompany.entity"

export type AppDivisionEntityId = string

@Entity("appDivisions")
@WpEntity("appDivision")
@Unique("divisionUid", ["company", "uid"])
export class AppDivisionEntity implements IAppDivision {
  @PrimaryColumn({ type: "uuid" })
  id: AppDivisionEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  uid: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  name: string

  @ManyToOne(() => AppCompanyEntity, (company) => company.divisions, {
    nullable: false,
  })
  company: AppCompanyEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
