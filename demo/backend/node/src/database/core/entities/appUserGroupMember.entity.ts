import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppUserEntity } from "./appUser.entity"
import { AppUserGroupEntity } from "./appUserGroup.entity"

export type AppUserGroupMemberEntityId = string

@Unique("groupMember", ["group", "user"])
@Entity("appUserGroupMembers")
@WpEntity("appUserGroupMember")
export class AppUserGroupMemberEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppUserGroupMemberEntityId

  @ManyToOne(() => AppUserGroupEntity, (group) => group.userMembers, {
    nullable: false,
  })
  group: AppUserGroupEntity

  @ManyToOne(() => AppUserEntity, (user) => user.userGroups, {
    nullable: false,
  })
  user: AppUserEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
