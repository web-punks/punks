import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
  Index,
} from "typeorm"
import { IAppRole, WpEntity } from "@punks/backend-entity-manager"
import { AppUserRoleEntity } from "./appUserRole.entity"
import { AppRoleOrganizationalUnitTypeEntity } from "./appRoleOrganizationalUnitType.entity"
import { AppRoleAssignableRoleEntity } from "./appRoleAssignableRole.entity"
import { AppRolePermissionEntity } from "./appRolePermission.entity"

export type AppRoleEntityId = string

@Entity("appRoles")
@WpEntity("appRole")
export class AppRoleEntity implements IAppRole {
  @PrimaryColumn({ type: "uuid" })
  id: AppRoleEntityId

  @Index()
  @Column({ type: "varchar", length: 255, unique: true })
  uid: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  name: string

  @Column({ type: "boolean", default: true })
  enabled: boolean

  @Column({ type: "boolean", default: true })
  allowAsRoot: boolean

  @OneToMany(() => AppRoleOrganizationalUnitTypeEntity, (type) => type.role)
  allowedOrganizationalUnitTypes?: AppRoleOrganizationalUnitTypeEntity[]

  @OneToMany(() => AppRolePermissionEntity, (permission) => permission.role)
  permissions?: AppRolePermissionEntity[]

  @OneToMany(() => AppRoleAssignableRoleEntity, (role) => role.userRole)
  assignableRoles?: AppRoleAssignableRoleEntity[]

  @OneToMany(() => AppUserRoleEntity, (role) => role.user)
  userRoles?: AppUserRoleEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
