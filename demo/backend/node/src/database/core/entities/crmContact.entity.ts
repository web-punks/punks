import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Column,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { AppOrganizationalUnitEntity } from "./appOrganizationalUnit.entity"

@Entity("crmContacts")
@WpEntity("crmContact")
export class CrmContactEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255 })
  email: string

  @Column({ type: "varchar", length: 255 })
  firstName: string

  @Column({ type: "varchar", length: 255 })
  lastName: string

  @ManyToOne(
    () => AppOrganizationalUnitEntity,
    (organizationalUnit) => organizationalUnit.contacts,
    {
      nullable: true,
    }
  )
  organizationalUnit?: AppOrganizationalUnitEntity

  @ManyToOne(() => AppOrganizationEntity, {
    eager: true,
  })
  organization: AppOrganizationEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
