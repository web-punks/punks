import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Index,
  Column,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"

export type AppCacheEntryEntityId = string

@Entity("appCacheEntries")
@WpEntity("appCacheEntry")
export class AppCacheEntryEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppCacheEntryEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  instance: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  key: string

  @Column({
    type: "jsonb",
    nullable: true,
  })
  data: any

  @Column({ type: "timestamptz" })
  expirationTime: Date

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
