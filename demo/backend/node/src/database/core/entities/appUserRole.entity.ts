import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppRoleEntity } from "./appRole.entity"
import { AppUserEntity } from "./appUser.entity"

export type AppUserRoleEntityId = string

@Unique("userRole", ["user", "role"])
@Entity("appUserRoles")
@WpEntity("appUserRole")
export class AppUserRoleEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppUserRoleEntityId

  @ManyToOne(() => AppUserEntity, (user) => user.userRoles, {
    nullable: false,
  })
  user: AppUserEntity

  @ManyToOne(() => AppRoleEntity, (roles) => roles.userRoles, {
    nullable: false,
  })
  role: AppRoleEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
