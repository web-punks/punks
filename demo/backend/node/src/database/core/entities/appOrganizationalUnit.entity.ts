import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Column,
  Unique,
  Index,
  OneToMany,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { AppOrganizationalUnitTypeEntity } from "./appOrganizationalUnitType.entity"
import { AppUserEntity } from "./appUser.entity"
import { CrmContactEntity } from "./crmContact.entity"

export type AppOrganizationalUnitEntityId = string

@Entity("appOrganizationalUnits")
@WpEntity("appOrganizationalUnit")
@Unique("organizationalUnitPathName", ["name", "parent", "organization"])
@Unique("organizationalUnitPathUid", ["uid", "parent", "organization"])
export class AppOrganizationalUnitEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppOrganizationalUnitEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  name: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  uid: string

  @ManyToOne(() => AppOrganizationalUnitTypeEntity, {
    nullable: false,
  })
  type: AppOrganizationalUnitTypeEntity

  @Index()
  @ManyToOne(() => AppOrganizationalUnitEntity, {
    nullable: true,
  })
  parent?: AppOrganizationalUnitEntity

  @Index()
  @ManyToOne(() => AppOrganizationEntity, {
    nullable: false,
  })
  organization: AppOrganizationEntity

  @OneToMany(() => AppUserEntity, (user) => user.organizationalUnit)
  users?: AppUserEntity[]

  @OneToMany(() => CrmContactEntity, (contact) => contact.organizationalUnit)
  contacts?: CrmContactEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
