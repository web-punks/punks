import {
  CreateDateColumn,
  Column,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
  Index,
} from "typeorm"
import { IAppUserProfile, WpEntity } from "@punks/backend-entity-manager"

export type AppUserProfileEntityId = string

@Entity("appUserProfiles")
@WpEntity("appUserProfile")
export class AppUserProfileEntity implements IAppUserProfile {
  @PrimaryColumn({ type: "uuid" })
  id: AppUserProfileEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  firstName: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  lastName: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
