import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
} from "typeorm"
import { JobRunType, JobStatus, WpEntity } from "@punks/backend-entity-manager"

export type AppJobInstanceEntityId = string

@Entity("appJobInstances")
@WpEntity("appJobInstance")
export class AppJobInstanceEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppJobInstanceEntityId

  @Column({ type: "varchar", length: 256, nullable: true })
  scheduleUid?: string

  @Column({ type: "varchar", length: 256 })
  jobUid: string

  @Column({ type: "varchar", length: 256 })
  runType: JobRunType

  @Column({ type: "varchar", length: 256 })
  status: JobStatus

  @Column({ type: "timestamptz", nullable: true })
  insertTime?: Date

  @Column({ type: "timestamptz", nullable: true })
  startTime?: Date

  @Column({ type: "timestamptz", nullable: true })
  endTime?: Date

  @Column({
    type: "jsonb",
    nullable: true,
  })
  result?: any

  @Column({
    type: "jsonb",
    nullable: true,
  })
  error?: any

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
