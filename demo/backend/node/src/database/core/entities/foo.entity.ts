import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  Index,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"

@Entity("foos")
@WpEntity("foo", {
  versioning: {
    enabled: true,
  },
})
export class FooEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255 })
  name: string

  @Column({ type: "varchar", length: 255 })
  type: string

  @Column({ type: "varchar", length: 255, nullable: true })
  other?: string

  @Column({ type: "int" })
  age: number

  @Column({ type: "varchar", length: 255 })
  uid: string

  @Column({ type: "boolean", default: true })
  enabled: boolean

  @Index()
  @ManyToOne(() => FooEntity, {
    nullable: true,
    onDelete: "CASCADE",
  })
  parent?: FooEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
