import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
} from "typeorm"
import {
  JobConcurrency,
  JobSchedule,
  WpEntity,
} from "@punks/backend-entity-manager"

export type AppJobDefinitionEntityId = string

@Entity("appJobDefinitions")
@WpEntity("appJobDefinition")
export class AppJobDefinitionEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppJobDefinitionEntityId

  @Column({ type: "varchar", length: 64, unique: true })
  uid: string

  @Column({ type: "varchar", length: 256 })
  name: string

  @Column({ type: "varchar", length: 64 })
  provider: string

  @Column({
    type: "jsonb",
    nullable: false,
  })
  infrastructureParams: any

  @Column({
    type: "jsonb",
    nullable: false,
  })
  invocationParams: any

  @Column({
    type: "jsonb",
    nullable: false,
  })
  schedules: JobSchedule<unknown>[]

  @Column({ type: "varchar", length: 64 })
  concurrency: JobConcurrency

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
