import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"

export type AppEventLogEntityId = string

@Entity("appEventLogs")
@WpEntity("appEventLog")
export class AppEventLogEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppEventLogEntityId

  @Column({ type: "varchar", length: 255 })
  type: string

  @Column({ type: "varchar", length: 255, nullable: true })
  userId?: string

  @Column({ type: "varchar", length: 255, nullable: true })
  userName?: string

  @Column({
    type: "jsonb",
    nullable: true,
  })
  payload?: any

  @Column({ type: "timestamptz" })
  timestamp: Date

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
