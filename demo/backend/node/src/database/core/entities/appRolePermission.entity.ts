import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Unique,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppPermissionEntity } from "./appPermission.entity"
import { AppRoleEntity } from "./appRole.entity"

export type AppRolePermissionEntityId = string

@Entity("appRolePermissions")
@WpEntity("appRolePermission")
@Unique(["role", "permission"])
export class AppRolePermissionEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppRolePermissionEntityId

  @ManyToOne(() => AppRoleEntity, {
    nullable: false,
  })
  role: AppRoleEntity

  @ManyToOne(() => AppPermissionEntity, {
    nullable: false,
  })
  permission: AppPermissionEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
