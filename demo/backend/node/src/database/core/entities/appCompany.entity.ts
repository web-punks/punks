import {
  Entity,
  Column,
  CreateDateColumn,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
  Index,
} from "typeorm"
import { IAppCompany, WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { AppDivisionEntity } from "./appDivision.entity"

export type AppCompanyEntityId = string

@Entity("appCompanies")
@WpEntity("appCompany")
@Unique("companyUid", ["organization", "uid"])
export class AppCompanyEntity implements IAppCompany {
  @PrimaryColumn({ type: "uuid" })
  id: AppCompanyEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  uid: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  name: string

  @ManyToOne(
    () => AppOrganizationEntity,
    (organization) => organization.companies,
    {
      nullable: false,
    }
  )
  organization: AppOrganizationEntity

  @OneToMany(() => AppDivisionEntity, (division) => division.company)
  divisions?: AppDivisionEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
