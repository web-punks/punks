import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"

export type AppOperationLockEntityId = string

@Entity("appOperationLocks")
@WpEntity("appOperationLock")
export class AppOperationLockEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppOperationLockEntityId

  @Column({ type: "varchar", length: 255, unique: true })
  uid: string

  @Column({ type: "varchar", length: 255, nullable: true })
  lockedBy?: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
