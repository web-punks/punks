import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Index,
  Column,
  OneToMany,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppRolePermissionEntity } from "./appRolePermission.entity"

export type AppPermissionEntityId = string

@Entity("appPermissions")
@WpEntity("appPermission")
export class AppPermissionEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppPermissionEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  uid: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  name: string

  @OneToMany(() => AppRolePermissionEntity, (role) => role.permission)
  roles?: AppRolePermissionEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
