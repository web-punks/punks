import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  UpdateDateColumn,
  Index,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "./appOrganization.entity"

export type AppEmailLogEntityId = string

@Entity("appEmailLogs")
@WpEntity("appEmailLog")
export class AppEmailLogEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppEmailLogEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  emailType: string

  @Column("varchar", { array: true, nullable: true })
  to?: string[]

  @Column("varchar", { array: true, nullable: true })
  cc?: string[]

  @Column("varchar", { array: true, nullable: true })
  bcc?: string[]

  @Column({
    type: "jsonb",
    nullable: true,
  })
  payload?: any

  @ManyToOne(() => AppOrganizationEntity, {
    nullable: true,
  })
  organization: AppOrganizationEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
