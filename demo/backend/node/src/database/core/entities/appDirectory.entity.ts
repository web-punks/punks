import {
  Entity,
  Column,
  CreateDateColumn,
  PrimaryColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
  Unique,
  Index,
} from "typeorm"
import { IAppDirectory, WpEntity } from "@punks/backend-entity-manager"
import { AppTenantEntity } from "./appTenant.entity"
import { AppUserEntity } from "./appUser.entity"

export type AppDirectoryEntityId = string

@Entity("appDirectories")
@WpEntity("appDirectory")
@Unique("directoryUid", ["tenant", "uid"])
export class AppDirectoryEntity implements IAppDirectory {
  @PrimaryColumn({ type: "uuid" })
  id: AppDirectoryEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  uid: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  name: string

  @Column({ type: "boolean", default: false })
  default: boolean

  @ManyToOne(() => AppTenantEntity, (tenant) => tenant.directories, {
    nullable: false,
  })
  tenant: AppTenantEntity

  @OneToMany(() => AppUserEntity, (user) => user.directory)
  users?: AppUserEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
