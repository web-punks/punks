import {
  Entity,
  Column,
  CreateDateColumn,
  PrimaryColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
  Unique,
  Index,
} from "typeorm"
import { IAppOrganization, WpEntity } from "@punks/backend-entity-manager"
import { AppCompanyEntity } from "./appCompany.entity"
import { AppTenantEntity } from "./appTenant.entity"
import { AppUserEntity } from "./appUser.entity"
import { AppUserGroupEntity } from "./appUserGroup.entity"
import { AppLanguageEntity } from "./appLanguage.entity"

export type AppOrganizationEntityId = string

@Entity("appOrganizations")
@WpEntity("appOrganization")
@Unique("organizationUid", ["tenant", "uid"])
export class AppOrganizationEntity implements IAppOrganization {
  @PrimaryColumn({ type: "uuid" })
  id: AppOrganizationEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  uid: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  name: string

  @ManyToOne(() => AppTenantEntity, (tenant) => tenant.organizations, {
    nullable: false,
  })
  tenant: AppTenantEntity

  @OneToMany(() => AppCompanyEntity, (company) => company.organization)
  companies?: AppCompanyEntity[]

  @OneToMany(() => AppUserEntity, (user) => user.organization)
  users?: AppUserEntity[]

  @OneToMany(() => AppUserGroupEntity, (userGroup) => userGroup.organization)
  userGroups?: AppUserGroupEntity[]

  @OneToMany(() => AppLanguageEntity, (language) => language.organization)
  languages?: AppLanguageEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
