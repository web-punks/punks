import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Unique,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppRoleEntity } from "./appRole.entity"

export type AppRoleAssignableRoleEntityId = string

@Entity("appRoleAssignableRoles")
@WpEntity("appRoleAssignableRole")
@Unique(["userRole", "assignableRole"])
export class AppRoleAssignableRoleEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppRoleAssignableRoleEntityId

  @ManyToOne(() => AppRoleEntity, {
    nullable: false,
  })
  userRole: AppRoleEntity

  @ManyToOne(() => AppRoleEntity, {
    nullable: false,
  })
  assignableRole: AppRoleEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
