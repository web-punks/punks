import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  OneToMany,
  PrimaryColumn,
  ManyToOne,
  Unique,
  UpdateDateColumn,
} from "typeorm"
import { IAppUser, WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { AppUserProfileEntity } from "./appUserProfile.entity"
import { AppUserRoleEntity } from "./appUserRole.entity"
import { AppUserGroupMemberEntity } from "./appUserGroupMember.entity"
import { AppTenantEntity } from "./appTenant.entity"
import { AppDirectoryEntity } from "./appDirectory.entity"
import { AppOrganizationalUnitEntity } from "./appOrganizationalUnit.entity"

export type AppUserEntityId = string

@Unique("userUserName", ["tenant", "organization", "userName"])
@Unique("userEmail", ["tenant", "organization", "email"])
@Unique("userUid", ["tenant", "organization", "uid"])
@Entity("appUsers")
@WpEntity("appUser", {
  versioning: {
    enabled: true,
  },
})
export class AppUserEntity implements IAppUser {
  @PrimaryColumn({ type: "uuid" })
  id: AppUserEntityId

  @Column({ type: "varchar", length: 255, nullable: true })
  uid?: string

  @Column({ type: "varchar", nullable: true })
  passwordHash: string

  @Column({ type: "timestamptz", nullable: true })
  passwordUpdateTimestamp?: Date

  @Column({ type: "bool", default: false })
  temporaryPassword: boolean

  @Column({ type: "varchar", length: 255 })
  userName: string

  @Column({ type: "varchar", length: 255 })
  email: string

  @Column({ type: "bool", default: false })
  verified: boolean

  @Column({ type: "timestamptz", nullable: true })
  verifiedTimestamp?: Date

  @Column({ type: "bool", default: false })
  disabled: boolean

  @OneToOne(() => AppUserProfileEntity, {
    cascade: true,
    eager: true,
    onDelete: "CASCADE",
  })
  @JoinColumn()
  profile: AppUserProfileEntity

  @OneToMany(() => AppUserRoleEntity, (role) => role.user, { nullable: true })
  userRoles?: AppUserRoleEntity[]

  @OneToMany(() => AppUserGroupMemberEntity, (member) => member.group)
  userGroups?: AppUserGroupMemberEntity[]

  @ManyToOne(() => AppTenantEntity, (tenant) => tenant.users, {
    nullable: false,
  })
  tenant: AppTenantEntity

  @ManyToOne(() => AppDirectoryEntity, (directory) => directory.users, {
    nullable: true,
  })
  directory: AppDirectoryEntity

  @ManyToOne(
    () => AppOrganizationalUnitEntity,
    (organizationalUnit) => organizationalUnit.users,
    {
      nullable: true,
    }
  )
  organizationalUnit?: AppOrganizationalUnitEntity

  @ManyToOne(
    () => AppOrganizationEntity,
    (organization) => organization.users,
    { nullable: true }
  )
  organization?: AppOrganizationEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}

export const AppUserEntityRelations = {
  Organization: "organization",
  Profile: "profile",
  Roles: "userRoles",
  Tenant: "tenant",
  Directory: "directory",
  UserRoles: "userRoles",
  UserGroups: "userGroups",
}
