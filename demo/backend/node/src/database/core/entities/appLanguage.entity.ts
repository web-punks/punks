import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
  Index,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "./appOrganization.entity"

export type AppLanguageEntityId = string

@Unique("organizationLanguage", ["organization", "uid"])
@Entity("appLanguages")
@WpEntity("appLanguage")
export class AppLanguageEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppLanguageEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  uid: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  name: string

  @Column({ type: "boolean", default: false })
  default: boolean

  @ManyToOne(
    () => AppOrganizationEntity,
    (organization) => organization.languages,
    {
      nullable: false,
    }
  )
  organization: AppOrganizationEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
