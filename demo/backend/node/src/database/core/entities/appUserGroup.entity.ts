import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
  Index,
} from "typeorm"
import { IAppUserGroup, WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { AppUserGroupMemberEntity } from "./appUserGroupMember.entity"

export type AppUserGroupEntityId = string

@Unique("userGroupUid", ["organization", "uid"])
@Entity("appUserGroups")
@WpEntity("appUserGroup")
export class AppUserGroupEntity implements IAppUserGroup {
  @PrimaryColumn({ type: "uuid" })
  id: AppUserGroupEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  uid: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  name: string

  @Column({ type: "bool", default: false })
  disabled: boolean

  @ManyToOne(
    () => AppOrganizationEntity,
    (organization) => organization.userGroups,
    { nullable: true }
  )
  organization?: AppOrganizationEntity

  @OneToMany(() => AppUserGroupMemberEntity, (member) => member.group)
  userMembers: AppUserGroupMemberEntity[]

  @Column({ type: "bool", default: false })
  isBuiltIn: boolean

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
