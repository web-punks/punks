import { AppCompanyRepository } from "./repositories/appCompany.repository"
import { AppDivisionRepository } from "./repositories/appDivision.repository"
import { AppEmailLogRepository } from "./repositories/appEmailLog.repository"
import { AppEventLogRepository } from "./repositories/appEventLog.repository"
import { AppEntityVersionRepository } from "./repositories/appEntityVersion.repository"
import { AppFileReferenceRepository } from "./repositories/appFileReference.repository"
import { AppLanguageRepository } from "./repositories/appLanguage.repository"
import { AppOrganizationRepository } from "./repositories/appOrganization.repository"
import { AppRoleRepository } from "./repositories/appRole.repository"
import { AppTenantRepository } from "./repositories/appTenant.repository"
import { AppUserRepository } from "./repositories/appUser.repository"
import { AppUserGroupRepository } from "./repositories/appUserGroup.repository"
import { AppUserGroupMemberRepository } from "./repositories/appUserGroupMember.repository"
import { AppUserProfileRepository } from "./repositories/appUserProfile.repository"
import { AppUserRoleRepository } from "./repositories/appUserRole.repository"
import { AppDirectoryRepository } from "./repositories/appDirectory.repository"
import { AppOrganizationalUnitRepository } from "./repositories/appOrganizationalUnit.repository"
import { AppOrganizationalUnitTypeRepository } from "./repositories/appOrganizationalUnitType.repository"
import { AppOrganizationalUnitTypeChildRepository } from "./repositories/appOrganizationalUnitTypeChild.repository"
import { AppCacheEntryRepository } from "./repositories/appCacheEntry.repository"
import { AppRoleOrganizationalUnitTypeRepository } from "./repositories/appRoleOrganizationalUnitType.repository"
import { AppRoleAssignableRoleRepository } from "./repositories/appRoleAssignableRole.repository"
import { AppPermissionRepository } from "./repositories/appPermission.repository"
import { AppRolePermissionRepository } from "./repositories/appRolePermission.repository"
import { CrmContactRepository } from "./repositories/crmContact.repository"
import { FooRepository } from "./repositories/foo.repository"
import { AppOperationLockRepository } from "./repositories/appOperationLock.repository"
import { AppJobDefinitionRepository } from "./repositories/appJobDefinition.repository"
import { AppJobInstanceRepository } from "./repositories/appJobInstance.repository"

export const CoreDatabaseRepositories = [
  AppCacheEntryRepository,
  AppCompanyRepository,
  AppDirectoryRepository,
  AppDivisionRepository,
  AppEmailLogRepository,
  AppEventLogRepository,
  AppEntityVersionRepository,
  AppFileReferenceRepository,
  AppJobDefinitionRepository,
  AppJobInstanceRepository,
  AppLanguageRepository,
  AppOperationLockRepository,
  AppOrganizationRepository,
  AppOrganizationalUnitRepository,
  AppOrganizationalUnitTypeRepository,
  AppOrganizationalUnitTypeChildRepository,
  AppPermissionRepository,
  AppRoleRepository,
  AppRoleAssignableRoleRepository,
  AppRoleOrganizationalUnitTypeRepository,
  AppRolePermissionRepository,
  AppTenantRepository,
  AppUserRepository,
  AppUserGroupRepository,
  AppUserGroupMemberRepository,
  AppUserProfileRepository,
  AppUserRoleRepository,
  CrmContactRepository,
  FooRepository,
]
