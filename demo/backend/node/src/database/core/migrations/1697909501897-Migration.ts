import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1697909501897 implements MigrationInterface {
    name = 'Migration1697909501897'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "appEmailLogs" ("id" uuid NOT NULL, "emailType" character varying(255) NOT NULL, "to" character varying, "cc" character varying, "bcc" character varying, "payload" jsonb, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "organizationId" uuid, CONSTRAINT "PK_77d8243304fdc273d54c0f64113" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "appLanguages" ADD "default" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "appDivisions" DROP CONSTRAINT "FK_6a73b83e5cb236163555c4035aa"`);
        await queryRunner.query(`ALTER TABLE "appDivisions" DROP CONSTRAINT "divisionUid"`);
        await queryRunner.query(`ALTER TABLE "appDivisions" ALTER COLUMN "companyId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appCompanies" DROP CONSTRAINT "FK_7c33cda8bc11101d7a0f3817622"`);
        await queryRunner.query(`ALTER TABLE "appCompanies" DROP CONSTRAINT "companyUid"`);
        await queryRunner.query(`ALTER TABLE "appCompanies" ALTER COLUMN "organizationId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" DROP CONSTRAINT "FK_d12bca62469a33321c02159e6f4"`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" DROP CONSTRAINT "FK_83d37d1656af41f81cfbb45fd37"`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" DROP CONSTRAINT "userRole"`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ALTER COLUMN "userId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ALTER COLUMN "roleId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" DROP CONSTRAINT "FK_8b6378560c8868086e23570a2f4"`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" DROP CONSTRAINT "FK_b6e22f5eae6e308d65786a56360"`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" DROP CONSTRAINT "groupMember"`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ALTER COLUMN "groupId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ALTER COLUMN "userId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "FK_6e2edc74c70cabc319fd316e5d1"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "userUid"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "userEmail"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "userUserName"`);
        await queryRunner.query(`ALTER TABLE "appUsers" ALTER COLUMN "tenantId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appLanguages" DROP CONSTRAINT "FK_f0a08e0a4e4f35d02e4bbabb32d"`);
        await queryRunner.query(`ALTER TABLE "appLanguages" DROP CONSTRAINT "organizationLanguage"`);
        await queryRunner.query(`ALTER TABLE "appLanguages" ALTER COLUMN "organizationId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" DROP CONSTRAINT "FK_958358e0de706f9b48603efec54"`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" DROP CONSTRAINT "organizationUid"`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" ALTER COLUMN "tenantId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appDirectories" DROP CONSTRAINT "FK_7237bcc5a999276dfbb7ad46786"`);
        await queryRunner.query(`ALTER TABLE "appDirectories" DROP CONSTRAINT "directoryUid"`);
        await queryRunner.query(`ALTER TABLE "appDirectories" ALTER COLUMN "tenantId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appDivisions" ADD CONSTRAINT "divisionUid" UNIQUE ("companyId", "uid")`);
        await queryRunner.query(`ALTER TABLE "appCompanies" ADD CONSTRAINT "companyUid" UNIQUE ("organizationId", "uid")`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ADD CONSTRAINT "userRole" UNIQUE ("userId", "roleId")`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ADD CONSTRAINT "groupMember" UNIQUE ("groupId", "userId")`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "userUid" UNIQUE ("tenantId", "organizationId", "uid")`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "userEmail" UNIQUE ("tenantId", "organizationId", "email")`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "userUserName" UNIQUE ("tenantId", "organizationId", "userName")`);
        await queryRunner.query(`ALTER TABLE "appLanguages" ADD CONSTRAINT "organizationLanguage" UNIQUE ("organizationId", "uid")`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" ADD CONSTRAINT "organizationUid" UNIQUE ("tenantId", "uid")`);
        await queryRunner.query(`ALTER TABLE "appDirectories" ADD CONSTRAINT "directoryUid" UNIQUE ("tenantId", "uid")`);
        await queryRunner.query(`ALTER TABLE "appDivisions" ADD CONSTRAINT "FK_6a73b83e5cb236163555c4035aa" FOREIGN KEY ("companyId") REFERENCES "appCompanies"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appCompanies" ADD CONSTRAINT "FK_7c33cda8bc11101d7a0f3817622" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ADD CONSTRAINT "FK_d12bca62469a33321c02159e6f4" FOREIGN KEY ("userId") REFERENCES "appUsers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ADD CONSTRAINT "FK_83d37d1656af41f81cfbb45fd37" FOREIGN KEY ("roleId") REFERENCES "appRoles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ADD CONSTRAINT "FK_8b6378560c8868086e23570a2f4" FOREIGN KEY ("groupId") REFERENCES "appUserGroups"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ADD CONSTRAINT "FK_b6e22f5eae6e308d65786a56360" FOREIGN KEY ("userId") REFERENCES "appUsers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "FK_6e2edc74c70cabc319fd316e5d1" FOREIGN KEY ("tenantId") REFERENCES "appTenants"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appLanguages" ADD CONSTRAINT "FK_f0a08e0a4e4f35d02e4bbabb32d" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" ADD CONSTRAINT "FK_958358e0de706f9b48603efec54" FOREIGN KEY ("tenantId") REFERENCES "appTenants"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appDirectories" ADD CONSTRAINT "FK_7237bcc5a999276dfbb7ad46786" FOREIGN KEY ("tenantId") REFERENCES "appTenants"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appEmailLogs" ADD CONSTRAINT "FK_593e24978282b2de56f029099fc" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appEmailLogs" DROP CONSTRAINT "FK_593e24978282b2de56f029099fc"`);
        await queryRunner.query(`ALTER TABLE "appDirectories" DROP CONSTRAINT "FK_7237bcc5a999276dfbb7ad46786"`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" DROP CONSTRAINT "FK_958358e0de706f9b48603efec54"`);
        await queryRunner.query(`ALTER TABLE "appLanguages" DROP CONSTRAINT "FK_f0a08e0a4e4f35d02e4bbabb32d"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "FK_6e2edc74c70cabc319fd316e5d1"`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" DROP CONSTRAINT "FK_b6e22f5eae6e308d65786a56360"`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" DROP CONSTRAINT "FK_8b6378560c8868086e23570a2f4"`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" DROP CONSTRAINT "FK_83d37d1656af41f81cfbb45fd37"`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" DROP CONSTRAINT "FK_d12bca62469a33321c02159e6f4"`);
        await queryRunner.query(`ALTER TABLE "appCompanies" DROP CONSTRAINT "FK_7c33cda8bc11101d7a0f3817622"`);
        await queryRunner.query(`ALTER TABLE "appDivisions" DROP CONSTRAINT "FK_6a73b83e5cb236163555c4035aa"`);
        await queryRunner.query(`ALTER TABLE "appDirectories" DROP CONSTRAINT "directoryUid"`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" DROP CONSTRAINT "organizationUid"`);
        await queryRunner.query(`ALTER TABLE "appLanguages" DROP CONSTRAINT "organizationLanguage"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "userUserName"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "userEmail"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "userUid"`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" DROP CONSTRAINT "groupMember"`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" DROP CONSTRAINT "userRole"`);
        await queryRunner.query(`ALTER TABLE "appCompanies" DROP CONSTRAINT "companyUid"`);
        await queryRunner.query(`ALTER TABLE "appDivisions" DROP CONSTRAINT "divisionUid"`);
        await queryRunner.query(`ALTER TABLE "appDirectories" ALTER COLUMN "tenantId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appDirectories" ADD CONSTRAINT "directoryUid" UNIQUE ("uid", "tenantId")`);
        await queryRunner.query(`ALTER TABLE "appDirectories" ADD CONSTRAINT "FK_7237bcc5a999276dfbb7ad46786" FOREIGN KEY ("tenantId") REFERENCES "appTenants"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" ALTER COLUMN "tenantId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" ADD CONSTRAINT "organizationUid" UNIQUE ("uid", "tenantId")`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" ADD CONSTRAINT "FK_958358e0de706f9b48603efec54" FOREIGN KEY ("tenantId") REFERENCES "appTenants"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appLanguages" ALTER COLUMN "organizationId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appLanguages" ADD CONSTRAINT "organizationLanguage" UNIQUE ("uid", "organizationId")`);
        await queryRunner.query(`ALTER TABLE "appLanguages" ADD CONSTRAINT "FK_f0a08e0a4e4f35d02e4bbabb32d" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUsers" ALTER COLUMN "tenantId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "userUserName" UNIQUE ("userName", "tenantId", "organizationId")`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "userEmail" UNIQUE ("email", "tenantId", "organizationId")`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "userUid" UNIQUE ("uid", "tenantId", "organizationId")`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "FK_6e2edc74c70cabc319fd316e5d1" FOREIGN KEY ("tenantId") REFERENCES "appTenants"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ALTER COLUMN "userId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ALTER COLUMN "groupId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ADD CONSTRAINT "groupMember" UNIQUE ("groupId", "userId")`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ADD CONSTRAINT "FK_b6e22f5eae6e308d65786a56360" FOREIGN KEY ("userId") REFERENCES "appUsers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ADD CONSTRAINT "FK_8b6378560c8868086e23570a2f4" FOREIGN KEY ("groupId") REFERENCES "appUserGroups"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ALTER COLUMN "roleId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ALTER COLUMN "userId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ADD CONSTRAINT "userRole" UNIQUE ("userId", "roleId")`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ADD CONSTRAINT "FK_83d37d1656af41f81cfbb45fd37" FOREIGN KEY ("roleId") REFERENCES "appRoles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ADD CONSTRAINT "FK_d12bca62469a33321c02159e6f4" FOREIGN KEY ("userId") REFERENCES "appUsers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appCompanies" ALTER COLUMN "organizationId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appCompanies" ADD CONSTRAINT "companyUid" UNIQUE ("uid", "organizationId")`);
        await queryRunner.query(`ALTER TABLE "appCompanies" ADD CONSTRAINT "FK_7c33cda8bc11101d7a0f3817622" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appDivisions" ALTER COLUMN "companyId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appDivisions" ADD CONSTRAINT "divisionUid" UNIQUE ("uid", "companyId")`);
        await queryRunner.query(`ALTER TABLE "appDivisions" ADD CONSTRAINT "FK_6a73b83e5cb236163555c4035aa" FOREIGN KEY ("companyId") REFERENCES "appCompanies"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appLanguages" DROP COLUMN "default"`);
        await queryRunner.query(`DROP TABLE "appEmailLogs"`);
    }

}
