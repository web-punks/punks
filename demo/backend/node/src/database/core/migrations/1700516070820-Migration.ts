import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1700516070820 implements MigrationInterface {
    name = 'Migration1700516070820'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "foos" DROP CONSTRAINT "FK_7f3d3bba32f78260e0d5e87e8a5"`);
        await queryRunner.query(`ALTER TABLE "foos" ADD CONSTRAINT "FK_7f3d3bba32f78260e0d5e87e8a5" FOREIGN KEY ("parentId") REFERENCES "foos"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "foos" DROP CONSTRAINT "FK_7f3d3bba32f78260e0d5e87e8a5"`);
        await queryRunner.query(`ALTER TABLE "foos" ADD CONSTRAINT "FK_7f3d3bba32f78260e0d5e87e8a5" FOREIGN KEY ("parentId") REFERENCES "foos"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
