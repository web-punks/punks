import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1691178397007 implements MigrationInterface {
    name = 'Migration1691178397007'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appUserGroups" ADD "isBuiltIn" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appUserGroups" DROP COLUMN "isBuiltIn"`);
    }

}
