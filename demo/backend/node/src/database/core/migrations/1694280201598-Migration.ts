import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1694280201598 implements MigrationInterface {
    name = 'Migration1694280201598'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "foos" ADD "type" character varying(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "foos" ADD "age" integer NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "foos" DROP COLUMN "age"`);
        await queryRunner.query(`ALTER TABLE "foos" DROP COLUMN "type"`);
    }

}
