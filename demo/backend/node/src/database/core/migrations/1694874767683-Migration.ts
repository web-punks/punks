import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1694874767683 implements MigrationInterface {
    name = 'Migration1694874767683'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "appDirectories" ("id" uuid NOT NULL, "uid" character varying(255) NOT NULL, "name" character varying(255) NOT NULL, "default" boolean NOT NULL DEFAULT false, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "tenantId" uuid, CONSTRAINT "directoryUid" UNIQUE ("tenantId", "uid"), CONSTRAINT "PK_6badfb7353152fb4b98984c3d47" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD "directoryId" uuid`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "FK_c90fdd3058c087fbef47f23486f" FOREIGN KEY ("directoryId") REFERENCES "appDirectories"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appDirectories" ADD CONSTRAINT "FK_7237bcc5a999276dfbb7ad46786" FOREIGN KEY ("tenantId") REFERENCES "appTenants"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appDirectories" DROP CONSTRAINT "FK_7237bcc5a999276dfbb7ad46786"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "FK_c90fdd3058c087fbef47f23486f"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP COLUMN "directoryId"`);
        await queryRunner.query(`DROP TABLE "appDirectories"`);
    }

}
