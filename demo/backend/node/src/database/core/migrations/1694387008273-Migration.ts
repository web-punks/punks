import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1694387008273 implements MigrationInterface {
    name = 'Migration1694387008273'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."appEntityVersions_operationtype_enum" AS ENUM('create', 'update', 'upsert', 'delete')`);
        await queryRunner.query(`CREATE TABLE "appEntityVersions" ("id" uuid NOT NULL, "entityId" uuid NOT NULL, "entityType" character varying(255) NOT NULL, "data" jsonb, "operationType" "public"."appEntityVersions_operationtype_enum" NOT NULL, "operationUserId" uuid, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_b38b2d700b132ab07e7904cc476" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "entityId_entityType" ON "appEntityVersions" ("entityId", "entityType") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."entityId_entityType"`);
        await queryRunner.query(`DROP TABLE "appEntityVersions"`);
        await queryRunner.query(`DROP TYPE "public"."appEntityVersions_operationtype_enum"`);
    }

}
