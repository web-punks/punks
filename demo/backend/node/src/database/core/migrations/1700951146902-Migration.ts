import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1700951146902 implements MigrationInterface {
    name = 'Migration1700951146902'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "appRoleOrganizationalUnitTypes" ("id" uuid NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "roleId" uuid NOT NULL, "typeId" uuid NOT NULL, CONSTRAINT "UQ_d8efe9fe5abaa01e67c7d04ce79" UNIQUE ("roleId", "typeId"), CONSTRAINT "PK_37a86967601420142128e7a3367" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appRoleAssignableRoles" ("id" uuid NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "userRoleId" uuid NOT NULL, "assignableRoleId" uuid NOT NULL, CONSTRAINT "UQ_dfaa80726f348eca6384a0697bd" UNIQUE ("userRoleId", "assignableRoleId"), CONSTRAINT "PK_c0cf35ef8fd3a649c9c86db121e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appPermissions" ("id" uuid NOT NULL, "uid" character varying(255) NOT NULL, "name" character varying(255) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_7e2a2912eb2687853f61cbfaabb" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_3a20a3af5bc336150d1093602d" ON "appPermissions" ("uid") `);
        await queryRunner.query(`CREATE INDEX "IDX_b65befd7214044bec9804d34a1" ON "appPermissions" ("name") `);
        await queryRunner.query(`CREATE TABLE "appRolePermissions" ("id" uuid NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "roleId" uuid NOT NULL, "permissionId" uuid NOT NULL, CONSTRAINT "UQ_39342a5a2030c5d2feac0f0abaf" UNIQUE ("roleId", "permissionId"), CONSTRAINT "PK_81823a888a95f5df737fb47e09c" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appCacheEntries" ("id" uuid NOT NULL, "instance" character varying(255) NOT NULL, "key" character varying(255) NOT NULL, "data" jsonb, "expirationTime" TIMESTAMP WITH TIME ZONE NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_3f98d8eea3d3b88d64b87dc98b9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_d19b98470e6dcd7647ef5b2182" ON "appCacheEntries" ("instance") `);
        await queryRunner.query(`CREATE INDEX "IDX_1ccb69d1b60e8b2aa67ccc4fbe" ON "appCacheEntries" ("key") `);
        await queryRunner.query(`ALTER TABLE "crmContacts" ADD "organizationalUnitId" uuid`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnitTypeChildren" ADD "childTypeId" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appRoles" ADD "enabled" boolean NOT NULL DEFAULT true`);
        await queryRunner.query(`ALTER TABLE "appRoles" ADD "allowAsRoot" boolean NOT NULL DEFAULT true`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD "organizationalUnitId" uuid`);
        await queryRunner.query(`CREATE INDEX "IDX_60d8d0f52a7961577aa17f9e1d" ON "appEntityVersions" ("entityId") `);
        await queryRunner.query(`CREATE INDEX "IDX_4979bd3b6273600f72398bb761" ON "appEntityVersions" ("entityType") `);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnitTypeChildren" ADD CONSTRAINT "organizationalUnitTypeChild" UNIQUE ("parentTypeId", "childTypeId")`);
        await queryRunner.query(`ALTER TABLE "crmContacts" ADD CONSTRAINT "FK_61887e337640ae4a1d72bc689a2" FOREIGN KEY ("organizationalUnitId") REFERENCES "appOrganizationalUnits"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnitTypeChildren" ADD CONSTRAINT "FK_e1ddecc1bad42f4237e3463d2f4" FOREIGN KEY ("childTypeId") REFERENCES "appOrganizationalUnitTypes"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appRoleOrganizationalUnitTypes" ADD CONSTRAINT "FK_9f1806b5d9b1c14d8a3cfeb614c" FOREIGN KEY ("roleId") REFERENCES "appRoles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appRoleOrganizationalUnitTypes" ADD CONSTRAINT "FK_f032c446f931fab4cce5f0cc3e0" FOREIGN KEY ("typeId") REFERENCES "appOrganizationalUnitTypes"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appRoleAssignableRoles" ADD CONSTRAINT "FK_a55e14674386a289f912f5b2f64" FOREIGN KEY ("userRoleId") REFERENCES "appRoles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appRoleAssignableRoles" ADD CONSTRAINT "FK_28e72d144a662dd6eb30ed152bf" FOREIGN KEY ("assignableRoleId") REFERENCES "appRoles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appRolePermissions" ADD CONSTRAINT "FK_fce8a1866ab74e55541640c132e" FOREIGN KEY ("roleId") REFERENCES "appRoles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appRolePermissions" ADD CONSTRAINT "FK_448a2b137ae1fbd987e9fe6dc6c" FOREIGN KEY ("permissionId") REFERENCES "appPermissions"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "FK_761b137ad91ee98d69b86b64606" FOREIGN KEY ("organizationalUnitId") REFERENCES "appOrganizationalUnits"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "FK_761b137ad91ee98d69b86b64606"`);
        await queryRunner.query(`ALTER TABLE "appRolePermissions" DROP CONSTRAINT "FK_448a2b137ae1fbd987e9fe6dc6c"`);
        await queryRunner.query(`ALTER TABLE "appRolePermissions" DROP CONSTRAINT "FK_fce8a1866ab74e55541640c132e"`);
        await queryRunner.query(`ALTER TABLE "appRoleAssignableRoles" DROP CONSTRAINT "FK_28e72d144a662dd6eb30ed152bf"`);
        await queryRunner.query(`ALTER TABLE "appRoleAssignableRoles" DROP CONSTRAINT "FK_a55e14674386a289f912f5b2f64"`);
        await queryRunner.query(`ALTER TABLE "appRoleOrganizationalUnitTypes" DROP CONSTRAINT "FK_f032c446f931fab4cce5f0cc3e0"`);
        await queryRunner.query(`ALTER TABLE "appRoleOrganizationalUnitTypes" DROP CONSTRAINT "FK_9f1806b5d9b1c14d8a3cfeb614c"`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnitTypeChildren" DROP CONSTRAINT "FK_e1ddecc1bad42f4237e3463d2f4"`);
        await queryRunner.query(`ALTER TABLE "crmContacts" DROP CONSTRAINT "FK_61887e337640ae4a1d72bc689a2"`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnitTypeChildren" DROP CONSTRAINT "organizationalUnitTypeChild"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_4979bd3b6273600f72398bb761"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_60d8d0f52a7961577aa17f9e1d"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP COLUMN "organizationalUnitId"`);
        await queryRunner.query(`ALTER TABLE "appRoles" DROP COLUMN "allowAsRoot"`);
        await queryRunner.query(`ALTER TABLE "appRoles" DROP COLUMN "enabled"`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnitTypeChildren" DROP COLUMN "childTypeId"`);
        await queryRunner.query(`ALTER TABLE "crmContacts" DROP COLUMN "organizationalUnitId"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_1ccb69d1b60e8b2aa67ccc4fbe"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_d19b98470e6dcd7647ef5b2182"`);
        await queryRunner.query(`DROP TABLE "appCacheEntries"`);
        await queryRunner.query(`DROP TABLE "appRolePermissions"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_b65befd7214044bec9804d34a1"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_3a20a3af5bc336150d1093602d"`);
        await queryRunner.query(`DROP TABLE "appPermissions"`);
        await queryRunner.query(`DROP TABLE "appRoleAssignableRoles"`);
        await queryRunner.query(`DROP TABLE "appRoleOrganizationalUnitTypes"`);
    }

}
