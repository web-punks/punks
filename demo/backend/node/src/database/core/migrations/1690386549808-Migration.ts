import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1690386549808 implements MigrationInterface {
    name = 'Migration1690386549808'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "appUserProfiles" ("id" uuid NOT NULL, "firstName" character varying(255) NOT NULL, "lastName" character varying(255) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_43b79c95cf9e08d998f0726f2ad" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appRoles" ("id" uuid NOT NULL, "uid" character varying(255) NOT NULL, "name" character varying(255) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "UQ_0c26cb377b2806be9d9a6b595fc" UNIQUE ("uid"), CONSTRAINT "PK_03ba3d22b32795d7a3fba0387ee" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appUserRoles" ("id" uuid NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "userId" uuid, "roleId" uuid, CONSTRAINT "userRole" UNIQUE ("userId", "roleId"), CONSTRAINT "PK_75457876ffbf2af2fb5ed98b6a0" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appUserGroups" ("id" uuid NOT NULL, "uid" character varying(255) NOT NULL, "name" character varying(255) NOT NULL, "disabled" boolean NOT NULL DEFAULT false, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "organizationId" uuid, CONSTRAINT "userGroupUid" UNIQUE ("organizationId", "uid"), CONSTRAINT "PK_0782702ba58ab7130a81a70d6c6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appUserGroupMembers" ("id" uuid NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "groupId" uuid, "userId" uuid, CONSTRAINT "groupMember" UNIQUE ("groupId", "userId"), CONSTRAINT "PK_2a82283970dd5e96e6d686ea9b6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appUsers" ("id" uuid NOT NULL, "uid" character varying(255), "passwordHash" character varying, "passwordUpdateTimestamp" TIMESTAMP WITH TIME ZONE, "temporaryPassword" boolean NOT NULL DEFAULT false, "userName" character varying(255) NOT NULL, "email" character varying(255) NOT NULL, "verified" boolean NOT NULL DEFAULT false, "verifiedTimestamp" TIMESTAMP WITH TIME ZONE, "disabled" boolean NOT NULL DEFAULT false, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "profileId" uuid, "tenantId" uuid, "organizationId" uuid, CONSTRAINT "userUid" UNIQUE ("tenantId", "organizationId", "uid"), CONSTRAINT "userEmail" UNIQUE ("tenantId", "organizationId", "email"), CONSTRAINT "userUserName" UNIQUE ("tenantId", "organizationId", "userName"), CONSTRAINT "REL_0d874b1195a66da876e44152af" UNIQUE ("profileId"), CONSTRAINT "PK_4071d67d9afb17190f2d7a13b75" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appTenants" ("id" uuid NOT NULL, "uid" character varying(255) NOT NULL, "name" character varying(255) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "UQ_0b13cceb880257f3bf19888bee6" UNIQUE ("uid"), CONSTRAINT "PK_6412b5d82718ad60119dff795f8" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appOrganizations" ("id" uuid NOT NULL, "uid" character varying(255) NOT NULL, "name" character varying(255) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "tenantId" uuid, CONSTRAINT "organizationUid" UNIQUE ("tenantId", "uid"), CONSTRAINT "PK_4d03442d604164dfc3af1343b61" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appDivisions" ("id" uuid NOT NULL, "uid" character varying(255) NOT NULL, "name" character varying(255) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "companyId" uuid, CONSTRAINT "divisionUid" UNIQUE ("companyId", "uid"), CONSTRAINT "PK_6e1a62284b6a67acc4ef57e1124" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appCompanies" ("id" uuid NOT NULL, "uid" character varying(255) NOT NULL, "name" character varying(255) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "organizationId" uuid, CONSTRAINT "companyUid" UNIQUE ("organizationId", "uid"), CONSTRAINT "PK_b6d5345a64a41965921d2a3b96c" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appEventLogs" ("id" uuid NOT NULL, "type" character varying(255) NOT NULL, "userId" character varying(255), "userName" character varying(255), "payload" jsonb, "timestamp" TIMESTAMP WITH TIME ZONE NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_1cab17f3ef06e35b935e7010566" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ADD CONSTRAINT "FK_d12bca62469a33321c02159e6f4" FOREIGN KEY ("userId") REFERENCES "appUsers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" ADD CONSTRAINT "FK_83d37d1656af41f81cfbb45fd37" FOREIGN KEY ("roleId") REFERENCES "appRoles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserGroups" ADD CONSTRAINT "FK_6bfb9c1e819715e279ff4ee60f3" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ADD CONSTRAINT "FK_8b6378560c8868086e23570a2f4" FOREIGN KEY ("groupId") REFERENCES "appUserGroups"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" ADD CONSTRAINT "FK_b6e22f5eae6e308d65786a56360" FOREIGN KEY ("userId") REFERENCES "appUsers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "FK_0d874b1195a66da876e44152af5" FOREIGN KEY ("profileId") REFERENCES "appUserProfiles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "FK_6e2edc74c70cabc319fd316e5d1" FOREIGN KEY ("tenantId") REFERENCES "appTenants"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appUsers" ADD CONSTRAINT "FK_ac6e87e4813728317adb6ce3590" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" ADD CONSTRAINT "FK_958358e0de706f9b48603efec54" FOREIGN KEY ("tenantId") REFERENCES "appTenants"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appDivisions" ADD CONSTRAINT "FK_6a73b83e5cb236163555c4035aa" FOREIGN KEY ("companyId") REFERENCES "appCompanies"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appCompanies" ADD CONSTRAINT "FK_7c33cda8bc11101d7a0f3817622" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appCompanies" DROP CONSTRAINT "FK_7c33cda8bc11101d7a0f3817622"`);
        await queryRunner.query(`ALTER TABLE "appDivisions" DROP CONSTRAINT "FK_6a73b83e5cb236163555c4035aa"`);
        await queryRunner.query(`ALTER TABLE "appOrganizations" DROP CONSTRAINT "FK_958358e0de706f9b48603efec54"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "FK_ac6e87e4813728317adb6ce3590"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "FK_6e2edc74c70cabc319fd316e5d1"`);
        await queryRunner.query(`ALTER TABLE "appUsers" DROP CONSTRAINT "FK_0d874b1195a66da876e44152af5"`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" DROP CONSTRAINT "FK_b6e22f5eae6e308d65786a56360"`);
        await queryRunner.query(`ALTER TABLE "appUserGroupMembers" DROP CONSTRAINT "FK_8b6378560c8868086e23570a2f4"`);
        await queryRunner.query(`ALTER TABLE "appUserGroups" DROP CONSTRAINT "FK_6bfb9c1e819715e279ff4ee60f3"`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" DROP CONSTRAINT "FK_83d37d1656af41f81cfbb45fd37"`);
        await queryRunner.query(`ALTER TABLE "appUserRoles" DROP CONSTRAINT "FK_d12bca62469a33321c02159e6f4"`);
        await queryRunner.query(`DROP TABLE "appEventLogs"`);
        await queryRunner.query(`DROP TABLE "appCompanies"`);
        await queryRunner.query(`DROP TABLE "appDivisions"`);
        await queryRunner.query(`DROP TABLE "appOrganizations"`);
        await queryRunner.query(`DROP TABLE "appTenants"`);
        await queryRunner.query(`DROP TABLE "appUsers"`);
        await queryRunner.query(`DROP TABLE "appUserGroupMembers"`);
        await queryRunner.query(`DROP TABLE "appUserGroups"`);
        await queryRunner.query(`DROP TABLE "appUserRoles"`);
        await queryRunner.query(`DROP TABLE "appRoles"`);
        await queryRunner.query(`DROP TABLE "appUserProfiles"`);
    }

}
