import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1694362577935 implements MigrationInterface {
    name = 'Migration1694362577935'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "foos" ADD "enabled" boolean NOT NULL DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "foos" DROP COLUMN "enabled"`);
    }

}
