import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1697912702192 implements MigrationInterface {
    name = 'Migration1697912702192'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appEmailLogs" DROP COLUMN "to"`);
        await queryRunner.query(`ALTER TABLE "appEmailLogs" ADD "to" character varying array`);
        await queryRunner.query(`ALTER TABLE "appEmailLogs" DROP COLUMN "cc"`);
        await queryRunner.query(`ALTER TABLE "appEmailLogs" ADD "cc" character varying array`);
        await queryRunner.query(`ALTER TABLE "appEmailLogs" DROP COLUMN "bcc"`);
        await queryRunner.query(`ALTER TABLE "appEmailLogs" ADD "bcc" character varying array`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appEmailLogs" DROP COLUMN "bcc"`);
        await queryRunner.query(`ALTER TABLE "appEmailLogs" ADD "bcc" character varying`);
        await queryRunner.query(`ALTER TABLE "appEmailLogs" DROP COLUMN "cc"`);
        await queryRunner.query(`ALTER TABLE "appEmailLogs" ADD "cc" character varying`);
        await queryRunner.query(`ALTER TABLE "appEmailLogs" DROP COLUMN "to"`);
        await queryRunner.query(`ALTER TABLE "appEmailLogs" ADD "to" character varying`);
    }

}
