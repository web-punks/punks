import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1697185694835 implements MigrationInterface {
    name = 'Migration1697185694835'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "appLanguages" ("id" uuid NOT NULL, "uid" character varying(255) NOT NULL, "name" character varying(255) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "organizationId" uuid, CONSTRAINT "organizationLanguage" UNIQUE ("organizationId", "uid"), CONSTRAINT "PK_63732ce028f900e84ecf34b6385" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appFileReferences" ("id" uuid NOT NULL, "reference" character varying NOT NULL, "provider" character varying(255) NOT NULL, "folder" character varying NOT NULL, "fileName" character varying NOT NULL, "contentType" character varying(255) NOT NULL, "fileSize" bigint NOT NULL, "metadata" jsonb, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_4f404aab09a88d3b181e54e7d48" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "appLanguages" ADD CONSTRAINT "FK_f0a08e0a4e4f35d02e4bbabb32d" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appLanguages" DROP CONSTRAINT "FK_f0a08e0a4e4f35d02e4bbabb32d"`);
        await queryRunner.query(`DROP TABLE "appFileReferences"`);
        await queryRunner.query(`DROP TABLE "appLanguages"`);
    }

}
