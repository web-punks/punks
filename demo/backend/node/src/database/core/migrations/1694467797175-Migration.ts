import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1694467797175 implements MigrationInterface {
    name = 'Migration1694467797175'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."entityId_entityType"`);
        await queryRunner.query(`ALTER TABLE "appEntityVersions" DROP COLUMN "entityId"`);
        await queryRunner.query(`ALTER TABLE "appEntityVersions" ADD "entityId" character varying(255) NOT NULL`);
        await queryRunner.query(`CREATE INDEX "entityId_entityType" ON "appEntityVersions" ("entityId", "entityType") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."entityId_entityType"`);
        await queryRunner.query(`ALTER TABLE "appEntityVersions" DROP COLUMN "entityId"`);
        await queryRunner.query(`ALTER TABLE "appEntityVersions" ADD "entityId" uuid NOT NULL`);
        await queryRunner.query(`CREATE INDEX "entityId_entityType" ON "appEntityVersions" ("entityId", "entityType") `);
    }

}
