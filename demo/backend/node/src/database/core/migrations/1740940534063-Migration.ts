import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1740940534063 implements MigrationInterface {
    name = 'Migration1740940534063'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "foos" ADD "other" character varying(255)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "foos" DROP COLUMN "other"`);
    }

}
