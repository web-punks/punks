import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1700085581146 implements MigrationInterface {
    name = 'Migration1700085581146'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "appOrganizationalUnitTypeChildren" ("id" uuid NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "parentTypeId" uuid NOT NULL, CONSTRAINT "PK_46b845a182bc922f90a2a970dae" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appOrganizationalUnitTypes" ("id" uuid NOT NULL, "uid" character varying(255) NOT NULL, "name" character varying(255) NOT NULL, "allowAsRoot" boolean NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "organizationId" uuid NOT NULL, CONSTRAINT "organizationalUnitName" UNIQUE ("organizationId", "name"), CONSTRAINT "organizationalUnitUid" UNIQUE ("organizationId", "uid"), CONSTRAINT "PK_1baf098e17fde533371c07ad6b6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appOrganizationalUnits" ("id" uuid NOT NULL, "name" character varying(255) NOT NULL, "uid" character varying(255) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "typeId" uuid NOT NULL, "parentId" uuid, "organizationId" uuid NOT NULL, CONSTRAINT "organizationalUnitPathUid" UNIQUE ("uid", "parentId"), CONSTRAINT "organizationalUnitPathName" UNIQUE ("name", "parentId"), CONSTRAINT "PK_e419d3df1e6b465d7ef3c2a147e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_34440045a38cdb03752a03883a" ON "appOrganizationalUnits" ("name") `);
        await queryRunner.query(`CREATE INDEX "IDX_0cc82557bdcb02b2fe393d9568" ON "appOrganizationalUnits" ("uid") `);
        await queryRunner.query(`CREATE INDEX "IDX_366825c6f9173d2bc9bdee1e81" ON "appOrganizationalUnits" ("parentId") `);
        await queryRunner.query(`CREATE INDEX "IDX_990da363f706979636fb643cf3" ON "appOrganizationalUnits" ("organizationId") `);
        await queryRunner.query(`CREATE INDEX "IDX_e4c3da8997e4c51444ea05372b" ON "appDivisions" ("uid") `);
        await queryRunner.query(`CREATE INDEX "IDX_b5e2713ed379040d3040a3b8c1" ON "appDivisions" ("name") `);
        await queryRunner.query(`CREATE INDEX "IDX_ae80324d1986927eccbfaced47" ON "appCompanies" ("uid") `);
        await queryRunner.query(`CREATE INDEX "IDX_3787fcaf49d009c4f1cef7c696" ON "appCompanies" ("name") `);
        await queryRunner.query(`CREATE INDEX "IDX_9f9dba10ea0bca995dcfa30775" ON "appUserProfiles" ("firstName") `);
        await queryRunner.query(`CREATE INDEX "IDX_666abd9daf84bef60ed4dd1963" ON "appUserProfiles" ("lastName") `);
        await queryRunner.query(`CREATE INDEX "IDX_0c26cb377b2806be9d9a6b595f" ON "appRoles" ("uid") `);
        await queryRunner.query(`CREATE INDEX "IDX_fb1522df88449639c4229af5d6" ON "appRoles" ("name") `);
        await queryRunner.query(`CREATE INDEX "IDX_01574f07307743ee30cc0216fa" ON "appUserGroups" ("uid") `);
        await queryRunner.query(`CREATE INDEX "IDX_e47962f69d16a8d9f640c879ba" ON "appUserGroups" ("name") `);
        await queryRunner.query(`CREATE INDEX "IDX_cb3b95e82208430e0683e37c28" ON "appLanguages" ("uid") `);
        await queryRunner.query(`CREATE INDEX "IDX_a19bdcb338271c7870facf3538" ON "appLanguages" ("name") `);
        await queryRunner.query(`CREATE INDEX "IDX_6c3c432704fb68162ded1726c7" ON "appOrganizations" ("uid") `);
        await queryRunner.query(`CREATE INDEX "IDX_a889a820118428cb4f33a9b87d" ON "appOrganizations" ("name") `);
        await queryRunner.query(`CREATE INDEX "IDX_0b13cceb880257f3bf19888bee" ON "appTenants" ("uid") `);
        await queryRunner.query(`CREATE INDEX "IDX_417f77f1a7dce054b0425f9b65" ON "appTenants" ("name") `);
        await queryRunner.query(`CREATE INDEX "IDX_0b623b73d9c8ba29192e0d1403" ON "appDirectories" ("uid") `);
        await queryRunner.query(`CREATE INDEX "IDX_e02d28cf5ee2623146cc037e61" ON "appDirectories" ("name") `);
        await queryRunner.query(`CREATE INDEX "IDX_ebd7f2735f74f7bd7d7dec47fc" ON "appEmailLogs" ("emailType") `);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnitTypeChildren" ADD CONSTRAINT "FK_942bef18ad890e38796a665c4e6" FOREIGN KEY ("parentTypeId") REFERENCES "appOrganizationalUnitTypes"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnitTypes" ADD CONSTRAINT "FK_d45bb2b95e57326ab7f47be5508" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnits" ADD CONSTRAINT "FK_a80ef40a58d0a596b6c1a650aa2" FOREIGN KEY ("typeId") REFERENCES "appOrganizationalUnitTypes"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnits" ADD CONSTRAINT "FK_366825c6f9173d2bc9bdee1e813" FOREIGN KEY ("parentId") REFERENCES "appOrganizationalUnits"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnits" ADD CONSTRAINT "FK_990da363f706979636fb643cf33" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnits" DROP CONSTRAINT "FK_990da363f706979636fb643cf33"`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnits" DROP CONSTRAINT "FK_366825c6f9173d2bc9bdee1e813"`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnits" DROP CONSTRAINT "FK_a80ef40a58d0a596b6c1a650aa2"`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnitTypes" DROP CONSTRAINT "FK_d45bb2b95e57326ab7f47be5508"`);
        await queryRunner.query(`ALTER TABLE "appOrganizationalUnitTypeChildren" DROP CONSTRAINT "FK_942bef18ad890e38796a665c4e6"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ebd7f2735f74f7bd7d7dec47fc"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_e02d28cf5ee2623146cc037e61"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_0b623b73d9c8ba29192e0d1403"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_417f77f1a7dce054b0425f9b65"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_0b13cceb880257f3bf19888bee"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_a889a820118428cb4f33a9b87d"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_6c3c432704fb68162ded1726c7"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_a19bdcb338271c7870facf3538"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_cb3b95e82208430e0683e37c28"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_e47962f69d16a8d9f640c879ba"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_01574f07307743ee30cc0216fa"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_fb1522df88449639c4229af5d6"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_0c26cb377b2806be9d9a6b595f"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_666abd9daf84bef60ed4dd1963"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_9f9dba10ea0bca995dcfa30775"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_3787fcaf49d009c4f1cef7c696"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ae80324d1986927eccbfaced47"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_b5e2713ed379040d3040a3b8c1"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_e4c3da8997e4c51444ea05372b"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_990da363f706979636fb643cf3"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_366825c6f9173d2bc9bdee1e81"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_0cc82557bdcb02b2fe393d9568"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_34440045a38cdb03752a03883a"`);
        await queryRunner.query(`DROP TABLE "appOrganizationalUnits"`);
        await queryRunner.query(`DROP TABLE "appOrganizationalUnitTypes"`);
        await queryRunner.query(`DROP TABLE "appOrganizationalUnitTypeChildren"`);
    }

}
