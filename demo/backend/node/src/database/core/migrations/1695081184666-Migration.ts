import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1695081184666 implements MigrationInterface {
    name = 'Migration1695081184666'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "crmContacts" ("id" uuid NOT NULL, "email" character varying(255) NOT NULL, "firstName" character varying(255) NOT NULL, "lastName" character varying(255) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "organizationId" uuid, CONSTRAINT "PK_9b97f6e1f1b10c9b05e9b5d4886" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "crmContacts" ADD CONSTRAINT "FK_509de8cf990ae74b559518b6f52" FOREIGN KEY ("organizationId") REFERENCES "appOrganizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "crmContacts" DROP CONSTRAINT "FK_509de8cf990ae74b559518b6f52"`);
        await queryRunner.query(`DROP TABLE "crmContacts"`);
    }

}
