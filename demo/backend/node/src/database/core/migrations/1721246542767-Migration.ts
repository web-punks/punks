import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1721246542767 implements MigrationInterface {
    name = 'Migration1721246542767'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "appOperationLocks" ("id" uuid NOT NULL, "uid" character varying(255) NOT NULL, "lockedBy" character varying(255), "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "UQ_6e0355605c6763a6056290f1ac9" UNIQUE ("uid"), CONSTRAINT "PK_7ded1ddddc573a68006cf3598d5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appJobDefinitions" ("id" uuid NOT NULL, "uid" character varying(64) NOT NULL, "name" character varying(256) NOT NULL, "provider" character varying(64) NOT NULL, "infrastructureParams" jsonb NOT NULL, "invocationParams" jsonb NOT NULL, "schedules" jsonb NOT NULL, "concurrency" character varying(64) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "UQ_c334eba20827cecad14ce1719f1" UNIQUE ("uid"), CONSTRAINT "PK_7d0b5505e7163e81d315d58f9b8" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "appJobInstances" ("id" uuid NOT NULL, "scheduleUid" character varying(256), "jobUid" character varying(256) NOT NULL, "runType" character varying(256) NOT NULL, "status" character varying(256) NOT NULL, "insertTime" TIMESTAMP WITH TIME ZONE, "startTime" TIMESTAMP WITH TIME ZONE, "endTime" TIMESTAMP WITH TIME ZONE, "result" jsonb, "error" jsonb, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_d05838b92b871f2ae2a1ee1b6c1" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "appJobInstances"`);
        await queryRunner.query(`DROP TABLE "appJobDefinitions"`);
        await queryRunner.query(`DROP TABLE "appOperationLocks"`);
    }

}
