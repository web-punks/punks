import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1693043029205 implements MigrationInterface {
    name = 'Migration1693043029205'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "foos" ("id" uuid NOT NULL, "name" character varying(255) NOT NULL, "uid" character varying(255) NOT NULL, "createdOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_9e23e5c82189ece8bd7deea61e0" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "foos"`);
    }

}
