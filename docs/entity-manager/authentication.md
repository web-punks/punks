# Entity manager authentication

## Nest Authentication context

The nest authentication context is managed trough a custom authentication middleware specified inside `AuthenticationModule`:

```
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(appSessionMiddleware)
      .forRoutes("*")
      .apply(AuthenticationMiddleware)
      .forRoutes("*")
  }
```

### App session middleware

The first middleware is used to persist the express `req` object:

```
export function appSessionMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
) {
  sessionStorage.run(new AppSession(req, res), () => {
    next()
  })
}
```

The class `AppSession` is a dictionary like class to store data in memory. The component `sessionStorage` is a request-scoped storage for storing a value and be able to retrieve globally from the code:

Initialization in session storage:

```
  sessionStorage.run(new AppSession(req, res), () => {
    next()
  })
```

Usage from session storage:

```
sessionStorage.getStore()
```

Note: the object can only be retrieved form code that runs inside `run` store method

Source: https://nodejs.org/api/async_context.html

### Authentication Middleware

The `AuthenticationMiddleware` is responsible to retrieve the original express `req` object and to extract the authentication information from it:

For interactive users logged with jwt the following code is executed in order to create the context from the token:

```
const token = this.extractTokenFromHeader(req)
    if (token) {
      const parsedToken = await this.authService.userTokenVerify({ token })
      if (parsedToken.isValid) {
        const user = await this.getUserFromToken(parsedToken.data)
        const roles = user ? await this.getUserRoles(user) : []
        const permissions = user ? await this.getUserPermissions(user) : []
        const organizationalUnits = user
          ? await this.getUserOrganizationalUnits(user)
          : []

        authData = {
          ...(authData ?? {}),
          user,
          roles,
          permissions,
          organizationalUnits,
        }
      }
    }
```

### Authentication context accessor

Inside the nestjs project a authentication context provider that implements `IAuthenticationContextProvider<AppUserContext>` interface should be specified inside the app `initialize` method:

```
    await app.get(EntityManagerInitializer).initialize(app, {
      modulesContainer: app.get(ModulesContainer),
      authenticationProvider: app.get(AppAuthContextProvider),
      settings: getEntityManagerSettings(),
    })
```
