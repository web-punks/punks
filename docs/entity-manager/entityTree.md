# Entities trees

## Backend

In order to implement tree entities, you need to do the following:

- Add a parent relation inside the database entity

- Add `includeChildrenMap?: boolean` to `<Entity>SearchOptions`` class

- Add `childrenMap` property to `<Entity>SearchResponse`` class

```
  @ApiProperty({
    type: "object",
    required: false,
  })
  childrenMap?: EntitiesChildrenMap
```

- Implement `calculateChildrenMap` method inside `<Entity>QueryBuilder`` class specifying the name of the id field and the parent field:

```
  protected async calculateChildrenMap(
    items: FooEntity[]
  ): Promise<{ [x: string]: { count: number } }> {
    return await this.queryChildrenMap({
      idField: "id",
      parentField: "parent",
      nodeIds: items.map((item) => item.id),
    })
  }
```

## Frontend

Inside the frontend add the traverse parameters inside the provider. Add `traverse: input.traverse` and `includeChildrenMap: true` options to the fetch operation.

Example:

```
export const FooProvider = ({
  children,
  dataFilter,
  mode,
}: FooProviderProps) => {
  return (
    <FooEntityProviders
    mode={mode}
    dataFilter={dataFilter}
    options={{
      queryFilterStrategy: "server",
    }}
    operations={{
      fetch: {
        action: (input) =>
          BackendClient.fooSearch({
            params: {
              filters: input.filters,
              traverse: input.traverse,
              options: {
                includeFacets: true,
                includeChildrenMap: true,
              },
              query: input.query
                ? {
                    term: input.query,
                    fields: ["name", "uid"],
                  }
                : undefined,
              paging: input.paging,
              sorting: input.sorting
                ? {
                    fields: [
                      {
                        field: input.sorting.value,
                        direction: input.sorting
                          .direction as FooSearchSortingField.direction,
                      },
                    ],
                  }
                : {
                    fields: [
                      {
                        field: FooSearchSortingField.field.NAME,
                        direction: FooSearchSortingField.direction.ASC,
                      },
                    ],
                  },
            },
          }),
      },
      create: {
        action: (input) =>
          BackendClient.fooCreate({
            ...input.data,
            parentId: input.parent?.id,
          }),
      },
      update: {
        action: (input) => BackendClient.fooUpdate(input.id, input.data),
      },
      delete: {
        action: (input) => BackendClient.fooDelete(input.item.id),
      },
      get: {
        action: (input) => BackendClient.fooGet(input.item.id),
      },
      export: {
        action: () =>
          BackendClient.fooExport({
            options: {
              format: FooEntitiesExportOptions.format.XLSX,
            },
          }),
      },
      sampleDownload: {
        action: () =>
          BackendClient.fooSampleDownload({
            format: FooEntitiesExportOptions.format.XLSX,
          }),
      },
      import: {
        action: async (input) =>
          BackendClient.fooImport(FooEntitiesExportOptions.format.XLSX, {
            file: input.file,
          }),
      },
      versions: {
        action: (input) =>
          BackendClient.fooVersions({
            params: {
              entity: {
                entityId: input.id,
              },
              sorting: {
                direction:
                  input.sorting.direction === "desc"
                    ? EntityVersionsSorting.direction.DESC
                    : EntityVersionsSorting.direction.ASC,
              },
            },
          }),
      },
    }}
    modals={{
      versions: {
        table: {
          columns: [
            {
              key: "age",
              title: "Age",
              render: (x) => x.age,
            },
            {
              key: "name",
              title: "Name",
              render: (x) => x.name,
            },
            {
              key: "type",
              title: "Type",
              render: (x) => x.type,
            },
            {
              key: "timestamp",
              title: "Timestamp",
              render: (x) => x.updatedOn,
            },
          ],
        },
      },
    }}
    metadata={{
      name: "Foo",
    }}
    initialPaging={{
      pageSize: 5,
    }}
    forms={{
      create: FooCreateForm,
      createAdditionalProps: {
        foo: "bar",
      },
      update: FooUpdateForm,
      updateAdditionalProps: {
        foo: "bar",
      },
      details: () => <div>details</div>,
      detailsAdditionalProps: {
        foo: "bar",
      },
    }}
    selectors={{
      getDisplayName: (entity) => entity.name,
      getId: (entity) => entity.id,
      getParentId: (entity) => entity.parentId,
    }}
    labels={fooLabels}>
      {children}
    </FooEntityProviders>
  )
}
```

After modifying the provider just use `EntityTreeTableController` instead of `EntityTableController` component.
