import * as route53 from "@aws-cdk/aws-route53"
import * as cdk from "@aws-cdk/core"
import { EnvironmentType } from "../lib/env"

export const lookupRoute53Zone = (scope: cdk.Construct, domainName: string) =>
  route53.HostedZone.fromLookup(scope, "Zone", {
    domainName,
  })

export const lookupDefaultRoute53Zone = (
  scope: cdk.Construct,
  environment: EnvironmentType
) =>
  route53.HostedZone.fromLookup(scope, "Zone", {
    domainName: environment.domainName,
  })
