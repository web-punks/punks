import { EnvironmentType } from "../lib/env"

export const buildDns = (
  subdomain: string,
  environment: EnvironmentType,
  domainName?: string
) =>
  `${environment.dnsPrefix}${subdomain}.${domainName ?? environment.domainName}`

export const vpcName = (stack: string, environment: EnvironmentType) =>
  `${environment.id}-${stack}-vpc`
