import * as dotenv from "dotenv"
import "source-map-support/register"
import * as path from "path"
import * as cdk from "@aws-cdk/core"
import * as ec2 from "@aws-cdk/aws-ec2"
import { Environment } from "../lib/env"
import { StaticSiteStack } from "../lib/StaticSite"
import { BackendStack } from "../lib/Backend"

dotenv.config()

const rootDir = process.env.PROJ_ROOT_DIR || path.join(__dirname, "..", "..")
const staticSites = [
  {
    id: "sanity-demo-web",
    path: path.join(rootDir, "demo", "sanity", "web", "public"),
    domainName: "punx.it",
    siteSubDomain: "sanity-demo-web",
  },
]

const app = new cdk.App()

const environments = [Environment.Production, Environment.Staging]

environments.forEach((environment) => {
  const commonStackProps = {
    environment,
    env: {
      account: process.env.CDK_DEFAULT_ACCOUNT,
      region: process.env.CDK_DEFAULT_REGION,
    },
  }

  staticSites.map(
    (site) =>
      new StaticSiteStack(app, `${environment.id}-site-${site.id}`, {
        projectPath: site.path,
        domainName: site.domainName,
        siteSubDomain: site.siteSubDomain,
        ...commonStackProps,
      })
  )

  new BackendStack(app, `${environment.id}-backend`, {
    subDomain: "api",
    ...commonStackProps,
  })
})
