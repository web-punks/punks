import * as fs from "fs"
import * as cloudfront from "@aws-cdk/aws-cloudfront"
import * as route53 from "@aws-cdk/aws-route53"
import * as s3 from "@aws-cdk/aws-s3"
import * as s3deploy from "@aws-cdk/aws-s3-deployment"
import * as acm from "@aws-cdk/aws-certificatemanager"
import * as cdk from "@aws-cdk/core"
import * as targets from "@aws-cdk/aws-route53-targets/lib"

import { EnvironmentType } from "../env"
import { buildDns } from "../../common/resourceNames"

export interface StaticSiteProps extends cdk.StackProps {
  domainName: string
  siteSubDomain: string
  projectPath: string
  environment: EnvironmentType
}

export class StaticSite extends cdk.Construct {
  constructor(parent: cdk.Construct, name: string, props: StaticSiteProps) {
    super(parent, name)

    const zone = route53.HostedZone.fromLookup(this, "Zone", {
      domainName: props.domainName,
    })

    const siteDomain = buildDns(props.siteSubDomain, props.environment)
    new cdk.CfnOutput(this, "Site", { value: "https://" + siteDomain })

    // Content bucket
    const siteBucket = new s3.Bucket(this, `SiteBucket-${siteDomain}`, {
      bucketName: `static-site-${siteDomain}`,
      websiteIndexDocument: "index.html",
      publicReadAccess: true,

      // The default removal policy is RETAIN, which means that cdk destroy will not attempt to delete
      // the new bucket, and it will remain in your account until manually deleted. By setting the policy to
      // DESTROY, cdk destroy will attempt to delete the bucket, but will error if the bucket is not empty.
      removalPolicy: cdk.RemovalPolicy.DESTROY, // NOT recommended for production code
    })
    new cdk.CfnOutput(this, "Bucket", { value: siteBucket.bucketName })

    // TLS certificate
    const certificateArn = new acm.DnsValidatedCertificate(
      this,
      "SiteCertificate",
      {
        domainName: siteDomain,
        hostedZone: zone,
        region: "us-east-1", // Cloudfront only checks this region for certificates.
      }
    ).certificateArn
    new cdk.CfnOutput(this, "Certificate", { value: certificateArn })

    // CloudFront distribution that provides HTTPS
    const distribution = new cloudfront.CloudFrontWebDistribution(
      this,
      `dist-${siteDomain}`,
      {
        aliasConfiguration: {
          acmCertRef: certificateArn,
          names: [siteDomain],
          sslMethod: cloudfront.SSLMethod.SNI,
          securityPolicy: cloudfront.SecurityPolicyProtocol.TLS_V1_1_2016,
        },
        originConfigs: [
          {
            customOriginSource: {
              domainName: siteBucket.bucketWebsiteDomainName,
              originProtocolPolicy: cloudfront.OriginProtocolPolicy.HTTP_ONLY,
            },
            behaviors: [{ isDefaultBehavior: true }],
          },
        ],
      }
    )
    new cdk.CfnOutput(this, "DistributionId", {
      value: distribution.distributionId,
    })
    new cdk.CfnOutput(this, "DistributionUrl", {
      value: distribution.distributionDomainName,
    })

    new route53.ARecord(this, "SiteAliasRecord", {
      recordName: siteDomain,
      target: route53.RecordTarget.fromAlias(
        new targets.CloudFrontTarget(distribution)
      ),
      zone,
    })

    if (fs.existsSync(props.projectPath)) {
      new s3deploy.BucketDeployment(
        this,
        `DeployWithInvalidation-${siteDomain}`,
        {
          sources: [s3deploy.Source.asset(props.projectPath)],
          destinationBucket: siteBucket,
          distribution,
          distributionPaths: ["/*"],
        }
      )
    } else {
      console.warn(
        "Failed to find directory - make sure that current stack has assets dir",
        props.projectPath
      )
    }
  }
}
