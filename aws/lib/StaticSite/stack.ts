import * as cdk from "@aws-cdk/core"
import { StaticSite, StaticSiteProps } from "./construct"

export class StaticSiteStack extends cdk.Stack {
  constructor(scope: cdk.Construct, name: string, props: StaticSiteProps) {
    super(scope, name, props)

    new StaticSite(this, name, props)
    cdk.Tags.of(this).add("Environment", props.environment.name)
  }
}
