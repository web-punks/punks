export const Environment = {
  Production: {
    id: "prod",
    name: "Production",
    dnsPrefix: "",
    domainName: "punx.it",
  },
  Staging: {
    id: "staging",
    name: "Staging",
    dnsPrefix: "stg.",
    domainName: "punx.it",
  },
}
export type EnvironmentType = typeof Environment.Production
