import * as cdk from "@aws-cdk/core"
import * as ec2 from "@aws-cdk/aws-ec2"
import { vpcName } from "../../common/resourceNames"
import { Backend, BackendProps } from "./construct"

export class BackendStack extends cdk.Stack {
  constructor(scope: cdk.Construct, name: string, props: BackendProps) {
    super(scope, name, props)

    const vpc = new ec2.Vpc(this, vpcName("backend", props.environment), {
      maxAzs: 2,
      cidr: "10.1.0.0/16",
    })

    new Backend(this, name, props, vpc)

    cdk.Tags.of(this).add("Environment", props.environment.name)
  }
}
