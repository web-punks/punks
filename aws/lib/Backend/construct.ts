import * as cdk from "@aws-cdk/core"
import * as ec2 from "@aws-cdk/aws-ec2"
import * as ecs from "@aws-cdk/aws-ecs"
import * as elbv2 from "@aws-cdk/aws-elasticloadbalancingv2"
import * as acm from "@aws-cdk/aws-certificatemanager"
import * as route53 from "@aws-cdk/aws-route53"
import * as targets from "@aws-cdk/aws-route53-targets/lib"
import * as ecs_patterns from "@aws-cdk/aws-ecs-patterns"

import { EnvironmentType } from "../env"
import { buildDns } from "../../common/resourceNames"
import { lookupDefaultRoute53Zone } from "../../common/lookup"

export interface BackendProps extends cdk.StackProps {
  subDomain: string
  environment: EnvironmentType
}

export class Backend extends cdk.Construct {
  constructor(
    parent: cdk.Construct,
    name: string,
    props: BackendProps,
    vpc: ec2.Vpc
  ) {
    super(parent, name)

    // Create a cluster
    const cluster = new ecs.Cluster(parent, "Cluster", { vpc })

    const lbDns = buildDns(props.subDomain, props.environment)

    // // TLS certificate
    // const certificateArn = new acm.DnsValidatedCertificate(
    //   this,
    //   "LbCertificate",
    //   {
    //     domainName: lbDns,
    //     hostedZone: lookupDefaultRoute53Zone(parent, props.environment),
    //     region: "us-east-1",
    //   }
    // ).certificateArn
    // new cdk.CfnOutput(this, "Certificate", {
    //   value: certificateArn,
    // })

    // Instantiate Fargate Service with just cluster and image
    const service = new ecs_patterns.ApplicationLoadBalancedFargateService(
      this,
      "Service",
      {
        cluster,
        taskImageOptions: {
          image: ecs.ContainerImage.fromRegistry("amazon/amazon-ecs-sample"),
        },
        domainName: lbDns,
        domainZone: lookupDefaultRoute53Zone(this, props.environment),
        protocol: elbv2.ApplicationProtocol.HTTPS,
      }
    )

    service.loadBalancer.addRedirect()
  }
}
