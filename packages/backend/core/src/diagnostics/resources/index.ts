import { Log } from "../../logging"

export const logMemoryUsage = () => {
  const usedMemory = process.memoryUsage()

  Log.getLogger("Diagnostics").debug(
    `Memory usage: \n${Object.keys(usedMemory)
      .map(
        (x) =>
          `${x} ${
            Math.round(((usedMemory as any)[x] / 1024 / 1024) * 100) / 100
          } MB`
      )
      .join("\n")}`
  )
}
