export type LocalizedField<T> = {
  [key: string]: T
}
