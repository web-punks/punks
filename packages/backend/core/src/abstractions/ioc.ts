export interface IResolveServiceOptions {
  optional?: boolean
}

export interface IServiceLocator {
  register<T>(name: string, service: T): void
  resolve<T>(name: string, options?: IResolveServiceOptions): T
}
