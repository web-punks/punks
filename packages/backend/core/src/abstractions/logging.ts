export enum LogLevel {
  Debug = "debug",
  Info = "info",
  Warn = "warn",
  Error = "error",
  Fatal = "fatal",
}

export enum MetaSerializationType {
  None = "none",
  JSON = "json",
}

export interface ILoggerProvider {
  debug(loggerName: string, message: string, meta?: any): void
  info(loggerName: string, message: string, meta?: any): void
  warn(loggerName: string, message: string, meta?: any): void
  error(loggerName: string, message: string, meta?: any): void
  fatal(loggerName: string, message: string, meta?: any): void
  exception(loggerName: string, message: string, error: Error, meta?: any): void
}

export interface ILogger {
  debug(message: string, meta?: any): void
  info(message: string, meta?: any): void
  warn(message: string, meta?: any): void
  error(message: string, meta?: any): void
  fatal(message: string, meta?: any): void
  exception(message: string, error: Error, meta?: any): void
}
