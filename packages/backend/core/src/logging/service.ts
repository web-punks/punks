import {
  ILogger,
  ILoggerProvider,
  LogLevel,
  MetaSerializationType,
} from "../abstractions"
import { jsonSerialize, truncateString } from "../utils"
import { getLevelValue } from "./utils"

export type LoggerInstance = {
  options: LoggerOptions
  provider: ILoggerProvider
}

type LoggerContainer = {
  instances: LoggerInstance[]
}

type LogOptions = {
  truncateMetaDisabled?: boolean
}

class InternalLogger implements ILogger {
  constructor(
    private readonly loggerName: string,
    private readonly container: LoggerContainer
  ) {}

  debug(message: string, meta?: any, options?: LogOptions): void {
    this.getEnabledInstances(LogLevel.Debug).forEach((x) =>
      x.provider.debug(
        this.loggerName,
        message,
        this.serializeMeta(x.options, meta, options)
      )
    )
  }

  info(message: string, meta?: any, options?: LogOptions): void {
    this.getEnabledInstances(LogLevel.Info).forEach((x) =>
      x.provider.info(
        this.loggerName,
        message,
        this.serializeMeta(x.options, meta, options)
      )
    )
  }

  warn(message: string, meta?: any, options?: LogOptions): void {
    this.getEnabledInstances(LogLevel.Warn).forEach((x) =>
      x.provider.warn(
        this.loggerName,
        message,
        this.serializeMeta(x.options, meta, options)
      )
    )
  }

  error(message: string, meta?: any, options?: LogOptions): void {
    this.getEnabledInstances(LogLevel.Error).forEach((x) =>
      x.provider.error(
        this.loggerName,
        message,
        this.serializeMeta(x.options, meta, options)
      )
    )
  }

  fatal(message: string, meta?: any, options?: LogOptions): void {
    this.getEnabledInstances(LogLevel.Fatal).forEach((x) =>
      x.provider.fatal(
        this.loggerName,
        message,
        this.serializeMeta(x.options, meta, options)
      )
    )
  }

  exception(
    message: string,
    error: Error,
    meta?: any,
    options?: LogOptions
  ): void {
    this.getEnabledInstances(LogLevel.Error).forEach((x) =>
      x.provider.exception(
        this.loggerName,
        message,
        error,
        this.serializeMeta(x.options, meta, options)
      )
    )
  }

  private getEnabledInstances(level: LogLevel) {
    return this.container.instances.filter(
      (x) =>
        x.options.enabled &&
        getLevelValue(x.options.level) <= getLevelValue(level)
    )
  }

  private serializeMeta(
    options: LoggerOptions,
    meta?: any,
    logOptions?: LogOptions
  ): any {
    if (meta && typeof meta === "string") {
      return meta
    }

    const serializedMeta = jsonSerialize(meta, {
      prettify: true,
      stripBinaryData: true,
    })

    switch (options.serialization) {
      case MetaSerializationType.JSON:
        return serializedMeta &&
          options.maxMetaLength &&
          !logOptions?.truncateMetaDisabled
          ? truncateString(serializedMeta, options.maxMetaLength, "...")
          : serializedMeta
      case MetaSerializationType.None:
      default:
        return serializedMeta ? JSON.parse(serializedMeta) : undefined
    }
  }
}

type LoggerOptions = {
  enabled: boolean
  level: LogLevel
  loggerName?: string
  serialization: MetaSerializationType
  maxMetaLength?: number
}

export class Log {
  private static readonly container: LoggerContainer = {
    instances: [],
  }

  static configure(instance: LoggerInstance) {
    this.container.instances.push(instance)
  }

  static getLogger(loggerName: string): ILogger {
    return new InternalLogger(loggerName, this.container)
  }
}
