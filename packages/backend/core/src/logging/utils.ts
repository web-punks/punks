import { LogLevel } from "../abstractions"

export const getLevelValue = (level: LogLevel) => {
  switch (level) {
    case LogLevel.Debug:
      return 0
    case LogLevel.Info:
      return 1
    case LogLevel.Warn:
      return 2
    case LogLevel.Error:
      return 3
    case LogLevel.Fatal:
      return 4
  }
}
