export type AppServiceReference = {
  serviceName: string
  appName: string
  roleName: string
  environmentName: string
}
