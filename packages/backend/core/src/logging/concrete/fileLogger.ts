import * as winston from "winston"
import { ILoggerProvider } from "../../abstractions"
import { AppServiceReference } from "../types"

export type FileLoggerSettings = {
  folder?: string
}

export type FileLoggingSettings = {
  service: AppServiceReference
  files?: FileLoggerSettings
}

const createLoggerInstance = (settings: FileLoggingSettings) => {
  const folder = settings.files?.folder ?? "logs"
  const logger = winston.createLogger({
    level: "debug",
    format: winston.format.combine(
      winston.format.timestamp({
        format: "YYYY-MM-DD HH:mm:ss",
      }),
      winston.format.errors({ stack: true }),
      winston.format.splat(),
      winston.format.json()
    ),
    defaultMeta: {
      service: settings.service,
      app: settings.service.appName,
      role: settings.service.roleName,
      environment: settings.service.environmentName,
    },
    transports: [
      new winston.transports.File({
        filename: `./${folder}/error.log`,
        level: "error",
      }),
      new winston.transports.File({ filename: `./${folder}/combined.log` }),
    ],
  })

  logger.exceptions.handle(
    new winston.transports.File({
      filename: `./${folder}/unhandledError.log`,
      level: "error",
    })
  )

  return logger
}

const formatMessage = (loggerName: string, message: string) =>
  `${loggerName ? `[${loggerName}] ` : ""}${message}`

export class FileLogger implements ILoggerProvider {
  private readonly logger = createLoggerInstance(this.settings)

  constructor(private readonly settings: FileLoggingSettings) {}

  debug(loggerName: string, message: string, meta?: any): void {
    this.logger.debug(formatMessage(loggerName, message), meta)
  }

  info(loggerName: string, message: string, meta?: any): void {
    this.logger.info(formatMessage(loggerName, message), meta)
  }

  warn(loggerName: string, message: string, meta?: any): void {
    this.logger.warn(formatMessage(loggerName, message), meta)
  }

  error(loggerName: string, message: string, meta?: any): void {
    this.logger.error(formatMessage(loggerName, message), meta)
  }

  fatal(loggerName: string, message: string, meta?: any): void {
    this.logger.error(formatMessage(loggerName, message), meta)
  }

  exception(
    loggerName: string,
    message: string,
    error: Error,
    meta?: any
  ): void {
    this.logger.error(formatMessage(loggerName, message), error, meta)
  }
}
