import * as winston from "winston"
import { ILoggerProvider } from "../../abstractions"
import { AppServiceReference } from "../types"

export type DatadogApiSettings = {
  apiKey: string
  site: string
}

export type DatadogLoggingSettings = {
  service: AppServiceReference
  datadog: DatadogApiSettings
}

const createHttpTransportOptions = (settings: DatadogLoggingSettings) => ({
  host: `http-intake.logs.${settings.datadog.site}`,
  path: `/api/v2/logs?dd-api-key=${settings.datadog.apiKey}&ddsource=nodejs&service=${settings.service.serviceName}`,
  ssl: true,
})

const createLoggerInstance = (settings: DatadogLoggingSettings) => {
  const logger = winston.createLogger({
    level: "debug",
    format: winston.format.combine(
      winston.format.timestamp({
        format: "YYYY-MM-DD HH:mm:ss",
      }),
      winston.format.errors({ stack: true }),
      winston.format.splat(),
      winston.format.json()
    ),
    defaultMeta: {
      service: settings.service,
      app: settings.service.appName,
      role: settings.service.roleName,
      environment: settings.service.environmentName,
    },
    transports: [
      new winston.transports.Http(createHttpTransportOptions(settings)),
    ],
  })

  logger.exceptions.handle(
    new winston.transports.Http(createHttpTransportOptions(settings))
  )

  return logger
}

const formatMessage = (loggerName: string, message: string) =>
  `${loggerName ? `[${loggerName}] ` : ""}${message}`

export class DatadogLogger implements ILoggerProvider {
  private readonly logger = createLoggerInstance(this.settings)

  constructor(private readonly settings: DatadogLoggingSettings) {}

  debug(loggerName: string, message: string, meta?: any): void {
    this.logger.debug(formatMessage(loggerName, message), meta)
  }

  info(loggerName: string, message: string, meta?: any): void {
    this.logger.info(formatMessage(loggerName, message), meta)
  }

  warn(loggerName: string, message: string, meta?: any): void {
    this.logger.warn(formatMessage(loggerName, message), meta)
  }

  error(loggerName: string, message: string, meta?: any): void {
    this.logger.error(formatMessage(loggerName, message), meta)
  }

  fatal(loggerName: string, message: string, meta?: any): void {
    this.logger.error(formatMessage(loggerName, message), meta)
  }

  exception(
    loggerName: string,
    message: string,
    error: Error,
    meta?: any
  ): void {
    this.logger.error(formatMessage(loggerName, message), error, meta)
  }
}
