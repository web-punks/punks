import { ConsoleLogger } from "../consoleLogger"

describe("console logger", () => {
  it("should log messages", () => {
    const logger = new ConsoleLogger()

    logger.debug("test", "debug message")
    logger.info("test", "info message")
    logger.warn("test", "warn message")
    logger.error("test", "error message")
    logger.fatal("test", "fatal message")
    logger.exception("test", "error message", new Error("exception details"))
  })
})
