import { ILoggerProvider } from "../../abstractions"

export class ConsoleLogger implements ILoggerProvider {
  debug(loggerName: string, message: string, meta?: any): void {
    if (meta) {
      console.log(
        `\x1b[96m${loggerName ? `[${loggerName}] ` : ""}${message}\x1b[0m`,
        meta
      )
    } else {
      console.log(
        `\x1b[96m${loggerName ? `[${loggerName}] ` : ""}${message}\x1b[0m`
      )
    }
  }

  info(loggerName: string, message: string, meta?: any): void {
    if (meta) {
      console.info(
        `\x1b[34m${loggerName ? `[${loggerName}] ` : ""}${message}\x1b[0m`,
        meta
      )
    } else {
      console.info(
        `\x1b[34m${loggerName ? `[${loggerName}] ` : ""}${message}\x1b[0m`
      )
    }
  }

  warn(loggerName: string, message: string, meta?: any): void {
    if (meta) {
      console.warn(
        `\x1b[33m${loggerName ? `[${loggerName}] ` : ""}${message}\x1b[0m`,
        meta
      )
    } else {
      console.warn(
        `\x1b[33m${loggerName ? `[${loggerName}] ` : ""}${message}\x1b[0m`
      )
    }
  }

  error(loggerName: string, message: string, meta?: any): void {
    if (meta) {
      console.error(`${loggerName ? `[${loggerName}] ` : ""}${message}`, meta)
    } else {
      console.error(`${loggerName ? `[${loggerName}] ` : ""}${message}`)
    }
  }

  fatal(loggerName: string, message: string, meta?: any): void {
    if (meta) {
      console.error(`${loggerName ? `[${loggerName}] ` : ""}${message}`, meta)
    } else {
      console.error(`${loggerName ? `[${loggerName}] ` : ""}${message}`)
    }
  }

  exception(
    loggerName: string,
    message: string,
    error: Error,
    meta?: any
  ): void {
    if (meta) {
      console.error(
        `${loggerName ? `[${loggerName}] ` : ""}${message}`,
        error,
        meta
      )
    } else {
      console.error(`${loggerName ? `[${loggerName}] ` : ""}${message}`, error)
    }
  }
}
