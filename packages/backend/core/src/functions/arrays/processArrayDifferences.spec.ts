import { processArrayDifferences } from "."

type Element = { id: number }

describe("processArrayDifferences", () => {
  const elementIdSelector = (item: { id: number }) => item.id
  const desiredItemSelector = (item: { id: number }) => item.id

  it("handles all matching elements", () => {
    const elements = [{ id: 1 }, { id: 2 }]
    const desiredItems = [{ id: 1 }, { id: 2 }]

    const result = processArrayDifferences({
      elements,
      elementIdSelector,
      desiredItems,
      desiredItemSelector,
    })

    expect(result.missingElements).toEqual([])
    expect(result.exceedingElements).toEqual([])
    expect(result.correspondingElements.map((x) => x.current)).toEqual(elements)
    expect(result.correspondingElements.map((x) => x.desired)).toEqual(elements)
  })

  it("identifies missing elements", () => {
    const elements = [{ id: 1 }]
    const desiredItems = [{ id: 1 }, { id: 2 }]

    const result = processArrayDifferences({
      elements,
      elementIdSelector,
      desiredItems,
      desiredItemSelector,
    })

    expect(result.missingElements).toEqual([{ id: 2 }])
    expect(result.exceedingElements).toEqual([])
    expect(result.correspondingElements.map((x) => x.current)).toEqual([
      { id: 1 },
    ])
    expect(result.correspondingElements.map((x) => x.desired)).toEqual([
      { id: 1 },
    ])
  })

  it("identifies exceeding elements", () => {
    const elements = [{ id: 1 }, { id: 2 }]
    const desiredItems = [{ id: 1 }]

    const result = processArrayDifferences({
      elements,
      elementIdSelector,
      desiredItems,
      desiredItemSelector,
    })

    expect(result.missingElements).toEqual([])
    expect(result.exceedingElements).toEqual([{ id: 2 }])
    expect(result.correspondingElements.map((x) => x.current)).toEqual([
      { id: 1 },
    ])
    expect(result.correspondingElements.map((x) => x.desired)).toEqual([
      { id: 1 },
    ])
  })

  it("handles completely different arrays", () => {
    const elements = [{ id: 1 }]
    const desiredItems = [{ id: 2 }]

    const result = processArrayDifferences({
      elements,
      elementIdSelector,
      desiredItems,
      desiredItemSelector,
    })

    expect(result.missingElements).toEqual([{ id: 2 }])
    expect(result.exceedingElements).toEqual([{ id: 1 }])
    expect(result.correspondingElements.map((x) => x.current)).toEqual([])
    expect(result.correspondingElements.map((x) => x.desired)).toEqual([])
  })

  it("handles empty elements array", () => {
    const elements: Element[] = []
    const desiredItems = [{ id: 1 }, { id: 2 }]

    const result = processArrayDifferences({
      elements,
      elementIdSelector,
      desiredItems,
      desiredItemSelector,
    })

    expect(result.missingElements).toEqual(desiredItems)
    expect(result.correspondingElements.map((x) => x.current)).toEqual([])
    expect(result.correspondingElements.map((x) => x.desired)).toEqual([])
  })

  it("handles empty desiredItems array", () => {
    const elements = [{ id: 1 }, { id: 2 }]
    const desiredItems: Element[] = []

    const result = processArrayDifferences({
      elements,
      elementIdSelector,
      desiredItems,
      desiredItemSelector,
    })

    expect(result.missingElements).toEqual([])
    expect(result.exceedingElements).toEqual(elements)
    expect(result.correspondingElements.map((x) => x.current)).toEqual([])
    expect(result.correspondingElements.map((x) => x.desired)).toEqual([])
  })

  it("handles both arrays being empty", () => {
    const elements: Element[] = []
    const desiredItems: Element[] = []

    const result = processArrayDifferences({
      elements,
      elementIdSelector,
      desiredItems,
      desiredItemSelector,
    })

    expect(result.missingElements).toEqual([])
    expect(result.exceedingElements).toEqual([])
    expect(result.correspondingElements.map((x) => x.current)).toEqual([])
    expect(result.correspondingElements.map((x) => x.desired)).toEqual([])
  })
})
