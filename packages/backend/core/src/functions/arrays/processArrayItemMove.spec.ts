import { processArrayItemMove } from "."

describe("processArrayItemMove", () => {
  it("should move item forward", () => {
    const items = ["a", "b", "c"]
    const input = {
      oldIndex: 0,
      newIndex: 1,
    }
    const result = processArrayItemMove(items, input)
    expect(result).toEqual([
      {
        item: "b",
        oldIndex: 1,
        newIndex: 0,
        isMoved: true,
      },
      {
        item: "a",
        oldIndex: 0,
        newIndex: 1,
        isMoved: true,
      },
      {
        item: "c",
        oldIndex: 2,
        newIndex: 2,
        isMoved: false,
      },
    ])
  })

  it("should move item backward from beginning", () => {
    const items = ["a", "b", "c"]
    const input = {
      oldIndex: 1,
      newIndex: 0,
    }
    const result = processArrayItemMove(items, input)
    expect(result).toEqual([
      {
        item: "b",
        oldIndex: 1,
        newIndex: 0,
        isMoved: true,
      },
      {
        item: "a",
        oldIndex: 0,
        newIndex: 1,
        isMoved: true,
      },
      {
        item: "c",
        oldIndex: 2,
        newIndex: 2,
        isMoved: false,
      },
    ])
  })

  it("should move item backward from end", () => {
    const items = ["a", "b", "c"]
    const input = {
      oldIndex: 2,
      newIndex: 0,
    }
    const result = processArrayItemMove(items, input)
    expect(result).toEqual([
      {
        item: "c",
        oldIndex: 2,
        newIndex: 0,
        isMoved: true,
      },
      {
        item: "a",
        oldIndex: 0,
        newIndex: 1,
        isMoved: true,
      },
      {
        item: "b",
        oldIndex: 1,
        newIndex: 2,
        isMoved: true,
      },
    ])
  })
})
