import { subArrays } from "../../utils"

export const processArrayDifferences = <TItem, TInputItem, TId>({
  elements,
  elementIdSelector,
  desiredItems,
  desiredItemSelector,
}: {
  elements: TItem[]
  elementIdSelector: (item: TItem) => TId
  desiredItems: TInputItem[]
  desiredItemSelector: (item: TInputItem) => TId
}) => {
  const currentElementsIds = elements.map(elementIdSelector)
  const desiredItemsIds = desiredItems.map(desiredItemSelector)

  const missingElementIds = subArrays(
    desiredItemsIds,
    currentElementsIds,
    (a, b) => a === b
  )
  const missingElements = desiredItems.filter((x) =>
    missingElementIds.includes(desiredItemSelector(x))
  )

  const exceedingElementIds = subArrays(
    currentElementsIds,
    desiredItemsIds,
    (a, b) => a === b
  )
  const exceedingElements = elements.filter((x) =>
    exceedingElementIds.includes(elementIdSelector(x))
  )

  const correspondingElementIds = subArrays(
    currentElementsIds,
    exceedingElementIds,
    (a, b) => a === b
  )

  const correspondingElements = correspondingElementIds.map((id) => ({
    current: elements.find((x) => elementIdSelector(x) === id) as TItem,
    desired: desiredItems.find(
      (x) => desiredItemSelector(x) === id
    ) as TInputItem,
  }))

  return {
    missingElements,
    exceedingElements,
    correspondingElements,
  }
}

export const processArrayItemMove = <TItem>(
  items: TItem[],
  input: {
    oldIndex: number
    newIndex: number
  }
) => {
  const { oldIndex, newIndex } = input

  const item = items[oldIndex]
  const newItems = [...items]
  newItems.splice(oldIndex, 1)
  newItems.splice(newIndex, 0, item)

  return newItems.map((x, i) => ({
    item: x,
    newIndex: i,
    oldIndex: items.indexOf(x),
    isMoved: i !== items.indexOf(x),
  }))
}
