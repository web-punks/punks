import { PortableTextBlock } from "@portabletext/types"

export type RichText = PortableTextBlock[]

export type Optional<T> = T | undefined
