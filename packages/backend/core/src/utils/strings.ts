export const trimStart = (str: string, value: string) =>
  str.startsWith(value) ? str.substring(value.length) : str

export const trimEnd = (str: string, value: string) =>
  str.endsWith(value) ? str.substring(0, str.length - value.length) : str

export const trim = (str: string, value: string) =>
  trimStart(trimEnd(str, value), value)

export const camelToSnakeCase = (str: string) =>
  trimStart(
    str.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`),
    "_"
  )

export const camelToKebabCase = (str: string) =>
  trimStart(
    str.replace(/[A-Z]/g, (letter) => `-${letter.toLowerCase()}`),
    "-"
  )

export const toCamelCase = (str: string) =>
  `${str[0].toLowerCase()}${str.slice(1)}`

export const toTitleCase = (str: string) =>
  `${str[0].toUpperCase()}${str.slice(1)}`

export const ensureTailingSlash = (value: string) =>
  value?.endsWith("/") ? value : value + "/"

export const ensureStartSlash = (value: string) =>
  value?.startsWith("/") ? value : "/" + value

export const multipleSplit = (str: string, separators: string[]): string[] => {
  const escapeRegExp = (separator: string): string => {
    return separator.replace(/[.*+?^${}()|[\]\\]/g, "\\$&") // $& means the whole matched string
  }
  const regexPattern = separators
    .map((separator) => escapeRegExp(separator))
    .join("|")
  return str.split(new RegExp(regexPattern))
}

export const truncateString = (
  str: string,
  maxLength: number,
  filler = "..."
) => (str.length > maxLength ? str.substring(0, maxLength) + filler : str)
