import { existsSync, mkdirSync } from "fs"
import path from "path"

export const joinPath = (...paths: string[]) => path.join(...paths)
export const splitPath = (filePath: string) => filePath.split(path.delimiter)

export const ensureDirectory = (path: string) => {
  if (!existsSync(path)) {
    mkdirSync(path, { recursive: true })
  }
}

export const getDirectoryPath = (filePath: string) => path.dirname(filePath)

export const createDayPath = (d: Date = new Date(), separator = "/") =>
  `${d.getFullYear()}${separator}${(d.getMonth() + 1)
    .toString()
    .padStart(2, "0")}${separator}${d.getDate().toString().padStart(2, "0")}`
