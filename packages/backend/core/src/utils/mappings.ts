export const mapOrThrow = <T>({
  mapper,
  throw: throwFn,
}: {
  mapper: () => T | undefined
  throw: () => void
}): T => {
  const output = mapper()
  if (!output) {
    return throwFn() as T
  }
  return output
}
