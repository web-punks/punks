const DEFAULT_DELIMITER = ";"

const splitRows = (data: string | string[] | Buffer): string[] => {
  if (typeof data === "string") {
    return data.split("\n")
  }

  if (data instanceof Buffer) {
    const text = data.toString("utf-8")
    return splitRows(text)
  }

  return data
}

export const csvParse = (
  data: string | string[] | Buffer,
  separator = DEFAULT_DELIMITER
): Record<string, any>[] => {
  const rows: string[] = splitRows(data)
  const records: Record<string, any>[] = []

  if (rows.length === 0) {
    return records
  }

  const headers: string[] = rows[0].split(separator)

  for (let i = 1; i < rows.length; i++) {
    const values: string[] = rows[i].split(separator)
    const record: Record<string, any> = {}

    for (let j = 0; j < headers.length; j++) {
      const header = headers[j]
      const value = values[j]
      record[header] = value
    }

    records.push(record)
  }

  return records
}

export interface CsvColumnDefinition<T> {
  name: string
  value: (item: T) => any
}

const cleanValue = (value: string) =>
  value.replace(/,/g, " ").replace(/\n/g, " ").replace(/\t/g, " ")

const formatRow = (values: any[], delimiter: string) =>
  values.map((x) => cleanValue(x?.toString() ?? "")).join(delimiter)

const toCsvRow = <T>(
  item: T,
  columns: CsvColumnDefinition<T>[],
  delimiter: string
) =>
  formatRow(
    columns.map((x) => x.value(item)),
    delimiter
  )

export type CsvBuildOptions = {
  delimiter?: string
}

export const csvBuild = <T>(
  data: T[],
  columns: CsvColumnDefinition<T>[],
  options?: CsvBuildOptions
) => {
  const delimiter = options?.delimiter ?? DEFAULT_DELIMITER
  const header = formatRow(
    columns.map((x) => x.name),
    delimiter
  )
  const rows = data.map((x) => toCsvRow(x, columns, delimiter))
  return [header, ...rows].join("\n")
}
