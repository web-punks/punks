export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

export const mapAsync = async <T, R>(
  array: T[],
  callback: (value: T, index: number, array: T[]) => Promise<R>
): Promise<R[]> => {
  const result = []
  for (let i = 0; i < array.length; i++) {
    result.push(await callback(array[i], i, array))
  }
  return result
}
