export const range = (start: number, end: number): number[] => {
  return new Array(end - start + 1).fill(undefined).map((x, i) => i + start)
}

export const indexes = (startIndex: number, count: number) =>
  range(startIndex, startIndex + count - 1)

export interface Dict<TVal> {
  [key: string]: TVal
}

export const toDict = <TVal>(
  array: TVal[],
  keySelector: (value: TVal) => string
): Dict<TVal> => {
  const data: Dict<TVal> = {}
  for (const item of array) {
    data[keySelector(item)] = item
  }
  return data
}

export const toItemsDict = <TItem, TVal>(
  array: TItem[],
  keySelector: (value: TItem) => string,
  elementSelector: (value: TItem) => TVal
): Dict<TVal> => {
  const data: Dict<TVal> = {}
  for (const item of array) {
    data[keySelector(item)] = elementSelector(item)
  }
  return data
}

export const toArrayDict = <TVal>(
  array: TVal[],
  keySelector: (value: TVal) => string
): Dict<TVal[]> => {
  const data: Dict<TVal[]> = {}
  for (const item of array) {
    const key = keySelector(item)
    if (!data[key]) {
      data[key] = []
    }
    data[key].push(item)
  }
  return data
}

export const toMap = <TKey, TVal>(
  array: TVal[],
  key: (val: TVal) => TKey
): Map<TKey, TVal> => {
  const map = new Map<TKey, TVal>()
  array.forEach((x) => map.set(key(x), x))
  return map
}

export const toItemsMap = <TItem, TKey, TVal>(
  array: TItem[],
  key: (val: TItem) => TKey,
  element: (val: TItem) => TVal
): Map<TKey, TVal> => {
  const map = new Map<TKey, TVal>()
  array.forEach((x) => map.set(key(x), element(x)))
  return map
}

export const last = <T>(arr: T[]): T => arr[arr.length - 1]
export const first = <T>(arr: T[]): T => arr[0]

const compareFields = <T>(a: T, b: T, selector: (value: T) => any): number => {
  if (selector(a) < selector(b)) {
    return -1
  }
  if (selector(a) > selector(b)) {
    return 1
  }
  return 0
}

type ArrayComparer<T> = (a: T, b: T) => number

export const byField = <T>(selector: (value: T) => any): ArrayComparer<T> => {
  return function (a: T, b: T) {
    return compareFields(a, b, selector)
  }
}

export const byFieldDesc = <T>(
  selector: (value: T) => any
): ArrayComparer<T> => {
  return function (a: T, b: T) {
    return -compareFields(a, b, selector)
  }
}

export const sort = <T>(array: T[], ...comparers: ArrayComparer<T>[]) =>
  array.sort((a, b) => {
    for (const comparer of comparers) {
      const comparison = comparer(a, b)
      if (comparison !== 0) {
        return comparison
      }
    }

    return 0
  })

export const notUndefined = <T>(x: T | undefined): x is T => {
  return x !== undefined
}

export const notNull = <T>(x: T | null): x is T => {
  return x !== null
}

export const selectMany = <TArr, TVal>(
  array: TArr[],
  selector: (value: TArr) => TVal[]
): TVal[] => {
  return (
    array?.reduce(function (a, b) {
      return a.concat(selector(b))
    }, [] as TVal[]) ?? []
  )
}

export const flatten = <T>(array: T[][]): T[] => selectMany(array, (x) => x)

export const distinct = <T>(values: T[]) => {
  return values?.filter((n, i) => values.indexOf(n) === i) ?? []
}

export const distinctElements = <T>(
  values: T[],
  comparer: (value: T) => any
) => {
  return (
    values?.filter(
      (other, i, arr) =>
        arr.findIndex((t) => comparer(t) === comparer(other)) === i
    ) ?? []
  )
}

export const jsonDistinct = <T>(values: T[]) => {
  return distinctElements(values, (x) => JSON.stringify(x))
}

export const iterate = <T>(
  values: T[],
  action: (item: T, next?: T, prev?: T) => void
) => {
  values?.forEach((value, index) => {
    action(value, values?.[index + 1], values?.[index - 1])
  })
}

export const groupBy = <TKey, TValue>(
  values: TValue[],
  keySelector: (value: TValue) => TKey
): Map<TKey, TValue[]> => {
  const map = new Map<TKey, TValue[]>()
  values?.forEach((value) => {
    const key = keySelector(value)
    const items = map.get(key) || []
    items.push(value)
    map.set(key, items)
  })
  return map
}

export const subArrays = <T>(
  array1: T[],
  array2: T[],
  eqFn: (a: T, b: T) => boolean
) => {
  return array1.filter((a1) => !array2.some((a2) => eqFn(a1, a2)))
}
