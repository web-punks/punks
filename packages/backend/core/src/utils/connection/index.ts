const testConnection = async (url: string) => {
  try {
    const controller = new AbortController()
    const timeoutId = setTimeout(() => controller.abort(), 30 * 1000)

    const response = await fetch(url, { signal: controller.signal })

    clearTimeout(timeoutId)

    if (response.ok) {
      return {
        status: "success",
      }
    } else {
      return {
        status: "error",
        message: `HTTP error: ${response.status}`,
      }
    }
  } catch (e: any) {
    return {
      status: "error",
      message: e.name === "AbortError" ? "Request timed out" : e.message,
    }
  }
}

const DEFAULT_HEALTH_CHECK_URLS = [
  "https://www.google.com",
  "https://www.cloudflare.com",
  "https://www.microsoft.com",
]

export const testInternetConnection = async (options?: {
  healthCheckUrls?: string[]
}) => {
  const websitesToCheck = options?.healthCheckUrls ?? DEFAULT_HEALTH_CHECK_URLS

  const results = await Promise.all(websitesToCheck.map(testConnection))

  const allSuccess = results.every((result) => result.status === "success")

  if (allSuccess) {
    return {
      status: "success",
    }
  } else {
    return {
      status: "error",
      details: results,
    }
  }
}
