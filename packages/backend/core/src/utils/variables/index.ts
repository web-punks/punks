export const maskVariable = (value: string | undefined, unmaskedLength = 4) =>
  value
    ? value.substring(0, unmaskedLength).concat(
        Array.from(value.substring(unmaskedLength))
          .map(() => "*")
          .join("")
      )
    : ""
