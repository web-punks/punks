import { build, parse } from "node-xlsx"
import { addDays, format } from "date-fns"

export interface ExcelColumnDefinition<T> {
  value: (row: T) => any
  header: string
  headerSize?: number
}

export type ExcelSheetDefinition<T> = {
  sheetName: string
  data: T[]
  columns: ExcelColumnDefinition<T>[]
}

const buildRow = <T>(record: T, sheet: ExcelSheetDefinition<T>) => {
  return sheet.columns.map((x) => x.value(record))
}

export const excelBuild = <T>(
  ...sheets: ExcelSheetDefinition<T>[]
): ArrayBuffer => {
  const sheetContents = []
  for (const sheet of sheets) {
    sheetContents.push({
      name: sheet.sheetName.substring(0, 31), // Excel sheet name limit
      data: [
        sheet.columns.map((x) => x.header),
        ...sheet.data.map((d) => Array.from(buildRow(d, sheet))),
      ],
      options: {
        "!cols": sheet.columns.map((x) => ({ wch: x.headerSize ?? 12 })),
      },
    })
  }
  return build(sheetContents)
}

export const parseExcelRaw = (file: string | ArrayBuffer) => parse(file)

export enum ExcelKeyTransform {
  Lower = "lower",
  Upper = "upper",
}

export type ParsedExcelSheet = ReturnType<typeof parseExcelRaw>[number]
export type ExcelParseOptions = {
  keysTransform?: ExcelKeyTransform
  sheetIndex?: number
  dateColumns?: string[]
  sheetFilter?: (sheet: ParsedExcelSheet) => boolean
}

const trim = (rowValue: any) => {
  if (typeof rowValue === "string") {
    return rowValue.trim()
  }
  return rowValue
}

const isValidRow = (row: any) => {
  return row.filter((x: any) => trim(x)).length > 0
}

const normalizeKey = (
  colName: string,
  colIndex: number,
  keysTransform?: ExcelKeyTransform
) => {
  if (!colName) {
    return `Col${colIndex}`
  }

  colName = colName.replace(/ /g, "")
  if (!keysTransform) {
    return colName
  }

  if (keysTransform === ExcelKeyTransform.Lower) {
    return colName.toLowerCase()
  }

  if (keysTransform === ExcelKeyTransform.Upper) {
    return colName.toUpperCase()
  }

  return colName
}

const excelSerialToDate = (serial: number) => {
  const excelStartDate = new Date(1900, 0, 1)
  const daysSinceStart = serial - 2
  const date = addDays(excelStartDate, daysSinceStart)
  return format(date, "yyyy-MM-dd")
}

const aggregateRow = (
  header: string[],
  row: any[],
  options?: ExcelParseOptions
) => {
  const record: Record<PropertyKey, any> = {}
  const dateColumns = options?.dateColumns?.map((x, i) =>
    normalizeKey(x, i, options?.keysTransform)
  )
  row.forEach((value, index) => {
    const key = normalizeKey(header[index], index, options?.keysTransform)
    if (dateColumns?.includes(key) && typeof value === "number" && value > 59) {
      record[key] = excelSerialToDate(value)
    } else {
      record[key] = value
    }
  })
  return record
}

export const excelParse = (
  file: string | ArrayBuffer,
  options?: ExcelParseOptions
) => {
  const sheets = parseExcelRaw(file).reduce<any[]>((acc, sheet) => {
    if (options?.sheetFilter && !options.sheetFilter(sheet)) return acc

    const [header, ...rows] = sheet.data.filter((row) => isValidRow(row))
    acc.push(rows.map((row) => aggregateRow(header as string[], row, options)))

    return acc
  }, [])
  return sheets.length === 1 ? sheets[0] : sheets.flat()
}
