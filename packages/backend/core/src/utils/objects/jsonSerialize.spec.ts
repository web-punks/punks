import { jsonSerialize } from "."

describe("jsonSerialize", () => {
  it("should serialize undefined", () => {
    expect(jsonSerialize(undefined)).toBe(undefined)
  })

  it("should strip binary data", () => {
    expect(
      jsonSerialize(
        {
          foo: Buffer.from("bar"),
          bar: "bar",
        },
        { stripBinaryData: true }
      )
    ).toBe(
      JSON.stringify({
        foo: "<binary data (3 bytes)>",
        bar: "bar",
      })
    )
  })

  it("should strip binary data prettified", () => {
    expect(
      jsonSerialize(
        {
          foo: Buffer.from("bar"),
          bar: "bar",
        },
        { stripBinaryData: true, prettify: true }
      )
    ).toBe(
      JSON.stringify(
        {
          foo: "<binary data (3 bytes)>",
          bar: "bar",
        },
        null,
        2
      )
    )
  })

  it("should serialize nested data and strip binary data prettified", () => {
    expect(
      jsonSerialize(
        {
          root: {
            foo: Buffer.from("bar"),
            bar: "bar",
          },
          other: 24,
        },
        { stripBinaryData: true, prettify: true }
      )
    ).toBe(
      JSON.stringify(
        {
          root: {
            foo: "<binary data (3 bytes)>",
            bar: "bar",
          },
          other: 24,
        },
        null,
        2
      )
    )
  })
})
