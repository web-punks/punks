import { mergeDeep } from "."

describe("mergeDeep", () => {
  it("should merge two objects", () => {
    const a = {
      profile: {
        name: "John",
      },
    }
    const b = {
      profile: {
        surname: "Doe",
      },
      foo: "bar",
    }
    const c = mergeDeep(a, b)
    expect(c).toEqual({
      profile: {
        name: "John",
        surname: "Doe",
      },
      foo: "bar",
    })
  })
})
