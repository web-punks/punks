import { ReplaceUndefinedWithNull } from "./types"

export const removeUndefinedProps = <T extends Record<string, any>>(
  obj: T,
  options: {
    recursive?: boolean
  } = { recursive: false }
): T => {
  const { recursive } = options
  const result: any = {}

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      const value = obj[key]

      if (value !== undefined) {
        if (recursive && typeof value === "object" && !Array.isArray(value)) {
          result[key] = removeUndefinedProps(value, { recursive: true })
        } else {
          result[key] = value
        }
      }
    }
  }

  return result as T
}

export const isNullOrUndefined = (value: any) => {
  return value === null || value === undefined
}

export const buildObject = <T>(
  ...props: {
    key: string
    value: any
  }[]
) => {
  const out = {} as any
  for (const prop of props) {
    const propPath = prop.key.split(".")
    const propName = propPath[0]
    const nestedPropPath = propPath.slice(1).join(".")

    if (nestedPropPath) {
      out[propName] = {
        ...(out[propName] ?? {}),
        ...buildObject({
          key: nestedPropPath,
          value: prop.value,
        }),
      }
    } else {
      out[propName] = prop.value
    }
  }
  return out as T
}

export const mergeDeep = <T>(a: any, b: any) => {
  const c = { ...(a ?? {}) }
  for (const key in b ?? {}) {
    if (b.hasOwnProperty(key)) {
      const value = b[key]
      if (typeof value === "object" && !Array.isArray(value)) {
        c[key] = mergeDeep(c[key] ?? {}, value)
      } else {
        c[key] = value
      }
    }
  }
  return c as T
}

const isSerializable = (obj: any) => {
  try {
    JSON.stringify(obj)
    return true
  } catch (e) {
    return false
  }
}

export const jsonSerialize = (
  obj: any,
  options?: {
    removeNonSerializableObjects?: boolean
    removeCircularReferences?: boolean
    stripBinaryData?: boolean
    maxStringsLength?: number
    prettify?: boolean
  }
) => {
  const {
    removeNonSerializableObjects = true,
    removeCircularReferences = true,
    stripBinaryData = true,
    prettify = false,
    maxStringsLength,
  } = options ?? {}
  const replacer = (key: string, value: any) => {
    // remove non serializable objects
    if (removeNonSerializableObjects && !isSerializable(value)) {
      return `<non serializable property ${typeof value}>`
    }

    // remove circular references
    const seen = new WeakSet()
    if (
      removeCircularReferences &&
      typeof value === "object" &&
      value !== null
    ) {
      if (seen.has(value)) {
        return
      }
      seen.add(value)
    }

    // trim long strings
    if (maxStringsLength && typeof value === "string") {
      return value.length > maxStringsLength
        ? `${value.substring(0, maxStringsLength)}...`
        : value
    }

    // strip binary data
    if (stripBinaryData) {
      if (value instanceof Buffer) {
        return `<binary data (${value.length} bytes)>`
      }
      if (
        typeof value === "object" &&
        value?.type === "Buffer" &&
        Array.isArray(value?.data)
      ) {
        return `<binary data (${value.data.length} bytes)>`
      }
    }
    return value
  }

  const spaces = prettify ? 2 : undefined
  return JSON.stringify(obj, replacer, spaces)
}

export function replaceUndefinedWithNull<T>(
  obj: T
): ReplaceUndefinedWithNull<T> {
  if (obj === undefined) {
    return null as ReplaceUndefinedWithNull<T>
  }
  if (typeof obj !== "object" || obj === null) {
    return obj as ReplaceUndefinedWithNull<T>
  }
  if (Array.isArray(obj)) {
    return obj.map(replaceUndefinedWithNull) as ReplaceUndefinedWithNull<T>
  }
  const newObj: any = {}
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      newObj[key] = replaceUndefinedWithNull(obj[key])
    }
  }
  return newObj as ReplaceUndefinedWithNull<T>
}
