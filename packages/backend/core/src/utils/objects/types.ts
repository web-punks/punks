export type ReplaceUndefinedWithNull<T> = T extends undefined
  ? null
  : T extends (infer U)[]
  ? ReplaceUndefinedWithNull<U>[]
  : T extends object
  ? { [K in keyof T]: ReplaceUndefinedWithNull<T[K]> }
  : T
