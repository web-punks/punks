import { buildObject, removeUndefinedProps } from "."

interface MyObject {
  name: string
  age?: number
  address?: string
  nested?: {
    prop1?: string
    prop2?: number
  }
}

describe("removeUndefinedProps", () => {
  it("should remove undefined properties from the top-level object", () => {
    const input: MyObject = {
      name: "John",
      age: 30,
      address: undefined,
    }

    const output = removeUndefinedProps(input)

    expect(output).toEqual({
      name: "John",
      age: 30,
    })
  })

  it("should recursively remove undefined properties from nested objects", () => {
    const input: MyObject = {
      name: "John",
      age: 30,
      address: undefined,
      nested: {
        prop1: "Nested property 1",
        prop2: undefined,
      },
    }

    const output = removeUndefinedProps(input, { recursive: true })

    expect(output).toEqual({
      name: "John",
      age: 30,
      nested: {
        prop1: "Nested property 1",
      },
    })
  })

  it("should not modify the input object", () => {
    const input: MyObject = {
      name: "John",
      age: 30,
      address: undefined,
    }

    const output = removeUndefinedProps(input)

    expect(output).not.toBe(input) // Ensure that the output is a new object
    expect(input).toEqual({
      name: "John",
      age: 30,
      address: undefined,
    }) // Ensure that the input object remains unchanged
  })

  it("should handle an empty object", () => {
    const input: MyObject = {} as any

    const output = removeUndefinedProps(input)

    expect(output).toEqual({})
  })

  it("should handle an object with all properties set to undefined", () => {
    const input: MyObject = {
      name: undefined,
      age: undefined,
      address: undefined,
    } as any

    const output = removeUndefinedProps(input)

    expect(output).toEqual({})
  })

  it("should build simple object", () => {
    const obj = buildObject(
      {
        key: "name",
        value: "John",
      },
      {
        key: "surname",
        value: "John Doe",
      },
      {
        key: "profile.age",
        value: 30,
      },
      {
        key: "profile.level.name",
        value: "pro",
      }
    )
    expect(obj).toEqual({
      name: "John",
      surname: "John Doe",
      profile: {
        age: 30,
        level: {
          name: "pro",
        },
      },
    })
  })
})
