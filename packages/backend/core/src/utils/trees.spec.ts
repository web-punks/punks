import { buildTree } from "./trees"

describe("trees", () => {
  it("should create a tree", async () => {
    const items = [
      {
        id: "1",
        parentId: undefined,
        name: "A",
      },
      {
        id: "2",
        parentId: "1",
        name: "B",
      },
      {
        id: "3",
        parentId: "2",
        name: "C",
      },
      {
        id: "4",
        parentId: "2",
        name: "D",
      },
      {
        id: "5",
        parentId: "1",
        name: "E",
      },
      {
        id: "6",
        parentId: "5",
        name: "F",
      },
    ]

    const tree = await buildTree(items, {
      idSelector: (x) => x.id,
      parentIdSelector: (x) => x.parentId,
      additionalPropsBuilder: async (x, a, c) => ({
        name: x.name,
        path: [...a.map((x) => x.name), x.name].join("/"),
      }),
    })

    expect(tree).toEqual({
      root: [
        {
          name: "A",
          path: "A",
          value: { id: "1", name: "A" },
          id: "1",
          children: [
            {
              name: "B",
              path: "A/B",
              value: { id: "2", parentId: "1", name: "B" },
              id: "2",
              children: [
                {
                  name: "C",
                  path: "A/B/C",
                  value: { id: "3", parentId: "2", name: "C" },
                  id: "3",
                  children: [],
                },
                {
                  name: "D",
                  path: "A/B/D",
                  value: { id: "4", parentId: "2", name: "D" },
                  id: "4",
                  children: [],
                },
              ],
            },
            {
              name: "E",
              path: "A/E",
              value: { id: "5", parentId: "1", name: "E" },
              id: "5",
              children: [
                {
                  name: "F",
                  path: "A/E/F",
                  value: { id: "6", parentId: "5", name: "F" },
                  id: "6",
                  children: [],
                },
              ],
            },
          ],
        },
      ],
    })
  })
})
