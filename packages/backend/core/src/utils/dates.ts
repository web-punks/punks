export type TimeUnit =
  | "years"
  | "months"
  | "days"
  | "hours"
  | "minutes"
  | "seconds"
  | "milliseconds"

export const addTime = (
  date: Date,
  offset: {
    value: number
    unit: TimeUnit
  }
) => {
  const { value, unit } = offset
  const newDate = new Date(date)
  switch (unit) {
    case "years":
      newDate.setFullYear(date.getFullYear() + value)
      break
    case "months":
      newDate.setMonth(date.getMonth() + value)
      break
    case "days":
      newDate.setDate(date.getDate() + value)
      break
    case "hours":
      newDate.setHours(date.getHours() + value)
      break
    case "minutes":
      newDate.setMinutes(date.getMinutes() + value)
      break
    case "seconds":
      newDate.setSeconds(date.getSeconds() + value)
      break
    case "milliseconds":
      newDate.setMilliseconds(date.getMilliseconds() + value)
      break
  }
  return newDate
}

export const subtractTime = (
  date: Date,
  offset: {
    value: number
    unit: TimeUnit
  }
) => {
  return addTime(date, {
    value: -offset.value,
    unit: offset.unit,
  })
}

export function floorDateToSecond(date: Date): Date {
  const flooredDate = new Date(date)
  flooredDate.setMilliseconds(0)
  return flooredDate
}
