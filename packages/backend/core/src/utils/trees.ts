import { ITree, ITreeNode } from "../models"
import { groupBy, toMap } from "./array"

export type TreeNodeBuilder<
  TKey,
  TRecord,
  TTreeNode extends ITreeNode<TKey, TRecord>
> = {
  idSelector: (record: TRecord) => TKey
  parentIdSelector: (record: TRecord) => TKey | undefined
  additionalPropsBuilder?: (
    record: TRecord,
    ancestors: TRecord[],
    children: TRecord[]
  ) => Promise<Omit<TTreeNode, "id" | "children" | "value">>
}

const buildNode = async <
  TKey,
  TRecord,
  TTreeNode extends ITreeNode<TKey, TRecord>
>(
  record: TRecord,
  {
    ancestors,
    nodesDict,
    childrenDict,
    nodeBuilder,
  }: {
    ancestors: TRecord[]
    nodesDict: Map<TKey, TRecord>
    childrenDict: Map<TKey, TRecord[]>
    nodeBuilder: TreeNodeBuilder<TKey, TRecord, TTreeNode>
  }
): Promise<TTreeNode> => {
  const children = childrenDict.get(nodeBuilder.idSelector(record)) || []
  const childrenNodes: TTreeNode[] = await Promise.all(
    children.map((x) =>
      buildNode<TKey, TRecord, TTreeNode>(x, {
        ancestors: [...ancestors, record],
        nodesDict,
        childrenDict,
        nodeBuilder,
      })
    )
  )
  const additionalProps = await nodeBuilder.additionalPropsBuilder?.(
    record,
    ancestors,
    children
  )
  return {
    ...((additionalProps ?? {}) as TTreeNode),
    value: record,
    id: nodeBuilder.idSelector(record),
    children: childrenNodes,
  }
}

export const buildTree = async <
  TKey,
  TRecord,
  TTreeNode extends ITreeNode<TKey, TRecord>,
  TTree extends ITree<TKey, TRecord>
>(
  data: TRecord[],
  nodeBuilder: TreeNodeBuilder<TKey, TRecord, TTreeNode>
): Promise<TTree> => {
  const nodesDict = toMap(data, (x) => nodeBuilder.idSelector(x))
  const childrenDict = groupBy<TKey, TRecord>(
    data.filter((x) => nodeBuilder.parentIdSelector(x)),
    (x) => nodeBuilder.parentIdSelector(x) as TKey
  )
  const roots = data.filter((x) => !nodeBuilder.parentIdSelector(x))
  const rootNodes: TTreeNode[] = await Promise.all(
    roots.map((x) =>
      buildNode<TKey, TRecord, TTreeNode>(x, {
        ancestors: [],
        nodesDict,
        childrenDict,
        nodeBuilder,
      })
    )
  )

  return {
    root: rootNodes,
  } as any
}
