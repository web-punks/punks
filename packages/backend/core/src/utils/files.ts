import { readdirSync, statSync } from "fs"
import { join } from "path"

const readFilesRecursively = async (
  dir: string,
  recursive: boolean
): Promise<string[]> => {
  const files: string[] = []
  const entries: string[] = readdirSync(dir)

  for (const entry of entries) {
    const entryPath = join(dir, entry)
    const stats = statSync(entryPath)

    if (stats.isFile()) {
      files.push(entryPath)
    } else if (recursive && stats.isDirectory()) {
      const nestedFiles = await readFilesRecursively(entryPath, recursive)
      files.push(...nestedFiles)
    }
  }

  return files
}

export const getDirectoryFilePaths = async (
  directoryPath: string,
  options?: {
    recursive?: boolean
  }
): Promise<string[]> => {
  const { recursive = false } = options || {}
  const filePaths: string[] = await readFilesRecursively(
    directoryPath,
    recursive
  )
  return filePaths
}
