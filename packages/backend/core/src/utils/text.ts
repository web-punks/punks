export const pluralize = (word: string) => {
  return word.endsWith("s") ? `${word}es` : `${word}s`
}
