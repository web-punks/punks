export interface ITreeNode<TKey, TValue> {
  id: TKey
  value: TValue
  children: ITreeNode<TKey, TValue>[]
}

export interface ITree<TKey, TValue> {
  root: ITreeNode<TKey, TValue>[]
}
