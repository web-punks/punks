import { IResolveServiceOptions, IServiceLocator } from "../abstractions/ioc"

// todo: implement a real IoC container with scopes and stuff
export class ServiceLocator implements IServiceLocator {
  private services: { [key: string]: any } = {}

  register<T>(name: string, service: T): void {
    this.services[name] = service
  }

  resolve<T>(name: string, options?: IResolveServiceOptions): T {
    const service = this.services[name]
    if (!service && !options?.optional) {
      throw new Error(`Service "${name}" not found in ServiceLocator`)
    }
    return service
  }
}
