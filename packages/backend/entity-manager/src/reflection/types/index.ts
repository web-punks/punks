export const getTypeName = (type: any): string => {
  if (typeof type === "string") {
    return type
  }

  if (typeof type === "number") {
    return type.toString()
  }

  if (typeof type === "symbol") {
    return type.toString()
  }

  if (typeof type === "function") {
    return type.name
  }

  return type.constructor.name
}
