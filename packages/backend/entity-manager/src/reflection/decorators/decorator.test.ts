import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { getInstanceDecoratorData } from "."

interface DecoratorProps {
  templateId: string
}

const TestDecorator = (templateId: string) =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, DecoratorProps>(Symbol.for("TEST"), {
      templateId,
    })
  )

@TestDecorator("test")
class TestClass {}

describe("Decorator", () => {
  it("should read decorator data from class instance", () => {
    const instance = new TestClass()
    const metadata = getInstanceDecoratorData(Symbol.for("TEST"), instance)
    expect(metadata).toEqual({ templateId: "test" })
  })
})
