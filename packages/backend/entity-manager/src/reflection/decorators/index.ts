import { Reflector } from "@nestjs/core"

export const getInstanceDecoratorData = <T>(
  metadataKey: any,
  instance: Object
) => {
  const reflector = new Reflector()
  return reflector.get(metadataKey, instance.constructor) as T
}
