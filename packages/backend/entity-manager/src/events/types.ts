export interface MessagingEmailSentPayload {
  from?: string
  to?: string[]
  cc?: string[]
  bcc?: string[]
  subject?: string
  body?: string
  payload?: any
  request?: any
}
