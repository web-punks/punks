export const PLATFORM_EVENT_NAMESPACE = "platform"

export const PlatformEvents = {
  Messaging: {
    EmailSent: `${PLATFORM_EVENT_NAMESPACE}:messaging.emailSent`,
  },
}
