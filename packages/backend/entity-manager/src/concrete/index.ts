import {
  IEntitiesCountAction,
  IEntitiesDeleteAction,
  IEntitiesExportAction,
  IEntitiesImportAction,
  IEntitiesParseAction,
  IEntitiesSampleDownloadAction,
  IEntitiesSearchAction,
  IEntityActions,
  IEntityCreateAction,
  IEntityDeleteAction,
  IEntityExistsAction,
  IEntityGetAction,
  IEntityUpdateAction,
  IEntityUpsertAction,
  IEntityVersionsSearchAction,
} from "../abstractions/actions"
import {
  IEntitiesDeleteCommand,
  IEntitiesDeleteParameters,
  IEntitiesExportCommand,
  IEntitiesImportCommand,
  IEntitiesSampleDownloadCommand,
  IEntityUpsertByCommand,
  IEntityCreateCommand,
  IEntityDeleteCommand,
  IEntityUpdateCommand,
  IEntityUpsertCommand,
  IEntityVersionCommand,
  IEntitiesParseCommand,
} from "../abstractions/commands"
import { SortingType } from "../abstractions/common"
import { IEntityManager } from "../abstractions/manager"
import {
  IEntitiesCountQuery,
  IEntitiesFindQuery,
  IEntitiesSearchQuery,
  IEntityExistsQuery,
  IEntityGetQuery,
  IEntityVersionsSearchQuery,
} from "../abstractions/queries"
import { IEntitySearchParameters } from "../abstractions/searchParameters"
import { IEntityFacets } from "../abstractions/searchResults"
import { EntityServiceLocator } from "../providers/services"

export class EntityManager<
  TEntity,
  TEntityId,
  TEntityCreateData,
  TEntityUpdateData,
  TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets
> implements
    IEntityManager<
      TEntity,
      TEntityId,
      TEntityCreateData,
      TEntityUpdateData,
      TEntitiesDeleteParameters,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets
    >
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  get exists(): IEntityExistsQuery<
    TEntity,
    NonNullable<TEntitySearchParameters["filters"]>
  > {
    return this.services.resolveExistsQuery<
      NonNullable<TEntitySearchParameters["filters"]>
    >()
  }

  get count(): IEntitiesCountQuery<
    TEntity,
    NonNullable<TEntitySearchParameters["filters"]>
  > {
    return this.services.resolveCountQuery<
      NonNullable<TEntitySearchParameters["filters"]>
    >()
  }

  get deleteItems(): IEntitiesDeleteCommand<
    TEntity,
    TEntitiesDeleteParameters,
    TSorting
  > {
    return this.services.resolveDeleteItemsCommand<
      TEntitiesDeleteParameters,
      TSorting
    >()
  }

  get get(): IEntityGetQuery<TEntity, TEntityId, TEntitySearchParameters> {
    return this.services.resolveGetQuery()
  }

  get search(): IEntitiesSearchQuery<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor,
    TFacets
  > {
    return this.services.resolveSearchQuery()
  }

  get find(): IEntitiesFindQuery<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor
  > {
    return this.services.resolveFindQuery()
  }

  get version(): IEntityVersionCommand<TEntity, TEntityId> {
    return this.services.resolveVersionCommand()
  }

  get create(): IEntityCreateCommand<TEntity, TEntityId, TEntityCreateData> {
    return this.services.resolveCreateCommand()
  }

  get update(): IEntityUpdateCommand<TEntity, TEntityId, TEntityUpdateData> {
    return this.services.resolveUpdateCommand()
  }

  get upsert(): IEntityUpsertCommand<TEntity, TEntityId, TEntityUpdateData> {
    return this.services.resolveUpsertCommand()
  }

  get upsertBy(): IEntityUpsertByCommand<
    TEntity,
    NonNullable<TEntitySearchParameters["filters"]>
  > {
    return this.services.resolveUpsertByCommand<
      NonNullable<TEntitySearchParameters["filters"]>
    >()
  }

  get delete(): IEntityDeleteCommand<TEntity, TEntityId> {
    return this.services.resolveDeleteCommand()
  }

  get export(): IEntitiesExportCommand<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor
  > {
    return this.services.resolveExportCommand()
  }

  get import(): IEntitiesImportCommand<TEntity> {
    return this.services.resolveImportCommand()
  }

  get parse(): IEntitiesParseCommand<TEntity> {
    return this.services.resolveParseCommand()
  }

  get sampleDownload(): IEntitiesSampleDownloadCommand<TEntity> {
    return this.services.resolveSampleDownloadCommand()
  }

  get versions(): IEntityVersionsSearchQuery<TEntity, TCursor> {
    return this.services.resolveVersionsSearchQuery<TCursor>()
  }
}

export class EntityActions<
  TEntity,
  TEntityId,
  TEntityCreateDto,
  TEntityUpdateDto,
  TEntityDto,
  TEntityListItemDto,
  TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets
> implements
    IEntityActions<
      TEntity,
      TEntityId,
      TEntityCreateDto,
      TEntityUpdateDto,
      TEntityDto,
      TEntityListItemDto,
      TEntitiesDeleteParameters,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets
    >
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}
  get exists(): IEntityExistsAction<
    TEntity,
    NonNullable<TEntitySearchParameters["filters"]>
  > {
    return this.services.resolveExistsAction<
      NonNullable<TEntitySearchParameters["filters"]>
    >()
  }

  get count(): IEntitiesCountAction<
    TEntity,
    NonNullable<TEntitySearchParameters["filters"]>
  > {
    return this.services.resolveCountAction<
      NonNullable<TEntitySearchParameters["filters"]>
    >()
  }

  get deleteItems(): IEntitiesDeleteAction<
    TEntity,
    TEntitiesDeleteParameters,
    TSorting
  > {
    return this.services.resolveDeleteItemsAction<
      TEntitiesDeleteParameters,
      TSorting
    >()
  }

  get get(): IEntityGetAction<
    TEntity,
    TEntityId,
    TEntityDto,
    TEntitySearchParameters
  > {
    return this.services.resolveGetAction<
      TEntityId,
      TEntityDto,
      TEntitySearchParameters
    >()
  }

  get search(): IEntitiesSearchAction<
    TEntity,
    TEntitySearchParameters,
    TEntityListItemDto,
    TSorting,
    TCursor,
    TFacets
  > {
    return this.services.resolveSearchAction<
      TEntitySearchParameters,
      TEntityListItemDto,
      TSorting,
      TCursor,
      TFacets
    >()
  }

  get create(): IEntityCreateAction<TEntity, TEntityCreateDto, TEntityDto> {
    return this.services.resolveCreateAction<TEntityCreateDto, TEntityDto>()
  }

  get update(): IEntityUpdateAction<
    TEntity,
    TEntityId,
    TEntityUpdateDto,
    TEntityDto
  > {
    return this.services.resolveUpdateAction<TEntityUpdateDto, TEntityDto>()
  }

  get upsert(): IEntityUpsertAction<
    TEntity,
    TEntityId,
    TEntityUpdateDto,
    TEntityDto
  > {
    return this.services.resolveUpsertAction<TEntityUpdateDto, TEntityDto>()
  }

  get delete(): IEntityDeleteAction<TEntity, TEntityId> {
    return this.services.resolveDeleteAction()
  }

  get export(): IEntitiesExportAction<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor
  > {
    return this.services.resolveExportAction()
  }

  get import(): IEntitiesImportAction<TEntity> {
    return this.services.resolveImportAction()
  }

  get parse(): IEntitiesParseAction<TEntity> {
    return this.services.resolveParseAction()
  }

  get sampleDownload(): IEntitiesSampleDownloadAction<TEntity> {
    return this.services.resolveSampleDownloadAction()
  }

  get versions(): IEntityVersionsSearchAction<
    TEntity,
    TEntityListItemDto,
    TCursor
  > {
    return this.services.resolveVersionsSearchAction<
      TEntityListItemDto,
      TCursor
    >()
  }
}
