import {
  PipelineErrorType,
  PipelineOperationError,
  PipelineOperationResult,
  PipelineStepErrorType,
  PipelineStepResult,
} from "../../../types"

export const toPipelineOperationError = (
  error: Error
): PipelineOperationError => ({
  message: error.message,
  stackTrace: error.stack,
  exception: error,
})

export const getStepOutput = <TStepInput, TStepOutput>(
  result: PipelineStepResult<TStepInput, TStepOutput>
) => {
  return result.type === "success" ? result.output : undefined
}

export const getOperationOutput = <TStepInput, TStepOutput>(
  result: PipelineOperationResult<TStepInput, TStepOutput>
) => {
  return result.type === "success" ? result.output : undefined
}

export const mapPipelineErrorType = (
  stepError: PipelineStepErrorType
): PipelineErrorType => {
  switch (stepError) {
    case PipelineStepErrorType.FailedPrecondition:
      return PipelineErrorType.FailedPrecondition
    case PipelineStepErrorType.OperationError:
      return PipelineErrorType.OperationError
    case PipelineStepErrorType.RollbackError:
      return PipelineErrorType.RollbackError
    case PipelineStepErrorType.GenericError:
      return PipelineErrorType.GenericError
  }
}
