import {
  IPipelineInstance,
  IPipelinesController,
} from "../../../abstractions/pipelines"
import { PipelineDefinition } from "../../../types"
import { PipelineInstance } from "../instance"

export class PipelineController implements IPipelinesController {
  async createInstance<TInput, TOutput, TContext>(
    definition: PipelineDefinition<TInput, TOutput, TContext>,
    input: TInput,
    context: TContext
  ): Promise<IPipelineInstance<TInput, TOutput, TContext>> {
    return new PipelineInstance<TInput, TOutput, TContext>(
      definition,
      input,
      context
    )
  }
}
