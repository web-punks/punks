import {
  PipelineOperationResultType,
  PipelineOperationRollbackResultType,
  PipelineStepResultType,
} from "../../../types"

export const isOperationSuccessStatus = (type: PipelineOperationResultType) => {
  return ["success", "skipped"].includes(type)
}

export const isOperationRollbackErrorStatus = (
  type: PipelineOperationRollbackResultType
) => ["error"].includes(type)

export const isStepSuccessStatus = (type: PipelineStepResultType) =>
  ["success"].includes(type)
