import { Log } from "@punks/backend-core"
import { IPipelineInstance } from "../../../abstractions/pipelines"
import {
  PipelineDefinition,
  PipelineOperationResult,
  PipelineOperationRollbackResult,
  PipelineResult,
  PipelineStatus,
  PipelineStep,
  PipelineStepOperation,
  PipelineStepResult,
  PipelineStepRollbackResult,
  RollbackOperationDefinition,
  PipelineCurrentStepState,
  PipelineCompletedStepState,
  PipelineStepErrorType,
} from "../../../types"
import {
  getOperationOutput,
  getStepOutput,
  mapPipelineErrorType,
  toPipelineOperationError,
} from "../converters"
import {
  isOperationRollbackErrorStatus,
  isOperationSuccessStatus,
} from "../utils"

export class PipelineInstance<TPipelineInput, TPipelineOutput, TContext>
  implements IPipelineInstance<TPipelineInput, TPipelineOutput, TContext>
{
  private status = PipelineStatus.Initializing
  private readonly logger = Log.getLogger(PipelineInstance.name)

  constructor(
    private readonly definition: PipelineDefinition<
      TPipelineInput,
      TPipelineOutput,
      TContext
    >,
    private readonly input: TPipelineInput,
    private readonly context: TContext
  ) {}

  async execute(): Promise<PipelineResult<TPipelineInput, TPipelineOutput>> {
    this.status = PipelineStatus.Running

    const executedSteps: PipelineStep<
      unknown,
      unknown,
      TPipelineInput,
      TContext
    >[] = []
    const stepResults: PipelineStepResult<unknown, unknown>[] = []
    let state = this.buildInitialStepState()

    for (const [i, step] of this.definition.steps.entries()) {
      executedSteps.push(step)
      const stepResult = await this.executeStep(
        step,
        i > 0 ? getStepOutput(stepResults[i - 1]) : this.input,
        state
      )
      stepResults.push(stepResult)
      state = this.buildNextStepState(state, stepResult)

      if (stepResult.type !== "success") {
        this.status = PipelineStatus.RollbackInProgress
        await this.rollbackSteps(executedSteps, stepResults, state)

        return {
          type: "error",
          errorType: mapPipelineErrorType(stepResult.errorType),
          input: this.input,
          stepResults,
        }
      }
    }

    this.status = PipelineStatus.Completed
    return {
      type: "success",
      input: this.input,
      output: getStepOutput(
        stepResults[stepResults.length - 1]
      ) as TPipelineOutput,
      stepResults,
    }
  }

  private buildInitialStepState(): PipelineCurrentStepState<
    TPipelineInput,
    TContext,
    unknown
  > {
    return {
      reference: {
        index: 0,
        key: this.definition.steps[0].operations[0].key,
        name: this.definition.steps[0].operations[0].name,
      },
      stepInput: this.input,
      context: this.context,
      pipelineInput: this.input,
    }
  }

  private buildNextStepState(
    state: PipelineCurrentStepState<TPipelineInput, TContext, unknown>,
    stepResult: PipelineStepResult<unknown, unknown>
  ): PipelineCurrentStepState<TPipelineInput, TContext, unknown> {
    return {
      reference: {
        index: state.reference.index + 1,
        name: this.definition.steps[state.reference.index + 1]?.operations[0]
          .name,
        key: this.definition.steps[state.reference.index + 1]?.operations[0]
          .key,
      },
      stepInput: getStepOutput(stepResult),
      context: this.context,
      pipelineInput: this.input,
      previousStep: {
        reference: {
          index: state.reference.index,
          key: state.reference.key,
          name: state.reference.name,
        },
        stepInput: state.stepInput,
        stepOutput: getStepOutput(stepResult),
        context: this.context,
        pipelineInput: this.input,
        previousStep: state.previousStep,
      },
    }
  }

  private async rollbackSteps(
    executedSteps: PipelineStep<unknown, unknown, TPipelineInput, TContext>[],
    stepsResults: PipelineStepResult<unknown, unknown>[],
    state: PipelineCurrentStepState<TPipelineInput, TContext, unknown>
  ) {
    const rollbackSteps = executedSteps
      .map((x, i) => ({
        step: x,
        result: stepsResults[i],
      }))
      .reverse()

    let rollbackState: PipelineCompletedStepState<
      TPipelineInput,
      TContext,
      unknown,
      unknown
    > = state
    for (const step of rollbackSteps) {
      await this.rollbackStep(step.step, step.result, rollbackState)
      rollbackState = state.previousStep as PipelineCompletedStepState<
        TPipelineInput,
        TContext,
        unknown,
        unknown
      >
    }
  }

  private async rollbackStep(
    step: PipelineStep<unknown, unknown, TPipelineInput, TContext>,
    stepResults: PipelineStepResult<unknown, unknown>,
    state: PipelineCurrentStepState<TPipelineInput, TContext, unknown>
  ): Promise<PipelineStepRollbackResult> {
    const completedOperations = stepResults.operationResults.map((x, i) => ({
      result: x,
      operation: step.operations[i],
    }))

    const rollbackOperations = completedOperations.map((x) =>
      this.rollbackOperations(
        x.operation,
        x.result.input,
        getOperationOutput(x.result),
        state
      )
    )
    const rollbackResults = await Promise.all(rollbackOperations)
    const isRollbackError = rollbackResults.some((x) =>
      isOperationRollbackErrorStatus(x.type)
    )
    if (isRollbackError) {
      return {
        type: "error",
      }
    }

    return {
      type: "success",
    }
  }

  private async validateStepPrecondition<TStepInput, TContext>(
    step: PipelineStep<unknown, unknown, TPipelineInput, TContext>,
    input: TStepInput,
    state: PipelineCurrentStepState<TPipelineInput, TContext, unknown>
  ) {
    const preconditionOperations = step.operations
      .filter((x) => x.operation.precondition)
      .map((x) => x.operation.precondition!(input, state))

    const preconditionResults = await Promise.all(preconditionOperations)

    return {
      hasValidPrecondition: preconditionResults.every((x) => x),
    }
  }

  private async executeStep<TStepInput, TStepOutput, TContext>(
    step: PipelineStep<unknown, unknown, TPipelineInput, TContext>,
    input: TStepInput,
    state: PipelineCurrentStepState<TPipelineInput, TContext, unknown>
  ): Promise<PipelineStepResult<TStepInput, TStepOutput>> {
    const preconditionResult = await this.validateStepPrecondition(
      step,
      input,
      state
    )

    if (!preconditionResult.hasValidPrecondition) {
      this.logger.error(
        `[VALIDATION ERROR] | Step ${step.name} failed for invalid precondition`
      )
      return {
        operationResults: [],
        type: "error",
        errorType: PipelineStepErrorType.FailedPrecondition,
        input,
      }
    }

    const stepOperations = step.operations.map((x) =>
      this.executeOperation(x, input, state)
    )
    const operationResults = await Promise.all(stepOperations)
    const isSuccess = operationResults.every((x) =>
      isOperationSuccessStatus(x.type)
    )
    if (isSuccess) {
      const output = step.outputsReducer
        ? step.outputsReducer(
            operationResults.map((x) => getOperationOutput(x))
          )
        : operationResults.map((x) => getOperationOutput(x))

      return {
        type: "success",
        operationResults,
        input,
        output: output as TStepOutput,
      }
    }

    return {
      type: "error",
      errorType: PipelineStepErrorType.OperationError,
      operationResults,
      input,
    }
  }

  private async executeOperation<TOperationInput, TOperationOutput, TContext>(
    operation: PipelineStepOperation<
      TOperationInput,
      TOperationOutput,
      unknown,
      TPipelineInput,
      TContext
    >,
    input: TOperationInput,
    state: PipelineCurrentStepState<TPipelineInput, TContext, unknown>
  ): Promise<PipelineOperationResult<TOperationInput, TOperationOutput>> {
    try {
      const shouldSkip = await this.shouldSkipOperation(operation, input, state)
      if (shouldSkip) {
        this.logger.info(
          `Pipeline operation skipped -> operation:${operation.name}`,
          {
            input,
          }
        )
        return {
          name: operation.name,
          key: operation.key,
          type: "skipped",
          input,
        }
      }

      const operationResult = await operation.operation.action(input, state)
      return {
        name: operation.name,
        key: operation.key,
        type: "success",
        input,
        output: operationResult,
      }
    } catch (error) {
      this.logger.exception(
        `[OPERATION ERROR] -> operation:${operation.name}`,
        error as Error,
        {
          input,
        }
      )
      return {
        name: operation.name,
        key: operation.key,
        type: "error",
        input,
        error: toPipelineOperationError(error as Error),
      }
    }
  }

  private async shouldSkipOperation<
    TOperationInput,
    TOperationOutput,
    TContext
  >(
    {
      operation,
    }: PipelineStepOperation<
      TOperationInput,
      TOperationOutput,
      unknown,
      TPipelineInput,
      TContext
    >,
    input: TOperationInput,
    state: PipelineCurrentStepState<TPipelineInput, TContext, unknown>
  ) {
    if (!operation.skipIf) {
      return false
    }

    return await operation.skipIf(input, state)
  }

  private async rollbackOperations<TOperationInput, TOperationOutput, TContext>(
    operation: PipelineStepOperation<
      TOperationInput,
      TOperationOutput,
      unknown,
      TPipelineInput,
      TContext
    >,
    operationInput: TOperationInput,
    operationOutput: TOperationOutput,
    state: PipelineCompletedStepState<
      TPipelineInput,
      TContext,
      unknown,
      unknown
    >
  ): Promise<PipelineOperationRollbackResult> {
    if (!operation.rollbackOperations?.length) {
      return {
        type: "skipped",
      }
    }

    for (const rollbackOperation of operation.rollbackOperations) {
      const rollbackResult = await this.rollbackOperation(
        rollbackOperation,
        operationInput,
        operationOutput,
        state
      )
      if (rollbackResult.type === "error") {
        return {
          type: "error",
          error: rollbackResult.error,
        }
      }
    }

    return {
      type: "success",
    }
  }

  private async rollbackOperation<TOperationInput, TOperationOutput, TContext>(
    rollbackOperation: RollbackOperationDefinition<
      TOperationInput,
      TOperationOutput,
      unknown,
      TPipelineInput,
      TContext
    >,
    operationInput: TOperationInput,
    operationOutput: TOperationOutput,
    state: PipelineCompletedStepState<
      TPipelineInput,
      TContext,
      unknown,
      unknown
    >
  ): Promise<PipelineOperationRollbackResult> {
    try {
      await rollbackOperation.action(operationInput, operationOutput, state)
      return {
        type: "success",
      }
    } catch (error) {
      this.logger.exception(
        `Pipeline operation rollback error -> rollbackOperation:${rollbackOperation.name}`,
        error as Error,
        {
          operationInput,
          operationOutput,
          state,
        }
      )
      return {
        type: "error",
        error: toPipelineOperationError(error as Error),
      }
    }
  }
}
