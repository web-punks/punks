import {
  IAuthenticationContext,
  IEntitiesQueryBuilder,
  IEntityFacets,
  IEntitySearchParameters,
  ISearchFilters,
  ISearchRequestPaging,
} from "../../abstractions"
import { SortingType } from "../../abstractions/common"
import { IEntitiesDeleteResult } from "../../abstractions/repository"
import {
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
} from "../../models"

export enum QueryBuilderOperation {
  Search = "search",
  Get = "get",
}

export abstract class QueryBuilderBase<
  TEntity,
  TEntityId,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets,
  TUserContext
> implements
    IEntitiesQueryBuilder<
      TEntity,
      TEntityId,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets,
      TUserContext
    >
{
  abstract get(
    id: TEntityId,
    context?: IAuthenticationContext<TUserContext> | undefined
  ): Promise<TEntity | undefined>

  abstract exists(
    filters: NonNullable<TEntitySearchParameters["filters"]>,
    context?: IAuthenticationContext<TUserContext> | undefined
  ): Promise<boolean>

  abstract count(
    filters: NonNullable<TEntitySearchParameters["filters"]>,
    context?: IAuthenticationContext<TUserContext> | undefined
  ): Promise<number>

  abstract delete(
    filters: NonNullable<TEntitySearchParameters["filters"]>,
    context?: IAuthenticationContext<TUserContext> | undefined
  ): Promise<IEntitiesDeleteResult>

  abstract search(
    request: TEntitySearchParameters,
    context?: IAuthenticationContext<TUserContext>
  ): Promise<
    IEntitiesSearchResults<
      TEntity,
      TEntitySearchParameters,
      TEntity,
      TSorting,
      TCursor,
      TFacets
    >
  >

  abstract find(
    request: {
      filters?: TEntitySearchParameters["filters"]
      sorting?: TEntitySearchParameters["sorting"]
    },
    context?: IAuthenticationContext<TUserContext>
  ): Promise<TEntity>

  protected getIndexBasedPagingResult({
    paging,
    totResults,
    currentPageResults,
  }: {
    paging?: ISearchRequestPaging<number>
    totResults: number
    currentPageResults: number
  }): IEntitiesSearchResultsPaging<number> | undefined {
    if (!paging) {
      return undefined
    }

    const cursor = paging.cursor ?? 0
    const hasMorePages =
      cursor * paging.pageSize + currentPageResults < totResults

    return {
      pageIndex: cursor,
      pageSize: paging.pageSize,
      totItems: totResults,
      totPageItems: currentPageResults,
      totPages: Math.ceil(totResults / paging.pageSize),
      currentPageCursor: cursor,
      prevPageCursor: cursor > 0 ? cursor - 1 : undefined,
      nextPageCursor: hasMorePages ? cursor + 1 : undefined,
    }
  }
}
