import { ISearchFilters } from "../abstractions"
import { IEntityExistsAction } from "../abstractions/actions"
import { EntityServiceLocator } from "../providers/services"

export class EntityExistsAction<TEntity, TEntityFilters extends ISearchFilters>
  implements IEntityExistsAction<TEntity, TEntityFilters>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {}

  public async execute(filters: TEntityFilters): Promise<boolean> {
    return await this.services.resolveExistsQuery().execute(filters)
  }
}
