import { ILogger, Log } from "@punks/backend-core"
import { IEntitiesExportAction } from "../abstractions/actions"
import { EntityServiceLocator } from "../providers/services"
import { IEntitySearchParameters } from "../abstractions/searchParameters"
import { SortingType } from "../abstractions/common"
import { EntitiesExportInput } from "../abstractions/export"

export class EntitiesExportAction<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor
> implements
    IEntitiesExportAction<TEntity, TEntitySearchParameters, TSorting, TCursor>
{
  private readonly logger: ILogger

  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {
    this.logger = Log.getLogger(`${services.getEntityName()} -> Export`)
  }

  public async execute<TPayload>(
    input: EntitiesExportInput<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TPayload
    >
  ) {
    this.logger.debug("Export action started", { input })

    const result = await this.services
      .resolveExportCommand()
      .execute<TPayload>(input)

    this.logger.debug("Export action completed", { input })

    return result
  }
}
