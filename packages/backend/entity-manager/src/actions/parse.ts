import { ILogger, Log } from "@punks/backend-core"
import { IEntitiesParseAction } from "../abstractions/actions"
import { EntityServiceLocator } from "../providers/services"
import { EntitiesParseInput } from "../abstractions/parse"

export class EntitiesParseAction<TEntity>
  implements IEntitiesParseAction<TEntity>
{
  private readonly logger: ILogger

  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {
    this.logger = Log.getLogger(`${services.getEntityName()} -> Import`)
  }

  public async execute<TPayload>(input: EntitiesParseInput<TPayload>) {
    this.logger.debug("Parse action started", { input })

    const result = await this.services.resolveParseCommand().execute(input)

    this.logger.debug("Parse action completed", { input })

    return result
  }
}
