import { ILogger, Log } from "@punks/backend-core"
import { EntityServiceLocator } from "../providers/services"
import { IEntitiesSampleDownloadAction } from "../abstractions/actions"
import {
  EntitiesSampleDownloadOptions,
  EntitiesSampleDownloadResult,
} from "../abstractions"

export class EntitiesSampleDownloadAction<TEntity>
  implements IEntitiesSampleDownloadAction<TEntity>
{
  private readonly logger: ILogger

  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {
    this.logger = Log.getLogger(`${services.getEntityName()} -> Import`)
  }

  async execute(
    input: EntitiesSampleDownloadOptions
  ): Promise<EntitiesSampleDownloadResult> {
    this.logger.debug("Sample download action started", { input })

    const result = await this.services
      .resolveSampleDownloadCommand()
      .execute(input)

    this.logger.debug("Sample download action completed", { input })

    return result
  }
}
