import { ILogger, Log } from "@punks/backend-core"
import { IEntitiesDeleteAction } from "../abstractions/actions"
import { IEntitiesDeleteParameters } from "../abstractions/commands"
import { EntityServiceLocator } from "../providers/services"

export class EntitiesDeleteAction<
  TEntity,
  TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
  TSorting
> implements
    IEntitiesDeleteAction<TEntity, TEntitiesDeleteParameters, TSorting>
{
  private readonly logger: ILogger

  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {
    this.logger = Log.getLogger(`${services.getEntityName()} -> DeleteItems`)
  }

  public async execute(params: TEntitiesDeleteParameters) {
    this.logger.debug("DeleteItems action started")

    await this.services.resolveDeleteItemsCommand().execute(params)

    this.logger.debug("DeleteItems action completed")
  }
}
