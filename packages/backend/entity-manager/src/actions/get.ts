import {
  IEntityGetAction,
  IEntityGetActionOptions,
} from "../abstractions/actions"
import { IEntitySearchParameters } from "../abstractions/searchParameters"
import { EntityServiceLocator } from "../providers/services"

export class EntityGetAction<
  TEntity,
  TEntityId,
  TEntityDto,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    unknown,
    unknown
  >
> implements
    IEntityGetAction<TEntity, TEntityId, TEntityDto, TEntitySearchParameters>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  public async execute(
    id: TEntityId,
    options?: IEntityGetActionOptions<TEntity, TEntitySearchParameters>
  ): Promise<TEntityDto | undefined> {
    const entity = await this.services.resolveGetQuery().execute(id, options)
    const converter = this.services.resolveConverter<
      TEntityDto,
      unknown,
      unknown,
      unknown
    >()
    return entity
      ? (await converter?.toEntityDto(entity)) ?? (entity as TEntityDto)
      : undefined
  }
}
