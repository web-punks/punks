import { mapAsync } from "@punks/backend-core"
import { IEntityFacets } from "../abstractions"
import {
  IEntitiesSearchAction,
  IEntitiesSearchActionOptions,
} from "../abstractions/actions"
import { SortingType } from "../abstractions/common"
import { IEntitySearchParameters } from "../abstractions/searchParameters"
import { IEntitiesSearchResults } from "../models"
import { EntityServiceLocator } from "../providers/services"

export class EntitiesSearchAction<
  TEntity,
  TEntityId,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TListItemDto,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets
> implements
    IEntitiesSearchAction<
      TEntity,
      TEntitySearchParameters,
      TListItemDto,
      TSorting,
      TCursor,
      TFacets
    >
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  public async execute(
    request: TEntitySearchParameters,
    options?: IEntitiesSearchActionOptions<
      TEntity,
      TEntitySearchParameters,
      TListItemDto
    >
  ): Promise<
    IEntitiesSearchResults<
      TEntity,
      TEntitySearchParameters,
      TListItemDto,
      TSorting,
      TCursor,
      TFacets
    >
  > {
    const searchQuery = this.services.resolveSearchQuery<
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets
    >()
    const results = await searchQuery.execute(request, options)

    const converter = this.services.resolveConverter<
      unknown,
      TListItemDto,
      unknown,
      unknown
    >()

    return {
      items: await mapAsync<TEntity, TListItemDto>(
        results.items,
        async (x) =>
          ((await (options?.converter?.(x) ?? converter?.toListItemDto(x))) ??
            x) as TListItemDto
      ),
      paging: results.paging,
      childrenMap: results.childrenMap,
      facets: results.facets,
      request: results.request,
    }
  }
}
