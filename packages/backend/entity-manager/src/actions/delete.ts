import { ILogger, Log } from "@punks/backend-core"
import { IEntityDeleteAction } from "../abstractions/actions"
import { EntityServiceLocator } from "../providers/services"

export class EntityDeleteAction<TEntity, TEntityId>
  implements IEntityDeleteAction<TEntity, TEntityId>
{
  private readonly logger: ILogger

  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {
    this.logger = Log.getLogger(`${services.getEntityName()} -> Delete`)
  }

  public async execute(id: TEntityId) {
    this.logger.debug("Delete action started", { id })

    await this.services.resolveDeleteCommand().execute(id)

    this.logger.debug("Delete action completed", { id })
  }
}
