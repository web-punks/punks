import { ILogger, Log } from "@punks/backend-core"
import { IEntityCreateAction } from "../abstractions/actions"
import { EntityServiceLocator } from "../providers/services"
import { DeepPartial } from "../types"

export class EntityCreateAction<
  TEntity,
  TEntityId,
  TEntityCreateDto,
  TEntityDto
> implements IEntityCreateAction<TEntity, TEntityCreateDto, TEntityDto>
{
  private readonly logger: ILogger

  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {
    this.logger = Log.getLogger(`${services.getEntityName()} -> Create`)
  }

  async execute(input: TEntityCreateDto): Promise<TEntityDto> {
    this.logger.debug("Create action started", { input })
    const converter = this.services.resolveConverter<
      TEntityDto,
      unknown,
      TEntityCreateDto,
      unknown
    >()

    const context = await this.getContext()
    const createInput =
      converter?.createDtoToEntity(input, context) ??
      (input as DeepPartial<TEntity>)
    const ref = await this.services
      .resolveCreateCommand<DeepPartial<TEntity>>()
      .execute(createInput)

    const entity = await this.services
      .resolveGetQuery<TEntityId, any>()
      .execute(ref.id)
    if (!entity) {
      throw new Error(`Newly created entity not found with id ${ref.id}`)
    }

    const result =
      (await converter?.toEntityDto(entity)) ?? (entity as TEntityDto)
    this.logger.debug("Create action completed", { input, result })
    return result
  }

  private async getContext() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    return await contextService?.getContext()
  }
}
