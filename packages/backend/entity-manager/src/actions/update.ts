import { ILogger, Log } from "@punks/backend-core"
import { IEntityUpdateAction } from "../abstractions/actions"
import { EntityServiceLocator } from "../providers/services"
import { DeepPartial } from "../types"

export class EntityUpdateAction<
  TEntity,
  TEntityId,
  TEntityUpdateData,
  TEntityDto
> implements
    IEntityUpdateAction<TEntity, TEntityId, TEntityUpdateData, TEntityDto>
{
  private readonly logger: ILogger

  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {
    this.logger = Log.getLogger(`${services.getEntityName()} -> Update`)
  }

  public async execute(
    id: TEntityId,
    input: TEntityUpdateData
  ): Promise<TEntityDto> {
    this.logger.debug("Update action started", { id, input })

    const converter = this.services.resolveConverter<
      TEntityDto,
      unknown,
      unknown,
      TEntityUpdateData
    >()

    const context = await this.getContext()
    const updateInput =
      converter?.updateDtoToEntity(input, context) ??
      (input as DeepPartial<TEntity>)

    await this.services
      .resolveUpdateCommand<DeepPartial<TEntity>>()
      .execute(id, updateInput)

    const entity = await this.services
      .resolveGetQuery<TEntityId, any>()
      .execute(id)
    if (!entity) {
      throw new Error(`Newly created entity not found with id ${id}`)
    }

    const result =
      (await converter?.toEntityDto(entity)) ?? (entity as TEntityDto)

    this.logger.debug("Update action started", { id, input, result })

    return result
  }

  private async getContext() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    return await contextService?.getContext()
  }
}
