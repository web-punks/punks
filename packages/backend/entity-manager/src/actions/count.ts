import { ISearchFilters } from "../abstractions"
import { IEntitiesCountAction } from "../abstractions/actions"
import { EntityServiceLocator } from "../providers/services"

export class EntitiesCountAction<TEntity, TEntityFilters extends ISearchFilters>
  implements IEntitiesCountAction<TEntity, TEntityFilters>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {}

  public async execute(filters: TEntityFilters): Promise<number> {
    return await this.services.resolveCountQuery().execute(filters)
  }
}
