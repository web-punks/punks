import { IEntityVersionsSearchAction } from "../abstractions/actions"
import {
  IEntityVersionsSearchRequest,
  IEntityVersionsSearchResponse,
} from "../models/versioning"
import { EntityServiceLocator } from "../providers/services"

export class EntityVersionsSearchAction<
  TEntity,
  TEntityId,
  TListItemDto,
  TCursor
> implements IEntityVersionsSearchAction<TEntity, TListItemDto, TCursor>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  async execute(
    request: IEntityVersionsSearchRequest<TCursor>
  ): Promise<IEntityVersionsSearchResponse<TListItemDto, TCursor>> {
    const results = await this.services
      .resolveVersionsSearchQuery<TCursor>()
      .execute(request.params)

    const converter = this.services.resolveConverter<
      unknown,
      TListItemDto,
      unknown,
      unknown
    >()
    return {
      request,
      paging: results.paging,
      items: results.items.map(
        (x) => (converter?.toListItemDto(x) ?? x) as TListItemDto
      ),
    }
  }
}
