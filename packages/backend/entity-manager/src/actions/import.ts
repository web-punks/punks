import { ILogger, Log } from "@punks/backend-core"
import { IEntitiesImportAction } from "../abstractions/actions"
import { EntityServiceLocator } from "../providers/services"
import { EntitiesImportInput } from "../abstractions/import"

export class EntitiesImportAction<TEntity>
  implements IEntitiesImportAction<TEntity>
{
  private readonly logger: ILogger

  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {
    this.logger = Log.getLogger(`${services.getEntityName()} -> Import`)
  }

  public async execute(input: EntitiesImportInput<unknown>) {
    this.logger.debug("Import action started", { input })

    const result = await this.services.resolveImportCommand().execute(input)

    this.logger.debug("Import action completed", { input })

    return result
  }
}
