import { IEntitiesSampleDownloadCommand } from "../abstractions/commands"
import { EntityServiceLocator } from "../providers/services"
import {
  EntitiesImportExportSettings,
  EntitiesSampleDownloadOptions,
  EntitiesSampleDownloadResult,
  IBucketProvider,
} from "../abstractions"
import { EntityExportFile } from "../abstractions/serializer"
import { createDayPath } from "./export"

export class EntitiesSampleDownloadCommand<TEntity>
  implements IEntitiesSampleDownloadCommand<TEntity>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>,
    private readonly settings: EntitiesImportExportSettings
  ) {}

  async execute(
    input: EntitiesSampleDownloadOptions
  ): Promise<EntitiesSampleDownloadResult> {
    const sample = await this.services
      .resolveSerializer()
      .createSample(input.format)

    const downloadUrl = await this.uploadSampleFile(sample)

    return {
      file: {
        content: sample.content,
        contentType: sample.contentType,
        name: sample.fileName,
      },
      downloadUrl,
    }
  }

  private async uploadSampleFile(file: EntityExportFile) {
    await this.bucket.fileUpload({
      bucket: this.settings.exportBucket.bucket,
      filePath: this.buildAbsoluteBucketPath(file.fileName),
      content: file.content,
      contentType: file.contentType,
    })
    return await this.bucket.filePublicUrlCreate({
      bucket: this.settings.exportBucket.bucket,
      expirationMinutes:
        this.settings.exportBucket.publicLinksExpirationMinutes,
      filePath: this.buildAbsoluteBucketPath(file.fileName),
    })
  }

  private buildAbsoluteBucketPath(relativePath: string) {
    return `${
      this.settings.exportBucket.rootFolderPath ?? ""
    }/samples/${createDayPath(new Date())}/${relativePath}`
  }

  private get bucket(): IBucketProvider {
    return this.services.getRootServices().resolveDefaultBucketProvider()
  }
}
