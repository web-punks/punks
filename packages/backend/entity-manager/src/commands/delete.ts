import { isNullOrUndefined } from "@punks/backend-core"
import { IEntityDeleteCommand } from "../abstractions/commands"
import {
  EntityNotFoundException,
  EntityOperationType,
  EntityOperationUnauthorizedException,
  MissingEntityIdError,
} from "../abstractions/errors"
import { EntityServiceLocator } from "../providers/services"
import { EntityVersionOperation } from "../abstractions"

export class EntityDeleteCommand<TEntity, TEntityId>
  implements IEntityDeleteCommand<TEntity, TEntityId>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  async execute(id: TEntityId) {
    if (isNullOrUndefined(id)) {
      throw new MissingEntityIdError()
    }

    await this.authorize(id)
    await this.versionEntity(id)
    await this.services.resolveRepository().delete(id)
    await this.services.resolveEventsManager().processEntityDeletedEvent(id)
  }

  private async authorize(id: TEntityId) {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const entity = await this.services.resolveRepository().get(id)
    if (entity == null) {
      throw new EntityNotFoundException<TEntityId>({
        id,
        type: this.services.getEntityName(),
      })
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    const context = await contextService?.getContext()
    if (!context) {
      return
    }

    const authorizationResult = await authorization.canDelete(entity, context)
    if (!authorizationResult.isAuthorized)
      throw new EntityOperationUnauthorizedException<TEntity>(
        EntityOperationType.Delete,
        this.services.getEntityName(),
        entity
      )
  }

  private async versionEntity(id: TEntityId) {
    if (!this.isVersioningEnabled()) {
      return
    }

    await this.services.resolveVersionCommand().execute({
      id,
      operation: EntityVersionOperation.Delete,
    })
  }

  private isVersioningEnabled() {
    return (
      this.services.resolveEntityConfiguration().versioning?.enabled ?? false
    )
  }
}
