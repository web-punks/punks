import {
  EntitiesImportInput,
  EntitiesImportResult,
  InvalidEntityHandling,
} from "../abstractions/import"
import { IEntitiesImportCommand } from "../abstractions/commands"
import { EntityServiceLocator } from "../providers/services"
import { EntitiesImportExportSettings, IBucketProvider } from "../abstractions"
import { createDayPath } from "./export"
import {
  EntityExportFile,
  EntitySerializationFormat,
} from "../abstractions/serializer"

export class EntitiesImportCommand<TEntity>
  implements IEntitiesImportCommand<TEntity>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>,
    private readonly settings: EntitiesImportExportSettings
  ) {}

  async execute<TPayload>(
    input: EntitiesImportInput<TPayload>
  ): Promise<EntitiesImportResult> {
    await this.uploadImportFile({
      content: input.file.content,
      contentType: input.file.contentType,
      fileName: input.file.fileName,
    })

    const importEntities = await this.parseImportFile<TPayload>(
      input.file.content,
      input.format,
      input?.payload
    )

    const validEntities = importEntities.filter((x) => x.status.isValid)
    const invalidEntities = importEntities.filter((x) => !x.status.isValid)

    if (
      invalidEntities.length > 0 &&
      input.options?.invalidEntitiesHandling !== InvalidEntityHandling.Skip
    ) {
      throw new Error(
        `Cannot import file due to invalid entities: ${invalidEntities
          .map((x) => x.id)
          .join(", ")}`
      )
    }

    await this.services.resolveSerializer().import(
      validEntities.map((x) => x.item),
      input?.payload
    )

    return {
      statistics: {
        importedCount: validEntities.length,
        skippedCount: invalidEntities.length,
        createdCount: 0,
        unchangedCount: 0,
        updatedCount: 0,
      },
    }
  }

  private async parseImportFile<TPayload>(
    content: Buffer,
    format: EntitySerializationFormat,
    payload?: TPayload
  ) {
    return await this.services
      .resolveSerializer()
      .parse<TPayload>(content, format, payload)
  }

  private async uploadImportFile(file: EntityExportFile) {
    await this.bucket.fileUpload({
      bucket: this.settings.exportBucket.bucket,
      filePath: this.buildAbsoluteBucketPath(file.fileName),
      content: file.content,
      contentType: file.contentType,
    })
  }

  private buildAbsoluteBucketPath(relativePath: string) {
    return `${
      this.settings.exportBucket.rootFolderPath ?? ""
    }/imports/${createDayPath(new Date())}/${relativePath}`
  }

  private get bucket(): IBucketProvider {
    return this.services.getRootServices().resolveDefaultBucketProvider()
  }
}
