import { IEntitiesParseCommand } from "../abstractions/commands"
import { EntityServiceLocator } from "../providers/services"
import { EntitiesImportExportSettings, IBucketProvider } from "../abstractions"
import { createDayPath } from "./export"
import {
  EntityExportFile,
  EntitySerializationFormat,
} from "../abstractions/serializer"
import { EntitiesParseInput, EntitiesParseResult } from "../abstractions/parse"

export class EntitiesParseCommand<TEntity>
  implements IEntitiesParseCommand<TEntity>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>,
    private readonly settings: EntitiesImportExportSettings
  ) {}

  async execute<TPayload>(
    input: EntitiesParseInput<TPayload>
  ): Promise<EntitiesParseResult<unknown>> {
    await this.uploadImportFile({
      content: input.file.content,
      contentType: input.file.contentType,
      fileName: input.file.fileName,
    })

    const entries = await this.parseImportFile<TPayload>(
      input.file.content,
      input.format,
      input?.payload
    )
    return {
      entries,
    }
  }

  private async parseImportFile<TPayload>(
    content: Buffer,
    format: EntitySerializationFormat,
    payload?: TPayload
  ) {
    return await this.services
      .resolveSerializer()
      .parse<TPayload>(content, format, payload)
  }

  private async uploadImportFile(file: EntityExportFile) {
    await this.bucket.fileUpload({
      bucket: this.settings.exportBucket.bucket,
      filePath: this.buildAbsoluteBucketPath(file.fileName),
      content: file.content,
      contentType: file.contentType,
    })
  }

  private buildAbsoluteBucketPath(relativePath: string) {
    return `${
      this.settings.exportBucket.rootFolderPath ?? ""
    }/parsers/${createDayPath(new Date())}/${relativePath}`
  }

  private get bucket(): IBucketProvider {
    return this.services.getRootServices().resolveDefaultBucketProvider()
  }
}
