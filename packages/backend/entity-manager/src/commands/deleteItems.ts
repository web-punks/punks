import { IAuthenticationContext } from "../abstractions"
import {
  IEntitiesDeleteCommand,
  IEntitiesDeleteParameters,
  IEntitiesDeleteResult,
} from "../abstractions/commands"
import {
  EntityOperationType,
  EntityOperationUnauthorizedException,
} from "../abstractions/errors"
import { EntityServiceLocator } from "../providers/services"

export class EntitiesDeleteCommand<
  TEntity,
  TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
  TSorting,
  TUserContext
> implements
    IEntitiesDeleteCommand<TEntity, TEntitiesDeleteParameters, TSorting>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {}

  async execute(
    params: TEntitiesDeleteParameters
  ): Promise<IEntitiesDeleteResult> {
    await this.authorize()
    const context = await this.getContext()

    const deleteResult = await this.services
      .resolveQueryBuilder<unknown, any, any, any, any, TUserContext>()
      .delete(params.filters, context as IAuthenticationContext<TUserContext>)

    // todo: version deleted entities
    // todo: add entities deleted event

    return deleteResult
  }

  private async getContext() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return undefined
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    return await contextService?.getContext()
  }

  private async authorize() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    const context = await contextService?.getContext()
    if (!context) {
      return
    }

    const authorizationResult = await authorization.canDeleteItems(context)
    if (!authorizationResult.isAuthorized)
      throw new EntityOperationUnauthorizedException<TEntity>(
        EntityOperationType.Delete,
        this.services.getEntityName()
      )
  }
}
