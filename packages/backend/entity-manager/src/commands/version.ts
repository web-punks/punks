import {
  EntityVersionCommandInput,
  IEntityVersionCommand,
} from "../abstractions/commands"
import { EntityServiceLocator } from "../providers/services"

export class EntityVersionCommand<TEntity, TEntityId>
  implements IEntityVersionCommand<TEntity, TEntityId>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  public async execute({
    id,
    operation,
    entity,
  }: EntityVersionCommandInput<TEntity, TEntityId>) {
    const versioningProvider = this.services.resolveVersioningProvider()
    if (!versioningProvider) {
      throw new Error("Versioning provider is not configured")
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    const context = await contextService?.getContext()
    const snapshotService = this.services.resolveSnapshotService()
    const snapshot = (await snapshotService?.getSnapshot(id)) ?? entity

    await versioningProvider.createVersion({
      operationType: operation,
      entityId: id,
      entityType: this.services.getEntityName(),
      data: snapshot,
      modifiedByUserId: context?.userId,
    })
  }
}
