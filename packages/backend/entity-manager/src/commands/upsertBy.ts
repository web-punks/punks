import { ISearchFilters, MultipleEntitiesFoundException } from "../abstractions"
import {
  IEntityUpsertByCommand,
  IEntityUpsertByParameters,
  IEntityUpsertByResult,
} from "../abstractions/commands"
import { EntityServiceLocator } from "../providers/services"

export class EntitiesUpsertByCommand<
  TEntity,
  TEntityFilters extends ISearchFilters
> implements IEntityUpsertByCommand<TEntity, TEntityFilters>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {}

  async execute(
    params: IEntityUpsertByParameters<TEntity, TEntityFilters>
  ): Promise<IEntityUpsertByResult> {
    const { items } = await this.services.resolveSearchQuery().execute({
      filters: params.filter,
    })
    if (items.length === 0) {
      const createdItem = await this.services
        .resolveCreateCommand()
        .execute(params.data)
      // todo: parametrize id field
      const id = (createdItem as any).id
      return {
        id,
      }
    }

    if (items.length > 1) {
      throw new MultipleEntitiesFoundException(params, items)
    }

    // todo: parametrize id field
    const id = (items[0] as any).id
    await this.services.resolveUpdateCommand().execute(id, params.data)
    return {
      id,
    }
  }
}
