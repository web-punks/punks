import { EntityVersionOperation } from "../abstractions"
import { IEntityUpsertCommand } from "../abstractions/commands"
import {
  EntityOperationType,
  EntityOperationUnauthorizedException,
} from "../abstractions/errors"
import { EntityServiceLocator } from "../providers/services"
import { DeepPartial } from "../types"

export class EntityUpsertCommand<TEntity, TEntityId, TEntityUpdateData>
  implements IEntityUpsertCommand<TEntity, TEntityId, TEntityUpdateData>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  async execute(id: TEntityId | undefined, data: TEntityUpdateData) {
    const entity = await this.adaptEntity(id, data)
    await this.authorize(id, entity)

    if (id) {
      const updatedEntity = await this.services
        .resolveRepository()
        .upsert(id, entity)
      await this.versionEntity(id, updatedEntity)

      await this.services
        .resolveEventsManager()
        .processEntityUpdatedEvent(updatedEntity)
      return {
        id,
      }
    }

    const createdItem = await this.services.resolveRepository().create(entity)

    // todo: parametrize id field
    const newId = (createdItem as any).id
    await this.versionEntity(newId, createdItem)

    await this.services
      .resolveEventsManager()
      .processEntityCreatedEvent(createdItem)

    return {
      id: newId,
    }
  }

  private async adaptEntity(
    id: TEntityId | undefined,
    input: TEntityUpdateData
  ) {
    const context = await this.getContext()
    const adapter = this.services.resolveAdapter<unknown, TEntityUpdateData>()
    if (!adapter) {
      return input as DeepPartial<TEntity>
    }
    return id
      ? adapter.updateDataToEntity(input, context!)
      : adapter.createDataToEntity(input, context!)
  }

  private async authorize(
    id: TEntityId | undefined,
    entity: DeepPartial<TEntity>
  ) {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const context = await this.getContext()
    if (!context) {
      return
    }

    const currentEntity = id
      ? await this.services.resolveRepository().get(id)
      : undefined
    if (currentEntity) {
      const updateResult = await authorization.canUpdate(currentEntity, context)
      if (!updateResult.isAuthorized)
        throw new EntityOperationUnauthorizedException<TEntity>(
          EntityOperationType.Update,
          this.services.getEntityName(),
          currentEntity
        )
      return
    }

    const authorizationResult = await authorization.canCreate(entity, context)
    if (!authorizationResult.isAuthorized)
      throw new EntityOperationUnauthorizedException<TEntity>(
        EntityOperationType.Create,
        this.services.getEntityName(),
        entity
      )
  }

  private async getContext() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    return await contextService?.getContext()
  }

  private async versionEntity(id: TEntityId, entity: TEntity) {
    if (!this.isVersioningEnabled()) {
      return
    }

    await this.services.resolveVersionCommand().execute({
      id,
      entity,
      operation: EntityVersionOperation.Upsert,
    })
  }

  private isVersioningEnabled() {
    return (
      this.services.resolveEntityConfiguration().versioning?.enabled ?? false
    )
  }
}
