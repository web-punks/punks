import {
  EntitiesExportInput,
  EntitiesExportResult,
  EntitiesImportExportSettings,
  EntitySerializationFormat,
  IBucketProvider,
} from "../abstractions"
import { IEntitiesExportCommand } from "../abstractions/commands"
import { SortingType } from "../abstractions/common"
import { IEntitySearchParameters } from "../abstractions/searchParameters"
import { EntityServiceLocator } from "../providers/services"
import { EntityExportFile } from "../abstractions/serializer"

export const createDayPath = (d: Date) =>
  `${d.getFullYear()}/${(d.getMonth() + 1).toString().padStart(2, "0")}/${d
    .getDate()
    .toString()
    .padStart(2, "0")}`

export class EntitiesExportCommand<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor
> implements
    IEntitiesExportCommand<TEntity, TEntitySearchParameters, TSorting, TCursor>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>,
    private readonly settings: EntitiesImportExportSettings
  ) {}

  async execute<TPayload>(
    input: EntitiesExportInput<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TPayload
    >
  ): Promise<EntitiesExportResult> {
    const sheetItems = await this.getExportSheetItems<TPayload>(
      input.filter,
      input.payload
    )

    const outputFile = await this.buildExportFile<TPayload>(
      sheetItems,
      input.options.format,
      input.payload
    )

    const downloadUrl = await this.uploadExportFile(outputFile)

    return {
      downloadUrl,
      file: {
        content: outputFile.content,
        contentType: outputFile.contentType,
        name: outputFile.fileName,
      },
    }
  }

  private async buildExportFile<TPayload>(
    data: unknown[],
    format: EntitySerializationFormat,
    payload?: TPayload
  ): Promise<EntityExportFile> {
    return await this.services
      .resolveSerializer()
      .serialize(data, format, payload)
  }

  private async uploadExportFile(file: EntityExportFile) {
    await this.bucket.fileUpload({
      bucket: this.settings.exportBucket.bucket,
      filePath: this.buildAbsoluteBucketPath(file.fileName),
      content: file.content,
      contentType: file.contentType,
    })
    return await this.bucket.filePublicUrlCreate({
      bucket: this.settings.exportBucket.bucket,
      expirationMinutes:
        this.settings.exportBucket.publicLinksExpirationMinutes,
      filePath: this.buildAbsoluteBucketPath(file.fileName),
    })
  }

  private buildAbsoluteBucketPath(relativePath: string) {
    return `${
      this.settings.exportBucket.rootFolderPath ?? ""
    }/exports/${createDayPath(new Date())}/${relativePath}`
  }

  private async getExportSheetItems<TPayload>(
    filters?: TEntitySearchParameters,
    payload?: TPayload
  ) {
    return await this.services.resolveSerializer().export(filters, payload)
  }

  private get bucket(): IBucketProvider {
    return this.services.getRootServices().resolveDefaultBucketProvider()
  }
}
