import { EntityVersionOperation } from "../abstractions"
import { IEntityUpdateCommand } from "../abstractions/commands"
import {
  EntityNotFoundException,
  EntityOperationType,
  EntityOperationUnauthorizedException,
} from "../abstractions/errors"
import { EntityServiceLocator } from "../providers/services"

export class EntityUpdateCommand<TEntity, TEntityId, TEntityUpdateData>
  implements IEntityUpdateCommand<TEntity, TEntityId, TEntityUpdateData>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  public async execute(id: TEntityId, input: TEntityUpdateData) {
    const entity = await this.adaptEntity(input)

    await this.authorize(id)

    const updatedEntity = await this.services
      .resolveRepository()
      .update(id, entity)

    await this.versionEntity(id, updatedEntity)

    await this.services
      .resolveEventsManager()
      .processEntityUpdatedEvent(updatedEntity)

    return {
      id,
    }
  }

  private async adaptEntity(input: TEntityUpdateData) {
    const context = await this.getContext()
    const adapter = this.services.resolveAdapter<unknown, TEntityUpdateData>()
    return adapter
      ? adapter.updateDataToEntity(input, context!)
      : (input as unknown as Partial<TEntity>)
  }

  private async authorize(id: TEntityId) {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const currentEntity = await this.services.resolveRepository().get(id)
    if (currentEntity == null) {
      throw new EntityNotFoundException({
        id,
        type: this.services.getEntityName(),
      })
    }

    const context = await this.getContext()
    if (!context) {
      return
    }

    const authorizationResult = await authorization.canUpdate(
      currentEntity,
      context
    )
    if (!authorizationResult.isAuthorized)
      throw new EntityOperationUnauthorizedException<TEntity>(
        EntityOperationType.Update,
        this.services.getEntityName(),
        currentEntity
      )
  }

  private async getContext() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    return await contextService?.getContext()
  }

  private async versionEntity(id: TEntityId, entity: TEntity) {
    if (!this.isVersioningEnabled()) {
      return
    }

    await this.services.resolveVersionCommand().execute({
      id,
      entity,
      operation: EntityVersionOperation.Update,
    })
  }

  private isVersioningEnabled() {
    return (
      this.services.resolveEntityConfiguration().versioning?.enabled ?? false
    )
  }
}
