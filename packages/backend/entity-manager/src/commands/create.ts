import { EntityVersionOperation } from "../abstractions"
import { IEntityCreateCommand } from "../abstractions/commands"
import {
  EntityOperationType,
  EntityOperationUnauthorizedException,
} from "../abstractions/errors"
import { EntityReference } from "../models"
import { EntityServiceLocator } from "../providers/services"
import { DeepPartial } from "../types"

export class EntityCreateCommand<TEntity, TEntityId, TEntityCreateData>
  implements IEntityCreateCommand<TEntity, TEntityId, TEntityCreateData>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  async execute(input: TEntityCreateData): Promise<EntityReference<TEntityId>> {
    const entity = await this.adaptEntity(input)

    await this.authorize(entity)

    const createdItem = await this.services.resolveRepository().create(entity)

    // todo: parametrize id field
    const id = (createdItem as any).id
    await this.versionEntity(id, createdItem)

    await this.services
      .resolveEventsManager()
      .processEntityCreatedEvent(createdItem)

    return {
      id,
    }
  }

  private async adaptEntity(
    input: TEntityCreateData
  ): Promise<DeepPartial<TEntity>> {
    const context = await this.getContext()
    const adapter = this.services.resolveAdapter<TEntityCreateData, unknown>()
    return adapter
      ? adapter.createDataToEntity(input, context!)
      : (input as DeepPartial<TEntity>)
  }

  private async authorize(entity: DeepPartial<TEntity>) {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const context = await this.getContext()
    if (!context) {
      return
    }

    const authorizationResult = await authorization.canCreate(entity, context)
    if (!authorizationResult.isAuthorized)
      throw new EntityOperationUnauthorizedException<TEntity>(
        EntityOperationType.Create,
        this.services.getEntityName(),
        entity
      )
  }

  private async getContext() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    return await contextService?.getContext()
  }

  private async versionEntity(id: TEntityId, entity: TEntity) {
    if (!this.isVersioningEnabled()) {
      return
    }

    const versioning = this.services.resolveVersioningProvider()
    if (!versioning) {
      return
    }

    await this.services.resolveVersionCommand().execute({
      id,
      entity,
      operation: EntityVersionOperation.Create,
    })
  }

  private isVersioningEnabled() {
    return (
      this.services.resolveEntityConfiguration().versioning?.enabled ?? false
    )
  }
}
