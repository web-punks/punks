export class AppInMemorySettings<T> {
  private _instance: T

  constructor(private readonly name: string) {}

  initialize(value: T) {
    this._instance = value
  }

  get value(): T {
    if (!this._instance) {
      throw new Error(`AppSettings ${this.name} not initialized`)
    }
    return this._instance
  }
}
