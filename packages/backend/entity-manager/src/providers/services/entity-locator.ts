import {
  IAuthenticationContext,
  IAuthenticationContextProvider,
  IEntityFacets,
  IEntitySerializer,
  IEntitySnapshotService,
  IEntityVersioningProvider,
} from "../../abstractions"
import {
  IEntitiesCountAction,
  IEntitiesDeleteAction,
  IEntitiesExportAction,
  IEntitiesImportAction,
  IEntitiesParseAction,
  IEntitiesSampleDownloadAction,
  IEntitiesSearchAction,
  IEntityActions,
  IEntityCreateAction,
  IEntityDeleteAction,
  IEntityExistsAction,
  IEntityGetAction,
  IEntityUpdateAction,
  IEntityUpsertAction,
} from "../../abstractions/actions"
import { IEntityAdapter } from "../../abstractions/adapters"
import { IEntityAuthorizationMiddleware } from "../../abstractions/authorization"
import {
  IEntitiesDeleteCommand,
  IEntitiesDeleteParameters,
  IEntitiesExportCommand,
  IEntitiesImportCommand,
  IEntitiesSampleDownloadCommand,
  IEntityUpsertByCommand,
  IEntityCreateCommand,
  IEntityDeleteCommand,
  IEntityUpdateCommand,
  IEntityUpsertCommand,
  IEntityVersionCommand,
  IEntitiesParseCommand,
} from "../../abstractions/commands"
import { SortingType } from "../../abstractions/common"
import {
  IConnectorsConfiguration,
  IEntityConnectorDeleteManager,
  IEntityConnectorSyncManager,
} from "../../abstractions/connectors"
import { IEntityConverter } from "../../abstractions/converters"
import { IEntityEventsManager } from "../../abstractions/events"
import { IEntityManager } from "../../abstractions/manager"
import {
  IEntitiesCountQuery,
  IEntitiesFindQuery,
  IEntitiesQueryBuilder,
  IEntitiesSearchQuery,
  IEntityExistsQuery,
  IEntityGetQuery,
  IEntityVersionsSearchQuery,
} from "../../abstractions/queries"
import {
  IEntityReplicaDeleteManager,
  IEntityReplicaSyncManager,
  IReplicasConfiguration,
} from "../../abstractions/replication"
import { IEntityRepository } from "../../abstractions/repository"
import {
  IEntitySearchParameters,
  ISearchFilters,
} from "../../abstractions/searchParameters"
import { IEntityConfiguration } from "../../abstractions/settings"
import { EntitiesServiceLocator } from "./entities-locator"

export class EntityServiceLocator<TEntity, TEntityId> {
  constructor(
    private readonly services: EntitiesServiceLocator,
    private readonly entityName: string
  ) {}

  getRootServices() {
    return this.services
  }

  resolveAuthenticationContextProvider<
    TAuthenticationContextProvider extends IAuthenticationContextProvider<TUserContext>,
    TUserContext
  >(): TAuthenticationContextProvider {
    return this.services.resolveAuthenticationContextProvider<
      TAuthenticationContextProvider,
      TUserContext
    >()
  }

  resolveEntityManager(): IEntityManager<
    TEntity,
    unknown,
    unknown,
    unknown,
    any,
    any,
    any,
    unknown,
    any
  > {
    return this.services.resolveEntityManager<TEntity>(this.entityName)
  }

  resolveEntityActions(): IEntityActions<
    TEntity,
    TEntityId,
    unknown,
    unknown,
    unknown,
    unknown,
    any,
    any,
    any,
    unknown,
    any
  > {
    return this.services.resolveEntityActions<TEntity>(this.entityName)
  }

  resolveRepository<
    TGetConditions,
    TFindCondition,
    TUpdateCondition,
    TDeleteCondition
  >(): IEntityRepository<
    TEntity,
    TEntityId,
    TGetConditions,
    TFindCondition,
    TUpdateCondition,
    TDeleteCondition
  > {
    return this.services.resolveRepository<
      TEntity,
      TEntityId,
      TGetConditions,
      TFindCondition,
      TUpdateCondition,
      TDeleteCondition
    >(this.entityName)
  }

  resolveEventsManager(): IEntityEventsManager<TEntity, TEntityId> {
    return this.services.resolveEventsManager<TEntity, TEntityId>(
      this.entityName
    )
  }

  resolveVersioningProvider(): IEntityVersioningProvider {
    return this.services.resolveEntityVersioningProvider()
  }

  resolveEntityConfiguration(): IEntityConfiguration {
    return this.services.resolveEntityConfiguration(this.entityName)
  }

  resolveReplicaSyncManager(): IEntityReplicaSyncManager<TEntity> {
    return this.services.resolveReplicaSyncManager<TEntity>(this.entityName)
  }

  resolveReplicaConfiguration(): IReplicasConfiguration<TEntity, TEntityId> {
    return this.services.resolveReplicaConfiguration<TEntity, TEntityId>(
      this.entityName
    )
  }

  resolveReplicaDeleteManager<TEntityId>(): IEntityReplicaDeleteManager<
    TEntity,
    TEntityId
  > {
    return this.services.resolveReplicaDeleteManager<TEntity, TEntityId>(
      this.entityName
    )
  }

  resolveConnectorSyncManager(): IEntityConnectorSyncManager<TEntity> {
    return this.services.resolveConnectorSyncManager<TEntity>(this.entityName)
  }

  resolveConnectorDeleteManager<TEntityId>(): IEntityConnectorDeleteManager<
    TEntity,
    TEntityId
  > {
    return this.services.resolveConnectorDeleteManager<TEntity, TEntityId>(
      this.entityName
    )
  }

  resolveConnectorsConfiguration(): IConnectorsConfiguration<TEntity> {
    return this.services.resolveConnectorsConfiguration<TEntity>(
      this.entityName
    )
  }

  resolveConverter<
    TEntityDto,
    TListItemDto,
    TEntityCreateDto,
    TEntityUpdateDto
  >():
    | IEntityConverter<
        TEntity,
        TEntityDto,
        TListItemDto,
        TEntityCreateDto,
        TEntityUpdateDto
      >
    | undefined {
    return this.services.resolveConverter<
      TEntity,
      TEntityDto,
      TListItemDto,
      TEntityCreateDto,
      TEntityUpdateDto
    >(this.entityName)
  }

  resolveSerializer(): IEntitySerializer<TEntity, TEntityId, unknown, unknown> {
    return this.services.resolveSerializer<TEntity, TEntityId>(this.entityName)
  }

  resolveSnapshotService<TEntity, TEntityId>():
    | IEntitySnapshotService<TEntityId, unknown>
    | undefined {
    return this.services.resolveSnapshotService<TEntity, TEntityId>(
      this.entityName
    )
  }

  resolveAdapter<TEntityCreateData, TEntityUpdateData>():
    | IEntityAdapter<TEntity, TEntityCreateData, TEntityUpdateData>
    | undefined {
    return this.services.resolveAdapter<
      TEntity,
      TEntityCreateData,
      TEntityUpdateData
    >(this.entityName)
  }

  resolveAuthorizationMiddleware<
    TAuthenticationContext extends IAuthenticationContext<TUserContext>,
    TUserContext
  >():
    | IEntityAuthorizationMiddleware<
        TEntity,
        TAuthenticationContext,
        TUserContext
      >
    | undefined {
    return this.services.resolveAuthorizationMiddleware<
      TEntity,
      TAuthenticationContext,
      TUserContext
    >(this.entityName)
  }

  resolveGetQuery<
    TEntityId,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      unknown,
      unknown
    >
  >(): IEntityGetQuery<TEntity, TEntityId, TEntitySearchParameters> {
    return this.services.resolveGetQuery<
      TEntity,
      TEntityId,
      TEntitySearchParameters
    >(this.entityName)
  }

  resolveExistsQuery<
    TEntityFilters extends ISearchFilters
  >(): IEntityExistsQuery<TEntity, TEntityFilters> {
    return this.services.resolveExistsQuery<TEntity, TEntityFilters>(
      this.entityName
    )
  }

  resolveCountQuery<
    TEntityFilters extends ISearchFilters
  >(): IEntitiesCountQuery<TEntity, TEntityFilters> {
    return this.services.resolveCountQuery<TEntity, TEntityFilters>(
      this.entityName
    )
  }

  resolveVersionsSearchQuery<TCursor>(): IEntityVersionsSearchQuery<
    TEntity,
    TCursor
  > {
    return this.services.resolveVersionsSearchQuery<TEntity, TCursor>(
      this.entityName
    )
  }

  resolveSearchQuery<
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor,
    TFacets extends IEntityFacets
  >(): IEntitiesSearchQuery<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor,
    TFacets
  > {
    return this.services.resolveSearchQuery<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets
    >(this.entityName)
  }

  resolveFindQuery<
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor
  >(): IEntitiesFindQuery<TEntity, TEntitySearchParameters, TSorting, TCursor> {
    return this.services.resolveFindQuery<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor
    >(this.entityName)
  }

  resolveCreateCommand<TEntityCreateData>(): IEntityCreateCommand<
    TEntity,
    TEntityId,
    TEntityCreateData
  > {
    return this.services.resolveCreateCommand<
      TEntity,
      TEntityId,
      TEntityCreateData
    >(this.entityName)
  }

  resolveUpdateCommand<TEntityUpdateData>(): IEntityUpdateCommand<
    TEntity,
    TEntityId,
    TEntityUpdateData
  > {
    return this.services.resolveUpdateCommand<
      TEntity,
      TEntityId,
      TEntityUpdateData
    >(this.entityName)
  }

  resolveUpsertCommand<TEntityUpdateData>(): IEntityUpsertCommand<
    TEntity,
    TEntityId,
    TEntityUpdateData
  > {
    return this.services.resolveUpsertCommand<
      TEntity,
      TEntityId,
      TEntityUpdateData
    >(this.entityName)
  }

  resolveUpsertByCommand<
    TEntityFilters extends ISearchFilters
  >(): IEntityUpsertByCommand<TEntity, TEntityFilters> {
    return this.services.resolveUpsertByCommand<TEntity, TEntityFilters>(
      this.entityName
    )
  }

  resolveDeleteCommand<TEntityId>(): IEntityDeleteCommand<TEntity, TEntityId> {
    return this.services.resolveDeleteCommand<TEntity, TEntityId>(
      this.entityName
    )
  }

  resolveDeleteItemsCommand<
    TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
    TSorting
  >(): IEntitiesDeleteCommand<TEntity, TEntitiesDeleteParameters, TSorting> {
    return this.services.resolveDeleteItemsCommand<
      TEntity,
      TEntitiesDeleteParameters,
      TSorting
    >(this.entityName)
  }

  resolveVersionCommand(): IEntityVersionCommand<TEntity, TEntityId> {
    return this.services.resolveVersionCommand<TEntity, TEntityId>(
      this.entityName
    )
  }

  resolveSampleDownloadCommand(): IEntitiesSampleDownloadCommand<TEntity> {
    return this.services.resolveSampleDownloadCommand<TEntity>(this.entityName)
  }

  resolveImportCommand(): IEntitiesImportCommand<TEntity> {
    return this.services.resolveImportCommand<TEntity>(this.entityName)
  }

  resolveParseCommand(): IEntitiesParseCommand<TEntity> {
    return this.services.resolveParseCommand<TEntity>(this.entityName)
  }

  resolveExportCommand<
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor
  >(): IEntitiesExportCommand<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor
  > {
    return this.services.resolveExportCommand<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor
    >(this.entityName)
  }

  resolveGetAction<
    TEntityId,
    TEntityDto,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      unknown,
      unknown
    >
  >(): IEntityGetAction<
    TEntity,
    TEntityId,
    TEntityDto,
    TEntitySearchParameters
  > {
    return this.services.resolveGetAction<
      TEntity,
      TEntityId,
      TEntityDto,
      TEntitySearchParameters
    >(this.entityName)
  }

  resolveExistsAction<
    TEntityFilters extends ISearchFilters
  >(): IEntityExistsAction<TEntity, TEntityFilters> {
    return this.services.resolveExistsAction<TEntity, TEntityFilters>(
      this.entityName
    )
  }

  resolveCountAction<
    TEntityFilters extends ISearchFilters
  >(): IEntitiesCountAction<TEntity, TEntityFilters> {
    return this.services.resolveCountAction<TEntity, TEntityFilters>(
      this.entityName
    )
  }

  resolveSearchAction<
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TEntityListItemDto,
    TSorting extends SortingType,
    TCursor,
    TFacets extends IEntityFacets
  >(): IEntitiesSearchAction<
    TEntity,
    TEntitySearchParameters,
    TEntityListItemDto,
    TSorting,
    TCursor,
    TFacets
  > {
    return this.services.resolveSearchAction<
      TEntity,
      TEntitySearchParameters,
      TEntityListItemDto,
      TSorting,
      TCursor,
      TFacets
    >(this.entityName)
  }

  resolveVersionsSearchAction<TEntityListItemDto, TCursor>() {
    return this.services.resolveVersionsSearchAction<
      TEntity,
      TEntityListItemDto,
      TCursor
    >(this.entityName)
  }

  resolveCreateAction<TEntityCreateData, TEntityDto>(): IEntityCreateAction<
    TEntity,
    TEntityCreateData,
    TEntityDto
  > {
    return this.services.resolveCreateAction<
      TEntity,
      TEntityCreateData,
      TEntityDto
    >(this.entityName)
  }

  resolveUpdateAction<TEntityUpdateData, TEntityDto>(): IEntityUpdateAction<
    TEntity,
    TEntityId,
    TEntityUpdateData,
    TEntityDto
  > {
    return this.services.resolveUpdateAction<
      TEntity,
      TEntityId,
      TEntityUpdateData,
      TEntityDto
    >(this.entityName)
  }

  resolveUpsertAction<TEntityUpdateData, TEntityDto>(): IEntityUpsertAction<
    TEntity,
    TEntityId,
    TEntityUpdateData,
    TEntityDto
  > {
    return this.services.resolveUpsertAction<
      TEntity,
      TEntityId,
      TEntityUpdateData,
      TEntityDto
    >(this.entityName)
  }

  resolveDeleteAction(): IEntityDeleteAction<TEntity, TEntityId> {
    return this.services.resolveDeleteAction<TEntity, TEntityId>(
      this.entityName
    )
  }

  resolveDeleteItemsAction<
    TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
    TSorting
  >(): IEntitiesDeleteAction<TEntity, TEntitiesDeleteParameters, TSorting> {
    return this.services.resolveDeleteItemsAction<
      TEntity,
      TEntitiesDeleteParameters,
      TSorting
    >(this.entityName)
  }

  resolveImportAction(): IEntitiesImportAction<TEntity> {
    return this.services.resolveImportAction<TEntity>(this.entityName)
  }

  resolveParseAction(): IEntitiesParseAction<TEntity> {
    return this.services.resolveParseAction<TEntity>(this.entityName)
  }

  resolveSampleDownloadAction(): IEntitiesSampleDownloadAction<TEntity> {
    return this.services.resolveSampleDownloadAction<TEntity>(this.entityName)
  }

  resolveExportAction<
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor
  >(): IEntitiesExportAction<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor
  > {
    return this.services.resolveExportAction<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor
    >(this.entityName)
  }

  resolveQueryBuilder<
    TEntityId,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor,
    TFacets extends IEntityFacets,
    TUserContext
  >(): IEntitiesQueryBuilder<
    TEntity,
    TEntityId,
    TEntitySearchParameters,
    TSorting,
    TCursor,
    TFacets,
    TUserContext
  > {
    return this.services.resolveQueryBuilder<
      TEntity,
      TEntityId,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets,
      TUserContext
    >(this.entityName)
  }

  getEntityName() {
    return this.entityName
  }
}
