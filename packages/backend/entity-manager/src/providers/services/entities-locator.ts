import {
  IEntitiesCountAction,
  IEntitiesDeleteAction,
  IEntitiesExportAction,
  IEntitiesImportAction,
  IEntitiesParseAction,
  IEntitiesSampleDownloadAction,
  IEntitiesSearchAction,
  IEntityActions,
  IEntityCreateAction,
  IEntityDeleteAction,
  IEntityExistsAction,
  IEntityGetAction,
  IEntityUpdateAction,
  IEntityUpsertAction,
  IEntityVersionsSearchAction,
} from "../../abstractions/actions"
import { IEntityAdapter } from "../../abstractions/adapters"
import {
  IAuthenticationContext,
  IAuthenticationContextProvider,
  IAuthenticationMiddleware,
} from "../../abstractions/authentication"
import { IEntityAuthorizationMiddleware } from "../../abstractions/authorization"
import { IBucketProvider } from "../../abstractions/buckets"
import { ICacheInstance } from "../../abstractions/cache"
import {
  IEntitiesDeleteCommand,
  IEntitiesDeleteParameters,
  IEntitiesExportCommand,
  IEntitiesImportCommand,
  IEntitiesParseCommand,
  IEntitiesSampleDownloadCommand,
  IEntityCreateCommand,
  IEntityDeleteCommand,
  IEntityUpdateCommand,
  IEntityUpsertByCommand,
  IEntityUpsertCommand,
  IEntityVersionCommand,
} from "../../abstractions/commands"
import { SortingType } from "../../abstractions/common"
import {
  IConnectorsConfiguration,
  IEntityConnectorDeleteManager,
  IEntityConnectorSyncManager,
} from "../../abstractions/connectors"
import { IEntityConverter } from "../../abstractions/converters"
import {
  IEmailLogger,
  IEmailProvider,
  IEmailTemplateMiddleware,
  IEmailTemplatesCollection,
} from "../../abstractions/email"
import { IEntityEventsManager, IEventEmitter } from "../../abstractions/events"
import {
  IFileProvider,
  IFilesReferenceRepository,
} from "../../abstractions/files"
import { IEntityManager } from "../../abstractions/manager"
import {
  IMediaFolderRepository,
  IMediaProvider,
  IMediaReferenceRepository,
} from "../../abstractions/media"
import { IOperationLockService } from "../../abstractions/operations"
import { IPipelinesController } from "../../abstractions/pipelines"
import {
  IEntitiesCountQuery,
  IEntitiesFindQuery,
  IEntitiesQueryBuilder,
  IEntitiesSearchQuery,
  IEntityExistsQuery,
  IEntityGetQuery,
  IEntityVersionsSearchQuery,
} from "../../abstractions/queries"
import {
  IEntityReplicaDeleteManager,
  IEntityReplicaSyncManager,
  IReplicasConfiguration,
} from "../../abstractions/replication"
import { IEntityRepository } from "../../abstractions/repository"
import {
  IEntitySearchParameters,
  ISearchFilters,
} from "../../abstractions/searchParameters"
import { IEntityFacets } from "../../abstractions/searchResults"
import { ISecretsProvider } from "../../abstractions/secrets"
import { IEntitySerializer } from "../../abstractions/serializer"
import {
  EntityManagerSettings,
  IEntityConfiguration,
} from "../../abstractions/settings"
import { IEntitySnapshotService } from "../../abstractions/snapshot"
import { IEventLog, IEventsTracker } from "../../abstractions/tracking"
import { IEntityVersioningProvider } from "../../abstractions/versioning"
import { ServiceLocator } from "../../ioc"
import { EntityServices, GlobalServices } from "../../symbols/ioc"

export class EntitiesServiceLocator {
  constructor(private readonly provider: ServiceLocator) {}

  getProvider() {
    return this.provider
  }

  registerSettings(settings: EntityManagerSettings) {
    this.provider.register<EntityManagerSettings>(
      GlobalServices.Settings.EntityManagerSettings,
      settings
    )
    return this
  }

  resolveSettings() {
    return this.provider.resolve<EntityManagerSettings>(
      GlobalServices.Settings.EntityManagerSettings
    )
  }

  resolveEventEmitter<TEventEmitter extends IEventEmitter>(): TEventEmitter {
    return this.provider.resolve<TEventEmitter>(
      GlobalServices.Events.IEventEmitter,
      {
        optional: true,
      }
    )
  }

  registerEventEmitter<TEventEmitter extends IEventEmitter>(
    instance: TEventEmitter
  ) {
    this.provider.register<IEventEmitter>(
      GlobalServices.Events.IEventEmitter,
      instance
    )
  }

  resolveEmailProvider<
    TEmailProvider extends IEmailProvider<unknown>
  >(): TEmailProvider {
    return this.provider.resolve<TEmailProvider>(
      GlobalServices.Plugins.IEmailProvider
    )
  }

  registerEmailProvider<TEmailProvider extends IEmailProvider<unknown>>(
    instance: TEmailProvider
  ) {
    this.provider.register<IEmailProvider<unknown>>(
      GlobalServices.Plugins.IEmailProvider,
      instance
    )
  }

  resolveEntityVersioningProvider<
    TEntityVersioningProvider extends IEntityVersioningProvider
  >(): TEntityVersioningProvider {
    return this.provider.resolve<TEntityVersioningProvider>(
      GlobalServices.Versioning.IEntityVersioningProvider
    )
  }

  registerEntityVersioningProvider<
    TEntityVersioningProvider extends IEntityVersioningProvider
  >(instance: TEntityVersioningProvider) {
    this.provider.register<IEntityVersioningProvider>(
      GlobalServices.Versioning.IEntityVersioningProvider,
      instance
    )
  }

  resolveDefaultBucketProvider<
    TBucketProvider extends IBucketProvider
  >(): TBucketProvider {
    const settings = this.resolveSettings()
    return this.provider.resolveMultipleNamed<TBucketProvider>(
      GlobalServices.Plugins.IBucketProvider,
      settings.defaultBucketProvider
    )
  }

  resolveDefaultFilesProvider<
    TFileProvider extends IFileProvider
  >(): TFileProvider {
    const settings = this.resolveSettings()
    return this.provider.resolveMultipleNamed<TFileProvider>(
      GlobalServices.Plugins.IFileProvider,
      settings.defaultFilesProvider
    )
  }

  resolveDefaultMediaProvider<
    TMediaProvider extends IMediaProvider
  >(): TMediaProvider {
    const settings = this.resolveSettings()
    return this.provider.resolveMultipleNamed<TMediaProvider>(
      GlobalServices.Plugins.IMediaProvider,
      settings.defaultMediaProvider
    )
  }

  resolveCacheInstance<TCacheInstance extends ICacheInstance>(
    name: string
  ): TCacheInstance {
    return this.provider.resolveMultipleNamed<TCacheInstance>(
      GlobalServices.Plugins.ICacheInstance,
      name
    )
  }

  resolveCacheInstances<
    TCacheInstance extends ICacheInstance
  >(): TCacheInstance[] {
    return this.provider.resolveMultiple<TCacheInstance>(
      GlobalServices.Plugins.ICacheInstance
    )
  }

  registerCacheInstance<TCacheInstance extends ICacheInstance>(
    name: string,
    instance: TCacheInstance
  ) {
    this.provider.registerMultiple<TCacheInstance>(
      GlobalServices.Plugins.ICacheInstance,
      name,
      instance
    )
    return this
  }

  resolveBucketProviders<
    TBucketProvider extends IFileProvider
  >(): TBucketProvider[] {
    return this.provider.resolveMultiple<TBucketProvider>(
      GlobalServices.Plugins.IFileProvider
    )
  }

  registerBucketProvider<TBucketProvider extends IBucketProvider>(
    name: string,
    instance: TBucketProvider
  ) {
    this.provider.registerMultiple<TBucketProvider>(
      GlobalServices.Plugins.IBucketProvider,
      name,
      instance
    )
    return this
  }

  resolveSecretsProviders<
    TSecretsProvider extends ISecretsProvider
  >(): TSecretsProvider[] {
    return this.provider.resolveMultiple<TSecretsProvider>(
      GlobalServices.Plugins.ISecretsProvider
    )
  }

  registerSecretsProvider<TSecretsProvider extends ISecretsProvider>(
    name: string,
    instance: TSecretsProvider
  ) {
    this.provider.registerMultiple<TSecretsProvider>(
      GlobalServices.Plugins.ISecretsProvider,
      name,
      instance
    )
    return this
  }

  resolveAuthenticationMiddlewares<
    TAuthenticationMiddleware extends IAuthenticationMiddleware<any, unknown>
  >(): TAuthenticationMiddleware[] {
    return (
      this.provider.resolveMultiple<TAuthenticationMiddleware>(
        GlobalServices.Authentication.IAuthenticationMiddleware,
        {
          optional: true,
        }
      ) ?? []
    )
  }

  registerAuthenticationMiddleware<
    TAuthenticationMiddleware extends IAuthenticationMiddleware<any, unknown>
  >(name: string, instance: TAuthenticationMiddleware) {
    this.provider.registerMultiple<TAuthenticationMiddleware>(
      GlobalServices.Authentication.IAuthenticationMiddleware,
      name,
      instance
    )
    return this
  }

  resolveMediaReferenceRepository<
    TMediaReferenceRepository extends IMediaReferenceRepository
  >(): TMediaReferenceRepository {
    return this.provider.resolve<TMediaReferenceRepository>(
      GlobalServices.Plugins.IMediaReferenceRepository
    )
  }

  registerMediaReferenceRepository<
    TMediaReferenceRepository extends IMediaReferenceRepository
  >(instance: TMediaReferenceRepository) {
    this.provider.register<IMediaReferenceRepository>(
      GlobalServices.Plugins.IMediaReferenceRepository,
      instance
    )
  }

  resolveMediaFolderRepository<
    TMediaFolderRepository extends IMediaFolderRepository
  >(): TMediaFolderRepository {
    return this.provider.resolve<TMediaFolderRepository>(
      GlobalServices.Plugins.IMediaFolderRepository
    )
  }

  registerMediaFolderRepository<
    TMediaFolderRepository extends IMediaFolderRepository
  >(instance: TMediaFolderRepository) {
    this.provider.register<IMediaFolderRepository>(
      GlobalServices.Plugins.IMediaFolderRepository,
      instance
    )
  }

  resolveMediaProvider<TMediaProvider extends IMediaProvider>(
    providerId: string
  ): TMediaProvider {
    return this.provider.resolveMultipleNamed<TMediaProvider>(
      GlobalServices.Plugins.IMediaProvider,
      providerId
    )
  }

  registerMediaProvider<TMediaProvider extends IMediaProvider>(
    name: string,
    instance: TMediaProvider
  ) {
    this.provider.registerMultiple<TMediaProvider>(
      GlobalServices.Plugins.IMediaProvider,
      name,
      instance
    )
    return this
  }

  resolveFileProvider<TFileProvider extends IFileProvider>(
    providerId: string
  ): TFileProvider {
    return this.provider.resolveMultipleNamed<TFileProvider>(
      GlobalServices.Plugins.IFileProvider,
      providerId
    )
  }

  registerFileProvider<TFileProvider extends IFileProvider>(
    name: string,
    instance: TFileProvider
  ) {
    this.provider.registerMultiple<TFileProvider>(
      GlobalServices.Plugins.IFileProvider,
      name,
      instance
    )
    return this
  }

  resolveFilesReferenceRepositoryProviders<
    TFilesReferenceRepo extends IFilesReferenceRepository
  >(): TFilesReferenceRepo {
    return this.provider.resolve<TFilesReferenceRepo>(
      GlobalServices.Files.IFilesReferenceRepo
    )
  }

  registerFilesReferenceRepositoryProviders<
    TFilesReferenceRepo extends IFilesReferenceRepository
  >(instance: TFilesReferenceRepo) {
    this.provider.register<TFilesReferenceRepo>(
      GlobalServices.Files.IFilesReferenceRepo,
      instance
    )
  }

  resolveEventsTracker<
    TEventsTracker extends IEventsTracker<TEventLog>,
    TEventLog extends IEventLog<unknown>
  >(): TEventsTracker {
    return this.provider.resolve<TEventsTracker>(
      GlobalServices.Events.IEventsTracker
    )
  }

  registerEventsTracker<
    TEventsTracker extends IEventsTracker<TEventLog>,
    TEventLog extends IEventLog<unknown>
  >(instance: TEventsTracker) {
    this.provider.register<IEventsTracker<TEventLog>>(
      GlobalServices.Events.IEventsTracker,
      instance
    )
  }

  resolveOperationLockService(): IOperationLockService {
    return this.provider.resolve<IOperationLockService>(
      GlobalServices.Operations.IOperationLockService
    )
  }

  registerOperationLockService(instance: IOperationLockService) {
    this.provider.register<IOperationLockService>(
      GlobalServices.Operations.IOperationLockService,
      instance
    )
  }

  resolveEmailTemplatesCollection(): IEmailTemplatesCollection {
    return this.provider.resolve<IEmailTemplatesCollection>(
      GlobalServices.Plugins.IEmailTemplatesCollection
    )
  }

  registerEmailTemplatesCollection(instance: IEmailTemplatesCollection) {
    this.provider.register<IEmailTemplatesCollection>(
      GlobalServices.Plugins.IEmailTemplatesCollection,
      instance
    )
  }

  resolveEmailTemplateMiddleware(): IEmailTemplateMiddleware | undefined {
    return this.provider.resolve<IEmailTemplateMiddleware>(
      GlobalServices.Plugins.IEmailTemplateMiddleware,
      {
        optional: true,
      }
    )
  }

  registerEmailTemplateMiddleware(instance: IEmailTemplateMiddleware) {
    this.provider.register<IEmailTemplateMiddleware>(
      GlobalServices.Plugins.IEmailTemplateMiddleware,
      instance
    )
  }

  resolveEmailLogger(): IEmailLogger {
    return this.provider.resolve<IEmailLogger>(
      GlobalServices.Plugins.IEmailLogger
    )
  }

  registerEmailLogger(instance: IEmailLogger) {
    this.provider.register<IEmailLogger>(
      GlobalServices.Plugins.IEmailLogger,
      instance
    )
  }

  resolvePipelinesController(): IPipelinesController {
    return this.provider.resolve<IPipelinesController>(
      GlobalServices.Pipelines.IPipelineController
    )
  }

  registerPipelinesController(instance: IPipelinesController) {
    this.provider.register<IPipelinesController>(
      GlobalServices.Pipelines.IPipelineController,
      instance
    )
  }

  resolveAuthenticationContextProvider<
    TAuthenticationContextProvider extends IAuthenticationContextProvider<TUserContext>,
    TUserContext
  >(): TAuthenticationContextProvider {
    return this.provider.resolve<TAuthenticationContextProvider>(
      GlobalServices.Authentication.IAuthenticationContextProvider,
      {
        optional: true,
      }
    )
  }

  registerAuthenticationContextProvider<
    TAuthenticationContextProvider extends IAuthenticationContextProvider<TUserContext>,
    TUserContext
  >(instance: TAuthenticationContextProvider) {
    this.provider.register<TAuthenticationContextProvider>(
      GlobalServices.Authentication.IAuthenticationContextProvider,
      instance
    )
  }

  resolveEntityConfiguration(entityName: string): IEntityConfiguration {
    return this.provider.resolveEntityService<IEntityConfiguration>(
      EntityServices.Settings.IEntityConfiguration,
      entityName
    )
  }

  registerEntityConfiguration(
    entityName: string,
    instance: IEntityConfiguration
  ) {
    return this.provider.registerEntityService<IEntityConfiguration>(
      EntityServices.Settings.IEntityConfiguration,
      entityName,
      instance
    )
  }

  resolveEntityManager<TEntity>(
    entityName: string
  ): IEntityManager<
    TEntity,
    unknown,
    unknown,
    unknown,
    any,
    any,
    any,
    unknown,
    any
  > {
    return this.provider.resolveEntityService<
      IEntityManager<
        TEntity,
        unknown,
        unknown,
        unknown,
        any,
        any,
        any,
        unknown,
        any
      >
    >(EntityServices.Services.IEntityManager, entityName)
  }

  registerEntityManager<TEntity>(
    entityName: string,
    instance: IEntityManager<
      TEntity,
      unknown,
      unknown,
      unknown,
      any,
      any,
      any,
      unknown,
      any
    >
  ) {
    return this.provider.registerEntityService<
      IEntityManager<
        TEntity,
        unknown,
        unknown,
        unknown,
        any,
        any,
        any,
        unknown,
        any
      >
    >(EntityServices.Services.IEntityManager, entityName, instance)
  }

  resolveEntityActions<TEntity>(
    entityName: string
  ): IEntityActions<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown,
    any,
    any,
    any,
    unknown,
    any
  > {
    return this.provider.resolveEntityService<
      IEntityActions<
        TEntity,
        unknown,
        unknown,
        unknown,
        unknown,
        unknown,
        any,
        any,
        any,
        unknown,
        any
      >
    >(EntityServices.Services.IEntityActions, entityName)
  }

  registerEntityActions<TEntity>(
    entityName: string,
    instance: IEntityActions<
      TEntity,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      any,
      any,
      any,
      unknown,
      any
    >
  ) {
    this.provider.registerEntityService<
      IEntityActions<
        TEntity,
        unknown,
        unknown,
        unknown,
        unknown,
        unknown,
        any,
        any,
        any,
        unknown,
        any
      >
    >(EntityServices.Services.IEntityActions, entityName, instance)
  }

  resolveRepository<
    TEntity,
    TEntityId,
    TGetConditions,
    TFindCondition,
    TUpdateCondition,
    TDeleteCondition
  >(
    entityName: string
  ): IEntityRepository<
    TEntity,
    TEntityId,
    TGetConditions,
    TFindCondition,
    TUpdateCondition,
    TDeleteCondition
  > {
    return this.provider.resolveEntityService<
      IEntityRepository<
        TEntity,
        TEntityId,
        TGetConditions,
        TFindCondition,
        TUpdateCondition,
        TDeleteCondition
      >
    >(EntityServices.Storage.IEntityRepository, entityName)
  }

  registerRepository<
    TEntity,
    TEntityId,
    TGetConditions,
    TFindCondition,
    TUpdateCondition,
    TDeleteCondition
  >(
    entityName: string,
    instance: IEntityRepository<
      TEntity,
      TEntityId,
      TGetConditions,
      TFindCondition,
      TUpdateCondition,
      TDeleteCondition
    >
  ) {
    this.provider.registerEntityService<
      IEntityRepository<
        TEntity,
        TEntityId,
        TGetConditions,
        TFindCondition,
        TUpdateCondition,
        TDeleteCondition
      >
    >(EntityServices.Storage.IEntityRepository, entityName, instance)
  }

  resolveEventsManager<TEntity, TEntityId>(
    entityName: string
  ): IEntityEventsManager<TEntity, TEntityId> {
    return this.provider.resolveEntityService<
      IEntityEventsManager<TEntity, TEntityId>
    >(EntityServices.Events.IEntityEventsManager, entityName)
  }

  registerEventsManager<TEntity, TEntityId>(
    entityName: string,
    instance: IEntityEventsManager<TEntity, TEntityId>
  ) {
    this.provider.registerEntityService<
      IEntityEventsManager<TEntity, TEntityId>
    >(EntityServices.Events.IEntityEventsManager, entityName, instance)
  }

  resolveReplicaSyncManager<TEntity>(
    entityName: string
  ): IEntityReplicaSyncManager<TEntity> {
    return this.provider.resolveEntityService<
      IEntityReplicaSyncManager<TEntity>
    >(EntityServices.Replication.IEntityReplicaSyncManager, entityName)
  }

  registerReplicaSyncManager<TEntity>(
    entityName: string,
    instance: IEntityReplicaSyncManager<TEntity>
  ) {
    this.provider.registerEntityService<IEntityReplicaSyncManager<TEntity>>(
      EntityServices.Replication.IEntityReplicaSyncManager,
      entityName,
      instance
    )
  }

  resolveReplicaConfiguration<TEntity, TEntityId>(
    entityName: string
  ): IReplicasConfiguration<TEntity, TEntityId> {
    return this.provider.resolveEntityService<
      IReplicasConfiguration<TEntity, TEntityId>
    >(EntityServices.Replication.IReplicasConfiguration, entityName)
  }

  registerReplicaConfiguration<TEntity, TEntityId>(
    entityName: string,
    instance: IReplicasConfiguration<TEntity, TEntityId>
  ) {
    this.provider.registerEntityService<
      IReplicasConfiguration<TEntity, TEntityId>
    >(EntityServices.Replication.IReplicasConfiguration, entityName, instance)
  }

  resolveReplicaDeleteManager<TEntity, TEntityId>(
    entityName: string
  ): IEntityReplicaDeleteManager<TEntity, TEntityId> {
    return this.provider.resolveEntityService<
      IEntityReplicaDeleteManager<TEntity, TEntityId>
    >(EntityServices.Replication.IEntityReplicaDeleteManager, entityName)
  }

  registerReplicaDeleteManager<TEntity, TEntityId>(
    entityName: string,
    instance: IEntityReplicaDeleteManager<TEntity, TEntityId>
  ) {
    this.provider.registerEntityService<
      IEntityReplicaDeleteManager<TEntity, TEntityId>
    >(
      EntityServices.Replication.IEntityReplicaDeleteManager,
      entityName,
      instance
    )
  }

  resolveConnectorSyncManager<TEntity>(
    entityName: string
  ): IEntityConnectorSyncManager<TEntity> {
    return this.provider.resolveEntityService<
      IEntityConnectorSyncManager<TEntity>
    >(EntityServices.Connectors.IEntityConnectorSyncManager, entityName)
  }

  registerConnectorSyncManager<TEntity>(
    entityName: string,
    instance: IEntityConnectorSyncManager<TEntity>
  ) {
    this.provider.registerEntityService<IEntityConnectorSyncManager<TEntity>>(
      EntityServices.Connectors.IEntityConnectorSyncManager,
      entityName,
      instance
    )
  }

  resolveConnectorDeleteManager<TEntity, TEntityId>(
    entityName: string
  ): IEntityConnectorDeleteManager<TEntity, TEntityId> {
    return this.provider.resolveEntityService<
      IEntityConnectorDeleteManager<TEntity, TEntityId>
    >(EntityServices.Connectors.IEntityConnectorDeleteManager, entityName)
  }

  registerConnectorDeleteManager<TEntity, TEntityId>(
    entityName: string,
    instance: IEntityConnectorDeleteManager<TEntity, TEntityId>
  ) {
    this.provider.registerEntityService<
      IEntityConnectorDeleteManager<TEntity, TEntityId>
    >(
      EntityServices.Connectors.IEntityConnectorDeleteManager,
      entityName,
      instance
    )
  }

  resolveConnectorsConfiguration<TEntity>(
    entityName: string
  ): IConnectorsConfiguration<TEntity> {
    return this.provider.resolveEntityService<
      IConnectorsConfiguration<TEntity>
    >(EntityServices.Connectors.IConnectorsConfiguration, entityName)
  }

  registerConnectorsConfiguration<TEntity>(
    entityName: string,
    instance: IConnectorsConfiguration<TEntity>
  ) {
    this.provider.registerEntityService<IConnectorsConfiguration<TEntity>>(
      EntityServices.Connectors.IConnectorsConfiguration,
      entityName,
      instance
    )
  }

  registerAdapter<TEntity, TEntityCreateData, TEntityUpdateData>(
    entityName: string,
    instance: IEntityAdapter<TEntity, TEntityCreateData, TEntityUpdateData>
  ) {
    this.provider.registerEntityService<
      IEntityAdapter<TEntity, TEntityCreateData, TEntityUpdateData>
    >(EntityServices.Converters.IEntityAdapter, entityName, instance)
  }

  resolveAdapter<TEntity, TEntityCreateData, TEntityUpdateData>(
    entityName: string
  ): IEntityAdapter<TEntity, TEntityCreateData, TEntityUpdateData> | undefined {
    return this.provider.resolveEntityService<
      IEntityAdapter<TEntity, TEntityCreateData, TEntityUpdateData>
    >(EntityServices.Converters.IEntityAdapter, entityName, {
      optional: true,
    })
  }

  registerConverter<
    TEntity,
    TEntityDto,
    TListItemDto,
    TEntityCreateDto,
    TEntityUpdateDto
  >(
    entityName: string,
    instance: IEntityConverter<
      TEntity,
      TEntityDto,
      TListItemDto,
      TEntityCreateDto,
      TEntityUpdateDto
    >
  ) {
    this.provider.registerEntityService<
      IEntityConverter<
        TEntity,
        TEntityDto,
        TListItemDto,
        TEntityCreateDto,
        TEntityUpdateDto
      >
    >(EntityServices.Converters.IEntityConverter, entityName, instance)
  }

  resolveConverter<
    TEntity,
    TEntityDto,
    TListItemDto,
    TEntityCreateDto,
    TEntityUpdateDto
  >(
    entityName: string
  ):
    | IEntityConverter<
        TEntity,
        TEntityDto,
        TListItemDto,
        TEntityCreateDto,
        TEntityUpdateDto
      >
    | undefined {
    return this.provider.resolveEntityService<
      IEntityConverter<
        TEntity,
        TEntityDto,
        TListItemDto,
        TEntityCreateDto,
        TEntityUpdateDto
      >
    >(EntityServices.Converters.IEntityConverter, entityName, {
      optional: true,
    })
  }

  registerSerializer<TEntity, TEntityId>(
    entityName: string,
    instance: IEntitySerializer<TEntity, TEntityId, unknown, unknown>
  ) {
    this.provider.registerEntityService<
      IEntitySerializer<TEntity, TEntityId, unknown, unknown>
    >(EntityServices.Converters.IEntitySerializer, entityName, instance)
  }

  resolveSerializer<TEntity, TEntityId>(
    entityName: string
  ): IEntitySerializer<TEntity, TEntityId, unknown, unknown> {
    return this.provider.resolveEntityService<
      IEntitySerializer<TEntity, TEntityId, unknown, unknown>
    >(EntityServices.Converters.IEntitySerializer, entityName)
  }

  registerSnapshotService<TEntity, TEntityId>(
    entityName: string,
    instance: IEntitySnapshotService<TEntityId, unknown>
  ) {
    this.provider.registerEntityService<
      IEntitySnapshotService<TEntityId, unknown>
    >(EntityServices.Converters.IEntitySnapshotService, entityName, instance)
  }

  resolveSnapshotService<TEntity, TEntityId>(
    entityName: string
  ): IEntitySnapshotService<TEntityId, unknown> | undefined {
    return this.provider.resolveEntityService<
      IEntitySnapshotService<TEntityId, unknown>
    >(EntityServices.Converters.IEntitySnapshotService, entityName, {
      optional: true,
    })
  }

  resolveAuthorizationMiddleware<
    TEntity,
    TAuthenticationContext extends IAuthenticationContext<TUserContext>,
    TUserContext
  >(
    entityName: string
  ):
    | IEntityAuthorizationMiddleware<
        TEntity,
        TAuthenticationContext,
        TUserContext
      >
    | undefined {
    return this.provider.resolveEntityService<
      IEntityAuthorizationMiddleware<
        TEntity,
        TAuthenticationContext,
        TUserContext
      >
    >(EntityServices.Authorization.IEntityAuthorizationMiddleware, entityName, {
      optional: true,
    })
  }

  registerAuthorizationMiddleware<
    TEntity,
    TAuthenticationContext extends IAuthenticationContext<TUserContext>,
    TUserContext
  >(
    entityName: string,
    instance: IEntityAuthorizationMiddleware<
      TEntity,
      TAuthenticationContext,
      TUserContext
    >
  ) {
    this.provider.registerEntityService<
      IEntityAuthorizationMiddleware<
        TEntity,
        TAuthenticationContext,
        TUserContext
      >
    >(
      EntityServices.Authorization.IEntityAuthorizationMiddleware,
      entityName,
      instance
    )
  }

  resolveGetQuery<
    TEntity,
    TEntityId,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      unknown,
      unknown
    >
  >(entityName: string) {
    return this.provider.resolveEntityService<
      IEntityGetQuery<TEntity, TEntityId, TEntitySearchParameters>
    >(EntityServices.Queries.IEntityGetQuery, entityName)
  }

  registerGetQuery<
    TEntity,
    TEntityId,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      unknown,
      unknown
    >
  >(
    entityName: string,
    instance: IEntityGetQuery<TEntity, TEntityId, TEntitySearchParameters>
  ) {
    this.provider.registerEntityService<
      IEntityGetQuery<TEntity, TEntityId, TEntitySearchParameters>
    >(EntityServices.Queries.IEntityGetQuery, entityName, instance)
  }

  resolveSearchQuery<
    TEntity,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor,
    TFacets extends IEntityFacets
  >(
    entityName: string
  ): IEntitiesSearchQuery<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor,
    TFacets
  > {
    return this.provider.resolveEntityService<
      IEntitiesSearchQuery<
        TEntity,
        TEntitySearchParameters,
        TSorting,
        TCursor,
        TFacets
      >
    >(EntityServices.Queries.IEntitiesSearchQuery, entityName)
  }

  registerSearchQuery<
    TEntity,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor,
    TFacets extends IEntityFacets
  >(
    entityName: string,
    instance: IEntitiesSearchQuery<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets
    >
  ) {
    this.provider.registerEntityService<
      IEntitiesSearchQuery<
        TEntity,
        TEntitySearchParameters,
        TSorting,
        TCursor,
        TFacets
      >
    >(EntityServices.Queries.IEntitiesSearchQuery, entityName, instance)
  }

  resolveFindQuery<
    TEntity,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor
  >(
    entityName: string
  ): IEntitiesFindQuery<TEntity, TEntitySearchParameters, TSorting, TCursor> {
    return this.provider.resolveEntityService<
      IEntitiesFindQuery<TEntity, TEntitySearchParameters, TSorting, TCursor>
    >(EntityServices.Queries.IEntitiesFindQuery, entityName)
  }

  registerFindQuery<
    TEntity,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor
  >(
    entityName: string,
    instance: IEntitiesFindQuery<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor
    >
  ) {
    this.provider.registerEntityService<
      IEntitiesFindQuery<TEntity, TEntitySearchParameters, TSorting, TCursor>
    >(EntityServices.Queries.IEntitiesFindQuery, entityName, instance)
  }

  resolveExistsQuery<TEntity, TEntityFilters extends ISearchFilters>(
    entityName: string
  ) {
    return this.provider.resolveEntityService<
      IEntityExistsQuery<TEntity, TEntityFilters>
    >(EntityServices.Queries.IEntityExistsQuery, entityName)
  }

  registerExistsQuery<TEntity, TEntityFilters extends ISearchFilters>(
    entityName: string,
    instance: IEntityExistsQuery<TEntity, TEntityFilters>
  ) {
    this.provider.registerEntityService<
      IEntityExistsQuery<TEntity, TEntityFilters>
    >(EntityServices.Queries.IEntityExistsQuery, entityName, instance)
  }

  resolveCountQuery<TEntity, TEntityFilters extends ISearchFilters>(
    entityName: string
  ) {
    return this.provider.resolveEntityService<
      IEntitiesCountQuery<TEntity, TEntityFilters>
    >(EntityServices.Queries.IEntityCountQuery, entityName)
  }

  registerCountQuery<TEntity, TEntityFilters extends ISearchFilters>(
    entityName: string,
    instance: IEntitiesCountQuery<TEntity, TEntityFilters>
  ) {
    this.provider.registerEntityService<
      IEntitiesCountQuery<TEntity, TEntityFilters>
    >(EntityServices.Queries.IEntityCountQuery, entityName, instance)
  }

  resolveVersionsSearchQuery<TEntity, TCursor>(entityName: string) {
    return this.provider.resolveEntityService<
      IEntityVersionsSearchQuery<TEntity, TCursor>
    >(EntityServices.Queries.IEntityVersionsSearchQuery, entityName)
  }

  registerVersionsSearchQuery<TEntity, TCursor>(
    entityName: string,
    instance: IEntityVersionsSearchQuery<TEntity, TCursor>
  ) {
    this.provider.registerEntityService<
      IEntityVersionsSearchQuery<TEntity, TCursor>
    >(EntityServices.Queries.IEntityVersionsSearchQuery, entityName, instance)
  }

  resolveCreateCommand<TEntity, TEntityId, TEntityCreateData>(
    entityName: string
  ): IEntityCreateCommand<TEntity, TEntityId, TEntityCreateData> {
    return this.provider.resolveEntityService<
      IEntityCreateCommand<TEntity, TEntityId, TEntityCreateData>
    >(EntityServices.Commands.IEntityCreateCommand, entityName)
  }

  registerCreateCommand<TEntity, TEntityId, TEntityCreateData>(
    entityName: string,
    instance: IEntityCreateCommand<TEntity, TEntityId, TEntityCreateData>
  ) {
    this.provider.registerEntityService<
      IEntityCreateCommand<TEntity, TEntityId, TEntityCreateData>
    >(EntityServices.Commands.IEntityCreateCommand, entityName, instance)
  }

  resolveUpdateCommand<TEntity, TEntityId, TEntityUpdateData>(
    entityName: string
  ): IEntityUpdateCommand<TEntity, TEntityId, TEntityUpdateData> {
    return this.provider.resolveEntityService<
      IEntityUpdateCommand<TEntity, TEntityId, TEntityUpdateData>
    >(EntityServices.Commands.IEntityUpdateCommand, entityName)
  }

  registerUpdateCommand<TEntity, TEntityId, TEntityUpdateData>(
    entityName: string,
    instance: IEntityUpdateCommand<TEntity, TEntityId, TEntityUpdateData>
  ) {
    this.provider.registerEntityService<
      IEntityUpdateCommand<TEntity, TEntityId, TEntityUpdateData>
    >(EntityServices.Commands.IEntityUpdateCommand, entityName, instance)
  }

  resolveUpsertCommand<TEntity, TEntityId, TEntityUpdateData>(
    entityName: string
  ): IEntityUpsertCommand<TEntity, TEntityId, TEntityUpdateData> {
    return this.provider.resolveEntityService<
      IEntityUpsertCommand<TEntity, TEntityId, TEntityUpdateData>
    >(EntityServices.Commands.IEntityUpsertCommand, entityName)
  }

  registerUpsertCommand<TEntity, TEntityId, TEntityUpdateData>(
    entityName: string,
    instance: IEntityUpsertCommand<TEntity, TEntityId, TEntityUpdateData>
  ) {
    this.provider.registerEntityService<
      IEntityUpsertCommand<TEntity, TEntityId, TEntityUpdateData>
    >(EntityServices.Commands.IEntityUpsertCommand, entityName, instance)
  }

  registerUpsertByCommand<TEntity, TEntityFilters extends ISearchFilters>(
    entityName: string,
    instance: IEntityUpsertByCommand<TEntity, TEntityFilters>
  ) {
    this.provider.registerEntityService<
      IEntityUpsertByCommand<TEntity, TEntityFilters>
    >(EntityServices.Commands.IEntityUpsertByCommand, entityName, instance)
  }

  resolveUpsertByCommand<TEntity, TEntityFilters extends ISearchFilters>(
    entityName: string
  ): IEntityUpsertByCommand<TEntity, TEntityFilters> {
    return this.provider.resolveEntityService<
      IEntityUpsertByCommand<TEntity, TEntityFilters>
    >(EntityServices.Commands.IEntityUpsertByCommand, entityName)
  }

  resolveDeleteItemsCommand<
    TEntity,
    TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
    TSorting
  >(
    entityName: string
  ): IEntitiesDeleteCommand<TEntity, TEntitiesDeleteParameters, TSorting> {
    return this.provider.resolveEntityService<
      IEntitiesDeleteCommand<TEntity, TEntitiesDeleteParameters, TSorting>
    >(EntityServices.Commands.IEntitiesDeleteCommand, entityName)
  }

  registerDeleteItemsCommand<
    TEntity,
    TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
    TSorting
  >(
    entityName: string,
    instance: IEntitiesDeleteCommand<
      TEntity,
      TEntitiesDeleteParameters,
      TSorting
    >
  ) {
    this.provider.registerEntityService<
      IEntitiesDeleteCommand<TEntity, TEntitiesDeleteParameters, TSorting>
    >(EntityServices.Commands.IEntitiesDeleteCommand, entityName, instance)
  }

  resolveDeleteCommand<TEntity, TEntityId>(
    entityName: string
  ): IEntityDeleteCommand<TEntity, TEntityId> {
    return this.provider.resolveEntityService<
      IEntityDeleteCommand<TEntity, TEntityId>
    >(EntityServices.Commands.IEntityDeleteCommand, entityName)
  }

  registerDeleteCommand<TEntity, TEntityId>(
    entityName: string,
    instance: IEntityDeleteCommand<TEntity, TEntityId>
  ) {
    this.provider.registerEntityService<
      IEntityDeleteCommand<TEntity, TEntityId>
    >(EntityServices.Commands.IEntityDeleteCommand, entityName, instance)
  }

  resolveVersionCommand<TEntity, TEntityId>(
    entityName: string
  ): IEntityVersionCommand<TEntity, TEntityId> {
    return this.provider.resolveEntityService<
      IEntityVersionCommand<TEntity, TEntityId>
    >(EntityServices.Commands.IEntityVersionCommand, entityName)
  }

  registerVersionCommand<TEntity, TEntityId>(
    entityName: string,
    instance: IEntityVersionCommand<TEntity, TEntityId>
  ) {
    this.provider.registerEntityService<
      IEntityVersionCommand<TEntity, TEntityId>
    >(EntityServices.Commands.IEntityVersionCommand, entityName, instance)
  }

  resolveExportCommand<
    TEntity,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor
  >(
    entityName: string
  ): IEntitiesExportCommand<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor
  > {
    return this.provider.resolveEntityService<
      IEntitiesExportCommand<
        TEntity,
        TEntitySearchParameters,
        TSorting,
        TCursor
      >
    >(EntityServices.Commands.IEntitiesExportCommand, entityName)
  }

  registerExportCommand<
    TEntity,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor
  >(
    entityName: string,
    instance: IEntitiesExportCommand<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor
    >
  ) {
    this.provider.registerEntityService<
      IEntitiesExportCommand<
        TEntity,
        TEntitySearchParameters,
        TSorting,
        TCursor
      >
    >(EntityServices.Commands.IEntitiesExportCommand, entityName, instance)
  }

  resolveImportCommand<TEntity>(
    entityName: string
  ): IEntitiesImportCommand<TEntity> {
    return this.provider.resolveEntityService<IEntitiesImportCommand<TEntity>>(
      EntityServices.Commands.IEntitiesImportCommand,
      entityName
    )
  }

  registerImportCommand<TEntity>(
    entityName: string,
    instance: IEntitiesImportCommand<TEntity>
  ) {
    this.provider.registerEntityService<IEntitiesImportCommand<TEntity>>(
      EntityServices.Commands.IEntitiesImportCommand,
      entityName,
      instance
    )
  }

  resolveParseCommand<TEntity>(
    entityName: string
  ): IEntitiesParseCommand<TEntity> {
    return this.provider.resolveEntityService<IEntitiesParseCommand<TEntity>>(
      EntityServices.Commands.IEntitiesParseCommand,
      entityName
    )
  }

  registerParseCommand<TEntity>(
    entityName: string,
    instance: IEntitiesParseCommand<TEntity>
  ) {
    this.provider.registerEntityService<IEntitiesParseCommand<TEntity>>(
      EntityServices.Commands.IEntitiesParseCommand,
      entityName,
      instance
    )
  }

  resolveSampleDownloadCommand<TEntity>(
    entityName: string
  ): IEntitiesSampleDownloadCommand<TEntity> {
    return this.provider.resolveEntityService<
      IEntitiesSampleDownloadCommand<TEntity>
    >(EntityServices.Commands.IEntitiesSampleDownloadCommand, entityName)
  }

  registerSampleDownloadCommand<TEntity>(
    entityName: string,
    instance: IEntitiesSampleDownloadCommand<TEntity>
  ) {
    this.provider.registerEntityService<
      IEntitiesSampleDownloadCommand<TEntity>
    >(
      EntityServices.Commands.IEntitiesSampleDownloadCommand,
      entityName,
      instance
    )
  }

  resolveGetAction<
    TEntity,
    TEntityId,
    TEntityDto,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      unknown,
      unknown
    >
  >(
    entityName: string
  ): IEntityGetAction<TEntity, TEntityId, TEntityDto, TEntitySearchParameters> {
    return this.provider.resolveEntityService<
      IEntityGetAction<TEntity, TEntityId, TEntityDto, TEntitySearchParameters>
    >(EntityServices.Actions.IEntityGetAction, entityName)
  }

  registerGetAction<
    TEntity,
    TEntityId,
    TEntityDto,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      unknown,
      unknown
    >
  >(
    entityName: string,
    instance: IEntityGetAction<
      TEntity,
      TEntityId,
      TEntityDto,
      TEntitySearchParameters
    >
  ) {
    this.provider.registerEntityService<
      IEntityGetAction<TEntity, TEntityId, TEntityDto, TEntitySearchParameters>
    >(EntityServices.Actions.IEntityGetAction, entityName, instance)
  }

  resolveExistsAction<TEntity, TEntityFilters extends ISearchFilters>(
    entityName: string
  ) {
    return this.provider.resolveEntityService<
      IEntityExistsAction<TEntity, TEntityFilters>
    >(EntityServices.Actions.IEntityExistsAction, entityName)
  }

  registerExistsAction<TEntity, TEntityFilters extends ISearchFilters>(
    entityName: string,
    instance: IEntityExistsAction<TEntity, TEntityFilters>
  ) {
    this.provider.registerEntityService<
      IEntityExistsAction<TEntity, TEntityFilters>
    >(EntityServices.Actions.IEntityExistsAction, entityName, instance)
  }

  resolveCountAction<TEntity, TEntityFilters extends ISearchFilters>(
    entityName: string
  ) {
    return this.provider.resolveEntityService<
      IEntitiesCountAction<TEntity, TEntityFilters>
    >(EntityServices.Actions.IEntitiesCountAction, entityName)
  }

  registerCountAction<TEntity, TEntityFilters extends ISearchFilters>(
    entityName: string,
    instance: IEntitiesCountAction<TEntity, TEntityFilters>
  ) {
    this.provider.registerEntityService<
      IEntitiesCountAction<TEntity, TEntityFilters>
    >(EntityServices.Actions.IEntitiesCountAction, entityName, instance)
  }

  resolveSearchAction<
    TEntity,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TEntityListItemDto,
    TSorting extends SortingType,
    TCursor,
    TFacets extends IEntityFacets
  >(
    entityName: string
  ): IEntitiesSearchAction<
    TEntity,
    TEntitySearchParameters,
    TEntityListItemDto,
    TSorting,
    TCursor,
    TFacets
  > {
    return this.provider.resolveEntityService<
      IEntitiesSearchAction<
        TEntity,
        TEntitySearchParameters,
        TEntityListItemDto,
        TSorting,
        TCursor,
        TFacets
      >
    >(EntityServices.Actions.IEntitiesSearchAction, entityName)
  }

  registerSearchAction<
    TEntity,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TEntityListItemDto,
    TSorting extends SortingType,
    TCursor,
    TFacets extends IEntityFacets
  >(
    entityName: string,
    instance: IEntitiesSearchAction<
      TEntity,
      TEntitySearchParameters,
      TEntityListItemDto,
      TSorting,
      TCursor,
      TFacets
    >
  ) {
    this.provider.registerEntityService<
      IEntitiesSearchAction<
        TEntity,
        TEntitySearchParameters,
        TEntityListItemDto,
        TSorting,
        TCursor,
        TFacets
      >
    >(EntityServices.Actions.IEntitiesSearchAction, entityName, instance)
  }

  resolveVersionsSearchAction<TEntity, TEntityListItemDto, TCursor>(
    entityName: string
  ) {
    return this.provider.resolveEntityService<
      IEntityVersionsSearchAction<TEntity, TEntityListItemDto, TCursor>
    >(EntityServices.Actions.IEntityVersionsSearchAction, entityName)
  }

  registerVersionsSearchAction<TEntity, TEntityListItemDto, TCursor>(
    entityName: string,
    instance: IEntityVersionsSearchAction<TEntity, TEntityListItemDto, TCursor>
  ) {
    this.provider.registerEntityService<
      IEntityVersionsSearchAction<TEntity, TEntityListItemDto, TCursor>
    >(EntityServices.Actions.IEntityVersionsSearchAction, entityName, instance)
  }

  resolveCreateAction<TEntity, TEntityCreateData, TEntityDto>(
    entityName: string
  ): IEntityCreateAction<TEntity, TEntityCreateData, TEntityDto> {
    return this.provider.resolveEntityService<
      IEntityCreateAction<TEntity, TEntityCreateData, TEntityDto>
    >(EntityServices.Actions.IEntityCreateAction, entityName)
  }

  registerCreateAction<TEntity, TEntityCreateData, TEntityDto>(
    entityName: string,
    instance: IEntityCreateAction<TEntity, TEntityCreateData, TEntityDto>
  ) {
    this.provider.registerEntityService<
      IEntityCreateAction<TEntity, TEntityCreateData, TEntityDto>
    >(EntityServices.Actions.IEntityCreateAction, entityName, instance)
  }

  resolveUpdateAction<TEntity, TEntityId, TEntityUpdateData, TEntityDto>(
    entityName: string
  ): IEntityUpdateAction<TEntity, TEntityId, TEntityUpdateData, TEntityDto> {
    return this.provider.resolveEntityService<
      IEntityUpdateAction<TEntity, TEntityId, TEntityUpdateData, TEntityDto>
    >(EntityServices.Actions.IEntityUpdateAction, entityName)
  }

  registerUpdateAction<TEntity, TEntityId, TEntityUpdateData, TEntityDto>(
    entityName: string,
    instance: IEntityUpdateAction<
      TEntity,
      TEntityId,
      TEntityUpdateData,
      TEntityDto
    >
  ) {
    this.provider.registerEntityService<
      IEntityUpdateAction<TEntity, TEntityId, TEntityUpdateData, TEntityDto>
    >(EntityServices.Actions.IEntityUpdateAction, entityName, instance)
  }

  resolveUpsertAction<TEntity, TEntityId, TEntityUpdateData, TEntityDto>(
    entityName: string
  ): IEntityUpsertAction<TEntity, TEntityId, TEntityUpdateData, TEntityDto> {
    return this.provider.resolveEntityService<
      IEntityUpsertAction<TEntity, TEntityId, TEntityUpdateData, TEntityDto>
    >(EntityServices.Actions.IEntityUpsertAction, entityName)
  }

  registerUpsertAction<TEntity, TEntityId, TEntityUpdateData, TEntityDto>(
    entityName: string,
    instance: IEntityUpsertAction<
      TEntity,
      TEntityId,
      TEntityUpdateData,
      TEntityDto
    >
  ) {
    this.provider.registerEntityService<
      IEntityUpsertAction<TEntity, TEntityId, TEntityUpdateData, TEntityDto>
    >(EntityServices.Actions.IEntityUpsertAction, entityName, instance)
  }

  resolveDeleteAction<TEntity, TEntityId>(
    entityName: string
  ): IEntityDeleteAction<TEntity, TEntityId> {
    return this.provider.resolveEntityService<
      IEntityDeleteAction<TEntity, TEntityId>
    >(EntityServices.Actions.IEntityDeleteAction, entityName)
  }

  registerDeleteAction<TEntity, TEntityId>(
    entityName: string,
    instance: IEntityDeleteAction<TEntity, TEntityId>
  ) {
    this.provider.registerEntityService<
      IEntityDeleteAction<TEntity, TEntityId>
    >(EntityServices.Actions.IEntityDeleteAction, entityName, instance)
  }

  resolveDeleteItemsAction<
    TEntity,
    TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
    TSorting
  >(
    entityName: string
  ): IEntitiesDeleteAction<TEntity, TEntitiesDeleteParameters, TSorting> {
    return this.provider.resolveEntityService<
      IEntitiesDeleteAction<TEntity, TEntitiesDeleteParameters, TSorting>
    >(EntityServices.Actions.IEntitiesDeleteAction, entityName)
  }

  registerDeleteItemsAction<
    TEntity,
    TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
    TSorting
  >(
    entityName: string,
    instance: IEntitiesDeleteAction<
      TEntity,
      TEntitiesDeleteParameters,
      TSorting
    >
  ) {
    this.provider.registerEntityService<
      IEntitiesDeleteAction<TEntity, TEntitiesDeleteParameters, TSorting>
    >(EntityServices.Actions.IEntitiesDeleteAction, entityName, instance)
  }

  resolveExportAction<
    TEntity,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor
  >(
    entityName: string
  ): IEntitiesExportAction<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor
  > {
    return this.provider.resolveEntityService<
      IEntitiesExportAction<TEntity, TEntitySearchParameters, TSorting, TCursor>
    >(EntityServices.Actions.IEntitiesExportAction, entityName)
  }

  registerExportAction<
    TEntity,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor
  >(
    entityName: string,
    instance: IEntitiesExportAction<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor
    >
  ) {
    this.provider.registerEntityService<
      IEntitiesExportAction<TEntity, TEntitySearchParameters, TSorting, TCursor>
    >(EntityServices.Actions.IEntitiesExportAction, entityName, instance)
  }

  resolveSampleDownloadAction<TEntity>(
    entityName: string
  ): IEntitiesSampleDownloadAction<TEntity> {
    return this.provider.resolveEntityService<
      IEntitiesSampleDownloadAction<TEntity>
    >(EntityServices.Actions.IEntitiesSampleDownloadAction, entityName)
  }

  registerSampleDownloadAction<TEntity>(
    entityName: string,
    instance: IEntitiesSampleDownloadAction<TEntity>
  ) {
    this.provider.registerEntityService<IEntitiesSampleDownloadAction<TEntity>>(
      EntityServices.Actions.IEntitiesSampleDownloadAction,
      entityName,
      instance
    )
  }

  resolveImportAction<TEntity>(
    entityName: string
  ): IEntitiesImportAction<TEntity> {
    return this.provider.resolveEntityService<IEntitiesImportAction<TEntity>>(
      EntityServices.Actions.IEntitiesImportAction,
      entityName
    )
  }

  registerImportAction<TEntity>(
    entityName: string,
    instance: IEntitiesImportAction<TEntity>
  ) {
    this.provider.registerEntityService<IEntitiesImportAction<TEntity>>(
      EntityServices.Actions.IEntitiesImportAction,
      entityName,
      instance
    )
  }

  resolveParseAction<TEntity>(
    entityName: string
  ): IEntitiesParseAction<TEntity> {
    return this.provider.resolveEntityService<IEntitiesParseAction<TEntity>>(
      EntityServices.Actions.IEntitiesParseAction,
      entityName
    )
  }

  registerParseAction<TEntity>(
    entityName: string,
    instance: IEntitiesParseAction<TEntity>
  ) {
    this.provider.registerEntityService<IEntitiesParseAction<TEntity>>(
      EntityServices.Actions.IEntitiesParseAction,
      entityName,
      instance
    )
  }

  resolveQueryBuilder<
    TEntity,
    TEntityId,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor,
    TFacets extends IEntityFacets,
    TUserContext
  >(
    entityName: string
  ): IEntitiesQueryBuilder<
    TEntity,
    TEntityId,
    TEntitySearchParameters,
    TSorting,
    TCursor,
    TFacets,
    TUserContext
  > {
    return this.provider.resolveEntityService<
      IEntitiesQueryBuilder<
        TEntity,
        TEntityId,
        TEntitySearchParameters,
        TSorting,
        TCursor,
        TFacets,
        TUserContext
      >
    >(EntityServices.Queries.IEntitiesQueryBuilder, entityName)
  }

  registerQueryBuilder<
    TEntity,
    TEntityId,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor,
    TFacets extends IEntityFacets,
    TUserContext
  >(
    entityName: string,
    instance: IEntitiesQueryBuilder<
      TEntity,
      TEntityId,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets,
      TUserContext
    >
  ) {
    this.provider.registerEntityService<
      IEntitiesQueryBuilder<
        TEntity,
        TEntityId,
        TEntitySearchParameters,
        TSorting,
        TCursor,
        TFacets,
        TUserContext
      >
    >(EntityServices.Queries.IEntitiesQueryBuilder, entityName, instance)
  }
}
