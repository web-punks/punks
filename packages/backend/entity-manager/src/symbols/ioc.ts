export const EntityServices = {
  Actions: {
    IEntityGetAction: "IEntityGetAction",
    IEntityExistsAction: "IEntityExistsAction",
    IEntitiesCountAction: "IEntitiesCountAction",
    IEntitiesDeleteAction: "IEntitiesDeleteAction",
    IEntitiesSearchAction: "IEntitiesSearchAction",
    IEntityCreateAction: "IEntityCreateAction",
    IEntityUpdateAction: "IEntityUpdateAction",
    IEntityUpsertAction: "IEntityUpsertAction",
    IEntityDeleteAction: "IEntityDeleteAction",
    IEntityMergeAction: "IEntityMergeAction",
    IEntitiesExportAction: "IEntitiesExportAction",
    IEntitiesParseAction: "IEntitiesParseAction",
    IEntitiesImportAction: "IEntitiesImportAction",
    IEntitiesSampleDownloadAction: "IEntitiesSampleDownloadAction",
    IEntityVersionsSearchAction: "IEntityVersionsSearchAction",
  },
  Authorization: {
    IEntityAuthorizationMiddleware: "IEntityAuthorizationMiddleware",
  },
  Commands: {
    IEntityCreateCommand: "IEntityCreateCommand",
    IEntityUpdateCommand: "IEntityUpdateCommand",
    IEntityUpsertCommand: "IEntityUpsertCommand",
    IEntityUpsertByCommand: "IEntityUpsertByCommand",
    IEntityMergeCommand: "IEntityMergeCommand",
    IEntityDeleteCommand: "IEntityDeleteCommand",
    IEntityVersionCommand: "IEntityVersionCommand",
    IEntitiesDeleteCommand: "IEntitiesDeleteCommand",
    IEntitiesExportCommand: "IEntitiesExportCommand",
    IEntitiesImportCommand: "IEntitiesImportCommand",
    IEntitiesParseCommand: "IEntitiesParseCommand",
    IEntitiesSampleDownloadCommand: "IEntitiesSampleDownloadCommand",
  },
  Converters: {
    IEntityAdapter: "IEntityAdapter",
    IEntityConverter: "IEntityConverter",
    IEntitySerializer: "IEntitySerializer",
    IEntitySnapshotService: "IEntitySnapshotService",
  },
  Connectors: {
    IConnectorsConfiguration: "IConnectorsConfiguration",
    IEntityConnectorDeleteManager: "IEntityConnectorDeleteManager",
    IEntityConnectorSyncManager: "IEntityConnectorSyncManager",
  },
  Events: {
    IEntityEventsManager: "IEntityEventsManager",
  },
  Queries: {
    IEntityCountQuery: "IEntityCountQuery",
    IEntityExistsQuery: "IEntityExistsQuery",
    IEntityGetQuery: "IEntityGetQuery",
    IEntitiesFindQuery: "IEntitiesFindQuery",
    IEntitiesSearchQuery: "IEntitiesSearchQuery",
    IEntitiesQueryBuilder: "IEntitiesQueryBuilder",
    IEntityVersionsSearchQuery: "IEntityVersionsSearchQuery",
  },
  Replication: {
    IReplicasConfiguration: "IReplicasConfiguration",
    IEntityReplicaDeleteManager: "IEntityReplicaDeleteManager",
    IEntityReplicaSyncManager: "IEntityReplicaSyncManager",
  },
  Services: {
    IEntityActions: "IEntityActions",
    IEntityManager: "IEntityManager",
  },
  Storage: {
    IEntityRepository: "IEntityRepository",
  },
  Settings: {
    IEntityConfiguration: "IEntityConfiguration",
  },
}

export const GlobalServices = {
  Files: {
    IFilesReferenceRepo: "IFilesReferenceRepo",
  },
  Events: {
    IEventEmitter: "IEventEmitter",
    IEventsTracker: "IEventsTracker",
  },
  Authentication: {
    IAuthenticationContextProvider: "IAuthenticationContextProvider",
    IAuthenticationMiddleware: "IAuthenticationMiddleware",
  },
  Operations: {
    IOperationLockService: "IOperationLockService",
  },
  Pipelines: {
    IPipelineController: "IPipelineController",
  },
  Plugins: {
    ICacheInstance: "ICacheInstance",
    IEmailProvider: "IEmailProvider",
    IEmailLogger: "IEmailLogger",
    IEmailTemplateMiddleware: "IEmailTemplateMiddleware",
    IEmailTemplatesCollection: "IEmailTemplatesCollection",
    IBucketProvider: "IBucketProvider",
    ISecretsProvider: "ISecretsProvider",
    IFileProvider: "IFileProvider",
    IMediaProvider: "IMediaProvider",
    IMediaFolderRepository: "IMediaFolderRepository",
    IMediaReferenceRepository: "IMediaReferenceRepository",
  },
  Settings: {
    EntityManagerSettings: "EntityManagerSettings",
  },
  Versioning: {
    IEntityVersioningProvider: "IEntityVersioningProvider",
  },
}
