import {
  IReplicasConfiguration,
  ReplicaConfiguration,
  ReplicaOptions,
} from "../abstractions/replication"
import { IEntityRepository } from "../abstractions/repository"

export class ReplicasConfiguration<TEntity, TEntityId>
  implements IReplicasConfiguration<TEntity, TEntityId>
{
  private readonly replicas = {} as Record<
    string,
    ReplicaConfiguration<TEntity, TEntityId>
  >

  configureReplica<
    TRepository extends IEntityRepository<
      TEntity,
      TEntityId,
      unknown,
      unknown,
      unknown,
      unknown
    >
  >(name: string, options: ReplicaOptions, repository: TRepository) {
    if (this.replicas[name]) {
      throw new Error(`Replica with name ${name} already configured`)
    }

    this.replicas[name] = {
      name,
      options,
      repository,
    }
  }

  getReplica(name: string): ReplicaConfiguration<TEntity, TEntityId> {
    return this.replicas[name]
  }

  getReplicas(): ReplicaConfiguration<TEntity, TEntityId>[] {
    return Object.values(this.replicas)
  }
}
