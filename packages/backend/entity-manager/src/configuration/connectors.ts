import {
  ConnectorConfiguration,
  ConnectorOptions,
  IConnectorsConfiguration,
  IEntityConnector,
} from "../abstractions/connectors"
import { IEntityMapper } from "../abstractions/mappers"

export class ConnectorsConfiguration<TEntity>
  implements IConnectorsConfiguration<TEntity>
{
  private readonly connectors = {} as Record<
    string,
    ConnectorConfiguration<TEntity>
  >

  configureConnector<
    TEntityConnector extends IEntityConnector<TEntity, unknown, TMappedType>,
    TMappedType,
    TMapper extends IEntityMapper<TEntity, TMappedType>
  >(
    name: string,
    options: ConnectorOptions,
    connector: TEntityConnector,
    mapper: TMapper
  ) {
    if (this.connectors[name]) {
      throw new Error(`Connector with name ${name} already configured`)
    }

    this.connectors[name] = {
      name,
      options,
      connector,
      mapper,
    }
  }

  getConnector(name: string): ConnectorConfiguration<TEntity> {
    return this.connectors[name]
  }
  getConnectors(): ConnectorConfiguration<TEntity>[] {
    return Object.values(this.connectors)
  }
}
