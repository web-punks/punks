import {
  EntitiesImportExportSettings,
  IAuthenticationContext,
  IAuthenticationContextProvider,
  IEntityFacets,
  IEntitySerializer,
  IEntitySnapshotService,
} from "../abstractions"
import { IEntityAdapter } from "../abstractions/adapters"
import { IEntityAuthorizationMiddleware } from "../abstractions/authorization"
import { EnumType, SortingType } from "../abstractions/common"
import {
  EntitiesMapOperationsInput,
  IEntityManagerServiceCollection,
  IEntityManagerServiceRoot,
} from "../abstractions/configuration"
import { ConnectorOptions, IEntityConnector } from "../abstractions/connectors"
import { IEntityConverter } from "../abstractions/converters"
import { IEntityMapper } from "../abstractions/mappers"
import { IEntitiesQueryBuilder } from "../abstractions/queries"
import { ReplicaOptions } from "../abstractions/replication"
import { IEntityRepository } from "../abstractions/repository"
import {
  IEntitySearchParameters,
  ISearchFilters,
} from "../abstractions/searchParameters"
import { IEntityConfiguration } from "../abstractions/settings"
import { EntitiesCountAction } from "../actions/count"
import { EntityCreateAction } from "../actions/create"
import { EntityDeleteAction } from "../actions/delete"
import { EntitiesDeleteAction } from "../actions/deleteItems"
import { EntityExistsAction } from "../actions/exists"
import { EntitiesExportAction } from "../actions/export"
import { EntityGetAction } from "../actions/get"
import { EntitiesImportAction } from "../actions/import"
import { EntitiesParseAction } from "../actions/parse"
import { EntitiesSampleDownloadAction } from "../actions/sampleDownload"
import { EntitiesSearchAction } from "../actions/search"
import { EntityUpdateAction } from "../actions/update"
import { EntityUpsertAction } from "../actions/upsert"
import { EntityVersionsSearchAction } from "../actions/versions"
import { EntityCreateCommand } from "../commands/create"
import { EntityDeleteCommand } from "../commands/delete"
import { EntitiesDeleteCommand } from "../commands/deleteItems"
import { EntitiesExportCommand } from "../commands/export"
import { EntitiesImportCommand } from "../commands/import"
import { EntitiesParseCommand } from "../commands/parse"
import { EntitiesSampleDownloadCommand } from "../commands/sampleDownload"
import { EntityUpdateCommand } from "../commands/update"
import { EntityUpsertCommand } from "../commands/upsert"
import { EntitiesUpsertByCommand } from "../commands/upsertBy"
import { EntityVersionCommand } from "../commands/version"
import { EntityActions, EntityManager } from "../concrete"
import { ServiceLocator } from "../ioc"
import {
  EntitiesServiceLocator,
  EntityServiceLocator,
} from "../providers/services"
import { EntitiesCountQuery } from "../queries/count"
import { EntityExistsQuery } from "../queries/exists"
import { EntitiesFindQuery } from "../queries/find"
import { EntityGetQuery } from "../queries/get"
import { EntitiesSearchQuery } from "../queries/search"
import { EntityVersionsSearchQuery } from "../queries/versions"
import { EntityConnectorsManager } from "../services/connectors"
import { EntityEventsManager } from "../services/events"
import { EntityReplicaManager } from "../services/replication"
import { ConnectorsConfiguration } from "./connectors"
import { ReplicasConfiguration } from "./replicas"

class EntityManagerServiceRoot implements IEntityManagerServiceRoot {
  private readonly services = new ServiceLocator()

  get locator() {
    return this.services
  }

  getEntitiesServicesLocator() {
    return new EntitiesServiceLocator(this.services)
  }

  getEntityServicesLocator<TEntity, TEntityId>(entityName: string) {
    return new EntityServiceLocator<TEntity, TEntityId>(
      this.getEntitiesServicesLocator(),
      entityName
    )
  }

  addAuthentication<
    TAuthenticationContextProvider extends IAuthenticationContextProvider<TUserContext>,
    TUserContext
  >({ provider }: { provider: TAuthenticationContextProvider }) {
    this.getEntitiesServicesLocator().registerAuthenticationContextProvider(
      provider
    )
  }

  registerEntity<
    TEntity,
    TEntityId,
    TRepository extends IEntityRepository<
      TEntity,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown
    >
  >(
    entity: IEntityConfiguration,
    repository: TRepository
  ): IEntityManagerServiceCollection<TEntity, TEntityId> {
    const { name: entityName } = entity

    const collection = new EntityManagerServiceCollection<TEntity, TEntityId>(
      entityName,
      this.getEntitiesServicesLocator()
    )

    const services = this.getEntityServicesLocator<TEntity, TEntityId>(
      entityName
    )

    collection.locator.registerEntityManager<TEntity>(
      entityName,
      new EntityManager(services)
    )

    collection.locator.registerEntityActions<TEntity>(
      entityName,
      new EntityActions(services)
    )

    collection.locator.registerEntityConfiguration(entityName, entity)

    collection.locator.registerRepository<
      TEntity,
      TEntityId,
      unknown,
      unknown,
      unknown,
      unknown
    >(entityName, repository)

    const replicaManager = new EntityReplicaManager(services)
    collection.locator.registerReplicaSyncManager<TEntity>(
      entityName,
      replicaManager
    )
    collection.locator.registerReplicaDeleteManager<TEntity, TEntityId>(
      entityName,
      replicaManager
    )
    collection.locator.registerReplicaConfiguration<TEntity, TEntityId>(
      entityName,
      new ReplicasConfiguration()
    )

    const connectorsManager = new EntityConnectorsManager(services)
    collection.locator.registerConnectorSyncManager<TEntity>(
      entityName,
      connectorsManager
    )
    collection.locator.registerConnectorDeleteManager<TEntity, TEntityId>(
      entityName,
      connectorsManager
    )
    collection.locator.registerConnectorsConfiguration<TEntity>(
      entityName,
      new ConnectorsConfiguration()
    )

    collection.locator.registerEventsManager<TEntity, TEntityId>(
      entityName,
      new EntityEventsManager(entityName, services)
    )

    return collection
  }
}

class EntityManagerServiceCollection<TEntity, TEntityId>
  implements IEntityManagerServiceCollection<TEntity, TEntityId>
{
  private readonly resolver: EntityServiceLocator<TEntity, TEntityId>

  constructor(
    public readonly entityName: string,
    public readonly locator: EntitiesServiceLocator
  ) {
    this.resolver = new EntityServiceLocator<TEntity, TEntityId>(
      locator,
      entityName
    )
  }

  getServiceLocator(): EntityServiceLocator<TEntity, TEntityId> {
    return this.resolver
  }

  mapCrudOperations<
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends EnumType,
    TCursor,
    TFacets extends IEntityFacets,
    TEntitiesQueryBuilder extends IEntitiesQueryBuilder<
      TEntity,
      unknown,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets,
      unknown
    >
  >({
    queryBuilder,
    settings,
  }: EntitiesMapOperationsInput<TEntitiesQueryBuilder>): IEntityManagerServiceCollection<
    TEntity,
    TEntityId
  > {
    this.mapGet()
    this.mapCount()
    this.mapExists()
    this.mapSearch({
      queryBuilder,
    })
    this.mapCreate()
    this.mapUpdate()
    this.mapDelete()
    this.mapImportExport(settings.importExport)

    return this
  }

  mapGet<
    TEntityDto,
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      unknown,
      unknown
    >
  >(): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.locator.registerGetQuery(
      this.entityName,
      new EntityGetQuery<TEntity, TEntityId, TEntitySearchParameters>(
        this.resolver
      )
    )
    this.locator.registerGetAction(
      this.entityName,
      new EntityGetAction<
        TEntity,
        TEntityId,
        TEntityDto,
        TEntitySearchParameters
      >(this.resolver)
    )

    return this
  }

  mapCount<
    TEntityFilters extends ISearchFilters
  >(): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.locator.registerCountQuery(
      this.entityName,
      new EntitiesCountQuery<TEntity, TEntityFilters, unknown>(this.resolver)
    )
    this.locator.registerCountAction(
      this.entityName,
      new EntitiesCountAction<TEntity, TEntityFilters>(this.resolver)
    )

    return this
  }

  mapExists<
    TEntityFilters extends ISearchFilters
  >(): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.locator.registerExistsQuery(
      this.entityName,
      new EntityExistsQuery<TEntity, TEntityFilters, unknown>(this.resolver)
    )
    this.locator.registerExistsAction(
      this.entityName,
      new EntityExistsAction<TEntity, TEntityFilters>(this.resolver)
    )

    return this
  }

  mapSearch<
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends EnumType,
    TCursor,
    TFacets extends IEntityFacets,
    TEntitiesQueryBuilder extends IEntitiesQueryBuilder<
      TEntity,
      unknown,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets,
      unknown
    >
  >({
    queryBuilder,
  }: {
    queryBuilder: TEntitiesQueryBuilder
  }): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.locator.registerFindQuery(
      this.entityName,
      new EntitiesFindQuery(this.resolver)
    )
    this.locator.registerSearchQuery(
      this.entityName,
      new EntitiesSearchQuery(this.resolver)
    )
    this.locator.registerSearchAction(
      this.entityName,
      new EntitiesSearchAction(this.resolver)
    )
    this.locator.registerQueryBuilder(this.entityName, queryBuilder)

    this.locator.registerVersionsSearchQuery(
      this.entityName,
      new EntityVersionsSearchQuery(this.resolver)
    )
    this.locator.registerVersionsSearchAction(
      this.entityName,
      new EntityVersionsSearchAction(this.resolver)
    )

    return this
  }

  mapCreate<TEntityCreateData, TEntityDto>(): IEntityManagerServiceCollection<
    TEntity,
    TEntityId
  > {
    this.locator.registerCreateCommand(
      this.entityName,
      new EntityCreateCommand(this.resolver)
    )
    this.locator.registerCreateAction(
      this.entityName,
      new EntityCreateAction(this.resolver)
    )
    return this
  }

  mapImportExport<
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor
  >(
    settings: EntitiesImportExportSettings
  ): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.locator.registerVersionCommand(
      this.entityName,
      new EntityVersionCommand(this.resolver)
    )

    this.locator.registerExportCommand(
      this.entityName,
      new EntitiesExportCommand<
        TEntity,
        TEntitySearchParameters,
        TSorting,
        TCursor
      >(this.resolver, settings)
    )
    this.locator.registerExportAction(
      this.entityName,
      new EntitiesExportAction<
        TEntity,
        TEntitySearchParameters,
        TSorting,
        TCursor
      >(this.resolver)
    )

    this.locator.registerImportCommand(
      this.entityName,
      new EntitiesImportCommand<TEntity>(this.resolver, settings)
    )
    this.locator.registerParseCommand(
      this.entityName,
      new EntitiesParseCommand<TEntity>(this.resolver, settings)
    )

    this.locator.registerImportAction(
      this.entityName,
      new EntitiesImportAction<TEntity>(this.resolver)
    )
    this.locator.registerParseAction(
      this.entityName,
      new EntitiesParseAction<TEntity>(this.resolver)
    )

    this.locator.registerSampleDownloadCommand(
      this.entityName,
      new EntitiesSampleDownloadCommand<TEntity>(this.resolver, settings)
    )
    this.locator.registerSampleDownloadAction(
      this.entityName,
      new EntitiesSampleDownloadAction<TEntity>(this.resolver)
    )

    return this
  }

  mapUpdate<TEntityUpdateData, TEntityDto>(): IEntityManagerServiceCollection<
    TEntity,
    TEntityId
  > {
    this.locator.registerUpsertByCommand(
      this.entityName,
      new EntitiesUpsertByCommand(this.resolver)
    )
    this.locator.registerUpsertCommand(
      this.entityName,
      new EntityUpsertCommand(this.resolver)
    )
    this.locator.registerUpsertAction(
      this.entityName,
      new EntityUpsertAction(this.resolver)
    )
    this.locator.registerUpdateCommand(
      this.entityName,
      new EntityUpdateCommand(this.resolver)
    )
    this.locator.registerUpdateAction(
      this.entityName,
      new EntityUpdateAction(this.resolver)
    )
    return this
  }

  mapDelete(): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.locator.registerDeleteCommand(
      this.entityName,
      new EntityDeleteCommand<TEntity, TEntityId>(this.resolver)
    )
    this.locator.registerDeleteAction(
      this.entityName,
      new EntityDeleteAction<TEntity, TEntityId>(this.resolver)
    )
    this.locator.registerDeleteItemsCommand(
      this.entityName,
      new EntitiesDeleteCommand<TEntity, any, unknown, unknown>(this.resolver)
    )
    this.locator.registerDeleteItemsAction(
      this.entityName,
      new EntitiesDeleteAction<TEntity, any, unknown>(this.resolver)
    )
    return this
  }

  addAdapter<
    TEntityCreateData,
    TEntityUpdateData,
    TEntityAdapter extends IEntityAdapter<
      TEntity,
      TEntityCreateData,
      TEntityUpdateData
    >
  >(
    adapter: TEntityAdapter
  ): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.locator.registerAdapter(this.entityName, adapter)
    return this
  }

  addSerializer<
    TEntitySerializer extends IEntitySerializer<
      TEntity,
      TEntityId,
      unknown,
      unknown
    >
  >(
    serializer: TEntitySerializer
  ): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.locator.registerSerializer(this.entityName, serializer)
    return this
  }

  addSnapshotService<
    TEntitySnapshotService extends IEntitySnapshotService<TEntityId, unknown>
  >(
    service: TEntitySnapshotService
  ): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.locator.registerSnapshotService(this.entityName, service)
    return this
  }

  addConverter<
    TEntityDto,
    TListItemDto,
    TEntityCreateInput,
    TEntityUpdateInput,
    TEntityConverter extends IEntityConverter<
      TEntity,
      TEntityDto,
      TListItemDto,
      TEntityCreateInput,
      TEntityUpdateInput
    >
  >(
    converter: TEntityConverter
  ): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.locator.registerConverter(this.entityName, converter)
    return this
  }

  addAuthorization<
    TEntityAuthorizationMiddleware extends IEntityAuthorizationMiddleware<
      TEntity,
      TAuthenticationContext,
      TUserContext
    >,
    TAuthenticationContext extends IAuthenticationContext<TUserContext>,
    TUserContext
  >({
    middleware,
  }: {
    middleware: TEntityAuthorizationMiddleware
  }): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.locator.registerAuthorizationMiddleware(this.entityName, middleware)
    return this
  }

  withReplica<
    TRepository extends IEntityRepository<
      TEntity,
      TEntityId,
      unknown,
      unknown,
      unknown,
      unknown
    >
  >({
    name,
    options,
    repository,
  }: {
    name: string
    options: ReplicaOptions
    repository: TRepository
  }): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.resolver
      .resolveReplicaConfiguration()
      .configureReplica(name, options, repository)
    return this
  }

  withSynchronization<
    TMappedType,
    TMapper extends IEntityMapper<TEntity, TMappedType>,
    TEntityConnector extends IEntityConnector<TEntity, unknown, TMappedType>
  >({
    name,
    options,
    connector,
    mapper,
  }: {
    name: string
    options: ConnectorOptions
    connector: TEntityConnector
    mapper: TMapper
  }): IEntityManagerServiceCollection<TEntity, TEntityId> {
    this.resolver
      .resolveConnectorsConfiguration()
      .configureConnector<TEntityConnector, TMappedType, any>(
        name,
        options,
        connector,
        mapper
      )
    return this
  }
}

export const createContainer = (): IEntityManagerServiceRoot =>
  new EntityManagerServiceRoot()
