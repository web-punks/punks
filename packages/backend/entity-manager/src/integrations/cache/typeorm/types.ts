export interface ICacheDatabaseItem {
  id: string
  instance: string
  key: string
  data: any
  createdOn: Date
  updatedOn: Date
  expirationTime: Date
}
