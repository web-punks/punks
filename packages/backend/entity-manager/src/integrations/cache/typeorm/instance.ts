import { MoreThanOrEqual, Repository } from "typeorm"
import {
  CacheEntryDetail,
  CacheEntryInfo,
  CacheTtl,
  ICacheInstance,
} from "../../../abstractions"
import { ILogger, Log, addTime, newUuid } from "@punks/backend-core"
import { ICacheDatabaseItem } from "./types"

export abstract class TypeormCacheInstance<TEntity extends ICacheDatabaseItem>
  implements ICacheInstance
{
  private readonly logger: ILogger

  constructor(protected readonly instanceName: string) {
    this.logger = Log.getLogger(`TypeOrm Cache Instance ${instanceName}`)
  }

  getInstanceName(): string {
    return this.instanceName
  }

  async getEntries(): Promise<CacheEntryInfo[]> {
    const items = await this.getRepository().find({
      where: {
        instance: this.instanceName,
        expirationTime: MoreThanOrEqual(new Date()),
      } as any,
    })
    return items.map((item) => ({
      instanceName: this.instanceName,
      key: item.key,
      expiration: item.expirationTime,
      createdOn: item.createdOn,
      updatedOn: item.updatedOn,
    }))
  }

  async getEntry(key: string): Promise<CacheEntryDetail | undefined> {
    const item = await this.getRepository().findOne({
      where: {
        instance: this.instanceName,
        key,
        expirationTime: MoreThanOrEqual(new Date()),
      } as any,
    })
    if (!item) {
      return undefined
    }
    return {
      instanceName: this.instanceName,
      key: item.key,
      expiration: item.expirationTime,
      createdOn: item.createdOn,
      updatedOn: item.updatedOn,
      data: item.data,
    }
  }

  async retrieve<T>(
    key: string,
    input: { ttl: CacheTtl; valueFactory: () => Promise<T> }
  ): Promise<T> {
    const item = await this.getRepository().findOne({
      where: {
        instance: this.instanceName,
        key,
        expirationTime: MoreThanOrEqual(new Date()),
      } as any,
    })
    if (item) {
      return item.data
    }
    const value = await input.valueFactory()
    await this.set(key, { value, ttl: input.ttl })
    return value
  }

  async get<T>(key: string): Promise<T | undefined> {
    const item = await this.getRepository().findOne({
      where: {
        instance: this.instanceName,
        key,
        expirationTime: MoreThanOrEqual(new Date()),
      } as any,
    })
    return item?.data
  }

  async set<T>(
    key: string,
    { ttl, value }: { value: T; ttl: CacheTtl }
  ): Promise<void> {
    await this.getRepository().manager.transaction(async (manager) => {
      const item = await manager.findOne(this.getRepository().metadata.target, {
        where: {
          instance: this.instanceName,
          key,
        } as any,
      })
      if (item) {
        item.data = value
        item.ttl = ttl
        item.expirationTime = addTime(new Date(), ttl)
        await manager.save(item)
        this.logger.debug("Updated cache item", { key, value })
      } else {
        const newItem = this.getRepository().create({
          id: newUuid(),
          instance: this.instanceName,
          key,
          data: value,
          expirationTime: addTime(new Date(), ttl),
        } as any)
        await manager.save(newItem)
        this.logger.debug("Created cache item", { key, value })
      }
    })
  }

  async delete(key: string): Promise<void> {
    await this.getRepository().delete({
      instance: this.instanceName,
      key,
    } as any)
    this.logger.debug("Deleted cache item", { key })
  }

  async clear(): Promise<void> {
    await this.getRepository().delete({
      instance: this.instanceName,
    } as any)
    this.logger.debug("Cleared cache")
  }

  protected abstract getRepository(): Repository<TEntity>
}
