export type DynamoDbCacheItem = {
  instance: string
  key: string
  expiration: number
  createdOn: number
  updatedOn: number
  data: any
}
