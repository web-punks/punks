import { CacheEntryDetail, CacheEntryInfo } from "../../../abstractions/cache"
import { DynamoDbCacheItem } from "./types"

export const toCacheEntryInfo = (item: DynamoDbCacheItem): CacheEntryInfo => ({
  instanceName: item.instance,
  key: item.key,
  expiration: new Date(item.expiration),
  createdOn: new Date(item.createdOn),
  updatedOn: new Date(item.updatedOn),
})

export const toCacheEntryDetail = (
  item: DynamoDbCacheItem
): CacheEntryDetail => ({
  instanceName: item.instance,
  key: item.key,
  expiration: new Date(item.expiration),
  createdOn: new Date(item.createdOn),
  updatedOn: new Date(item.updatedOn),
  data: item.data,
})
