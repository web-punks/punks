import { addTime, ILogger, Log } from "@punks/backend-core"
import { DynamoDbCollection } from "../../../platforms/nest/plugins/collections/aws-dynamodb"
import {
  CacheEntryDetail,
  CacheEntryInfo,
  CacheTtl,
  ICacheInstance,
} from "../../../abstractions/cache"
import { DynamoDbCacheItem } from "./types"
import { toCacheEntryDetail, toCacheEntryInfo } from "./converter"

export type DynamoDbCacheInstanceOptions = {
  tableName: string
  partitionKey?: string
  sortKey?: string
}

export abstract class DynamoDbCacheInstance implements ICacheInstance {
  private readonly logger: ILogger
  private readonly collection: DynamoDbCollection

  constructor(
    protected readonly instanceName: string,
    dynamoDbOptions: DynamoDbCacheInstanceOptions
  ) {
    this.logger = Log.getLogger(`DynamoDb Cache Instance ${instanceName}`)
    this.collection = new DynamoDbCollection({
      tableName: dynamoDbOptions.tableName,
      primaryKey: {
        partitionKey: dynamoDbOptions.partitionKey ?? "instance",
        sortKey: dynamoDbOptions.sortKey ?? "key",
      },
    })
  }
  async getEntries(): Promise<CacheEntryInfo[]> {
    const items = await this.collection.queryTableItems<DynamoDbCacheItem>({
      pageSize: 100,
    })
    return items.map(toCacheEntryInfo)
  }

  async getEntry(key: string): Promise<CacheEntryDetail | undefined> {
    const entry = await this.collection.getItem<DynamoDbCacheItem>({
      instance: {
        S: this.instanceName,
      },
      key: {
        S: key,
      },
    })
    return entry && !this.isExpired(entry, new Date())
      ? toCacheEntryDetail(entry)
      : undefined
  }

  async get<T>(key: string): Promise<T | undefined> {
    const item = await this.getEntry(key)
    return item?.data as T | undefined
  }

  async set<T>(key: string, input: { value: T; ttl: CacheTtl }): Promise<void> {
    const now = Date.now()
    await this.collection.putItem<DynamoDbCacheItem>({
      key,
      data: input.value,
      createdOn: now,
      updatedOn: now,
      expiration: addTime(new Date(), input.ttl).getTime(),
      instance: this.instanceName,
    })
  }

  async retrieve<T>(
    key: string,
    input: { ttl: CacheTtl; valueFactory: () => Promise<T> }
  ): Promise<T> {
    const item = await this.get<T>(key)
    if (item) {
      return item
    }
    const value = await input.valueFactory()
    await this.set(key, { value, ttl: input.ttl })
    return value
  }

  async delete(key: string): Promise<void> {
    await this.collection.deleteItem({
      instance: {
        S: this.instanceName,
      },
      key: {
        S: key,
      },
    })
  }

  async clear(): Promise<void> {
    await this.collection.scanTable<DynamoDbCacheItem>({
      pageSize: 100,
      callback: (items) => {
        items.forEach((item) => {
          if (item.instance === this.instanceName) {
            this.collection.deleteItem({
              instance: {
                S: this.instanceName,
              },
              key: {
                S: item.key,
              },
            })
          }
        })
      },
    })
  }

  getInstanceName(): string {
    return this.instanceName
  }

  private isExpired(item: DynamoDbCacheItem, when: Date) {
    return item.expiration < when.getTime()
  }
}
