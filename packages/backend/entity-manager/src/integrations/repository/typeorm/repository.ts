import {
  FindManyOptions,
  FindOneOptions,
  FindOptionsRelations,
  FindOptionsWhere,
  In,
  ObjectId,
  ObjectLiteral,
  Repository,
} from "typeorm"
import { IEntityRepository } from "../../../abstractions"
import { IEntitiesDeleteResult } from "../../../abstractions/repository"
import { ILogger, Log, isNullOrUndefined } from "@punks/backend-core"
import { EntityReference } from "../../../models"

export type UpdateCriteria<TEntity extends ObjectLiteral> =
  | string
  | string[]
  | number
  | number[]
  | Date
  | Date[]
  | ObjectId
  | ObjectId[]
  | FindOptionsWhere<TEntity>

export type DeleteCriteria<TEntity extends ObjectLiteral> =
  | string
  | string[]
  | number
  | number[]
  | Date
  | Date[]
  | ObjectId
  | ObjectId[]
  | FindOptionsWhere<TEntity>

export class TypeOrmRepository<TEntity extends ObjectLiteral, TEntityId>
  implements
    IEntityRepository<
      TEntity,
      TEntityId,
      FindOneOptions<TEntity>,
      FindManyOptions<TEntity>,
      UpdateCriteria<TEntity>,
      DeleteCriteria<TEntity>
    >
{
  private readonly logger: ILogger

  constructor(protected readonly innerRepository: Repository<TEntity>) {
    this.logger = Log.getLogger(
      `TypeOrm Repository ${innerRepository.metadata.name}`
    )
  }

  getInnerRepository() {
    return this.innerRepository
  }

  async distinct<TField extends keyof TEntity, TValue>(
    field: TField,
    condition: FindManyOptions<TEntity>
  ) {
    this.logger.debug("Getting distinct values", { field })
    const columnName = this.innerRepository.metadata.findColumnWithPropertyName(
      field as string
    )?.databaseName
    if (!columnName) {
      throw new Error(`Column '${field as string}' not found`)
    }

    const results = await this.innerRepository
      .createQueryBuilder()
      .select(`DISTINCT ${columnName}`)
      .where(condition)
      .orderBy(columnName)
      .getRawMany<TValue>()

    return results.map((x) => (x as any)[columnName])
  }

  async exists(id: TEntityId): Promise<boolean> {
    if (isNullOrUndefined(id)) {
      throw new Error("Invalid 'id' parameter.")
    }

    this.logger.debug("Checking entity exists by id", { id })
    return await this.innerRepository.exist({
      where: {
        id: id as any,
      },
    })
  }

  async existsBy(condition: FindOneOptions<TEntity>): Promise<boolean> {
    this.logger.debug("Checking entity exists by condition", { condition })
    return await this.innerRepository.exist(condition)
  }

  async get(
    id: TEntityId,
    options?: {
      relations?: FindOptionsRelations<TEntity>
    }
  ): Promise<TEntity | undefined> {
    if (isNullOrUndefined(id)) {
      throw new Error("Missing 'id' parameter.")
    }

    this.logger.debug("Getting entity by id", { id })
    const result = await this.innerRepository.findOne({
      where: {
        id: id as any,
      },
      relations: options?.relations,
    })
    return result ?? undefined
  }

  async getBy(
    condition: FindOneOptions<TEntity>
  ): Promise<TEntity | undefined> {
    this.logger.debug("Getting entity by condition", { condition })
    const result = await this.innerRepository.findOne(condition)
    return result ?? undefined
  }

  async all(): Promise<TEntity[]> {
    this.logger.debug("Getting all entities")
    return await this.innerRepository.find()
  }

  async count(condition: FindManyOptions<TEntity>): Promise<number> {
    this.logger.debug("Counting entities", { condition })
    return await this.innerRepository.count(condition)
  }

  async find(condition: FindManyOptions<TEntity>): Promise<TEntity[]> {
    this.logger.debug("Finding entities by condition", { condition })
    return await this.innerRepository.find(condition)
  }

  async findOne(
    condition: FindManyOptions<TEntity>
  ): Promise<TEntity | undefined> {
    this.logger.debug("Finding entity by condition", { condition })
    const items = await this.innerRepository.find(condition)
    return items[0]
  }

  async findOneOrFail(condition: FindManyOptions<TEntity>): Promise<TEntity> {
    this.logger.debug("Finding entity by condition", { condition })
    const items = await this.innerRepository.find(condition)
    if (items.length === 0) {
      throw new Error("Entity not found")
    }
    return items[0]
  }

  async findById(id: TEntityId[]): Promise<TEntity[]> {
    this.logger.debug("Finding entities by id", { id })
    return await this.innerRepository.findBy({
      id: In(id),
    } as any)
  }

  async delete(id: TEntityId): Promise<void> {
    if (isNullOrUndefined(id)) {
      throw new Error("Invalid 'id' parameter.")
    }

    this.logger.debug("Deleting entity by id", { id })
    await this.innerRepository.delete({
      id: id as any,
    })
  }

  async deleteBy(
    condition: DeleteCriteria<TEntity>
  ): Promise<IEntitiesDeleteResult> {
    const result = await this.innerRepository.delete(condition)
    this.logger.debug("Deleting entities by condition", { condition, result })
    return {
      deletedCount: result.affected as number,
    }
  }

  async create(entity: Partial<TEntity>): Promise<TEntity> {
    this.logger.debug("Creating entity", { entity })
    const createResult = await this.innerRepository.insert(entity)
    const current = await this.get(createResult.identifiers[0].id)
    if (!current) {
      throw new Error(`Entity ${entity.id} not found`)
    }
    return current
  }

  async update(id: TEntityId, entity: Partial<TEntity>): Promise<TEntity> {
    if (isNullOrUndefined(id)) {
      throw new Error("Invalid 'id' parameter.")
    }

    this.logger.debug("Updating entity", { id, entity })
    // await this.innerRepository.preload(id as any)
    // await this.innerRepository.save({
    //   id,
    //   ...entity,
    // } as unknown as TEntity)
    await this.innerRepository.update(id as any, entity)
    const current = await this.get(id)
    if (!current) {
      throw new Error(`Entity ${id} not found`)
    }
    return current
  }

  async updateBy(
    entity: Partial<TEntity>,
    condition: UpdateCriteria<TEntity>
  ): Promise<TEntity[]> {
    this.logger.debug("Updating entities by condition", { entity, condition })
    const updateResult = await this.innerRepository.update(condition, entity)
    return await this.findById(updateResult.generatedMaps.map((x) => x.id))
  }

  async upsert(id: TEntityId, entity: Partial<TEntity>): Promise<TEntity> {
    if (isNullOrUndefined(id)) {
      throw new Error("Invalid 'id' parameter.")
    }

    this.logger.debug("Upserting entity", { id, entity })
    if (await this.exists(id)) {
      return await this.update(id, entity)
    }
    return await this.create({
      ...entity,
      id,
    })
  }

  // todo: fix this
  async upsertBy({
    data,
    filter,
  }: {
    data: Partial<TEntity>
    filter: FindManyOptions<TEntity>
  }): Promise<EntityReference<string>> {
    this.logger.debug("Upsert entities by condition", { filter, data })
    const current = await this.find(filter)
    if (current) {
      const id = (current as any).id
      await this.update(id, data)
      return {
        id,
      }
    }

    const entity = await this.create(data)
    return {
      id: entity.id,
    }
  }
}
