import { FindOneOptions, ObjectLiteral, Repository } from "typeorm"
import { EntitySeeder } from "../../../base"

export abstract class TypeOrmEntitySeeder<
  TEntity extends ObjectLiteral
> extends EntitySeeder<TEntity, FindOneOptions<TEntity>> {
  protected async exists(condition: FindOneOptions<TEntity>): Promise<boolean> {
    const count = await this.getRepository().count(condition)
    return count > 0
  }

  protected async create(entity: TEntity): Promise<void> {
    await this.getRepository().insert(entity)
  }

  protected abstract getRepository(): Repository<TEntity>
}
