import {
  Equal,
  FindOperator,
  LessThan,
  LessThanOrEqual,
  MoreThan,
  MoreThanOrEqual,
  In,
  Not,
  And,
  Or,
  ILike,
  IsNull,
  Raw,
  Like,
  Between,
} from "typeorm"
import {
  ArrayFilter,
  BooleanFilter,
  DateFilter,
  EnumFilter,
  IdFilter,
  NumericFilter,
  StringFilter,
  StringListFilter,
} from "./clause"
import { isNullOrUndefined } from "@punks/backend-core"

export class QueryClauseBuilder {
  stringArrayFilter(filter: ArrayFilter<string>): FindOperator<string> {
    if (filter.eq) {
      return `{${filter.eq
        .map((v) => `"${v}"`)
        .join(",")}}` as unknown as FindOperator<string>
    }

    if (filter?.isNull) {
      return IsNull()
    }

    return And()
  }

  enumFilter<T>(filter: EnumFilter<T>): FindOperator<T> {
    const clauses = []

    if (!isNullOrUndefined(filter?.in)) {
      clauses.push(this.buildInClause(filter.in!))
    }

    if (!isNullOrUndefined(filter?.eq)) {
      clauses.push(Equal(filter.eq))
    }

    if (!isNullOrUndefined(filter?.ne)) {
      clauses.push(Not(Equal(filter.ne)))
    }

    if (!isNullOrUndefined(filter?.notIn)) {
      clauses.push(Not(this.buildInClause(filter.notIn!)))
    }

    if (filter?.isNull === true) {
      clauses.push(IsNull())
    }

    if (filter?.isNull === false) {
      clauses.push(Not(IsNull()))
    }

    return And(...clauses)
  }

  idFilter(filter: IdFilter): FindOperator<string> {
    const clauses = []

    if (!isNullOrUndefined(filter?.in)) {
      clauses.push(this.buildInClause(filter.in!))
    }

    if (!isNullOrUndefined(filter?.eq)) {
      clauses.push(Equal(filter.eq))
    }

    if (!isNullOrUndefined(filter?.ne)) {
      clauses.push(Not(Equal(filter.ne)))
    }

    if (!isNullOrUndefined(filter?.notIn)) {
      clauses.push(Not(this.buildInClause(filter.notIn!)))
    }

    if (filter?.isNull === true) {
      clauses.push(IsNull())
    }

    if (filter?.isNull === false) {
      clauses.push(Not(IsNull()))
    }

    return And(...clauses)
  }

  stringFilter(filter: StringFilter): FindOperator<string> {
    const clauses = []

    if (!isNullOrUndefined(filter?.gte)) {
      clauses.push(MoreThanOrEqual(filter.gte))
    }

    if (!isNullOrUndefined(filter?.gt)) {
      clauses.push(MoreThan(filter.gt))
    }

    if (!isNullOrUndefined(filter?.lte)) {
      clauses.push(LessThanOrEqual(filter.lte))
    }

    if (!isNullOrUndefined(filter?.lt)) {
      clauses.push(LessThan(filter.lt))
    }

    if (!isNullOrUndefined(filter?.in)) {
      clauses.push(this.buildInClause(filter.in!))
    }

    if (!isNullOrUndefined(filter?.eq)) {
      clauses.push(Equal(filter.eq))
    }

    if (!isNullOrUndefined(filter?.ieq)) {
      clauses.push(ILike(filter.eq))
    }

    if (!isNullOrUndefined(filter?.like)) {
      clauses.push(ILike(`%${filter.like!.replaceAll("*", "%")}%`))
    }

    if (!isNullOrUndefined(filter?.ne)) {
      clauses.push(Not(Equal(filter.ne)))
    }

    if (!isNullOrUndefined(filter?.ine)) {
      clauses.push(Not(ILike(filter.ine)))
    }

    if (!isNullOrUndefined(filter?.notIn)) {
      clauses.push(Not(this.buildInClause(filter.notIn!)))
    }

    if (!isNullOrUndefined(filter?.notLike)) {
      clauses.push(Not(ILike(filter.notLike!.replaceAll("*", "%"))))
    }

    if (filter?.isNull === true) {
      clauses.push(IsNull())
    }

    if (filter?.isNull === false) {
      clauses.push(Not(IsNull()))
    }

    if (filter?.isEmpty === true) {
      clauses.push(
        Raw((alias) => `TRIM(${alias}) IS NULL OR TRIM(${alias}) = ''`)
      )
    }

    if (filter?.isEmpty === false) {
      clauses.push(
        Raw((alias) => `TRIM(${alias}) IS NOT NULL AND TRIM(${alias}) <> ''`)
      )
    }

    return And(...clauses)
  }

  stringListFilter(filter: StringListFilter): FindOperator<string> {
    const clauses = []

    if (!isNullOrUndefined(filter?.contains)) {
      clauses.push(Like(`%${filter.contains}%`))
    }

    if (!isNullOrUndefined(filter?.iContains)) {
      clauses.push(ILike(`%${filter.iContains}%`))
    }

    return And(...clauses)
  }

  numericFilter(filter: NumericFilter): FindOperator<number> {
    const clauses = []

    if (!isNullOrUndefined(filter?.gte)) {
      clauses.push(MoreThanOrEqual(filter.gte))
    }

    if (!isNullOrUndefined(filter?.gt)) {
      clauses.push(MoreThan(filter.gt))
    }

    if (!isNullOrUndefined(filter?.lte)) {
      clauses.push(LessThanOrEqual(filter.lte))
    }

    if (!isNullOrUndefined(filter?.lt)) {
      clauses.push(LessThan(filter.lt))
    }

    if (!isNullOrUndefined(filter?.in)) {
      clauses.push(this.buildInClause(filter.in ?? []))
    }

    if (!isNullOrUndefined(filter?.notIn)) {
      clauses.push(Not(this.buildInClause(filter.notIn!)))
    }

    if (!isNullOrUndefined(filter?.eq)) {
      clauses.push(Equal(filter.eq))
    }

    if (filter?.isNull === true) {
      clauses.push(IsNull())
    }

    if (filter?.isNull === false) {
      clauses.push(Not(IsNull()))
    }

    return And(...clauses)
  }

  dateFilter(filter: DateFilter): FindOperator<Date> {
    const clauses = []

    if (!isNullOrUndefined(filter?.eq)) {
      const startOfDay = new Date(filter.eq as Date)
      startOfDay.setHours(0, 0, 0, 0)
      const endOfDay = new Date(filter.eq as Date)
      endOfDay.setHours(23, 59, 59, 999)
      clauses.push(Between(startOfDay, endOfDay))
    }

    if (!isNullOrUndefined(filter?.gte)) {
      clauses.push(MoreThanOrEqual(filter.gte))
    }

    if (!isNullOrUndefined(filter?.gt)) {
      clauses.push(MoreThan(filter.gt))
    }

    if (!isNullOrUndefined(filter?.lte)) {
      clauses.push(LessThanOrEqual(filter.lte))
    }

    if (!isNullOrUndefined(filter?.lt)) {
      clauses.push(LessThan(filter.lt))
    }

    if (filter?.isNull === true) {
      clauses.push(IsNull())
    }

    if (filter?.isNull === false) {
      clauses.push(Not(IsNull()))
    }

    return And(...clauses)
  }

  boolFilter(filter: BooleanFilter): FindOperator<boolean> {
    const clauses = []

    if (!isNullOrUndefined(filter?.eq)) {
      clauses.push(Equal(filter.eq as boolean))
    }

    if (!isNullOrUndefined(filter?.ne)) {
      clauses.push(Not(Equal(filter.ne as boolean)))
    }

    if (filter?.isNull === true) {
      clauses.push(IsNull())
    }

    if (filter?.isNull === false) {
      clauses.push(Not(IsNull()))
    }

    if (!isNullOrUndefined(filter?.in)) {
      clauses.push(this.buildInClause(filter.in ?? []))
    }

    if (!isNullOrUndefined(filter?.notIn)) {
      clauses.push(Not(this.buildInClause(filter.notIn!)))
    }

    return And(...clauses)
  }

  private buildInClause<T>(values: T[]) {
    const containsNulls = values.some((v) => isNullOrUndefined(v))
    if (!containsNulls) {
      return In(values)
    }

    return Or(In(values.filter((v) => !isNullOrUndefined(v))), IsNull())
  }
}
