import { FindOptionsWhere } from "typeorm"

export const mergeWhereClauses = <TEntity>(
  where1: FindOptionsWhere<TEntity>,
  where2: FindOptionsWhere<TEntity>
) => {
  const mergedWhere: FindOptionsWhere<TEntity> = {}
  for (const key in where1) {
    if (where1.hasOwnProperty(key)) {
      mergedWhere[key] = where1[key]
    }
  }

  for (const key in where2) {
    if (where2.hasOwnProperty(key)) {
      if (mergedWhere.hasOwnProperty(key)) {
        if (
          typeof where2[key] === "object" &&
          typeof mergedWhere[key] === "object"
        ) {
          mergedWhere[key] = {
            ...(mergedWhere[key] as any),
            ...(where2[key] as any),
          }
        } else {
          mergedWhere[key] = where2[key]
        }
      } else {
        mergedWhere[key] = where2[key]
      }
    }
  }

  return mergedWhere
}
