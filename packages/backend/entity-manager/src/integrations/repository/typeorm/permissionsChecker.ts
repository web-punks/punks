import { IAuthenticationContext } from "../../../abstractions"

export class PermissionsChecker<TUserContext> {
  hasAny(
    context: IAuthenticationContext<TUserContext>,
    permissions: string[]
  ): boolean {
    return (
      context.userPermissions?.some((p) => permissions.includes(p.uid)) ?? false
    )
  }

  hasAll(
    context: IAuthenticationContext<TUserContext>,
    permissions: string[]
  ): boolean {
    return (
      permissions.every((p) =>
        context.userPermissions?.some((up) => up.uid === p)
      ) ?? false
    )
  }
}
