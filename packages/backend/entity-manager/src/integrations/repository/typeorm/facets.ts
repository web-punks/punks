import { IAuthenticationContext } from "../../../abstractions/authentication"

export type FacetValueType<T> = {
  value: T
  label: string
  count: number
}

export type FacetType<T> = {
  values: FacetValueType<T>[]
}

export type StringFacetsType = FacetType<string>
export type NumberFacetsType = FacetType<number>
export type BooleanFacetsType = FacetType<boolean>
export type DateFacetsType = FacetType<Date>
export type FacetRelations = {
  columns: string[]
}

export type FacetBuilderInput<TEntity, TEntitySearchParameters, TUserContext> =
  {
    field: keyof TEntity
    request: TEntitySearchParameters
    context: IAuthenticationContext<TUserContext> | undefined
    relations?: FacetRelations
    labelSelector?: string
  }
