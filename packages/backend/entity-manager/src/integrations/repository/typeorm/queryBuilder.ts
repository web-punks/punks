import {
  FindOptionsOrder,
  FindOptionsRelations,
  FindOptionsWhere,
  ILike,
  In,
  ObjectLiteral,
} from "typeorm"
import {
  IAuthenticationContext,
  IEntityFacets,
  IEntitySearchParameters,
  SortingType,
} from "../../../abstractions"
import { IEntitiesSearchResults } from "../../../models"
import { QueryBuilderBase } from "../../../templates"
import { DeleteCriteria, TypeOrmRepository } from "./repository"
import { EntityServiceLocator } from "../../../providers/services"
import { IEntitiesDeleteResult } from "../../../abstractions/repository"
import { QueryClauseBuilder } from "./queryClauseBuilder"
import {
  BooleanFacetsType,
  FacetRelations,
  FacetType,
  NumberFacetsType,
  FacetBuilderInput,
  StringFacetsType,
} from "./facets"
import { QueryBuilderOperation } from "../../../templates/queryBuilder"
import { buildObject } from "@punks/backend-core"
import { ChildrenMap } from "./traverse"
import { PermissionsChecker } from "./permissionsChecker"
import { mergeWhereClauses } from "./utils"
import { IEntityGetQueryOptions } from "../../../abstractions/queries"

export abstract class TypeOrmQueryBuilder<
  TEntity extends ObjectLiteral,
  TEntityId,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    number
  >,
  TSorting extends SortingType,
  TFacets extends IEntityFacets,
  TUserContext
> extends QueryBuilderBase<
  TEntity,
  TEntityId,
  TEntitySearchParameters,
  TSorting,
  number,
  TFacets,
  TUserContext
> {
  private repository: TypeOrmRepository<TEntity, unknown>
  protected clause = new QueryClauseBuilder()
  protected permissions = new PermissionsChecker<TUserContext>()

  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {
    super()
  }

  async get(
    id: TEntityId,
    context?: IAuthenticationContext<TUserContext>,
    options?: IEntityGetQueryOptions<TEntity, TEntitySearchParameters>
  ): Promise<TEntity | undefined> {
    return await this.getRepository().get(id, {
      relations:
        options?.relations ??
        this.getRelationsToLoad(undefined, context, QueryBuilderOperation.Get),
    })
  }

  async exists(
    filters: NonNullable<TEntitySearchParameters["filters"]>,
    context?: IAuthenticationContext<TUserContext> | undefined
  ): Promise<boolean> {
    return (await this.count(filters, context)) > 0
  }

  async count(
    filters: NonNullable<TEntitySearchParameters["filters"]>,
    context?: IAuthenticationContext<TUserContext> | undefined
  ): Promise<number> {
    const request = {
      filters,
    } as TEntitySearchParameters

    return await this.countQueryResults(request, context)
  }

  async delete(
    filters: NonNullable<TEntitySearchParameters["filters"]>,
    context?: IAuthenticationContext<TUserContext> | undefined
  ): Promise<IEntitiesDeleteResult> {
    const request = {
      filters,
    } as TEntitySearchParameters

    return await this.getRepository().deleteBy(
      this.buildWhere(request, context) as DeleteCriteria<TEntity>
    )
  }

  async find(
    request: {
      filters?: TEntitySearchParameters["filters"]
      sorting?: TEntitySearchParameters["sorting"]
      relations?: TEntitySearchParameters["relations"]
    },
    context?: IAuthenticationContext<TUserContext>
  ): Promise<TEntity> {
    const results = await this.findPagedQueryResults(
      {
        filters: request.filters,
        sorting: request.sorting,
        paging: {
          pageSize: 1,
        },
        relations: request.relations,
      } as TEntitySearchParameters,
      context
    )
    return results[0]
  }

  async search(
    request: TEntitySearchParameters,
    context?: IAuthenticationContext<TUserContext>
  ): Promise<
    IEntitiesSearchResults<
      TEntity,
      TEntitySearchParameters,
      TEntity,
      TSorting,
      number,
      TFacets
    >
  > {
    // todo: execute inside read transaction in order to perform the three queries on the same dataset
    const queryResults = await this.countQueryResults(request, context)
    const results = await this.findPagedQueryResults(request, context)
    const facets = request.options?.includeFacets
      ? // todo: refactor avoiding to manipulate the request object
        await this.calculateFacets(
          {
            ...request,
            filters: request.options.facetsFilters,
          },
          context
        )
      : undefined
    const childrenMap = request.options?.includeChildrenMap
      ? await this.calculateChildrenMap(results)
      : undefined

    return {
      items: results,
      request,
      paging: this.getIndexBasedPagingResult({
        paging: request.paging,
        currentPageResults: results.length,
        totResults: queryResults,
      }),
      facets,
      childrenMap,
    }
  }

  protected async getFieldDistinctValues<T>(
    field: keyof TEntity,
    request: TEntitySearchParameters,
    context: IAuthenticationContext<TUserContext> | undefined
  ): Promise<T[]> {
    return await this.getRepository().distinct<keyof TEntity, T>(field, {
      where: this.buildWhere(request, context),
    })
  }

  protected async findPagedQueryResults(
    request: TEntitySearchParameters,
    context: IAuthenticationContext<TUserContext> | undefined
  ) {
    return await this.getRepository().find({
      where: this.buildWhere(request, context),
      relations: this.buildSearchRelations(request, context),
      order: this.buildSortingClause(request),
      ...this.buildPagingParameters(request),
    })
  }

  protected getRelationsToLoad(
    request: TEntitySearchParameters | undefined,
    context: IAuthenticationContext<TUserContext> | undefined,
    operation: QueryBuilderOperation
  ): FindOptionsRelations<TEntity> | undefined {
    return undefined
  }

  protected buildPagingParameters(request: TEntitySearchParameters) {
    return {
      skip: request.paging?.cursor
        ? request.paging.cursor * request.paging.pageSize
        : undefined,
      take: request.paging?.pageSize,
    }
  }

  protected async countQueryResults(
    request: TEntitySearchParameters,
    context?: IAuthenticationContext<TUserContext>
  ) {
    return await this.getRepository().count({
      where: this.buildWhere(request, context),
    })
  }

  protected abstract buildSortingClause(
    request: TEntitySearchParameters
  ): FindOptionsOrder<TEntity>

  protected abstract buildWhereClause(
    request: TEntitySearchParameters
  ): FindOptionsWhere<TEntity>[] | FindOptionsWhere<TEntity>

  protected buildTextSearchClause(
    request: TEntitySearchParameters
  ): FindOptionsWhere<TEntity>[] | FindOptionsWhere<TEntity> {
    if (
      !request.query ||
      !request.query.term ||
      !request.query.fields?.length
    ) {
      return {}
    }

    return request.query.fields.map((field) =>
      buildObject({
        key: field,
        value: ILike(`%${request.query?.term}%`),
      })
    ) as FindOptionsWhere<TEntity>[]
  }

  protected abstract buildContextFilter(
    context?: IAuthenticationContext<TUserContext>
  ): FindOptionsWhere<TEntity>[] | FindOptionsWhere<TEntity>

  protected abstract calculateFacets(
    request: TEntitySearchParameters,
    context?: IAuthenticationContext<TUserContext>
  ): Promise<TFacets>

  protected calculateChildrenMap(items: TEntity[]): Promise<ChildrenMap> {
    return Promise.resolve({})
  }

  protected async queryChildrenMap<TField extends keyof TEntity>({
    nodeIds,
    idField,
    parentField,
  }: {
    nodeIds: string[]
    idField: TField
    parentField: TField
  }): Promise<ChildrenMap> {
    const result = await this.getRepository()
      .getInnerRepository()
      .createQueryBuilder("entity")
      .where({
        [parentField.toString()]: {
          [idField.toString()]: In(nodeIds),
        },
      })
      .select(
        `entity.${parentField.toString()}.${idField.toString()}`,
        "parentId"
      )
      .addSelect(`COUNT(entity.${idField.toString()})`, "count")
      .groupBy(`entity.${parentField.toString()}.${idField.toString()}`)
      .getRawMany()
    return result.reduce((acc, x) => {
      acc[x.parentId] = {
        count: parseInt(x.count),
      }
      return acc
    }, {})
  }

  protected async calculateStringFieldFacets(
    input: FacetBuilderInput<TEntity, TEntitySearchParameters, TUserContext>
  ): Promise<StringFacetsType> {
    return await this.calculateFacet<string>(input)
  }

  protected async calculateNumericFieldFacets(
    input: FacetBuilderInput<TEntity, TEntitySearchParameters, TUserContext>
  ): Promise<NumberFacetsType> {
    return await this.calculateFacet<number>(input)
  }

  protected async calculateBooleanFieldFacets(
    input: FacetBuilderInput<TEntity, TEntitySearchParameters, TUserContext>
  ): Promise<BooleanFacetsType> {
    return await this.calculateFacet<boolean>(input)
  }

  protected async calculateFacet<TValue>({
    field,
    request,
    context,
    relations,
    labelSelector,
  }: FacetBuilderInput<
    TEntity,
    TEntitySearchParameters,
    TUserContext
  >): Promise<FacetType<TValue>> {
    let query = this.getRepository()
      .getInnerRepository()
      .createQueryBuilder("entity")

    for (const relation of relations?.columns ?? []) {
      query = query.leftJoin(`entity.${relation}`, relation)
    }

    const fieldName = `"${field as string}"`
    query = query
      .select(fieldName, "value")
      .addSelect(`COUNT(${fieldName})`, "count")
      .where(this.buildWhere(request, context))
      .addGroupBy(fieldName)

    if (labelSelector) {
      query.addSelect(labelSelector, "label").addGroupBy(labelSelector)
    }

    const results = await query.orderBy(labelSelector ?? fieldName).getRawMany()

    return {
      values: results.map((x) => ({
        value: x.value,
        label: String(x.label ?? x.value ?? ""),
        count: Number(x.count),
      })),
    }
  }

  protected getRepository(): TypeOrmRepository<TEntity, unknown> {
    if (!this.repository) {
      this.repository = this.services.resolveRepository() as TypeOrmRepository<
        TEntity,
        unknown
      >
    }
    return this.repository
  }

  private buildSearchRelations(
    request: TEntitySearchParameters,
    context: IAuthenticationContext<TUserContext> | undefined
  ): FindOptionsRelations<TEntity> | undefined {
    return (
      (request.relations as FindOptionsRelations<TEntity>) ??
      this.getRelationsToLoad(request, context, QueryBuilderOperation.Search)
    )
  }

  private buildWhere(
    request: TEntitySearchParameters,
    context: IAuthenticationContext<TUserContext> | undefined
  ) {
    return this.mergeWhereClauses(
      context ? this.buildContextFilter(context) : {},
      this.buildWhereClause(request),
      this.buildTextSearchClause(request)
    )
  }

  private mergeWhereClauses(
    ...clauses: (FindOptionsWhere<TEntity>[] | FindOptionsWhere<TEntity>)[]
  ): FindOptionsWhere<TEntity> | FindOptionsWhere<TEntity>[] {
    return clauses.reduce((a, b) => this.mergeWheres(a, b))
  }

  private mergeWheres(
    a: FindOptionsWhere<TEntity>[] | FindOptionsWhere<TEntity>,
    b: FindOptionsWhere<TEntity>[] | FindOptionsWhere<TEntity>
  ): FindOptionsWhere<TEntity>[] | FindOptionsWhere<TEntity> {
    if (Array.isArray(a) && Array.isArray(b)) {
      const result: FindOptionsWhere<TEntity>[] = []
      for (const x of a) {
        for (const y of b) {
          result.push({
            ...x,
            ...y,
          })
        }
      }
      return result
    }

    if (!Array.isArray(a) && !Array.isArray(b)) {
      return mergeWhereClauses(a, b)
    }

    if (Array.isArray(a) && !Array.isArray(b)) {
      return a.map((x) =>
        mergeWhereClauses(x, b)
      ) as FindOptionsWhere<TEntity>[]
    }

    if (!Array.isArray(a) && Array.isArray(b)) {
      return b.map((x) =>
        mergeWhereClauses(a, x)
      ) as FindOptionsWhere<TEntity>[]
    }

    return {
      ...a,
      ...b,
    }
  }
}
