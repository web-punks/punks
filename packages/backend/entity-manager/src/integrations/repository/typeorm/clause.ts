export type NumericFilter = {
  in?: number[]
  eq?: number
  gt?: number
  gte?: number
  lt?: number
  lte?: number
  isNull?: boolean
  notIn?: number[]
}

export type StringFilter = {
  gt?: string
  gte?: string
  lt?: string
  lte?: string
  in?: string[]
  ieq?: string
  eq?: string
  like?: string
  ne?: string
  ine?: string
  notIn?: string[]
  notLike?: string
  isNull?: boolean
  isEmpty?: boolean
}

export type IdFilter = {
  in?: string[]
  eq?: string
  ne?: string
  notIn?: string[]
  isNull?: boolean
}

export type EnumFilter<T> = {
  in?: T[]
  eq?: T
  ne?: T
  notIn?: T[]
  isNull?: boolean
}

export type ArrayFilter<T> = {
  eq?: T[]
  isNull?: boolean
}

export type BooleanFilter = {
  eq?: boolean
  ne?: boolean
  isNull?: boolean
  in?: boolean[]
  notIn?: boolean[]
}

export type DateFilter = {
  in?: Date[]
  eq?: Date
  gt?: Date
  gte?: Date
  lt?: Date
  lte?: Date
  isNull?: boolean
}

export type StringListFilter = {
  contains?: string
  iContains?: string
  notContains?: string
  iNotContains?: string
}
