import { Log } from "@punks/backend-core"
import { IEntitySeedEntry, IEntitySeeder } from "../abstractions/seed"

export abstract class EntitySeeder<TEntity, TFindCondition>
  implements IEntitySeeder
{
  protected readonly logger = Log.getLogger(EntitySeeder.name)

  async execute(): Promise<void> {
    const items = await this.getEntries()
    for (const item of items) {
      const exists = await this.exists(item.identifier)
      if (!exists) {
        const data = item.data()
        await this.create(data)
        this.logger.info(`Entity created`, {
          data,
        })
      }
    }
  }

  protected abstract getEntries(): Promise<
    IEntitySeedEntry<TEntity, TFindCondition>[]
  >
  protected abstract exists(condition: TFindCondition): Promise<boolean>
  protected abstract create(entity: Partial<TEntity>): Promise<void>

  abstract get priority(): number | undefined
}
