import { IEntitySnapshotService } from "../abstractions"

export abstract class EntitySnapshotService<TEntityId, TEntitySnapshot>
  implements IEntitySnapshotService<TEntityId, TEntitySnapshot>
{
  constructor(protected readonly entityName: string) {}

  abstract getSnapshot(id: TEntityId): Promise<TEntitySnapshot>
}
