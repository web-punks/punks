import {
  ExcelKeyTransform,
  ILogger,
  Log,
  csvBuild,
  csvParse,
  excelBuild,
  excelParse,
} from "@punks/backend-core"
import { EntitySerializationFormat, IEntitySerializer } from "../abstractions"
import {
  EntityExportFile,
  EntitySerializerColumnDefinition,
  EntitySerializerColumnModifier,
  EntitySerializerSheetDefinition,
  ImportEntry,
  ImportEntryValidationError,
} from "../abstractions/serializer"
import { EntityServiceLocator } from "../providers/services"
import { EntityReference } from "../models"

const DEFAULT_DELIMITER = ";"
const DEFAULT_ARRAY_SEPARATOR = "|"

const MODIFIER_FNS: Record<
  EntitySerializerColumnModifier,
  (value: any) => any
> = {
  lowercase: (value) => value?.toString()?.toLowerCase(),
  uppercase: (value) => value?.toString()?.toUpperCase(),
  trim: (value) => value?.toString()?.trim(),
}

const applyModifiers = (
  value: any,
  modifiers?: EntitySerializerColumnModifier[]
) => {
  return modifiers
    ? modifiers.reduce((acc, x) => MODIFIER_FNS[x](acc), value)
    : value
}

const isValidValue = (value: any) => {
  if (typeof value === "string") return value.trim().length > 0

  return true
}

const normalizeSheetColumn = (column: string) =>
  column.replace(/ /g, "").toLowerCase()

const getRealSeparator = (
  separator?: string | string[] | RegExp | undefined
): string => {
  if (!separator) return DEFAULT_ARRAY_SEPARATOR
  if (Array.isArray(separator)) return separator[0]
  if (typeof separator === "string") return separator
  if (separator instanceof RegExp) {
    const separators = separator.source.split("|")
    if (separators.length > 1) return separators[0]
  }
  return DEFAULT_ARRAY_SEPARATOR
}

const splitArrayColumn = (
  value: any,
  separator?: string | string[] | RegExp
) => {
  return value?.toString().split(separator ?? DEFAULT_ARRAY_SEPARATOR) ?? []
}

const joinArrayColumn = (value: any[], separator?: string | RegExp) =>
  value
    ?.map((x) => x.toString().trim())
    .filter((x) => x)
    .join(getRealSeparator(separator))

export type EntitySerializerOptions = {
  delimiter?: string
}
export type ParseColumnValueInput<TSheetItem> = {
  row: any
  columnName: string
  parser: EntitySerializerColumnDefinition<TSheetItem>["parser"]
}

export abstract class EntitySerializer<
  TEntity,
  TEntityId,
  TEntitySearchParameters,
  TSheetItem,
  TContext,
  TPayload = unknown
> implements
    IEntitySerializer<TEntity, TEntityId, TEntitySearchParameters, TSheetItem>
{
  private readonly logger: ILogger
  protected readonly entityName: string

  constructor(
    protected readonly services: EntityServiceLocator<TEntity, TEntityId>,
    protected readonly options?: EntitySerializerOptions
  ) {
    this.entityName = services.getEntityName()
    this.logger = Log.getLogger(`${services.getEntityName()} -> Serializer`)
  }

  async export(
    filters?: TEntitySearchParameters,
    payload?: TPayload
  ): Promise<TSheetItem[]> {
    const entities = await this.loadEntities(filters ?? ({} as any))
    return await this.convertToSheetItems(entities, payload)
  }

  async import(items: TSheetItem[], payload?: TPayload): Promise<void> {
    this.logger.info(`Entities import -> started (${items.length} items)`)
    const context = await this.getContext()
    await this.importItems(items, context, payload)
    this.logger.info(`Entities import -> completed (${items.length} items)`)
  }

  async parse<TPayload>(
    data: Buffer,
    format: EntitySerializationFormat,
    payload?: TPayload
  ): Promise<ImportEntry<TSheetItem, TEntityId>[]> {
    const context = await this.getContext()
    const definition = await this.getDefinition(context, payload as any)
    switch (format) {
      case EntitySerializationFormat.Csv:
        return this.parseCsv(data, definition)
      case EntitySerializationFormat.Xlsx:
        return this.parseXlsx(data, definition)
      default:
        throw new Error(`Format ${format} not supported`)
    }
  }

  protected async importItems(
    items: TSheetItem[],
    context: TContext,
    payload?: TPayload
  ) {
    for (const [index, item] of items.entries()) {
      await this.importItem(item, context, payload)
      this.logger.info(`Entity imported -> ${index + 1}/${items.length}`)
    }
  }

  private parseCsv(
    data: Buffer,
    definition: EntitySerializerSheetDefinition<TSheetItem>
  ): ImportEntry<TSheetItem, TEntityId>[] {
    const records = csvParse(data, DEFAULT_DELIMITER)
    return records.map((x, i) => this.convertSheetRecord(x, definition, i))
  }

  private parseXlsx(
    data: Buffer,
    definition: EntitySerializerSheetDefinition<TSheetItem>
  ): ImportEntry<TSheetItem, TEntityId>[] {
    const dateColumns = definition.columns.filter(
      (x) => x.sheetParser === "date"
    )
    const records = excelParse(data, {
      keysTransform: ExcelKeyTransform.Lower,
      dateColumns: dateColumns.map((x) => x.name),
      ...(definition?.options ? definition.options : {}),
    })
    return records.map((x: any, i: number) =>
      this.convertSheetRecord(x, definition, i)
    )
  }

  private convertSheetRecord(
    record: Record<string, any>,
    definition: EntitySerializerSheetDefinition<TSheetItem>,
    rowIndex: number
  ): ImportEntry<TSheetItem, TEntityId> {
    if (
      definition.sheet?.useTypeColumn &&
      (!record._type || record._type !== this.entityName)
    ) {
      throw new Error(
        `Invalid record type ${record._type} -> record: \n${JSON.stringify(
          record
        )}`
      )
    }

    const entity = (definition.autoColumns ? { ...record } : {}) as TSheetItem
    for (const column of definition.columns) {
      const columnName =
        [column.name, ...(column?.aliases ?? [])].find(
          (x) => normalizeSheetColumn(x) in record
        ) ?? null
      if (!columnName) continue

      if (column.parseAction) {
        column.parseAction(record[normalizeSheetColumn(columnName)], entity)
        continue
      }
      if (typeof column.selector === "function") {
        throw new Error(
          "Function selectors are not supported with parseAction specified"
        )
      }

      const parsedColumn = this.parseColumnValue<TSheetItem>({
        row: record,
        columnName,
        parser: column.parser,
      })
      entity[column.selector] = column.array
        ? splitArrayColumn(parsedColumn, column.arraySeparator)
            .map((x: any) => applyModifiers(x, column.modifiers))
            .filter((x: any) => isValidValue(x))
        : applyModifiers(parsedColumn, column.modifiers)
      delete (entity as any)[columnName]
    }

    const validationErrors: ImportEntryValidationError[] = []
    for (const column of definition.columns) {
      for (const validator of column.validators ?? []) {
        const result = validator.fn(entity)
        if (!result.isValid) {
          for (const error of result.validationErrors) {
            validationErrors.push({
              errorCode: error.errorCode,
              column: {
                name: column.name,
                key: column.key,
                value: error.value,
              },
            })
          }
        }
      }
    }

    const idField = definition.columns.find(
      (x) => x.idColumn || x.selector === "id"
    )

    return {
      id: idField
        ? (this.selectColumnValue(entity, idField.selector) as TEntityId)
        : undefined,
      item: entity,
      rowIndex,
      status: {
        isValid: validationErrors.length === 0,
        validationErrors,
      },
    }
  }

  private parseColumnValue<T = TSheetItem>({
    row,
    columnName,
    parser,
  }: ParseColumnValueInput<T>) {
    const value = row[normalizeSheetColumn(columnName)]
    return parser ? parser(value) : value
  }

  async createSample(
    format: EntitySerializationFormat
  ): Promise<EntityExportFile> {
    return await this.buildSampleFile(format)
  }

  private async buildSampleFile(
    format: EntitySerializationFormat
  ): Promise<EntityExportFile> {
    const fileName = this.buildExportFileName({
      format,
      refDate: new Date(),
    })
    const context = await this.getContext()

    const definition = await this.getDefinition(context)
    switch (format) {
      case EntitySerializationFormat.Csv:
        return {
          fileName,
          contentType: "text/csv",
          content: Buffer.from(
            csvBuild(
              [{}],
              [
                ...(definition.sheet?.useTypeColumn
                  ? [
                      {
                        name: "_type",
                        value: () => this.entityName,
                      },
                    ]
                  : []),
                ...definition.columns.map((c) => ({
                  name: c.name,
                  value: () => c.sampleValue ?? "",
                })),
              ],
              {
                delimiter: DEFAULT_DELIMITER,
              }
            ),
            "utf-8"
          ),
        }

      case EntitySerializationFormat.Xlsx:
        return {
          fileName,
          contentType:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          content: Buffer.from(
            excelBuild<any>({
              data: [{}],
              sheetName: definition?.sheet?.name ?? this.entityName,
              columns: [
                ...(definition.sheet?.useTypeColumn
                  ? [
                      {
                        header: "_type",
                        value: () => this.entityName,
                      },
                    ]
                  : []),
                ...definition.columns.map((c) => ({
                  header: c.name,
                  value: () => c.sampleValue ?? "",
                  headerSize: c.colSpan,
                })),
              ],
            })
          ),
        }

      case EntitySerializationFormat.Json:
        throw new Error("Json sample not implemented")
    }
  }

  async serialize(
    data: TSheetItem[],
    format: EntitySerializationFormat,
    payload?: TPayload
  ): Promise<EntityExportFile> {
    return await this.buildExportFile(data, format, payload)
  }

  protected abstract getDefinition(
    context: TContext,
    payload?: TPayload
  ): Promise<EntitySerializerSheetDefinition<TSheetItem>>

  protected abstract loadEntities(
    filters?: TEntitySearchParameters
  ): Promise<TEntity[]>

  protected abstract convertToSheetItems(
    entities: TEntity[],
    payload?: TPayload
  ): Promise<TSheetItem[]>

  protected validateSheetItem(
    item: TSheetItem,
    allItems: TSheetItem[],
    context: TContext
  ) {}

  protected abstract importItem(
    item: TSheetItem,
    context: TContext,
    payload?: TPayload
  ): Promise<EntityReference<TEntityId>>

  protected async getContext() {
    return (await this.services
      .resolveAuthenticationContextProvider()
      ?.getContext()) as TContext
  }

  private async buildExportFile(
    data: TSheetItem[],
    format: EntitySerializationFormat,
    payload?: TPayload
  ): Promise<EntityExportFile> {
    const fileName = this.buildExportFileName({
      format,
      refDate: new Date(),
    })
    const context = await this.getContext()
    const definition = await this.getDefinition(context, payload)
    switch (format) {
      case EntitySerializationFormat.Csv:
        return {
          fileName,
          contentType: "text/csv",
          content: Buffer.from(
            csvBuild(
              data,
              [
                ...(definition.sheet?.useTypeColumn
                  ? [
                      {
                        name: "_type",
                        value: () => this.entityName,
                      },
                    ]
                  : []),
                ...definition.columns.map((c) => ({
                  name: c.name,
                  value: (item: any) => {
                    const value = this.getColumnValue(item, c)
                    return c.array
                      ? joinArrayColumn(value, c.arraySeparator)
                      : value
                  },
                })),
              ],
              {
                delimiter: DEFAULT_DELIMITER,
              }
            ),
            "utf-8"
          ),
        }

      case EntitySerializationFormat.Xlsx:
        return {
          fileName,
          contentType:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          content: Buffer.from(
            excelBuild<TSheetItem>({
              data,
              sheetName: this.entityName,
              columns: [
                ...(definition.sheet?.useTypeColumn
                  ? [
                      {
                        header: "_type",
                        value: () => this.entityName,
                      },
                    ]
                  : []),
                ...definition.columns.map((c) => ({
                  header: c.name,
                  value: (item: any) => {
                    const value = this.getColumnValue(item, c)
                    return c.array
                      ? joinArrayColumn(value, c.arraySeparator)
                      : value
                  },
                  headerSize: c.colSpan,
                })),
              ],
            })
          ),
        }

      case EntitySerializationFormat.Json:
        return {
          fileName,
          contentType: "application/json",
          content: Buffer.from(JSON.stringify(data), "utf-8"),
        }
    }
  }

  private getColumnValue(
    item: TSheetItem,
    definition: EntitySerializerColumnDefinition<TSheetItem>
  ) {
    const rawValue = this.selectColumnValue(item, definition.selector)
    return definition.converter
      ? definition.converter(rawValue, item)
      : rawValue
  }

  private selectColumnValue(
    item: TSheetItem,
    selector: keyof TSheetItem | ((item: TSheetItem) => any)
  ) {
    return typeof selector === "function" ? selector(item) : item[selector]
  }

  private buildExportFileName(input: {
    refDate: Date
    format: EntitySerializationFormat
  }) {
    return `${input.refDate.toISOString().replaceAll(":", "_")}_${
      this.entityName
    }.${input.format}`
  }
}
