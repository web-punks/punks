import {
  EntityOperationType,
  EntityOperationUnauthorizedException,
  IAuthenticationContext,
} from "../abstractions"
import { SortingType } from "../abstractions/common"
import { IEntitiesFindQuery } from "../abstractions/queries"
import { IEntitySearchParameters } from "../abstractions/searchParameters"
import { EntityServiceLocator } from "../providers/services"

export class EntitiesFindQuery<
  TEntity,
  TEntityId,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TUserContext
> implements
    IEntitiesFindQuery<TEntity, TEntitySearchParameters, TSorting, TCursor>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  public async execute(request: TEntitySearchParameters): Promise<TEntity> {
    const context = await this.getContext()
    await this.authorizeSearch(context)

    const result = await this.services
      .resolveQueryBuilder<
        unknown,
        TEntitySearchParameters,
        TSorting,
        TCursor,
        any,
        TUserContext
      >()
      .find(request, context as IAuthenticationContext<TUserContext>)

    const filteredEntities = await this.filterAllowedEntities([result], context)
    return filteredEntities[0]
  }

  private async getContext() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return undefined
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    return await contextService?.getContext()
  }

  private async filterAllowedEntities(
    entities: TEntity[],
    context?: IAuthenticationContext<unknown>
  ): Promise<TEntity[]> {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return entities
    }

    const filteredEntities = await Promise.all(
      entities.map(async (entity) => {
        const authorizationResult = await authorization.canRead(
          entity,
          context as IAuthenticationContext<unknown>
        )
        if (!authorizationResult.isAuthorized) {
          return null
        }
        return entity
      })
    )

    return filteredEntities.filter((entity) => entity !== null) as TEntity[]
  }

  private async authorizeSearch(context?: IAuthenticationContext<unknown>) {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const { isAuthorized } = await authorization.canSearch(
      context as IAuthenticationContext<unknown>
    )

    if (!isAuthorized) {
      throw new EntityOperationUnauthorizedException<TEntity>(
        EntityOperationType.Find,
        this.services.getEntityName()
      )
    }
  }
}
