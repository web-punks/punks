import { IAuthenticationContext, ISearchFilters } from "../abstractions"
import {
  EntityOperationType,
  EntityOperationUnauthorizedException,
} from "../abstractions/errors"
import { IEntityExistsQuery } from "../abstractions/queries"
import { EntityServiceLocator } from "../providers/services"

export class EntityExistsQuery<
  TEntity,
  TEntityFilters extends ISearchFilters,
  TUserContext
> implements IEntityExistsQuery<TEntity, TEntityFilters>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {}

  public async execute(filters: TEntityFilters) {
    const context = await this.getContext()
    await this.authorizeSearch(context)

    return await this.services
      .resolveQueryBuilder<any, any, any, any, any, TUserContext>()
      .exists(filters, context as IAuthenticationContext<TUserContext>)
  }

  private async getContext() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return undefined
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    return await contextService?.getContext()
  }

  private async authorizeSearch(context?: IAuthenticationContext<unknown>) {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const { isAuthorized } = await authorization.canSearch(
      context as IAuthenticationContext<unknown>
    )

    if (!isAuthorized) {
      throw new EntityOperationUnauthorizedException<TEntity>(
        EntityOperationType.Search,
        this.services.getEntityName()
      )
    }
  }
}
