import { IAuthenticationContext } from "../abstractions"
import {
  EntityOperationType,
  EntityOperationUnauthorizedException,
} from "../abstractions/errors"
import { IEntityVersionsSearchQuery } from "../abstractions/queries"
import {
  IEntityVersionsSearchParameters,
  IEntityVersionsSearchResults,
} from "../models"
import { EntityServiceLocator } from "../providers/services"

export class EntityVersionsSearchQuery<TEntity, TCursor>
  implements IEntityVersionsSearchQuery<TEntity, TCursor>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, unknown>
  ) {}

  async execute(
    params: IEntityVersionsSearchParameters<TCursor>
  ): Promise<IEntityVersionsSearchResults<TEntity, TCursor>> {
    const context = await this.getContext()
    await this.authorizeSearch(context)

    return await this.services
      .resolveVersioningProvider()
      .searchVersions<TEntity, TCursor>({
        params,
        entityType: this.services.getEntityName(),
      })
  }

  private async getContext() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return undefined
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    return await contextService?.getContext()
  }

  private async authorizeSearch(context?: IAuthenticationContext<unknown>) {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const { isAuthorized } = await authorization.canSearch(
      context as IAuthenticationContext<unknown>
    )

    if (!isAuthorized) {
      throw new EntityOperationUnauthorizedException<TEntity>(
        EntityOperationType.Search,
        this.services.getEntityName()
      )
    }
  }
}
