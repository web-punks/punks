import {
  EntityOperationType,
  EntityOperationUnauthorizedException,
} from "../abstractions/errors"
import {
  IEntityGetQuery,
  IEntityGetQueryOptions,
} from "../abstractions/queries"
import { IEntitySearchParameters } from "../abstractions/searchParameters"
import { EntityServiceLocator } from "../providers/services"

export class EntityGetQuery<
  TEntity,
  TEntityId,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    unknown,
    unknown
  >
> implements IEntityGetQuery<TEntity, TEntityId, TEntitySearchParameters>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  public async execute(
    id: TEntityId,
    options?: IEntityGetQueryOptions<TEntity, TEntitySearchParameters>
  ) {
    const context = await this.getContext()

    const entity = await this.services
      .resolveQueryBuilder<unknown, any, any, unknown, any, any>()
      .get(id, context, options)

    if (entity) {
      await this.authorize(entity)
    }

    return entity
  }

  private async authorize(entity: TEntity) {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    const context = await contextService?.getContext()
    if (!context) {
      return
    }

    const authorizationResult = await authorization.canRead(entity, context)
    if (!authorizationResult.isAuthorized)
      throw new EntityOperationUnauthorizedException<TEntity>(
        EntityOperationType.Read,
        this.services.getEntityName(),
        entity
      )
  }

  private async getContext() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return undefined
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    return await contextService?.getContext()
  }
}
