import {
  EntityOperationType,
  EntityOperationUnauthorizedException,
  IAuthenticationContext,
  IEntityFacets,
} from "../abstractions"
import { SortingType } from "../abstractions/common"
import {
  IEntitiesSearchQuery,
  IEntitiesSearchQueryOptions,
} from "../abstractions/queries"
import { IEntitySearchParameters } from "../abstractions/searchParameters"
import { IEntitiesSearchResults } from "../models"
import { EntityServiceLocator } from "../providers/services"

export class EntitiesSearchQuery<
  TEntity,
  TEntityId,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets,
  TUserContext
> implements
    IEntitiesSearchQuery<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets
    >
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  public async execute(
    request: TEntitySearchParameters,
    options?: IEntitiesSearchQueryOptions<TEntity, TEntitySearchParameters>
  ): Promise<
    IEntitiesSearchResults<
      TEntity,
      TEntitySearchParameters,
      TEntity,
      TSorting,
      TCursor,
      TFacets
    >
  > {
    const context = await this.getContext()
    await this.authorizeSearch(context)

    const queryBuilder = this.services.resolveQueryBuilder<
      unknown,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets,
      TUserContext
    >()
    const result = await queryBuilder.search(
      {
        ...request,
        relations: options?.relations ?? request.relations,
      },
      context as IAuthenticationContext<TUserContext>
    )

    const filteredEntities = await this.filterAllowedEntities(
      result.items,
      context
    )

    return {
      ...result,
      items: filteredEntities,
      paging: result.paging
        ? {
            ...result.paging,
            totPageItems: filteredEntities.length,
          }
        : undefined,
    }
  }

  private async getContext() {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return undefined
    }

    const contextService = this.services.resolveAuthenticationContextProvider()
    return await contextService?.getContext()
  }

  private async filterAllowedEntities(
    entities: TEntity[],
    context?: IAuthenticationContext<unknown>
  ): Promise<TEntity[]> {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return entities
    }

    const filteredEntities = await Promise.all(
      entities.map(async (entity) => {
        const authorizationResult = await authorization.canRead(
          entity,
          context as IAuthenticationContext<unknown>
        )
        if (!authorizationResult.isAuthorized) {
          return null
        }
        return entity
      })
    )

    return filteredEntities.filter((entity) => entity !== null) as TEntity[]
  }

  private async authorizeSearch(context?: IAuthenticationContext<unknown>) {
    const authorization = this.services.resolveAuthorizationMiddleware()
    if (!authorization) {
      return
    }

    const { isAuthorized } = await authorization.canSearch(
      context as IAuthenticationContext<unknown>
    )

    if (!isAuthorized) {
      throw new EntityOperationUnauthorizedException<TEntity>(
        EntityOperationType.Search,
        this.services.getEntityName()
      )
    }
  }
}
