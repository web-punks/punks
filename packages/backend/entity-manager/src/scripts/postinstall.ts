const shell = require("shelljs")

const isLibRepo = () => {
  return process.cwd().includes("packages/backend/entity-manager")
}

const run = () => {
  if (!isLibRepo()) {
    return
  }

  shell.exec("yarn patch:package")
}

run()
