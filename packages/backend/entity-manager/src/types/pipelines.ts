export type PipelineStepReference = {
  index: number
  key?: string
  name: string
}

export type PipelineStepState<TPipelineInput, TContext, TStepInput> = {
  reference: PipelineStepReference
  context: TContext
  stepInput: TStepInput
  pipelineInput: TPipelineInput
  previousStep?: PipelineCompletedStepState<
    TPipelineInput,
    TContext,
    any,
    TStepInput
  >
}

export type PipelineCompletedStepState<
  TPipelineInput,
  TContext,
  TStepInput,
  TStepOutput
> = PipelineStepState<TPipelineInput, TContext, TStepInput> & {
  stepOutput?: TStepOutput
}

export type PipelineCurrentStepState<TPipelineInput, TContext, TStepInput> =
  PipelineStepState<TPipelineInput, TContext, TStepInput> & {}

export type OperationDefinition<
  TOperationInput,
  TOperationOutput,
  TCurrentStepInput,
  TPipelineInput,
  TContext
> = {
  key?: string
  name: string
  action: (
    input: TOperationInput,
    state: PipelineCurrentStepState<TPipelineInput, TContext, TCurrentStepInput>
  ) => Promise<TOperationOutput> | TOperationOutput
  precondition?: (
    input: TOperationInput,
    state: PipelineCurrentStepState<TPipelineInput, TContext, TCurrentStepInput>
  ) => Promise<boolean> | boolean
  skipIf?: (
    input: TOperationInput,
    state: PipelineCurrentStepState<TPipelineInput, TContext, TCurrentStepInput>
  ) => Promise<boolean> | boolean
}

export type RollbackOperationDefinition<
  TOperationInput,
  TOperationOutput,
  TCurrentStepInput,
  TPipelineInput,
  TContext
> = {
  name: string
  action: (
    input: TOperationInput,
    output: TOperationOutput,
    state: PipelineCurrentStepState<TPipelineInput, TContext, TCurrentStepInput>
  ) => Promise<void> | void
}

export type PipelineStepOperation<
  TOperationInput,
  TOperationOutput,
  TCurrentStepInput,
  TPipelineInput,
  TContext
> = {
  key?: string
  name: string
  operation: OperationDefinition<
    TOperationInput,
    TOperationOutput,
    TCurrentStepInput,
    TPipelineInput,
    TContext
  >
  rollbackOperations?: RollbackOperationDefinition<
    TOperationInput,
    TOperationOutput,
    TCurrentStepInput,
    TPipelineInput,
    TContext
  >[]
}

export type PipelineStep<TStepInput, TStepOutput, TPipelineInput, TContext> = {
  name: string
  operations: PipelineStepOperation<
    TStepInput,
    TStepOutput,
    TStepInput,
    TPipelineInput,
    TContext
  >[]
  outputsReducer?: (results: unknown[]) => TStepOutput
}

export type PipelineDefinition<TPipelineInput, TPipelineOutput, TContext> = {
  steps: PipelineStep<unknown, unknown, TPipelineInput, TContext>[]
}

export type PipelineOperationError = {
  message: string
  stackTrace?: string
  exception?: Error
}

export type PipelineOperationResult<TOperationInput, TOperationOutput> = {
  key?: string
  name: string
  input: TOperationInput
} & (
  | {
      type: "success"
      output: TOperationOutput
    }
  | {
      type: "skipped"
    }
  | {
      type: "preconditionFailed"
    }
  | {
      type: "error" | "rollbackCompleted"
      error: PipelineOperationError
    }
  | {
      type: "rollbackFailed"
      rollbackError: PipelineOperationError
      error: PipelineOperationError
    }
)
export type PipelineOperationResultType = PipelineOperationResult<
  unknown,
  unknown
>["type"]

export type PipelineOperationRollbackResult =
  | {
      type: "success" | "skipped"
    }
  | {
      type: "error"
      error: PipelineOperationError
    }

export type PipelineOperationRollbackResultType =
  PipelineOperationRollbackResult["type"]

export enum PipelineStepErrorType {
  FailedPrecondition = "failedPrecondition",
  OperationError = "operationError",
  RollbackError = "rollbackError",
  GenericError = "genericError",
}

export type PipelineStepResult<TStepInput, TStepOutput> = {
  input: TStepInput
  operationResults: PipelineOperationResult<unknown, unknown>[]
} & (
  | {
      type: "success"
      output: TStepOutput
    }
  | {
      type: "error"
      errorType: PipelineStepErrorType
    }
)

export type PipelineStepResultType = PipelineStepResult<
  unknown,
  unknown
>["type"]

export type PipelineStepRollbackResult = {} & (
  | {
      type: "success"
    }
  | {
      type: "error"
    }
)

export type PipelineStepRollbackResultType = PipelineStepRollbackResult["type"]

export enum PipelineErrorType {
  FailedPrecondition = "failedPrecondition",
  OperationError = "operationError",
  RollbackError = "rollbackError",
  Unauthorized = "unauthorized",
  GenericError = "genericError",
}

export type PipelineResult<TPipelineInput, TPipelineOutput> = {
  stepResults: PipelineStepResult<unknown, unknown>[]
  input: TPipelineInput
} & (
  | {
      type: "success"
      output: TPipelineOutput
    }
  | {
      type: "error"
      errorType: PipelineErrorType
      exception?: Error
      output?: TPipelineOutput
    }
)

export type PipelineResultType = PipelineResult<unknown, unknown>["type"]

export enum PipelineStatus {
  Initializing = "initializing",
  Running = "running",
  RollbackInProgress = "rollbackInProgress",
  Completed = "completed",
  Failed = "failed",
}
