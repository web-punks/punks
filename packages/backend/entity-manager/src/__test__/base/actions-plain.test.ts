import { IEntityActions } from "../../abstractions/actions"
import { createContainer } from "../../configuration"
import { EntityServiceLocator } from "../../providers/services"
import {
  FooEntity,
  FooRepository,
  FooSearchParameters,
  registerFooEntity,
} from "../entity/foo"

type FooEntityActions = IEntityActions<
  FooEntity,
  string,
  FooEntity,
  Partial<FooEntity>,
  unknown,
  unknown,
  any,
  any,
  any,
  unknown,
  any
>

describe("Test plain actions", () => {
  const container = createContainer()
  let fooServices: EntityServiceLocator<FooEntity, string>
  let fooEntityActions: FooEntityActions
  let fooRepository: FooRepository

  beforeEach(() => {
    const locator = registerFooEntity(container)
    fooServices = locator.getServiceLocator()
    fooEntityActions = fooServices.resolveEntityActions()
    fooRepository = fooServices.resolveRepository() as FooRepository
  })

  it("should create a new entity", async () => {
    const dto = await fooEntityActions.create.execute({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })
    expect(dto).toEqual({
      id: expect.any(String),
      age: 10,
      name: "foo",
      updatedOn: expect.any(Date),
    })

    const items = await fooRepository.all()
    expect(items).toHaveLength(1)
    expect(items[0]).toEqual({
      id: expect.any(String),
      age: 10,
      name: "foo",
      updatedOn: expect.any(Date),
    })
  })

  it("should update a new entity", async () => {
    await fooRepository.create({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    const dto = await fooEntityActions.update.execute("1", {
      age: 20,
      name: "foo2",
      updatedOn: new Date(),
    })
    expect(dto).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })

    const item = await fooRepository.get("1")
    expect(item).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })
  })

  it("should upsert an existing entity", async () => {
    await fooRepository.create({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    const dto = await fooEntityActions.upsert.execute("1", {
      age: 20,
      name: "foo2",
      updatedOn: new Date(),
    })
    expect(dto).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })

    const item = await fooRepository.get("1")
    expect(item).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })
  })

  it("should upsert an new entity", async () => {
    const dto = await fooEntityActions.upsert.execute("1", {
      age: 20,
      name: "foo2",
      updatedOn: new Date(),
    })
    expect(dto).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })

    const item = await fooRepository.get("1")
    expect(item).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })
  })

  it("should delete an entity", async () => {
    await fooRepository.create({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await fooEntityActions.delete.execute("1")

    const item = await fooRepository.get("1")
    expect(item).toBeUndefined()
  })

  it("should throw error on delete without id", async () => {
    const deleteAction = fooEntityActions.delete.execute(undefined as any)
    await expect(deleteAction).rejects.toThrowError()
  })

  it("should search entities", async () => {
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        minAge: 20,
      },
    }
    const results = await fooEntityActions.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "2",
          age: 20,
          name: "foo2",
          updatedOn: expect.any(Date),
        },
        {
          id: "3",
          age: 30,
          name: "foo3",
          updatedOn: expect.any(Date),
        },
        {
          id: "5",
          age: 50,
          name: "foo4",
          updatedOn: expect.any(Date),
        },
      ],
    })
  })

  it("should search entities with paging from beginning", async () => {
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        minAge: 20,
      },
      paging: {
        pageSize: 2,
      },
    }
    const results = await fooEntityActions.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "2",
          age: 20,
          name: "foo2",
          updatedOn: expect.any(Date),
        },
        {
          id: "3",
          age: 30,
          name: "foo3",
          updatedOn: expect.any(Date),
        },
      ],
      paging: {
        pageIndex: 0,
        pageSize: 2,
        totItems: 3,
        totPages: 2,
        totPageItems: 2,
        currentPageCursor: 0,
        nextPageCursor: 1,
      },
    })
  })

  it("should search entities with paging from page 2", async () => {
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        minAge: 20,
      },
      paging: {
        cursor: 1,
        pageSize: 2,
      },
    }
    const results = await fooEntityActions.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "5",
          age: 50,
          name: "foo4",
          updatedOn: expect.any(Date),
        },
      ],
      paging: {
        pageIndex: 1,
        pageSize: 2,
        totItems: 3,
        totPages: 2,
        totPageItems: 1,
        currentPageCursor: 1,
      },
    })
  })

  const mockSearchEntities = async () => {
    await fooRepository.create({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })
    await fooRepository.create({
      id: "2",
      age: 20,
      name: "foo2",
      updatedOn: new Date(),
    })
    await fooRepository.create({
      id: "3",
      age: 30,
      name: "foo3",
      updatedOn: new Date(),
    })
    await fooRepository.create({
      id: "4",
      age: 10,
      name: "foo4",
      updatedOn: new Date(),
    })
    await fooRepository.create({
      id: "5",
      age: 50,
      name: "foo4",
      updatedOn: new Date(),
    })
  }
})
