import { IEntityActions } from "../../abstractions/actions"
import { createContainer } from "../../configuration"
import { EntityServiceLocator } from "../../providers/services"
import {
  FooCreateDto,
  FooCursor,
  FooDto,
  FooEntity,
  FooFacets,
  FooListItemDto,
  FooRepository,
  FooSearchParameters,
  FooSorting,
  FooUpdateDto,
  registerFooEntity,
} from "../entity/foo"
import { FooDeleteParameters } from "../providers/typeorm/models/foo"

type FooEntityActions = IEntityActions<
  FooEntity,
  string,
  FooCreateDto,
  FooUpdateDto,
  FooDto,
  FooListItemDto,
  FooDeleteParameters,
  FooSearchParameters,
  FooSorting,
  FooCursor,
  FooFacets
>

describe("Test actions with converters", () => {
  const container = createContainer()
  let fooServices: EntityServiceLocator<FooEntity, string>
  let fooEntityActions: FooEntityActions
  let fooRepository: FooRepository

  beforeEach(() => {
    const locator = registerFooEntity(container, {
      useConverter: true,
    })
    fooServices = locator.getServiceLocator()
    fooEntityActions = fooServices.resolveEntityActions() as FooEntityActions
    fooRepository = fooServices.resolveRepository() as FooRepository
  })

  it("should create a new entity", async () => {
    const dto = await fooEntityActions.create.execute({
      profile: {
        age: 10,
        name: "foo",
      },
    })
    expect(dto).toEqual({
      id: expect.any(String),
      profile: {
        age: 10,
        name: "foo",
      },
      updatedOn: expect.any(Date),
    })

    const items = await fooRepository.all()
    expect(items).toHaveLength(1)
    expect(items[0]).toEqual({
      id: expect.any(String),
      age: 10,
      name: "foo",
      updatedOn: expect.any(Date),
    })
  })

  it("should update a new entity", async () => {
    await fooRepository.create({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    const dto = await fooEntityActions.update.execute("1", {
      profile: {
        age: 20,
        name: "foo2",
      },
    })
    expect(dto).toEqual({
      id: "1",
      profile: {
        age: 20,
        name: "foo2",
      },
      updatedOn: expect.any(Date),
    })

    const item = await fooRepository.get("1")
    expect(item).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })
  })

  it("should upsert an existing entity", async () => {
    await fooRepository.create({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    const dto = await fooEntityActions.upsert.execute("1", {
      profile: {
        age: 20,
        name: "foo2",
      },
    })
    expect(dto).toEqual({
      id: "1",
      profile: {
        age: 20,
        name: "foo2",
      },
      updatedOn: expect.any(Date),
    })

    const item = await fooRepository.get("1")
    expect(item).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })
  })

  it("should upsert an new entity", async () => {
    const dto = await fooEntityActions.upsert.execute("1", {
      profile: {
        age: 20,
        name: "foo2",
      },
    })
    expect(dto).toEqual({
      id: "1",
      profile: {
        age: 20,
        name: "foo2",
      },
      updatedOn: expect.any(Date),
    })

    const item = await fooRepository.get("1")
    expect(item).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })
  })

  it("should delete an entity", async () => {
    await fooRepository.create({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await fooEntityActions.delete.execute("1")

    const item = await fooRepository.get("1")
    expect(item).toBeUndefined()
  })
})
