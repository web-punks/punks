import { createContainer } from "../../configuration"
import { EntityServiceLocator } from "../../providers/services"
import {
  FooEntity,
  FooEntityManager,
  FooRepository,
  registerFooEntity,
} from "../entity/foo"
import { EntitySerializationFormat } from "../../abstractions"

describe("Test default entity manager", () => {
  const container = createContainer()
  let fooServices: EntityServiceLocator<FooEntity, string>
  let fooEntityManager: FooEntityManager
  let fooRepository: FooRepository

  beforeEach(() => {
    const locator = registerFooEntity(container)
    fooServices = locator.getServiceLocator()
    fooEntityManager = fooServices.resolveEntityManager() as FooEntityManager
    fooRepository = fooServices.resolveRepository() as FooRepository
  })

  it("should create a new entity", async () => {
    await fooEntityManager.create.execute({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    const item = await fooRepository.get("1")
    expect(item).toEqual({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: expect.any(Date),
    })
  })

  it("should update a new entity", async () => {
    await fooRepository.create({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await fooEntityManager.update.execute("1", {
      age: 20,
      name: "foo2",
      updatedOn: new Date(),
    })

    const item = await fooRepository.get("1")
    expect(item).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })
  })

  it("should upsert an existing entity", async () => {
    await fooRepository.create({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await fooEntityManager.upsert.execute("1", {
      age: 20,
      name: "foo2",
      updatedOn: new Date(),
    })

    const item = await fooRepository.get("1")
    expect(item).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })
  })

  it("should upsert an new entity", async () => {
    await fooEntityManager.upsert.execute("1", {
      age: 20,
      name: "foo2",
      updatedOn: new Date(),
    })

    const item = await fooRepository.get("1")
    expect(item).toEqual({
      id: "1",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })
  })

  it("should delete an entity", async () => {
    await fooRepository.create({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await fooEntityManager.delete.execute("1")

    const item = await fooRepository.get("1")
    expect(item).toBeUndefined()
  })

  it("should download entities", async () => {
    await fooRepository.create({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date("2020-01-01T00:00:00.000Z"),
    })
    await fooRepository.create({
      id: "2",
      age: 20,
      name: "foo2",
      updatedOn: new Date("2020-01-01T00:00:00.000Z"),
    })

    const csvExport = await fooEntityManager.export.execute({
      options: {
        format: EntitySerializationFormat.Csv,
      },
    })
    expect(csvExport.file.content.toString("utf-8")).toMatchSnapshot()

    const xlsExport = await fooEntityManager.export.execute({
      options: {
        format: EntitySerializationFormat.Xlsx,
      },
    })
    expect(xlsExport.file.content.toString("utf-8")).toMatchSnapshot()

    const jsonExport = await fooEntityManager.export.execute({
      options: {
        format: EntitySerializationFormat.Json,
      },
    })
    expect(jsonExport.file.content.toString("utf-8")).toMatchSnapshot()
  })

  it("should download sheet sample", async () => {
    const csvExport = await fooEntityManager.sampleDownload.execute({
      format: EntitySerializationFormat.Csv,
    })
    expect(csvExport.file.content.toString("utf-8")).toMatchSnapshot()

    const xlsExport = await fooEntityManager.sampleDownload.execute({
      format: EntitySerializationFormat.Xlsx,
    })
    expect(xlsExport.file.content.toString("utf-8")).toMatchSnapshot()
  })
})
