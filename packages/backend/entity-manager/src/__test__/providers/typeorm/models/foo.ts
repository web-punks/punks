import {
  IEntitiesDeleteParameters,
  IEntitySearchParameters,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../abstractions"
import {
  BooleanFilter,
  NumericFilter,
  StringFilter,
} from "../../../../integrations/repository/typeorm/clause"
import { FooEntity } from "../entity/foo"

export interface FooSearchFilters {
  age?: NumericFilter
  name?: StringFilter
  enabled?: BooleanFilter
}

export enum FooSorting {
  Name = "Name",
  Age = "Age",
}

export type FooCreateData = Partial<FooEntity>
export type FooUpdateData = Partial<FooEntity>

export type FooCursor = number

export interface FooSearchParameters
  extends IEntitySearchParameters<FooEntity, FooSorting, FooCursor> {
  filters?: FooSearchFilters
}

export interface FooSearchSortingField extends ISearchSortingField<FooSorting> {
  field: FooSorting
  direction: SortDirection
}

export interface FooQuerySorting extends ISearchSorting<FooSorting> {
  fields: FooSearchSortingField[]
}

export interface FooDeleteParameters
  extends IEntitiesDeleteParameters<FooSorting> {
  filters?: FooSearchFilters
  sorting?: FooQuerySorting
}

export interface FooFacets {}

export interface FooDto {
  id: string
  profile: {
    name: string
    age: number
  }
  updatedOn: Date
}

export interface FooListItemDto {
  id: string
  profile: {
    name: string
    age: number
  }
}

export interface FooCreateInput {
  profile: {
    name: string
    age: number
  }
}

export interface FooUpdateInput {
  id: string
  profile: {
    name: string
    age: number
  }
}

export type FooSheetItem = FooEntity
