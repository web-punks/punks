import { EntitySerializerSheetDefinition } from "../../../../abstractions"
import { EntitySerializer } from "../../../../base"
import { EntityServiceLocator } from "../../../../providers/services"
import { FooEntity, FooEntityId } from "../entity/foo"
import { FooSearchParameters, FooSheetItem } from "../models/foo"

export class FooSerializer extends EntitySerializer<
  FooEntity,
  FooEntityId,
  FooSearchParameters,
  FooSheetItem,
  any
> {
  constructor(services: EntityServiceLocator<FooEntity, FooEntityId>) {
    super(services)
  }

  protected async loadEntities(
    filters?: FooSearchParameters | undefined
  ): Promise<FooEntity[]> {
    return await this.services.resolveRepository().find({})
  }

  protected convertToSheetItems(entities: FooEntity[]): Promise<FooEntity[]> {
    return Promise.resolve(entities)
  }

  protected async importItem(item: FooEntity): Promise<FooEntity> {
    if (await this.services.resolveRepository().exists(item.id)) {
      return await this.services.resolveRepository().update(item.id, item)
    }
    return await this.services.resolveRepository().create(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<FooEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
        {
          name: "Name",
          key: "name",
          selector: "name",
          parser: (value) => (value?.toString() as string).toUpperCase(),
          converter: (value) => (value?.toString() as string).toLowerCase(),
          sampleValue: "Foo",
        },
        {
          name: "Age",
          key: "age",
          selector: "age",
          parser: (value) => parseInt(value?.toString() as string),
          sampleValue: 18,
        },
      ],
    }
  }
}
