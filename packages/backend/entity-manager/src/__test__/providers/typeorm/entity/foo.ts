import { Entity, PrimaryColumn, Column } from "typeorm"

export type FooEntityId = string

@Entity()
export class FooEntity {
  @PrimaryColumn({ type: "varchar" })
  id: FooEntityId

  @Column({ type: "varchar" })
  name: string

  @Column({ type: "int", nullable: true })
  age: number

  @Column({ type: "boolean", nullable: true })
  enabled: boolean

  @Column({ type: "timestamptz" })
  updatedOn: Date
}
