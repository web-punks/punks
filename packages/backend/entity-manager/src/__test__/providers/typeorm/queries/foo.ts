import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import { IAuthenticationContext } from "../../../../abstractions"
import { TypeOrmQueryBuilder } from "../../../../integrations"
import { EntityServiceLocator } from "../../../../providers/services"
import { FooEntity } from "../entity/foo"
import { FooFacets, FooSearchParameters, FooSorting } from "../models/foo"

export class FooQueryBuilder extends TypeOrmQueryBuilder<
  FooEntity,
  unknown,
  FooSearchParameters,
  FooSorting,
  FooFacets,
  unknown
> {
  constructor(services: EntityServiceLocator<FooEntity, string>) {
    super(services)
  }

  protected buildContextFilter(
    context?: IAuthenticationContext<unknown> | undefined
  ): FindOptionsWhere<FooEntity> | FindOptionsWhere<FooEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: FooSearchParameters
  ): FindOptionsOrder<FooEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: FooSearchParameters
  ): FindOptionsWhere<FooEntity> | FindOptionsWhere<FooEntity>[] {
    return {
      ...(!!request.filters?.age
        ? {
            age: this.clause.numericFilter(request.filters.age),
          }
        : {}),
      ...(!!request.filters?.name
        ? {
            name: this.clause.stringFilter(request.filters.name),
          }
        : {}),
      ...(!!request.filters?.enabled
        ? {
            enabled: this.clause.boolFilter(request.filters.enabled),
          }
        : {}),
    }
  }

  protected calculateFacets(request: FooSearchParameters): Promise<FooFacets> {
    return Promise.resolve({})
  }
}
