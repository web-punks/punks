import { Repository } from "typeorm"
import { TypeOrmRepository } from "../../../../integrations"
import { FooEntity } from "../entity/foo"

export class FooTypeOrmRepo extends TypeOrmRepository<FooEntity, string> {
  constructor(repository: Repository<FooEntity>) {
    super(repository)
  }
}
