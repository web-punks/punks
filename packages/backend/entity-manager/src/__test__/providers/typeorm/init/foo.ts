import { DataSource } from "typeorm"
import { IEntityManagerServiceRoot } from "../../../../abstractions"
import { InMemoryBucketProvider } from "../../../../platforms"
import { FooEntity } from "../entity/foo"
import { FooQueryBuilder } from "../queries/foo"
import { FooSerializer } from "../serializer/foo"
import { FooConverter } from "../converter/foo"
import { FooTypeOrmRepo } from "../repository/foo"
import { InMemoryFileProvider } from "../../../../platforms/nest/plugins/buckets/testing/mock"

export const registerFooTypeOrmEntity = (
  container: IEntityManagerServiceRoot,
  dataSource: DataSource,
  options?: {
    useConverter?: boolean
  }
) => {
  container
    .getEntitiesServicesLocator()
    .registerSettings({
      importExport: {
        exportBucket: {
          bucket: "test",
          publicLinksExpirationMinutes: 60,
          rootFolderPath: "test",
        },
      },
      defaultBucketProvider: "inMemory",
      defaultFilesProvider: "inMemory",
      defaultMediaProvider: "inMemory",
    })
    .registerBucketProvider("inMemory", new InMemoryBucketProvider())
    .registerFileProvider("inMemory", new InMemoryFileProvider())

  const repo = new FooTypeOrmRepo(dataSource.getRepository(FooEntity))

  const c = container.registerEntity<FooEntity, string, FooTypeOrmRepo>(
    {
      name: "Foo",
    },
    repo
  )
  c.mapCrudOperations({
    queryBuilder: new FooQueryBuilder(c.getServiceLocator()),
    settings: {
      importExport: {
        exportBucket: {
          bucket: "test",
          publicLinksExpirationMinutes: 60,
          rootFolderPath: "foo",
        },
      },
      defaultBucketProvider: "inMemory",
      defaultFilesProvider: "inMemory",
      defaultMediaProvider: "inMemory",
    },
  })
  c.addSerializer(new FooSerializer(c.getServiceLocator()))
  if (options?.useConverter) {
    c.addConverter(new FooConverter())
  }
  return c
}
