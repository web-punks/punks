import { Repository } from "typeorm"
import { mockPostgresDatabase } from "./mock"
import { newUuid } from "@punks/backend-core"
import { FooEntity } from "./entity/foo"
import { createContainer } from "../../../configuration"
import { EntitySerializationFormat } from "../../../abstractions"
import { FooEntityManager } from "./manager/foo"
import { FooSearchParameters } from "./models/foo"
import { registerFooTypeOrmEntity } from "./init/foo"

describe("Test typeorm actions", () => {
  const container = createContainer()
  let repository: Repository<FooEntity>
  let entityManager: FooEntityManager

  beforeEach(async () => {
    const db = await mockPostgresDatabase({ entities: [FooEntity] })
    repository = db.dataSource.getRepository(FooEntity)

    const locator = registerFooTypeOrmEntity(container, db.dataSource)
    const fooServices = locator.getServiceLocator()
    entityManager = fooServices.resolveEntityManager() as FooEntityManager
  })

  it("should create a new entity using repository only", async () => {
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    const item = await repository.findOneOrFail({
      where: {
        id,
      },
    })

    expect(item).toEqual({
      id,
      age: 10,
      name: "foo",
      enabled: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should create a new entity", async () => {
    const id = newUuid()
    await entityManager.create.execute({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toEqual({
      id,
      age: 10,
      name: "foo",
      enabled: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should update a full entity", async () => {
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await entityManager.update.execute(id, {
      age: 200,
      name: "bar2",
      enabled: false,
      updatedOn: new Date(),
    })

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toEqual({
      id,
      age: 200,
      name: "bar2",
      enabled: false,
      updatedOn: expect.any(Date),
    })
  })

  it("should update a partial entity", async () => {
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await entityManager.update.execute(id, {
      name: "bar2",
      updatedOn: new Date(),
    })

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toEqual({
      id,
      age: 10,
      name: "bar2",
      enabled: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should delete a entity", async () => {
    const id = newUuid()
    const id2 = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })
    await repository.insert({
      id: id2,
      age: 20,
      name: "foo2",
      updatedOn: new Date(),
    })

    await entityManager.delete.execute(id)

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toBeNull()

    const item2 = await repository.findOneBy({
      id: id2,
    })
    expect(item2).toMatchObject({
      id: id2,
    })
  })

  it("should delete not delete entities if no id", async () => {
    const deleteAction = entityManager.delete.execute(undefined as any)
    await expect(deleteAction).rejects.toThrowError()
  })

  it("should search entities with numeric filter", async () => {
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        age: {
          gte: 20,
        },
      },
    }
    const results = await entityManager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "2",
          age: 20,
          name: "foo2",
          updatedOn: expect.any(Date),
        },
        {
          id: "3",
          age: 30,
          name: "foo3",
          updatedOn: expect.any(Date),
        },
        {
          id: "5",
          age: 50,
          name: "foo4",
          updatedOn: expect.any(Date),
        },
      ],
    })
  })

  it("should search entities with paging from beginning", async () => {
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        age: {
          gte: 20,
        },
      },
      paging: {
        pageSize: 2,
      },
    }
    const results = await entityManager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "2",
          age: 20,
          name: "foo2",
          updatedOn: expect.any(Date),
        },
        {
          id: "3",
          age: 30,
          name: "foo3",
          updatedOn: expect.any(Date),
        },
      ],
      paging: {
        pageIndex: 0,
        pageSize: 2,
        totItems: 3,
        totPages: 2,
        totPageItems: 2,
        currentPageCursor: 0,
        nextPageCursor: 1,
      },
    })
  })

  it("should search entities with paging from page 2", async () => {
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        age: {
          gte: 20,
        },
      },
      paging: {
        cursor: 1,
        pageSize: 2,
      },
    }
    const results = await entityManager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "5",
          age: 50,
          name: "foo4",
          updatedOn: expect.any(Date),
        },
      ],
      paging: {
        pageIndex: 1,
        pageSize: 2,
        totItems: 3,
        totPages: 2,
        totPageItems: 1,
        currentPageCursor: 1,
      },
    })
  })

  it("should search entities with string filter", async () => {
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        name: {
          like: "*oo4",
        },
      },
    }
    const results = await entityManager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "4",
          age: 10,
          name: "foo4",
          enabled: true,
          updatedOn: expect.any(Date),
        },
        {
          id: "5",
          age: 50,
          name: "foo4",
          enabled: false,
          updatedOn: expect.any(Date),
        },
      ],
    })
  })

  it("should search entities with boolean filter", async () => {
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        enabled: {
          eq: false,
        },
      },
    }
    const results = await entityManager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "3",
          age: 30,
          name: "foo3",
          enabled: false,
          updatedOn: expect.any(Date),
        },
        {
          id: "5",
          age: 50,
          name: "foo4",
          enabled: false,
          updatedOn: expect.any(Date),
        },
      ],
    })
  })

  it("should search entities with all filters", async () => {
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        age: {
          gte: 30,
        },
        enabled: {
          eq: false,
        },
        name: {
          like: "*oo4",
        },
      },
    }
    const results = await entityManager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "5",
          age: 50,
          name: "foo4",
          enabled: false,
          updatedOn: expect.any(Date),
        },
      ],
    })
  })

  it("should export entities", async () => {
    await mockSearchEntities()

    const csvExport = await entityManager.export.execute({
      options: {
        format: EntitySerializationFormat.Csv,
      },
    })
    expect(csvExport.file.content.toString("utf-8")).toMatchSnapshot()

    const xlsExport = await entityManager.export.execute({
      options: {
        format: EntitySerializationFormat.Xlsx,
      },
    })
    expect(xlsExport.file.content.toString("utf-8")).toMatchSnapshot()

    const jsonExport = await entityManager.export.execute({
      options: {
        format: EntitySerializationFormat.Json,
      },
    })
    expect(jsonExport.file.content.toString("utf-8")).toMatchSnapshot()
  })

  it("should import entities", async () => {
    await mockSearchEntities()

    const csvExport = await entityManager.export.execute({
      options: {
        format: EntitySerializationFormat.Csv,
      },
    })
    expect(csvExport.file.content.toString("utf-8")).toMatchSnapshot()

    await entityManager.deleteItems.execute({})

    let items = await repository.find()
    expect(items).toHaveLength(0)

    // await entityManager.import.execute({
    //   format: EntitySerializationFormat.Csv,
    //   fileName: csvExport.file.name,
    //   content: csvExport.file.content,
    //   contentType: csvExport.file.contentType,
    // })

    // items = await repository.find()
    // expect(items).toHaveLength(5)
    // expect(items).toMatchSnapshot()
  })

  it("should download sheet sample", async () => {
    const csvExport = await entityManager.sampleDownload.execute({
      format: EntitySerializationFormat.Csv,
    })
    expect(csvExport.file.content.toString("utf-8")).toMatchSnapshot()

    const xlsExport = await entityManager.sampleDownload.execute({
      format: EntitySerializationFormat.Xlsx,
    })
    expect(xlsExport.file.content.toString("utf-8")).toMatchSnapshot()
  })

  const mockSearchEntities = async () => {
    await repository.insert({
      id: "1",
      age: 10,
      name: "foo",
      enabled: true,
      updatedOn: new Date("2020-01-01T00:00:00.000Z"),
    })
    await repository.insert({
      id: "2",
      age: 20,
      name: "foo2",
      enabled: true,
      updatedOn: new Date("2020-01-01T00:00:00.000Z"),
    })
    await repository.insert({
      id: "3",
      age: 30,
      name: "foo3",
      enabled: false,
      updatedOn: new Date("2020-01-01T00:00:00.000Z"),
    })
    await repository.insert({
      id: "4",
      age: 10,
      name: "foo4",
      enabled: true,
      updatedOn: new Date("2020-01-01T00:00:00.000Z"),
    })
    await repository.insert({
      id: "5",
      age: 50,
      name: "foo4",
      enabled: false,
      updatedOn: new Date("2020-01-01T00:00:00.000Z"),
    })
  }
})
