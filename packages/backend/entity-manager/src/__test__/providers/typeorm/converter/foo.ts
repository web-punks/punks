import { newUuid } from "@punks/backend-core"
import { IEntityConverter } from "../../../../abstractions"
import { FooEntity } from "../entity/foo"
import {
  FooCreateInput,
  FooDto,
  FooListItemDto,
  FooUpdateInput,
} from "../models/foo"

export class FooConverter
  implements
    IEntityConverter<
      FooEntity,
      FooDto,
      FooListItemDto,
      FooCreateInput,
      FooUpdateInput
    >
{
  toListItemDto(entity: FooEntity): FooListItemDto {
    return {
      id: entity.id,
      profile: {
        name: entity.name,
        age: entity.age,
      },
    }
  }

  toEntityDto(entity: FooEntity): FooDto {
    return {
      id: entity.id,
      profile: {
        name: entity.name,
        age: entity.age,
      },
      updatedOn: entity.updatedOn,
    }
  }

  createDtoToEntity(input: FooCreateInput): Partial<FooEntity> {
    return {
      age: input.profile.age,
      name: input.profile.name,
      id: newUuid(),
      updatedOn: new Date(),
    }
  }

  updateDtoToEntity(input: FooUpdateInput): Partial<FooEntity> {
    return {
      id: input.id,
      age: input.profile.age,
      name: input.profile.name,
      updatedOn: new Date(),
    }
  }
}
