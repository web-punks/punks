import { DataSource } from "typeorm"
import { DataType, newDb } from "pg-mem"
import { newUuid } from "@punks/backend-core"

export const mockPostgresDatabase = async ({
  entities,
}: {
  entities: any[]
}) => {
  const db = newDb({
    autoCreateForeignKeyIndices: true,
  })
  db.public.registerFunction({
    implementation: () => "test",
    name: "current_database",
  })
  db.public.registerFunction({
    implementation: () => "15.3",
    name: "version",
  })
  db.public.registerFunction({
    name: "obj_description",
    args: [DataType.text, DataType.text],
    returns: DataType.text,
    implementation: (x, y) => "This is a mock response",
  })
  const dataSource: DataSource = db.adapters.createTypeormDataSource({
    name: newUuid(),
    type: "postgres",
    entities,
  })
  await dataSource.initialize()
  await dataSource.synchronize()

  return {
    db,
    dataSource,
  }
}
