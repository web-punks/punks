import { IEntityManager } from "../../../../abstractions/manager"
import { FooEntity } from "../entity/foo"
import {
  FooCursor,
  FooDeleteParameters,
  FooFacets,
  FooSearchParameters,
  FooSorting,
} from "../models/foo"

export type FooEntityManager = IEntityManager<
  FooEntity,
  string,
  Partial<FooEntity>,
  Partial<FooEntity>,
  FooDeleteParameters,
  FooSearchParameters,
  FooSorting,
  FooCursor,
  FooFacets
>
