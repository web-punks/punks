import { newUuid } from "@punks/backend-core"
import { IEntityManagerServiceRoot } from "../../abstractions/configuration"
import { IEntityConverter } from "../../abstractions/converters"
import { IEntitiesQueryBuilder } from "../../abstractions/queries"
import {
  IEntitySearchParameters,
  ISearchSorting,
  SortDirection,
} from "../../abstractions/searchParameters"
import { IEntitiesSearchResults } from "../../models"
import { EntityServiceLocator } from "../../providers/services"
import { InMemoryRepository } from "../../testing/mocks/repository"
import {
  EntitySerializerSheetDefinition,
  IAuthenticationContext,
  IEntityManager,
} from "../../abstractions"
import { IEntitiesDeleteResult } from "../../abstractions/repository"
import { EntitySerializer } from "../../base"
import { InMemoryBucketProvider } from "../../platforms"
import { FooDeleteParameters } from "../providers/typeorm/models/foo"
import { InMemoryFileProvider } from "../../platforms/nest/plugins/buckets/testing/mock"
import { FooEntityId } from "../providers/typeorm/entity/foo"

export interface FooEntity {
  id: string
  name: string
  age: number
  updatedOn: Date
}

export type FooEntityManager = IEntityManager<
  FooEntity,
  string,
  Partial<FooEntity>,
  Partial<FooEntity>,
  FooDeleteParameters,
  FooSearchParameters,
  FooSorting,
  FooCursor,
  FooFacets
>

export class FooRepository extends InMemoryRepository<FooEntity> {}

export interface FooSearchFilters {
  minAge?: number
  maxAge?: number
}

export enum FooSorting {
  Name = "Name",
  Age = "Age",
}

export type FooCursor = number

export interface FooSearchParameters
  extends IEntitySearchParameters<FooEntity, FooSorting, FooCursor> {
  filters?: FooSearchFilters
}

export interface FooFacets {}

export class FooQueryBuilder
  implements
    IEntitiesQueryBuilder<
      FooEntity,
      string,
      FooSearchParameters,
      FooSorting,
      FooCursor,
      FooFacets,
      unknown
    >
{
  private readonly repository: FooRepository

  constructor(services: EntityServiceLocator<FooEntity, string>) {
    this.repository = services.resolveRepository() as FooRepository
  }

  async find(
    request?:
      | {
          filters?: FooSearchFilters | undefined
          sorting?: ISearchSorting<FooSorting> | undefined
        }
      | undefined,
    context?: IAuthenticationContext<unknown> | undefined
  ): Promise<FooEntity> {
    const results = await this.search(request ?? {})
    return results.items[0]
  }

  async get(id: string): Promise<FooEntity | undefined> {
    return await this.repository.get(id)
  }

  async exists(
    filters: FooSearchFilters,
    context?: IAuthenticationContext<unknown> | undefined
  ): Promise<boolean> {
    return (await this.count(filters, context)) > 0
  }

  async count(
    filters: FooSearchFilters,
    context?: IAuthenticationContext<unknown> | undefined
  ): Promise<number> {
    const result = await this.search({ filters })
    return result.items.length
  }

  async delete(
    filters: FooSearchFilters,
    context?: IAuthenticationContext<unknown> | undefined
  ): Promise<IEntitiesDeleteResult> {
    const result = await this.search({ filters })
    await this.repository.deleteBy((x) => result.items.includes(x))
    return {
      deletedCount: result.items.length,
    }
  }

  async search(
    request: FooSearchParameters
  ): Promise<
    IEntitiesSearchResults<
      FooEntity,
      FooSearchParameters,
      FooEntity,
      FooSorting,
      FooCursor,
      FooFacets
    >
  > {
    let results = await this.repository.find(
      (x) =>
        (!request.filters?.minAge || x.age >= request.filters?.minAge) &&
        (!request.filters?.maxAge || x.age <= request.filters?.maxAge)
    )

    if (request.sorting?.fields?.[0]?.field === FooSorting.Age) {
      results = results.sort((a, b) => {
        if (request.sorting?.fields?.[0]?.direction === SortDirection.Asc) {
          return a.age - b.age
        } else {
          return b.age - a.age
        }
      })
    }

    const originalTotItems = results.length
    let nextPageCursor: FooCursor | undefined
    let prevPageCursor: FooCursor | undefined
    if (request.paging) {
      const cursor = request.paging.cursor ?? 0
      const offset = cursor * request.paging.pageSize
      results = results.slice(offset, offset + request.paging.pageSize)
      nextPageCursor =
        results.length < request.paging.pageSize ? undefined : cursor + 1
      prevPageCursor = cursor > 0 ? cursor - 1 : undefined
    }

    return {
      items: results,
      request,
      paging: request.paging
        ? {
            pageIndex: request.paging.cursor ?? 0,
            pageSize: request.paging.pageSize,
            totItems: originalTotItems,
            totPageItems: results.length,
            totPages: Math.ceil(originalTotItems / request.paging.pageSize),
            nextPageCursor,
            currentPageCursor: request.paging.cursor ?? 0,
            prevPageCursor,
          }
        : undefined,
    }
  }
}

export interface FooDto {
  id: string
  profile: {
    name: string
    age: number
  }
  updatedOn: Date
}

export interface FooListItemDto {
  id: string
  profile: {
    name: string
    age: number
  }
}

export interface FooCreateDto {
  profile: {
    name: string
    age: number
  }
}

export interface FooUpdateDto {
  profile: {
    name: string
    age: number
  }
}

export type FooSheetItem = FooEntity

export class FooSerializer extends EntitySerializer<
  FooEntity,
  FooEntityId,
  FooSearchParameters,
  FooSheetItem,
  any
> {
  private readonly repo: FooRepository

  constructor(services: EntityServiceLocator<FooEntity, string>) {
    super(services)
    this.repo = services.resolveRepository() as FooRepository
  }

  protected loadEntities(
    filters?: FooSearchParameters | undefined
  ): Promise<FooEntity[]> {
    return this.repo.find(() => true)
  }

  protected convertToSheetItems(entities: FooEntity[]): Promise<FooEntity[]> {
    return Promise.resolve(entities)
  }

  protected async importItem(item: FooEntity): Promise<FooEntity> {
    if (await this.repo.exists(item.id)) {
      return await this.repo.update(item.id, item)
    }
    return await this.repo.create(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<FooEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
        {
          name: "Name",
          key: "name",
          selector: "name",
          parser: (value) => (value?.toString() as string).toUpperCase(),
          converter: (value) => (value?.toString() as string).toLowerCase(),
          sampleValue: "Foo",
        },
        {
          name: "Age",
          key: "age",
          selector: "age",
          parser: (value) => parseInt(value?.toString() as string),
          sampleValue: 18,
        },
      ],
    }
  }
}

export class FooConverter
  implements
    IEntityConverter<
      FooEntity,
      FooDto,
      FooListItemDto,
      FooCreateDto,
      FooUpdateDto
    >
{
  toListItemDto(entity: FooEntity): FooListItemDto {
    return {
      id: entity.id,
      profile: {
        name: entity.name,
        age: entity.age,
      },
    }
  }

  toEntityDto(entity: FooEntity): FooDto {
    return {
      id: entity.id,
      profile: {
        name: entity.name,
        age: entity.age,
      },
      updatedOn: entity.updatedOn,
    }
  }

  createDtoToEntity(input: FooCreateDto): Partial<FooEntity> {
    return {
      age: input.profile.age,
      name: input.profile.name,
      id: newUuid(),
      updatedOn: new Date(),
    }
  }

  updateDtoToEntity(input: FooUpdateDto): Partial<FooEntity> {
    return {
      age: input.profile.age,
      name: input.profile.name,
      updatedOn: new Date(),
    }
  }
}

export const registerFooEntity = (
  container: IEntityManagerServiceRoot,
  options?: {
    useConverter?: boolean
  }
) => {
  container
    .getEntitiesServicesLocator()
    .registerSettings({
      importExport: {
        exportBucket: {
          bucket: "test",
          publicLinksExpirationMinutes: 60,
          rootFolderPath: "test",
        },
      },
      defaultBucketProvider: "inMemory",
      defaultFilesProvider: "inMemory",
      defaultMediaProvider: "inMemory",
    })
    .registerBucketProvider("inMemory", new InMemoryBucketProvider())
    .registerFileProvider("inMemory", new InMemoryFileProvider())

  const c = container.registerEntity<FooEntity, string, FooRepository>(
    {
      name: "foo",
    },
    new FooRepository()
  )
  c.mapCrudOperations({
    queryBuilder: new FooQueryBuilder(c.getServiceLocator()),
    settings: {
      importExport: {
        exportBucket: {
          bucket: "test",
          publicLinksExpirationMinutes: 60,
          rootFolderPath: "foo",
        },
      },
      defaultBucketProvider: "inMemory",
      defaultFilesProvider: "inMemory",
      defaultMediaProvider: "inMemory",
    },
  })

  c.addSerializer(new FooSerializer(c.getServiceLocator()))
  if (options?.useConverter) {
    c.addConverter(new FooConverter())
  }

  return c
}
