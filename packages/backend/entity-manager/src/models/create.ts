export type EntityReference<TEntityId> = {
  id: TEntityId
}
