import { SortDirection } from "../abstractions"

export interface IEntityVersionsReference {
  entityId: string
}

export interface IEntityVersionsFilters {
  timestampFrom?: Date
  timestampTo?: Date
}

export interface IEntityVersionsSorting {
  direction: SortDirection
}

export class IEntityVersionsCursor<TCursor> {
  cursor?: TCursor
  pageSize: number
}

export interface IEntityVersionsSearchParameters<TCursor> {
  entity: IEntityVersionsReference
  filters?: IEntityVersionsFilters
  sorting?: IEntityVersionsSorting
  paging?: IEntityVersionsCursor<TCursor>
}

export interface IEntityVersionsResultsPaging<TCursor> {
  pageIndex: number
  pageSize: number
  totPageItems: number
  totPages: number
  totItems: number
  nextPageCursor?: TCursor
  currentPageCursor?: TCursor
  prevPageCursor?: TCursor
}

export interface IEntityVersionsSearchResults<TResult, TCursor> {
  paging?: IEntityVersionsResultsPaging<TCursor>
  items: TResult[]
}

export interface IEntityVersionsSearchRequest<TCursor> {
  params: IEntityVersionsSearchParameters<TCursor>
}

export interface IEntityVersionsSearchResponse<TResult, TCursor> {
  request: IEntityVersionsSearchRequest<TCursor>
  paging?: IEntityVersionsResultsPaging<TCursor>
  items: TResult[]
}
