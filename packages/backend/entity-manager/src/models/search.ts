import { SortingType } from "../abstractions/common"
import { IEntitySearchParameters } from "../abstractions/searchParameters"

export interface IChildrenMap extends Record<string, { count: number }> {}

export interface IEntitiesSearchResultsPaging<TCursor> {
  pageIndex: number
  pageSize: number
  totPageItems: number
  totPages: number
  totItems: number
  nextPageCursor?: TCursor
  currentPageCursor?: TCursor
  prevPageCursor?: TCursor
}

export interface IEntitiesSearchResults<
  TEntity,
  TSearchParameters extends IEntitySearchParameters<TEntity, TSorting, TCursor>,
  TResult,
  TSorting extends SortingType,
  TCursor,
  TFacets
> {
  request: TSearchParameters
  items: TResult[]
  facets?: TFacets
  paging?: IEntitiesSearchResultsPaging<TCursor>
  childrenMap?: IChildrenMap
}
