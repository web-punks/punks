import { newUuid as coreNewUuid } from "@punks/backend-core"

export const newUuid = coreNewUuid
