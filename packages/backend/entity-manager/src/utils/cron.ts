import { parseExpression } from "cron-parser"

export const getCronCurrentSchedule = (cronExpression: string, ref: Date) => {
  const interval = parseExpression(cronExpression, {
    currentDate: ref,
  })
  return interval.prev().toDate()
}
