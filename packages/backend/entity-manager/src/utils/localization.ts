import { LocalizedTexts } from "../abstractions"

export const getLocalizedText = (
  item: string | LocalizedTexts,
  languageId: string,
  defaultLanguageId?: string
) => {
  return typeof item === "string"
    ? item
    : item[languageId] ??
        (defaultLanguageId ? item[defaultLanguageId] : undefined)
}
