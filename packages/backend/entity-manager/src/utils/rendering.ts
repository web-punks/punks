import { isNullOrUndefined } from "@punks/backend-core"
import Handlebars from "handlebars"

let helpersRegistered = false
const registerHandlebarsHelpers = () => {
  if (helpersRegistered) {
    return
  }
  Handlebars.registerHelper("truncate", function (text, maxChars) {
    if (text?.length > maxChars) {
      return text.substring(0, maxChars) + "..."
    } else {
      return text
    }
  })
  Handlebars.registerHelper("uppercase", function (str) {
    return str?.toUpperCase()
  })
  Handlebars.registerHelper("isDefined", function (value) {
    return !isNullOrUndefined(value)
  })
  Handlebars.registerHelper("isUndefined", function (value) {
    return isNullOrUndefined(value)
  })
  Handlebars.registerHelper("eq", function (a, b) {
    return a === b
  })
  Handlebars.registerHelper("ne", function (a, b) {
    return a !== b
  })
  helpersRegistered = true
}

export const renderHandlebarsTemplate = <TContext extends object>(input: {
  template: string
  context: TContext
}) => {
  registerHandlebarsHelpers()
  const compiled = Handlebars.compile(input.template)
  return compiled(input.context)
}
