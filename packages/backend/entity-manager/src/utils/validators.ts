import {
  EntryValidationError,
  EntryValidationResult,
} from "../abstractions/serializer"

export const fieldRequiredValidator = <TSheetItem>(
  item: TSheetItem,
  selector: (item: TSheetItem) => any
): EntryValidationResult => {
  const value = selector(item)
  const isValid = typeof value === "string" ? value.trim().length > 0 : !!value
  return {
    isValid,
    validationErrors: value ? [] : [{ errorCode: "required" }],
  }
}

export const fieldNumericValidator = <TSheetItem>(
  item: TSheetItem,
  selector: (item: TSheetItem) => number,
  params?: {
    min?: number
    max?: number
  }
): EntryValidationResult => {
  const value = selector(item)
  const errors: EntryValidationError[] = []

  if (!!value && isNaN(value)) {
    errors.push({ errorCode: "notNumeric" })
  }

  if (!!params?.min && !!value && value < params.min) {
    errors.push({
      errorCode: "min",
      payload: { current: value, expected: params.min },
    })
  }

  if (!!params?.max && !!value && value > params.max) {
    errors.push({
      errorCode: "max",
      payload: { current: value, expected: params.max },
    })
  }

  return {
    isValid: errors.length === 0,
    validationErrors: errors,
  }
}

export const fieldTextValidator = <TSheetItem>(
  item: TSheetItem,
  selector: (item: TSheetItem) => string | string[],
  params: {
    required?: boolean
    minLength?: number
    maxLength?: number
    regex?: RegExp
    regexErrorCode?: string
  }
): EntryValidationResult => {
  const value = selector(item)
  const values = Array.isArray(value)
    ? value.filter(Boolean)
    : [value].filter(Boolean)

  const isRequiredValid = params.required
    ? values.length > 0 &&
      values.every((v) => typeof v === "string" && v.length > 0)
    : true

  const validateSingle = (v: string): EntryValidationError[] => {
    const length = v.length
    const isMinLengthValid = length >= (params.minLength ?? 0)
    const isMaxLengthValid = length <= (params.maxLength ?? Infinity)
    const isRegexValid = params.regex?.test(v) ?? true

    return [
      ...(isMinLengthValid
        ? []
        : [
            {
              errorCode: "minLength",
              payload: { current: length, expected: params.minLength },
            },
          ]),
      ...(isMaxLengthValid
        ? []
        : [
            {
              errorCode: "maxLength",
              payload: { current: length, expected: params.maxLength },
            },
          ]),
      ...(isRegexValid
        ? []
        : [{ errorCode: params.regexErrorCode ?? "regex" }]),
    ]
  }

  const allErrors = values?.flatMap(validateSingle) ?? []

  return {
    isValid: isRequiredValid && allErrors.length === 0,
    validationErrors: [
      ...(isRequiredValid ? [] : [{ errorCode: "required" }]),
      ...allErrors,
    ],
  }
}

export const fieldEmailValidator = <TSheetItem>(
  item: TSheetItem,
  selector: (item: TSheetItem) => any,
  params: {
    required?: boolean
  }
): EntryValidationResult => {
  return fieldTextValidator(item, selector, {
    ...params,
    regex: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
    regexErrorCode: "email",
  })
}

export const fieldOptionValidator = <TSheetItem>(
  item: TSheetItem,
  selector: (item: TSheetItem) => string,
  {
    availableOptions,
    required,
  }: {
    availableOptions: string[]
    required?: boolean
  }
): EntryValidationResult => {
  const value = selector(item)
  if (!value) {
    return {
      isValid: true,
      validationErrors: [],
    }
  }

  const errors = []

  const isValidOption = availableOptions.includes(value)
  if (!isValidOption) {
    errors.push({ errorCode: "invalidOption" })
  }

  if (required && !value) {
    errors.push({ errorCode: "required" })
  }

  return {
    isValid: errors.length === 0,
    validationErrors: errors,
  }
}

export const fieldOptionsValidator = <TSheetItem>(
  item: TSheetItem,
  selector: (item: TSheetItem) => string[],
  {
    availableOptions,
    minSelected,
    maxSelected,
  }: {
    availableOptions: string[]
    minSelected?: number
    maxSelected?: number
  }
): EntryValidationResult => {
  const values = selector(item) ?? []

  const errors: EntryValidationError[] = []

  const invalidOptions = values.filter((x) => !availableOptions.includes(x))
  invalidOptions.forEach((x) =>
    errors.push({ errorCode: "invalidOption", value: x })
  )

  if (minSelected && values.length < minSelected) {
    errors.push({ errorCode: "minSelected" })
  }

  if (maxSelected && values.length > maxSelected) {
    errors.push({ errorCode: "maxSelected" })
  }

  return {
    isValid: errors.length === 0,
    validationErrors: errors,
  }
}
