import { Log } from "@punks/backend-core"
import { Type } from "../abstractions/common"
import { IResolveServiceOptions, IServiceLocator } from "../abstractions/ioc"

type NamedService<T> = { [name: string]: T }

// todo: implement a real IoC container with scopes and stuff
export class ServiceLocator implements IServiceLocator {
  private services: { [key: string]: any } = {}
  private multipleServices: { [key: string]: NamedService<any> } = {}
  private logger = Log.getLogger("ServiceLocator")

  all() {
    return this.services
  }

  register<T>(name: string, service: T): void {
    this.services[name] = service
  }

  registerMultiple<T>(
    serviceName: string,
    instanceName: string,
    service: T
  ): void {
    if (!this.multipleServices[serviceName]) {
      this.multipleServices[serviceName] = {}
    }
    if (this.multipleServices[serviceName][instanceName]) {
      this.logger.warn(
        `Service instance "${serviceName}" "${instanceName}" already registered in ServiceLocator`
      )
    }
    this.multipleServices[serviceName][instanceName] = service
  }

  registerEntityService<T>(
    serviceName: string,
    entityName: string,
    service: T
  ) {
    this.register(this.buildServiceName(serviceName, entityName), service)
  }

  registerEntityServiceType<T>(
    serviceType: Type<T>,
    entityName: string,
    service: T
  ) {
    this.register(this.buildServiceTypeName(serviceType, entityName), service)
  }

  resolve<T>(name: string, options?: IResolveServiceOptions): T {
    const service = this.services[name]
    if (!service && !options?.optional) {
      throw new Error(`Service "${name}" not found in ServiceLocator`)
    }
    return service
  }

  resolveMultiple<T>(
    serviceName: string,
    options?: IResolveServiceOptions
  ): T[] {
    const services = this.multipleServices[serviceName]
    if (!services && !options?.optional) {
      throw new Error(
        `Service type "${serviceName}" not found in ServiceLocator`
      )
    }
    return Object.entries(services ?? {}).map(([, value]) => value)
  }

  resolveMultipleNamed<T>(
    serviceName: string,
    instanceName: string,
    options?: IResolveServiceOptions
  ): T {
    const services = this.multipleServices[serviceName]
    if (!services && !options?.optional) {
      throw new Error(
        `Service type "${serviceName}" not found in ServiceLocator`
      )
    }
    const service = services[instanceName]
    if (!service && !options?.optional) {
      throw new Error(
        `Service "${serviceName}" instance "${instanceName}" not found in ServiceLocator -> Available: ${
          services ? Object.keys(services) : "none"
        }`
      )
    }
    return service
  }

  resolveEntityService<T>(
    serviceName: string,
    entityName: string,
    options?: IResolveServiceOptions
  ) {
    return this.resolve<T>(
      this.buildServiceName(serviceName, entityName),
      options
    )
  }

  resolveEntityServiceType<T>(
    serviceType: Type<T>,
    entityName: string,
    options?: IResolveServiceOptions
  ) {
    return this.resolve<T>(
      this.buildServiceTypeName(serviceType, entityName),
      options
    )
  }

  private buildServiceTypeName<T>(serviceType: Type<T>, entityName: string) {
    return this.buildServiceName(serviceType.name, entityName)
  }

  private buildServiceName(serviceName: string, entityName: string) {
    return `${serviceName}.${entityName}`
  }
}
