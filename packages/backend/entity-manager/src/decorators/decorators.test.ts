import { Entity, getEntityName } from "."

@Entity("MyEntity")
export class TestClass {}

describe("Decorators", () => {
  it("should resolve entity metadata", () => {
    const entityName = getEntityName(TestClass)
    expect(entityName).toBe("MyEntity")
  })
})
