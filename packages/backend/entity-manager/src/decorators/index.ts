import "reflect-metadata"

const ENTITY_TYPE_KEY = "wp:entity-type"

export function Entity(metadata: string) {
  return function (target: any) {
    Reflect.defineMetadata(ENTITY_TYPE_KEY, metadata, target)
  }
}

export const getEntityName = (target: Object) => {
  return Reflect.getMetadata(ENTITY_TYPE_KEY, target)
}
