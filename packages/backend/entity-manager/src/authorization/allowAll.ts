import {
  IAuthorizationResult,
  IEntityAuthorizationMiddleware,
} from "../abstractions/authorization"

export class AllowAllAuthorizationMiddleware<TEntity>
  implements IEntityAuthorizationMiddleware<TEntity, any, unknown>
{
  async canSearch(): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  async canRead(entity: TEntity): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  async canCreate(entity: TEntity) {
    return {
      isAuthorized: true,
    }
  }

  async canUpdate(entity: TEntity) {
    return {
      isAuthorized: true,
    }
  }

  async canDelete(entity: TEntity) {
    return {
      isAuthorized: true,
    }
  }

  async canDeleteItems(): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }
}
