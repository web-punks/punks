import { EntitySerializationFormat } from "./serializer"

export type EntitiesImportInputFile = {
  fileName: string
  content: Buffer
  contentType: string
}

export enum InvalidEntityHandling {
  Skip = "skip",
  Fail = "fail",
}

export type EntitiesImportInput<TPayload> = {
  format: EntitySerializationFormat
  file: EntitiesImportInputFile
  payload?: TPayload
  options?: {
    invalidEntitiesHandling: InvalidEntityHandling
  }
}

export type EntitiesImportStatistics = {
  importedCount: number
  skippedCount: number
  updatedCount: number
  createdCount: number
  unchangedCount: number
}

export type EntitiesImportResult = {
  statistics: EntitiesImportStatistics
}
