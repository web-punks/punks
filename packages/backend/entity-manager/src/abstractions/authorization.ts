import { DeepPartial } from "../types"
import { IAuthenticationContext } from "./authentication"

export interface IAuthorizationResult {
  isAuthorized: boolean
}

export interface IEntityAuthorizationMiddleware<
  TEntity,
  TAuthenticationContext extends IAuthenticationContext<TUserContext>,
  TUserContext
> {
  canSearch(context: TAuthenticationContext): Promise<IAuthorizationResult>

  canRead(
    entity: DeepPartial<TEntity>,
    context: TAuthenticationContext
  ): Promise<IAuthorizationResult>

  canCreate(
    entity: DeepPartial<TEntity>,
    context: TAuthenticationContext
  ): Promise<IAuthorizationResult>

  canUpdate(
    entity: DeepPartial<TEntity>,
    context: TAuthenticationContext
  ): Promise<IAuthorizationResult>

  canDelete(
    entity: DeepPartial<TEntity>,
    context: TAuthenticationContext
  ): Promise<IAuthorizationResult>

  canDeleteItems(context: TAuthenticationContext): Promise<IAuthorizationResult>
}
