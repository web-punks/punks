import { IEntityRepository } from "./repository"

export interface IReplicasConfiguration<TEntity, TEntityId> {
  configureReplica<
    TRepository extends IEntityRepository<
      TEntity,
      TEntityId,
      unknown,
      unknown,
      unknown,
      unknown
    >
  >(
    name: string,
    options: ReplicaOptions,
    repository: TRepository
  ): void
  getReplica(name: string): ReplicaConfiguration<TEntity, TEntityId>
  getReplicas(): ReplicaConfiguration<TEntity, TEntityId>[]
}

export enum ReplicationMode {
  Sync,
  Async,
}

export type ReplicaOptions = {
  mode: ReplicationMode
  enabled?: boolean
}

export type ReplicaConfiguration<TEntity, TEntityId> = {
  name: string
  options: ReplicaOptions
  repository: IEntityRepository<
    TEntity,
    TEntityId,
    unknown,
    unknown,
    unknown,
    unknown
  >
}

export interface IEntityReplicaDeleteManager<TEntity, TEntityId> {
  deleteReplicas(entityId: TEntityId): Promise<void>
}

export interface IEntityReplicaSyncManager<TEntity> {
  syncReplicas(entity: TEntity): Promise<void>
}
