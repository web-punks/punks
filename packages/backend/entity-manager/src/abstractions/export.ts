import { SortingType } from "./common"
import { IEntitySearchParameters } from "./searchParameters"
import { EntitySerializationFormat } from "./serializer"

export type EntitiesExportOptions = {
  format: EntitySerializationFormat
}

export type EntitiesExportFile = {
  name: string
  content: Buffer
  contentType: string
}

export type EntitiesExportResult = {
  file: EntitiesExportFile
  downloadUrl: string
}

export type EntitiesExportInput<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TPayload = unknown
> = {
  filter?: TEntitySearchParameters
  options: EntitiesExportOptions
  payload?: TPayload
}

export type EntityExportBucketSettings = {
  bucket: string
  publicLinksExpirationMinutes: number
  rootFolderPath?: string
}

export type EntitiesImportExportSettings = {
  exportBucket: EntityExportBucketSettings
}

export type EntitiesSampleDownloadOptions = {
  format: EntitySerializationFormat
}

export type EntitiesSampleDownloadResult = {
  file: EntitiesExportFile
  downloadUrl: string
}
