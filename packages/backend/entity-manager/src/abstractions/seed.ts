export interface IEntitySeedEntry<TEntity, TFindCondition> {
  identifier: TFindCondition
  data: () => Partial<TEntity>
}

export interface IEntitySeeder {
  execute(): Promise<void>
  get priority(): number | undefined
}
