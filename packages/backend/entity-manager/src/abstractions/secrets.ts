export type AppSecretType = "string" | "number" | "boolean" | "json"

export type AppSecretDefinition = {
  key: string
  type: AppSecretType
  hidden: boolean
  defaultValue?: any
}

export type AppSecret<TValue> = {
  key: string
  value: TValue
  type: AppSecretType
  hidden: boolean
  timestamp: number
}

export type AppSecretInput<TValue> = {
  key: string
  value: TValue
}

export type AppSecretsPageMetadata = {
  tags?: { key: string; value: string }[]
}

export interface ISecretsProvider {
  getSecrets(pageName: string): Promise<AppSecret<any>[]>
  setSecret<TValue>(pageName: string, secret: AppSecret<TValue>): Promise<void>
  defineSecret(pageName: string, definition: AppSecretDefinition): Promise<void>
  pageInitialize(
    pageName: string,
    metadata: AppSecretsPageMetadata
  ): Promise<void>
  pageEnsure(pageName: string, metadata: AppSecretsPageMetadata): Promise<void>
  pageExists(pageName: string): Promise<boolean>
}
