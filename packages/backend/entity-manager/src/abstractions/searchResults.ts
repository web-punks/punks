import { SortingType } from "./common"
import { IEntitySearchParameters } from "./searchParameters"

export interface IEntityFacetValue<T> {
  value: T
  label: string
  count: number
}

export interface IEntityFacet<T> {
  values: IEntityFacetValue<T>[]
}

export interface IEntityFacets {}

export interface IEntitySearchResults<
  TEntity,
  TSearchParameters extends IEntitySearchParameters<TEntity, TSorting, TCursor>,
  TResult,
  TSorting extends SortingType,
  TCursor,
  TFacets
> {
  request: TSearchParameters
  items: TResult[]
  Facets?: TFacets
  paging?: ISearchResultsPaging<TCursor>
}

export interface ISearchResultsPaging<TCursor> {
  pageIndex: number
  pageSize: number
  totPages: number
  nextPageCursor?: TCursor
  currentPageCursor?: TCursor
  prevPageCursor?: TCursor
}
