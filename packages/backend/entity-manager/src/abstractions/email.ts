export interface IEmailTemplateMiddleware {
  processPayload(payload: unknown): Promise<unknown>
  processInput(
    input: TemplatedEmailInput<unknown>
  ): Promise<TemplatedEmailInput<unknown>>
}

export interface IEmailTemplate<
  TTemplateData,
  TPayload,
  TAugmentedPayload = TPayload
> {
  processPayload(payload: TPayload): Promise<TAugmentedPayload>
  getTemplateData(payload: TAugmentedPayload): Promise<TTemplateData>
}

export interface IEmailTemplatesCollection {
  registerTemplate(
    id: string,
    template: IEmailTemplate<unknown, unknown, unknown>
  ): void

  getTemplate<TTemplateData, TPayload, TAugmentedPayload = TPayload>(
    id: string
  ): IEmailTemplate<TTemplateData, TPayload, TAugmentedPayload>
}

export type EmailAttachmentData = {
  content: string | Buffer
  filename: string
  type?: string
  disposition?: string
  contentId?: string
}

export type TemplatedEmailInput<TPayload> = {
  from?: string
  to?: string[]
  cc?: string[]
  bcc?: string[]
  subjectTemplate?: string
  templateId: string
  languageCode: string
  payload: TPayload
  attachments?: EmailAttachmentData[]
}

export interface HtmlEmailInput<TPayload> {
  replyTo?: string
  from?: string
  to?: string[]
  cc?: string[]
  bcc?: string[]
  subjectTemplate: string
  bodyTemplate: string
  payload: TPayload
  attachments?: EmailAttachmentData[]
}

export type EmailSendOptions = {
  sandboxMode?: boolean
  forceDelivery?: boolean
}

export interface IEmailProvider<TTemplateData> {
  sendTemplatedEmail<TPayload, TAugmentedPayload>(
    input: TemplatedEmailInput<TPayload>,
    template: IEmailTemplate<TTemplateData, TPayload, TAugmentedPayload>,
    options?: EmailSendOptions
  ): Promise<void>

  sendHtmlEmail<TPayload>(
    input: HtmlEmailInput<TPayload>,
    options?: EmailSendOptions
  ): Promise<void>
}

export interface IEmailLogger {
  logTemplatedEmail<TPayload>(
    input: TemplatedEmailInput<TPayload>
  ): Promise<void>

  logHtmlEmail<TPayload>(input: HtmlEmailInput<TPayload>): Promise<void>
}
