import { IEntityMapper } from "./mappers"

export interface IEntityConnector<TEntity, TEntityId, TMappedType> {
  syncEntity(entity: TMappedType): Promise<void>
  deleteEntity(id: TEntityId): Promise<void>
}

export interface IConnectorsConfiguration<TEntity> {
  configureConnector<
    TEntityConnector extends IEntityConnector<TEntity, unknown, TMappedType>,
    TMappedType,
    TMapper extends IEntityMapper<TEntity, TMappedType>
  >(
    name: string,
    options: ConnectorOptions,
    connector: TEntityConnector,
    mapper: TMapper
  ): void
  getConnector(name: string): ConnectorConfiguration<TEntity>
  getConnectors(): ConnectorConfiguration<TEntity>[]
}

export enum ConnectorMode {
  Sync,
  Async,
}

export type ConnectorOptions = {
  mode: ConnectorMode
  enabled?: boolean
}

export type ConnectorConfiguration<TEntity> = {
  name: string
  options: ConnectorOptions
  connector: IEntityConnector<TEntity, unknown, unknown>
  mapper: IEntityMapper<TEntity, unknown>
}

export interface IEntityConnectorSyncManager<TEntity> {
  syncEntity(entity: TEntity): Promise<void>
}

export interface IEntityConnectorDeleteManager<TEntity, TEntityId> {
  deleteEntity(entityId: TEntityId): Promise<void>
}
