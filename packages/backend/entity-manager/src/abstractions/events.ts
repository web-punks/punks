export interface IEntityEventsManager<TEntity, TEntityId> {
  processEntityCreatedEvent(entity: TEntity): Promise<void>
  processEntityUpdatedEvent(entity: TEntity): Promise<void>
  processEntityDeletedEvent(id: TEntityId): Promise<void>
}

export interface IEventEmitter {
  emit(event: string, ...args: any[]): Promise<void>
}
