export interface IEntitySearchParameters<TEntity, TSorting, TCursor> {
  query?: IFullTextQuery
  filters?: ISearchFilters
  traverse?: ITraverseFilters
  sorting?: ISearchSorting<TSorting>
  paging?: ISearchRequestPaging<TCursor>
  options?: ISearchOptions
  relations?: ISearchQueryRelations<TEntity>
}

export interface ISearchFilters {}

export interface ITraverseFilters {
  rootOnly?: boolean
  parentIds?: string[]
}

export interface ISearchOptions {
  includeFacets?: boolean
  includeChildrenMap?: boolean
  facetsFilters?: ISearchFilters
}

export interface ISearchRequestPaging<TCursor> {
  cursor?: TCursor
  pageSize: number
}

export interface ISearchSortingField<TSorting> {
  field: TSorting
  direction: SortDirection
}

export interface ISearchSorting<TSorting> {
  fields: ISearchSortingField<TSorting>[]
}

export enum SortDirection {
  Asc = "asc",
  Desc = "desc",
}

export interface IFullTextQuery {
  term: string
  fields: string[]
}

export type ISearchQueryRelationsProperty<TProperty> =
  TProperty extends Promise<infer I>
    ? ISearchQueryRelationsProperty<NonNullable<I>> | boolean
    : TProperty extends Array<infer I>
    ? ISearchQueryRelationsProperty<NonNullable<I>> | boolean
    : TProperty extends string
    ? never
    : TProperty extends number
    ? never
    : TProperty extends boolean
    ? never
    : TProperty extends Function
    ? never
    : TProperty extends Buffer
    ? never
    : TProperty extends Date
    ? never
    : TProperty extends object
    ? ISearchQueryRelations<TProperty> | boolean
    : boolean

export type ISearchQueryRelations<TEntity> = {
  [P in keyof TEntity]?: P extends "toString"
    ? unknown
    : ISearchQueryRelationsProperty<NonNullable<TEntity[P]>>
}
