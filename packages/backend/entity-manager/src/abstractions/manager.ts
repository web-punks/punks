import {
  IEntitiesDeleteCommand,
  IEntitiesDeleteParameters,
  IEntitiesExportCommand,
  IEntitiesImportCommand,
  IEntitiesParseCommand,
  IEntitiesSampleDownloadCommand,
  IEntityCreateCommand,
  IEntityDeleteCommand,
  IEntityUpdateCommand,
  IEntityUpsertByCommand,
  IEntityUpsertCommand,
  IEntityVersionCommand,
} from "./commands"
import { SortingType } from "./common"
import {
  IEntitiesCountQuery,
  IEntitiesFindQuery,
  IEntitiesSearchQuery,
  IEntityExistsQuery,
  IEntityGetQuery,
  IEntityVersionsSearchQuery,
} from "./queries"
import { IEntitySearchParameters } from "./searchParameters"
import { IEntityFacets } from "./searchResults"

export interface IEntityManager<
  TEntity,
  TEntityId,
  TEntityCreateData,
  TEntityUpdateData,
  TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets
> {
  get get(): IEntityGetQuery<TEntity, TEntityId, TEntitySearchParameters>
  get search(): IEntitiesSearchQuery<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor,
    TFacets
  >
  get find(): IEntitiesFindQuery<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor
  >

  get exists(): IEntityExistsQuery<
    TEntity,
    NonNullable<TEntitySearchParameters["filters"]>
  >
  get count(): IEntitiesCountQuery<
    TEntity,
    NonNullable<TEntitySearchParameters["filters"]>
  >
  get version(): IEntityVersionCommand<TEntity, TEntityId>
  get create(): IEntityCreateCommand<TEntity, TEntityId, TEntityCreateData>
  get update(): IEntityUpdateCommand<TEntity, TEntityId, TEntityUpdateData>
  get upsert(): IEntityUpsertCommand<TEntity, TEntityId, TEntityUpdateData>
  get upsertBy(): IEntityUpsertByCommand<
    TEntity,
    NonNullable<TEntitySearchParameters["filters"]>
  >
  get delete(): IEntityDeleteCommand<TEntity, TEntityId>
  get deleteItems(): IEntitiesDeleteCommand<
    TEntity,
    TEntitiesDeleteParameters,
    TSorting
  >
  get import(): IEntitiesImportCommand<TEntity>
  get parse(): IEntitiesParseCommand<TEntity>
  get export(): IEntitiesExportCommand<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor
  >
  get sampleDownload(): IEntitiesSampleDownloadCommand<TEntity>
  get versions(): IEntityVersionsSearchQuery<TEntity, TCursor>
}
