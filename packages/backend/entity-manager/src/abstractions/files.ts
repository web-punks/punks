export type FileData = {
  fileId?: string
  content: Buffer
  contentType: string
  fileName: string
  folderPath: string
  storageFilePath?: string
  storageBucket?: string
  metadata?: Record<string, any>
}

export type FileDownloadUrl = {
  downloadUrl: string
  contentType: string
  fileName: string
}

export type FileReference = {
  fileId: string
  reference: string
  providerId: string
}

export interface IFileManager {
  uploadFile(file: FileData): Promise<FileReference>
  removeFile(fileId: string): Promise<void>
  getFileContent(fileId: string): Promise<FileData>
  getFileDownloadUrl(fileId: string): Promise<FileDownloadUrl>
}

export type FilesReferenceData = {
  reference: string
  providerId: string
  metadata?: Record<string, any>
  fileId?: string
  fileName: string
  filePath: string
  fileSize: number
  contentType: string
}

export type FileReferenceRecord = FilesReferenceData & {
  fileId: string
  createdOn: Date
  updatedOn: Date
}

export interface IFilesReferenceRepository {
  searchReference({
    reference,
    providerId,
  }: {
    reference: string
    providerId: string
  }): Promise<FileReferenceRecord>
  getReference(fileId: string): Promise<FileReferenceRecord>
  createReference(file: FilesReferenceData): Promise<FileReferenceRecord>
  updateReference(
    fileId: string,
    file: FilesReferenceData
  ): Promise<FileReferenceRecord>
  deleteReference(fileId: string): Promise<void>
}

export type FileProviderReference = {
  reference: string
  bucket?: string
}

export type FileProviderUploadData = {
  fileId?: string
  fileName: string
  filePath?: string
  content: Buffer
  contentType: string
  bucket?: string
}

export type FileProviderDownloadData = {
  content: Buffer
}

export type FileProviderDownloadUrl = {
  downloadUrl: string
}

export interface IFileProvider {
  getProviderId(): string
  uploadFile(file: FileProviderUploadData): Promise<FileProviderReference>
  deleteFile(reference: FileProviderReference): Promise<void>
  downloadFile(
    reference: FileProviderReference
  ): Promise<FileProviderDownloadData>
  getFileProviderDownloadUrl(
    reference: FileProviderReference
  ): Promise<FileProviderDownloadUrl>
  get defaultBucket(): string
}
