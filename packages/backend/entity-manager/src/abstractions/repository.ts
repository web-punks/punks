import { EntityReference } from "../models"
import { DeepPartial } from "../types"

export interface IEntitiesDeleteResult {
  deletedCount: number
}

export interface IEntityRepository<
  TEntity,
  TEntityId,
  TGetConditions,
  TFindCondition,
  TUpdateCondition,
  TDeleteCondition
> {
  exists(id: TEntityId): Promise<boolean>
  existsBy(condition: TGetConditions): Promise<boolean>
  get(id: TEntityId): Promise<TEntity | undefined>
  getBy(condition: TGetConditions): Promise<TEntity | undefined>
  all(): Promise<TEntity[]>
  find(condition: TFindCondition): Promise<TEntity[]>
  findOne(condition: TFindCondition): Promise<TEntity | undefined>
  findOneOrFail(condition: TFindCondition): Promise<TEntity>
  findById(id: TEntityId[]): Promise<TEntity[]>
  count(condition: TFindCondition): Promise<number>
  delete(id: TEntityId): Promise<void>
  deleteBy(condition: TDeleteCondition): Promise<IEntitiesDeleteResult>
  create(entity: DeepPartial<TEntity>): Promise<TEntity>
  update(id: TEntityId, entity: DeepPartial<TEntity>): Promise<TEntity>
  updateBy(
    entity: DeepPartial<TEntity>,
    condition: TUpdateCondition
  ): Promise<TEntity[]>
  upsert(id: TEntityId, entity: DeepPartial<TEntity>): Promise<TEntity>
  upsertBy({
    data,
    filter,
  }: {
    data: Partial<TEntity>
    filter: TFindCondition
  }): Promise<EntityReference<string>>
}
