import {
  PipelineDefinition,
  PipelineOperationResult,
  PipelineResult,
} from "../types"

export interface IPipelineInstance<TPipelineInput, TPipelineOutput, TContext> {
  execute(): Promise<PipelineResult<TPipelineInput, TPipelineOutput>>
}

export interface IPipelinesController {
  createInstance<TPipelineInput, TPipelineOutput, TContext>(
    definition: PipelineDefinition<TPipelineInput, TPipelineOutput, TContext>,
    input: TPipelineInput,
    context: TContext | undefined
  ): Promise<IPipelineInstance<TPipelineInput, TPipelineOutput, TContext>>
}

export interface IPipelineOperationExecutor<
  TPipelineInput,
  TPipelineOutput,
  TContext
> {
  execute(
    input: TPipelineInput,
    context: TContext
  ): Promise<PipelineOperationResult<TPipelineInput, TPipelineOutput>>
}

export type PipelineConcurrency = "exclusive" | "sequential"
