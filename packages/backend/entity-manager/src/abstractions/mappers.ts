export interface IEntityMapper<TEntity, TMappedEntity> {
  mapEntity(entity: TEntity): Promise<TMappedEntity>
}
