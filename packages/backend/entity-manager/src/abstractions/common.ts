export type EnumType = string | number | symbol

export type SortingType = EnumType

export interface ClassType {
  new (...args: any[]): any
  constructor: {
    name: string
  }
}

export interface Type<T = any> extends Function {
  new (...args: any[]): T
}
