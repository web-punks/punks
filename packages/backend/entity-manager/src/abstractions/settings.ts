import { EntitiesImportExportSettings } from "./export"

// todo: parametrize buckets for supporting {organizationId}/path/to/file

export type EntityManagerSettings = {
  importExport: EntitiesImportExportSettings
  defaultBucketProvider: string
  defaultFilesProvider: string
  defaultMediaProvider: string
}

export type EntityVersioningOptions = {
  enabled: boolean
}

export type IEntityConfiguration = {
  name: string
  versioning?: EntityVersioningOptions
}
