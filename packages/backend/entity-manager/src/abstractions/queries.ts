import {
  IEntitiesSearchResults,
  IEntityVersionsSearchParameters,
  IEntityVersionsSearchResults,
} from "../models"
import { IAuthenticationContext } from "./authentication"
import { SortingType } from "./common"
import { IEntitiesDeleteResult } from "./repository"
import { IEntitySearchParameters, ISearchFilters } from "./searchParameters"
import { IEntityFacets } from "./searchResults"

export interface IEntitiesSearchQueryOptions<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    unknown,
    unknown
  >
> {
  relations?: TEntitySearchParameters["relations"]
}

export interface IEntitiesSearchQuery<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets
> {
  execute(
    request: TEntitySearchParameters,
    options?: IEntitiesSearchQueryOptions<TEntity, TEntitySearchParameters>
  ): Promise<
    IEntitiesSearchResults<
      TEntity,
      TEntitySearchParameters,
      TEntity,
      TSorting,
      TCursor,
      TFacets
    >
  >
}

export interface IEntityGetQueryOptions<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    unknown,
    unknown
  >
> {
  relations?: TEntitySearchParameters["relations"]
}

export interface IEntityGetQuery<
  TEntity,
  TEntityId,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    unknown,
    unknown
  >
> {
  execute(
    id: TEntityId,
    options?: IEntityGetQueryOptions<TEntity, TEntitySearchParameters>
  ): Promise<TEntity | undefined>
}

export interface IEntitiesFindQuery<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor
> {
  execute(request: {
    filters?: TEntitySearchParameters["filters"]
    sorting?: TEntitySearchParameters["sorting"]
    relations?: TEntitySearchParameters["relations"]
  }): Promise<TEntity>
}

export interface IEntityExistsQuery<
  TEntity,
  TEntityFilters extends ISearchFilters
> {
  execute(filters: TEntityFilters): Promise<boolean>
}

export interface IEntitiesCountQuery<
  TEntity,
  TEntityFilters extends ISearchFilters
> {
  execute(filters: TEntityFilters): Promise<number>
}

export interface IEntityVersionsSearchQuery<TEntity, TCursor> {
  execute(
    params: IEntityVersionsSearchParameters<TCursor>
  ): Promise<IEntityVersionsSearchResults<TEntity, TCursor>>
}

export interface IEntitiesQueryBuilder<
  TEntity,
  TEntityId,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets,
  TUserContext
> {
  get(
    id: TEntityId,
    context?: IAuthenticationContext<TUserContext>,
    options?: IEntityGetQueryOptions<TEntity, TEntitySearchParameters>
  ): Promise<TEntity | undefined>

  find(
    request?: {
      filters?: TEntitySearchParameters["filters"]
      sorting?: TEntitySearchParameters["sorting"]
      relations?: TEntitySearchParameters["relations"]
    },
    context?: IAuthenticationContext<TUserContext>
  ): Promise<TEntity>

  search(
    request: TEntitySearchParameters,
    context?: IAuthenticationContext<TUserContext>
  ): Promise<
    IEntitiesSearchResults<
      TEntity,
      TEntitySearchParameters,
      TEntity,
      TSorting,
      TCursor,
      TFacets
    >
  >

  exists(
    filters: NonNullable<TEntitySearchParameters["filters"]>,
    context?: IAuthenticationContext<TUserContext>
  ): Promise<boolean>

  count(
    filters: NonNullable<TEntitySearchParameters["filters"]>,
    context?: IAuthenticationContext<TUserContext>
  ): Promise<number>

  delete(
    filters: NonNullable<TEntitySearchParameters["filters"]>,
    context?: IAuthenticationContext<TUserContext>
  ): Promise<IEntitiesDeleteResult>
}
