import { ExcelParseOptions } from "@punks/backend-core"

export type EntryValidationError = {
  errorCode: string
  value?: any
  payload?: any
}

export type EntryValidationResult = {
  isValid: boolean
  validationErrors: EntryValidationError[]
}

export type ImportEntryValidationError = {
  errorCode: string
  column: {
    name: string
    key: string
    value?: any
  }
}

export type ImportEntryValidationResult = {
  isValid: boolean
  validationErrors: ImportEntryValidationError[]
}

export type EntitySerializerColumnParser = (value: any) => any

export type EntitySerializerColumnParseAction<TSheetItem> = (
  value: any,
  item: TSheetItem
) => void

export type EntitySerializerColumnConverter<TSheetItem> = (
  value: any,
  item: TSheetItem
) => any

export type EntitySerializerColumnValidator<TSheetItem> = {
  key: string
  fn: (item: TSheetItem) => EntryValidationResult
}

export type EntitySerializerSheetCustomParser = "date"

export type EntitySerializerColumnModifier = "trim" | "lowercase" | "uppercase"

export type EntitySerializerColumnDefinition<TSheetItem> = {
  name: string
  aliases?: string[]
  key: string
  selector: keyof TSheetItem | ((item: TSheetItem) => any)
  colSpan?: number
  sampleValue?: any
  parser?: EntitySerializerColumnParser
  parseAction?: EntitySerializerColumnParseAction<TSheetItem>
  converter?: EntitySerializerColumnConverter<TSheetItem>
  validators?: EntitySerializerColumnValidator<TSheetItem>[]
  idColumn?: boolean
  sheetParser?: EntitySerializerSheetCustomParser
  array?: boolean
  arraySeparator?: string | RegExp
  modifiers?: EntitySerializerColumnModifier[]
}

export type EntitySerializerSheetOptions = {
  name?: string
  useTypeColumn?: boolean
}

export type EntitySerializerSheetDefinition<TSheetItem> = {
  autoColumns?: boolean
  columns: EntitySerializerColumnDefinition<TSheetItem>[]
  sheet?: EntitySerializerSheetOptions
  options?: ExcelParseOptions
}

export type EntityExportFile = {
  content: Buffer
  contentType: string
  fileName: string
}

export enum EntitySerializationFormat {
  Csv = "csv",
  Json = "json",
  Xlsx = "xlsx",
}

export type ImportEntry<TSheetItem, TEntityId> = {
  id?: TEntityId
  item: TSheetItem
  rowIndex: number
  status: ImportEntryValidationResult
}

export interface IEntitySerializer<
  TEntity,
  TEntityId,
  TEntitySearchParameters,
  TSheetItem,
  TPayload = unknown
> {
  export(
    filters?: TEntitySearchParameters,
    payload?: TPayload
  ): Promise<TSheetItem[]>
  import(items: TSheetItem[], payload?: TPayload): Promise<void>
  serialize(
    items: TSheetItem[],
    format: EntitySerializationFormat,
    payload?: TPayload
  ): Promise<EntityExportFile>
  parse<TPayload>(
    data: Buffer,
    format: EntitySerializationFormat,
    payload?: TPayload
  ): Promise<ImportEntry<TSheetItem, TEntityId>[]>
  createSample(format: EntitySerializationFormat): Promise<EntityExportFile>
}
