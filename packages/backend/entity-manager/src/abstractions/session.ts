export interface IAppSessionService<TRequest> {
  getValue<T>(key: string): T
  setValue<T>(key: string, value: T): void
  clearValue(key: string): void
  getRequest(): TRequest
  retrieveRequest(): TRequest
}
