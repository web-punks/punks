export type CacheTtl = {
  value: number
  unit: "years" | "months" | "days" | "hours" | "minutes" | "seconds"
}

export type CacheEntryInfo = {
  instanceName: string
  key: string
  expiration: Date
  createdOn: Date
  updatedOn: Date
}

export type CacheEntryDetail = CacheEntryInfo & {
  data: any
}

export interface ICacheInstance {
  getInstanceName(): string
  getEntries(): Promise<CacheEntryInfo[]>
  getEntry(key: string): Promise<CacheEntryDetail | undefined>
  get<T>(key: string): Promise<T | undefined>
  set<T>(key: string, input: { value: T; ttl: CacheTtl }): Promise<void>
  retrieve<T>(
    key: string,
    input: { ttl: CacheTtl; valueFactory: () => Promise<T> }
  ): Promise<T>
  delete(key: string): Promise<void>
  clear(): Promise<void>
}

export interface ICache {
  getInstance(instanceName: string): Promise<ICacheInstance>
}
