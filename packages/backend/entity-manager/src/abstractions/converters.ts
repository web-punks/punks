import { DeepPartial } from "../types"
import { IAuthenticationContext } from "./authentication"

export interface IEntitiesSearchActionConverter<TEntity, TListItemDto> {
  toListItemDto(entity: TEntity): TListItemDto | Promise<TListItemDto>
}

export interface IEntityGetActionConverter<TEntity, TEntityDto> {
  toEntityDto(entity: TEntity): TEntityDto | Promise<TEntityDto>
}

export interface IEntityCreateActionConverter<
  TEntity,
  TEntityDto,
  TEntityCreateDto
> {
  createDtoToEntity(
    data: TEntityCreateDto,
    context?: IAuthenticationContext<unknown>
  ): DeepPartial<TEntity>
  toEntityDto(entity: TEntity): TEntityDto | Promise<TEntityDto>
}

export interface IEntityUpdateActionConverter<
  TEntity,
  TEntityDto,
  TEntityUpdateDto
> {
  toEntityDto(entity: TEntity): TEntityDto | Promise<TEntityDto>
  updateDtoToEntity(
    data: TEntityUpdateDto,
    context?: IAuthenticationContext<unknown>
  ): DeepPartial<TEntity>
}

export interface IEntityConverter<
  TEntity,
  TEntityDto,
  TListItemDto,
  TEntityCreateDto,
  TEntityUpdateDto
> extends IEntitiesSearchActionConverter<TEntity, TListItemDto>,
    IEntityGetActionConverter<TEntity, TEntityDto>,
    IEntityCreateActionConverter<TEntity, TEntityDto, TEntityCreateDto>,
    IEntityUpdateActionConverter<TEntity, TEntityDto, TEntityUpdateDto> {}
