import { EntityReference } from "../models"
import { SortingType } from "./common"
import {
  EntitiesExportInput,
  EntitiesExportResult,
  EntitiesSampleDownloadOptions,
  EntitiesSampleDownloadResult,
} from "./export"
import { EntitiesImportInput, EntitiesImportResult } from "./import"
import { EntitiesParseInput, EntitiesParseResult } from "./parse"
import {
  IEntitySearchParameters,
  ISearchFilters,
  ISearchSorting,
} from "./searchParameters"
import { EntityVersionOperation } from "./versioning"

export interface IEntityCreateCommand<TEntity, TEntityId, TEntityCreateData> {
  execute(input: TEntityCreateData): Promise<EntityReference<TEntityId>>
}

export interface IEntityUpdateCommand<TEntity, TEntityId, TEntityUpdateData> {
  execute(
    id: TEntityId,
    data: TEntityUpdateData
  ): Promise<EntityReference<TEntityId>>
}

export interface IEntityUpsertCommand<TEntity, TEntityId, TEntityUpdateData> {
  execute(
    id: TEntityId,
    data: TEntityUpdateData
  ): Promise<EntityReference<TEntityId>>
}

export interface IEntityDeleteCommand<TEntity, TEntityId> {
  execute(id: TEntityId): Promise<void>
}

export type EntityVersionCommandInput<TEntity, TEntityId> = {
  operation: EntityVersionOperation
  id: TEntityId
  entity?: TEntity
}

export interface IEntityVersionCommand<TEntity, TEntityId> {
  execute(input: EntityVersionCommandInput<TEntity, TEntityId>): Promise<void>
}

export interface IEntitiesDeleteParameters<TSorting> {
  filters?: ISearchFilters
  sorting?: ISearchSorting<TSorting>
}

export interface IEntitiesDeleteResult {
  deletedCount: number
}

export interface IEntitiesDeleteCommand<
  TEntity,
  TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
  TSorting
> {
  execute(params: TEntitiesDeleteParameters): Promise<IEntitiesDeleteResult>
}

export interface IEntitiesExportCommand<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor
> {
  execute<TPayload>(
    input: EntitiesExportInput<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TPayload
    >
  ): Promise<EntitiesExportResult>
}

export interface IEntitiesImportCommand<TEntity> {
  execute(input: EntitiesImportInput<unknown>): Promise<EntitiesImportResult>
}

export interface IEntitiesParseCommand<TEntity> {
  execute(
    input: EntitiesParseInput<unknown>
  ): Promise<EntitiesParseResult<unknown>>
}

export interface IEntitiesSampleDownloadCommand<TEntity> {
  execute(
    input: EntitiesSampleDownloadOptions
  ): Promise<EntitiesSampleDownloadResult>
}

export interface IEntityUpsertByParameters<
  TEntity,
  TEntityFilters extends ISearchFilters
> {
  filter: TEntityFilters
  data: Partial<TEntity>
}

export interface IEntityUpsertByResult {
  id: string
}

export interface IEntityUpsertByCommand<
  TEntity,
  TEntityFilters extends ISearchFilters
> {
  execute(
    params: IEntityUpsertByParameters<TEntity, TEntityFilters>
  ): Promise<IEntityUpsertByResult>
}
