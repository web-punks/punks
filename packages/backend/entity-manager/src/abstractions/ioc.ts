import { ClassType, Type } from "./common"

export interface IResolveServiceOptions {
  optional?: boolean
}

export interface IServiceLocator {
  register<T extends ClassType>(name: string, service: Type<T>): void
  registerEntityService<T>(
    serviceName: string,
    entityName: string,
    service: T
  ): void
  registerEntityServiceType<T>(
    serviceType: Type<T>,
    entityName: string,
    service: T
  ): void
  resolve<T extends ClassType>(
    name: string,
    options?: IResolveServiceOptions
  ): T
  resolveEntityService<T>(
    serviceName: string,
    entityName: string,
    options?: IResolveServiceOptions
  ): T
  resolveEntityServiceType<T>(
    serviceType: Type<T>,
    entityName: string,
    options?: IResolveServiceOptions
  ): T
}
