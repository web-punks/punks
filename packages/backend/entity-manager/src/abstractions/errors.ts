import { DeepPartial } from "../types"

const getOperationLabel = (operation: EntityOperationType): string => {
  switch (operation) {
    case EntityOperationType.Create:
      return "create"
    case EntityOperationType.Read:
      return "read"
    case EntityOperationType.Update:
      return "update"
    case EntityOperationType.Delete:
      return "delete"
    case EntityOperationType.Search:
      return "search"
    case EntityOperationType.Find:
      return "find"
  }
}

export class EntityManagerException extends Error {
  constructor(message: string) {
    super(message)
    this.name = "EntityManagerException"
  }
}

export enum EntityOperationType {
  Create,
  Update,
  Delete,
  Read,
  Search,
  Find,
}

export abstract class EntityManagerUnauthorizedException extends EntityManagerException {
  constructor(message: string) {
    super(message)
    this.name = "EntityManagerUnauthorizedException"
  }
}

export class EntityOperationUnauthorizedException<
  TEntity
> extends EntityManagerUnauthorizedException {
  private readonly operation: EntityOperationType
  private readonly entity?: DeepPartial<TEntity>

  constructor(
    operationType: EntityOperationType,
    entityName: string,
    entity?: DeepPartial<TEntity>
  ) {
    super(
      `The current user is not authorized to ${getOperationLabel(
        operationType
      )} the entity of type ${entityName}.`
    )
    this.entity = entity
    this.operation = operationType
  }

  public get getEntity() {
    return this.entity
  }

  public get getOperation() {
    return this.operation
  }
}

export class EntityNotFoundException<TEntityId> extends EntityManagerException {
  private readonly entityId: TEntityId | undefined
  private readonly entityType: string | undefined

  constructor(entityData: { id: TEntityId; type: string }) {
    super(
      entityData
        ? `Entity ${entityData.type} with id ${entityData.id} not found`
        : "Entity not found"
    )
    this.entityId = entityData.id
    this.entityType = entityData.type
  }

  public getEntityId() {
    return this.entityId
  }

  public getEntityType() {
    return this.entityType
  }
}

export class MultipleEntitiesFoundException extends EntityManagerException {
  constructor(params: object, items: any[]) {
    super(
      `Multiple entities found: ${items
        .map((item) => item.id)
        .join(", ")} - params: ${JSON.stringify(params)}`
    )
  }
}

export class EntityManagerConfigurationError extends EntityManagerException {
  constructor(message: string) {
    super(message)
    this.name = "EntityManagerConfigurationError"
  }
}

export class MissingEntityIdError extends EntityManagerException {
  constructor() {
    super("Missing entity id")
    this.name = "MissingEntityIdError"
  }
}
