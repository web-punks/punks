import { EntitySerializationFormat, ImportEntry } from "./serializer"

export type EntitiesParseInputFile = {
  fileName: string
  content: Buffer
  contentType: string
}

export type EntitiesParseError = {
  fieldName: string
  errorType: string
}

export type EntitiesParseResultItem<TItem> = {
  isValid: boolean
  item: TItem
  errors: EntitiesParseError[]
}

export type EntitiesParseResult<TItem> = {
  entries: ImportEntry<unknown, unknown>[]
}

export type EntitiesParseInput<TPayload> = {
  format: EntitySerializationFormat
  file: EntitiesParseInputFile
  payload?: TPayload
}
