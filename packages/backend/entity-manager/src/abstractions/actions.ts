import { IEntitiesSearchResults } from "../models"
import {
  IEntityVersionsSearchRequest,
  IEntityVersionsSearchResponse,
} from "../models/versioning"
import { IEntitiesDeleteParameters } from "./commands"
import { SortingType } from "./common"
import {
  EntitiesExportInput,
  EntitiesExportResult,
  EntitiesSampleDownloadOptions,
  EntitiesSampleDownloadResult,
} from "./export"
import { EntitiesImportInput, EntitiesImportResult } from "./import"
import { EntitiesParseInput, EntitiesParseResult } from "./parse"
import { IEntitiesSearchQueryOptions, IEntityGetQueryOptions } from "./queries"
import {
  IEntitySearchParameters,
  ISearchFilters,
  ISearchQueryRelations,
} from "./searchParameters"
import { IEntityFacets } from "./searchResults"

export interface IEntityCreateAction<TEntity, TEntityCreateDto, TEntityDto> {
  execute(input: TEntityCreateDto): Promise<TEntityDto>
}

export interface IEntityDeleteAction<TEntity, TEntityId> {
  execute(id: TEntityId): Promise<void>
}

export interface IEntitiesDeleteAction<
  TEntity,
  TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
  TSorting
> {
  execute(params: TEntitiesDeleteParameters): Promise<void>
}

export interface IEntityGetActionOptions<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    unknown,
    unknown
  >
> extends IEntityGetQueryOptions<TEntity, TEntitySearchParameters> {}

export interface IEntityGetAction<
  TEntity,
  TEntityId,
  TEntityDto,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    unknown,
    unknown
  >
> {
  execute(
    id: TEntityId,
    options?: IEntityGetActionOptions<TEntity, TEntitySearchParameters>
  ): Promise<TEntityDto | undefined>
}

export interface IEntityExistsAction<
  TEntity,
  TEntityFilters extends ISearchFilters
> {
  execute(filters: TEntityFilters): Promise<boolean>
}

export interface IEntitiesCountAction<
  TEntity,
  TEntityFilters extends ISearchFilters
> {
  execute(filters: TEntityFilters): Promise<number>
}

export interface IEntityUpdateAction<
  TEntity,
  TEntityId,
  TEntityUpdateDto,
  TEntityDto
> {
  execute(id: TEntityId, data: TEntityUpdateDto): Promise<TEntityDto>
}

export interface IEntityUpsertAction<
  TEntity,
  TEntityId,
  TEntityUpdateDto,
  TEntityDto
> {
  execute(id: TEntityId, input: TEntityUpdateDto): Promise<TEntityDto>
}

export interface IEntitiesSearchActionOptions<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    unknown,
    unknown
  >,
  TListItemDto
> extends IEntitiesSearchQueryOptions<TEntity, TEntitySearchParameters> {
  converter?: (item: TEntity) => TListItemDto
}

export interface IEntitiesSearchAction<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TEntityListItemDto,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets
> {
  execute(
    request: TEntitySearchParameters,
    options?: IEntitiesSearchActionOptions<
      TEntity,
      TEntitySearchParameters,
      TEntityListItemDto
    >
  ): Promise<
    IEntitiesSearchResults<
      TEntity,
      TEntitySearchParameters,
      TEntityListItemDto,
      TSorting,
      TCursor,
      TFacets
    >
  >
}

export interface IEntitiesExportAction<
  TEntity,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor
> {
  execute<TPayload = unknown>(
    input: EntitiesExportInput<
      TEntity,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TPayload
    >
  ): Promise<EntitiesExportResult>
}

export interface IEntitiesImportAction<TEntity> {
  execute<TPayload = unknown>(
    input: EntitiesImportInput<TPayload>
  ): Promise<EntitiesImportResult>
}

export interface IEntitiesParseAction<TEntity> {
  execute<TPayload = unknown>(
    input: EntitiesParseInput<TPayload>
  ): Promise<EntitiesParseResult<unknown>>
}

export interface IEntitiesSampleDownloadAction<TEntity> {
  execute(
    input: EntitiesSampleDownloadOptions
  ): Promise<EntitiesSampleDownloadResult>
}

export interface IEntityVersionsSearchAction<
  TEntity,
  TEntityVersionDto,
  TCursor
> {
  execute(
    request: IEntityVersionsSearchRequest<TCursor>
  ): Promise<IEntityVersionsSearchResponse<TEntityVersionDto, TCursor>>
}

export interface IEntityActions<
  TEntity,
  TEntityId,
  TEntityCreateDto,
  TEntityUpdateDto,
  TEntityDto,
  TEntityListItemDto,
  TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets
> {
  get get(): IEntityGetAction<
    TEntity,
    TEntityId,
    TEntityDto,
    TEntitySearchParameters
  >
  get search(): IEntitiesSearchAction<
    TEntity,
    TEntitySearchParameters,
    TEntityListItemDto,
    TSorting,
    TCursor,
    TFacets
  >
  get exists(): IEntityExistsAction<
    TEntity,
    NonNullable<TEntitySearchParameters["filters"]>
  >
  get count(): IEntitiesCountAction<
    TEntity,
    NonNullable<TEntitySearchParameters["filters"]>
  >
  get create(): IEntityCreateAction<TEntity, TEntityCreateDto, TEntityDto>
  get update(): IEntityUpdateAction<
    TEntity,
    TEntityId,
    TEntityUpdateDto,
    TEntityDto
  >
  get upsert(): IEntityUpsertAction<
    TEntity,
    TEntityId,
    TEntityUpdateDto,
    TEntityDto
  >
  get delete(): IEntityDeleteAction<TEntity, TEntityId>
  get deleteItems(): IEntitiesDeleteAction<
    TEntity,
    TEntitiesDeleteParameters,
    TSorting
  >

  get export(): IEntitiesExportAction<
    TEntity,
    TEntitySearchParameters,
    TSorting,
    TCursor
  >

  get import(): IEntitiesImportAction<TEntity>
  get parse(): IEntitiesParseAction<TEntity>
  get sampleDownload(): IEntitiesSampleDownloadAction<TEntity>

  get versions(): IEntityVersionsSearchAction<
    TEntity,
    TEntityListItemDto,
    TCursor
  >
}
