export interface IEntitySnapshotService<TEntityId, TEntitySnapshot> {
  getSnapshot(id: TEntityId): Promise<TEntitySnapshot>
}
