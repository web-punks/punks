import { DeepPartial } from "../types"
import { IAuthenticationContext } from "./authentication"

export interface IEntityCreateCommandAdapter<TEntity, TEntityCreateData> {
  createDataToEntity(
    data: TEntityCreateData,
    context: IAuthenticationContext<unknown>
  ): DeepPartial<TEntity>
}

export interface IEntityUpdateCommandAdapter<TEntity, TEntityUpdateData> {
  updateDataToEntity(
    data: TEntityUpdateData,
    context: IAuthenticationContext<unknown>
  ): DeepPartial<TEntity>
}

export interface IEntityAdapter<TEntity, TEntityCreateData, TEntityUpdateData>
  extends IEntityCreateCommandAdapter<TEntity, TEntityCreateData>,
    IEntityUpdateCommandAdapter<TEntity, TEntityUpdateData> {}
