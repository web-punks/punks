import { INestApplicationContext } from "@nestjs/common"

export interface IAppInitializer {
  initialize(app: INestApplicationContext): Promise<void>
}
