import { ServiceLocator } from "../ioc"
import {
  EntitiesServiceLocator,
  EntityServiceLocator,
} from "../providers/services"
import { IEntityAdapter } from "./adapters"
import {
  IAuthenticationContext,
  IAuthenticationContextProvider,
} from "./authentication"
import { IEntityAuthorizationMiddleware } from "./authorization"
import { SortingType } from "./common"
import { ConnectorOptions, IEntityConnector } from "./connectors"
import { IEntityConverter } from "./converters"
import { IEntityMapper } from "./mappers"
import { IEntitiesQueryBuilder } from "./queries"
import { ReplicaOptions } from "./replication"
import { IEntityRepository } from "./repository"
import { IEntitySearchParameters } from "./searchParameters"
import { IEntityFacets } from "./searchResults"
import { IEntitySerializer } from "./serializer"
import { IEntityConfiguration, EntityManagerSettings } from "./settings"
import { IEntitySnapshotService } from "./snapshot"

export type EntitiesMapOperationsInput<TEntitiesQueryBuilder> = {
  queryBuilder: TEntitiesQueryBuilder
  settings: EntityManagerSettings
}

export interface IEntityManagerServiceRoot {
  locator: ServiceLocator

  getEntitiesServicesLocator(): EntitiesServiceLocator

  getEntityServicesLocator<TEntity, TEntityId>(
    entityName: string
  ): EntityServiceLocator<TEntity, TEntityId>

  addAuthentication<
    TAuthenticationContextProvider extends IAuthenticationContextProvider<TUserContext>,
    TUserContext
  >({
    provider,
  }: {
    provider: TAuthenticationContextProvider
  }): void

  registerEntity<
    TEntity,
    TEntityId,
    TRepository extends IEntityRepository<
      TEntity,
      TEntityId,
      unknown,
      unknown,
      unknown,
      unknown
    >
  >(
    entity: IEntityConfiguration,
    repository: TRepository
  ): IEntityManagerServiceCollection<TEntity, TEntityId>
}

export interface IEntityManagerServiceCollection<TEntity, TEntityId> {
  getServiceLocator(): EntityServiceLocator<TEntity, TEntityId>

  mapCrudOperations<
    TEntitySearchParameters extends IEntitySearchParameters<
      TEntity,
      TSorting,
      TCursor
    >,
    TSorting extends SortingType,
    TCursor,
    TFacets extends IEntityFacets,
    TEntitiesQueryBuilder extends IEntitiesQueryBuilder<
      TEntity,
      unknown,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      TFacets,
      unknown
    >
  >(
    input: EntitiesMapOperationsInput<TEntitiesQueryBuilder>
  ): IEntityManagerServiceCollection<TEntity, TEntityId>

  addSerializer<
    TEntitySerializer extends IEntitySerializer<
      TEntity,
      TEntityId,
      unknown,
      unknown
    >
  >(
    serializer: TEntitySerializer
  ): IEntityManagerServiceCollection<TEntity, TEntityId>

  addSnapshotService<
    TEntitySnapshotService extends IEntitySnapshotService<TEntityId, unknown>
  >(
    service: TEntitySnapshotService
  ): IEntityManagerServiceCollection<TEntity, TEntityId>

  addConverter<
    TEntityDto,
    TListItemDto,
    TEntityCreateInput,
    TEntityUpdateInput,
    TEntityConverter extends IEntityConverter<
      TEntity,
      TEntityDto,
      TListItemDto,
      TEntityCreateInput,
      TEntityUpdateInput
    >
  >(
    converter: TEntityConverter
  ): IEntityManagerServiceCollection<TEntity, TEntityId>

  addAdapter<
    TEntityCreateData,
    TEntityUpdateData,
    TEntityAdapter extends IEntityAdapter<
      TEntity,
      TEntityCreateData,
      TEntityUpdateData
    >
  >(
    adapter: TEntityAdapter
  ): IEntityManagerServiceCollection<TEntity, TEntityId>

  addAuthorization<
    TEntityAuthorizationMiddleware extends IEntityAuthorizationMiddleware<
      TEntity,
      TAuthenticationContext,
      TUserContext
    >,
    TAuthenticationContext extends IAuthenticationContext<TUserContext>,
    TUserContext
  >({
    middleware,
  }: {
    middleware: TEntityAuthorizationMiddleware
  }): IEntityManagerServiceCollection<TEntity, TEntityId>

  withReplica<
    TRepository extends IEntityRepository<
      TEntity,
      TEntityId,
      unknown,
      unknown,
      unknown,
      unknown
    >
  >({
    name,
    options,
    repository,
  }: {
    name: string
    options: ReplicaOptions
    repository: TRepository
  }): IEntityManagerServiceCollection<TEntity, TEntityId>

  withSynchronization<
    TMappedType,
    TMapper extends IEntityMapper<TEntity, TMappedType>,
    TEntityConnector extends IEntityConnector<TEntity, TEntityId, TMappedType>
  >({
    name,
    options,
    connector,
    mapper,
  }: {
    name: string
    options: ConnectorOptions
    connector: TEntityConnector
    mapper: TMapper
  }): IEntityManagerServiceCollection<TEntity, TEntityId>
}
