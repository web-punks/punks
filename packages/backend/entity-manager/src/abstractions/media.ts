export type MediaUploadInput = {
  fileName: string
  content: Buffer
  contentType?: string
  folderId?: string
}

export type MediaReference = {
  ref: string
}

export type MediaItemReference = {
  id: string
  ref: string
  folderId?: string
}

export type IMediaProvider = {
  mediaUpload(input: MediaUploadInput): Promise<MediaReference>
  mediaDelete(input: MediaReference): Promise<void>
  mediaDownload(input: MediaReference): Promise<Buffer>
  getFileName(input: MediaReference): string
  getProviderId(): string
}

export type MediaReferenceRecord = {
  id: string
  ref: string
  folderId?: string
  createdOn: Date
  updatedOn: Date
}

export type MediaReferenceCreateInput = {
  media: MediaReference
  fileName: string
  contentType?: string
  folderId?: string
  providerId: string
}

export type IMediaReferenceRepository = {
  getReference(file: MediaReference): Promise<MediaReferenceRecord>
  createReference(
    input: MediaReferenceCreateInput
  ): Promise<MediaReferenceRecord>
  updateReferenceFolder(
    file: MediaReference,
    folderId: string | undefined
  ): Promise<void>
  deleteReference(file: MediaReference): Promise<void>
  getFolderReferences(folderId?: string): Promise<MediaReferenceRecord[]>
}

export type MediaFolderRecord = {
  id: string
  name: string
  createdOn: Date
  updatedOn: Date
}

export type IMediaFolderRepository = {
  folderCreate(input: {
    name: string
    parentId?: string
    organizationId?: string
  }): Promise<MediaFolderRecord>
  folderRename(id: string, name: string): Promise<void>
  folderMove(id: string, parentId?: string): Promise<void>
  folderDelete(id: string): Promise<void>
  foldersList(parentId?: string): Promise<MediaFolderRecord[]>
  folderFind(name: string, parentId?: string): Promise<MediaFolderRecord>
}

export type MediaFolderCreateInput = {
  folderName: string
  parentId?: string
  organizationId?: string
}

export type MediaFolderReference = {
  id: string
  name: string
}

export type MediaFolderRenameInput = {
  id: string
  folderName?: string
}

export type MediaFolderMoveInput = {
  id: string
  folderId?: string
}

export type MediaInfo = {
  ref: string
  fileName: string
}

export type MediaFolderEnsureInput = {
  path: string[]
  organizationId?: string
}

export type IMediaLibraryManager = {
  mediaUpload(input: MediaUploadInput): Promise<MediaItemReference>
  mediaDelete(input: MediaReference): Promise<void>
  mediaDownload(input: MediaReference): Promise<Buffer>
  folderEnsure(input: MediaFolderEnsureInput): Promise<MediaFolderReference>
  folderCreate(input: MediaFolderCreateInput): Promise<MediaFolderReference>
  folderDelete(input: MediaFolderReference): Promise<void>
  folderMove(input: MediaFolderMoveInput): Promise<void>
  folderRename(input: MediaFolderRenameInput): Promise<void>
  foldersList(parent?: MediaFolderReference): Promise<MediaFolderReference[]>
  getFolderMedia(input: MediaFolderReference): Promise<MediaInfo[]>
}
