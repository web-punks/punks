export enum TaskRunType {
  Scheduled = "Scheduled",
  OnDemand = "OnDemand",
}

export type TaskContext = {
  name: string
  runId: string
  runType: TaskRunType
  startedAt: Date
}

export interface ITask {
  execute(context: TaskContext): Promise<void>
}
