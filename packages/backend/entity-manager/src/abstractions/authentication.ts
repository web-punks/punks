import { IAuthUser } from "../platforms"

export interface IAuthenticationUserRole {
  uid: string
}

export interface IAuthenticationUserPermission {
  uid: string
}

export interface IAuthenticationOrganizationalUnit {
  id: string
  uid: string
}

export interface IAuthenticationContext<TUserContextData> {
  isAuthenticated: boolean
  isAnonymous: boolean
  apiKeyId?: string
  userId?: string
  userContext?: TUserContextData
  userRoles?: IAuthenticationUserRole[]
  userPermissions?: IAuthenticationUserPermission[]
  userOrganizationalUnits?: IAuthenticationOrganizationalUnit[]
}

export interface IAuthenticationContextProvider<TUserContext> {
  getContext(): Promise<IAuthenticationContext<TUserContext>>
}

export interface IAuthenticationData {
  user: IAuthUser
  roles: IAuthenticationUserRole[]
  permissions: IAuthenticationUserPermission[]
  organizationalUnits: IAuthenticationOrganizationalUnit[]
}

export interface IAuthenticationMiddleware<
  TAuthenticationData extends IAuthenticationData,
  TOutput
> {
  processContext(data: TAuthenticationData): Promise<TOutput>
  getPriority(): number
}
