export type BucketFolderListInput = {
  bucket: string
  maxResults?: number
  path?: string
}

export enum BucketItemType {
  File = "file",
  Folder = "folder",
}

export type BucketContentItem = {
  type: BucketItemType
  name: string
  path: string
}

export type BucketFolderContentResult = {
  items: BucketContentItem[]
}

export type BucketFolderListResult = {}

export type BucketFolderCreateInput = {
  bucket: string
  path: string
}

export type BucketFolderEnsureInput = {
  bucket: string
  path: string
}

export type BucketFolderExistsInput = {
  bucket: string
  path: string
}

export type BucketFileExistsInput = {
  bucket: string
  filePath: string
}

export type BucketFileDownloadInput = {
  bucket: string
  filePath: string
}

export type BucketFilePublicUrlCreateInput = {
  bucket: string
  filePath: string
  expirationMinutes: number
}

export type BucketFileUploadInput = {
  bucket: string
  filePath: string
  content: Buffer
  contentType?: string
}

export type BucketFileDeleteInput = {
  bucket: string
  filePath: string
}

export type BucketFileCopyInput = {
  bucket: string
  filePath: string
  targetBucket: string
  targetPath: string
}

export type BucketFileMoveInput = {
  bucket: string
  filePath: string
  targetBucket: string
  targetPath: string
}

export interface IBucketProvider {
  folderList(input: BucketFolderListInput): Promise<BucketFolderListResult>
  folderEnsure(input: BucketFolderEnsureInput): Promise<void>
  folderExists(input: BucketFolderExistsInput): Promise<boolean>
  fileExists(input: BucketFileExistsInput): Promise<boolean>
  fileDownload(input: BucketFileDownloadInput): Promise<Buffer>
  filePublicUrlCreate(input: BucketFilePublicUrlCreateInput): Promise<string>
  fileUpload(input: BucketFileUploadInput): Promise<void>
  fileDelete(input: BucketFileDeleteInput): Promise<void>
}
