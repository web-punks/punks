export type LocalizedMap<T> = {
  [key: string]: T
}

export type LocalizedTexts = LocalizedMap<string>
