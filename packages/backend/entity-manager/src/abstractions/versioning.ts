import {
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchParameters,
} from "../models"

export enum EntityVersionOperation {
  Create = "create",
  Update = "update",
  Upsert = "upsert",
  Delete = "delete",
}

export type EntityVersionInput<TEntity, TEntityId> = {
  operationType: EntityVersionOperation
  entityType: string
  entityId: TEntityId
  data?: TEntity
  modifiedByUserId?: string
}

export interface IEntityVersioningResults<TResult, TCursor> {
  paging?: IEntityVersionsResultsPaging<TCursor>
  items: TResult[]
}

export interface IEntityVersionsSearchInput<TCursor> {
  entityType: string
  params: IEntityVersionsSearchParameters<TCursor>
}

export interface IEntityVersioningProvider {
  isEnabled(): boolean
  createVersion<TEntity, TEntityId>(
    input: EntityVersionInput<TEntity, TEntityId>
  ): Promise<void>
  searchVersions<TEntity, TCursor>(
    input: IEntityVersionsSearchInput<TCursor>
  ): Promise<IEntityVersioningResults<TEntity, TCursor>>
}
