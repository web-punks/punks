export type LockItem = {
  uid: string
  lockedBy?: string
  createdOn: Date
}

export type LockAcquireInput = {
  lockUid: string
  requestedBy?: string
}

export type LockAcquireResult = {
  available: boolean
  lockItem: LockItem
}

export type LockReleaseInput = {
  lockUid: string
}

export class LockNotFoundError extends Error {}

export interface ILockRepository {
  acquireLock(input: LockAcquireInput): Promise<LockAcquireResult>
  releaseLock(input: LockReleaseInput): Promise<void>
  getLock(lockUid: string): Promise<LockItem | undefined>
}

export interface ExecuteSequentialInput<T> {
  lockUid: string
  requestedBy?: string
  lockTimeout?: number
  lockPolling?: number
  operation: () => Promise<T>
}

export class ExclusiveOperationResult<T> {
  skipped: boolean
  result?: T
}

export interface ExecuteExclusiveInput<T> {
  lockUid: string
  requestedBy?: string
  lockTimeout?: number
  operation: () => Promise<T>
}

export interface IOperationLockService {
  executeSequential<T>(input: ExecuteSequentialInput<T>): Promise<T>

  executeExclusive<T>(
    input: ExecuteExclusiveInput<T>
  ): Promise<ExclusiveOperationResult<T>>
}
