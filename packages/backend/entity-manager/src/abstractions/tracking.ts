export interface IEventLog<TPayload> {
  type: string
  payload?: TPayload
  userId?: string
  userName?: string
  timestamp: Date
}

export interface IEventsTracker<TEventLog extends IEventLog<unknown>> {
  track(log: TEventLog): Promise<void>
}
