export type FilterExpression<TEntity> = (entity: TEntity) => boolean
