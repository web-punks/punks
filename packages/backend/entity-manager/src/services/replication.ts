import {
  IEntityReplicaDeleteManager,
  IEntityReplicaSyncManager,
  ReplicaConfiguration,
  ReplicationMode,
} from "../abstractions/replication"
import { IEntityRepository } from "../abstractions/repository"
import { EntityServiceLocator } from "../providers/services"

export class EntityReplicaManager<TEntity, TEntityId>
  implements
    IEntityReplicaSyncManager<TEntity>,
    IEntityReplicaDeleteManager<TEntity, TEntityId>
{
  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  async syncReplicas(entity: TEntity) {
    for (const replica of this.getEnabledReplicas()) {
      switch (replica.options.mode) {
        case ReplicationMode.Sync:
          await this.syncReplica(entity, replica)
          break
        case ReplicationMode.Async:
          this.syncReplica(entity, replica)
          break
        default:
          throw new Error("Invalid replication mode")
      }
    }
  }

  private async syncReplica(
    entity: TEntity,
    replica: ReplicaConfiguration<TEntity, TEntityId>
  ) {
    var repository = this.resolveRepository(replica)
    await repository.upsert((entity as any).id, entity)
  }

  async deleteReplicas(entityId: TEntityId) {
    for (const replica of this.getEnabledReplicas()) {
      switch (replica.options.mode) {
        case ReplicationMode.Sync:
          await this.deleteReplica(entityId, replica)
          break
        case ReplicationMode.Async:
          this.deleteReplica(entityId, replica)
          break
        default:
          throw new Error("Invalid replication mode")
      }
    }
  }

  private async deleteReplica(
    entityId: TEntityId,
    replica: ReplicaConfiguration<TEntity, TEntityId>
  ) {
    var repository = this.resolveRepository(replica)
    if (await repository.exists(entityId)) {
      await repository.delete(entityId)
    }
  }

  private getEnabledReplicas(): ReplicaConfiguration<TEntity, TEntityId>[] {
    return this.services
      .resolveReplicaConfiguration()
      .getReplicas()
      .filter((x) => x.options.enabled != false)
  }

  private resolveRepository(
    replica: ReplicaConfiguration<TEntity, TEntityId>
  ): IEntityRepository<TEntity, TEntityId, unknown, unknown, unknown, unknown> {
    return replica.repository
  }
}
