import { Log } from "@punks/backend-core"
import { IEntityEventsManager } from "../abstractions/events"
import { EntityServiceLocator } from "../providers/services"
import {
  EntityCreatedPayload,
  EntityDeletedPayload,
  EntityUpdatedPayload,
} from "./types"

export class EntityEventsManager<TEntity, TEntityId>
  implements IEntityEventsManager<TEntity, TEntityId>
{
  private readonly logger = Log.getLogger("EntityEventsManager")

  constructor(
    private readonly entityName: string,
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {}

  async processEntityCreatedEvent(entity: TEntity): Promise<void> {
    await this.services.resolveReplicaSyncManager().syncReplicas(entity)
    await this.services.resolveConnectorSyncManager().syncEntity(entity)
    await this.emitEntityEvent("created", {
      entity,
    } as EntityCreatedPayload<TEntity>)
  }

  async processEntityUpdatedEvent(entity: TEntity): Promise<void> {
    await this.services.resolveReplicaSyncManager().syncReplicas(entity)
    await this.services.resolveConnectorSyncManager().syncEntity(entity)
    await this.emitEntityEvent("updated", {
      entity,
    } as EntityUpdatedPayload<TEntity>)
  }

  async processEntityDeletedEvent(id: TEntityId): Promise<void> {
    this.services.resolveReplicaDeleteManager().deleteReplicas(id)
    this.services.resolveConnectorDeleteManager().deleteEntity(id)
    await this.emitEntityEvent("deleted", {
      id,
    } as EntityDeletedPayload<TEntityId>)
  }

  private async emitEntityEvent(event: string, payload: any) {
    const eventEmitter = this.services.getRootServices().resolveEventEmitter()
    if (!eventEmitter) {
      return
    }

    this.logger.debug(`Event emitted: ${this.buildEventName(event)}`, payload)
    await eventEmitter.emit(this.buildEventName(event), payload)
  }

  private buildEventName(event: string): string {
    return `${this.entityName}.${event}`
  }
}
