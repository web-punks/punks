export type EntityCreatedPayload<TEntity> = {
  entity: TEntity
}

export type EntityUpdatedPayload<TEntity> = {
  entity: TEntity
}

export type EntityDeletedPayload<TEntityId> = {
  id: TEntityId
}
