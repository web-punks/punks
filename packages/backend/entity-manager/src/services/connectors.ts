import { ILogger, Log } from "@punks/backend-core"
import {
  ConnectorConfiguration,
  ConnectorMode,
  IEntityConnector,
  IEntityConnectorDeleteManager,
  IEntityConnectorSyncManager,
} from "../abstractions/connectors"
import { IEntityMapper } from "../abstractions/mappers"
import { EntityServiceLocator } from "../providers/services"

export class EntityConnectorsManager<TEntity, TEntityId>
  implements
    IEntityConnectorSyncManager<TEntity>,
    IEntityConnectorDeleteManager<TEntity, TEntityId>
{
  private readonly logger: ILogger

  constructor(
    private readonly services: EntityServiceLocator<TEntity, TEntityId>
  ) {
    this.logger = Log.getLogger(
      `[${services.getEntityName()}]: ${EntityConnectorsManager.name}`
    )
  }

  async syncEntity(entity: TEntity) {
    for (const connector of this.getEnabledConnectors()) {
      this.logger.debug(`Entity sync started - connector: ${connector.name}`, {
        entity,
      })

      switch (connector.options.mode) {
        case ConnectorMode.Sync:
          await this.processSync(connector, entity)
          this.logger.debug(
            `Entity sync completed - connector: ${connector.name}`,
            {
              connector: connector.name,
              entity,
            }
          )
          break
        case ConnectorMode.Async:
          this.processSync(connector, entity)
            .then(() =>
              this.logger.debug(
                `Entity sync completed - connector: ${connector.name}`,
                {
                  connector: connector.name,
                  entity,
                }
              )
            )
            .catch((error) => {
              this.logger.exception(
                `Entity sync error - connector: ${connector.name}`,
                error,
                {
                  connector: connector.name,
                  entity,
                }
              )
            })
          break
        default:
          throw new Error("Invalid connector mode found")
      }
    }
  }

  private async processSync<TMappedType>(
    connector: ConnectorConfiguration<TEntity>,
    entity: TEntity
  ) {
    const connectorInstance = this.resolveConnector<TMappedType>(connector)
    var mapper = this.resolveMapper<TMappedType>(connector)
    var mappedEntity = await mapper.mapEntity(entity)
    await connectorInstance.syncEntity(mappedEntity)
  }

  public async deleteEntity(entityId: TEntityId) {
    for (const connector of this.getEnabledConnectors()) {
      this.logger.debug(
        `Entity sync delete started - connector: ${connector.name}`,
        {
          entityId,
        }
      )

      switch (connector.options.mode) {
        case ConnectorMode.Sync:
          await this.processDelete(connector, entityId)
          this.logger.debug(
            `Entity sync delete completed - connector: ${connector.name}`,
            {
              entityId,
            }
          )
          break
        case ConnectorMode.Async:
          this.processDelete(connector, entityId)
            .then(() =>
              this.logger.debug(
                `Entity sync delete completed - connector: ${connector.name}`,
                {
                  connector: connector.name,
                  entityId,
                }
              )
            )
            .catch((error) => {
              this.logger.exception(
                `Entity sync delete error - connector: ${connector.name}`,
                error,
                {
                  connector: connector.name,
                  entityId,
                }
              )
            })
          break
        default:
          throw new Error("Invalid connector mode")
      }
    }
  }

  private async processDelete(
    connector: ConnectorConfiguration<TEntity>,
    entityId: TEntityId
  ) {
    const connectorInstance = this.resolveConnector(connector)
    await connectorInstance.deleteEntity(entityId)
  }

  private resolveConnector<TMappedType>(
    connector: ConnectorConfiguration<TEntity>
  ): IEntityConnector<TEntity, unknown, TMappedType> {
    return connector.connector as IEntityConnector<
      TEntity,
      unknown,
      TMappedType
    >
  }

  private resolveMapper<TMappedType>(
    connector: ConnectorConfiguration<TEntity>
  ): IEntityMapper<TEntity, TMappedType> {
    return connector.mapper as IEntityMapper<TEntity, TMappedType>
  }

  private getEnabledConnectors(): ConnectorConfiguration<TEntity>[] {
    return this.services
      .resolveConnectorsConfiguration()
      .getConnectors()
      .filter((x) => x.options.enabled !== false)
  }
}
