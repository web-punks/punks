import {
  AppSecret,
  AppSecretDefinition,
  AppSecretsPageMetadata,
  ISecretsProvider,
} from "../../abstractions/secrets"
import { WpSecretsProvider } from "../../platforms/nest/decorators/secrets"

type SecretValue = {
  definition: AppSecretDefinition
  value: AppSecret<unknown>
}

type SecretsPage = {
  metadata: AppSecretsPageMetadata
  secrets: Record<string, SecretValue>
}

@WpSecretsProvider("inMemorySecretsManager")
export class InMemorySecretsProvider implements ISecretsProvider {
  private readonly secrets: Map<string, SecretsPage> = new Map()

  async getSecrets(pageName: string): Promise<AppSecret<any>[]> {
    return Object.values(this.secrets.get(pageName)!.secrets).map(
      (secret) => secret.value
    )
  }

  async setSecret<TValue>(
    pageName: string,
    secret: AppSecret<TValue>
  ): Promise<void> {
    this.secrets.get(pageName)!.secrets[secret.key] = {
      definition: {
        key: secret.key,
        type: secret.type,
        hidden: secret.hidden,
        defaultValue: secret.value,
      },
      value: secret,
    }
  }

  async defineSecret(
    pageName: string,
    definition: AppSecretDefinition
  ): Promise<void> {
    this.secrets.get(pageName)!.secrets[definition.key] = {
      definition,
      value: {
        key: definition.key,
        value: definition.defaultValue,
        type: definition.type,
        hidden: definition.hidden,
        timestamp: Date.now(),
      },
    }
  }

  async pageInitialize(
    pageName: string,
    metadata: AppSecretsPageMetadata
  ): Promise<void> {
    this.secrets.set(pageName, { metadata, secrets: {} })
  }

  async pageEnsure(
    pageName: string,
    metadata: AppSecretsPageMetadata
  ): Promise<void> {
    if (this.secrets.has(pageName)) {
      return
    }
    this.secrets.set(pageName, { metadata, secrets: {} })
  }

  async pageExists(pageName: string): Promise<boolean> {
    return this.secrets.has(pageName)
  }
}
