import { IAppSessionService } from "../../abstractions/session"

export class TestingAppSessionService implements IAppSessionService<unknown> {
  private readonly values: Map<string, any> = new Map()

  getValue<T>(key: string) {
    return this.values.get(key) as T
  }

  setValue<T>(key: string, value: T) {
    this.values.set(key, value)
  }

  clearValue(key: string) {
    this.values.set(key, undefined)
  }

  getRequest() {
    return this.values.get("request")
  }

  mockRequest(request: any) {
    this.values.set("request", request)
  }

  retrieveRequest() {
    return this.getRequest()
  }
}
