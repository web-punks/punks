import { FilterExpression } from "../../abstractions/filters"
import {
  IEntitiesDeleteResult,
  IEntityRepository,
} from "../../abstractions/repository"
import { EntityReference } from "../../models"

export class InMemoryRepository<TEntity>
  implements
    IEntityRepository<
      TEntity,
      string,
      FilterExpression<TEntity>,
      FilterExpression<TEntity>,
      FilterExpression<TEntity>,
      FilterExpression<TEntity>
    >
{
  private entities: { [key: string]: TEntity } = {}

  exists(id: string): Promise<boolean> {
    if (typeof id === "undefined") {
      throw new Error("Invalid 'id' parameter.")
    }

    if (typeof id === "object") {
      const key = this.getId(id as TEntity)
      const entity = this.entities[key]
      return Promise.resolve(!!entity)
    }

    // Check if an entity with the specified id exists
    const key = (id as any).toString()
    const entity = this.entities[key]
    return Promise.resolve(!!entity)
  }

  existsBy(condition: FilterExpression<TEntity>): Promise<boolean> {
    const filteredEntities = this.filterEntities(condition)
    return Promise.resolve(filteredEntities.length > 0)
  }

  get(id: string): Promise<TEntity | undefined> {
    if (typeof id === "undefined") {
      throw new Error("Invalid 'condition' parameter.")
    }

    const entity = this.entities[id]
    return Promise.resolve(entity)
  }

  getBy(condition: FilterExpression<TEntity>): Promise<TEntity | undefined> {
    const entities = this.filterEntities(condition)
    return Promise.resolve(entities[0])
  }

  all(): Promise<TEntity[]> {
    const entities = Object.values(this.entities)
    return Promise.resolve(entities)
  }

  find(condition: FilterExpression<TEntity>): Promise<TEntity[]> {
    const entities = this.filterEntities(condition)
    return Promise.resolve(entities)
  }

  findOne(condition: FilterExpression<TEntity>): Promise<TEntity | undefined> {
    const entities = this.filterEntities(condition)
    return Promise.resolve(entities[0])
  }

  async findOneOrFail(condition: FilterExpression<TEntity>): Promise<TEntity> {
    const entity = await this.findOne(condition)
    if (!entity) {
      throw new Error("Entity not found.")
    }
    return Promise.resolve(entity)
  }

  count(condition: FilterExpression<TEntity>): Promise<number> {
    const entities = this.filterEntities(condition)
    return Promise.resolve(entities.length)
  }

  findById(id: string[]): Promise<TEntity[]> {
    const entities = id.map((id) => this.entities[id]).filter((x) => x)
    return Promise.resolve(entities)
  }

  delete(id: string): Promise<void> {
    const entity = this.entities[id]
    if (!entity) {
      throw new Error("Entity not found.")
    }
    delete this.entities[id]
    return Promise.resolve()
  }

  deleteBy(
    condition: FilterExpression<TEntity>
  ): Promise<IEntitiesDeleteResult> {
    const entities = this.filterEntities(condition)
    for (const entity of entities) {
      const key = this.getId(entity)
      delete this.entities[key]
    }
    return Promise.resolve({
      deletedCount: entities.length,
    })
  }

  create(entity: TEntity): Promise<TEntity> {
    const key = this.getId(entity)
    if (this.entities[key]) {
      throw new Error("Entity already exists.")
    }
    this.entities[key] = entity
    return Promise.resolve(entity)
  }

  update(id: string, entity: Partial<TEntity>): Promise<TEntity> {
    if (!this.entities[id]) {
      throw new Error("Entity not found.")
    }
    this.entities[id] = {
      ...this.entities[id],
      ...entity,
    }
    return Promise.resolve(this.entities[id])
  }

  updateBy(
    entity: Partial<TEntity>,
    condition: FilterExpression<TEntity>
  ): Promise<TEntity[]> {
    const filteredEntities = this.filterEntities(condition)
    const updateEntities = filteredEntities.map((e) => ({
      ...e,
      ...entity,
    }))
    for (const entity of updateEntities) {
      const key = this.getId(entity)
      this.entities[key] = entity
    }

    return Promise.resolve(updateEntities)
  }

  upsert(id: string, entity: TEntity): Promise<TEntity> {
    if (typeof id === "undefined") {
      throw new Error("Invalid 'entity' parameter.")
    }

    const existingEntity = this.entities[id]
    if (existingEntity) {
      const updatedEntity = { ...existingEntity, ...entity }
      return this.update(id, updatedEntity)
    } else {
      return this.create({
        ...entity,
        id,
      })
    }
  }

  async upsertBy({
    data,
    filter,
  }: {
    data: Partial<TEntity>
    filter: FilterExpression<TEntity>
  }): Promise<EntityReference<string>> {
    const entity = this.filterEntities(filter)[0]
    if (entity) {
      const id = this.getId(entity)
      await this.update(id, {
        ...entity,
        ...data,
      })
      return { id }
    } else {
      const item = await this.create(data as TEntity)
      return { id: this.getId(item) }
    }
  }

  private getId(entity: Partial<TEntity>): string {
    const entityId = (entity as any).id
    if (entityId === undefined) {
      throw new Error("Entity must have an 'id' property.")
    }
    return entityId.toString()
  }

  private filterEntities(condition: FilterExpression<TEntity>): TEntity[] {
    const entities = Object.values(this.entities)
    return entities.filter(condition)
  }
}
