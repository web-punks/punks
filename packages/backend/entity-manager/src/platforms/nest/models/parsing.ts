import { ApiProperty } from "@nestjs/swagger"

export class EntityParseValidationColumn {
  @ApiProperty()
  name: string

  @ApiProperty()
  key: string
}

export class EntityParseValidationError {
  @ApiProperty()
  errorCode: string

  @ApiProperty()
  column: EntityParseValidationColumn
}

export class EntityParseStatus {
  @ApiProperty()
  isValid: boolean

  @ApiProperty({ type: [EntityParseValidationError] })
  validationErrors: EntityParseValidationError[]
}

export class EntityParseResult {
  @ApiProperty()
  rowIndex: number

  @ApiProperty({ type: Object })
  item: any

  @ApiProperty()
  status: EntityParseStatus
}
