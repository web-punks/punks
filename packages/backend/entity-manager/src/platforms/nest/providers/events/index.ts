import { Injectable } from "@nestjs/common"
import { EventEmitter2 } from "@nestjs/event-emitter"
import { IEventEmitter } from "../../../../abstractions/events"

@Injectable()
export class NestEventEmitter implements IEventEmitter {
  constructor(private eventEmitter: EventEmitter2) {}

  async emit(event: string, ...args: any[]): Promise<void> {
    await this.eventEmitter.emitAsync(event, ...args)
  }
}
