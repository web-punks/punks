import {
  EntityManagerConfigurationError,
  IEmailTemplate,
} from "../../../../abstractions"

export class EmailTemplatesCollection {
  private readonly _collection = new Map<
    string,
    IEmailTemplate<unknown, unknown, unknown>
  >()

  registerTemplate(
    id: string,
    template: IEmailTemplate<unknown, unknown, unknown>
  ) {
    if (this._collection.has(id)) {
      throw new EntityManagerConfigurationError(
        `Email template with id "${id}" already registered`
      )
    }
    this._collection.set(id, template)
  }

  getTemplate<TTemplateData, TPayload, TAugmentedPayload = TPayload>(
    id: string
  ) {
    if (!this._collection.has(id)) {
      throw new EntityManagerConfigurationError(
        `Email template with id "${id}" not found`
      )
    }
    return this._collection.get(id) as IEmailTemplate<
      TTemplateData,
      TPayload,
      TAugmentedPayload
    >
  }
}
