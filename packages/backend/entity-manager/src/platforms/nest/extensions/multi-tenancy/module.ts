import { Module } from "@nestjs/common"
import { Providers } from "../../providers"
import { EntityManagerModule } from "../../module"
import { Services } from "./services/providers"

@Module({
  imports: [EntityManagerModule],
  providers: [...Providers, ...Services],
  exports: [...Services],
})
export class MultiTenancyModule {}
