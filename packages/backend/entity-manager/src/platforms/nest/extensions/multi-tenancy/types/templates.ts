import {
  IAppCompany,
  IAppTenant,
  IAppUser,
  IAppUserGroup,
} from "../abstractions"

export type TenantAdmin<TUser extends IAppUser> = {
  user: Partial<TUser>
  password: string
}

export type TenantTemplate<
  TTenant extends IAppTenant,
  TUser extends IAppUser
> = {
  tenant: Partial<TTenant>
  defaultAdmin?: TenantAdmin<TUser>
}

export type OrganizationAdmin<TUser extends IAppUser> = {
  user: Partial<TUser>
  password: string
}

export type OrganizationTemplate<
  TCompany extends IAppCompany,
  TUser extends IAppUser,
  TUserGroup extends IAppUserGroup
> = {
  tenant: Partial<TCompany["organization"]["tenant"]>
  organization: Partial<TCompany["organization"]>
  company: Partial<TCompany>
  groups: Partial<TUserGroup>[]
  defaultAdmin?: OrganizationAdmin<TUser>
}
