export const MultiTenancyEntities = {
  Company: "appCompany",
  Division: "appDivision",
  Organization: "appOrganization",
  Role: "appRole",
  Tenant: "appTenant",
  User: "appUser",
  UserGroup: "appUserGroup",
  UserRole: "appUserRole",
  Directory: "appDirectory",
}
