import { Injectable } from "@nestjs/common"
import { OrganizationTemplate } from "../../types"
import { IAppCompany, IAppUser, IAppUserGroup } from "../../abstractions"
import { EntityManagerRegistry } from "../../../../ioc"

@Injectable()
export class OrganizationInitializerService {
  constructor(private readonly registry: EntityManagerRegistry) {}

  async execute<
    TCompany extends IAppCompany,
    TUser extends IAppUser,
    TUserGroup extends IAppUserGroup
  >(template: OrganizationTemplate<TCompany, TUser, TUserGroup>) {}
}
