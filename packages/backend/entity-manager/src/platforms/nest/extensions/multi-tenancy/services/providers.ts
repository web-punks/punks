import { OrganizationInitializerService } from "./organization-initializer"
import { TenantInitializerService } from "./tenant-initializer"

export const Services = [
  OrganizationInitializerService,
  TenantInitializerService,
]
