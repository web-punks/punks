export interface IAppDirectory {
  id: string
  uid: string
  name: string
}

export interface IAppTenant {
  id: string
  uid: string
  name: string
}

export interface IAppOrganization {
  id: string
  uid: string
  name: string
  tenant: IAppTenant
}

export interface IAppCompany {
  id: string
  uid: string
  name: string
  organization: IAppOrganization
}

export interface IAppDivision {
  id: string
  uid: string
  name: string
  company: IAppCompany
}

export interface IAppUserProfile {
  firstName: string
  lastName: string
}

export interface IAppUser {
  id: string
  uid?: string
  userName: string
  email: string
  verified: boolean
  disabled: boolean
  profile: IAppUserProfile
  tenant: IAppTenant
  directory: IAppDirectory
  organization?: IAppOrganization
}

export interface IAppUserGroup {
  id: string
  uid: string
  name: string
  disabled: boolean
  isBuiltIn: boolean
  organization?: IAppOrganization
}

export interface IAppRole {
  id: string
  uid: string
  name: string
}
