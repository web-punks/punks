import { Injectable } from "@nestjs/common"
import { TenantTemplate } from "../../types"
import { IAppTenant, IAppUser } from "../../abstractions"
import { EntityManagerRegistry } from "../../../../ioc"

@Injectable()
export class TenantInitializerService {
  constructor(private readonly registry: EntityManagerRegistry) {}

  async execute<TTenant extends IAppTenant, TUser extends IAppUser>(
    template: TenantTemplate<TTenant, TUser>
  ) {}
}
