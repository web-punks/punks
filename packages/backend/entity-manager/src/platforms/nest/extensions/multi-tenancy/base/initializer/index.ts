import { EntityManagerRegistry } from "../../../../ioc"
import { MultiTenancyEntities } from "../../symbols"

export abstract class BaseInitializer {
  constructor(private readonly registry: EntityManagerRegistry) {}

  protected get companiesRepo() {
    return this.registry
      .resolveEntityServicesCollection(MultiTenancyEntities.Company)
      .resolveRepository()
  }
}
