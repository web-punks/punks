import { IAppCompany, IAppOrganization, IAppTenant } from "./models"

export interface IMultiTenantEntity {
  tenant: IAppTenant
}

export interface IMultiOrganizationEntity {
  organization: IAppOrganization
}

export interface IMultiCompanyEntity {
  company: IAppCompany
}
