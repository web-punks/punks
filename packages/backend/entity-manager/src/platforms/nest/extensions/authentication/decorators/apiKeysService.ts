import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { AuthenticationExtensionSymbols } from "./symbols"

export const WpApiKeysService = () =>
  applyDecorators(
    Injectable(),
    SetMetadata(AuthenticationExtensionSymbols.ApiKeysService, true)
  )
