import { JwtService } from "@nestjs/jwt"
import { Injectable } from "@nestjs/common"
import { authSettings } from "../../settings"

@Injectable()
export class JwtProvider {
  constructor(private readonly jwtService: JwtService) {}

  async sign<T extends object>(payload: T) {
    return await this.jwtService.signAsync(JSON.stringify(payload), {
      secret: authSettings.value.jwtSecret,
    })
  }

  async parse<T extends object>(token: string) {
    return await this.jwtService.verifyAsync<T>(token, {
      ignoreExpiration: true,
      secret: authSettings.value.jwtSecret,
    })
  }

  async verify<T extends object>(token: string) {
    return await this.jwtService.verifyAsync<T>(token, {
      ignoreExpiration: false,
      secret: authSettings.value.jwtSecret,
    })
  }
}
