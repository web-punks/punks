import { IAuthUserContext } from "../../abstractions"

export type UserVerifyRequestInput<TUserContext extends IAuthUserContext> = {
  email: string
  callback: UserVerifyRequestCallbackTemplate
  languageCode: string
  context?: TUserContext
  emailTemplateId?: string
}

export type UserVerifyRequestResult = {
  success: boolean
}

export type UserVerifyRequestCallbackTemplate = {
  urlTemplate: string
  tokenPlaceholder: string
}
