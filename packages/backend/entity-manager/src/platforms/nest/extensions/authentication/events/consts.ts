export const AUTHENTICATION_EVENTS_NAMESPACE = "authentication"

export const AuthenticationEvents = {
  UserLogin: `${AUTHENTICATION_EVENTS_NAMESPACE}:user.login`,
  UserRegistrationStarted: `${AUTHENTICATION_EVENTS_NAMESPACE}:user.registrationStarted`,
  UserRegistrationCompleted: `${AUTHENTICATION_EVENTS_NAMESPACE}:user.registrationCompleted`,
  UserPasswordResetStarted: `${AUTHENTICATION_EVENTS_NAMESPACE}:user.passwordResetStarted`,
  UserPasswordResetCompleted: `${AUTHENTICATION_EVENTS_NAMESPACE}:user.passwordResetCompleted`,
}
