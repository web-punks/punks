import { Injectable, NestMiddleware } from "@nestjs/common"
import { Request } from "express"
import { IAuthUser, IAuthUserTokenData } from "../../abstractions"
import { AuthenticationService } from "../../services"
import { EntityManagerRegistry } from "../../../../ioc"

@Injectable()
export class AuthenticationMiddleware implements NestMiddleware {
  constructor(
    private readonly registry: EntityManagerRegistry,
    private readonly authService: AuthenticationService
  ) {}

  async use(req: any, res: any, next: (error?: any) => void) {
    let authData = null

    const apiKey = this.extractApiKeyFromHeader(req)
    if (apiKey) {
      const entry = await this.authService.apiKeysService.find(apiKey)
      if (entry && !entry.disabled) {
        authData = {
          apiKey: entry,
        }
      }
    }

    const token = this.extractTokenFromHeader(req)
    if (token) {
      const parsedToken = await this.authService.userTokenVerify({ token })
      if (parsedToken.isValid) {
        const user = await this.getUserFromToken(parsedToken.data)
        const roles = user ? await this.getUserRoles(user) : []
        const permissions = user ? await this.getUserPermissions(user) : []
        const organizationalUnits = user
          ? await this.getUserOrganizationalUnits(user)
          : []

        authData = {
          ...(authData ?? {}),
          user,
          roles,
          permissions,
          organizationalUnits,
        }
      }
    }

    if (authData) {
      req.auth = await this.postProcessContext(authData)
    }

    next()
  }

  private async postProcessContext(authData: any) {
    let processedAuth = authData
    const authenticationMiddlewares = this.getAuthenticationMiddlewares()
    for (const middleware of authenticationMiddlewares) {
      const additionalProps: any = await middleware.processContext(authData)
      processedAuth = {
        ...authData,
        ...additionalProps,
      }
    }
    return processedAuth
  }

  private getAuthenticationMiddlewares() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveAuthenticationMiddlewares()
      .sort((a, b) => a.getPriority() - b.getPriority())
  }

  private async getUserFromToken(
    parsedToken: IAuthUserTokenData<any>
  ): Promise<IAuthUser> {
    return await this.authService.usersService.getById(parsedToken.userId)
  }

  private async getUserRoles(user: IAuthUser) {
    return await this.authService.userRolesService.getUserRoles(user.id)
  }

  private async getUserPermissions(user: IAuthUser) {
    return await this.authService.userRolesService.getUserPermissions(user.id)
  }

  private async getUserOrganizationalUnits(user: IAuthUser) {
    return await this.authService.userRolesService.getUserOrganizationalUnits(
      user.id
    )
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers.authorization?.split(" ") ?? []
    return type === "Bearer" ? token : undefined
  }

  private extractApiKeyFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers.authorization?.split(" ") ?? []
    return type === "ApiKey" ? token : undefined
  }
}
