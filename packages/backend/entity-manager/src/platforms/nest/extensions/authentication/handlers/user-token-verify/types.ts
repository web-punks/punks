import { IAuthUserContext, IAuthUserTokenData } from "../../abstractions"

export type UserTokenVerifyResult<TUserContext extends IAuthUserContext> = {
  isValid: boolean
  data: IAuthUserTokenData<TUserContext>
}

export type UserTokenVerifyInput = {
  token: string
}
