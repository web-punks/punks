import { Injectable } from "@nestjs/common"
import { PasswordHashingProvider } from "../../providers/password-hasher"
import { AuthenticationServicesResolver } from "../../resolver"
import { JwtProvider } from "../../providers/jwt"
import { extractUserTokenData } from "../../converters"
import { IAuthUser, IAuthUserContext } from "../../abstractions"
import { UserLoginInput, UserLoginResult } from "./types"

@Injectable()
export class UserLoginHandler {
  constructor(
    private readonly services: AuthenticationServicesResolver,
    private readonly passwordHashingProvider: PasswordHashingProvider,
    private readonly jwtProvider: JwtProvider
  ) {}

  async execute<TUserContext extends IAuthUserContext>(
    input: UserLoginInput<TUserContext>
  ): Promise<UserLoginResult> {
    const user = await this.resolveUser(input.userName, input.context)
    if (!user) {
      return {
        success: false,
      }
    }

    const newPasswordHash = await this.passwordHashingProvider.verifyPassword({
      password: input.password,
      hash: user.passwordHash,
      userId: user.id,
    })

    if (!newPasswordHash.isMatching) {
      return {
        success: false,
      }
    }

    return {
      success: true,
      token: await this.generateUserJwtToken(user, input.context),
      userId: user.id,
    }
  }

  private async generateUserJwtToken<TUserContext extends IAuthUserContext>(
    user: IAuthUser,
    context?: TUserContext
  ) {
    return await this.jwtProvider.sign(extractUserTokenData(user, context))
  }

  private async resolveUser<TUserContext extends IAuthUserContext>(
    userName: string,
    context?: TUserContext
  ) {
    return (
      (await this.services
        .getUsersService()
        .getByUserName(userName, context)) ??
      (await this.services.getUsersService().getByEmail(userName, context))
    )
  }
}
