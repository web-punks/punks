import { IAuthUserContext } from "../../abstractions"

export enum UserCreationError {
  UserAlreadyExists = "userAlreadyExists",
}

export type UserCreationInput<
  TUserRegistrationInfo,
  TUserContext extends IAuthUserContext
> = {
  email: string
  userName: string
  password: string
  registrationInfo: TUserRegistrationInfo
  context?: TUserContext
  verified?: boolean
}

export type UserCreationResult = {
  success: boolean
  error?: UserCreationError
  userId?: string
}
