export type UserProfile = {
  firstName: string
  lastName: string
}

export type UserLoginEventPayload = {
  userId: string
  email: string
  profile: UserProfile
}

export type UserRegistrationStartedEventPayload = {
  email: string
  profile: UserProfile
}

export type UserRegistrationCompletedEventPayload = {
  userId: string
  email: string
  profile: UserProfile
}

export type UserPasswordResetStartedEventPayload = {
  email: string
}

export type UserPasswordResetCompletedEventPayload = {
  userId: string
  email: string
  profile: UserProfile
}
