import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { AuthenticationExtensionSymbols } from "./symbols"

export const WpRolesService = () =>
  applyDecorators(
    Injectable(),
    SetMetadata(AuthenticationExtensionSymbols.RolesService, true)
  )
