import { Injectable } from "@nestjs/common"
import { AppHashingService } from "../../../../services/hashing"
import { authSettings } from "../../settings"

@Injectable()
export class PasswordHashingProvider {
  constructor(private readonly hashing: AppHashingService) {}

  async hashPassword(input: { password: string; userId: string }) {
    return await this.hashing.hash(
      input.password,
      this.buildUserSalt({
        salt: authSettings.value.passwordSalt,
        userId: input.userId,
      })
    )
  }

  async verifyPassword(input: {
    password: string
    userId: string
    hash: string
  }) {
    const padHash = await this.hashPassword({
      password: input.password,
      userId: input.userId,
    })
    return {
      isMatching: padHash === input.hash,
    }
  }

  private buildUserSalt = (input: { salt: string; userId: string }) => {
    return `${input.salt}`
  }
}
