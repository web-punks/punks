import { Injectable } from "@nestjs/common"
import {
  IAuthOrganizationalUnit,
  IAuthPermission,
  IAuthRole,
  IAuthRoleService,
  IAuthService,
  IAuthUser,
  IAuthUserContext,
  IAuthUserRolesService,
  IAuthUserService,
} from "../../abstractions"
import { UserRegistrationHandler } from "../../handlers/user-register"
import { UserDisableHandler } from "../../handlers/user-disable"
import { UserEnableHandler } from "../../handlers/user-enable"
import { UserLoginHandler } from "../../handlers/user-login"
import { UserPasswordChangeHandler } from "../../handlers/user-password-change"
import { UserPasswordResetCompleteHandler } from "../../handlers/user-password-reset-complete"
import { UserPasswordResetRequestHandler } from "../../handlers/user-password-reset-request"
import { UserVerifyCompleteHandler } from "../../handlers/user-verify-complete"
import { UserTokenVerifyHandler } from "../../handlers/user-token-verify"
import { UserVerifyRequestHandler } from "../../handlers/user-verify-request"
import { UserDisableInput } from "../../handlers/user-disable/types"
import { UserEnableInput } from "../../handlers/user-enable/types"
import {
  UserLoginInput,
  UserLoginResult,
} from "../../handlers/user-login/types"
import { UserPasswordChangeInput } from "../../handlers/user-password-change/types"
import { UserPasswordResetCompleteInput } from "../../handlers/user-password-reset-complete/types"
import {
  UserPasswordResetRequestInput,
  UserPasswordResetRequestResult,
} from "../../handlers/user-password-reset-request/types"
import {
  UserRegistrationInput,
  UserRegistrationResult,
} from "../../handlers/user-register/types"
import {
  UserTokenVerifyInput,
  UserTokenVerifyResult,
} from "../../handlers/user-token-verify/types"
import { UserVerifyCompleteInput } from "../../handlers/user-verify-complete/types"
import {
  UserVerifyRequestInput,
  UserVerifyRequestResult,
} from "../../handlers/user-verify-request/types"
import { AuthenticationServicesResolver } from "../../resolver"
import { UserCreationHandler } from "../../handlers/user-create"
import { UserCreationInput, UserDeleteInput } from "../../handlers"
import { UserDeleteHandler } from "../../handlers/user-delete"
import { UserImpersonateHandler } from "../../handlers/user-impersonate"
import {
  UserImpersonateInput,
  UserImpersonateResult,
} from "../../handlers/user-impersonate/types"
import { UserInvitationSendHandler } from "../../handlers/user-invitation-send"
import { UserInvitationSendInput } from "../../handlers/user-invitation-send/types"

@Injectable()
export class AuthenticationService implements IAuthService {
  constructor(
    private readonly userCreationHandler: UserCreationHandler,
    private readonly userDisableHandler: UserDisableHandler,
    private readonly userDeleteHandler: UserDeleteHandler,
    private readonly userEnableHandler: UserEnableHandler,
    private readonly userImpersonateHandler: UserImpersonateHandler,
    private readonly userLoginHandler: UserLoginHandler,
    private readonly userPasswordChangeHandler: UserPasswordChangeHandler,
    private readonly userPasswordResetFinalizeHandler: UserPasswordResetCompleteHandler,
    private readonly userPasswordResetRequestHandler: UserPasswordResetRequestHandler,
    private readonly userRegistrationHandler: UserRegistrationHandler,
    private readonly userTokenVerifyHandler: UserTokenVerifyHandler,
    private readonly userVerifyRequestHandler: UserVerifyRequestHandler,
    private readonly userVerifyCompleteHandler: UserVerifyCompleteHandler,
    private readonly userInvitationHandler: UserInvitationSendHandler,
    private readonly resolver: AuthenticationServicesResolver
  ) {}

  async userCreate<
    TUserRegistrationInfo,
    TUserContext extends IAuthUserContext
  >(input: UserCreationInput<TUserRegistrationInfo, TUserContext>) {
    return await this.userCreationHandler.execute(input)
  }

  async userInvite(input: UserInvitationSendInput) {
    return await this.userInvitationHandler.execute(input)
  }

  async userDisable(input: UserDisableInput) {
    await this.userDisableHandler.execute(input)
  }

  async userDelete(input: UserDeleteInput) {
    await this.userDeleteHandler.execute(input)
  }

  async userEnable(input: UserEnableInput) {
    await this.userEnableHandler.execute(input)
  }

  async userImpersonate<TUserContext extends IAuthUserContext>(
    input: UserImpersonateInput<TUserContext>
  ): Promise<UserImpersonateResult> {
    return await this.userImpersonateHandler.execute(input)
  }

  async userLogin<TUserContext extends IAuthUserContext>(
    input: UserLoginInput<TUserContext>
  ): Promise<UserLoginResult> {
    return await this.userLoginHandler.execute(input)
  }

  async userPasswordChange(input: UserPasswordChangeInput) {
    await this.userPasswordChangeHandler.execute(input)
  }

  async userPasswordResetFinalize<TUserContext extends IAuthUserContext>(
    input: UserPasswordResetCompleteInput<TUserContext>
  ) {
    return await this.userPasswordResetFinalizeHandler.execute(input)
  }

  async userPasswordResetRequest<TUserContext extends IAuthUserContext>(
    input: UserPasswordResetRequestInput<TUserContext>
  ): Promise<UserPasswordResetRequestResult> {
    return await this.userPasswordResetRequestHandler.execute(input)
  }

  async userRegister<
    TUserRegistrationInfo,
    TUserContext extends IAuthUserContext
  >(
    input: UserRegistrationInput<TUserRegistrationInfo, TUserContext>
  ): Promise<UserRegistrationResult> {
    return await this.userRegistrationHandler.execute(input)
  }

  async userVerifyRequest<TUserContext extends IAuthUserContext>(
    input: UserVerifyRequestInput<TUserContext>
  ): Promise<UserVerifyRequestResult> {
    return await this.userVerifyRequestHandler.execute(input)
  }

  async userVerifyComplete<TUserContext extends IAuthUserContext>(
    input: UserVerifyCompleteInput<TUserContext>
  ) {
    return await this.userVerifyCompleteHandler.execute(input)
  }

  async userTokenVerify<TUserContext extends IAuthUserContext>(
    input: UserTokenVerifyInput
  ): Promise<UserTokenVerifyResult<TUserContext>> {
    return await this.userTokenVerifyHandler.execute<TUserContext>(input)
  }

  get apiKeysService() {
    return this.resolver.getApiKeysService()
  }

  get usersService() {
    return this.resolver.getUsersService() as IAuthUserService<
      IAuthUser,
      any,
      unknown
    >
  }

  get rolesService() {
    return this.resolver.getRoleService() as IAuthRoleService<
      IAuthRole,
      IAuthPermission
    >
  }

  get userRolesService() {
    return this.resolver.getUserRoleService() as IAuthUserRolesService<
      IAuthUser,
      IAuthRole,
      IAuthPermission,
      IAuthOrganizationalUnit
    >
  }
}
