import { Injectable } from "@nestjs/common"
import {
  IAuthApiKeysService,
  IAuthOrganizationalUnit,
  IAuthPermission,
  IAuthPermissionService,
  IAuthRole,
  IAuthRoleService,
  IAuthUser,
  IAuthUserRolesService,
  IAuthUserService,
} from "../abstractions"

@Injectable()
export class AuthenticationServicesResolver {
  private usersService: IAuthUserService<any, any, any>
  private rolesService: IAuthRoleService<any, any>
  private permissionsService: IAuthPermissionService<any>
  private usersRolesService: IAuthUserRolesService<any, any, any, any>
  private apiKeysService: IAuthApiKeysService<any>

  registerApiKeysService(service: IAuthApiKeysService<any>) {
    this.apiKeysService = service
  }

  registerUsersService(usersService: IAuthUserService<any, any, any>) {
    this.usersService = usersService
  }

  registerRoleService(rolesService: IAuthRoleService<any, any>) {
    this.rolesService = rolesService
  }

  registerPermissionsService(permissionsService: IAuthPermissionService<any>) {
    this.permissionsService = permissionsService
  }

  registerUserRolesService(
    userRolesService: IAuthUserRolesService<any, any, any, any>
  ) {
    this.usersRolesService = userRolesService
  }

  getApiKeysService() {
    if (!this.apiKeysService) {
      throw new Error("Api keys service is not registered")
    }
    return this.apiKeysService as IAuthApiKeysService<any>
  }

  getUsersService<TUser extends IAuthUser, TUserRegistrationInfo>() {
    if (!this.usersService) {
      throw new Error("Users service is not registered")
    }
    return this.usersService as IAuthUserService<
      TUser,
      any,
      TUserRegistrationInfo
    >
  }

  getRoleService<
    TRole extends IAuthRole,
    TPermission extends IAuthPermission
  >() {
    if (!this.rolesService) {
      throw new Error("Roles service is not registered")
    }
    return this.rolesService as IAuthRoleService<TRole, TPermission>
  }

  getPermissionService<TPermission extends IAuthPermission>() {
    if (!this.permissionsService) {
      throw new Error("Permissions service is not registered")
    }
    return this.permissionsService as IAuthPermissionService<TPermission>
  }

  getUserRoleService<
    TUser extends IAuthUser,
    TRole extends IAuthRole,
    TPermission extends IAuthPermission,
    TAuthOrganizationalUnit extends IAuthOrganizationalUnit
  >() {
    if (!this.usersRolesService) {
      throw new Error("User roles service is not registered")
    }
    return this.usersRolesService as IAuthUserRolesService<
      TUser,
      TRole,
      TPermission,
      TAuthOrganizationalUnit
    >
  }
}
