import { IAuthUserContext } from "../../abstractions"

export type UserLoginInput<TUserContext extends IAuthUserContext> = {
  userName: string
  password: string
  context?: TUserContext
}

export type UserLoginResult = {
  token?: string
  userId?: string
  success: boolean
}
