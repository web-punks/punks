export class AuthenticationError extends Error {
  constructor(message: string) {
    super(message)
    this.name = "AuthenticationError"
  }
}

export class InvalidCredentialsError extends AuthenticationError {
  constructor(message: string) {
    super(message)
    this.name = "InvalidCredentialsError"
  }
}

export class OperationTokenMismatchError extends AuthenticationError {
  constructor(message: string) {
    super(message)
    this.name = "OperationTokenMismatchError"
  }
}
