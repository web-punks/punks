import { Injectable } from "@nestjs/common"
import { IAuthUserContext } from "../../abstractions"
import { AuthenticationServicesResolver } from "../../resolver"
import { PasswordHashingProvider } from "../../providers/password-hasher"
import {
  UserCreationError,
  UserCreationInput,
  UserCreationResult,
} from "./types"
import { Log } from "@punks/backend-core"

@Injectable()
export class UserCreationHandler {
  private readonly logger = Log.getLogger("UserCreation")

  constructor(
    private readonly services: AuthenticationServicesResolver,
    private readonly passwordHashingProvider: PasswordHashingProvider
  ) {}

  async execute<TUserRegistrationInfo, TContext extends IAuthUserContext>(
    input: UserCreationInput<TUserRegistrationInfo, TContext>
  ): Promise<UserCreationResult> {
    const user =
      (await this.resolveUser(input.email, input.context)) ??
      (await this.resolveUser(input.userName, input.context))
    if (user && user.verified) {
      this.logger.debug(
        `User already exists: ${input.email} - ${input.userName}`,
        { user }
      )
      return {
        success: false,
        error: UserCreationError.UserAlreadyExists,
      }
    }

    if (user && !user.verified) {
      const passwordHash = await this.createPasswordHash(
        input.password,
        user.id
      )
      await this.services.getUsersService().update(user.id, {
        passwordHash,
        passwordUpdateTimestamp: new Date(),
        verified: input.verified ?? true,
      })
      this.logger.debug(
        `User already exists but not verified. Updating password and marking as verified: ${input.email} - ${input.userName}`,
        { user }
      )
      return {
        success: true,
        userId: user.id,
      }
    }

    const newUser = await this.createUser(
      input.email,
      input.userName,
      input.registrationInfo,
      input.context
    )
    const passwordHash = await this.createPasswordHash(
      input.password,
      newUser.id
    )
    await this.services.getUsersService().update(newUser.id, {
      passwordHash,
      passwordUpdateTimestamp: new Date(),
      verified: input.verified ?? true,
    })

    this.logger.debug(`New user created: ${input.email} - ${input.userName}`, {
      user: newUser,
    })
    return {
      success: true,
      userId: newUser.id,
    }
  }

  private async createPasswordHash(password: string, userId: string) {
    return await this.passwordHashingProvider.hashPassword({
      password,
      userId,
    })
  }

  private async createUser<
    TUserRegistrationInfo,
    TContext extends IAuthUserContext
  >(
    email: string,
    userName: string,
    info: TUserRegistrationInfo,
    context?: TContext
  ) {
    return await this.services
      .getUsersService()
      .create(email, userName, info, context)
  }

  private async resolveUser<TUserContext extends IAuthUserContext>(
    userName: string,
    context?: TUserContext
  ) {
    return (
      (await this.services
        .getUsersService()
        .getByUserName(userName, context)) ??
      (await this.services.getUsersService().getByEmail(userName, context))
    )
  }
}
