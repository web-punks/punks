import { Injectable } from "@nestjs/common"
import { AuthenticationServicesResolver } from "../../resolver"
import { JwtProvider } from "../../providers/jwt"
import { UserPasswordResetTokenPayload } from "../../types"
import { IAuthUser, IAuthUserContext } from "../../abstractions"
import { OperationTokenMismatchError } from "../../errors"
import { PasswordHashingProvider } from "../../providers/password-hasher"
import { UserPasswordResetCompleteInput } from "./types"
import { extractUserTokenData } from "../../converters"

@Injectable()
export class UserPasswordResetCompleteHandler {
  constructor(
    private readonly services: AuthenticationServicesResolver,
    private readonly passwordHashingProvider: PasswordHashingProvider,
    private readonly jwtProvider: JwtProvider
  ) {}

  async execute<TUserContext extends IAuthUserContext>(
    input: UserPasswordResetCompleteInput<TUserContext>
  ) {
    const tokenPayload = await this.decodeToken(input.token)

    const user = await this.getUser(tokenPayload)

    this.validateUser(user, tokenPayload)

    const newPasswordHash = await this.passwordHashingProvider.hashPassword({
      password: input.newPassword,
      userId: user.id,
    })

    await this.services.getUsersService().update(user.id, {
      passwordHash: newPasswordHash,
      passwordUpdateTimestamp: new Date(),
      temporaryPassword: input.temporary ?? false,
      ...(!user.verified
        ? {
            verified: true,
            verifiedTimestamp: new Date(),
          }
        : {}),
    })

    const token = await this.generateUserJwtToken(user, input.context)

    return {
      userId: user.id,
      token,
    }
  }

  private async generateUserJwtToken<TUserContext extends IAuthUserContext>(
    user: IAuthUser,
    context?: TUserContext
  ) {
    return await this.jwtProvider.sign(extractUserTokenData(user, context))
  }

  private async decodeToken(token: string) {
    return await this.jwtProvider.verify<UserPasswordResetTokenPayload>(token)
  }

  private validateUser(
    user: IAuthUser,
    tokenPayload: UserPasswordResetTokenPayload
  ) {
    if (user.email !== tokenPayload.email) {
      throw new OperationTokenMismatchError(
        "Verify token user email not matching"
      )
    }
  }

  private async getUser(tokenPayload: UserPasswordResetTokenPayload) {
    const user = await this.services
      .getUsersService()
      .getById(tokenPayload.userId)
    if (!user) {
      throw new OperationTokenMismatchError("Verify token user not found")
    }
    return user
  }
}
