import { JwtProvider } from "./jwt"
import { PasswordHashingProvider } from "./password-hasher"

export const AuthenticationProviders = [JwtProvider, PasswordHashingProvider]
