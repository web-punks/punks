import { Injectable } from "@nestjs/common"
import { AuthenticationServicesResolver } from "../../resolver"
import { UserDisableInput } from "./types"

@Injectable()
export class UserDisableHandler {
  constructor(private readonly services: AuthenticationServicesResolver) {}

  async execute(input: UserDisableInput) {
    await this.services.getUsersService().update(input.userId, {
      disabled: true,
    })
  }
}
