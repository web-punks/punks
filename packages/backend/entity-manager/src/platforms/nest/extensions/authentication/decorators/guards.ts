import { SetMetadata } from "@nestjs/common"
import { AuthenticationGuardsSymbols } from "./symbols"
import { AppPermission, AppRole, RolesGuardOptions } from "../types"
import { ApiKeyRouteOptions } from "./types"

export const Public = () =>
  SetMetadata(AuthenticationGuardsSymbols.Public, true)

export const Authenticated = () =>
  SetMetadata(AuthenticationGuardsSymbols.Authenticated, true)

export const Permissions = (...permissions: string[]) =>
  SetMetadata(AuthenticationGuardsSymbols.Permissions, permissions)

export const Roles = (...roles: string[]) =>
  SetMetadata(AuthenticationGuardsSymbols.Roles, roles)

export const MemberOf = (...groups: string[]) =>
  SetMetadata(AuthenticationGuardsSymbols.MemberOf, groups)

export const ApiKeyAccess = (options?: ApiKeyRouteOptions) =>
  SetMetadata(AuthenticationGuardsSymbols.ApiKey, options ?? {})

export const buildRolesGuard = (
  {
    mainRole,
    secondaryRoles,
  }: {
    mainRole: AppRole
    secondaryRoles?: AppRole[]
  },
  options?: RolesGuardOptions
) =>
  Roles(
    mainRole.uid,
    ...(!options?.exact && secondaryRoles
      ? secondaryRoles.map((role) => role.uid)
      : [])
  )

export const buildPermissionsGuard = ({
  permissions,
}: {
  permissions: AppPermission[]
}) => Permissions(...permissions.map((x) => x.uid))
