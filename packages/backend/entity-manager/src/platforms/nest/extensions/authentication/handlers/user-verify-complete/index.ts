import { Injectable } from "@nestjs/common"
import { AuthenticationServicesResolver } from "../../resolver"
import { JwtProvider } from "../../providers/jwt"
import { EmailVerifyTokenPayload } from "../../types"
import { OperationTokenMismatchError } from "../../errors"
import { IAuthUser, IAuthUserContext } from "../../abstractions"
import { UserVerifyCompleteInput } from "./types"
import { extractUserTokenData } from "../../converters"

@Injectable()
export class UserVerifyCompleteHandler {
  constructor(
    private readonly services: AuthenticationServicesResolver,
    private readonly jwtProvider: JwtProvider
  ) {}

  async execute<TUserContext extends IAuthUserContext>(
    input: UserVerifyCompleteInput<TUserContext>
  ) {
    const tokenPayload = await this.decodeToken(input.token)

    const user = await this.getUser(tokenPayload)

    this.validateUser(user, tokenPayload)

    await this.services.getUsersService().update(user.id, {
      verified: true,
      verifiedTimestamp: new Date(),
    })

    const token = await this.generateUserJwtToken(user, input.context)

    return {
      userId: user.id,
      token,
    }
  }

  private async generateUserJwtToken<TUserContext extends IAuthUserContext>(
    user: IAuthUser,
    context?: TUserContext
  ) {
    return await this.jwtProvider.sign(extractUserTokenData(user, context))
  }

  private async decodeToken(token: string) {
    return await this.jwtProvider.verify<EmailVerifyTokenPayload>(token)
  }

  private validateUser(user: IAuthUser, tokenPayload: EmailVerifyTokenPayload) {
    if (user.email !== tokenPayload.email) {
      throw new OperationTokenMismatchError(
        "Verify token user email not matching"
      )
    }
  }

  private async getUser(tokenPayload: EmailVerifyTokenPayload) {
    const user = await this.services
      .getUsersService()
      .getById(tokenPayload.userId)
    if (!user) {
      throw new OperationTokenMismatchError("Verify token user not found")
    }
    return user
  }
}
