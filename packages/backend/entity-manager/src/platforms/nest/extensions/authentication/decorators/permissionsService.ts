import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { AuthenticationExtensionSymbols } from "./symbols"

export const WpPermissionsService = () =>
  applyDecorators(
    Injectable(),
    SetMetadata(AuthenticationExtensionSymbols.PermissionsService, true)
  )
