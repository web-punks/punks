import { Injectable } from "@nestjs/common"
import { IAuthUser, IAuthUserContext } from "../../abstractions"
import { EmailService } from "../../../../services"
import { AuthenticationServicesResolver } from "../../resolver"
import { JwtProvider } from "../../providers/jwt"
import { EmailVerifyTokenPayload, UserRegistrationError } from "../../types"
import {
  AuthenticationEmailTemplates,
  RegistrationEmailPayload,
} from "../../email"
import { PasswordHashingProvider } from "../../providers/password-hasher"
import {
  UserRegistrationInput,
  UserRegisterCallbackTemplate,
  UserRegistrationResult,
} from "./types"
import { Log } from "@punks/backend-core"

@Injectable()
export class UserRegistrationHandler {
  private readonly logger = Log.getLogger("UserRegistration")

  constructor(
    private readonly services: AuthenticationServicesResolver,
    private readonly passwordHashingProvider: PasswordHashingProvider,
    private readonly emailService: EmailService,
    private readonly jwtProvider: JwtProvider
  ) {}

  async execute<TUserRegistrationInfo, TContext extends IAuthUserContext>(
    input: UserRegistrationInput<TUserRegistrationInfo, TContext>
  ): Promise<UserRegistrationResult> {
    const user =
      (await this.resolveUser(input.email, input.context)) ??
      (await this.resolveUser(input.userName, input.context))
    if (user && user.verified) {
      this.logger.debug(
        `User already exists: ${input.email} - ${input.userName}`,
        { user }
      )
      return {
        success: false,
        error: UserRegistrationError.UserAlreadyExists,
      }
    }

    if (user && !user.verified) {
      const passwordHash = await this.createPasswordHash(
        input.password,
        user.id
      )
      await this.services.getUsersService().update(user.id, {
        passwordHash,
        passwordUpdateTimestamp: new Date(),
      })
      if (input.callback) {
        await this.sendRegistrationEmail(
          user,
          input.callback,
          input.languageCode
        )
      }

      this.logger.debug(
        `User already exists but not verified. Sending new verification email: ${input.email} - ${input.userName}`,
        { user }
      )
      return {
        success: true,
        userId: user.id,
      }
    }

    const newUser = await this.createUser(
      input.email,
      input.userName,
      input.registrationInfo,
      input.context
    )
    const passwordHash = await this.createPasswordHash(
      input.password,
      newUser.id
    )
    await this.services.getUsersService().update(newUser.id, {
      passwordHash,
      passwordUpdateTimestamp: new Date(),
    })
    if (input.callback) {
      await this.sendRegistrationEmail(
        newUser,
        input.callback,
        input.languageCode
      )
    }

    this.logger.debug(`New user created: ${input.email} - ${input.userName}`, {
      user: newUser,
    })
    return {
      success: true,
      userId: newUser.id,
    }
  }

  private async createPasswordHash(password: string, userId: string) {
    return await this.passwordHashingProvider.hashPassword({
      password,
      userId,
    })
  }

  private async sendRegistrationEmail(
    user: IAuthUser,
    callback: UserRegisterCallbackTemplate,
    languageId: string
  ) {
    const token = await this.generateEmailVerifyToken(user)
    await this.emailService.sendTemplatedEmail<RegistrationEmailPayload>({
      to: [user.email],
      templateId: AuthenticationEmailTemplates.Registration,
      languageCode: languageId,
      payload: {
        firstName: user.profile.firstName,
        lastName: user.profile.lastName,
        callbackUrl: callback.urlTemplate.replace(
          callback.tokenPlaceholder,
          token
        ),
      },
    })
  }

  private async generateEmailVerifyToken(user: IAuthUser) {
    return await this.jwtProvider.sign<EmailVerifyTokenPayload>({
      email: user.email,
      userId: user.id,
      timestamp: Date.now(),
    })
  }

  private async createUser<
    TUserRegistrationInfo,
    TContext extends IAuthUserContext
  >(
    email: string,
    userName: string,
    info: TUserRegistrationInfo,
    context?: TContext
  ) {
    return await this.services
      .getUsersService()
      .create(email, userName, info, context)
  }

  private async resolveUser<TUserContext extends IAuthUserContext>(
    userName: string,
    context?: TUserContext
  ) {
    return (
      (await this.services
        .getUsersService()
        .getByUserName(userName, context)) ??
      (await this.services.getUsersService().getByEmail(userName, context))
    )
  }
}
