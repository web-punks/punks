import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common"
import { Reflector } from "@nestjs/core"
import {
  IAuthApiKey,
  IAuthPermission,
  IAuthRole,
  IAuthUser,
} from "../abstractions"
import { AuthenticationGuardsSymbols } from "../decorators/symbols"
import { Log } from "@punks/backend-core"
import { ApiKeyRouteOptions } from "../decorators"

@Injectable()
export class AuthGuard implements CanActivate {
  private readonly logger = Log.getLogger(AuthGuard.name)

  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const isPublic = this.getIsPublic(context)
    if (isPublic) {
      this.logger.debug(
        `Authorized:true -> public route`,
        this.getContextInfo({
          context,
        })
      )
      return true
    }

    const auth = this.getCurrentAuth(context)

    const apiRoute = this.getApiRoute(context)
    if (apiRoute) {
      if (auth?.apiKey && !auth.apiKey.disabled) {
        this.logger.debug(
          `Authorized:true -> api route with valid api key`,
          this.getContextInfo({
            context,
          })
        )
        return true
      }

      if (apiRoute.strict) {
        this.logger.debug(
          `Authorized:false -> api route without api key`,
          this.getContextInfo({
            context,
          })
        )
        return false
      }
    }

    const isForAllAuthenticated = this.getIsForAllAuthenticated(context)
    if (isForAllAuthenticated) {
      const isAuthenticated = !!auth?.user
      this.logger.debug(
        `Authorized:${isAuthenticated} -> authentication guard`,
        {
          ...this.getContextInfo({
            context,
            roles: auth?.roles,
            user: auth?.user,
            permissions: auth?.permissions,
          }),
        }
      )
      return isAuthenticated
    }

    const allowedRoles = this.getAllowedRoles(context)
    if (allowedRoles) {
      const isAllowed = this.isRoleMatching(allowedRoles, auth?.roles ?? [])
      this.logger.debug(`Authorized:${isAllowed} -> authorization role guard`, {
        ...this.getContextInfo({
          context,
          roles: auth?.roles,
          user: auth?.user,
          permissions: auth?.permissions,
        }),
        allowedRoles,
      })
      if (!isAllowed) {
        return false
      }
    }

    const allowedPermissions = this.getAllowedPermissions(context)
    if (allowedPermissions) {
      const isAllowed = this.isPermissionMatching(
        allowedPermissions,
        auth?.permissions ?? []
      )
      this.logger.debug(
        `Authorized:${isAllowed} -> authorization permission guard`,
        {
          ...this.getContextInfo({
            context,
            roles: auth?.roles,
            user: auth?.user,
            permissions: auth?.permissions,
          }),
          allowedPermissions,
        }
      )
      if (!isAllowed) {
        return false
      }
    }

    const isAuthenticated = !!auth?.user
    this.logger.debug(`Authorized:${isAuthenticated} -> auth guard`, {
      ...this.getContextInfo({
        context,
        roles: auth?.roles,
        user: auth?.user,
        permissions: auth?.permissions,
      }),
    })
    return isAuthenticated
  }

  private isRoleMatching(allowedRoles: string[], userRoles: IAuthRole[]) {
    return userRoles.some((role) => allowedRoles.includes(role.uid))
  }

  private isPermissionMatching(
    allowedPermissions: string[],
    userPermissions: IAuthRole[]
  ) {
    return userPermissions.some((permission) =>
      allowedPermissions.includes(permission.uid)
    )
  }

  private getIsForAllAuthenticated(context: ExecutionContext) {
    return this.getMetadata<boolean>(
      AuthenticationGuardsSymbols.Authenticated,
      context
    )
  }

  private getIsPublic(context: ExecutionContext) {
    return this.getMetadata<boolean>(
      AuthenticationGuardsSymbols.Public,
      context
    )
  }

  private getApiRoute(context: ExecutionContext) {
    return this.getMetadata<ApiKeyRouteOptions>(
      AuthenticationGuardsSymbols.ApiKey,
      context
    )
  }

  private getAllowedRoles(context: ExecutionContext) {
    return this.getMetadata<string[]>(
      AuthenticationGuardsSymbols.Roles,
      context
    )
  }

  private getAllowedPermissions(context: ExecutionContext) {
    return this.getMetadata<string[]>(
      AuthenticationGuardsSymbols.Permissions,
      context
    )
  }

  private getCurrentAuth(context: ExecutionContext) {
    const request = context.switchToHttp()?.getRequest()
    return request?.auth?.user || request.auth?.apiKey
      ? {
          apiKey: request.auth.apiKey as IAuthApiKey | undefined,
          user: request.auth.user as IAuthUser | undefined,
          roles: request.auth.roles as IAuthRole[] | undefined,
          permissions: request.auth.permissions as
            | IAuthPermission[]
            | undefined,
        }
      : undefined
  }

  private getMetadata<T>(symbol: string, context: ExecutionContext) {
    return this.reflector.getAllAndOverride<T>(symbol, [
      context.getHandler(),
      context.getClass(),
    ])
  }

  private getContextInfo({
    context,
    apiKey,
    user,
    roles,
    permissions,
  }: {
    context: ExecutionContext
    apiKey?: IAuthApiKey
    user?: IAuthUser
    roles?: IAuthRole[]
    permissions?: IAuthPermission[]
  }) {
    return {
      request: {
        path: context.switchToHttp()?.getRequest()?.path,
        method: context.switchToHttp()?.getRequest()?.method,
      },
      ...(apiKey
        ? {
            apiKey: {
              key: `${apiKey.key.substring(0, 7)}*****${apiKey.key.slice(-2)}`,
              name: apiKey.name,
              organizationId: apiKey.organization?.id,
            },
          }
        : {}),
      ...(user
        ? {
            user: {
              id: user.id,
              userName: user.userName,
              email: user.email,
              roles: roles?.map((role) => role.uid),
              permissions: permissions?.map((permission) => permission.uid),
            },
          }
        : {}),
    }
  }
}
