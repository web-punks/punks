import { INestApplicationContext, Logger } from "@nestjs/common"
import { IAppInitializer } from "../../../../../abstractions/app"
import { CustomDiscoveryService } from "../../../ioc/discovery"
import { AuthenticationExtensionSymbols } from "../decorators"
import { AuthenticationServicesResolver } from "../resolver"
import {
  IAuthApiKeysService,
  IAuthPermissionService,
  IAuthRoleService,
  IAuthUserRolesService,
  IAuthUserService,
} from "../abstractions"
import { WpAppInitializer } from "../../../decorators"

@WpAppInitializer()
export class AuthenticationInitializer implements IAppInitializer {
  private readonly logger = new Logger(AuthenticationInitializer.name)

  constructor(
    private readonly discover: CustomDiscoveryService,
    private readonly registry: AuthenticationServicesResolver
  ) {}

  async initialize(app: INestApplicationContext): Promise<void> {
    await this.registerRolesService()
    await this.registerPermissionsService()
    await this.registerUsersService()
    await this.registerUserRolesService()
    await this.registerApiKeysService()

    this.logger.log("Authentication initialized 🔑")
  }

  private async registerRolesService() {
    const service = await this.discoverRolesService()
    if (!service) {
      throw new Error("No roles service found")
    }

    this.registry.registerRoleService(
      service.discoveredClass.instance as IAuthRoleService<any, any>
    )
    this.logger.log(
      `Authentication roles service ${service.discoveredClass.name} registered`
    )
  }

  private async registerPermissionsService() {
    const service = await this.discoverPermissionsService()
    if (!service) {
      throw new Error("No roles service found")
    }

    this.registry.registerPermissionsService(
      service.discoveredClass.instance as IAuthPermissionService<any>
    )
    this.logger.log(
      `Authentication permissions service ${service.discoveredClass.name} registered`
    )
  }

  private async registerUsersService() {
    const service = await this.discoverUserService()
    if (!service) {
      throw new Error("No user service found")
    }

    this.registry.registerUsersService(
      service.discoveredClass.instance as IAuthUserService<any, any, any>
    )
    this.logger.log(
      `Authentication users service ${service.discoveredClass.name} registered`
    )
  }

  private async registerUserRolesService() {
    const service = await this.discoverUserRolesService()
    if (!service) {
      throw new Error("No user roles service found")
    }

    this.registry.registerUserRolesService(
      service.discoveredClass.instance as IAuthUserRolesService<
        any,
        any,
        any,
        any
      >
    )
    this.logger.log(
      `Authentication user roles service ${service.discoveredClass.name} registered`
    )
  }

  private async registerApiKeysService() {
    const service = await this.discoverApiKeysService()
    if (!service) {
      console.warn("No api keys service found")
      return
    }

    this.registry.registerApiKeysService(
      service.discoveredClass.instance as IAuthApiKeysService<any>
    )
    this.logger.log(
      `Authentication api key service ${service.discoveredClass.name} registered`
    )
  }

  private async discoverUserService() {
    return await this.discover.providerWithMetaAtKey<unknown>(
      AuthenticationExtensionSymbols.UserService
    )
  }

  private async discoverRolesService() {
    return await this.discover.providerWithMetaAtKey<unknown>(
      AuthenticationExtensionSymbols.RolesService
    )
  }

  private async discoverPermissionsService() {
    return await this.discover.providerWithMetaAtKey<unknown>(
      AuthenticationExtensionSymbols.PermissionsService
    )
  }

  private async discoverUserRolesService() {
    return await this.discover.providerWithMetaAtKey<unknown>(
      AuthenticationExtensionSymbols.UserRolesService
    )
  }

  private async discoverApiKeysService() {
    return await this.discover.providerWithMetaAtKey<unknown>(
      AuthenticationExtensionSymbols.ApiKeysService
    )
  }
}
