import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { AuthenticationExtensionSymbols } from "./symbols"

export const WpUserService = () =>
  applyDecorators(
    Injectable(),
    SetMetadata(AuthenticationExtensionSymbols.UserService, true)
  )
