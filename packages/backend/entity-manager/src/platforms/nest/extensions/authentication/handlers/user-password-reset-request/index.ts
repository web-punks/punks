import { Injectable } from "@nestjs/common"
import { EmailService } from "../../../../services"
import {
  AuthenticationEmailTemplates,
  PasswordResetEmailPayload,
} from "../../email"
import { IAuthUser, IAuthUserContext } from "../../abstractions"
import { AuthenticationServicesResolver } from "../../resolver"
import { JwtProvider } from "../../providers/jwt"
import { UserPasswordResetTokenPayload } from "../../types"
import {
  UserPasswordResetRequestCallbackTemplate,
  UserPasswordResetRequestInput,
  UserPasswordResetRequestResult,
} from "./types"

@Injectable()
export class UserPasswordResetRequestHandler {
  constructor(
    private readonly services: AuthenticationServicesResolver,
    private readonly emailService: EmailService,
    private readonly jwtProvider: JwtProvider
  ) {}

  async execute<TUserContext extends IAuthUserContext>(
    input: UserPasswordResetRequestInput<TUserContext>
  ): Promise<UserPasswordResetRequestResult> {
    const user = await this.resolveUser(input.email, input.context)
    if (!user) {
      return {
        success: false,
      }
    }

    await this.sendPasswordResetEmail({
      user,
      callback: input.callback,
      languageCode: input.languageCode,
      emailTemplateId: input.emailTemplateId,
    })
    return {
      success: true,
    }
  }

  private async sendPasswordResetEmail({
    user,
    callback,
    languageCode,
    emailTemplateId,
  }: {
    user: IAuthUser
    callback: UserPasswordResetRequestCallbackTemplate
    languageCode: string
    emailTemplateId?: string
  }) {
    const token = await this.generatePasswordResetToken(user)
    await this.emailService.sendTemplatedEmail<PasswordResetEmailPayload>({
      to: [user.email],
      templateId: emailTemplateId ?? AuthenticationEmailTemplates.PasswordReset,
      languageCode,
      payload: {
        firstName: user.profile.firstName,
        lastName: user.profile.lastName,
        callbackUrl: callback.urlTemplate.replace(
          callback.tokenPlaceholder,
          token
        ),
      },
    })
  }

  private async generatePasswordResetToken(user: IAuthUser) {
    return await this.jwtProvider.sign<UserPasswordResetTokenPayload>({
      email: user.email,
      userId: user.id,
      timestamp: Date.now(),
    })
  }

  private async resolveUser<TUserContext extends IAuthUserContext>(
    userName: string,
    context?: TUserContext
  ) {
    return (
      (await this.services
        .getUsersService()
        .getByUserName(userName, context)) ??
      (await this.services.getUsersService().getByEmail(userName, context))
    )
  }
}
