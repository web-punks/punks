import { IAuthUserContext } from "../../abstractions"
import { UserRegistrationError } from "../../types"

export type UserRegisterCallbackTemplate = {
  urlTemplate: string
  tokenPlaceholder: string
}

export type UserRegistrationInput<
  TUserRegistrationInfo,
  TUserContext extends IAuthUserContext
> = {
  email: string
  userName: string
  password: string
  registrationInfo: TUserRegistrationInfo
  callback?: UserRegisterCallbackTemplate
  languageCode: string
  context?: TUserContext
}

export type UserRegistrationResult = {
  success: boolean
  error?: UserRegistrationError
  userId?: string
}
