import { Injectable } from "@nestjs/common"
import { AuthenticationServicesResolver } from "../../resolver"
import { JwtProvider } from "../../providers/jwt"
import { extractUserTokenData } from "../../converters"
import { IAuthUser, IAuthUserContext } from "../../abstractions"
import { UserImpersonateInput, UserImpersonateResult } from "./types"

@Injectable()
export class UserImpersonateHandler {
  constructor(
    private readonly services: AuthenticationServicesResolver,
    private readonly jwtProvider: JwtProvider
  ) {}

  async execute<TUserContext extends IAuthUserContext>(
    input: UserImpersonateInput<TUserContext>
  ): Promise<UserImpersonateResult> {
    const user = await this.resolveUser(input.userName, input.context)
    if (!user) {
      throw new Error(`User ${input.userName} not found`)
    }

    return {
      token: await this.generateUserJwtToken(user, input.context),
    }
  }

  private async generateUserJwtToken<TUserContext extends IAuthUserContext>(
    user: IAuthUser,
    context?: TUserContext
  ) {
    return await this.jwtProvider.sign(extractUserTokenData(user, context))
  }

  private async resolveUser<TUserContext extends IAuthUserContext>(
    userName: string,
    context?: TUserContext
  ) {
    return (
      (await this.services
        .getUsersService()
        .getByUserName(userName, context)) ??
      (await this.services.getUsersService().getByEmail(userName, context))
    )
  }
}
