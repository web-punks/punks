import { Injectable } from "@nestjs/common"
import { IAuthUser, IAuthUserContext } from "../../abstractions"
import { EmailService } from "../../../../services"
import { JwtProvider } from "../../providers/jwt"
import { AuthenticationServicesResolver } from "../../resolver"
import { EmailVerifyTokenPayload } from "../../types"
import {
  AuthenticationEmailTemplates,
  EmailVerifyEmailPayload,
} from "../../email"
import {
  UserVerifyRequestCallbackTemplate,
  UserVerifyRequestInput,
  UserVerifyRequestResult,
} from "./types"

@Injectable()
export class UserVerifyRequestHandler {
  constructor(
    private readonly services: AuthenticationServicesResolver,
    private readonly emailService: EmailService,
    private readonly jwtProvider: JwtProvider
  ) {}

  async execute<TUserContext extends IAuthUserContext>(
    input: UserVerifyRequestInput<TUserContext>
  ): Promise<UserVerifyRequestResult> {
    const user = await this.resolveUser(input.email, input.context)
    if (!user) {
      return {
        success: false,
      }
    }

    await this.sendEmailVerifyEmail({
      user,
      callback: input.callback,
      languageId: input.languageCode,
      emailTemplateId: input.emailTemplateId,
    })
    return {
      success: true,
    }
  }

  private async sendEmailVerifyEmail({
    user,
    callback,
    languageId,
    emailTemplateId,
  }: {
    user: IAuthUser
    callback: UserVerifyRequestCallbackTemplate
    languageId: string
    emailTemplateId?: string
  }) {
    const token = await this.generateEmailVerifyToken(user)
    await this.emailService.sendTemplatedEmail<EmailVerifyEmailPayload>({
      to: [user.email],
      templateId: emailTemplateId ?? AuthenticationEmailTemplates.EmailVerify,
      payload: {
        firstName: user.profile.firstName,
        lastName: user.profile.lastName,
        callbackUrl: callback.urlTemplate.replace(
          callback.tokenPlaceholder,
          token
        ),
      },
      languageCode: languageId,
    })
  }

  private async generateEmailVerifyToken(user: IAuthUser) {
    return await this.jwtProvider.sign<EmailVerifyTokenPayload>({
      email: user.email,
      userId: user.id,
      timestamp: Date.now(),
    })
  }

  private async resolveUser<TUserContext extends IAuthUserContext>(
    userName: string,
    context?: TUserContext
  ) {
    return (
      (await this.services
        .getUsersService()
        .getByUserName(userName, context)) ??
      (await this.services.getUsersService().getByEmail(userName, context))
    )
  }
}
