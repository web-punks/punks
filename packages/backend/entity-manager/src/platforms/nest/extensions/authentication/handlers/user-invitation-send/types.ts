import { UserRegisterCallbackTemplate } from "../user-register/types"

export type UserInvitationSendInput = {
  userId: string
  callback: UserRegisterCallbackTemplate
} & (
  | { emailTemplateId: string; languageId: string }
  | { emailTemplateId?: undefined; languageId?: string }
)
