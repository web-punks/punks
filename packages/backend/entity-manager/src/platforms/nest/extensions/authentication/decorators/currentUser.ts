import { createParamDecorator, ExecutionContext } from "@nestjs/common"
import {
  IAuthOrganizationalUnit,
  IAuthPermission,
  IAuthRole,
  IAuthUser,
} from "../abstractions"

export type CurrentUserData = {
  user: IAuthUser
  roles: IAuthRole[]
  permissions: IAuthPermission[]
  organizationalUnits: IAuthOrganizationalUnit[]
}

export const CurrentUser = createParamDecorator(
  (data: unknown, context: ExecutionContext): CurrentUserData | undefined => {
    const request = context.switchToHttp().getRequest()
    if (!request.auth) {
      return undefined
    }
    const user = request.auth.user as IAuthUser
    const roles = request.auth.roles as IAuthRole[]
    const permissions = request.auth.permissions as IAuthPermission[]
    const organizationalUnits = request.auth
      .organizationalUnits as IAuthOrganizationalUnit[]

    return {
      user,
      roles,
      permissions,
      organizationalUnits,
    }
  }
)
