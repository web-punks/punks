import { IAuthUserContext } from "../../abstractions"

export type UserVerifyCompleteInput<TUserContext extends IAuthUserContext> = {
  token: string
  context?: TUserContext
}
