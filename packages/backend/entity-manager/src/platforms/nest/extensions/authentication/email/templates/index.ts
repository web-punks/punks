export const AuthenticationEmailTemplates = {
  Registration: "registration",
  PasswordReset: "passwordReset",
  EmailVerify: "emailVerify",
}

export type RegistrationEmailPayload = {
  firstName: string
  lastName: string
  callbackUrl: string
}

export type EmailVerifyEmailPayload = {
  firstName: string
  lastName: string
  callbackUrl: string
}

export type PasswordResetEmailPayload = {
  firstName: string
  lastName: string
  callbackUrl: string
}
