import { Injectable } from "@nestjs/common"
import { JwtProvider } from "../../providers/jwt"
import { IAuthUserContext, IAuthUserTokenData } from "../../abstractions"
import { UserTokenVerifyInput, UserTokenVerifyResult } from "./types"

@Injectable()
export class UserTokenVerifyHandler {
  constructor(private readonly jwtProvider: JwtProvider) {}

  async execute<TUserContext extends IAuthUserContext>(
    input: UserTokenVerifyInput
  ): Promise<UserTokenVerifyResult<TUserContext>> {
    const decodedToken = await this.decodeUserJwtToken<TUserContext>(
      input.token
    )
    return {
      isValid: !!decodedToken,
      data: decodedToken,
    }
  }

  private async decodeUserJwtToken<TUserContext extends IAuthUserContext>(
    token: string
  ) {
    return await this.jwtProvider.verify<IAuthUserTokenData<TUserContext>>(
      token
    )
  }
}
