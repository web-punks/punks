import { IAuthUserContext } from "../../abstractions"

export type UserImpersonateInput<TUserContext extends IAuthUserContext> = {
  userName: string
  context?: TUserContext
}

export type UserImpersonateResult = {
  token: string
}
