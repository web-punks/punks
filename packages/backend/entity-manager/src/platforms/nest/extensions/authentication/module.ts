import {
  DynamicModule,
  MiddlewareConsumer,
  Module,
  NestModule,
} from "@nestjs/common"
import { Services } from "./services/providers"
import { EntityManagerModule } from "../../module"
import { UserHandlers as AuthenticationHandlers } from "./handlers/handlers"
import { AuthenticationProviders } from "./providers"
import { AuthenticationServicesResolver } from "./resolver"
import { AuthenticationModuleSettings } from "./types"
import { AuthenticationInitializer } from "./initializer"
import { JwtModule } from "@nestjs/jwt"
import { authSettings } from "./settings"
import { appSessionMiddleware } from "../../session/middleware"
import { AuthenticationMiddleware } from "./middlewares/authentication"
import { AuthGuard } from "./guards"

const ModuleData = {
  imports: [EntityManagerModule, JwtModule],
  providers: [
    AuthenticationServicesResolver,
    AuthenticationInitializer,
    AuthGuard,
    ...Services,
    ...AuthenticationHandlers,
    ...AuthenticationProviders,
  ],
  exports: [...Services],
}

@Module({
  ...ModuleData,
})
export class AuthenticationModule implements NestModule {
  /**
   * @deprecated use initialize instead
   */
  static forRoot(input: AuthenticationModuleSettings): DynamicModule {
    authSettings.initialize(input)
    return {
      module: AuthenticationModule,
      ...ModuleData,
    }
  }

  static initialize(input: AuthenticationModuleSettings) {
    authSettings.initialize(input)
  }

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(appSessionMiddleware)
      .forRoutes("*")
      .apply(AuthenticationMiddleware)
      .forRoutes("*")
  }
}
