import { Injectable } from "@nestjs/common"
import { AuthenticationServicesResolver } from "../../resolver"
import { IAuthUser } from "../../abstractions"
import { UserRegisterCallbackTemplate } from "../user-register/types"
import {
  AuthenticationEmailTemplates,
  RegistrationEmailPayload,
} from "../../email"
import { EmailService } from "../../../../services"
import { JwtProvider } from "../../providers/jwt"
import { EmailVerifyTokenPayload } from "../../types"
import { UserInvitationSendInput } from "./types"

@Injectable()
export class UserInvitationSendHandler {
  constructor(
    private readonly services: AuthenticationServicesResolver,
    private readonly emailService: EmailService,
    private readonly jwtProvider: JwtProvider
  ) {}

  async execute(input: UserInvitationSendInput) {
    const user = await this.services.getUsersService().getById(input.userId)
    if (!user) {
      throw new Error(`User not found ${input.userId}`)
    }
    const token = await this.generateEmailVerifyToken(user)
    const callbackUrl = input.callback.urlTemplate.replace(
      input.callback.tokenPlaceholder,
      token
    )
    if (input.emailTemplateId) {
      await this.sendRegistrationEmail({
        user,
        callbackUrl,
        languageId: input.languageId,
        emailTemplateId: input.emailTemplateId,
        token,
      })
    }

    return { token, callbackUrl }
  }

  private async sendRegistrationEmail({
    user,
    callbackUrl,
    languageId,
    emailTemplateId,
  }: {
    user: IAuthUser
    callbackUrl: string
    languageId: string
    emailTemplateId?: string
    token: string
  }) {
    await this.emailService.sendTemplatedEmail<RegistrationEmailPayload>({
      to: [user.email],
      templateId: emailTemplateId ?? AuthenticationEmailTemplates.Registration,
      languageCode: languageId,
      payload: {
        firstName: user.profile.firstName,
        lastName: user.profile.lastName,
        callbackUrl,
      },
    })
  }

  private async generateEmailVerifyToken(user: IAuthUser) {
    return await this.jwtProvider.sign<EmailVerifyTokenPayload>({
      email: user.email,
      userId: user.id,
      timestamp: Date.now(),
    })
  }
}
