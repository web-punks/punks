import { Injectable } from "@nestjs/common"
import { AuthenticationServicesResolver } from "../../resolver"
import { UserEnableInput } from "./types"

@Injectable()
export class UserEnableHandler {
  constructor(private readonly services: AuthenticationServicesResolver) {}

  async execute(input: UserEnableInput) {
    await this.services.getUsersService().update(input.userId, {
      disabled: false,
    })
  }
}
