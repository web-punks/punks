import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { AuthenticationExtensionSymbols } from "./symbols"

export const WpUserRolesService = () =>
  applyDecorators(
    Injectable(),
    SetMetadata(AuthenticationExtensionSymbols.UserRolesService, true)
  )
