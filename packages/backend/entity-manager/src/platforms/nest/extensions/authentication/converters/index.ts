import {
  IAuthUser,
  IAuthUserContext,
  IAuthUserTokenData,
} from "../abstractions"

export const extractUserTokenData = <TContext extends IAuthUserContext>(
  user: IAuthUser,
  context?: TContext
): IAuthUserTokenData<TContext> => ({
  email: user.email,
  userId: user.id,
  context,
})
