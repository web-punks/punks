import { UserCreationHandler } from "./user-create"
import { UserDeleteHandler } from "./user-delete"
import { UserDisableHandler } from "./user-disable"
import { UserEnableHandler } from "./user-enable"
import { UserImpersonateHandler } from "./user-impersonate"
import { UserInvitationSendHandler } from "./user-invitation-send"
import { UserLoginHandler } from "./user-login"
import { UserPasswordChangeHandler } from "./user-password-change"
import { UserPasswordResetCompleteHandler } from "./user-password-reset-complete"
import { UserPasswordResetRequestHandler } from "./user-password-reset-request"
import { UserRegistrationHandler } from "./user-register"
import { UserTokenVerifyHandler } from "./user-token-verify"
import { UserVerifyCompleteHandler } from "./user-verify-complete"
import { UserVerifyRequestHandler } from "./user-verify-request"

export const UserHandlers = [
  UserCreationHandler,
  UserDisableHandler,
  UserDeleteHandler,
  UserEnableHandler,
  UserImpersonateHandler,
  UserInvitationSendHandler,
  UserLoginHandler,
  UserPasswordChangeHandler,
  UserPasswordResetCompleteHandler,
  UserPasswordResetRequestHandler,
  UserRegistrationHandler,
  UserTokenVerifyHandler,
  UserVerifyCompleteHandler,
  UserVerifyRequestHandler,
]
