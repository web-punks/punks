import { IAuthUserContext } from "../../abstractions"

export type UserPasswordResetRequestCallbackTemplate = {
  urlTemplate: string
  tokenPlaceholder: string
}

export type UserPasswordResetRequestInput<
  TUserContext extends IAuthUserContext
> = {
  email: string
  callback: UserPasswordResetRequestCallbackTemplate
  languageCode: string
  context?: TUserContext
  emailTemplateId?: string
}

export type UserPasswordResetRequestResult = {
  success: boolean
}
