import { IAuthUserContext } from "../../abstractions"

export type UserPasswordResetCompleteInput<
  TUserContext extends IAuthUserContext
> = {
  token: string
  newPassword: string
  temporary?: boolean
  context?: TUserContext
}
