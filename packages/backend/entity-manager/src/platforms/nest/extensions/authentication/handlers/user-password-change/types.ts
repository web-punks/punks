export type UserPasswordChangeInput = {
  userId: string
  newPassword: string
  temporary: boolean
}
