import { Injectable } from "@nestjs/common"
import { AuthenticationServicesResolver } from "../../resolver"
import { UserDeleteInput } from "./types"

@Injectable()
export class UserDeleteHandler {
  constructor(private readonly services: AuthenticationServicesResolver) {}

  async execute(input: UserDeleteInput) {
    await this.services.getUsersService().delete(input.userId)
  }
}
