export interface AuthenticationModuleSettings {
  passwordSalt: string
  jwtSecret: string
}

export type UserPasswordResetTokenPayload = {
  userId: string
  email: string
  timestamp: number
}

export type EmailVerifyTokenPayload = {
  userId: string
  email: string
  timestamp: number
}

export enum UserRegistrationError {
  UserAlreadyExists = "userAlreadyExists",
}

export type AppRole = {
  uid: string
  name: string
}

export type AppPermission = {
  uid: string
  name: string
}

export type RolesGuardOptions = {
  exact?: boolean
}
