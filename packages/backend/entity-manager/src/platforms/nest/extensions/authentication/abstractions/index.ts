export interface IAuthService {}

export interface IAuthRole {
  id: string
  uid: string
  name: string
}

export interface IAuthPermission {
  id: string
  uid: string
  name: string
}

export interface IAuthOrganizationalUnit {
  id: string
  uid: string
  name: string
}

export interface IAuthUserProfile {
  firstName: string
  lastName: string
}

export interface IAuthTenant {
  id: string
  uid: string
  name: string
}

export interface IAuthOrganization {
  id: string
  uid: string
  name: string
  tenant: IAuthTenant
}

export interface IAuthDirectory {
  id: string
  uid: string
  name: string
}

export interface IAuthApiKey {
  id: string
  key: string
  name: string
  disabled: boolean
  organization?: IAuthOrganization
}

export interface IAuthUser {
  id: string
  passwordHash: string
  passwordUpdateTimestamp?: Date
  temporaryPassword: boolean
  userName: string
  email: string
  verified: boolean
  verifiedTimestamp?: Date
  disabled: boolean
  profile: IAuthUserProfile
  organization?: IAuthOrganization
  directory?: IAuthDirectory
}

export interface IAuthUserContext {
  organizationUid?: string
  directoryUid?: string
}

export interface IAuthUserTokenData<TContext extends IAuthUserContext> {
  userId: string
  email: string
  context?: TContext
}

export interface IAuthUserService<
  TUser extends IAuthUser,
  TUserContext extends IAuthUserContext,
  TUserRegistrationInfo
> {
  getById(id: string): Promise<TUser>
  getByEmail(email: string, context?: TUserContext): Promise<TUser>
  getByUserName(userName: string, context?: TUserContext): Promise<TUser>
  update(id: string, data: Partial<TUser>): Promise<void>
  delete(id: string): Promise<void>
  create(
    email: string,
    userName: string,
    data: TUserRegistrationInfo,
    context?: TUserContext
  ): Promise<TUser>
}

export interface IAuthRoleService<
  TRole extends IAuthRole,
  TPermission extends IAuthPermission
> {
  create(data: Partial<TRole>): Promise<TRole>
  update(id: string, data: Partial<TRole>): Promise<void>
  ensure(uid: string, data: Partial<Omit<TRole, "uid">>): Promise<TRole>
  delete(id: string): Promise<void>
  getById(id: string): Promise<TRole | undefined>
  getByUid(uid: string): Promise<TRole | undefined>
  addPermission(uid: string, permissionUid: string): Promise<void>
  removePermission(uid: string, permissionUid: string): Promise<void>
  getPermissions(uid: string): Promise<TPermission[]>
}

export interface IAuthPermissionService<TPermission extends IAuthPermission> {
  create(data: Partial<TPermission>): Promise<TPermission>
  update(id: string, data: Partial<TPermission>): Promise<void>
  ensure(
    uid: string,
    data: Partial<Omit<TPermission, "uid">>
  ): Promise<TPermission>
  delete(id: string): Promise<void>
  getById(id: string): Promise<TPermission | undefined>
  getByUid(uid: string): Promise<TPermission | undefined>
}

export interface IAuthUserRolesService<
  TUser extends IAuthUser,
  TRole extends IAuthRole,
  TPermission extends IAuthPermission,
  TOrganizationalUnit extends IAuthOrganizationalUnit
> {
  getRoleUsers(roleId: string): Promise<TUser[]>
  getUserRoles(userId: string): Promise<TRole[]>
  getUserPermissions(userId: string): Promise<TPermission[]>
  getUserOrganizationalUnits(userId: string): Promise<TOrganizationalUnit[]>
  addUserToRole(userId: string, roleId: string): Promise<void>
  addUserToRoleByUid(userId: string, roleUid: string): Promise<void>
  removeUserFromRole(userId: string, roleId: string): Promise<void>
  removeUserFromRoleByUid(userId: string, roleUid: string): Promise<void>
  clearUserRoles(userId: string): Promise<void>
  isUserInRole(userId: string, roleId: string): Promise<boolean>
}

export interface IAuthApiKeysService<TAuthApiKey extends IAuthApiKey> {
  find(apiKey: string): Promise<TAuthApiKey | undefined>
  create(data: {
    key: string
    name: string
    organizationId?: string
  }): Promise<TAuthApiKey>
  enable(id: string): Promise<void>
  disable(id: string): Promise<void>
  delete(id: string): Promise<void>
}
