export const AuthenticationExtensionSymbols = {
  ApiKeysService: Symbol.for("WP.EXT:AUTHENTICATION.API_KEYS_SERVICE"),
  UserRolesService: Symbol.for("WP.EXT:AUTHENTICATION.USER_ROLES_SERVICE"),
  UserService: Symbol.for("WP.EXT:AUTHENTICATION.USER_SERVICE"),
  PermissionsService: Symbol.for("WP.EXT:AUTHENTICATION.PERMISSIONS_SERVICE"),
  RolesService: Symbol.for("WP.EXT:AUTHENTICATION.ROLES_SERVICE"),
}

export const AuthenticationGuardsSymbols = {
  ApiKey: "guard:apiKey",
  Authenticated: "guard:authenticated",
  Public: "guard:public",
  Roles: "guard:roles",
  Permissions: "guard:permissions",
  MemberOf: "guard:memberOf",
}
