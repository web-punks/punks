import { AppInMemorySettings } from "../../../../../settings"
import { AuthenticationModuleSettings } from "../types"

export const authSettings =
  new AppInMemorySettings<AuthenticationModuleSettings>("authSettings")
