import { Injectable } from "@nestjs/common"
import { AuthenticationServicesResolver } from "../../resolver"
import { PasswordHashingProvider } from "../../providers/password-hasher"
import { UserPasswordChangeInput } from "./types"

@Injectable()
export class UserPasswordChangeHandler {
  constructor(
    private readonly services: AuthenticationServicesResolver,
    private readonly passwordHashingProvider: PasswordHashingProvider
  ) {}

  async execute(input: UserPasswordChangeInput) {
    const newPasswordHash = await this.passwordHashingProvider.hashPassword({
      password: input.newPassword,
      userId: input.userId,
    })
    await this.services.getUsersService().update(input.userId, {
      passwordHash: newPasswordHash,
      passwordUpdateTimestamp: new Date(),
      temporaryPassword: input.temporary ?? false,
    })
  }
}
