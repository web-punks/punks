import { Injectable, Logger } from "@nestjs/common"
import { TasksRegistry } from "../../providers/registry"
import { TaskShell } from "../../providers/shell"
import { TaskRunType } from "../../../../../../abstractions"

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name)

  constructor(
    private readonly jobShell: TaskShell,
    private readonly registry: TasksRegistry
  ) {}

  async triggerTask(name: string) {
    this.logger.log(`Manual task ${name} -> trigger`)
    const task = this.registry.getTask(name)
    this.jobShell
      .executeTask(task.instance, task.props, TaskRunType.OnDemand)
      .then(() => {
        this.logger.log(`Manual task ${name} -> completed`)
      })
      .catch((e) => {
        this.logger.error(`Manual task ${name} -> error: ${e.message}`)
      })
    this.logger.log(`Manual task ${name} -> triggered`)
  }

  async invokeTask(name: string) {
    this.logger.log(`Manual task ${name} -> invoke`)
    const task = this.registry.getTask(name)
    await this.jobShell.executeTask(
      task.instance,
      task.props,
      TaskRunType.OnDemand
    )
    this.logger.log(`Manual task ${name} -> completed`)
  }

  async getTasks() {
    return this.registry.getTasks()
  }
}
