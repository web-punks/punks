import { AppInMemorySettings } from "../../../../../settings"

export type TasksModuleSettings = {
  enabled?: boolean
}

export const tasksSettings = new AppInMemorySettings<TasksModuleSettings>(
  "tasksSettings"
)
