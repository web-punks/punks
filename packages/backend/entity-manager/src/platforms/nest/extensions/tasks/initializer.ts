import { INestApplicationContext, Logger } from "@nestjs/common"
import { IAppInitializer, ITask } from "../../../../abstractions"
import { WpAppInitializer } from "../../decorators"
import { CustomDiscoveryService } from "../../ioc/discovery"
import { TASK_KEY, TaskProps } from "../../decorators/tasks"
import { TaskScheduler } from "./providers/scheduler"
import { TasksRegistry } from "./providers/registry"
import { tasksSettings } from "./settings"

@WpAppInitializer()
export class TasksInitializer implements IAppInitializer {
  private readonly logger = new Logger(TasksInitializer.name)

  constructor(
    private readonly discover: CustomDiscoveryService,
    private readonly scheduler: TaskScheduler,
    private readonly registry: TasksRegistry
  ) {}

  async initialize(app: INestApplicationContext): Promise<void> {
    if (this.tasksSettings?.enabled === false) {
      this.logger.log("Tasks disabled 🎰")
      return
    }

    await this.registerCronJobs()
    this.logger.log("Tasks initialized 🎰")
  }

  private get tasksSettings() {
    return tasksSettings.value
  }

  private async registerCronJobs() {
    const tasks = await this.discoverTasks()

    const duplicatedJobs = tasks.filter(
      (job, index, self) =>
        index !== self.findIndex((t) => t.meta.name === job.meta.name)
    )
    if (duplicatedJobs.length) {
      throw new Error(
        `Duplicated jobs found: ${duplicatedJobs
          .map((j) => j.meta.name)
          .join(", ")}`
      )
    }

    for (const task of tasks) {
      this.registry.registerTask(
        task.meta,
        task.discoveredClass.instance as ITask
      )
      if (task.meta.cronExpression) {
        await this.scheduler.scheduleTask(
          task.meta,
          task.discoveredClass.instance as ITask
        )
      }
    }
  }

  private async discoverTasks() {
    return await this.discover.providersWithMetaAtKey<TaskProps>(TASK_KEY)
  }
}
