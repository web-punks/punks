import { TasksRegistry } from "./registry"
import { TaskScheduler } from "./scheduler"
import { TaskShell } from "./shell"

export const TasksModuleProviders = [TaskScheduler, TaskShell, TasksRegistry]
