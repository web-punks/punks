import { DynamicModule, Module, OnModuleDestroy } from "@nestjs/common"
import { TasksInitializer } from "./initializer"
import { TaskScheduler } from "./providers/scheduler"
import { EntityManagerModule } from "../../module"
import { TasksModuleProviders } from "./providers"
import { TaskModuleServices } from "./services"
import { TasksModuleSettings, tasksSettings } from "./settings"

@Module({
  imports: [EntityManagerModule],
  providers: [TasksInitializer, ...TasksModuleProviders, ...TaskModuleServices],
  exports: [...TaskModuleServices],
})
export class TasksModule implements OnModuleDestroy {
  constructor(private readonly scheduler: TaskScheduler) {}

  static initialize(input: TasksModuleSettings) {
    tasksSettings.initialize(input)
  }

  onModuleDestroy() {
    this.scheduler.stopAllTasks()
  }
}
