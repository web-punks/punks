import { Injectable, Logger } from "@nestjs/common"
import { SchedulerRegistry } from "@nestjs/schedule"
import { TaskProps } from "../../../../decorators/tasks"
import { ITask, TaskRunType } from "../../../../../../abstractions"
import { TaskShell } from "../shell"
import { CronJob } from "cron"

@Injectable()
export class TaskScheduler {
  private readonly logger = new Logger(TaskScheduler.name)
  private readonly cronJobs: CronJob[] = []

  constructor(
    private schedulerRegistry: SchedulerRegistry,
    private readonly jobShell: TaskShell
  ) {}

  async scheduleTask(job: TaskProps, instance: ITask) {
    if (!job.cronExpression) {
      throw new Error(`Task ${job.name} has no cron expression`)
    }
    const cronJob = new CronJob(job.cronExpression, async (context) => {
      await this.jobShell.executeTask(instance, job, TaskRunType.Scheduled)
    })

    this.schedulerRegistry.addCronJob(job.name, cronJob)
    cronJob.start()

    this.cronJobs.push(cronJob)
    this.logger.log(`Jobs ${job.name} registered`)
  }

  stopAllTasks() {
    this.cronJobs.forEach((job) => {
      job.stop()
    })
  }
}
