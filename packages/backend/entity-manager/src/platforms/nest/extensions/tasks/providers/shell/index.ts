import { Inject, Injectable } from "@nestjs/common"
import { TaskConcurrency, TaskProps } from "../../../../decorators/tasks"
import { ILockRepository, ITask } from "../../../../../../abstractions"
import { floorDateToSecond, Log, newUuid } from "@punks/backend-core"
import {
  EntityManagerRegistry,
  getEntityManagerProviderToken,
} from "../../../../ioc"
import { TaskRunType } from "../../../../../../abstractions/tasks"
import { getCronCurrentSchedule } from "../../../../../../utils/cron"

@Injectable()
export class TaskShell {
  private readonly logger = Log.getLogger(TaskShell.name)

  constructor(
    private readonly registry: EntityManagerRegistry,
    @Inject(getEntityManagerProviderToken("OperationsLockRepository"))
    private readonly operations: ILockRepository
  ) {}

  async executeTask(
    instance: ITask,
    settings: TaskProps,
    runType: TaskRunType
  ) {
    switch (settings.concurrency) {
      case TaskConcurrency.Single:
        return await this.executeTaskSequential(instance, settings, runType)
      case TaskConcurrency.Multiple:
        return await this.executeTaskParallel(instance, settings, runType)
      default:
        throw new Error(`Unknown task concurrency: ${settings.concurrency}`)
    }
  }

  private async executeTaskParallel(
    instance: ITask,
    settings: TaskProps,
    runType: TaskRunType
  ) {
    await this.invokeTask(instance, settings, runType)
  }

  private async executeTaskSequential(
    instance: ITask,
    settings: TaskProps,
    runType: TaskRunType
  ) {
    const currentSchedule =
      settings.cronExpression && runType === TaskRunType.Scheduled
        ? getCronCurrentSchedule(settings.cronExpression, new Date())
        : undefined
    const lockKey = this.getTaskLockKey(settings, currentSchedule)
    const lock = await this.operations.acquireLock({
      lockUid: lockKey,
    })
    if (lock.available) {
      await this.invokeTask(instance, settings, runType)
    }
  }

  private getTaskLockKey(settings: TaskProps, cronSchedule?: Date) {
    return `task:${settings.name}:${
      cronSchedule ? floorDateToSecond(cronSchedule).toISOString() : newUuid()
    }`
  }

  private async invokeTask(
    instance: ITask,
    settings: TaskProps,
    runType: TaskRunType
  ) {
    try {
      this.logger.info(`Task ${settings.name} -> starting`)

      await instance.execute({
        name: settings.name,
        startedAt: new Date(),
        runId: newUuid(),
        runType,
      })

      this.logger.info(`Task ${settings.name} -> completed`)
    } catch (error) {
      this.logger.exception(`Task ${settings.name} -> failed`, error as Error)
    }
  }

  private get operationsLockService() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveOperationLockService()
  }
}
