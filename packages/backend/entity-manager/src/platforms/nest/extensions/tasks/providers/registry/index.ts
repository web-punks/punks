import { Injectable } from "@nestjs/common"
import { TaskProps } from "../../../../decorators/tasks"
import { ITask } from "../../../../../../abstractions"

@Injectable()
export class TasksRegistry {
  private readonly tasks: Map<
    string,
    {
      props: TaskProps
      instance: ITask
    }
  > = new Map()

  registerTask(props: TaskProps, instance: ITask) {
    if (this.tasks.has(props.name)) {
      throw new Error(`Task with name ${props.name} already registered`)
    }
    this.tasks.set(props.name, {
      instance,
      props,
    })
  }

  getTask(name: string) {
    const task = this.tasks.get(name)
    if (!task) {
      throw new Error(`Task with name ${name} not found`)
    }
    return task
  }

  getTasks() {
    return Array.from(this.tasks.values())
  }
}
