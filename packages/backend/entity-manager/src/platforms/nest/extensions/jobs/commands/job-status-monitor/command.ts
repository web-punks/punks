import { JobDefinition } from "../../abstractions"

export interface JobStatusUpdateMonitorInput {
  job: JobDefinition<unknown, unknown>
  instanceId: string
}

export class JobStatusMonitorCommand {
  constructor(public readonly input: JobStatusUpdateMonitorInput) {}
}
