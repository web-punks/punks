import { Injectable } from "@nestjs/common"
import {
  GetJobStatusInput,
  JobDispatchInput,
  UpsertJobDefinitionInput,
} from "../../abstractions/runner"
import { JobDefinition } from "../../abstractions"
import { JobsProviderFactory } from "../../resolvers/factory"

@Injectable()
export class JobProviderExecutor {
  constructor(private readonly factory: JobsProviderFactory) {}

  async dispatch(input: JobDispatchInput) {
    return await this.getProvider(input.definition).dispatch(input)
  }

  async upsertJobDefinition(input: UpsertJobDefinitionInput) {
    return await this.getProvider(input.definition).upsertJobDefinition(input)
  }

  async getStatus(input: GetJobStatusInput) {
    return await this.getProvider(input.definition).getJobStatus(input)
  }

  private getProvider = (jobDefinition: JobDefinition<unknown, unknown>) =>
    this.factory.getProvider(jobDefinition.provider)
}
