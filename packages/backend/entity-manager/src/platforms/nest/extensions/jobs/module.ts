import { DynamicModule, Module, OnModuleInit, Type } from "@nestjs/common"
import { JobsProviders } from "./providers"
import { JobsServices } from "./services"
import { JobsSettings } from "./abstractions/settings"
import { JobHandlers } from "./commands"
import { JobProcessors } from "./processors"
import { JobResolverProviders } from "./resolvers"
import { JobsScheduler } from "./providers/scheduler"
import { JobsProviderFactory } from "./resolvers/factory"
import { JobTasks } from "./task"
import { jobsSettings } from "./settings"

const ModuleData = {
  providers: [
    ...JobsProviders,
    ...JobTasks,
    ...JobsServices,
    ...JobHandlers,
    ...JobProcessors,
    ...JobResolverProviders,
  ],
  exports: [...JobsServices],
}

@Module({
  ...ModuleData,
})
export class JobsModule implements OnModuleInit {
  constructor(
    private readonly jobsScheduler: JobsScheduler,
    private readonly jobsProviderFactory: JobsProviderFactory
  ) {}

  async onModuleInit() {
    this.jobsScheduler.register()
    await this.jobsProviderFactory.initialize()
  }

  /**
   * @deprecated use initialize instead
   */
  static forRoot(input: JobsSettings): DynamicModule {
    jobsSettings.initialize(input)
    return {
      module: JobsModule,
      ...ModuleData,
    }
  }

  static initialize(input: JobsSettings) {
    jobsSettings.initialize(input)
  }
}
