import { JobStatus } from "../abstractions"

export const isJobPending = (status: JobStatus) => {
  switch (status) {
    case JobStatus.Created:
    case JobStatus.Ready:
    case JobStatus.Initializing:
      return true
    default:
      return false
  }
}

export const isJobRunning = (status: JobStatus) => {
  switch (status) {
    case JobStatus.Running:
      return true
    default:
      return false
  }
}

export const isJobStarted = (status: JobStatus) => {
  switch (status) {
    case JobStatus.Running:
      return true
    default:
      return false
  }
}

export const isJobEnded = (status: JobStatus) => {
  switch (status) {
    case JobStatus.Completed:
    case JobStatus.Faulted:
    case JobStatus.InitializationError:
      return true
    default:
      return false
  }
}
