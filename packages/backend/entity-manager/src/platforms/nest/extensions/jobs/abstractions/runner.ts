import { BatchJobVariables } from "../../../plugins/jobs/aws-batch/manager/types"
import { JobDefinition, JobSchedule } from "./definition"

export interface JobDispatchInput {
  definition: JobDefinition<unknown, unknown>
  schedule?: JobSchedule<unknown>
  instanceId: string
  payload?: unknown
  commandPlaceholders?: Record<string, string>
  variables?: BatchJobVariables
}

export enum JobProviderState {
  Initializing = "initializing",
  Running = "running",
  Completed = "completed",
  CompletedWithErrors = "completedWithErrors",
}

export interface GetJobStatusInput {
  definition: JobDefinition<unknown, unknown>
  instanceId: string
}

export interface JobStatusResult {
  state: JobProviderState
  error?: string
}

export interface CancelJobStatusInput {
  instanceId: string
}

export interface UpsertJobDefinitionInput {
  definition: JobDefinition<unknown, unknown>
}

export interface JobProviderInstance {
  dispatch(input: JobDispatchInput): Promise<void>
  upsertJobDefinition(input: UpsertJobDefinitionInput): Promise<void>
  getJobStatus(input: GetJobStatusInput): Promise<JobStatusResult>
  cancelJob(input: CancelJobStatusInput): Promise<void>
}
