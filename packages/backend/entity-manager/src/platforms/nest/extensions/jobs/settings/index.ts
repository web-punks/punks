import { AppInMemorySettings } from "../../../../../settings"
import { JobsSettings } from "../abstractions/settings"

export const jobsSettings = new AppInMemorySettings<JobsSettings>(
  "jobsSettings"
)
