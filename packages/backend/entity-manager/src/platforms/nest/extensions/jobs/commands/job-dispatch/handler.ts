import { CommandHandler, ICommandHandler } from "@nestjs/cqrs"
import { JobDispatchCommand } from "./command"
import { OperationLockService } from "../../../../services"
import { IJobInstancesRepository } from "../../abstractions/repository"
import { Inject } from "@nestjs/common"
import { getEntityManagerProviderToken } from "../../../../ioc"
import { Log, newUuid } from "@punks/backend-core"
import { JobStatus } from "../../abstractions"
import { JobProviderExecutor } from "../../processors/runner"

@CommandHandler(JobDispatchCommand)
export class JobDispatchHandler implements ICommandHandler<JobDispatchCommand> {
  private readonly logger = Log.getLogger(JobDispatchHandler.name)

  constructor(
    private readonly lock: OperationLockService,
    private readonly executor: JobProviderExecutor,
    @Inject(getEntityManagerProviderToken("JobInstancesRepository"))
    private readonly instances: IJobInstancesRepository
  ) {}

  async execute(command: JobDispatchCommand) {
    const {
      input: { job },
    } = command
    const result = await this.lock.executeExclusive({
      lockUid: `job-dispatch-${job.uid}`,
      operation: async () => await this.dispatchJob(command),
    })
    return result.result
  }

  private async dispatchJob(command: JobDispatchCommand) {
    const {
      input: {
        job,
        schedule,
        runType,
        payload,
        commandPlaceholders,
        variables,
      },
    } = command
    const instanceId = newUuid()
    try {
      this.logger.info(
        `JOB DISPATCH -> dispatching started job ${job.uid} -> ${instanceId}`
      )

      await this.instances.appendInstance(job, {
        id: instanceId,
        runType,
        scheduleUid: schedule?.uid,
        insertTime: new Date(),
      })

      await this.executor.dispatch({
        definition: job,
        schedule,
        instanceId,
        payload,
        commandPlaceholders,
        variables,
      })

      await this.instances.updateInstance(instanceId, {
        status: JobStatus.Ready,
      })

      this.logger.info(
        `JOB DISPATCH -> dispatching completed job ${job.uid} -> ${instanceId}`
      )
      return await this.instances.getById(instanceId)
    } catch (e: any) {
      await this.instances.updateInstance(instanceId, {
        status: JobStatus.InitializationError,
        error: `${e.message} -> \n\n ${e.stack}`,
      })
      this.logger.exception(
        `JOB DISPATCH -> error dispatching job ${job.uid} -> ${instanceId}`,
        e
      )
    }
  }
}
