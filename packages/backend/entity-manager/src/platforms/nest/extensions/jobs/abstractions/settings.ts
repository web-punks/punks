export type JobsSettings = {
  scheduler: {
    enabled: boolean
    intervalSeconds: number
  }
  monitor: {
    enabled: boolean
    intervalSeconds: number
  }
}
