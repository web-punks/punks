import { JobDefinition } from "./definition"
import { JobInstance, JobStatus } from "./instance"

type JobData = Omit<
  JobDefinition<unknown, unknown>,
  "id" | "createdOn" | "updatedOn"
>

export interface IJobDefinitionsRepository {
  getAllDefinitions(): Promise<JobDefinition<unknown, unknown>[]>
  getDefinitionByUid(
    uid: string
  ): Promise<JobDefinition<unknown, unknown> | undefined>
  ensureDefinition(data: JobData): Promise<JobDefinition<unknown, unknown>>
  createDefinition(data: JobData): Promise<JobDefinition<unknown, unknown>>
  updateDefinition(
    uid: string,
    data: JobData
  ): Promise<JobDefinition<unknown, unknown>>
}

export interface IJobInstancesRepository {
  getById(id: string): Promise<JobInstance>
  getByStatus(...status: JobStatus[]): Promise<JobInstance[]>
  getLastInstance(
    jobUid: string,
    scheduleUid?: string
  ): Promise<JobInstance | undefined>
  appendInstance(
    job: JobDefinition<unknown, unknown>,
    data: Pick<JobInstance, "id" | "insertTime" | "scheduleUid" | "runType">
  ): Promise<JobInstance>
  updateInstance(
    instanceId: string,
    data: Pick<
      JobInstance,
      "startTime" | "endTime" | "status" | "result" | "error"
    >
  ): Promise<JobInstance>
}
