import { Inject, Injectable } from "@nestjs/common"
import { CommandBus } from "@nestjs/cqrs"
import { Log } from "@punks/backend-core"
import { getEntityManagerProviderToken } from "../../../../ioc"
import {
  IJobDefinitionsRepository,
  JobDefinition,
  JobRunType,
  JobSchedule,
} from "../../abstractions"
import { RunnableJobsProcessor } from "../../processors/runnable-jobs"
import { JobDispatchCommand } from "../../commands/job-dispatch/command"

@Injectable()
export class JobsSchedulerTask {
  private readonly logger = Log.getLogger(JobsSchedulerTask.name)

  constructor(
    @Inject(getEntityManagerProviderToken("JobDefinitionsRepository"))
    private readonly jobDefinitions: IJobDefinitionsRepository,
    private readonly evaluator: RunnableJobsProcessor,
    private readonly commandBus: CommandBus
  ) {}

  async run() {
    this.logger.info(`JobsSchedulerTask -> run start`)
    const jobs = await this.jobDefinitions.getAllDefinitions()
    for (const job of jobs) {
      await this.evaluateJob(job)
    }

    this.logger.info(`JobsSchedulerTask -> run completed`)
  }

  private async evaluateJob(job: JobDefinition<unknown, unknown>) {
    try {
      this.logger.info(`JobsSchedulerTask -> evaluating job ${job.uid}`)
      for (const schedule of job.schedules) {
        await this.evaluateJobSchedule(job, schedule)
      }

      this.logger.info(`JobsSchedulerTask -> run completed ${job.uid}`)
    } catch (e) {
      this.logger.error(
        `JobsSchedulerTask -> error processing job ${job.id}`,
        e
      )
    }
  }

  private async evaluateJobSchedule(
    job: JobDefinition<unknown, unknown>,
    schedule: JobSchedule<unknown>
  ) {
    try {
      this.logger.info(
        `JobsSchedulerTask -> evaluating job schedule ${job.uid} ${schedule.uid}`
      )
      if (!schedule.enabled) {
        this.logger.info(
          `JobsSchedulerTask -> job schedule disabled ${job.uid} ${schedule.uid}`
        )
        return
      }

      const shouldRun = await this.evaluator.shouldRun(job, schedule)
      if (!shouldRun) {
        this.logger.info(
          `JobsSchedulerTask -> job schedule skipped ${job.uid} ${schedule.uid}`
        )
        return
      }

      this.logger.info(
        `JobsSchedulerTask -> run started ${job.uid} ${schedule.uid}`
      )
      await this.commandBus.execute(
        new JobDispatchCommand({
          job,
          schedule,
          runType: JobRunType.Scheduled,
        })
      )

      this.logger.info(
        `JobsSchedulerTask -> run completed ${job.uid} ${schedule.uid}`
      )
    } catch (e) {
      this.logger.error(
        `JobsSchedulerTask -> error processing job schedule ${job.id} ${schedule.uid}`,
        e
      )
    }
  }
}
