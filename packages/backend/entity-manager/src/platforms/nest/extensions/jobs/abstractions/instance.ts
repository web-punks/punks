export enum JobStatus {
  Created = "created",
  Ready = "ready",
  Initializing = "initializing",
  Running = "running",
  Completed = "completed",
  Faulted = "faulted",
  InitializationError = "initializationError",
}

export enum JobRunType {
  Scheduled = "scheduled",
  OnDemand = "onDemand",
}

export class JobInstance {
  id: string
  scheduleUid?: string
  jobUid: string
  runType: JobRunType
  status: JobStatus
  insertTime?: Date
  startTime?: Date
  endTime?: Date
  result?: any
  error?: any
  createdOn?: Date
  updatedOn?: Date
}
