import { JobsMonitorTask } from "./jobs-monitor"
import { JobsSchedulerTask } from "./jobs-scheduler"

export const JobTasks = [JobsMonitorTask, JobsSchedulerTask]
