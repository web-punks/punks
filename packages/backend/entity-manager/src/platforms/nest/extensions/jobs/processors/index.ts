import { RunnableJobsProcessor } from "./runnable-jobs"
import { JobProviderExecutor } from "./runner"

export const JobProcessors = [RunnableJobsProcessor, JobProviderExecutor]
