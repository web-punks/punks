import { Inject, Injectable } from "@nestjs/common"
import { CommandBus } from "@nestjs/cqrs"
import { Log } from "@punks/backend-core"
import { JobInstance, JobStatus } from "../../abstractions"
import {
  IJobDefinitionsRepository,
  IJobInstancesRepository,
} from "../../abstractions/repository"
import { getEntityManagerProviderToken } from "../../../../ioc"
import { JobStatusMonitorCommand } from "../../commands/job-status-monitor/command"

@Injectable()
export class JobsMonitorTask {
  private readonly logger = Log.getLogger(JobsMonitorTask.name)
  constructor(
    @Inject(getEntityManagerProviderToken("JobInstancesRepository"))
    private readonly jobInstances: IJobInstancesRepository,
    @Inject(getEntityManagerProviderToken("JobDefinitionsRepository"))
    private readonly jobDefinitions: IJobDefinitionsRepository,
    private readonly commandBus: CommandBus
  ) {}

  async run() {
    this.logger.info(`JobsMonitorTask -> run start`)
    const instances = await this.jobInstances.getByStatus(
      JobStatus.Created,
      JobStatus.Ready,
      JobStatus.Running,
      JobStatus.Initializing
    )
    for (const instance of instances) {
      await this.monitorInstance(instance)
    }

    this.logger.info(`JobsMonitorTask -> run completed`)
  }

  private async monitorInstance(instance: JobInstance) {
    try {
      this.logger.info(`JobsMonitorTask -> evaluating job ${instance.id}`)

      const job = await this.jobDefinitions.getDefinitionByUid(instance.jobUid)
      if (!job) {
        throw new Error(`Job definition not found -> ${instance.jobUid}`)
      }

      await this.commandBus.execute(
        new JobStatusMonitorCommand({
          instanceId: instance.id,
          job,
        })
      )

      this.logger.info(`JobsMonitorTask -> run completed ${instance.id}`)
    } catch (e) {
      this.logger.error(
        `JobsMonitorTask -> error processing instance ${instance.id}`,
        e
      )
    }
  }
}
