import { BatchJobVariables } from "../../../../plugins/jobs/aws-batch/manager/types"

export type JobTriggerInput = {
  jobUid: string
  scheduleUid?: string
  payload?: any
  commandPlaceholders?: Record<string, string>
  variables?: BatchJobVariables
}
