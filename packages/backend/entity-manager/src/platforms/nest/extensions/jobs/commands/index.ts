import { JobDispatchHandler } from "./job-dispatch/handler"
import { JobStatusMonitorHandler } from "./job-status-monitor/handler"
import { JobDefinitionUpdateHandler } from "./job-update/handler"

export const JobHandlers = [
  JobDispatchHandler,
  JobStatusMonitorHandler,
  JobDefinitionUpdateHandler,
]
