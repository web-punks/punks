import { CommandHandler, ICommandHandler } from "@nestjs/cqrs"
import { JobDefinitionUpdateCommand } from "./command"
import { OperationLockService } from "../../../../services"
import { IJobDefinitionsRepository } from "../../abstractions"
import { Inject } from "@nestjs/common"
import { getEntityManagerProviderToken } from "../../../../ioc"
import { Log } from "@punks/backend-core"
import { JobProviderExecutor } from "../../processors/runner"

@CommandHandler(JobDefinitionUpdateCommand)
export class JobDefinitionUpdateHandler
  implements ICommandHandler<JobDefinitionUpdateCommand>
{
  private readonly logger = Log.getLogger(JobDefinitionUpdateHandler.name)

  constructor(
    private readonly lock: OperationLockService,
    @Inject(getEntityManagerProviderToken("JobDefinitionsRepository"))
    private readonly definitions: IJobDefinitionsRepository,
    private readonly executor: JobProviderExecutor
  ) {}

  async execute(command: JobDefinitionUpdateCommand) {
    const {
      input: { job },
    } = command
    await this.lock.executeExclusive({
      lockUid: `job-definition-update-${job.uid}`,
      operation: async () => await this.updateJob(command),
    })
  }

  private async updateJob(command: JobDefinitionUpdateCommand) {
    const {
      input: { job },
    } = command
    this.logger.info(
      `JOB DEFINITION UPDATE -> dispatching started job ${job.uid}`
    )

    await this.executor.upsertJobDefinition({
      definition: job,
    })

    await this.definitions.ensureDefinition(job)

    this.logger.info(
      `JOB DEFINITION UPDATE -> status update completed job ${job.uid}`
    )
  }
}
