import { Inject, Injectable } from "@nestjs/common"
import { Dict, Log, toItemsDict } from "@punks/backend-core"
import { JobProviderInstance } from "../../abstractions/runner"
import { JOB_PROVIDER } from "../../decorators/constants"
import {
  CustomDiscoveryService,
  getEntityManagerProviderToken,
} from "../../../../ioc"

@Injectable()
export class JobsProviderFactory {
  private readonly logger = Log.getLogger(JobsProviderFactory.name)
  private providers: Dict<JobProviderInstance>

  constructor(
    // @Inject(getEntityManagerProviderToken("DiscoveryService"))
    private readonly discover: CustomDiscoveryService
  ) {}

  async initialize() {
    const instances = await this.discover.providersWithMetaAtKey<string>(
      JOB_PROVIDER
    )

    this.providers = toItemsDict(
      instances,
      (x) => x.meta,
      (x) => x.discoveredClass.instance as JobProviderInstance
    )

    instances.forEach((x) =>
      this.logger.info(`Discovered job provider -> ${x.discoveredClass.name}`, {
        meta: x.meta,
      })
    )
  }

  getProvider(id: string) {
    const provider = this.providers[id]
    if (!provider) {
      throw new Error(
        `Job provider ${id} is not defined (available options: -> ${Object.keys(
          this.providers
        )})`
      )
    }
    return provider
  }
}
