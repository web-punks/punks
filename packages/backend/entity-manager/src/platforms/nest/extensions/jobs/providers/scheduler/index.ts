import { Injectable } from "@nestjs/common"
import { SchedulerRegistry } from "@nestjs/schedule"
import { CronJob } from "cron"
import { Log } from "@punks/backend-core"
import { JobsSchedulerTask } from "../../task/jobs-scheduler"
import { JobsMonitorTask } from "../../task/jobs-monitor"
import { jobsSettings } from "../../settings"

@Injectable()
export class JobsScheduler {
  private readonly logger = Log.getLogger(JobsScheduler.name)

  constructor(
    private readonly schedulerRegistry: SchedulerRegistry,
    private readonly schedulerTask: JobsSchedulerTask,
    private readonly monitorTask: JobsMonitorTask
  ) {}

  register() {
    this.registerJobsScheduler()
    this.registerJobsMonitor()
  }

  private registerJobsScheduler = () => {
    if (!this.jobsSettings.scheduler.enabled) {
      this.logger.info("Jos scheduler disabled")
      return
    }

    const job = new CronJob(this.batchJobsSchedule(), async () => {
      await this.schedulerTask.run()
    })
    this.schedulerRegistry.addCronJob("jobsScheduler", job)
    job.start()
    this.logger.info("Jos scheduler registered")
  }

  private registerJobsMonitor = () => {
    if (!this.jobsSettings.monitor.enabled) {
      this.logger.info("Jos monitor disabled")
      return
    }

    const job = new CronJob(this.batchJobsMonitor(), async () => {
      await this.monitorTask.run()
    })
    this.schedulerRegistry.addCronJob("jobsMonitor", job)
    job.start()
    this.logger.info("Jos monitor registered")
  }

  private batchJobsSchedule = () =>
    `*/${this.jobsSettings.scheduler.intervalSeconds} * * * * *`

  private batchJobsMonitor = () =>
    `*/${this.jobsSettings.monitor.intervalSeconds} * * * * *`

  private get jobsSettings() {
    return jobsSettings.value
  }
}
