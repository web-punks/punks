import { Inject, Injectable } from "@nestjs/common"
import { getEntityManagerProviderToken } from "../../../../ioc"
import {
  IJobDefinitionsRepository,
  IJobInstancesRepository,
  JobDefinition,
  JobInstance,
  JobRunType,
} from "../../abstractions"
import { CommandBus } from "@nestjs/cqrs"
import { JobTriggerInput } from "./types"
import { JobDispatchCommand } from "../../commands/job-dispatch/command"
import { JobDefinitionUpdateCommand } from "../../commands/job-update/command"
import { JobStatusMonitorCommand } from "../../commands/job-status-monitor/command"

@Injectable()
export class JobsService {
  constructor(
    @Inject(getEntityManagerProviderToken("JobDefinitionsRepository"))
    private readonly definitions: IJobDefinitionsRepository,
    @Inject(getEntityManagerProviderToken("JobInstancesRepository"))
    private readonly instances: IJobInstancesRepository,
    private readonly commandBus: CommandBus
  ) {}

  triggerJob = async (input: JobTriggerInput): Promise<JobInstance> => {
    const job = await this.definitions.getDefinitionByUid(input.jobUid)
    if (!job) {
      throw new Error(`Job ${input.jobUid} not found`)
    }
    const schedule = input.scheduleUid
      ? job.schedules.find((x) => x.uid === input.scheduleUid)
      : undefined
    if (input.scheduleUid && !schedule) {
      throw new Error(
        `Job ${input.jobUid} schedule ${input.scheduleUid} not found`
      )
    }

    return await this.commandBus.execute(
      new JobDispatchCommand({
        job,
        schedule,
        runType: JobRunType.OnDemand,
        payload: input.payload,
        commandPlaceholders: input.commandPlaceholders,
        variables: input.variables,
      })
    )
  }

  updateJob = async (input: JobDefinition<unknown, unknown>) => {
    await this.commandBus.execute(
      new JobDefinitionUpdateCommand({
        job: input,
      })
    )
  }

  instanceMonitor = async (input: { instanceId: string }) => {
    const { instanceId } = input
    const instance = await this.instances.getById(instanceId)
    if (!instance) {
      throw new Error(`Job instance ${instanceId} not found`)
    }

    const job = await this.definitions.getDefinitionByUid(instance.jobUid)
    if (!job) {
      throw new Error(`Job definition ${instance.jobUid} not found`)
    }
    return await this.commandBus.execute(
      new JobStatusMonitorCommand({
        instanceId,
        job,
      })
    )
  }
}
