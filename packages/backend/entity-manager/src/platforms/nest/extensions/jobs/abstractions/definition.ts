export enum JobConcurrency {
  Sequential = "sequential",
  Parallel = "parallel",
}

export class JobSchedule<TInvocationParams> {
  uid: string
  cron: string
  enabled: boolean
  invocationOverrides?: TInvocationParams
}

export type JobDefinition<TInfrastructureParams, TInvocationParams> = {
  id: string
  uid: string
  name: string
  provider: string
  infrastructureParams: TInfrastructureParams
  invocationParams: TInvocationParams
  schedules: JobSchedule<TInvocationParams>[]
  concurrency: JobConcurrency
}
