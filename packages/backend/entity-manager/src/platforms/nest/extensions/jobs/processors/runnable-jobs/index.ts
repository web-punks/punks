import { Inject, Injectable } from "@nestjs/common"
import { subDays } from "date-fns"
import { getEntityManagerProviderToken } from "../../../../ioc"
import {
  IJobInstancesRepository,
  JobConcurrency,
  JobDefinition,
  JobSchedule,
} from "../../abstractions"
import { isJobPending, isJobRunning } from "../../utils/status"
import { Log } from "@punks/backend-core"
import { getCronPrevDate } from "../../utils/cron"

@Injectable()
export class RunnableJobsProcessor {
  private readonly logger = Log.getLogger(RunnableJobsProcessor.name)

  constructor(
    @Inject(getEntityManagerProviderToken("JobInstancesRepository"))
    private readonly jobInstances: IJobInstancesRepository
  ) {}

  async shouldRun(
    job: JobDefinition<unknown, unknown>,
    schedule: JobSchedule<unknown>
  ) {
    const lastInstance = await this.jobInstances.getLastInstance(
      job.uid,
      schedule.uid
    )
    if (
      lastInstance &&
      (isJobPending(lastInstance.status) ||
        isJobRunning(lastInstance.status)) &&
      job.concurrency === JobConcurrency.Sequential
    ) {
      this.logger.info(`RunnableJobsProcessor -> job already running`)
      return false
    }

    const lastRun = lastInstance?.insertTime ?? subDays(new Date(), 365)
    const prevDate = getCronPrevDate(schedule.cron)

    const shouldRun = prevDate > lastRun
    this.logger.info(
      `RunnableJobsProcessor ${
        schedule.cron
      } -> shouldRun: ${shouldRun} - prev-> ${prevDate.toISOString()} - last-> ${lastRun.toISOString()}`
    )
    return shouldRun
  }
}
