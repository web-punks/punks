import { parseExpression } from "cron-parser"

export const getCronPrevDate = (cron: string) =>
  new Date(parseExpression(cron).prev().toISOString())

export const getCronNextDate = (cron: string) =>
  new Date(parseExpression(cron).next().toISOString())
