import { CommandHandler, ICommandHandler } from "@nestjs/cqrs"
import { JobStatusMonitorCommand } from "./command"
import {
  IJobInstancesRepository,
  JobProviderState,
  JobStatus,
} from "../../abstractions"
import { OperationLockService } from "../../../../services"
import { getEntityManagerProviderToken } from "../../../../ioc"
import { Inject } from "@nestjs/common"
import { Log } from "@punks/backend-core"
import { JobProviderExecutor } from "../../processors/runner"
import { isJobEnded, isJobStarted } from "../../utils/status"

const mapState = (providerState: JobProviderState): JobStatus => {
  switch (providerState) {
    case JobProviderState.Initializing:
      return JobStatus.Initializing
    case JobProviderState.Running:
      return JobStatus.Running
    case JobProviderState.Completed:
      return JobStatus.Completed
    case JobProviderState.CompletedWithErrors:
      return JobStatus.Faulted
  }
}

@CommandHandler(JobStatusMonitorCommand)
export class JobStatusMonitorHandler
  implements ICommandHandler<JobStatusMonitorCommand>
{
  private readonly logger = Log.getLogger(JobStatusMonitorHandler.name)

  constructor(
    private readonly lock: OperationLockService,
    private readonly executor: JobProviderExecutor,
    @Inject(getEntityManagerProviderToken("JobInstancesRepository"))
    private readonly instances: IJobInstancesRepository
  ) {}

  async execute(command: JobStatusMonitorCommand) {
    const {
      input: { job },
    } = command
    await this.lock.executeExclusive({
      lockUid: `job-status-update-${job.uid}`,
      operation: async () => await this.watchJob(command),
    })
  }

  private async watchJob(command: JobStatusMonitorCommand) {
    const {
      input: { job, instanceId },
    } = command
    try {
      this.logger.info(
        `JOB STATUS UPDATE -> dispatching started job ${job.uid} -> ${instanceId}`
      )

      const current = await this.instances.getById(instanceId)
      if (!current) {
        throw new Error(`Job instance ${instanceId} not found`)
      }

      if (current.jobUid !== job.uid) {
        throw new Error(
          `Job instance ${instanceId} not matching uid: ${current.jobUid} -> expected: ${job.uid}`
        )
      }

      const result = await this.executor.getStatus({
        definition: job,
        instanceId,
      })

      const newStatus = mapState(result.state)
      if (newStatus === current.status) {
        this.logger.info(
          `JOB STATUS UPDATE -> status unchanged ${job.uid} -> ${instanceId}`
        )
        return
      }

      await this.instances.updateInstance(instanceId, {
        status: newStatus,
        endTime:
          !current.endTime && isJobEnded(newStatus) ? new Date() : undefined,
        startTime:
          !current.startTime && isJobStarted(newStatus)
            ? new Date()
            : undefined,
      })

      this.logger.info(
        `JOB STATUS UPDATE -> status update completed job ${job.uid} -> ${instanceId}`
      )
    } catch (e) {
      this.logger.error(
        `JOB STATUS UPDATE -> updating job status ${job.uid} -> ${instanceId}`,
        e
      )
    }
  }
}
