import { SetMetadata } from "@nestjs/common"
import { JOB_PROVIDER } from "./constants"

export const JobProvider = (provider: string) =>
  SetMetadata(JOB_PROVIDER, provider)
