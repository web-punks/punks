import { JobDefinition } from "../../abstractions"

export interface JobDefinitionUpdateInput {
  job: JobDefinition<unknown, unknown>
}

export class JobDefinitionUpdateCommand {
  constructor(public readonly input: JobDefinitionUpdateInput) {}
}
