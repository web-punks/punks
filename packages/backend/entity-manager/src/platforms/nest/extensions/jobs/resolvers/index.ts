import { JobsProviderFactory } from "./factory"

export const JobResolverProviders = [JobsProviderFactory]
