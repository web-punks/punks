import { BatchJobVariables } from "../../../../plugins/jobs/aws-batch/manager/types"
import { JobDefinition, JobRunType, JobSchedule } from "../../abstractions"

export interface JobDispatchCommandInput {
  job: JobDefinition<unknown, unknown>
  schedule?: JobSchedule<unknown>
  runType: JobRunType
  payload?: unknown
  commandPlaceholders?: Record<string, string>
  variables?: BatchJobVariables
}

export class JobDispatchCommand {
  constructor(public readonly input: JobDispatchCommandInput) {}
}
