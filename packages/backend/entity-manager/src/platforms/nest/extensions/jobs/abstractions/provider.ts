export enum JobProviderState {
  Initializing = "initializing",
  Running = "running",
  Completed = "completed",
  CompletedWithErrors = "completedWithErrors",
}
