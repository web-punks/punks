import { byField, sort } from "@punks/backend-core"

export const orderByPriority = <T>(
  items: T[],
  priorityFieldSelector: (item: T) => number | undefined,
  nameFieldSelector: (item: T) => string
) => {
  return sort(
    items,
    byField((x) => priorityFieldSelector(x) ?? Number.MAX_VALUE),
    byField((x) => nameFieldSelector(x))
  )
}
