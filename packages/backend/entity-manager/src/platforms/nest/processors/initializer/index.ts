import { INestApplicationContext, Injectable, Logger } from "@nestjs/common"
import {
  BucketProviderProps,
  CacheInstanceProps,
  EmailLoggerProps,
  EmailTemplateProps,
  EntityAuthMiddlewareProps,
  EntityConnectorMapperProps,
  EntityConnectorProps,
  EntityConverterProps,
  EntityManagerSymbols,
  EntityQueryBuilderProps,
  EntityRepositoryProps,
  EntitySeederProps,
  EntitySerializerProps,
  EntitySnapshotServiceProps,
  EventsTrackerProps,
  GlobalAuthenticationMiddlewareProps,
} from "../../decorators"
import { toArrayDict, toDict } from "@punks/backend-core"
import { EntityManagerRegistry } from "../../ioc/registry"
import { EntityAdapterProps } from "../../decorators/adapter"
import {
  CustomDiscoveryService,
  ModulesContainerProvider,
} from "../../ioc/discovery"
import { NestEventEmitter } from "../../providers/events"
import { ModulesContainer } from "@nestjs/core"
import {
  EntityManagerSettings,
  IAuthenticationContextProvider,
  IAuthenticationMiddleware,
  IBucketProvider,
  ICacheInstance,
  IEmailLogger,
  IEmailProvider,
  IEmailTemplate,
  IEmailTemplateMiddleware,
  IEntityVersioningProvider,
  IEventsTracker,
  IFileProvider,
  IFilesReferenceRepository,
  IMediaFolderRepository,
  IMediaProvider,
  IMediaReferenceRepository,
  IOperationLockService,
  ISecretsProvider,
} from "../../../../abstractions"
import { IAppInitializer } from "../../../../abstractions/app"
import { EmailTemplatesCollection } from "../../providers/email-templates"
import { AppInitializerProps } from "../../decorators/initializer"
import { orderByPriority } from "./utils"
import { IEntitySeeder } from "../../../../abstractions/seed"
import { PipelineController } from "../../../../templates/pipelines/controller"
import { orderBy } from "lodash"
import { EntityProps } from "../../decorators/entity"
import { EntityVersioningProviderProps } from "../../decorators/versioning"
import {
  FileProviderProps,
  FileReferenceRepositoryProps,
} from "../../decorators/files"
import { SecretsProviderProps } from "../../decorators/secrets"
import { OperationLockServiceProps } from "../../decorators/operations"
import { EmailTemplateMiddlewareProps } from "../../decorators/email"

export interface EntityManagerStaticProviders {
  modulesContainer: ModulesContainer
  authenticationProvider?: IAuthenticationContextProvider<unknown>
  settings: EntityManagerSettings
}

@Injectable()
export class EntityManagerInitializer {
  private readonly logger = new Logger(EntityManagerInitializer.name)

  constructor(
    private readonly discover: CustomDiscoveryService,
    private readonly registry: EntityManagerRegistry,
    private readonly eventEmitter: NestEventEmitter
  ) {}

  async initialize(
    app: INestApplicationContext,
    staticProviders: EntityManagerStaticProviders
  ) {
    this.logger.log("Initializing entity manager static providers 🥁")
    await this.initializeProviders(app, staticProviders)

    this.logger.log("Initializing entity manager global services 🥁")
    await this.registry.registerGlobalServices({
      eventEmitter: this.eventEmitter,
    })

    this.logger.log("Initializing entity manager types 🥁")
    await this.registerEntities(app, staticProviders)

    if (staticProviders.authenticationProvider) {
      await this.registerAuthentication(staticProviders.authenticationProvider)
      this.logger.log("Entity manager authentication initialized 🔑")
    }

    await this.registerPipelinesServices()
    await this.registerEmailTemplates()
    await this.registerEmailProviders()
    await this.registerEventTrackingProviders()
    await this.registerVersioningProviders()
    await this.registerEmailLogger()
    await this.registerEmailTemplateMiddleware()
    await this.registerFilesReferenceRepositoryProviders()
    await this.registerFileProviders()
    await this.registerMediaProviders()
    await this.registerMediaReferenceRepositoryProviders()
    await this.registerMediaFolderRepositoryProviders()
    await this.registerGlobalAuthenticationMiddlewares()
    await this.registerBucketProviders()
    await this.registerSecretsProviders()
    await this.registerCacheInstances()
    await this.registerOperationLockService()

    await this.executeInitializers(app)
    await this.executeSeeders()

    this.logger.log("Entity manager initialization completed 🚀")
  }

  private async registerAuthentication(
    authenticationProvider: IAuthenticationContextProvider<unknown>
  ) {
    this.registry.getContainer().addAuthentication({
      provider: authenticationProvider,
    })
  }

  private async registerEntities(
    app: INestApplicationContext,
    staticProviders: EntityManagerStaticProviders
  ) {
    const entities = await this.discoverEntities()
    const repositories = await this.discoverRepositories()
    const queryBuilders = await this.discoverQueryBuilders()
    const converters = await this.discoverConverters()
    const serializers = await this.discoverSerializers()
    const snapshotServices = await this.discoverSnapshotService()
    const adapters = await this.discoverAdapters()
    const auth = await this.discoverAuthMiddlewares()
    const connectors = await this.discoverEntityConnectors()
    const connectorMappers = await this.discoverEntityConnectorMappers()

    const repositoriesDict = toDict(repositories, (q) => q.meta.entityName)
    const queryBuilderDict = toDict(queryBuilders, (q) => q.meta.entityName)
    const converterDict = toDict(converters, (c) => c.meta.entityName)
    const serializerDict = toDict(serializers, (s) => s.meta.entityName)
    const snapshotServicesDict = toDict(
      snapshotServices,
      (s) => s.meta.entityName
    )
    const adapterDict = toDict(adapters, (a) => a.meta.entityName)
    const authDict = toDict(auth, (a) => a.meta.entityName)
    const connectorsDict = toArrayDict(connectors, (c) => c.meta.entityName)
    const connectorMappersDict = toArrayDict(
      connectorMappers,
      (c) => c.meta.entityName
    )

    const sortedEntities = orderBy(entities, (x) => x.meta.name)
    this.logger.log(
      `Discovered entities: \n${sortedEntities
        .map((x) => x.discoveredClass.name)
        .join(" \n")}`
    )

    this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .registerSettings(staticProviders.settings)

    for (const entity of sortedEntities) {
      const entityName = entity.meta.name

      const repository = repositoriesDict[entityName]
      if (!repository) {
        throw new Error(`No repository found for entity: ${entityName}`)
      }

      const queryBuilder = queryBuilderDict[entityName]
      if (!queryBuilder) {
        throw new Error(`No query builder found for entity: ${entityName}`)
      }

      const converter = converterDict[entityName]
      const adapter = adapterDict[entityName]
      const authMiddleware = authDict[entityName]
      const serializer = serializerDict[entityName]
      const snapshotService = snapshotServicesDict[entityName]
      const connectors = connectorsDict[entityName] ?? []
      const connectorMappers = connectorMappersDict[entityName] ?? []

      await this.registry.registerDiscoveredEntity(app, {
        entityName,
        entity,
        adapter,
        repository,
        queryBuilder,
        converter,
        serializer,
        snapshotService,
        authMiddleware,
        settings: staticProviders.settings,
        connectorMappers,
        connectors,
      })

      this.logger.log(`Entity ${entityName} registered:
  - Repository: ${repository.discoveredClass.injectType?.name ?? ""}
  - Query builder: ${queryBuilder.discoveredClass.injectType?.name ?? ""}
  - Adapter: ${adapter?.discoveredClass.injectType?.name ?? ""}
  - Converter: ${converter?.discoveredClass.injectType?.name ?? ""}
  - Serializer: ${serializer?.discoveredClass.injectType?.name ?? ""}
  - SnapshotService: ${snapshotService?.discoveredClass.injectType?.name ?? ""}
  - Auth middleware: ${authMiddleware?.discoveredClass.injectType?.name ?? ""}
  - Connectors: ${connectors
    .map((x) => x?.discoveredClass.injectType?.name ?? "")
    .join(", ")}
✨- ConnectorMappers: ${connectorMappers
        .map((x) => x?.discoveredClass.injectType?.name ?? "")
        .join(", ")}
      `)
    }

    this.logger.log("Entity manager types initialized 👌")
  }

  private async executeInitializers(app: INestApplicationContext) {
    const initializers = await this.discoverAppInitializers()
    const sortedInitializers = orderByPriority(
      initializers,
      (x) => x.meta.priority,
      (x) => x.discoveredClass.name
    )
    for (const initializer of sortedInitializers) {
      await (
        initializer.discoveredClass.instance as IAppInitializer
      ).initialize(app)
      this.logger.log(
        `Entity manager app initializer ${initializer.discoveredClass.name} initialized 💪`
      )
    }
  }

  private async executeSeeders() {
    const seders = await this.discoverEntitySeeders()
    const sortedSeeder = orderByPriority(
      seders,
      (x) => x.meta.priority,
      (x) => x.discoveredClass.name
    )
    for (const initializer of sortedSeeder) {
      await (initializer.discoveredClass.instance as IEntitySeeder).execute()
      this.logger.log(
        `Entity seeder initializer ${initializer.discoveredClass.name} initialized 💪`
      )
    }
  }

  private async registerFileProviders() {
    const providers = await this.discoverFileProviders()
    if (!providers.length) {
      this.logger.warn("No file providers found ⚠️")
      return
    }

    for (const provider of providers) {
      this.registry
        .getContainer()
        .getEntitiesServicesLocator()
        .registerFileProvider(
          provider.meta.providerId,
          provider.discoveredClass.instance as IFileProvider
        )
      this.logger.log(
        `File provider ${provider.discoveredClass.name} registered 🚜`
      )
    }
  }

  private async registerMediaProviders() {
    const providers = await this.discoverMediaProviders()
    if (!providers.length) {
      this.logger.warn("No media providers found ⚠️")
      return
    }

    for (const provider of providers) {
      this.registry
        .getContainer()
        .getEntitiesServicesLocator()
        .registerMediaProvider(
          provider.meta.providerId,
          provider.discoveredClass.instance as IMediaProvider
        )
      this.logger.log(
        `Media provider ${provider.discoveredClass.name} registered 🚜`
      )
    }
  }

  private async registerMediaReferenceRepositoryProviders() {
    const providers = await this.discoverMediaReferenceRepositories()
    if (!providers.length) {
      this.logger.warn("No media reference repository ⚠️")
      return
    }

    this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .registerMediaReferenceRepository(
        providers[0].discoveredClass.instance as IMediaReferenceRepository
      )
    this.logger.log(
      `Media Reference Repository ${providers[0].discoveredClass.name} registered 🚜`
    )
  }

  private async registerMediaFolderRepositoryProviders() {
    const providers = await this.discoverMediaFolderRepositories()
    if (!providers.length) {
      this.logger.warn("No media folder repository ⚠️")
      return
    }

    this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .registerMediaFolderRepository(
        providers[0].discoveredClass.instance as IMediaFolderRepository
      )
    this.logger.log(
      `Media Folder Repository ${providers[0].discoveredClass.name} registered 🚜`
    )
  }

  private async registerGlobalAuthenticationMiddlewares() {
    const providers = await this.discoverGlobalAuthenticationMiddlewares()
    for (const provider of providers) {
      this.registry
        .getContainer()
        .getEntitiesServicesLocator()
        .registerAuthenticationMiddleware(
          provider.meta.name,
          provider.discoveredClass.instance as IAuthenticationMiddleware<
            any,
            unknown
          >
        )
      this.logger.log(
        `Authentication middleware ${provider.discoveredClass.name} registered 🚜`
      )
    }
  }

  private async registerSecretsProviders() {
    const providers = await this.discoverSecretsProviders()
    if (!providers.length) {
      this.logger.warn("No secrets providers found ⚠️")
      return
    }

    for (const provider of providers) {
      this.registry
        .getContainer()
        .getEntitiesServicesLocator()
        .registerSecretsProvider(
          provider.meta.providerId,
          provider.discoveredClass.instance as ISecretsProvider
        )
      this.logger.log(
        `Secrets provider ${provider.discoveredClass.name} registered 🚜`
      )
    }
  }

  private async registerBucketProviders() {
    const providers = await this.discoverBucketProviders()
    if (!providers.length) {
      this.logger.warn("No bucket providers found ⚠️")
      return
    }

    for (const provider of providers) {
      this.registry
        .getContainer()
        .getEntitiesServicesLocator()
        .registerBucketProvider(
          provider.meta.providerId,
          provider.discoveredClass.instance as IBucketProvider
        )
      this.logger.log(
        `Bucket provider ${provider.discoveredClass.name} registered 🚜`
      )
    }
  }

  private async registerCacheInstances() {
    const providers = await this.discoverCacheInstances()

    for (const provider of providers) {
      this.registry
        .getContainer()
        .getEntitiesServicesLocator()
        .registerCacheInstance(
          provider.meta.instanceName,
          provider.discoveredClass.instance as ICacheInstance
        )
      this.logger.log(
        `Cache instance ${provider.meta.instanceName} -> ${provider.discoveredClass.name} registered 🚜`
      )
    }
  }

  private async registerFilesReferenceRepositoryProviders() {
    const providers = await this.discoverFilesReferenceRepositoryProviders()
    if (!providers.length) {
      this.logger.warn("No files repository ⚠️")
      return
    }

    this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .registerFilesReferenceRepositoryProviders(
        providers[0].discoveredClass.instance as IFilesReferenceRepository
      )
    this.logger.log(
      `Files Reference Repository ${providers[0].discoveredClass.name} registered 🚜`
    )
  }

  private async registerEventTrackingProviders() {
    const providers = await this.discoverEventTrackingProviders()
    if (!providers.length) {
      this.logger.warn("No events tracker ⚠️")
      return
    }

    // todo: handle multiple trackers
    this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .registerEventsTracker(
        providers[0].discoveredClass.instance as IEventsTracker<any>
      )
    this.logger.log(
      `Events tracker ${providers[0].discoveredClass.name} registered 🚜`
    )
  }

  private async registerEmailLogger() {
    const loggers = await this.discoverEmailLogger()
    if (!loggers.length) {
      this.logger.warn("No email loggers ⚠️")
      return
    }

    this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .registerEmailLogger(loggers[0].discoveredClass.instance as IEmailLogger)
    this.logger.log(
      `Email logger ${loggers[0].discoveredClass.name} registered 🚜`
    )
  }

  private async registerEmailTemplateMiddleware() {
    const loggers = await this.discoverEmailTemplateMiddleware()
    if (!loggers.length) {
      return
    }

    this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .registerEmailTemplateMiddleware(
        loggers[0].discoveredClass.instance as IEmailTemplateMiddleware
      )
    this.logger.log(
      `Email template middleware ${loggers[0].discoveredClass.name} registered 🚜`
    )
  }

  private async registerVersioningProviders() {
    const providers = await this.discoverVersioningProviders()
    if (!providers.length) {
      return
    }

    for (const provider of providers) {
      this.registry
        .getContainer()
        .getEntitiesServicesLocator()
        .registerEntityVersioningProvider(
          provider.discoveredClass.instance as IEntityVersioningProvider
        )
      this.logger.log(
        `Versioning provider ${provider.discoveredClass.name} registered 🚜`
      )
    }
  }

  private async registerEmailProviders() {
    const providers = await this.discoverEmailProviders()
    if (!providers.length) {
      this.logger.warn("No email providers found ⚠️")
      return
    }

    // todo: handle multiple email providers
    this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .registerEmailProvider(
        providers[0].discoveredClass.instance as IEmailProvider<unknown>
      )
    this.logger.log(
      `Email provider ${providers[0].discoveredClass.name} registered 📧`
    )
  }

  private async registerEmailTemplates() {
    const emailTemplates = await this.discoverEmailTemplates()
    const collection = new EmailTemplatesCollection()
    for (const emailTemplate of emailTemplates) {
      collection.registerTemplate(
        emailTemplate.meta.templateId,
        emailTemplate.discoveredClass.instance as IEmailTemplate<
          unknown,
          unknown,
          unknown
        >
      )
      this.logger.log(
        `Entity manager email template ${emailTemplate.meta.templateId}: ${emailTemplate.discoveredClass.name} registered 📧`
      )
    }
    this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .registerEmailTemplatesCollection(collection)
  }

  private async registerOperationLockService() {
    const operationLockServices = await this.discoverOperationLockService()
    if (!operationLockServices.length) {
      this.logger.warn("No operation lock services found ⚠️")
      return
    }
    this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .registerOperationLockService(
        operationLockServices[0].discoveredClass
          .instance as IOperationLockService
      )
  }

  private async registerPipelinesServices() {
    this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .registerPipelinesController(new PipelineController())
  }

  private async initializeProviders(
    app: INestApplicationContext,
    staticProviders: EntityManagerStaticProviders
  ) {
    app
      .get(ModulesContainerProvider)
      .setModulesContainer(staticProviders.modulesContainer)
  }

  private async discoverEntities() {
    return await this.discover.providersWithMetaAtKey<EntityProps>(
      EntityManagerSymbols.Entity
    )
  }

  private async discoverRepositories() {
    return await this.discover.providersWithMetaAtKey<EntityRepositoryProps>(
      EntityManagerSymbols.EntityRepository
    )
  }

  private async discoverVersioningProviders() {
    return await this.discover.providersWithMetaAtKey<EntityVersioningProviderProps>(
      EntityManagerSymbols.EntityVersioningProvider
    )
  }

  private async discoverQueryBuilders() {
    return await this.discover.providersWithMetaAtKey<EntityQueryBuilderProps>(
      EntityManagerSymbols.EntityQueryBuilder
    )
  }

  private async discoverConverters() {
    return await this.discover.providersWithMetaAtKey<EntityConverterProps>(
      EntityManagerSymbols.EntityConverter
    )
  }

  private async discoverSerializers() {
    return await this.discover.providersWithMetaAtKey<EntitySerializerProps>(
      EntityManagerSymbols.EntitySerializer
    )
  }

  private async discoverSnapshotService() {
    return await this.discover.providersWithMetaAtKey<EntitySnapshotServiceProps>(
      EntityManagerSymbols.EntitySnapshotService
    )
  }

  private async discoverAdapters() {
    return await this.discover.providersWithMetaAtKey<EntityAdapterProps>(
      EntityManagerSymbols.EntityAdapter
    )
  }

  private async discoverAuthMiddlewares() {
    return await this.discover.providersWithMetaAtKey<EntityAuthMiddlewareProps>(
      EntityManagerSymbols.EntityAuthMiddleware
    )
  }

  private async discoverGlobalAuthenticationMiddlewares() {
    return await this.discover.providersWithMetaAtKey<GlobalAuthenticationMiddlewareProps>(
      EntityManagerSymbols.GlobalAuthenticationMiddleware
    )
  }

  private async discoverEntityConnectors() {
    return await this.discover.providersWithMetaAtKey<EntityConnectorProps>(
      EntityManagerSymbols.EntityConnector
    )
  }

  private async discoverEntityConnectorMappers() {
    return await this.discover.providersWithMetaAtKey<EntityConnectorMapperProps>(
      EntityManagerSymbols.EntityConnectorMapper
    )
  }

  private async discoverAppInitializers() {
    return await this.discover.providersWithMetaAtKey<AppInitializerProps>(
      EntityManagerSymbols.AppInitializer
    )
  }

  private async discoverEntitySeeders() {
    return await this.discover.providersWithMetaAtKey<EntitySeederProps>(
      EntityManagerSymbols.EntitySeeder
    )
  }

  private async discoverEmailTemplates() {
    return await this.discover.providersWithMetaAtKey<EmailTemplateProps>(
      EntityManagerSymbols.EmailTemplate
    )
  }

  private async discoverEmailProviders() {
    return await this.discover.providersWithMetaAtKey<EmailTemplateProps>(
      EntityManagerSymbols.EmailProvider
    )
  }

  private async discoverEventTrackingProviders() {
    return await this.discover.providersWithMetaAtKey<EventsTrackerProps>(
      EntityManagerSymbols.EventsTracker
    )
  }

  private async discoverEmailLogger() {
    return await this.discover.providersWithMetaAtKey<EmailLoggerProps>(
      EntityManagerSymbols.EmailLogger
    )
  }

  private async discoverEmailTemplateMiddleware() {
    return await this.discover.providersWithMetaAtKey<EmailTemplateMiddlewareProps>(
      EntityManagerSymbols.EmailTemplateMiddleware
    )
  }

  private async discoverOperationLockService() {
    return await this.discover.providersWithMetaAtKey<OperationLockServiceProps>(
      EntityManagerSymbols.OperationLockService
    )
  }

  private async discoverBucketProviders() {
    return await this.discover.providersWithMetaAtKey<BucketProviderProps>(
      EntityManagerSymbols.BucketProvider
    )
  }

  private async discoverSecretsProviders() {
    return await this.discover.providersWithMetaAtKey<SecretsProviderProps>(
      EntityManagerSymbols.SecretsProvider
    )
  }

  private async discoverCacheInstances() {
    return await this.discover.providersWithMetaAtKey<CacheInstanceProps>(
      EntityManagerSymbols.CacheInstance
    )
  }

  private async discoverFileProviders() {
    return await this.discover.providersWithMetaAtKey<FileProviderProps>(
      EntityManagerSymbols.FileProvider
    )
  }

  private async discoverFilesReferenceRepositoryProviders() {
    return await this.discover.providersWithMetaAtKey<FileReferenceRepositoryProps>(
      EntityManagerSymbols.FileReferenceRepository
    )
  }

  private async discoverMediaProviders() {
    return await this.discover.providersWithMetaAtKey<FileProviderProps>(
      EntityManagerSymbols.MediaProvider
    )
  }

  private async discoverMediaReferenceRepositories() {
    return await this.discover.providersWithMetaAtKey<FileReferenceRepositoryProps>(
      EntityManagerSymbols.MediaReferenceRepository
    )
  }

  private async discoverMediaFolderRepositories() {
    return await this.discover.providersWithMetaAtKey<FileReferenceRepositoryProps>(
      EntityManagerSymbols.MediaFolderRepository
    )
  }
}
