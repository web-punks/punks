import {
  IEntitiesSearchQuery,
  IEntityActions,
  IEntityFacets,
  IEntitySearchParameters,
  SortingType,
} from "../../../abstractions"
import { IEntitiesDeleteParameters } from "../../../abstractions/commands"
import { EntityServiceLocator } from "../../../providers/services"
import { EntityManagerRegistry } from "../ioc"

export abstract class NestEntityActions<
  TEntity,
  TEntityId,
  TEntityCreateDto,
  TEntityUpdateDto,
  TEntityDto,
  TEntityListItemDto,
  TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets
> {
  protected readonly services: EntityServiceLocator<TEntity, unknown>
  private actionsInstance: IEntityActions<
    TEntity,
    TEntityId,
    TEntityCreateDto,
    TEntityUpdateDto,
    TEntityDto,
    TEntityListItemDto,
    TEntitiesDeleteParameters,
    TEntitySearchParameters,
    TSorting,
    TCursor,
    TFacets
  >

  // todo: resolve entity name from decorator
  constructor(entityName: string, registry: EntityManagerRegistry) {
    this.services = registry.resolveEntityServicesCollection(entityName)
  }

  protected get converter() {
    return this.services.resolveConverter<
      TEntityDto,
      TEntityListItemDto,
      TEntityCreateDto,
      TEntityUpdateDto
    >()
  }

  get manager() {
    if (!this.actionsInstance) {
      this.actionsInstance =
        this.services.resolveEntityActions() as IEntityActions<
          TEntity,
          TEntityId,
          TEntityCreateDto,
          TEntityUpdateDto,
          TEntityDto,
          TEntityListItemDto,
          TEntitiesDeleteParameters,
          TEntitySearchParameters,
          TSorting,
          TCursor,
          TFacets
        >
    }
    return this.actionsInstance
  }
}
