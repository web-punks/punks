import {
  IEntityFacets,
  IEntityManager,
  IEntityRepository,
  IEntitySearchParameters,
  SortingType,
} from "../../../abstractions"
import { IEntitiesDeleteParameters } from "../../../abstractions/commands"
import { EntityServiceLocator } from "../../../providers/services"
import { EntityManagerRegistry } from "../ioc/registry"

export abstract class NestEntityManager<
  TEntity,
  TEntityId,
  TEntityCreateData,
  TEntityUpdateData,
  TEntitiesDeleteParameters extends IEntitiesDeleteParameters<TSorting>,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TFacets extends IEntityFacets
> {
  protected readonly services: EntityServiceLocator<TEntity, unknown>
  private managerInstance: IEntityManager<
    TEntity,
    TEntityId,
    TEntityCreateData,
    TEntityUpdateData,
    TEntitiesDeleteParameters,
    TEntitySearchParameters,
    TSorting,
    TCursor,
    TFacets
  >

  // todo: resolve entity name from decorator
  constructor(entityName: string, registry: EntityManagerRegistry) {
    this.services = registry.resolveEntityServicesCollection(entityName)
  }

  get manager() {
    if (!this.managerInstance) {
      this.managerInstance =
        this.services.resolveEntityManager() as IEntityManager<
          TEntity,
          TEntityId,
          TEntityCreateData,
          TEntityUpdateData,
          TEntitiesDeleteParameters,
          TEntitySearchParameters,
          TSorting,
          TCursor,
          TFacets
        >
    }
    return this.managerInstance
  }

  getRepository<
    TRepository extends IEntityRepository<
      TEntity,
      TEntityId,
      unknown,
      unknown,
      unknown,
      unknown
    >
  >() {
    return this.services.resolveRepository() as TRepository
  }
}
