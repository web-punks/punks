import { EntitySnapshotService } from "../../../base/snapshot"
import { EntityServiceLocator } from "../../../providers/services"
import { EntityManagerRegistry } from "../ioc"

export abstract class NestEntitySnapshotService<
  TEntity,
  TEntityId,
  TEntityRepository,
  TEntitySnapshot
> extends EntitySnapshotService<TEntityId, TEntitySnapshot> {
  protected readonly services: EntityServiceLocator<TEntity, unknown>

  constructor(entityName: string, registry: EntityManagerRegistry) {
    super(entityName)
    this.services = registry.resolveEntityServicesCollection(entityName)
  }

  get repository() {
    return this.services.resolveRepository() as TEntityRepository
  }
}
