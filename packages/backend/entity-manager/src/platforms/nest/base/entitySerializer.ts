import {
  IEntityManager,
  IEntitySearchParameters,
  SortingType,
} from "../../../abstractions"
import {
  EntitySerializer,
  EntitySerializerOptions,
} from "../../../base/serializer"
import { EntityServiceLocator } from "../../../providers/services"
import { EntityManagerRegistry } from "../ioc"

export abstract class NestEntitySerializer<
  TEntity,
  TEntityId,
  TEntityCreateData,
  TEntityUpdateData,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    TCursor
  >,
  TSorting extends SortingType,
  TCursor,
  TSheetItem,
  TContext,
  TPayload = unknown
> extends EntitySerializer<
  TEntity,
  TEntityId,
  TEntitySearchParameters,
  TSheetItem,
  TContext,
  TPayload
> {
  constructor(
    entityName: string,
    private readonly registry: EntityManagerRegistry,
    options?: EntitySerializerOptions
  ) {
    super(
      registry.resolveEntityServicesCollection(
        entityName
      ) as EntityServiceLocator<TEntity, TEntityId>,
      options
    )
  }

  protected get manager() {
    return this.registry
      .resolveEntityServicesCollection(this.entityName)
      .resolveEntityManager() as IEntityManager<
      TEntity,
      TEntityId,
      TEntityCreateData,
      TEntityUpdateData,
      any,
      TEntitySearchParameters,
      TSorting,
      TCursor,
      any
    >
  }
}
