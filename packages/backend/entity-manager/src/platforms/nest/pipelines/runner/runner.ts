import { Injectable } from "@nestjs/common"
import { EntityManagerRegistry } from "../../ioc"
import { PipelineDefinition } from "../../../../types"

@Injectable()
export class PipelinesRunner {
  constructor(private readonly registry: EntityManagerRegistry) {}

  async invokePipeline<TPipelineInput, TPipelineOutput, TContext>(
    definition: PipelineDefinition<TPipelineInput, TPipelineOutput, TContext>,
    data: {
      input: TPipelineInput
      context: TContext
    }
  ) {
    const instance = await this.controller.createInstance(
      definition,
      data.input,
      data.context
    )
    return await instance.execute()
  }

  private get controller() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolvePipelinesController()
  }
}
