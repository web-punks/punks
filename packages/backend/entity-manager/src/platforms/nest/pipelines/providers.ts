import { PipelinesBuilder } from "./builder"
import { PipelinesRunner } from "./runner"

export const PipelineProviders = [PipelinesBuilder, PipelinesRunner]
