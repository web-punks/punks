import {
  OperationDefinition,
  PipelineDefinition,
  RollbackOperationDefinition,
} from "../../../../types"

export interface IPipelineStepOperationOptionsBuilder<
  TOperationInput,
  TOperationOutput,
  TCurrentStepInput,
  TPipelineInput,
  TContext
> {
  withRollback(
    definition: RollbackOperationDefinition<
      TOperationInput,
      TOperationOutput,
      TCurrentStepInput,
      TPipelineInput,
      TContext
    >
  ): IPipelineStepOperationOptionsBuilder<
    TOperationInput,
    TOperationOutput,
    TCurrentStepInput,
    TPipelineInput,
    TContext
  >
}

export interface IPipelineStepOperationBuilder<
  TStepInput,
  TStepOutput,
  TPipelineInput,
  TContext
> {
  addOperation(
    operation: OperationDefinition<
      TStepInput,
      TStepOutput,
      TStepInput,
      TPipelineInput,
      TContext
    >
  ): IPipelineStepOperationOptionsBuilder<
    TStepInput,
    TStepOutput,
    TStepInput,
    TPipelineInput,
    TContext
  >
}

export interface IPipelineStepBuilder<
  TStepInput,
  TStepOutput,
  TPipelineInput,
  TContext
> {
  addStep<TNextStepOutput>(
    builder: (
      step: IPipelineStepOperationBuilder<
        TStepOutput,
        TNextStepOutput,
        TPipelineInput,
        TContext
      >
    ) => void
  ): IPipelineStepBuilder<
    TStepOutput,
    TNextStepOutput,
    TPipelineInput,
    TContext
  >
  complete(): PipelineDefinition<TPipelineInput, TStepOutput, TContext>
}

export interface IPipelineTemplateBuilder<TPipelineInput, TContext> {
  addStep<TStepOutput>(
    builder: (
      step: IPipelineStepOperationBuilder<
        TPipelineInput,
        TStepOutput,
        TPipelineInput,
        TContext
      >
    ) => void
  ): IPipelineStepBuilder<TPipelineInput, TStepOutput, TPipelineInput, TContext>
}
