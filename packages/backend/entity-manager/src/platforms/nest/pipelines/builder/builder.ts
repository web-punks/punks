import { Injectable } from "@nestjs/common"
import { EntityManagerRegistry } from "../../ioc"
import { IPipelineTemplateBuilder } from "./types"
import { PipelineTemplateBuilder } from "./concrete"

@Injectable()
export class PipelinesBuilder {
  constructor(private readonly registry: EntityManagerRegistry) {}

  createTemplate<TInput, TContext>(): IPipelineTemplateBuilder<
    TInput,
    TContext
  > {
    return new PipelineTemplateBuilder()
  }
}
