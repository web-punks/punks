import {
  OperationDefinition,
  PipelineStep,
  PipelineStepOperation,
  PipelineDefinition,
  RollbackOperationDefinition,
} from "../../../../types"
import {
  IPipelineStepBuilder,
  IPipelineStepOperationBuilder,
  IPipelineStepOperationOptionsBuilder,
  IPipelineTemplateBuilder,
} from "./types"

class PipelineStepOperationOptionsBuilder<
  TOperationInput,
  TOperationOutput,
  TCurrentStepInput,
  TPipelineInput,
  TContext
> implements
    IPipelineStepOperationOptionsBuilder<
      TOperationInput,
      TOperationOutput,
      TCurrentStepInput,
      TPipelineInput,
      TContext
    >
{
  private readonly rollbackOperations: RollbackOperationDefinition<
    TOperationInput,
    TOperationOutput,
    TCurrentStepInput,
    TPipelineInput,
    TContext
  >[] = []

  constructor(
    private readonly operation: OperationDefinition<
      TOperationInput,
      TOperationOutput,
      TCurrentStepInput,
      TPipelineInput,
      TContext
    >
  ) {}

  withRollback(
    definition: RollbackOperationDefinition<
      TOperationInput,
      TOperationOutput,
      TCurrentStepInput,
      TPipelineInput,
      TContext
    >
  ): IPipelineStepOperationOptionsBuilder<
    TOperationInput,
    TOperationOutput,
    TCurrentStepInput,
    TPipelineInput,
    TContext
  > {
    this.rollbackOperations.push(definition)
    return this
  }

  build(): PipelineStepOperation<
    TOperationInput,
    TOperationOutput,
    TCurrentStepInput,
    TPipelineInput,
    TContext
  > {
    return {
      key: this.operation.key,
      name: this.operation.name,
      operation: this.operation,
      rollbackOperations: this.rollbackOperations,
    }
  }
}

class PipelineStepOperationsBuilder<
  TStepInput,
  TStepOutput,
  TPipelineInput,
  TContext
> implements
    IPipelineStepOperationBuilder<
      TStepInput,
      TStepOutput,
      TPipelineInput,
      TContext
    >
{
  private readonly operationBuilders: PipelineStepOperationOptionsBuilder<
    TStepInput,
    TStepOutput,
    TStepInput,
    TPipelineInput,
    TContext
  >[] = []

  addOperation(
    operation: OperationDefinition<
      TStepInput,
      TStepOutput,
      TStepInput,
      TPipelineInput,
      TContext
    >
  ): IPipelineStepOperationOptionsBuilder<
    TStepInput,
    TStepOutput,
    TStepInput,
    TPipelineInput,
    TContext
  > {
    const operationBuilder = new PipelineStepOperationOptionsBuilder(operation)
    this.operationBuilders.push(operationBuilder)
    return operationBuilder
  }

  buildOperations(): PipelineStepOperation<
    TStepInput,
    TStepOutput,
    TStepInput,
    TPipelineInput,
    TContext
  >[] {
    return this.operationBuilders.map((builder) => builder.build())
  }
}

class PipelineStepBuilder<TStepInput, TStepOutput, TPipelineInput, TContext>
  implements
    IPipelineStepBuilder<TStepInput, TStepOutput, TPipelineInput, TContext>
{
  private readonly currentStep: PipelineStep<
    TStepInput,
    TStepOutput,
    TPipelineInput,
    TContext
  >

  constructor(
    private readonly previousSteps: PipelineStep<
      unknown,
      unknown,
      TPipelineInput,
      TContext
    >[],
    currentStepOperationsBuilder: (
      step: IPipelineStepOperationBuilder<
        TStepInput,
        TStepOutput,
        TPipelineInput,
        TContext
      >
    ) => void
  ) {
    this.currentStep = this.buildCurrentStep(currentStepOperationsBuilder)
  }

  addStep<TNextStepOutput>(
    builder: (
      step: IPipelineStepOperationBuilder<
        TStepOutput,
        TNextStepOutput,
        TPipelineInput,
        TContext
      >
    ) => void
  ): IPipelineStepBuilder<
    TStepOutput,
    TNextStepOutput,
    TPipelineInput,
    TContext
  > {
    return new PipelineStepBuilder<
      TStepOutput,
      TNextStepOutput,
      TPipelineInput,
      TContext
    >(this.currentSteps, builder)
  }

  buildCurrentStep(
    currentStepOperationsBuilder: (
      step: IPipelineStepOperationBuilder<
        TStepInput,
        TStepOutput,
        TPipelineInput,
        TContext
      >
    ) => void
  ): PipelineStep<TStepInput, TStepOutput, TPipelineInput, TContext> {
    const operationsBuilder = new PipelineStepOperationsBuilder<
      TStepInput,
      TStepOutput,
      TPipelineInput,
      TContext
    >()
    currentStepOperationsBuilder(operationsBuilder)
    return {
      name: `Step ${this.previousSteps.length + 1}`,
      operations: operationsBuilder.buildOperations(),
      // TODO: add support for multiple operation outputs
      outputsReducer: (outputs) => {
        if (outputs.length === 0) {
          throw new Error("Empty pipeline step found")
        }
        if (outputs.length !== 1) {
          throw new Error("Multiple outputs are not supported yet")
        }
        return outputs[0] as TStepOutput
      },
    }
  }

  complete(): PipelineDefinition<TPipelineInput, TStepOutput, TContext> {
    return {
      steps: this.currentSteps,
    }
  }

  private get currentSteps(): PipelineStep<
    unknown,
    unknown,
    TPipelineInput,
    TContext
  >[] {
    return [
      ...this.previousSteps,
      this.currentStep as PipelineStep<
        unknown,
        unknown,
        TPipelineInput,
        TContext
      >,
    ]
  }
}

export class PipelineTemplateBuilder<TPipelineInput, TContext>
  implements IPipelineTemplateBuilder<TPipelineInput, TContext>
{
  addStep<TStepInput, TStepOutput>(
    builder: (
      step: IPipelineStepOperationBuilder<
        TStepInput,
        TStepOutput,
        TPipelineInput,
        TContext
      >
    ) => void
  ): IPipelineStepBuilder<TStepInput, TStepOutput, TPipelineInput, TContext> {
    return new PipelineStepBuilder<
      TStepInput,
      TStepOutput,
      TPipelineInput,
      TContext
    >([], builder)
  }
}
