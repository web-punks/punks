import { PipelineCompletedStepState } from "../../../../types"

const buildCompletedStepsSequence = <TPipelineInput, TContext>(
  step: PipelineCompletedStepState<TPipelineInput, TContext, unknown, unknown>
) => {
  const steps: PipelineCompletedStepState<
    TPipelineInput,
    TContext,
    unknown,
    unknown
  >[] = []

  let currentStep:
    | PipelineCompletedStepState<TPipelineInput, TContext, unknown, unknown>
    | undefined = step

  while (currentStep) {
    steps.push(currentStep)
    currentStep = currentStep.previousStep
  }

  return steps.reverse()
}

export class PipelineUtils<TPipelineInput, TPipelineOutput, TContext> {
  getStepByKey = (
    state: PipelineCompletedStepState<
      TPipelineInput,
      TContext,
      unknown,
      unknown
    >,
    key: string
  ) => {
    const sequence = buildCompletedStepsSequence(state)
    const matchingStep = sequence.find((x) => x.reference.key === key)
    if (!matchingStep) {
      throw new Error(`Step key ${key} not found`)
    }
    return matchingStep
  }

  getStepInputByKey = <TStepInput>(
    state: PipelineCompletedStepState<
      TPipelineInput,
      TContext,
      unknown,
      unknown
    >,
    key: string
  ) => {
    return this.getStepByKey(state, key).stepInput as TStepInput
  }

  getStepOutputByKey = <TStepOutput>(
    state: PipelineCompletedStepState<
      TPipelineInput,
      TContext,
      unknown,
      unknown
    >,
    key: string
  ) => {
    return this.getStepByKey(state, key).stepOutput as TStepOutput
  }

  getStep = (
    state: PipelineCompletedStepState<
      TPipelineInput,
      TContext,
      unknown,
      unknown
    >,
    index: number
  ) => {
    const sequence = buildCompletedStepsSequence(state)
    if (index >= sequence.length) {
      throw new Error(`Step index ${index} is out of range`)
    }
    return sequence[index]
  }

  getStepInput = <TStepInput>(
    state: PipelineCompletedStepState<
      TPipelineInput,
      TContext,
      unknown,
      unknown
    >,
    index: number
  ) => {
    return this.getStep(state, index).stepInput as TStepInput
  }

  getStepOutput = <TStepOutput>(
    state: PipelineCompletedStepState<
      TPipelineInput,
      TContext,
      unknown,
      unknown
    >,
    index: number
  ) => {
    return this.getStep(state, index).stepOutput as TStepOutput
  }
}
