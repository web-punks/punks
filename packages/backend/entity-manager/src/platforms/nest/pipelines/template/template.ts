import {
  PipelineDefinition,
  PipelineErrorType,
  PipelineResult,
} from "../../../../types"
import { PipelineTemplateBuilder } from "../builder/concrete"
import { IPipelineTemplateBuilder } from "../builder/types"
import { getIoCContext } from "../../ioc/storage"
import { PipelineInvocationError } from "./errors"
import { PipelineUtils } from "./utils"
import { EntityManagerSymbols, PipelineTemplateProps } from "../../decorators"
import { getInstanceDecoratorData } from "../../../../reflection/decorators"
import { Log, newUuid } from "@punks/backend-core"
import { PipelineConcurrency } from "../../../../abstractions/pipelines"

export type PipelineTemplateOptions = {
  concurrency?: PipelineConcurrency
  logging?: {
    ignoreMeta?: boolean
  }
}

export abstract class NestPipelineTemplate<
  TPipelineInput,
  TPipelineOutput,
  TContext
> {
  constructor(private readonly options?: PipelineTemplateOptions) {}

  protected readonly logger = Log.getLogger(NestPipelineTemplate.name)

  protected readonly utils = new PipelineUtils<
    TPipelineInput,
    TPipelineOutput,
    TContext
  >()

  private cachedDefinition: PipelineDefinition<
    TPipelineInput,
    TPipelineOutput,
    TContext
  >

  protected abstract buildTemplate(
    builder: IPipelineTemplateBuilder<TPipelineInput, TContext>
  ): PipelineDefinition<TPipelineInput, TPipelineOutput, TContext>

  protected isAuthorized(context: TContext) {
    return true
  }

  protected getConcurrencyKey(input: TPipelineInput): string {
    return this.constructor.name
  }

  async invoke(input: TPipelineInput): Promise<TPipelineOutput> {
    const result = await this.execute({
      input,
      context: await this.getContext(),
    })

    if (result.type !== "success") {
      throw new PipelineInvocationError(
        result.errorType,
        result.exception,
        result
      )
    }

    return result.output
  }

  async execute(data: {
    input: TPipelineInput
    context: TContext
  }): Promise<PipelineResult<TPipelineInput, TPipelineOutput>> {
    if (this.concurrency === "sequential") {
      return await this.operationsLockService.executeSequential({
        lockUid: this.getConcurrencyKey(data.input),
        operation: async () => {
          return await this.pipelineExecute(data)
        },
      })
    }

    if (this.concurrency === "exclusive") {
      const result = await this.operationsLockService.executeExclusive({
        lockUid: this.getConcurrencyKey(data.input),
        operation: async () => {
          return await this.pipelineExecute(data)
        },
      })
      return result.result!
    }

    return await this.pipelineExecute(data)
  }

  private async pipelineExecute(data: {
    input: TPipelineInput
    context: TContext
  }): Promise<PipelineResult<TPipelineInput, TPipelineOutput>> {
    const instanceId = newUuid()
    const logMetadata = {
      input: data.input,
      context: data.context,
      instanceId,
    }

    this.logger.debug(
      `[START] | ${this.metadata.name}`,
      this.processLogMetadata(logMetadata)
    )

    if (!this.isAuthorized(data.context)) {
      this.logger.debug(
        `[UNAUTHORIZED] | ${this.metadata.name} -> Unauthorized`,
        this.processLogMetadata(logMetadata)
      )
      return {
        type: "error",
        errorType: PipelineErrorType.Unauthorized,
        input: data.input,
        stepResults: [],
      }
    }

    const instance = await this.controller.createInstance(
      this.getDefinition(),
      data.input,
      data.context
    )
    const result = await instance.execute()

    if (result.type === "error") {
      this.logger.error(
        `[ERROR] | ${this.metadata.name} -> ${
          result.exception
            ? `exception: ${result.exception.name}\n${result.exception.stack}`
            : ""
        }`,
        this.processLogMetadata({
          ...logMetadata,
          result,
        })
      )
    } else {
      this.logger.debug(
        `[COMPLETED] | ${this.metadata.name}`,
        this.processLogMetadata({
          ...logMetadata,
          result,
        })
      )
    }

    return result
  }

  private processLogMetadata(meta: any) {
    if (this.metadata.options?.logging?.ignoreMeta) {
      return undefined
    }
    return meta
  }

  private getDefinition() {
    if (!this.cachedDefinition) {
      this.cachedDefinition = this.buildDefinition()
    }
    return this.cachedDefinition
  }

  private buildDefinition(): PipelineDefinition<
    TPipelineInput,
    TPipelineOutput,
    TContext
  > {
    return this.buildTemplate(new PipelineTemplateBuilder())
  }

  private async getContext() {
    const contextService = this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveAuthenticationContextProvider()
    return (await contextService?.getContext()) as TContext
  }

  private get concurrency(): PipelineConcurrency | undefined {
    return this.options?.concurrency ?? this.metadata.concurrency
  }

  private get operationsLockService() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveOperationLockService()
  }

  private get controller() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolvePipelinesController()
  }

  private get registry() {
    return getIoCContext().registry
  }

  protected get metadata(): PipelineTemplateProps {
    return getInstanceDecoratorData<PipelineTemplateProps>(
      EntityManagerSymbols.PipelineTemplate,
      this
    )
  }
}
