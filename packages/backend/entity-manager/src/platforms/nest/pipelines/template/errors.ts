import { PipelineErrorType, PipelineResult } from "../../../../types"

export class PipelineInvocationError extends Error {
  constructor(
    public readonly errorType: PipelineErrorType,
    public readonly innerError: Error | undefined,
    public readonly rawResult: PipelineResult<unknown, unknown>,
    message?: string
  ) {
    super(message ?? innerError?.message)
  }
}
