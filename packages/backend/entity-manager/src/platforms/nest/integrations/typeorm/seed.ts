import { ObjectLiteral, Repository } from "typeorm"
import { TypeOrmEntitySeeder } from "../../../../integrations/repository/typeorm/seed"
import { getInstanceDecoratorData } from "../../../../reflection/decorators"
import { EntityManagerSymbols, EntitySeederProps } from "../../decorators"
import { EntityManagerRegistry } from "../../ioc"
import { TypeOrmRepository } from "../../../../integrations"

const resolveTypeormRepository = <TEntity extends ObjectLiteral>(
  entityName: string,
  registry: EntityManagerRegistry
) => {
  const repository = registry
    .resolveEntityServicesCollection(entityName)
    .resolveRepository<
      TEntity,
      unknown,
      unknown,
      unknown
    >() as TypeOrmRepository<TEntity, unknown>
  return repository.getInnerRepository()
}

export abstract class NestTypeOrmEntitySeeder<
  TEntity extends ObjectLiteral
> extends TypeOrmEntitySeeder<TEntity> {
  protected readonly metadata: EntitySeederProps

  constructor(private readonly registry: EntityManagerRegistry) {
    super()
    this.metadata = getInstanceDecoratorData<EntitySeederProps>(
      EntityManagerSymbols.EntitySeeder,
      this
    )
  }

  get priority(): number | undefined {
    return this.metadata.priority
  }

  protected getRepository(): Repository<TEntity> {
    return resolveTypeormRepository(this.metadata.entityName, this.registry)
  }
}
