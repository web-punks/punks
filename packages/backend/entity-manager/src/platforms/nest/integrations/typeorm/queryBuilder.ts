import { ObjectLiteral } from "typeorm"
import { TypeOrmQueryBuilder } from "../../../../integrations"
import {
  IEntityFacets,
  IEntitySearchParameters,
  SortingType,
} from "../../../../abstractions"
import { EntityManagerRegistry } from "../../ioc/registry"

export abstract class NestTypeOrmQueryBuilder<
  TEntity extends ObjectLiteral,
  TEntityId,
  TEntitySearchParameters extends IEntitySearchParameters<
    TEntity,
    TSorting,
    number
  >,
  TSorting extends SortingType,
  TFacets extends IEntityFacets,
  TUserContext
> extends TypeOrmQueryBuilder<
  TEntity,
  TEntityId,
  TEntitySearchParameters,
  TSorting,
  TFacets,
  TUserContext
> {
  constructor(entityName: string, registry: EntityManagerRegistry) {
    // todo: discover entityName from decorator
    super(registry.resolveEntityServicesCollection(entityName))
  }
}
