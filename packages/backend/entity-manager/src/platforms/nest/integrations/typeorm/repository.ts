import { ObjectLiteral, Repository } from "typeorm"
import { TypeOrmRepository } from "../../../../integrations"

export class NestTypeOrmRepository<
  TEntity extends ObjectLiteral,
  TEntityId
> extends TypeOrmRepository<TEntity, TEntityId> {
  constructor(repository: Repository<TEntity>) {
    super(repository)
  }
}
