import type { Response } from "express"
import { StreamableFile } from "@nestjs/common"

export const createExpressFileResponse = (
  res: Response,
  file: {
    content: Buffer
    contentType: string
    name: string
  }
) => {
  res.set({
    "Content-Type": file.contentType,
    "Content-Disposition": `attachment; filename="${file.name}"`,
  })
  return new StreamableFile(file.content)
}
