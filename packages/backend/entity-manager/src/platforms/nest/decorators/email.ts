import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EmailTemplateProps {
  templateId: string
}

export const WpEmailTemplate = (templateId: string) =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, EmailTemplateProps>(EntityManagerSymbols.EmailTemplate, {
      templateId,
    })
  )

export interface EmailProviderProps {
  providerId: string
}

export const WpEmailProvider = (providerId: string) =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, EmailProviderProps>(EntityManagerSymbols.EmailProvider, {
      providerId,
    })
  )

export interface EmailLoggerProps {}

export const WpEmailLogger = () =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, EmailLoggerProps>(EntityManagerSymbols.EmailLogger, {})
  )

export interface EmailTemplateMiddlewareProps {}

export const WpEmailTemplateMiddleware = () =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, EmailTemplateMiddlewareProps>(
      EntityManagerSymbols.EmailTemplateMiddleware,
      {}
    )
  )
