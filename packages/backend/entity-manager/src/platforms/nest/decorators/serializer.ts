import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntitySerializerProps {
  entityName: string
}

export const WpEntitySerializer = (
  entityName: string,
  props: Omit<EntitySerializerProps, "entityName"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntitySerializer, {
      entityName,
      ...props,
    })
  )
