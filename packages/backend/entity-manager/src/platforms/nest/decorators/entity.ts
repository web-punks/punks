import { SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export type EntityVersioningOptions = {
  enabled: boolean
}

export interface EntityProps {
  name: string
  description?: string
  versioning?: EntityVersioningOptions
}

export const WpEntity = (name: string, props: Omit<EntityProps, "name"> = {}) =>
  applyDecorators(
    SetMetadata(EntityManagerSymbols.Entity, {
      name,
      ...props,
    })
  )
