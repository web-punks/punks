import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export type OperationLockServiceProps = {}

export const WpOperationLockService = (
  props: Omit<OperationLockServiceProps, "name"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.OperationLockService, {
      ...props,
    })
  )
