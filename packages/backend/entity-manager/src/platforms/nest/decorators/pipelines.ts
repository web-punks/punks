import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"
import { PipelineConcurrency } from "../../../abstractions/pipelines"

export type PipelineTemplateOptions = {
  logging?: {
    enabled?: boolean
    ignoreMeta?: boolean
  }
}

export type PipelineTemplateProps = {
  name: string
  description?: string
  concurrency?: PipelineConcurrency
  options?: PipelineTemplateOptions
}

export const WpPipeline = (
  name: string,
  props: Omit<PipelineTemplateProps, "name"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.PipelineTemplate, {
      name,
      ...props,
    })
  )
