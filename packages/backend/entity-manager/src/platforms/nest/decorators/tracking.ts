import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EventsTrackerProps {
  disabled?: boolean
}

export const WpEventsTracker = (props: EventsTrackerProps = {}) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EventsTracker, {
      ...props,
    })
  )
