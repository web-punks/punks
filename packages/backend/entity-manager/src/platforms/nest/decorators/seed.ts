import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntitySeederProps {
  entityName: string
  priority?: number
}

export const WpEntitySeeder = (
  entityName: string,
  props: Omit<EntitySeederProps, "entityName"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntitySeeder, {
      entityName,
      ...props,
    })
  )
