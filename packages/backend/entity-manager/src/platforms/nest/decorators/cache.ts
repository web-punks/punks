import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface CacheInstanceProps {
  instanceName: string
}

export const WpCacheInstance = (instanceName: string) =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, CacheInstanceProps>(EntityManagerSymbols.CacheInstance, {
      instanceName,
    })
  )
