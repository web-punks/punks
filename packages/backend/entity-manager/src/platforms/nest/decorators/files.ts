import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface FileProviderProps {
  providerId: string
}

export const WpFileProvider = (providerId: string) =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, FileProviderProps>(EntityManagerSymbols.FileProvider, {
      providerId,
    })
  )

export interface FileReferenceRepositoryProps {}

export const WpFileReferenceRepository = () =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, FileReferenceRepositoryProps>(
      EntityManagerSymbols.FileReferenceRepository,
      true
    )
  )
