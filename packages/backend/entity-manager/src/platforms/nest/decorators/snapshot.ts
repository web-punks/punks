import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntitySnapshotServiceProps {
  entityName: string
}

export const WpEntitySnapshotService = (
  entityName: string,
  props: Omit<EntitySnapshotServiceProps, "entityName"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntitySnapshotService, {
      entityName,
      ...props,
    })
  )
