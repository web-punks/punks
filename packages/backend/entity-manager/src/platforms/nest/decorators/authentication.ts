import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface GlobalAuthenticationMiddlewareProps {
  name: string
  weight?: number
}

export const WpGlobalAuthenticationMiddleware = (
  name: string,
  props: Omit<GlobalAuthenticationMiddlewareProps, "name"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.GlobalAuthenticationMiddleware, {
      name,
      ...props,
    })
  )
