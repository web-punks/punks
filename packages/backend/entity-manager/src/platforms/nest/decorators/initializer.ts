import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface AppInitializerProps {
  priority?: number
}

export const WpAppInitializer = (props: AppInitializerProps = {}) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.AppInitializer, props)
  )
