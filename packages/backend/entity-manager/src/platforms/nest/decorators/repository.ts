import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntityRepositoryProps {
  entityName: string
}

export const WpEntityRepository = (
  entityName: string,
  props: Omit<EntityRepositoryProps, "entityName"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntityRepository, {
      entityName,
      ...props,
    })
  )
