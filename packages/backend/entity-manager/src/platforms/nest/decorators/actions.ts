import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntityActionsProps {
  entityName: string
}

export const WpEntityActions = (
  entityName: string,
  props: Omit<EntityActionsProps, "entityName"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntityActions, {
      entityName,
      ...props,
    })
  )
