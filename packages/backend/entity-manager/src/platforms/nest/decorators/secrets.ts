import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface SecretsProviderProps {
  providerId: string
}

export const WpSecretsProvider = (providerId: string) =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, SecretsProviderProps>(
      EntityManagerSymbols.SecretsProvider,
      {
        providerId,
      }
    )
  )
