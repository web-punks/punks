import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntityManagerProps {
  entityName: string
}

export const WpEntityManager = (
  entityName: string,
  props: Omit<EntityManagerProps, "entityName"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntityManager, {
      entityName,
      ...props,
    })
  )
