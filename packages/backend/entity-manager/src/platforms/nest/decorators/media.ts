import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface MediaProviderProps {
  providerId: string
}

export const WpMediaProvider = (providerId: string) =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, MediaProviderProps>(EntityManagerSymbols.MediaProvider, {
      providerId,
    })
  )

export interface MediaReferenceRepositoryProps {}

export const WpMediaReferenceRepository = () =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, MediaReferenceRepositoryProps>(
      EntityManagerSymbols.MediaReferenceRepository,
      true
    )
  )

export interface MediaFolderRepositoryProps {}

export const WpMediaFolderRepository = () =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, MediaFolderRepositoryProps>(
      EntityManagerSymbols.MediaFolderRepository,
      true
    )
  )
