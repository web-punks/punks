import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntityQueryBuilderProps {
  entityName: string
}

export const WpEntityQueryBuilder = (
  entityName: string,
  props: Omit<EntityQueryBuilderProps, "entityName"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntityQueryBuilder, {
      entityName,
      ...props,
    })
  )
