import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntityConnectorMapperProps {
  entityName: string
  connectorName: string
}

export const WpEntityConnectorMapper = (
  entityName: string,
  props: Omit<EntityConnectorMapperProps, "entityName">
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntityConnectorMapper, {
      entityName,
      ...props,
    })
  )
