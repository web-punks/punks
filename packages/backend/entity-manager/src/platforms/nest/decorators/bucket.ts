import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface BucketProviderProps {
  providerId: string
}

export const WpBucketProvider = (providerId: string) =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, BucketProviderProps>(EntityManagerSymbols.BucketProvider, {
      providerId,
    })
  )
