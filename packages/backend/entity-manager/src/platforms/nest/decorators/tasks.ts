import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"

export const TASK_KEY = "wp_task"

export enum TaskConcurrency {
  Single = "single",
  Multiple = "multiple",
}

export type TaskSettings = {
  cronExpression?: string
  concurrency: TaskConcurrency
  description?: string
  displayName?: string
  hidden?: boolean
}

export type TaskProps = TaskSettings & {
  name: string
}

export function WpTask(name: string, settings: TaskSettings) {
  return applyDecorators(
    Injectable(),
    SetMetadata(TASK_KEY, {
      name,
      ...settings,
    } satisfies TaskProps)
  )
}
