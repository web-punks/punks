import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntityConverterProps {
  entityName: string
}

export const WpEntityConverter = (
  entityName: string,
  props: Omit<EntityConverterProps, "entityName"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntityConverter, {
      entityName,
      ...props,
    })
  )
