import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntityAuthMiddlewareProps {
  entityName: string
}

export const WpEntityAuthMiddleware = (
  entityName: string,
  props: Omit<EntityAuthMiddlewareProps, "entityName"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntityAuthMiddleware, {
      entityName,
      ...props,
    })
  )
