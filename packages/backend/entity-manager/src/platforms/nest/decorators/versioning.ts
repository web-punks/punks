import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntityVersioningProviderProps {
  disabled?: boolean
}

export const WpEntityVersioningProvider = (
  props: EntityVersioningProviderProps = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntityVersioningProvider, {
      ...props,
    })
  )
