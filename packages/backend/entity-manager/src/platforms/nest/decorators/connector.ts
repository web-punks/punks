import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"
import { ConnectorOptions } from "../../../abstractions/connectors"

export interface EntityConnectorProps extends ConnectorOptions {
  entityName: string
  connectorName: string
}

export const WpEntityConnector = (
  entityName: string,
  props: Omit<EntityConnectorProps, "entityName">
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntityConnector, {
      entityName,
      ...props,
    })
  )
