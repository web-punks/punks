import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import { EntityManagerSymbols } from "./symbols"

export interface EntityAdapterProps {
  entityName: string
}

export const WpEntityAdapter = (
  entityName: string,
  props: Omit<EntityAdapterProps, "entityName"> = {}
) =>
  applyDecorators(
    Injectable(),
    SetMetadata(EntityManagerSymbols.EntityAdapter, {
      entityName,
      ...props,
    })
  )
