import { Injectable } from "@nestjs/common"
import { EntityManagerRegistry } from "../../ioc"

@Injectable()
export class CacheService {
  constructor(private readonly registry: EntityManagerRegistry) {}

  getInstance(instanceName: string) {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveCacheInstance(instanceName)
  }

  getInstances() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveCacheInstances()
  }
}
