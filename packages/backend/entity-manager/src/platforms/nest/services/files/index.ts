import { Injectable } from "@nestjs/common"
import {
  FileData,
  FileDownloadUrl,
  FileReference,
  IFileManager,
  IFileProvider,
} from "../../../../abstractions"
import { EntityManagerRegistry } from "../../ioc"

@Injectable()
export class FilesManager implements IFileManager {
  constructor(private readonly registry: EntityManagerRegistry) {}

  async uploadFile(file: FileData): Promise<FileReference> {
    const uploadResult = await this.defaultFileProvider.uploadFile({
      fileId: file.fileId,
      content: file.content,
      contentType: file.contentType,
      fileName: file.fileName,
      filePath: file.storageFilePath,
      bucket: file.storageBucket,
    })
    const record = await this.referencesRepo.createReference({
      fileId: file.fileId,
      providerId: this.defaultFileProvider.getProviderId(),
      reference: uploadResult.reference,
      contentType: file.contentType,
      fileName: file.fileName,
      filePath: file.folderPath,
      fileSize: file.content.length,
      metadata: file.metadata,
    })
    return {
      fileId: record.fileId,
      providerId: record.providerId,
      reference: record.reference,
    }
  }

  async removeFile(fileId: string): Promise<void> {
    const ref = await this.referencesRepo.getReference(fileId)
    await this.getFileProvider(ref.providerId).deleteFile({
      reference: ref.reference,
    })
    await this.referencesRepo.deleteReference(fileId)
  }

  async getFileContent(fileId: string): Promise<FileData> {
    const ref = await this.referencesRepo.getReference(fileId)
    const content = await this.getFileProvider(ref.providerId).downloadFile({
      reference: ref.reference,
    })
    return {
      content: content.content,
      contentType: ref.contentType,
      fileName: ref.fileName,
      folderPath: ref.filePath,
      metadata: ref.metadata,
    }
  }

  async getFileDownloadUrl(fileId: string): Promise<FileDownloadUrl> {
    const ref = await this.referencesRepo.getReference(fileId)
    const downloadUrl = await this.getFileProvider(
      ref.providerId
    ).getFileProviderDownloadUrl({
      reference: ref.reference,
    })
    return {
      contentType: ref.contentType,
      fileName: ref.fileName,
      downloadUrl: downloadUrl.downloadUrl,
    }
  }

  async getFileReferenceDownloadUrl({
    reference,
    providerId,
  }: {
    reference: string
    providerId: string
  }): Promise<FileDownloadUrl> {
    const ref = await this.referencesRepo.searchReference({
      providerId,
      reference,
    })
    const downloadUrl = await this.getFileProvider(
      ref.providerId
    ).getFileProviderDownloadUrl({
      reference: ref.reference,
    })
    return {
      contentType: ref.contentType,
      fileName: ref.fileName,
      downloadUrl: downloadUrl.downloadUrl,
    }
  }

  private getFileProvider(providerId: string): IFileProvider {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveFileProvider(providerId)
  }

  private get defaultFileProvider(): IFileProvider {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveDefaultFilesProvider()
  }

  private get referencesRepo() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveFilesReferenceRepositoryProviders()
  }
}
