import { Injectable } from "@nestjs/common"
import { EntityManagerRegistry } from "../../ioc"
import { IEntityManager } from "../../../../abstractions"

@Injectable()
export class EntityManagerService {
  constructor(private readonly registry: EntityManagerRegistry) {}

  getManager<TEntity>(
    entityName: string
  ): IEntityManager<
    TEntity,
    unknown,
    unknown,
    unknown,
    any,
    any,
    any,
    unknown,
    any
  > {
    return this.registry
      .resolveEntityServicesCollection<TEntity>(entityName)
      .resolveEntityManager()
  }

  getActions<TEntity>(entityName: string) {
    return this.registry
      .resolveEntityServicesCollection<TEntity>(entityName)
      .resolveEntityActions()
  }

  getRepository<TEntity>(entityName: string) {
    return this.registry
      .resolveEntityServicesCollection<TEntity>(entityName)
      .resolveRepository()
  }
}
