import { Injectable } from "@nestjs/common"
import { EntityManagerRegistry } from "../../ioc"
import { IEventLog } from "../../../../abstractions"

@Injectable()
export class TrackingService {
  constructor(private readonly registry: EntityManagerRegistry) {}

  async trackEvent<T>(event: IEventLog<T>) {
    await this.tracker.track(event)
  }

  private get tracker() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveEventsTracker()
  }
}
