import { createDayPath, newUuid } from "@punks/backend-core"
import {
  DataExportFile,
  DataExportInput,
  DataSerializationFormat,
} from "./types"
import { EntityManagerRegistry } from "../../ioc/registry"
import { IFileProvider } from "../../../../abstractions/files"
import { DataExportSerializer } from "./serializer"
import { Injectable } from "@nestjs/common"
import { IBucketProvider } from "../../../../abstractions/buckets"

@Injectable()
export class DataExportService {
  constructor(private readonly registry: EntityManagerRegistry) {}

  async exportData<TSheetItem>(
    data: TSheetItem[],
    input: DataExportInput<TSheetItem>
  ) {
    const file = await this.extractData(data, input)
    const { downloadUrl, fileName } = await this.uploadExportFile(file, {
      dataName: input.dataName,
      fileName: input.fileName,
      format: input.format,
    })

    return {
      downloadUrl,
      file: {
        fileName,
        content: file.content,
        contentType: file.contentType,
      },
    }
  }

  async extractData<TSheetItem>(
    data: TSheetItem[],
    input: Omit<DataExportInput<TSheetItem>, "fileName">
  ): Promise<DataExportFile> {
    const content = new DataExportSerializer<TSheetItem>().serialize(data, {
      columns: input.columns,
      format: input.format,
      name: input.dataName,
    })
    return {
      content: content.content,
      contentType: content.contentType,
    }
  }

  private async uploadExportFile(
    file: DataExportFile,
    {
      dataName,
      fileName,
      format,
    }: {
      dataName: string
      fileName: string
      format: DataSerializationFormat
    }
  ) {
    const fileNameWithExtension = this.buildExportFileName(fileName, format)
    const filePath = this.buildExportFilePath(dataName, fileNameWithExtension)
    await this.defaultBucketProvider.fileUpload({
      bucket: this.defaultFileProvider.defaultBucket,
      filePath,
      content: file.content,
      contentType: file.contentType,
    })
    const downloadUrl =
      await this.defaultFileProvider.getFileProviderDownloadUrl({
        reference: filePath,
      })
    return {
      downloadUrl,
      fileName: fileNameWithExtension,
    }
  }

  private buildExportFileName(
    fileName: string,
    format: DataSerializationFormat
  ) {
    return `${fileName}.${format}`
  }

  private buildExportFilePath(dataName: string, fileName: string) {
    return `/data-export/${dataName}/${createDayPath(
      new Date()
    )}/${newUuid()}_${fileName}`
  }

  private get defaultFileProvider(): IFileProvider {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveDefaultFilesProvider()
  }

  private get defaultBucketProvider(): IBucketProvider {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveDefaultBucketProvider()
  }
}
