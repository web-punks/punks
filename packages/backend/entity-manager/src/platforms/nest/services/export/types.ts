export type DataExportInput<TSheetItem> = {
  dataName: string
  fileName: string
  columns: DataSerializerColumnDefinition<TSheetItem>[]
  format: DataSerializationFormat
}

export type DataSerializerColumnDefinition<TSheetItem> = {
  name: string
  key: string
  selector: keyof TSheetItem | ((item: TSheetItem) => any)
  colSpan?: number
  array?: boolean
  arraySeparator?: string
}

export type DataExportFile = {
  content: Buffer
  contentType: string
}

export enum DataSerializationFormat {
  Csv = "csv",
  Json = "json",
  Xlsx = "xlsx",
}
