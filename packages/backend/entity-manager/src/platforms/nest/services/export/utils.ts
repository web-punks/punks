import { DataSerializerColumnDefinition } from "./types"

export const getColumnValue = <TExportItem>(
  item: TExportItem,
  definition: DataSerializerColumnDefinition<TExportItem>
) => {
  return typeof definition.selector === "function"
    ? definition.selector(item)
    : item[definition.selector]
}

const DEFAULT_ARRAY_SEPARATOR = "|"

export const joinArrayColumn = (value: any[], separator?: string) =>
  value
    ?.map((x) => x.toString().trim())
    .filter((x) => x)
    .join(separator ?? DEFAULT_ARRAY_SEPARATOR)
