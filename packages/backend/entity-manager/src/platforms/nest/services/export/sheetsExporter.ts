import { Injectable } from "@nestjs/common"
import { DataExportFile, DataSerializerColumnDefinition } from "./types"
import { excelBuild } from "@punks/backend-core"
import { getColumnValue, joinArrayColumn } from "./utils"

export type DataSheet<T> = {
  name: string
  data: T[]
  columns: DataSerializerColumnDefinition<T>[]
}

export class DataSheetsBuilder {
  private sheets: DataSheet<any>[] = []

  addSheet<T>(sheet: DataSheet<T>) {
    this.sheets.push(sheet)
  }

  async build(): Promise<DataExportFile> {
    return {
      contentType:
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      content: Buffer.from(
        excelBuild(
          ...this.sheets.map((s) => ({
            data: s.data,
            sheetName: s.name,
            columns: s.columns.map((c) => ({
              header: c.name,
              value: (item: any) => {
                const value = getColumnValue(item, c)
                return c.array
                  ? joinArrayColumn(value, c.arraySeparator)
                  : value
              },
              headerSize: c.colSpan,
            })),
          }))
        )
      ),
    }
  }
}

@Injectable()
export class DataSheetsExporterService {
  createBuilder() {
    return new DataSheetsBuilder()
  }
}
