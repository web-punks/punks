import { csvBuild, excelBuild } from "@punks/backend-core"
import {
  DataSerializationFormat,
  DataSerializerColumnDefinition,
} from "./types"
import { getColumnValue, joinArrayColumn } from "./utils"

const DEFAULT_DELIMITER = ";"

export class DataExportSheetsSerializer {
  serializeXlsx(input: { name: string }) {}
}

export class DataExportSerializer<TExportItem> {
  serialize(
    items: TExportItem[],
    exportParams: {
      name: string
      format: DataSerializationFormat
      columns: DataSerializerColumnDefinition<TExportItem>[]
    }
  ) {
    switch (exportParams.format) {
      case DataSerializationFormat.Csv:
        return {
          contentType: "text/csv",
          content: Buffer.from(
            csvBuild(
              items,
              exportParams.columns.map((c) => ({
                name: c.name,
                value: (item: any) => {
                  const value = getColumnValue<TExportItem>(item, c)
                  return c.array
                    ? joinArrayColumn(value, c.arraySeparator)
                    : value
                },
              })),
              {
                delimiter: DEFAULT_DELIMITER,
              }
            ),
            "utf-8"
          ),
        }

      case DataSerializationFormat.Xlsx:
        return {
          contentType:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          content: Buffer.from(
            excelBuild<TExportItem>({
              data: items,
              sheetName: exportParams.name,
              columns: exportParams.columns.map((c) => ({
                header: c.name,
                value: (item: any) => {
                  const value = getColumnValue<TExportItem>(item, c)
                  return c.array
                    ? joinArrayColumn(value, c.arraySeparator)
                    : value
                },
                headerSize: c.colSpan,
              })),
            })
          ),
        }

      case DataSerializationFormat.Json:
        return {
          contentType: "application/json",
          content: Buffer.from(JSON.stringify(items), "utf-8"),
        }
    }
  }
}
