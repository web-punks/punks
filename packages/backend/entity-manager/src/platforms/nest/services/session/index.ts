import { Injectable } from "@nestjs/common"
import { sessionStorage } from "../../session/storage"
import { IAppSessionService } from "../../../../abstractions/session"

@Injectable()
export class AppSessionService implements IAppSessionService<unknown> {
  getValue<T>(key: string) {
    return this.getSession().get<T>(key)
  }

  setValue<T>(key: string, value: T) {
    this.getSession().set(key, value)
  }

  clearValue(key: string) {
    this.getSession().set(key, undefined)
  }

  getRequest() {
    return this.getSession().request
  }

  retrieveRequest() {
    return this.retrieveSession()?.request
  }

  private retrieveSession() {
    const store = sessionStorage.getStore()
    if (!store) {
      return undefined
    }
    return store
  }

  private getSession() {
    const store = sessionStorage.getStore()
    if (!store) {
      throw new Error("No active context found")
    }
    return store
  }
}
