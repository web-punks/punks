import { Injectable } from "@nestjs/common"
import { hash } from "bcrypt"

@Injectable()
export class AppHashingService {
  public async hash(value: string, salt: string): Promise<string> {
    return await hash(value, salt)
  }

  public async compare(
    hash: string,
    value: string,
    salt: string
  ): Promise<boolean> {
    return (await this.hash(value, salt)) === hash
  }
}
