import { Injectable } from "@nestjs/common"
import {
  IMediaLibraryManager,
  MediaFolderCreateInput,
  MediaFolderEnsureInput,
  MediaFolderMoveInput,
  MediaFolderReference,
  MediaFolderRenameInput,
  MediaInfo,
  MediaItemReference,
  MediaReference,
  MediaUploadInput,
} from "../../../../abstractions"
import { EntityManagerRegistry } from "../../ioc"

@Injectable()
export class MediaLibraryService implements IMediaLibraryManager {
  constructor(private readonly registry: EntityManagerRegistry) {}

  async mediaUpload(input: MediaUploadInput): Promise<MediaItemReference> {
    const reference = await this.defaultMediaProvider.mediaUpload(input)
    const referenceItem = await this.mediaReferenceRepository.createReference({
      media: reference,
      fileName: input.fileName,
      contentType: input.contentType,
      folderId: input.folderId,
      providerId: this.defaultMediaProvider.getProviderId(),
    })
    return {
      id: referenceItem.id,
      ref: referenceItem.ref,
      folderId: referenceItem.folderId,
    }
  }

  async mediaDelete(input: MediaReference): Promise<void> {
    await this.getMediaProvider(
      this.parseReferenceProviderId(input)
    ).mediaDelete(input)
    await this.mediaReferenceRepository.deleteReference(input)
  }

  async mediaDownload(input: MediaReference): Promise<Buffer> {
    return await this.getMediaProvider(
      this.parseReferenceProviderId(input)
    ).mediaDownload(input)
  }

  async folderEnsure(
    input: MediaFolderEnsureInput
  ): Promise<MediaFolderReference> {
    let parentFolder: MediaFolderReference | undefined
    let folder: MediaFolderReference | undefined

    for (const folderName of input.path) {
      folder = await this.mediaFolderRepository.folderFind(
        folderName,
        parentFolder?.id
      )
      if (!folder) {
        folder = await this.mediaFolderRepository.folderCreate({
          name: folderName,
          parentId: parentFolder?.id,
          organizationId: input.organizationId,
        })
      }
      parentFolder = folder
    }

    if (!folder) {
      throw new Error("Cannot create folder")
    }

    return folder
  }

  async folderCreate(
    input: MediaFolderCreateInput
  ): Promise<MediaFolderReference> {
    return await this.mediaFolderRepository.folderCreate({
      name: input.folderName,
      parentId: input.parentId,
      organizationId: input.organizationId,
    })
  }

  async folderMove(input: MediaFolderMoveInput): Promise<void> {
    await this.mediaFolderRepository.folderMove(input.id, input.folderId)
  }

  async folderRename(input: MediaFolderRenameInput): Promise<void> {
    await this.mediaFolderRepository.folderRename(
      input.id,
      input.folderName ?? ""
    )
  }

  async folderDelete(input: MediaFolderReference): Promise<void> {
    const folderFiles = await this.mediaReferenceRepository.getFolderReferences(
      input.id
    )
    if (folderFiles.length > 0) {
      throw new Error("Cannot delete folder with files inside")
    }

    const subfolders = await this.mediaFolderRepository.foldersList(input.id)
    if (subfolders.length > 0) {
      throw new Error("Cannot delete folder with subfolders inside")
    }
    await this.mediaFolderRepository.folderDelete(input.id)
  }

  async foldersList(
    parent?: MediaFolderReference | undefined
  ): Promise<MediaFolderReference[]> {
    const subfolders = await this.mediaFolderRepository.foldersList(parent?.id)
    return subfolders.map((f) => ({
      id: f.id,
      name: f.name,
    }))
  }

  async getFolderMedia(input: MediaFolderReference): Promise<MediaInfo[]> {
    const folderFiles = await this.mediaReferenceRepository.getFolderReferences(
      input.id
    )
    return folderFiles.map((f) => ({
      ref: f.ref,
      fileName: this.getMediaProvider(
        this.parseReferenceProviderId(f)
      ).getFileName(f),
    }))
  }

  private parseReferenceProviderId(ref: MediaReference) {
    return ref.ref.split(":")[0]
  }

  private getMediaProvider(providerId: string) {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveMediaProvider(providerId)
  }

  private get defaultMediaProvider() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveDefaultMediaProvider()
  }

  private get mediaFolderRepository() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveMediaFolderRepository()
  }

  private get mediaReferenceRepository() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveMediaReferenceRepository()
  }
}
