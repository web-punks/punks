import { Injectable } from "@nestjs/common"
import { EntityManagerRegistry } from "../../ioc"
import {
  EmailSendOptions,
  HtmlEmailInput,
  TemplatedEmailInput,
} from "../../../../abstractions"
import { normalizeAddresses } from "./utils"

@Injectable()
export class EmailService {
  constructor(private readonly registry: EntityManagerRegistry) {}

  async sendTemplatedEmail<TPayload>(
    input: TemplatedEmailInput<TPayload>,
    options?: EmailSendOptions
  ): Promise<void> {
    const template = this.getTemplate(input.templateId)
    const transformedPayload =
      (await this.templateMiddleware?.processPayload(input.payload)) ??
      input.payload

    const sendInput = {
      ...input,
      payload: transformedPayload,
    }
    const transformedInput =
      (await this.templateMiddleware?.processInput(sendInput)) ?? sendInput

    const normalizedInput = {
      ...transformedInput,
      ...normalizeAddresses(transformedInput),
    }

    if (this.hasRecipients(normalizedInput)) {
      await this.provider.sendTemplatedEmail(normalizedInput, template, options)
    }

    await this.logger.logTemplatedEmail(normalizedInput)
  }

  async sendHtmlEmail<TPayload>(
    input: HtmlEmailInput<TPayload>,
    options?: EmailSendOptions
  ): Promise<void> {
    if (this.hasRecipients(input)) {
      await this.provider.sendHtmlEmail(input, options)
    }

    await this.logger.logHtmlEmail(input)
  }

  private getTemplate(templateId: string) {
    const template = this.templates.getTemplate(templateId)
    if (!template) {
      throw new Error(`Email template ${templateId} not found`)
    }
    return template
  }

  private hasRecipients(
    input: Pick<TemplatedEmailInput<unknown>, "to" | "cc" | "bcc">
  ) {
    return input.to?.length || input.cc?.length || input.bcc?.length
  }

  private get templateMiddleware() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveEmailTemplateMiddleware()
  }

  private get templates() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveEmailTemplatesCollection()
  }

  private get provider() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveEmailProvider()
  }

  private get logger() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveEmailLogger()
  }
}
