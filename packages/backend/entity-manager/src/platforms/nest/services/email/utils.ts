import { distinct } from "@punks/backend-core"
import { TemplatedEmailInput } from "../../../../abstractions"

const normalizeAddress = (value?: string) => {
  return value?.trim() ? value?.trim() : undefined
}

const normalizeAddressField = (
  values: string[] | undefined,
  invalidValues?: string[] | undefined
): string[] | undefined => {
  return distinct(
    (values
      ?.map(normalizeAddress)
      ?.filter(
        (value) =>
          value && !invalidValues?.map(normalizeAddress).includes(value)
      ) as string[]) ?? []
  )
}

export const normalizeAddresses = <TPayload>(
  input: TemplatedEmailInput<TPayload>
): Partial<TemplatedEmailInput<TPayload>> => {
  return {
    to: normalizeAddressField(input.to, []),
    cc: normalizeAddressField(input.cc, [...(input.to ?? [])]),
    bcc: normalizeAddressField(input.bcc, [
      ...(input.to ?? []),
      ...(input.cc ?? []),
    ]),
  }
}
