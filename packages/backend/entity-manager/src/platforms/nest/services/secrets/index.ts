import { Injectable } from "@nestjs/common"
import { EntityManagerRegistry } from "../../ioc"

@Injectable()
export class SecretsService {
  constructor(private readonly registry: EntityManagerRegistry) {}

  get secretsProvider() {
    return this.providers[0]
  }

  private get providers() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveSecretsProviders()
  }
}
