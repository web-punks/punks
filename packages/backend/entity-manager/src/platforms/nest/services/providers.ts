import { CacheService } from "./cache"
import { EmailService } from "./email"
import { EventsService } from "./events"
import { DataExportService, DataSheetsExporterService } from "./export"
import { FilesManager } from "./files"
import { AppHashingService } from "./hashing"
import { EntityManagerService } from "./manager"
import { MediaLibraryService } from "./media"
import { OperationLockService } from "./operations/operation-lock.service"
import { SecretsService } from "./secrets"
import { AppSessionService } from "./session"
import { TrackingService } from "./tracking"

export const Services = [
  AppSessionService,
  AppHashingService,
  CacheService,
  DataExportService,
  DataSheetsExporterService,
  EntityManagerService,
  EmailService,
  EventsService,
  FilesManager,
  MediaLibraryService,
  OperationLockService,
  SecretsService,
  TrackingService,
]
