import { Injectable } from "@nestjs/common"
import { EntityManagerRegistry } from "../../ioc"
import { IEventLog } from "../../../../abstractions"

export type IEventData<TEventPayload> = {
  payload?: TEventPayload
  userId?: string
  userName?: string
  timestamp?: Date
}

@Injectable()
export class EventsService {
  constructor(private readonly registry: EntityManagerRegistry) {}

  async emitEvent<TEventPayload>(
    eventName: string,
    eventData: IEventData<TEventPayload>
  ) {
    await this.eventEmitter.emit(
      eventName,
      this.buildPayload(eventName, eventData)
    )
  }

  private buildPayload<TEventPayload>(
    eventName: string,
    eventData: IEventData<TEventPayload>
  ): IEventLog<TEventPayload> {
    return {
      ...eventData,
      type: eventName,
      timestamp: eventData.timestamp ?? new Date(),
    }
  }

  private get eventEmitter() {
    return this.registry
      .getContainer()
      .getEntitiesServicesLocator()
      .resolveEventEmitter()
  }
}
