import { Test, TestingModule } from "@nestjs/testing"
import { OperationLockService } from "./operation-lock.service"
import { ILockRepository } from "../../../../abstractions"
import { mockPostgresDatabase } from "../../../../__test__/providers/typeorm/mock"
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  Repository,
  UpdateDateColumn,
} from "typeorm"
import { InjectRepository, getRepositoryToken } from "@nestjs/typeorm"
import { Injectable } from "@nestjs/common"
import { TypeormOperationLockRepository } from "./operation-lock.repository"
import { getEntityManagerProviderToken } from "../../ioc/tokens/symbols"
import { sleep } from "@punks/backend-core"

const mockResult = () => ({
  field1: "value1",
  field2: "value2",
})

const mockOtherResult = () => ({
  field1: "value1b",
  field2: "value2b",
})

@Entity("operationLockItem")
class OperationLockItemEntry {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255, unique: true })
  uid: string

  @Column({ type: "varchar", length: 255, nullable: true })
  lockedBy?: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}

@Injectable()
class OperationLockItemRepository extends TypeormOperationLockRepository<OperationLockItemEntry> {
  constructor(
    @InjectRepository(OperationLockItemEntry, "core")
    repo: Repository<OperationLockItemEntry>
  ) {
    super(repo, OperationLockItemEntry)
  }
}

describe("OperationLockService.executeSequential", () => {
  let service: OperationLockService
  let repo: ILockRepository

  beforeEach(async () => {
    const { dataSource } = await mockPostgresDatabase({
      entities: [OperationLockItemEntry],
    })
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(OperationLockItemEntry, "core"),
          useValue: dataSource.getRepository(OperationLockItemEntry),
        },
        {
          provide: getEntityManagerProviderToken("OperationsLockRepository"),
          useClass: OperationLockItemRepository,
        },
        OperationLockService,
      ],
    }).compile()

    service = module.get<OperationLockService>(OperationLockService)
    repo = module.get<OperationLockItemRepository>(
      getEntityManagerProviderToken("OperationsLockRepository")
    )
  })

  it("should be defined", () => {
    expect(service).toBeDefined()
  })

  it("should execute operation", async () => {
    const result = await service.executeSequential({
      lockUid: "operation1",
      operation: async () => mockResult(),
    })
    expect(result).toStrictEqual(mockResult())

    const lock = await repo.getLock("operation1")
    expect(lock).toBeUndefined()
  })

  it("should queue operations for result", async () => {
    await repo.acquireLock({
      lockUid: "operation2",
    })

    const lock = await repo.getLock("operation2")
    expect(lock).toBeDefined()
    expect(lock?.uid).toBe("operation2")

    let operation2Completed = false

    const task = service.executeSequential({
      lockUid: "operation2",
      operation: async () => {
        operation2Completed = true
      },
    })

    await sleep(1000)
    expect(operation2Completed).toBeFalsy()

    await repo.releaseLock({
      lockUid: "operation2",
    })

    await task
    expect(operation2Completed).toBeTruthy()
  })

  it("should execute operation again", async () => {
    let totExecutions = 0
    await service.executeSequential({
      lockUid: "operation3",
      operation: async () => {
        totExecutions++
      },
    })

    expect(totExecutions).toBe(1)

    const lock = await repo.getLock("operation3")
    expect(lock).toBeUndefined()

    await service.executeSequential({
      lockUid: "operation3",
      operation: async () => {
        totExecutions++
      },
    })

    expect(totExecutions).toBe(2)
  })
})
