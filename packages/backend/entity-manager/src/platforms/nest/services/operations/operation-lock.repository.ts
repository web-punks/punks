import { Injectable } from "@nestjs/common"
import { EntityTarget, Repository } from "typeorm"
import { newUuid } from "@punks/backend-core"
import {
  ILockRepository,
  LockAcquireInput,
  LockAcquireResult,
  LockNotFoundError,
  LockReleaseInput,
} from "../../../../abstractions"

class LockItemClass {
  uid: string
  lockedBy?: string
  createdOn: Date
}

@Injectable()
export class TypeormOperationLockRepository<TLockEntry extends LockItemClass>
  implements ILockRepository
{
  constructor(
    private readonly repo: Repository<TLockEntry>,
    private readonly entityClass: EntityTarget<TLockEntry>
  ) {}

  // todo: fix typing (and remove as any)
  getLock = async (lockUid: string) =>
    (await this.repo.findOne({
      where: {
        uid: lockUid,
      } as any,
    })) ?? undefined

  acquireLock = async (input: LockAcquireInput): Promise<LockAcquireResult> => {
    return await this.repo.manager.transaction(
      async (manager): Promise<LockAcquireResult> => {
        const currentLock = await manager.findOne(this.entityClass, {
          where: [
            {
              uid: input.lockUid,
            } as any,
          ],
        })

        if (currentLock) {
          return {
            available: false,
            lockItem: currentLock,
          }
        }

        const newLock = manager.create(this.entityClass, {
          id: newUuid(),
          uid: input.lockUid,
          lockedBy: input.requestedBy,
        } as any)
        await manager.save([newLock])

        return {
          available: true,
          lockItem: newLock,
        }
      }
    )
  }

  releaseLock = async (input: LockReleaseInput) => {
    const result = await this.repo.delete({
      uid: input.lockUid,
    } as any)
    if (result.affected === 0) {
      throw new LockNotFoundError(`Lock ${input.lockUid} was not found`)
    }
  }
}
