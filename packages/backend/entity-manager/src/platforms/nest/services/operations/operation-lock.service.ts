import { Inject, Injectable } from "@nestjs/common"
import { differenceInMinutes } from "date-fns"
import {
  ExclusiveOperationResult,
  ExecuteExclusiveInput,
  ExecuteSequentialInput,
  ILockRepository,
  IOperationLockService,
  LockItem,
} from "../../../../abstractions"
import { getEntityManagerProviderToken } from "../../ioc/tokens/symbols"
import { sleep } from "@punks/backend-core"
import { WpOperationLockService } from "../../decorators/operations"

const DEFAULT_LOCK_POLLING = 100

@WpOperationLockService()
export class OperationLockService implements IOperationLockService {
  constructor(
    @Inject(getEntityManagerProviderToken("OperationsLockRepository"))
    private readonly operations: ILockRepository
  ) {}

  executeSequential = async <T>(
    input: ExecuteSequentialInput<T>
  ): Promise<T> => {
    const lock = await this.operations.acquireLock({
      lockUid: input.lockUid,
      requestedBy: input.requestedBy,
    })

    if (
      !lock.available &&
      input.lockTimeout &&
      this.isLockExpired(lock.lockItem, new Date(), input.lockTimeout)
    ) {
      await this.operations.releaseLock({
        lockUid: input.lockUid,
      })
      return await this.executeSequential(input)
    }

    if (!lock.available) {
      await sleep(input.lockPolling ?? DEFAULT_LOCK_POLLING)
      return await this.executeSequential(input)
    }

    try {
      return await input.operation()
    } finally {
      await this.operations.releaseLock({
        lockUid: input.lockUid,
      })
    }
  }

  executeExclusive = async <T>(
    input: ExecuteExclusiveInput<T>
  ): Promise<ExclusiveOperationResult<T>> => {
    const lock = await this.operations.acquireLock({
      lockUid: input.lockUid,
      requestedBy: input.requestedBy,
    })

    if (
      !lock.available &&
      input.lockTimeout &&
      this.isLockExpired(lock.lockItem, new Date(), input.lockTimeout)
    ) {
      await this.operations.releaseLock({
        lockUid: input.lockUid,
      })
      return await this.executeExclusive(input)
    }

    if (!lock.available) {
      return {
        skipped: true,
        result: undefined,
      }
    }

    try {
      return {
        skipped: false,
        result: await input.operation(),
      }
    } finally {
      await this.operations.releaseLock({
        lockUid: input.lockUid,
      })
    }
  }

  private isLockExpired = (
    item: LockItem,
    refDate: Date,
    timeoutMinutes: number
  ) => {
    return differenceInMinutes(refDate, item.createdOn) > timeoutMinutes
  }
}
