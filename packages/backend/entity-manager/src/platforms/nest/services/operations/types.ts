export class ExecuteExclusiveTimeoutError extends Error {}

export class ExecuteSequentialTimeoutError extends Error {}
