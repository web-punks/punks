import request from "supertest"
import { Repository } from "typeorm"
import { createTestServer } from "../../server"
import { FooEntity } from "../../server/database/core/entities/foo.entity"
import { EntityManagerRegistry } from "../../../ioc/registry"
import { SessionController } from "../../server/session/session.controller"
import { AppAuthContextProvider } from "../../server/infrastructure/authentication"
import { NestTestServerPorts } from "../ports"

describe("Test server session", () => {
  let repository: Repository<FooEntity>
  let registry: EntityManagerRegistry
  let auth: AppAuthContextProvider
  let controller: SessionController
  let httpServer: any
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { app, server, dataSource, terminate } = await createTestServer({
      useAuthentication: true,
      createWebServer: true,
      extraControllers: [SessionController],
      serverStartPort: NestTestServerPorts.SessionTest,
    })
    repository = dataSource.getRepository(FooEntity)
    registry = await server.resolve<EntityManagerRegistry>(
      EntityManagerRegistry
    )
    auth = app.get(AppAuthContextProvider)
    controller = app.get(SessionController)
    httpServer = app.getHttpServer()
    terminateServer = terminate
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should retrieve user from controller", async () => {
    const data = await controller.getAndSetUserSessionData({
      item: {
        value: "123",
      },
    })
    expect(data).toEqual({
      item: {
        value: "123",
      },
    })
  })

  it("should retrieve user session", async () => {
    const response = await request(httpServer)
      .post("/session/getAndSetUserSessionData")
      .send({
        item: {
          value: "123",
        },
      })
      .expect(201)

    expect(response.body).toEqual({
      item: {
        value: "123",
      },
    })
  })

  it("should retrieve user session with middleware", async () => {
    const response = await request(httpServer)
      .post("/session/getAndSetUserSessionDataFromService")
      .send({
        item: {
          value: "123",
        },
      })
      .expect(201)

    expect(response.body).toEqual({
      item: {
        value: "123",
      },
    })
  })
})
