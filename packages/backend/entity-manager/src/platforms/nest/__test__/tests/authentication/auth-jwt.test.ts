import { createTestServer } from "../../server"
import { AuthenticationService } from "../../../extensions"

describe("Test auth jwt", () => {
  let authService: AuthenticationService

  beforeEach(async () => {
    const { app } = await createTestServer({
      useAuthentication: true,
    })
    authService = app.get(AuthenticationService)
  })

  it("should create and validate jwt", async () => {
    await createUser({
      userName: "user@test.it",
      password: "password",
    })

    const { token } = await authService.userLogin({
      userName: "user@test.it",
      password: "password",
    })

    expect(token).toBeDefined()

    const response = await authService.userTokenVerify({
      token: token as string,
    })
    expect(response.isValid).toBeTruthy()
    expect(response.data).toMatchObject({
      userId: expect.any(String),
      email: "user@test.it",
    })
  })

  const createUser = async (input: { userName: string; password: string }) => {
    await authService.userRegister({
      callback: {
        urlTemplate:
          "http://localhost:9400/authTest/confirmEmail?token={token}",
        tokenPlaceholder: "{token}",
      },
      email: input.userName,
      password: input.password,
      languageCode: "en",
      registrationInfo: {
        firstName: "name",
        lastName: "surname",
        userName: input.userName,
      },
      userName: input.userName,
    })
  }
})
