import { ITask } from "../../../../../abstractions"
import { TaskContext } from "../../../../../abstractions/tasks"
import { WpTask } from "../../../decorators"
import { TaskConcurrency } from "../../../decorators/tasks"

@WpTask("singleTask", {
  concurrency: TaskConcurrency.Single,
  cronExpression: "*/2 * * * * *",
})
export class SingleTask implements ITask {
  private static counter = 0
  async execute(context: TaskContext): Promise<void> {
    SingleTask.counter++
  }

  static getCounter() {
    return SingleTask.counter
  }
}

@WpTask("parallelTask", {
  concurrency: TaskConcurrency.Multiple,
  cronExpression: "*/2 * * * * *",
})
export class ParallelTask implements ITask {
  private static counter = 0
  async execute(context: TaskContext): Promise<void> {
    ParallelTask.counter++
  }

  static getCounter() {
    return ParallelTask.counter
  }
}

@WpTask("manualTask", {
  concurrency: TaskConcurrency.Single,
})
export class ManualTask implements ITask {
  private static counter = 0
  async execute(context: TaskContext): Promise<void> {
    ManualTask.counter++
  }

  static getCounter() {
    return ManualTask.counter
  }
}
