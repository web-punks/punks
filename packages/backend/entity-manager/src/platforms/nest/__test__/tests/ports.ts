export const NestTestServerPorts = {
  CrudBaseApi: 50000,
  CrudPipelineAPI: 50100,
  AuthApiTest: 50200,
  AuthGuardsTest: 50300,
  SessionTest: 50400,
  InitApiTest: 50500,
  VersioningApiTest: 50600,
  MultiTenancyApiTest: 50700,
  TenantInitPipeline: 51000,
  CompanyInitPipeline: 51100,
}
