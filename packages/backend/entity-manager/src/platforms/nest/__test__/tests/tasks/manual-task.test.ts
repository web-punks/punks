import { TasksService } from "../../../extensions/tasks/services/tasks-service"
import { createTestServer } from "../../server"
import { ManualTask } from "./tasks"
import { TestingModule } from "@nestjs/testing"

describe("Manual task", () => {
  let serverInstance: TestingModule
  let service: TasksService

  beforeEach(async () => {
    const { server } = await createTestServer({
      extraModules: [],
      extraProviders: [ManualTask],
    })

    serverInstance = server
    service = server.get(TasksService)
  })

  it("should run manual task", async () => {
    await service.invokeTask("manualTask")
    let counter = ManualTask.getCounter()
    expect(counter).toBe(1)

    await service.invokeTask("manualTask")
    counter = ManualTask.getCounter()
    expect(counter).toBe(2)
  })

  afterEach(async () => {
    serverInstance.close()
  })
})
