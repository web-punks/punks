import { Repository } from "typeorm"
import { createTestServer } from "../../server"
import { newUuid } from "@punks/backend-core"
import { CrmContactEntity } from "../../server/database/core/entities/crmContact.entity"
import { CrmContactActions } from "../../server/app/crmContacts/crmContact.actions"
import { AppTenantEntity } from "../../server/database/core/entities/appTenant.entity"
import { AppOrganizationEntity } from "../../server/database/core/entities/appOrganization.entity"
import { CrmContactSearchParameters } from "../../server/entities/crmContacts/crmContact.types"
import { AppAuthContextProvider } from "../../server/infrastructure/authentication"

export const mockAdminUser = (
  auth: AppAuthContextProvider,
  tenant: AppTenantEntity
) => {
  jest.spyOn(auth, "getContext").mockImplementation(async () => ({
    isAnonymous: false,
    isAuthenticated: true,
    userContext: {
      tenantId: tenant.id,
      tenantUid: tenant.uid,
    },
  }))
}

export const mockOrganizationUser = (
  auth: AppAuthContextProvider,
  organization: AppOrganizationEntity
) => {
  jest.spyOn(auth, "getContext").mockImplementation(async () => ({
    isAnonymous: false,
    isAuthenticated: true,
    userContext: {
      tenantId: organization.tenant.id,
      tenantUid: organization.tenant.uid,
      organizationId: organization.id,
      organizationUid: organization.uid,
    },
  }))
}

describe("Test multitenancy actions", () => {
  let tenants: Repository<AppTenantEntity>
  let organizations: Repository<AppOrganizationEntity>
  let repository: Repository<CrmContactEntity>
  let entityActions: CrmContactActions
  let contextProvider: AppAuthContextProvider

  beforeEach(async () => {
    const { app, dataSource } = await createTestServer({
      useAuthorization: true,
    })
    tenants = dataSource.getRepository(AppTenantEntity)
    organizations = dataSource.getRepository(AppOrganizationEntity)
    repository = dataSource.getRepository(CrmContactEntity)
    entityActions = app.get(CrmContactActions)
    contextProvider = app.get(AppAuthContextProvider)
  })

  it("should create a new entity", async () => {
    const { organization1 } = await mockTenantAndOrganizations()
    mockOrganizationUser(contextProvider, organization1)

    const result = await entityActions.manager.create.execute({
      organizationId: organization1.id,
      email: "test@test.it",
      firstName: "name",
      lastName: "surname",
    })

    const item = await repository.findOne({
      where: {
        id: result.id,
      },
      relations: ["organization"],
    })
    expect(item).toEqual({
      id: expect.any(String),
      email: "test@test.it",
      firstName: "name",
      lastName: "surname",
      organization: {
        id: organization1.id,
        name: "organization1",
        uid: "organization1",
        createdOn: expect.any(Date),
        updatedOn: expect.any(Date),
      },
      createdOn: expect.any(Date),
      updatedOn: expect.any(Date),
    })
  })

  it("should update organization entity", async () => {
    const { organization1, organization2 } = await mockTenantAndOrganizations()
    mockOrganizationUser(contextProvider, organization1)

    await mockSearchEntities({
      organization1,
      organization2,
    })
    mockOrganizationUser(contextProvider, organization1)

    let contact = await repository.findOneByOrFail({
      email: "test@test.it",
    })

    await entityActions.manager.update.execute(contact.id, {
      organizationId: organization1.id,
      email: "test@test.it",
      firstName: "name_new",
      lastName: "surname_new",
    })

    contact = await repository.findOneByOrFail({
      email: "test@test.it",
    })
    expect(contact).toMatchObject({
      email: "test@test.it",
      firstName: "name_new",
      lastName: "surname_new",
    })
  })

  it("should search entities with facets full", async () => {
    const { organization1, organization2 } = await mockTenantAndOrganizations()
    await mockSearchEntities({
      organization1,
      organization2,
    })
    mockAdminUser(contextProvider, organization1.tenant)

    const request: CrmContactSearchParameters = {
      filters: {},
      options: {
        includeFacets: true,
      },
    }
    const results = await entityActions.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: expect.any(String),
          email: "test@test.it",
          firstName: "name",
          lastName: "surname",
          createdOn: expect.any(Date),
          updatedOn: expect.any(Date),
          organization: {
            id: organization1.id,
            name: "organization1",
            uid: "organization1",
            createdOn: expect.any(Date),
            updatedOn: expect.any(Date),
          },
        },
        {
          id: expect.any(String),
          email: "test2@test.it",
          firstName: "name",
          lastName: "surname",
          createdOn: expect.any(Date),
          updatedOn: expect.any(Date),
          organization: {
            id: organization1.id,
            name: "organization1",
            uid: "organization1",
            createdOn: expect.any(Date),
            updatedOn: expect.any(Date),
          },
        },
        {
          id: expect.any(String),
          email: "test3@test.it",
          firstName: "name",
          lastName: "surname",
          createdOn: expect.any(Date),
          updatedOn: expect.any(Date),
          organization: {
            id: organization1.id,
            name: "organization1",
            uid: "organization1",
            createdOn: expect.any(Date),
            updatedOn: expect.any(Date),
          },
        },
        {
          id: expect.any(String),
          email: "test4@test.it",
          firstName: "name",
          lastName: "surname",
          createdOn: expect.any(Date),
          updatedOn: expect.any(Date),
          organization: {
            id: organization2.id,
            name: "organization2",
            uid: "organization2",
            createdOn: expect.any(Date),
            updatedOn: expect.any(Date),
          },
        },
        {
          id: expect.any(String),
          email: "test5@test.it",
          firstName: "name",
          lastName: "surname",
          createdOn: expect.any(Date),
          updatedOn: expect.any(Date),
          organization: {
            id: organization2.id,
            name: "organization2",
            uid: "organization2",
            createdOn: expect.any(Date),
            updatedOn: expect.any(Date),
          },
        },
      ],
      facets: {
        email: {
          values: [
            {
              count: 1,
              value: "test2@test.it",
            },
            {
              count: 1,
              value: "test3@test.it",
            },
            {
              count: 1,
              value: "test4@test.it",
            },
            {
              count: 1,
              value: "test5@test.it",
            },
            {
              count: 1,
              value: "test@test.it",
            },
          ],
        },
      },
    })
  })

  it("should search entities with facets from organization user", async () => {
    const { organization1, organization2 } = await mockTenantAndOrganizations()
    await mockSearchEntities({
      organization1,
      organization2,
    })
    mockOrganizationUser(contextProvider, organization1)

    const request: CrmContactSearchParameters = {
      filters: {},
      options: {
        includeFacets: true,
      },
    }
    const results = await entityActions.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: expect.any(String),
          email: "test@test.it",
          firstName: "name",
          lastName: "surname",
          createdOn: expect.any(Date),
          updatedOn: expect.any(Date),
          organization: {
            id: organization1.id,
            name: "organization1",
            uid: "organization1",
            createdOn: expect.any(Date),
            updatedOn: expect.any(Date),
          },
        },
        {
          id: expect.any(String),
          email: "test2@test.it",
          firstName: "name",
          lastName: "surname",
          createdOn: expect.any(Date),
          updatedOn: expect.any(Date),
          organization: {
            id: organization1.id,
            name: "organization1",
            uid: "organization1",
            createdOn: expect.any(Date),
            updatedOn: expect.any(Date),
          },
        },
        {
          id: expect.any(String),
          email: "test3@test.it",
          firstName: "name",
          lastName: "surname",
          createdOn: expect.any(Date),
          updatedOn: expect.any(Date),
          organization: {
            id: organization1.id,
            name: "organization1",
            uid: "organization1",
            createdOn: expect.any(Date),
            updatedOn: expect.any(Date),
          },
        },
      ],
      facets: {
        email: {
          values: [
            {
              count: 1,
              value: "test2@test.it",
            },
            {
              count: 1,
              value: "test3@test.it",
            },
            {
              count: 1,
              value: "test@test.it",
            },
          ],
        },
      },
    })
  })

  const mockSearchEntities = async ({
    organization1,
    organization2,
  }: {
    organization1: AppOrganizationEntity
    organization2: AppOrganizationEntity
  }) => {
    await repository.insert({
      id: newUuid(),
      email: "test@test.it",
      firstName: "name",
      lastName: "surname",
      organization: organization1,
      createdOn: new Date(),
      updatedOn: new Date(),
    })
    await repository.insert({
      id: newUuid(),
      email: "test2@test.it",
      firstName: "name",
      lastName: "surname",
      organization: organization1,
      createdOn: new Date(),
      updatedOn: new Date(),
    })
    await repository.insert({
      id: newUuid(),
      email: "test3@test.it",
      firstName: "name",
      lastName: "surname",
      organization: organization1,
      createdOn: new Date(),
      updatedOn: new Date(),
    })
    await repository.insert({
      id: newUuid(),
      email: "test4@test.it",
      firstName: "name",
      lastName: "surname",
      organization: organization2,
      createdOn: new Date(),
      updatedOn: new Date(),
    })
    await repository.insert({
      id: newUuid(),
      email: "test5@test.it",
      firstName: "name",
      lastName: "surname",
      organization: organization2,
      createdOn: new Date(),
      updatedOn: new Date(),
    })
  }

  const mockTenantAndOrganizations = async () => {
    const tenant = await tenants.insert({
      id: newUuid(),
      uid: "tenant1",
      name: "tenant1",
      updatedOn: new Date(),
    })
    const organization1 = await organizations.insert({
      id: newUuid(),
      uid: "organization1",
      name: "organization1",
      tenant: tenant.identifiers[0].id,
      updatedOn: new Date(),
    })
    const organization2 = await organizations.insert({
      id: newUuid(),
      uid: "organization2",
      name: "organization2",
      tenant: tenant.identifiers[0].id,
      updatedOn: new Date(),
    })

    return {
      organization1: (await organizations.findOne({
        where: {
          id: organization1.identifiers[0].id,
        },
        relations: ["tenant"],
      })) as AppOrganizationEntity,
      organization2: (await organizations.findOne({
        where: {
          id: organization2.identifiers[0].id,
        },
        relations: ["tenant"],
      })) as AppOrganizationEntity,
    }
  }
})
