import { Repository } from "typeorm"
import { createTestServer } from "../../server"
import { FooEntity } from "../../server/database/core/entities/foo.entity"
import { FooSeeder } from "../../server/entities/foos/foo.seeder"

describe("Test entity seeder actions", () => {
  let repository: Repository<FooEntity>

  beforeEach(async () => {
    const { dataSource } = await createTestServer({
      extraProviders: [FooSeeder],
    })

    repository = dataSource.getRepository(FooEntity)
  }, 30000)

  it("should have seed entity", async () => {
    const item = await repository.findOneBy({
      name: "foo",
    })
    expect(item).toEqual({
      id: expect.any(String),
      age: 20,
      name: "foo",
      uid: null,
      updatedOn: expect.any(Date),
    })
  })
})
