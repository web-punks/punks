import { Repository } from "typeorm"
import { createTestServer } from "../../server"
import { FooEntity } from "../../server/database/core/entities/foo.entity"
import { EntityManagerRegistry } from "../../../ioc/registry"
import { newUuid } from "@punks/backend-core"
import { FooActions } from "../../server/app/foos/foo.actions"
import { FooSearchParameters } from "../../server/entities/foos/foo.models"
import { AppTenantEntity } from "../../server/database/core/entities/appTenant.entity"
import { AppOrganizationEntity } from "../../server/database/core/entities/appOrganization.entity"
import { CrmContactEntity } from "../../server/database/core/entities/crmContact.entity"
import { mockOrganizationUser } from "../multitenancy/multitenancy-actions.test"
import { AppAuthContextProvider } from "../../server/infrastructure/authentication"

describe("Test typeorm actions", () => {
  let repository: Repository<FooEntity>
  let tenants: Repository<AppTenantEntity>
  let organizations: Repository<AppOrganizationEntity>
  let contacts: Repository<CrmContactEntity>
  let entityActions: FooActions
  let registry: EntityManagerRegistry
  let contextProvider: AppAuthContextProvider

  beforeEach(async () => {
    const { server, dataSource } = await createTestServer({
      useAuthentication: true,
    })
    repository = dataSource.getRepository(FooEntity)
    tenants = dataSource.getRepository(AppTenantEntity)
    organizations = dataSource.getRepository(AppOrganizationEntity)
    contacts = dataSource.getRepository(CrmContactEntity)
    entityActions = await server.resolve<FooActions>(FooActions)
    registry = await server.resolve<EntityManagerRegistry>(
      EntityManagerRegistry
    )
    contextProvider = server.get(AppAuthContextProvider)
  })

  it("should create a new entity", async () => {
    const { organization } = await mockSearchEntities()
    mockOrganizationUser(contextProvider, organization!)

    const result = await entityActions.manager.create.execute({
      profile: {
        age: 10,
        name: "foo",
      },
    })

    const item = await repository.findOneBy({
      id: result.id,
    })
    expect(item).toEqual({
      id: expect.any(String),
      age: 10,
      name: "foo",
      uid: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should update a entity", async () => {
    const { organization } = await mockSearchEntities()
    mockOrganizationUser(contextProvider, organization!)

    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await entityActions.manager.update.execute(id, {
      id,
      profile: {
        age: 200,
        name: "bar2",
      },
    })

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toEqual({
      id,
      age: 200,
      name: "bar2",
      uid: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should delete a entity", async () => {
    const { organization } = await mockSearchEntities()
    mockOrganizationUser(contextProvider, organization!)

    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await entityActions.manager.delete.execute(id)

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toBeNull()
  })

  it("should search entities", async () => {
    const { organization } = await mockSearchEntities()
    mockOrganizationUser(contextProvider, organization!)

    const request: FooSearchParameters = {
      filters: {
        minAge: 20,
      },
    }
    const results = await entityActions.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "2",
          profile: {
            age: 20,
            name: "foo2",
          },
        },
        {
          id: "3",
          profile: {
            age: 30,
            name: "foo3",
          },
        },
        {
          id: "5",
          profile: {
            age: 50,
            name: "foo4",
          },
        },
      ],
    })
  })

  it("should search entities with paging from beginning", async () => {
    const { organization } = await mockSearchEntities()
    mockOrganizationUser(contextProvider, organization!)

    const request: FooSearchParameters = {
      filters: {
        minAge: 20,
      },
      paging: {
        pageSize: 2,
      },
    }
    const results = await entityActions.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "2",
          profile: {
            age: 20,
            name: "foo2",
          },
        },
        {
          id: "3",
          profile: {
            age: 30,
            name: "foo3",
          },
        },
      ],
      paging: {
        pageIndex: 0,
        pageSize: 2,
        totItems: 3,
        totPages: 2,
        totPageItems: 2,
        currentPageCursor: 0,
        nextPageCursor: 1,
      },
    })
  })

  it("should search entities with paging from page 2", async () => {
    const { organization } = await mockSearchEntities()
    mockOrganizationUser(contextProvider, organization!)

    const request: FooSearchParameters = {
      filters: {
        minAge: 20,
      },
      paging: {
        cursor: 1,
        pageSize: 2,
      },
    }
    const results = await entityActions.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "5",
          profile: {
            age: 50,
            name: "foo4",
          },
        },
      ],
      paging: {
        pageIndex: 1,
        pageSize: 2,
        totItems: 3,
        totPages: 2,
        totPageItems: 1,
        currentPageCursor: 1,
      },
    })
  })

  it("should search entities with facets", async () => {
    const { organization } = await mockSearchEntities()
    mockOrganizationUser(contextProvider, organization!)

    const request: FooSearchParameters = {
      filters: {
        minAge: 30,
      },
      options: {
        includeFacets: true,
        facetsFilters: {
          minAge: 20,
        },
      },
    }
    const results = await entityActions.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "3",
          profile: {
            age: 30,
            name: "foo3",
          },
        },
        {
          id: "5",
          profile: {
            age: 50,
            name: "foo4",
          },
        },
      ],
      facets: {
        age: {
          values: [
            {
              count: 1,
              value: 20,
            },
            {
              count: 1,
              value: 30,
            },
            {
              count: 1,
              value: 50,
            },
          ],
        },
      },
    })
  })

  it("should search entities with facets authenticated", async () => {
    const { organization } = await mockSearchEntities()
    mockOrganizationUser(contextProvider, organization!)

    const request: FooSearchParameters = {
      filters: {
        minAge: 30,
      },
      options: {
        includeFacets: true,
        facetsFilters: {
          minAge: 20,
        },
      },
    }
    const results = await entityActions.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "3",
          profile: {
            age: 30,
            name: "foo3",
          },
        },
        {
          id: "5",
          profile: {
            age: 50,
            name: "foo4",
          },
        },
      ],
      facets: {
        age: {
          values: [
            {
              count: 1,
              value: 20,
            },
            {
              count: 1,
              value: 30,
            },
            {
              count: 1,
              value: 50,
            },
          ],
        },
      },
    })
  })

  it("should run full text search", async () => {
    const { organization } = await mockSearchEntities()
    mockOrganizationUser(contextProvider, organization!)

    const request: FooSearchParameters = {
      query: {
        term: "_Tes",
        fields: ["name", "uid"],
      },
    }
    const results = await entityActions.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "1",
          profile: {
            age: 10,
            name: "foo",
          },
        },
        {
          id: "2",
          profile: {
            age: 20,
            name: "foo2",
          },
        },
      ],
    })
  })

  it("should run full text search filtered", async () => {
    const { organization } = await mockSearchEntities()
    mockOrganizationUser(contextProvider, organization!)

    const request: FooSearchParameters = {
      filters: {
        minAge: 20,
      },
      query: {
        term: "_Tes",
        fields: ["name", "uid"],
      },
    }
    const results = await entityActions.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "2",
          profile: {
            age: 20,
            name: "foo2",
          },
        },
      ],
    })
  })

  const mockSearchEntities = async () => {
    await tenants.insert({
      id: newUuid(),
      name: "Default",
      uid: "default",
    })
    const tenant =
      (await tenants.findOne({
        where: {
          uid: "default",
        },
      })) ?? undefined
    await organizations.insert({
      id: newUuid(),
      name: "Acme",
      uid: "acme",
      tenant,
    })
    const organization =
      (await organizations.findOne({
        where: {
          uid: "acme",
        },
        relations: {
          tenant: true,
        },
      })) ?? undefined
    await contacts.insert({
      id: newUuid(),
      email: "test@prova.it",
      firstName: "test",
      lastName: "test",
      organization,
    })
    const contact = await contacts.findOne({
      where: {
        email: "test@prova.it",
      },
    })
    await repository.insert({
      id: "1",
      age: 10,
      name: "foo",
      uid: "foo_test",
      tenant,
      contact: {
        id: contact?.id,
      },
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "2",
      age: 20,
      name: "foo2",
      uid: "foo2_test",
      tenant,
      contact: {
        id: contact?.id,
      },
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "3",
      age: 30,
      name: "foo3",
      uid: "foo3",
      tenant,
      contact: {
        id: contact?.id,
      },
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "4",
      age: 10,
      name: "foo4",
      uid: "foo4",
      tenant,
      contact: {
        id: contact?.id,
      },
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "5",
      age: 50,
      name: "foo4",
      uid: "foo4",
      tenant,
      contact: {
        id: contact?.id,
      },
      updatedOn: new Date(),
    })

    return {
      organization,
      tenant,
    }
  }
})
