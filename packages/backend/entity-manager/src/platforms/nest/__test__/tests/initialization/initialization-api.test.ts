import request from "supertest"
import { createTestServer } from "../../server"
import { NestTestServerPorts } from "../ports"
import {
  TenantInitializeRequest,
  TenantInitializeResponse,
} from "../../server/app/appAdmin/templates/tenant-initialize/models"
import {
  OrganizationInitializeRequest,
  OrganizationInitializeResponse,
} from "../../server/app/appAdmin/templates/organization-register/models"
import {
  UserLoginRequest,
  UserLoginResponse,
} from "../../server/app/appAuth/appAuth.dto"

describe("Test initialization api", () => {
  let httpServer: any
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { app, terminate } = await createTestServer({
      useAuthentication: true,
      useAuthorization: true,
      createWebServer: true,
      serverStartPort: NestTestServerPorts.InitApiTest,
    })
    httpServer = app.getHttpServer()
    terminateServer = terminate
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should initialize platform", async () => {
    const {
      body: setupResponse,
    }: {
      body: TenantInitializeResponse
    } = await request(httpServer)
      .post("/v1/appAdmin/tenantInitialize")
      .send({
        tenant: {
          name: "Tenant 1",
          uid: "tenant1",
        },
        directory: {
          name: "Global",
          uid: "global",
        },
        defaultAdmin: {
          email: "amin@test.it",
          firstName: "Amin",
          lastName: "Test",
          password: "123456",
        },
      } as TenantInitializeRequest)
      .expect(201)

    expect(setupResponse.tenantId).toBeDefined()

    const {
      body: loginResponse,
    }: {
      body: UserLoginResponse
    } = await request(httpServer)
      .post("/v1/appAuth/login")
      .send({
        userName: "amin@test.it",
        password: "123456",
      } as UserLoginRequest)
      .expect(201)

    expect(loginResponse.token).toBeDefined()

    const {
      body: organizationInitializeResponse,
    }: {
      body: OrganizationInitializeResponse
    } = await request(httpServer)
      .post("/v1/appAdmin/organizationInitialize")
      .set("Authorization", `Bearer ${loginResponse.token}`)
      .send({
        company: {
          name: "Company 1",
          uid: "company1",
        },
        organization: {
          name: "Organization 1",
          uid: "organization1",
          tenantId: setupResponse.tenantId,
        },
      } as OrganizationInitializeRequest)
      .expect(201)

    expect(organizationInitializeResponse.companyId).toBeDefined()
    expect(organizationInitializeResponse.organizationId).toBeDefined()
  })
})
