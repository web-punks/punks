import { Repository } from "typeorm"
import request from "supertest"
import { createTestServer } from "../../server"
import { FooEntity } from "../../server/database/core/entities/foo.entity"
import { EntityManagerRegistry } from "../../../ioc/registry"
import { newUuid } from "@punks/backend-core"
import { FooEntityManager } from "../../server/entities/foos/foo.manager"
import { AppEntityVersionEntity } from "../../server/database/core/entities/appEntityVersion.entity"
import { NestTestServerPorts } from "../ports"
import {
  EntityVersionOperation,
  SortDirection,
} from "../../../../../abstractions"
import {
  FooVersionsSearchRequest,
  FooVersionsSearchResponse,
} from "../../server/app/foos/foo.dto"

describe("Entity versioning api", () => {
  let repository: Repository<FooEntity>
  let entityManager: FooEntityManager
  let registry: EntityManagerRegistry
  let versionsRepo: Repository<AppEntityVersionEntity>
  let httpServer: any
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { app, server, dataSource, terminate } = await createTestServer({
      useVersioning: true,
      createWebServer: true,
      serverStartPort: NestTestServerPorts.VersioningApiTest,
    })
    repository = dataSource.getRepository(FooEntity)
    versionsRepo = dataSource.getRepository(AppEntityVersionEntity)
    entityManager = await server.resolve<FooEntityManager>(FooEntityManager)
    registry = await server.resolve<EntityManagerRegistry>(
      EntityManagerRegistry
    )
    httpServer = app.getHttpServer()
    terminateServer = terminate

    await mockVersionEntities()
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should get all versions", async () => {
    const searchRequest: FooVersionsSearchRequest = {
      params: {
        entity: {
          entityId: "entityA",
        },
      },
    }
    const httpResponse = await request(httpServer)
      .post("/v1/foo/versions")
      .send(searchRequest)
      .expect(201)

    const response: FooVersionsSearchResponse = httpResponse.body
    expect(response).toMatchObject({
      request: searchRequest,
      items: [
        {
          id: "entityA",
          profile: {
            name: "1",
          },
        },
        {
          id: "entityA",
          profile: {
            name: "2",
          },
        },
        {
          id: "entityA",
          profile: {
            name: "3",
          },
        },
      ],
    })
  })

  it("should first page desc", async () => {
    const searchRequest: FooVersionsSearchRequest = {
      params: {
        entity: {
          entityId: "entityA",
        },
        paging: {
          pageSize: 2,
        },
        sorting: {
          direction: SortDirection.Desc,
        },
      },
    }
    const httpResponse = await request(httpServer)
      .post("/v1/foo/versions")
      .send(searchRequest)
      .expect(201)

    const response: FooVersionsSearchResponse = httpResponse.body
    expect(response).toMatchObject({
      request: searchRequest,
      items: [
        {
          id: "entityA",
          profile: {
            name: "3",
          },
        },
        {
          id: "entityA",
          profile: {
            name: "2",
          },
        },
      ],
      paging: {
        pageIndex: 0,
        pageSize: 2,
        totPageItems: 2,
        totPages: 2,
        totItems: 3,
        nextPageCursor: 1,
        currentPageCursor: 0,
      },
    })
  })

  it("should second page desc", async () => {
    const searchRequest: FooVersionsSearchRequest = {
      params: {
        entity: {
          entityId: "entityA",
        },
        paging: {
          pageSize: 2,
          cursor: 1,
        },
        sorting: {
          direction: SortDirection.Desc,
        },
      },
    }
    const httpResponse = await request(httpServer)
      .post("/v1/foo/versions")
      .send(searchRequest)
      .expect(201)

    const response: FooVersionsSearchResponse = httpResponse.body
    expect(response).toMatchObject({
      request: searchRequest,
      items: [
        {
          id: "entityA",
          profile: {
            name: "1",
          },
        },
      ],
      paging: {
        pageIndex: 1,
        pageSize: 2,
        totPageItems: 1,
        totPages: 2,
        totItems: 3,
        prevPageCursor: 0,
        currentPageCursor: 1,
      },
    })
  })

  const mockVersionEntities = async () => {
    await versionsRepo.insert({
      id: newUuid(),
      entityId: "entityA",
      entityType: "foo",
      data: {
        id: "entityA",
        name: "1",
      } as any,
      operationType: EntityVersionOperation.Create,
      updatedOn: new Date("2020-01-01T00:00:00.000Z"),
    })
    await versionsRepo.insert({
      id: newUuid(),
      entityId: "entityA",
      entityType: "foo2",
      data: {
        id: "entityA",
        name: "10",
      } as any,
      operationType: EntityVersionOperation.Create,
      updatedOn: new Date("2020-01-01T00:00:00.000Z"),
    })
    await versionsRepo.insert({
      id: newUuid(),
      entityId: "entityB",
      entityType: "foo",
      data: {
        id: "entityB",
        name: "1",
      } as any,
      operationType: EntityVersionOperation.Create,
      updatedOn: new Date("2020-01-01T00:00:00.000Z"),
    })
    await versionsRepo.insert({
      id: newUuid(),
      entityId: "entityA",
      entityType: "foo",
      data: {
        id: "entityA",
        name: "2",
      } as any,
      operationType: EntityVersionOperation.Update,
      updatedOn: new Date("2020-01-01T00:01:00.000Z"),
    })
    await versionsRepo.insert({
      id: newUuid(),
      entityId: "entityA",
      entityType: "foo",
      data: {
        id: "entityA",
        name: "3",
      } as any,
      operationType: EntityVersionOperation.Update,
      updatedOn: new Date("2020-01-01T00:02:00.000Z"),
    })
  }
})
