import { Repository } from "typeorm"
import { createTestServer } from "../../server"
import { FooEntity } from "../../server/database/core/entities/foo.entity"
import { EntityManagerRegistry } from "../../../ioc/registry"
import { newUuid } from "@punks/backend-core"
import { FooSearchParameters } from "../../server/entities/foos/foo.types"
import { FooEntityManager } from "../../server/entities/foos/foo.manager"
import { AppTenantEntity } from "../../server/database/core/entities/appTenant.entity"
import { FooSorting } from "../../server/entities/foos/foo.models"
import { SortDirection } from "../../../../../abstractions"

describe("Test typeorm operations", () => {
  let repository: Repository<FooEntity>
  let tenants: Repository<AppTenantEntity>
  let entityManager: FooEntityManager
  let registry: EntityManagerRegistry

  beforeEach(async () => {
    const { server, dataSource } = await createTestServer()
    repository = dataSource.getRepository(FooEntity)
    tenants = dataSource.getRepository(AppTenantEntity)
    entityManager = await server.resolve<FooEntityManager>(FooEntityManager)
    registry = await server.resolve<EntityManagerRegistry>(
      EntityManagerRegistry
    )
  })

  it("should register all providers", () => {
    expect(repository).toBeDefined()
    expect(registry).toBeDefined()

    const registeredServices = Object.keys(
      registry.getContainer().locator.all()
    )
    expect(registeredServices).toMatchSnapshot()
  })

  it("should create a new entity", async () => {
    await repository.delete({})
    const result = await entityManager.manager.create.execute({
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    const item = await repository.findOneBy({
      id: result.id,
    })
    expect(item).toEqual({
      id: expect.any(String),
      age: 10,
      name: "foo",
      uid: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should update a entity", async () => {
    await repository.delete({})
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await entityManager.manager.update.execute(id, {
      age: 200,
      name: "bar2",
    })

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toEqual({
      id,
      age: 200,
      name: "bar2",
      uid: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should update a partial entity", async () => {
    await repository.delete({})
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await entityManager.manager.update.execute(id, {
      name: "bar2",
      updatedOn: new Date(),
    })

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toEqual({
      id,
      age: 10,
      name: "bar2",
      uid: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should delete a entity", async () => {
    await repository.delete({})
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await entityManager.manager.delete.execute(id)

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toBeNull()
  })

  it("should find entity", async () => {
    await repository.delete({})
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        minAge: 20,
      },
    }
    const result = await entityManager.manager.find.execute(request)

    expect(result).toMatchObject({
      id: "2",
      age: 20,
      name: "foo2",
      updatedOn: expect.any(Date),
    })
  })

  it("should search entities", async () => {
    await repository.delete({})
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        minAge: 20,
      },
    }
    const results = await entityManager.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "2",
          age: 20,
          name: "foo2",
          updatedOn: expect.any(Date),
        },
        {
          id: "3",
          age: 30,
          name: "foo3",
          updatedOn: expect.any(Date),
        },
        {
          id: "5",
          age: 50,
          name: "foo4",
          updatedOn: expect.any(Date),
        },
      ],
    })
  })

  it("should search entities with paging from beginning", async () => {
    await repository.delete({})
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        minAge: 20,
      },
      paging: {
        pageSize: 2,
      },
    }
    const results = await entityManager.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "2",
          age: 20,
          name: "foo2",
          updatedOn: expect.any(Date),
        },
        {
          id: "3",
          age: 30,
          name: "foo3",
          updatedOn: expect.any(Date),
        },
      ],
      paging: {
        pageIndex: 0,
        pageSize: 2,
        totItems: 3,
        totPages: 2,
        totPageItems: 2,
        currentPageCursor: 0,
        nextPageCursor: 1,
      },
    })
  })

  it("should search entities with paging from page 2", async () => {
    await repository.delete({})
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        minAge: 20,
      },
      paging: {
        cursor: 1,
        pageSize: 2,
      },
    }
    const results = await entityManager.manager.search.execute(request)

    expect(results).toMatchObject({
      request,
      items: [
        {
          id: "5",
          age: 50,
          name: "foo4",
          updatedOn: expect.any(Date),
        },
      ],
      paging: {
        pageIndex: 1,
        pageSize: 2,
        totItems: 3,
        totPages: 2,
        totPageItems: 1,
        currentPageCursor: 1,
      },
    })
  })

  it("should count entities", async () => {
    await repository.delete({})
    await mockSearchEntities()

    const results = await entityManager.manager.count.execute({
      minAge: 20,
    })

    expect(results).toEqual(3)
  })

  it("should find existing entities", async () => {
    await repository.delete({})
    await mockSearchEntities()

    const results = await entityManager.manager.exists.execute({
      minAge: 20,
    })

    expect(results).toBeTruthy()
  })

  it("should not find existing entities", async () => {
    await repository.delete({})
    await mockSearchEntities()

    const results = await entityManager.manager.exists.execute({
      minAge: 20000,
    })

    expect(results).toBeFalsy()
  })

  it("should delete entities by query", async () => {
    await repository.delete({})
    await mockSearchEntities()

    const results = await entityManager.manager.deleteItems.execute({
      filters: {
        minAge: 20,
      },
    })

    expect(results.deletedCount).toBe(3)

    const itemsLeft = await repository.find()
    expect(itemsLeft).toMatchObject([
      {
        id: "1",
        age: 10,
        name: "foo",
        updatedOn: expect.any(Date),
      },
      {
        id: "4",
        age: 10,
        name: "foo4",
        updatedOn: expect.any(Date),
      },
    ])
  })

  it("should search entities with custom relations", async () => {
    await repository.delete({})
    await mockSearchEntities()

    const request: FooSearchParameters = {
      filters: {
        minAge: 10,
      },
      options: {
        includeFacets: true,
        facetsFilters: {
          minAge: 20,
        },
      },
      sorting: {
        fields: [
          {
            field: FooSorting.Age,
            direction: SortDirection.Asc,
          },
        ],
      },
      relations: {
        tenant: {
          directories: true,
          organizations: true,
        },
      },
    }
    const results = await entityManager.manager.search.execute(request)
    expect(JSON.parse(JSON.stringify(results))).toMatchObject({
      request,
      items: [
        {
          id: "1",
          age: 10,
          name: "foo",
          tenant: {
            name: "Default",
            uid: "default",
            organizations: [],
            updatedOn: expect.any(String),
            id: expect.any(String),
            createdOn: expect.any(String),
            directories: [],
          },
        },
        {
          id: "4",
          age: 10,
          name: "foo4",
          tenant: {
            name: "Default",
            uid: "default",
            organizations: [],
            updatedOn: expect.any(String),
            id: expect.any(String),
            createdOn: expect.any(String),
            directories: [],
          },
          uid: null,
          updatedOn: expect.any(String),
        },
        {
          id: "2",
          age: 20,
          name: "foo2",
          tenant: {
            name: "Default",
            uid: "default",
            organizations: [],
            updatedOn: expect.any(String),
            id: expect.any(String),
            createdOn: expect.any(String),
            directories: [],
          },
          uid: null,
          updatedOn: expect.any(String),
        },
        {
          id: "3",
          age: 30,
          name: "foo3",
          tenant: {
            name: "Default",
            uid: "default",
            organizations: [],
            updatedOn: expect.any(String),
            id: expect.any(String),
            createdOn: expect.any(String),
            directories: [],
          },
          uid: null,
          updatedOn: expect.any(String),
        },
        {
          id: "5",
          age: 50,
          name: "foo4",
          tenant: {
            name: "Default",
            uid: "default",
            organizations: [],
            updatedOn: expect.any(String),
            id: expect.any(String),
            createdOn: expect.any(String),
            directories: [],
          },
          uid: null,
          updatedOn: expect.any(String),
        },
      ],
      facets: {
        age: {
          values: [
            {
              count: 1,
              value: 20,
            },
            {
              count: 1,
              value: 30,
            },
            {
              count: 1,
              value: 50,
            },
          ],
        },
      },
    })
  })

  const mockSearchEntities = async () => {
    await tenants.insert({
      id: newUuid(),
      name: "Default",
      uid: "default",
    })
    const tenant =
      (await tenants.findOne({
        where: {
          uid: "default",
        },
      })) ?? undefined
    await repository.insert({
      id: "1",
      age: 10,
      name: "foo",
      tenant,
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "2",
      age: 20,
      name: "foo2",
      tenant,
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "3",
      age: 30,
      name: "foo3",
      tenant,
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "4",
      age: 10,
      name: "foo4",
      tenant,
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "5",
      age: 50,
      name: "foo4",
      tenant,
      updatedOn: new Date(),
    })
  }
})
