import request from "supertest"
import { createTestServer } from "../../server"
import { AuthenticationService } from "../../../extensions"
import { NestTestServerPorts } from "../ports"

describe("Test auth guards", () => {
  let httpServer: any
  let authService: AuthenticationService
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { app, terminate } = await createTestServer({
      useAuthentication: true,
      useAuthorization: true,
      createWebServer: true,
      serverStartPort: NestTestServerPorts.AuthGuardsTest,
    })
    httpServer = app.getHttpServer()
    authService = app.get(AuthenticationService)
    terminateServer = terminate
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should return public data to anonymous user", async () => {
    const response = await request(httpServer)
      .get("/authTest/getPublicData")
      .send()
      .expect(200)

    expect(response.body).toEqual({
      data: "foo",
    })
  })

  it("should return unauthorized to anonymous user", async () => {
    await request(httpServer)
      .get("/authTest/getAuthenticatedData")
      .send()
      .expect(403)
  })

  it("should return data to authenticated user", async () => {
    const { token } = await createUserAndLogin({
      userName: "user@test.it",
      password: "password",
      roles: [],
    })

    const response = await request(httpServer)
      .get("/authTest/getAuthenticatedData")
      .set("Authorization", `Bearer ${token}`)
      .send()
      .expect(200)
    expect(response.body).toEqual({
      data: "foo",
    })
  })

  it("should return data to admin", async () => {
    const { token } = await createUserAndLogin({
      userName: "user@test.it",
      password: "password",
      roles: ["admin"],
    })

    const response = await request(httpServer)
      .get("/authTest/getAdminData")
      .set("Authorization", `Bearer ${token}`)
      .send()
      .expect(200)

    expect(response.body).toEqual({
      data: "foo",
    })
  })

  it("should return data to non admin", async () => {
    const { token } = await createUserAndLogin({
      userName: "user@test.it",
      password: "password",
      roles: ["manager"],
    })

    await request(httpServer)
      .get("/authTest/getAdminData")
      .set("Authorization", `Bearer ${token}`)
      .send()
      .expect(403)
  })

  it("should return data to user with no roles", async () => {
    const { token } = await createUserAndLogin({
      userName: "user@test.it",
      password: "password",
      roles: [],
    })

    await request(httpServer)
      .get("/authTest/getAdminData")
      .set("Authorization", `Bearer ${token}`)
      .send()
      .expect(403)
  })

  it("shouldn't return data without api key", async () => {
    await request(httpServer).get("/authTest/getApiData").send().expect(403)
  })

  it("shouldn't return data without api key in a strict route", async () => {
    const { token } = await createUserAndLogin({
      userName: "user@test.it",
      password: "password",
      roles: [],
    })
    await request(httpServer)
      .get("/authTest/getApiData")
      .set("Authorization", `Bearer ${token}`)
      .send()
      .expect(403)
  })

  it("should return data using api key", async () => {
    const apiKey = await createApiKey()
    const response = await request(httpServer)
      .get("/authTest/getApiData")
      .set("Authorization", `ApiKey ${apiKey.key}`)
      .send()
      .expect(200)
    expect(response.body).toEqual({
      data: "foo",
    })
  })

  const createApiKey = async () => {
    return await authService.apiKeysService.create({
      key: "testApiKey",
      name: "Test Api Key",
    })
  }

  const createUserAndLogin = async (input: {
    userName: string
    password: string
    roles: string[]
  }) => {
    const { userId } = await authService.userRegister({
      callback: {
        urlTemplate:
          "http://localhost:9400/authTest/confirmEmail?token={token}",
        tokenPlaceholder: "{token}",
      },
      email: input.userName,
      password: input.password,
      languageCode: "en",
      registrationInfo: {
        firstName: "name",
        lastName: "surname",
        userName: input.userName,
      },
      userName: input.userName,
    })
    const loginResult = await authService.userLogin({
      userName: input.userName,
      password: input.password,
    })

    for (const role of input.roles) {
      await authService.rolesService.create({
        uid: role,
        name: `Role ${role}`,
      })
      await authService.userRolesService.addUserToRoleByUid(userId!, role)
    }

    return {
      token: loginResult.token as string,
    }
  }
})
