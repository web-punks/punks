import request from "supertest"
import { Repository } from "typeorm"
import { createTestServer } from "../../server"
import { FooEntity } from "../../server/database/core/entities/foo.entity"
import { EntityManagerRegistry } from "../../../ioc/registry"
import { newUuid } from "@punks/backend-core"
import { AppAuthContextProvider } from "../../server/infrastructure/authentication"
import { NestTestServerPorts } from "../ports"
import { FooAuthMiddleware } from "../../server/entities/foos/foo.authentication"
import {
  FooExportRequest,
  FooParseRequest,
  FooSampleDownloadRequest,
  FooSearchRequest,
} from "../../server/app/foos/foo.dto"
import { EntitySerializationFormat } from "../../../../../abstractions"
import { InMemoryBucketProvider } from "../../../plugins"

const mockAuthenticatedUser = (auth: AppAuthContextProvider) => {
  jest.spyOn(auth, "getContext").mockImplementation(async () => ({
    isAnonymous: false,
    isAuthenticated: true,
  }))
}

describe("Test api actions", () => {
  let repository: Repository<FooEntity>
  let registry: EntityManagerRegistry
  let auth: AppAuthContextProvider
  let middleware: FooAuthMiddleware
  let bucket: InMemoryBucketProvider
  let httpServer: any
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { app, server, dataSource, terminate } = await createTestServer({
      useAuthentication: true,
      createWebServer: true,
      serverStartPort: NestTestServerPorts.CrudBaseApi,
    })
    repository = dataSource.getRepository(FooEntity)
    registry = await server.resolve<EntityManagerRegistry>(
      EntityManagerRegistry
    )
    auth = app.get(AppAuthContextProvider)
    middleware = app.get(FooAuthMiddleware)
    httpServer = app.getHttpServer()
    bucket = app.get(InMemoryBucketProvider)
    terminateServer = terminate
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should not create a new entity", async () => {
    await repository.delete({})
    const response = await request(httpServer)
      .put("/v1/foo/create")
      .send({
        profile: {
          age: 10,
          name: "foo",
        },
      })
      .expect(401)

    const item = await repository.findOneBy({
      id: response.body.id,
    })
    expect(item).toEqual(null)
  })

  it("should create a new entity", async () => {
    mockAuthenticatedUser(auth)
    const response = await request(httpServer)
      .put("/v1/foo/create")
      .send({
        profile: {
          age: 10,
          name: "foo",
        },
      })
      .expect(200)

    const item = await repository.findOneBy({
      id: response.body.id,
    })
    expect(item).toEqual({
      id: expect.any(String),
      age: 10,
      name: "foo",
      uid: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should not update a entity", async () => {
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await request(httpServer)
      .post(`/v1/foo/update/${id}`)
      .send({
        profile: {
          age: 200,
          name: "bar2",
        },
      })
      .expect(401)

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toEqual({
      id,
      age: 10,
      name: "foo",
      uid: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should update a entity", async () => {
    await repository.delete({})
    mockAuthenticatedUser(auth)

    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await request(httpServer)
      .post(`/v1/foo/update/${id}`)
      .send({
        profile: {
          age: 200,
          name: "bar2",
        },
      })
      .expect(201)

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toEqual({
      id,
      age: 200,
      name: "bar2",
      uid: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should not delete a entity", async () => {
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await request(httpServer).delete(`/v1/foo/${id}`).expect(401)

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toEqual({
      id,
      age: 10,
      name: "foo",
      uid: null,
      updatedOn: expect.any(Date),
    })
  })

  it("should delete a entity", async () => {
    mockAuthenticatedUser(auth)

    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await request(httpServer).delete(`/v1/foo/${id}`).expect(200)

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toBeNull()
  })

  it("should not get entity", async () => {
    await mockSearchEntities()
    await request(httpServer).get("/v1/foo/item/1").expect(401)
  })

  it("should get entity", async () => {
    mockAuthenticatedUser(auth)

    await mockSearchEntities()

    const response = await request(httpServer).get("/v1/foo/item/1").expect(200)
    expect(response.body).toMatchObject({
      id: "1",
      profile: {
        age: 10,
        name: "foo",
      },
    })
  })

  it("should search but not find entities because is unauthorized", async () => {
    await mockSearchEntities()

    const searchRequest: FooSearchRequest = {
      params: {
        filters: {
          minAge: 20,
        },
        paging: {
          pageSize: 3,
        },
      },
    }
    await request(httpServer)
      .post("/v1/foo/search")
      .send(searchRequest)
      .expect(401)
  })

  it("should search but not find entities because he cannot read", async () => {
    await repository.delete({})
    mockAuthenticatedUser(auth)

    jest.spyOn(middleware, "canRead").mockImplementation(async () => ({
      isAuthorized: false,
    }))

    await mockSearchEntities()

    const searchRequest: FooSearchRequest = {
      params: {
        filters: {
          minAge: 20,
        },
        paging: {
          pageSize: 3,
        },
      },
    }
    const response = await request(httpServer)
      .post("/v1/foo/search")
      .send(searchRequest)
      .expect(201)

    expect(response.body).toMatchObject({
      request: searchRequest.params,
      items: [],
      paging: {
        pageIndex: 0,
        pageSize: 3,
        totItems: 3,
        totPages: 1,
        totPageItems: 0,
        currentPageCursor: 0,
      },
    })
  })

  it("should search entities", async () => {
    await repository.delete({})
    mockAuthenticatedUser(auth)
    await mockSearchEntities()

    const searchRequest: FooSearchRequest = {
      params: {
        filters: {
          minAge: 20,
        },
      },
    }
    const response = await request(httpServer)
      .post("/v1/foo/search")
      .send(searchRequest)
      .expect(201)

    expect(response.body).toMatchObject({
      request: searchRequest.params,
      items: [
        {
          id: "2",
          profile: {
            age: 20,
            name: "foo2",
          },
        },
        {
          id: "3",
          profile: {
            age: 30,
            name: "foo3",
          },
        },
        {
          id: "5",
          profile: {
            age: 50,
            name: "foo4",
          },
        },
      ],
    })
  })

  it("should search entities with paging from beginning", async () => {
    await repository.delete({})
    mockAuthenticatedUser(auth)
    await mockSearchEntities()

    const searchRequest: FooSearchRequest = {
      params: {
        filters: {
          minAge: 20,
        },
        paging: {
          pageSize: 2,
        },
      },
    }
    const response = await request(httpServer)
      .post("/v1/foo/search")
      .send(searchRequest)
      .expect(201)

    expect(response.body).toMatchObject({
      request: searchRequest.params,
      items: [
        {
          id: "2",
          profile: {
            age: 20,
            name: "foo2",
          },
        },
        {
          id: "3",
          profile: {
            age: 30,
            name: "foo3",
          },
        },
      ],
      paging: {
        pageIndex: 0,
        pageSize: 2,
        totItems: 3,
        totPages: 2,
        totPageItems: 2,
        currentPageCursor: 0,
        nextPageCursor: 1,
      },
    })
  })

  it("should search entities with paging from page 2", async () => {
    await repository.delete({})
    mockAuthenticatedUser(auth)
    await mockSearchEntities()

    const searchRequest: FooSearchRequest = {
      params: {
        filters: {
          minAge: 20,
        },
        paging: {
          cursor: 1,
          pageSize: 2,
        },
      },
    }
    const response = await request(httpServer)
      .post("/v1/foo/search")
      .send(searchRequest)
      .expect(201)

    expect(response.body).toMatchObject({
      request: searchRequest.params,
      items: [
        {
          id: "5",
          profile: {
            age: 50,
            name: "foo4",
          },
        },
      ],
      paging: {
        pageIndex: 1,
        pageSize: 2,
        totItems: 3,
        totPages: 2,
        totPageItems: 1,
        currentPageCursor: 1,
      },
    })
  })

  it("should export all entities", async () => {
    await repository.delete({})
    mockAuthenticatedUser(auth)
    await mockSearchEntities()

    const exportRequest: FooExportRequest = {
      filter: {},
      options: {
        format: EntitySerializationFormat.Csv,
      },
    }
    const response = await request(httpServer)
      .post("/v1/foo/export")
      .send(exportRequest)
      .expect(201)

    const content = await bucket.fileDownload({
      bucket: response.body.downloadUrl.split("/")[0],
      filePath: response.body.downloadUrl.split("/").slice(1).join("/"),
    })
    expect(content.toString("utf-8")).toMatchSnapshot()
  })

  it("should export filtered entities", async () => {
    await repository.delete({})
    mockAuthenticatedUser(auth)
    await mockSearchEntities()

    const exportRequest: FooExportRequest = {
      filter: {
        filters: {
          minAge: 20,
        },
      },
      options: {
        format: EntitySerializationFormat.Csv,
      },
    }
    const response = await request(httpServer)
      .post("/v1/foo/export")
      .send(exportRequest)
      .expect(201)

    const content = await bucket.fileDownload({
      bucket: response.body.downloadUrl.split("/")[0],
      filePath: response.body.downloadUrl.split("/").slice(1).join("/"),
    })
    expect(content.toString("utf-8")).toMatchSnapshot()
  })

  it("should entities download sample", async () => {
    await repository.delete({})
    mockAuthenticatedUser(auth)
    await mockSearchEntities()

    const sampleDownload: FooSampleDownloadRequest = {
      format: EntitySerializationFormat.Csv,
    }
    const response = await request(httpServer)
      .post("/v1/foo/sampleDownload")
      .send(sampleDownload)
      .expect(201)

    const content = await bucket.fileDownload({
      bucket: response.body.downloadUrl.split("/")[0],
      filePath: response.body.downloadUrl.split("/").slice(1).join("/"),
    })
    expect(content.toString("utf-8")).toMatchSnapshot()
  })

  const mockSearchEntities = async () => {
    await repository.insert({
      id: "1",
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "2",
      age: 20,
      name: "foo2",
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "3",
      age: 30,
      name: "foo3",
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "4",
      age: 10,
      name: "foo4",
      updatedOn: new Date(),
    })
    await repository.insert({
      id: "5",
      age: 50,
      name: "foo4",
      updatedOn: new Date(),
    })
  }
})
