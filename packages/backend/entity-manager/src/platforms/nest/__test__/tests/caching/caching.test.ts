import { createTestServer } from "../../server"
import { newUuid, addTime, subtractTime } from "@punks/backend-core"
import { CacheService } from "../../../services"
import { Repository } from "typeorm"
import { AppCacheEntry } from "../../server/database/core/entities/appCacheEntry.entity"

describe("Caching", () => {
  let cacheService: CacheService
  let repository: Repository<AppCacheEntry>

  beforeEach(async () => {
    const { app, dataSource } = await createTestServer()
    cacheService = app.get(CacheService)
    repository = dataSource.getRepository(AppCacheEntry)
  })

  it("should retrieve cache items", async () => {
    const value = await cacheService.getInstance("default").retrieve("test", {
      ttl: {
        value: 1000,
        unit: "seconds",
      },
      valueFactory: () =>
        Promise.resolve({
          foo: "bar",
        }),
    })
    expect(value).toEqual({
      foo: "bar",
    })
  })

  it("should not find expired item", async () => {
    await cacheService.getInstance("default").retrieve("test", {
      ttl: {
        value: 1,
        unit: "seconds",
      },
      valueFactory: () =>
        Promise.resolve({
          foo: "bar",
        }),
    })

    await repository.update(
      {
        key: "test",
      },
      {
        expirationTime: subtractTime(new Date(), {
          value: 1000,
          unit: "seconds",
        }),
      }
    )

    const cachedItem = await cacheService.getInstance("default").get("test")
    expect(cachedItem).toBeUndefined()

    const dbItems = await repository.find()
    expect(dbItems).toMatchObject([
      {
        key: "test",
        instance: "default",
        data: {
          foo: "bar",
        },
      },
    ])
  })

  it("should retrieve correct instance items", async () => {
    await repository.insert({
      id: newUuid(),
      instance: "instance2",
      key: "test",
      expirationTime: addTime(new Date(), {
        value: 1000,
        unit: "seconds",
      }),
      data: {
        value: "test1val",
      } as any,
    })

    await repository.insert({
      id: newUuid(),
      instance: "default",
      key: "test",
      expirationTime: addTime(new Date(), {
        value: 1000,
        unit: "seconds",
      }),
      data: {
        value: "test2val",
      } as any,
    })

    const value = await cacheService.getInstance("default").get("test")
    expect(value).toEqual({
      value: "test2val",
    })
  })
})
