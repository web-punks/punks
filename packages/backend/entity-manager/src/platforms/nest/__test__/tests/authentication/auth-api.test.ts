import request from "supertest"
import { createTestServer } from "../../server"
import {
  UserEmailVerifyCompleteRequest,
  UserLoginRequest,
  UserLoginResponse,
  UserRegisterRequest,
  UserRegisterResponse,
} from "../../server/app/appAuth/appAuth.dto"
import { EmailVerifyEmailPayload } from "../../../extensions"
import { NestTestServerPorts } from "../ports"
import { InMemoryEmailProvider } from "../../../plugins"

describe("Test auth api", () => {
  let httpServer: any
  let emailProvider: InMemoryEmailProvider
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { app, terminate } = await createTestServer({
      useAuthentication: true,
      useAuthorization: true,
      createWebServer: true,
      serverStartPort: NestTestServerPorts.AuthApiTest,
    })
    httpServer = app.getHttpServer()
    emailProvider = app.get(InMemoryEmailProvider)
    terminateServer = terminate
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should execute non multi tenant auth process", async () => {
    const email = "user@test.com"
    const password = "Password1!"

    const registrationResponse = await request(httpServer)
      .post("/v1/appAuth/register")
      .send({
        email,
        password,
        userName: email,
        callback: {
          urlTemplate:
            "http://localhost:9400/v1/auth/verifyComplete?token={token}",
          tokenPlaceholder: "{token}",
        },
        data: {
          firstName: "Valentino",
          lastName: "Rossi",
          birthDate: "1979-02-16",
        },
      } as UserRegisterRequest)
      .expect(201)

    expect(
      (registrationResponse.body as UserRegisterResponse).success
    ).toBeTruthy()

    expect(emailProvider.getSentEmails().length).toBe(1)
    const verifyToken = (
      emailProvider.getSentEmails()[0].input.payload as EmailVerifyEmailPayload
    ).callbackUrl.split("token=")[1]

    await request(httpServer)
      .post("/v1/appAuth/verifyComplete")
      .send({
        token: verifyToken,
      } as UserEmailVerifyCompleteRequest)
      .expect(201)

    const loginResponse = await request(httpServer)
      .post("/v1/appAuth/login")
      .send({
        userName: email,
        password,
      } as UserLoginRequest)
      .expect(201)

    const loginResult: UserLoginResponse = loginResponse.body
    expect(loginResult.success).toBeTruthy()
    expect(loginResult.token).toBeDefined()
  })
})
