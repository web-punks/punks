import { Repository } from "typeorm"
import { createTestServer } from "../../server"
import { FooEntity } from "../../server/database/core/entities/foo.entity"
import { newUuid } from "@punks/backend-core"
import { FooActions } from "../../server/app/foos/foo.actions"
import { FooConnectors } from "../../server/entities/foos/connectors"
import { FooCustomConnector } from "../../server/entities/foos/connectors/custom/connector"

describe("Test typeorm connectors", () => {
  let repository: Repository<FooEntity>
  let entityActions: FooActions
  let connector: FooCustomConnector

  beforeEach(async () => {
    const { server, dataSource } = await createTestServer({
      useAuthentication: false,
      extraProviders: [...FooConnectors],
    })
    repository = dataSource.getRepository(FooEntity)
    entityActions = await server.resolve<FooActions>(FooActions)
    connector = await server.resolve<FooCustomConnector>(FooCustomConnector)
  })

  it("should create a new entity", async () => {
    await entityActions.manager.create.execute({
      profile: {
        age: 10,
        name: "foo",
      },
    })

    const syncQueue = connector.getSynchedElements()
    expect(syncQueue.length).toBe(1)
    expect(syncQueue[0]).toMatchObject({
      displayName: "foo",
      id: expect.any(String),
    })

    const deleteQueue = connector.getDeletedElements()
    expect(deleteQueue.length).toBe(0)
  })

  it("should update a entity", async () => {
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await entityActions.manager.update.execute(id, {
      id,
      profile: {
        age: 200,
        name: "bar2",
      },
    })

    const syncQueue = connector.getSynchedElements()
    expect(syncQueue.length).toBe(1)
    expect(syncQueue[0]).toMatchObject({
      id,
      displayName: "bar2",
    })

    const deleteQueue = connector.getDeletedElements()
    expect(deleteQueue.length).toBe(0)
  })

  it("should delete a entity", async () => {
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await entityActions.manager.delete.execute(id)

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toBeNull()

    const syncQueue = connector.getSynchedElements()
    expect(syncQueue.length).toBe(0)

    const deleteQueue = connector.getDeletedElements()
    expect(deleteQueue.length).toBe(1)
    expect(deleteQueue[0]).toBe(id)
  })
})
