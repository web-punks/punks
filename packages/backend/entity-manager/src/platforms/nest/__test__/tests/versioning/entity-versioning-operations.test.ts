import { Repository } from "typeorm"
import { createTestServer } from "../../server"
import { FooEntity } from "../../server/database/core/entities/foo.entity"
import { newUuid } from "@punks/backend-core"
import { FooEntityManager } from "../../server/entities/foos/foo.manager"
import { AppEntityVersionEntity } from "../../server/database/core/entities/appEntityVersion.entity"

describe("Entity versioning test", () => {
  let repository: Repository<FooEntity>
  let entityManager: FooEntityManager
  let versionsRepo: Repository<AppEntityVersionEntity>

  beforeEach(async () => {
    const { server, dataSource } = await createTestServer({
      useVersioning: true,
    })
    repository = dataSource.getRepository(FooEntity)
    versionsRepo = dataSource.getRepository(AppEntityVersionEntity)
    entityManager = await server.resolve<FooEntityManager>(FooEntityManager)
  })

  it("should version create a new entity", async () => {
    const result = await entityManager.manager.create.execute({
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    const item = await repository.findOneBy({
      id: result.id,
    })
    expect(item).toEqual({
      id: expect.any(String),
      age: 10,
      name: "foo",
      uid: null,
      updatedOn: expect.any(Date),
    })

    const versioned = await versionsRepo.find()
    expect(versioned).toEqual([
      {
        id: expect.any(String),
        entityId: result.id,
        entityType: "foo",
        data: {
          id: expect.any(String),
          age: 10,
          name: "foo",
          uid: null,
          updatedOn: expect.any(String),
        },
        operationType: "create",
        operationUserId: null,
        createdOn: expect.any(Date),
        updatedOn: expect.any(Date),
      },
    ])
  })

  it("should version update a entity", async () => {
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await entityManager.manager.update.execute(id, {
      age: 200,
      name: "bar2",
    })

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toEqual({
      id,
      age: 200,
      name: "bar2",
      uid: null,
      updatedOn: expect.any(Date),
    })

    const versioned = await versionsRepo.find()
    expect(versioned).toEqual([
      {
        id: expect.any(String),
        entityId: id,
        entityType: "foo",
        data: {
          id: expect.any(String),
          age: 200,
          name: "bar2",
          uid: null,
          updatedOn: expect.any(String),
        },
        operationType: "update",
        operationUserId: null,
        createdOn: expect.any(Date),
        updatedOn: expect.any(Date),
      },
    ])
  })

  it("should version delete a entity", async () => {
    const id = newUuid()
    await repository.insert({
      id,
      age: 10,
      name: "foo",
      updatedOn: new Date(),
    })

    await entityManager.manager.delete.execute(id)

    const item = await repository.findOneBy({
      id,
    })
    expect(item).toBeNull()

    const versioned = await versionsRepo.find()
    expect(versioned).toEqual([
      {
        id: expect.any(String),
        entityId: id,
        entityType: "foo",
        data: null,
        operationType: "delete",
        operationUserId: null,
        createdOn: expect.any(Date),
        updatedOn: expect.any(Date),
      },
    ])
  })
})
