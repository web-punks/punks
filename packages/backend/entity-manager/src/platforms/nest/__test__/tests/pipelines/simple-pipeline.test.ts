import { PipelineErrorType, PipelineStepErrorType } from "../../../../../types"
import { PipelinesBuilder, PipelinesRunner } from "../../../pipelines"
import { PipelineUtils } from "../../../pipelines/template/utils"
import { createTestServer } from "../../server"

type TypeContext = {
  magicNumber: number
}

type Type1 = {
  value1: number
}
type Type2 = {
  value2: number
}
type Type3 = {
  value3: number
}
type Type4 = {
  value4: number
}

describe("Test Simple pipeline", () => {
  let builder: PipelinesBuilder
  let runner: PipelinesRunner

  beforeEach(async () => {
    const { server } = await createTestServer({
      enableLogging: true,
    })
    builder = server.get(PipelinesBuilder)
    runner = server.get(PipelinesRunner)
  })

  it("should run a simple pipeline template successfully", async () => {
    const rollbacks: string[] = []
    const template = builder
      .createTemplate<Type1, TypeContext>()
      .addStep<Type2>((step) =>
        step
          .addOperation({
            name: "step 1 operation",
            action: (input, state) => ({
              value2: input.value1 * state.context.magicNumber,
            }),
          })
          .withRollback({
            name: "step 1 rollback",
            action: () => {
              rollbacks.push("step1")
            },
          })
      )
      .addStep<Type3>((step) =>
        step
          .addOperation({
            name: "step 2 operation",
            action: (input, state) => ({
              value3:
                input.value2 +
                state.context.magicNumber *
                  state.previousStep!.stepInput.value1,
            }),
          })
          .withRollback({
            name: "step 2 rollback",
            action: () => {
              rollbacks.push("step2")
            },
          })
      )
      .addStep<Type4>((step) =>
        step
          .addOperation({
            name: "step 3 operation",
            action: (input, state) => ({
              value4: Math.pow(input.value3, state.context.magicNumber),
            }),
          })
          .withRollback({
            name: "step 3 rollback",
            action: () => {
              rollbacks.push("step3")
            },
          })
      )
      .complete()

    const result = await runner.invokePipeline(template, {
      input: {
        value1: 2,
      },
      context: {
        magicNumber: 3,
      },
    })

    expect(result).toEqual({
      input: {
        value1: 2,
      },
      type: "success",
      output: {
        value4: 1728,
      },
      stepResults: [
        {
          input: {
            value1: 2,
          },
          operationResults: [
            {
              input: {
                value1: 2,
              },
              output: {
                value2: 6,
              },
              type: "success",
            },
          ],
          output: {
            value2: 6,
          },
          type: "success",
        },
        {
          input: {
            value2: 6,
          },
          operationResults: [
            {
              input: {
                value2: 6,
              },
              output: {
                value3: 12,
              },
              type: "success",
            },
          ],
          output: {
            value3: 12,
          },
          type: "success",
        },
        {
          input: {
            value3: 12,
          },
          operationResults: [
            {
              input: {
                value3: 12,
              },
              output: {
                value4: 1728,
              },
              type: "success",
            },
          ],
          output: {
            value4: 1728,
          },
          type: "success",
        },
      ],
    })
    expect(rollbacks).toEqual([])
  })

  it("should run a simple pipeline template and rollback on error", async () => {
    const rollbacks: string[] = []
    const template = builder
      .createTemplate<Type1, TypeContext>()
      .addStep<Type2>((step) =>
        step
          .addOperation({
            name: "step 1 operation",
            action: (input, state) => ({
              value2: input.value1 * state.context.magicNumber,
            }),
          })
          .withRollback({
            name: "step 1 rollback",
            action: () => {
              rollbacks.push("step1")
            },
          })
      )
      .addStep<Type3>((step) =>
        step
          .addOperation({
            name: "step 2 operation",
            action: (input, state) => ({
              value3:
                input.value2 +
                state.context.magicNumber *
                  state.previousStep!.stepInput.value1,
            }),
          })
          .withRollback({
            name: "step 2 rollback",
            action: () => {
              rollbacks.push("step2")
            },
          })
      )
      .addStep<Type4>((step) =>
        step
          .addOperation({
            name: "step 3 operation",
            action: () => {
              throw new Error("Test error")
            },
          })
          .withRollback({
            name: "step 3 rollback",
            action: () => {
              rollbacks.push("step3")
            },
          })
      )
      .complete()

    const result = await runner.invokePipeline(template, {
      input: {
        value1: 2,
      },
      context: {
        magicNumber: 3,
      },
    })

    expect(result).toEqual({
      input: {
        value1: 2,
      },
      type: "error",
      errorType: PipelineErrorType.OperationError,
      stepResults: [
        {
          input: {
            value1: 2,
          },
          operationResults: [
            {
              input: {
                value1: 2,
              },
              output: {
                value2: 6,
              },
              type: "success",
            },
          ],
          output: {
            value2: 6,
          },
          type: "success",
        },
        {
          input: {
            value2: 6,
          },
          operationResults: [
            {
              input: {
                value2: 6,
              },
              output: {
                value3: 12,
              },
              type: "success",
            },
          ],
          output: {
            value3: 12,
          },
          type: "success",
        },
        {
          input: {
            value3: 12,
          },
          operationResults: [
            {
              error: {
                exception: expect.any(Error),
                message: "Test error",
                stackTrace: expect.any(String),
              },
              input: {
                value3: 12,
              },
              type: "error",
            },
          ],
          type: "error",
          errorType: PipelineStepErrorType.OperationError,
        },
      ],
    })
    expect(rollbacks).toEqual(["step3", "step2", "step1"])
  })

  it("should run a simple pipeline template skipping an operation", async () => {
    const rollbacks: string[] = []
    const template = builder
      .createTemplate<Type1, TypeContext>()
      .addStep<Type2>((step) =>
        step
          .addOperation({
            name: "step 1 operation",
            action: (input, state) => ({
              value2: input.value1 * state.context.magicNumber,
            }),
          })
          .withRollback({
            name: "step 1 rollback",
            action: () => {
              rollbacks.push("step1")
            },
          })
      )
      .addStep<Type3>((step) =>
        step
          .addOperation({
            name: "step 2 operation",
            action: (input, state) => ({
              value3:
                input.value2 +
                state.context.magicNumber *
                  state.previousStep!.stepInput.value1,
            }),
          })
          .withRollback({
            name: "step 2 rollback",
            action: () => {
              rollbacks.push("step2")
            },
          })
      )
      .addStep((step) =>
        step
          .addOperation({
            name: "step 3 operation",
            action: (input, state) => {
              throw new Error("Should not be called")
            },
            skipIf: (input, state) => state.context.magicNumber === 3,
          })
          .withRollback({
            name: "step 3 rollback",
            action: () => {
              rollbacks.push("step3")
            },
          })
      )
      .addStep<Type4>((step) =>
        step
          .addOperation({
            name: "step 4 operation",
            action: (input, state) => ({
              value4: Math.pow(
                state.previousStep?.previousStep?.stepOutput?.value3,
                state.context.magicNumber
              ),
            }),
          })
          .withRollback({
            name: "step 4 rollback",
            action: () => {
              rollbacks.push("step3")
            },
          })
      )
      .complete()

    const result = await runner.invokePipeline(template, {
      input: {
        value1: 2,
      },
      context: {
        magicNumber: 3,
      },
    })

    expect(result).toEqual({
      input: {
        value1: 2,
      },
      type: "success",
      output: {
        value4: 1728,
      },
      stepResults: [
        {
          input: {
            value1: 2,
          },
          operationResults: [
            {
              input: {
                value1: 2,
              },
              output: {
                value2: 6,
              },
              type: "success",
            },
          ],
          output: {
            value2: 6,
          },
          type: "success",
        },
        {
          input: {
            value2: 6,
          },
          operationResults: [
            {
              input: {
                value2: 6,
              },
              output: {
                value3: 12,
              },
              type: "success",
            },
          ],
          output: {
            value3: 12,
          },
          type: "success",
        },
        {
          input: {
            value3: 12,
          },
          operationResults: [
            {
              input: {
                value3: 12,
              },
              type: "skipped",
            },
          ],
          type: "success",
        },
        {
          input: undefined,
          operationResults: [
            {
              input: undefined,
              output: {
                value4: 1728,
              },
              type: "success",
            },
          ],
          output: {
            value4: 1728,
          },
          type: "success",
        },
      ],
    })
    expect(rollbacks).toEqual([])
  })

  it("should run a pipeline and get information from previous operation keys", async () => {
    const util = new PipelineUtils<{}, {}, TypeContext>()
    const template = builder
      .createTemplate<{}, TypeContext>()
      .addStep<string>((step) =>
        step.addOperation({
          key: "step1",
          name: "step 1 operation",
          action: (input, state) => "Simo",
        })
      )
      .addStep<string>((step) =>
        step.addOperation({
          key: "step2",
          name: "step 2 operation",
          action: (input, state) =>
            `Hi ${util.getStepOutputByKey(state, "step1")}!`,
        })
      )
      .complete()

    const result = await runner.invokePipeline(template, {
      input: {},
      context: {
        magicNumber: 3,
      },
    })

    expect(result).toEqual({
      input: {},
      type: "success",
      output: "Hi Simo!",
      stepResults: [
        {
          input: {},
          operationResults: [
            {
              input: {},
              output: "Simo",
              type: "success",
            },
          ],
          output: "Simo",
          type: "success",
        },
        {
          input: "Simo",
          operationResults: [
            {
              input: "Simo",
              output: "Hi Simo!",
              type: "success",
            },
          ],
          output: "Hi Simo!",
          type: "success",
        },
      ],
    })
  })

  // todo: implement
  // it("should run a pipeline template with multiple operations", async () => {
  //   const input = {
  //     base: 2,
  //     values: [1, 4, 8],
  //   }
  //   const template = builder
  //     .createTemplate<typeof input, TypeContext>()
  //     .addStep<number>((step) =>
  //       step.addOperation({
  //         name: "step 1 operation",
  //         action: (input, state) => input.base * state.context.magicNumber,
  //       })
  //     )
  //     .addStep<number>((step, state) =>
  //       step.mapOperations(state.pipelineInput.values, {
  //         name: "step 2 operation",
  //         action: (input, state) => input.stepInput * input.item,
  //       })
  //     )
  //     .complete()

  //   const result = await runner.invokePipeline(template, {
  //     input,
  //     context: {
  //       magicNumber: 3,
  //     },
  //   })

  //   expect(result).toEqual({})
  // })
})
