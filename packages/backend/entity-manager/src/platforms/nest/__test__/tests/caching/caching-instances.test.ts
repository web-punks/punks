import { createTestServer } from "../../server"
import { CacheService } from "../../../services"
import { WpCacheInstance } from "../../../decorators/cache"
import { TypeormCacheInstance } from "../../../../../integrations"
import { InjectRepository } from "@nestjs/typeorm"
import { AppCacheEntry } from "../../server/database/core/entities/appCacheEntry.entity"
import { Repository } from "typeorm"
import { DefaultCacheInstance } from "../../server/infrastructure/cache/providers/default-instance"

@WpCacheInstance("other")
export class OtherCacheInstance extends TypeormCacheInstance<AppCacheEntry> {
  constructor(
    @InjectRepository(AppCacheEntry, "core")
    private repository: Repository<AppCacheEntry>
  ) {
    super("other")
  }

  protected getRepository(): Repository<AppCacheEntry> {
    return this.repository
  }
}

describe("Cache instance", () => {
  let cacheService: CacheService

  beforeEach(async () => {
    const { app } = await createTestServer({
      extraGlobalProviders: [OtherCacheInstance],
    })
    cacheService = app.get(CacheService)
  })

  it("should resolve cache instances", async () => {
    const instances = cacheService.getInstances()
    expect(instances[0]).toBeInstanceOf(DefaultCacheInstance)
    expect(instances[1]).toBeInstanceOf(OtherCacheInstance)
  })
})
