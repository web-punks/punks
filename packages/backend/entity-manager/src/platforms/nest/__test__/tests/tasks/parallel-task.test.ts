import { createTestServer } from "../../server"
import { sleep } from "@punks/backend-core"
import { ParallelTask } from "./tasks"
import { TestingModule } from "@nestjs/testing"

describe("Parallel task", () => {
  let serverInstance: TestingModule

  beforeEach(async () => {
    const { server } = await createTestServer({
      extraModules: [],
      extraProviders: [ParallelTask],
    })

    serverInstance = server
  })

  it("should run sequential task", async () => {
    console.log("Running task")
    await sleep(5000)

    const counter = ParallelTask.getCounter()
    expect(counter).toBeGreaterThan(1)
  })

  afterEach(async () => {
    serverInstance.close()
  })
})
