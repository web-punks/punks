import { createTestServer } from "../../server"
import { sleep } from "@punks/backend-core"
import { SingleTask } from "./tasks"
import { TestingModule } from "@nestjs/testing"

describe("Simple task", () => {
  let serverInstance: TestingModule

  beforeEach(async () => {
    const { server } = await createTestServer({
      extraModules: [],
      extraProviders: [SingleTask],
    })

    serverInstance = server
  })

  it("should run sequential task", async () => {
    console.log("Running task")
    await sleep(5000)

    const counter = SingleTask.getCounter()
    expect(counter).toBeGreaterThan(1)
  })

  afterEach(async () => {
    serverInstance.close()
  })
})
