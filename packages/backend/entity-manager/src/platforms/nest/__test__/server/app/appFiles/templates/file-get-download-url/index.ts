import { PipelineDefinition } from "../../../../../../../../types"
import { WpPipeline } from "../../../../../../decorators"
import { IPipelineTemplateBuilder } from "../../../../../../pipelines/builder/types"
import { NestPipelineTemplate } from "../../../../../../pipelines/template"
import { AppAuthContext } from "../../../../infrastructure/authentication"
import { FileGetDownloadUrlInput, FileGetDownloadUrlResult } from "./models"
import { FilesManager } from "../../../../../../services"

@WpPipeline("FileGetDownloadUrl")
export class FileGetDownloadUrlTemplate extends NestPipelineTemplate<
  FileGetDownloadUrlInput,
  FileGetDownloadUrlResult,
  AppAuthContext
> {
  constructor(private readonly files: FilesManager) {
    super()
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<FileGetDownloadUrlInput, AppAuthContext>
  ): PipelineDefinition<
    FileGetDownloadUrlInput,
    FileGetDownloadUrlResult,
    AppAuthContext
  > {
    return builder
      .addStep<FileGetDownloadUrlResult>((step) => {
        step.addOperation({
          name: "File get download url",
          action: async (input) => {
            const result = await this.files.getFileDownloadUrl(input.fileId)
            return {
              url: result.downloadUrl,
            }
          },
        })
      })
      .complete()
  }
}
