import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { AppOrganizationActions } from "./appOrganization.actions"
import {
  AppOrganizationCreateDto,
  AppOrganizationDto,
  AppOrganizationSearchRequest,
  AppOrganizationSearchResponse,
  AppOrganizationUpdateDto,
} from "./appOrganization.dto"

@Controller("v1/appOrganization")
export class AppOrganizationController {
  constructor(private readonly actions: AppOrganizationActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appOrganizationGet",
  })
  @ApiOkResponse({
    type: AppOrganizationDto,
  })
  async item(@Param("id") id: string): Promise<AppOrganizationDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appOrganizationCreate",
  })
  @ApiOkResponse({
    type: AppOrganizationDto,
  })
  async create(
    @Body() data: AppOrganizationCreateDto
  ): Promise<AppOrganizationDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppOrganizationDto,
  })
  @ApiOperation({
    operationId: "appOrganizationUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppOrganizationUpdateDto
  ): Promise<AppOrganizationDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appOrganizationDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appOrganizationSearch",
  })
  @ApiOkResponse({
    type: AppOrganizationSearchResponse,
  })
  async search(
    @Body() request: AppOrganizationSearchRequest
  ): Promise<AppOrganizationSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }
}
