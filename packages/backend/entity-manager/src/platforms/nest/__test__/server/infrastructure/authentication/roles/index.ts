export const AppRoles = {
  TenantAdmin: {
    uid: "tenant-admin",
    name: "Tenant Admin",
  },
  OrganizationAdmin: {
    uid: "organization-admin",
    name: "Organization Admin",
  },
  OrganizationManager: {
    uid: "organization-manager",
    name: "Organization Manager",
  },
  OrganizationUser: {
    uid: "organization-user",
    name: "Organization User",
  },
  CompanyAdmin: {
    uid: "company-admin",
    name: "Company Admin",
  },
  CompanyManager: {
    uid: "company-manager",
    name: "Company Manager",
  },
  CompanyUser: {
    uid: "company-user",
    name: "Company User",
  },
}

export const InheritedRoles = {
  TenantAdmin: [
    AppRoles.OrganizationUser,
    AppRoles.OrganizationManager,
    AppRoles.OrganizationAdmin,
    AppRoles.CompanyUser,
    AppRoles.CompanyManager,
    AppRoles.CompanyAdmin,
  ],
  OrganizationAdmin: [
    AppRoles.CompanyUser,
    AppRoles.CompanyManager,
    AppRoles.CompanyAdmin,
  ],
  OrganizationManager: [AppRoles.CompanyUser, AppRoles.CompanyManager],
  OrganizationUser: [AppRoles.CompanyUser],
  CompanyAdmin: [AppRoles.CompanyUser, AppRoles.CompanyManager],
  CompanyManager: [AppRoles.CompanyUser],
  CompanyUser: [],
}
