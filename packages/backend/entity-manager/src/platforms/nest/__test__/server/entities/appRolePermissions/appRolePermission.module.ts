import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { AppRolePermissionAdapter } from "./appRolePermission.adapter"
import { AppRolePermissionAuthMiddleware } from "./appRolePermission.authentication"
import { AppRolePermissionEntityManager } from "./appRolePermission.manager"
import { AppRolePermissionQueryBuilder } from "./appRolePermission.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppRolePermissionAdapter,
    AppRolePermissionAuthMiddleware,
    AppRolePermissionEntityManager,
    AppRolePermissionQueryBuilder,
  ],
  exports: [AppRolePermissionEntityManager],
})
export class AppRolePermissionEntityModule {}
