import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from "typeorm"
import { AppOrganizationalUnitTypeEntity } from "./appOrganizationalUnitType.entity"
import { AppRoleEntity } from "./appRole.entity"
import { WpEntity } from "../../../../../decorators"

export type AppRoleOrganizationalUnitTypeEntityId = string

@Entity("appRoleOrganizationalUnitTypes")
@WpEntity("appRoleOrganizationalUnitType")
export class AppRoleOrganizationalUnitTypeEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppRoleOrganizationalUnitTypeEntityId

  @ManyToOne(() => AppRoleEntity, {
    nullable: false,
  })
  role: AppRoleEntity

  @ManyToOne(() => AppOrganizationalUnitTypeEntity, {
    nullable: false,
  })
  type: AppOrganizationalUnitTypeEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
