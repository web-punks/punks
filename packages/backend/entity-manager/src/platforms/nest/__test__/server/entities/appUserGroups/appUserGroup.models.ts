import {
  DeepPartial,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppUserGroupEntity } from "../../database/core/entities/appUserGroup.entity"
import { IEntitiesDeleteParameters } from "../../../../../../abstractions/commands"

export type AppUserGroupEntityId = string

export type AppUserGroupCreateData = DeepPartial<Omit<AppUserGroupEntity, "id">>
export type AppUserGroupUpdateData = DeepPartial<Omit<AppUserGroupEntity, "id">>

export class AppUserGroupFacets {}

export class AppUserGroupSearchFilters {}

export enum AppUserGroupSorting {
  Name = "Name",
}

export type AppUserGroupCursor = number

export class AppUserGroupSearchSortingField
  implements ISearchSortingField<AppUserGroupSorting>
{
  @ApiProperty({ enum: AppUserGroupSorting })
  field: AppUserGroupSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppUserGroupQuerySorting
  implements ISearchSorting<AppUserGroupSorting>
{
  @ApiProperty({ required: false, type: [AppUserGroupSearchSortingField] })
  fields: AppUserGroupSearchSortingField[]
}

export class AppUserGroupQueryPaging
  implements ISearchRequestPaging<AppUserGroupCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppUserGroupCursor

  @ApiProperty()
  pageSize: number
}

export class AppUserGroupSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppUserGroupSearchParameters
  implements
    IEntitySearchParameters<
      AppUserGroupEntity,
      AppUserGroupSorting,
      AppUserGroupCursor
    >
{
  @ApiProperty({ required: false })
  filters?: AppUserGroupSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserGroupQuerySorting

  @ApiProperty({ required: false })
  paging?: AppUserGroupQueryPaging

  @ApiProperty({ required: false })
  options?: AppUserGroupSearchOptions
}

export class AppUserGroupSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppUserGroupCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserGroupCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserGroupCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserGroupCursor
}

export class AppUserGroupSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppUserGroupEntity,
      AppUserGroupSearchParameters,
      TResult,
      AppUserGroupSorting,
      AppUserGroupCursor,
      AppUserGroupFacets
    >
{
  @ApiProperty()
  request: AppUserGroupSearchParameters

  @ApiProperty()
  facets?: AppUserGroupFacets

  @ApiProperty()
  paging?: AppUserGroupSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppUserGroupDeleteParameters
  implements IEntitiesDeleteParameters<AppUserGroupSorting>
{
  @ApiProperty({ required: false })
  filters?: AppUserGroupSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserGroupQuerySorting
}

export type AppUserGroupSheetItem = AppUserGroupEntity
