import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { AppDirectoryAdapter } from "./appDirectory.adapter"
import { AppDirectoryAuthMiddleware } from "./appDirectory.authentication"
import { AppDirectoryEntityManager } from "./appDirectory.manager"
import { AppDirectoryQueryBuilder } from "./appDirectory.query"
import { AppDirectorySerializer } from "./appDirectory.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppDirectoryAdapter,
    AppDirectoryAuthMiddleware,
    AppDirectoryEntityManager,
    AppDirectoryQueryBuilder,
    AppDirectorySerializer,
  ],
  exports: [AppDirectoryEntityManager],
})
export class AppDirectoryEntityModule {}
