import { Module } from "@nestjs/common"
import { AuthenticationModule, EntityManagerModule } from "../../.."
import { EntityModules } from "../entities/modules"

@Module({
  imports: [EntityManagerModule, AuthenticationModule, ...EntityModules],
  exports: [EntityManagerModule, AuthenticationModule, ...EntityModules],
})
export class SharedModule {}
