import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppTenantCreateData,
  AppTenantCursor,
  AppTenantDeleteParameters,
  AppTenantEntityId,
  AppTenantFacets,
  AppTenantSearchParameters,
  AppTenantSorting,
  AppTenantUpdateData,
} from "./appTenant.models"
import { AppTenantEntity } from "../../database/core/entities/appTenant.entity"

@WpEntityManager("appTenant")
export class AppTenantEntityManager extends NestEntityManager<
  AppTenantEntity,
  AppTenantEntityId,
  AppTenantCreateData,
  AppTenantUpdateData,
  AppTenantDeleteParameters,
  AppTenantSearchParameters,
  AppTenantSorting,
  AppTenantCursor,
  AppTenantFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appTenant", registry)
  }
}
