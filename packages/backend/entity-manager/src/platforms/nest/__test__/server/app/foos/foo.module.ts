import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { FooActions } from "./foo.actions"
import { FooController } from "./foo.controller"
import { FooConverter } from "./foo.converter"
import { FooEntityModule } from "../../entities/foos/foo.module"

@Module({
  imports: [SharedModule, FooEntityModule],
  providers: [FooActions, FooConverter],
  controllers: [FooController],
})
export class FooAppModule {}
