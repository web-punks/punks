import {
  Entity,
  Column,
  CreateDateColumn,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from "typeorm"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { AppDivisionEntity } from "./appDivision.entity"
import { WpEntity } from "../../../../.."

@Entity("appCompanies")
@WpEntity("appCompany")
@Unique("companyUid", ["organization", "uid"])
export class AppCompanyEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255 })
  uid: string

  @Column({ type: "varchar", length: 255 })
  name: string

  @ManyToOne(
    () => AppOrganizationEntity,
    (organization) => organization.companies
  )
  organization: AppOrganizationEntity

  @OneToMany(() => AppDivisionEntity, (division) => division.company)
  divisions?: AppDivisionEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
