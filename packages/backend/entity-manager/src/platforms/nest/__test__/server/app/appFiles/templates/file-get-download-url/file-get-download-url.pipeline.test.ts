import { createTestServer } from "../../../.."
import { FileGetDownloadUrlTemplate } from "."
import { AppFileAppModule } from "../../appFile.module"
import { FilesManager } from "../../../../../../services"

describe("Test File Get Download Url Pipeline", () => {
  let files: FilesManager
  let template: FileGetDownloadUrlTemplate
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { server, terminate } = await createTestServer({
      extraModules: [AppFileAppModule],
    })
    files = server.get(FilesManager)
    template = server.get(FileGetDownloadUrlTemplate)
    terminateServer = terminate
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should run the pipeline template successfully", async () => {
    const file = await files.uploadFile({
      content: Buffer.from("test1234567890"),
      contentType: "text/plain",
      fileName: "test.txt",
      folderPath: "test",
    })
    const result = await template.execute({
      input: {
        fileId: file.fileId,
      },
      context: {
        isAnonymous: true,
        isAuthenticated: false,
      },
    })

    expect(result.type).toBe("success")
    expect(result.output).toMatchObject({
      url: expect.any(String),
    })
  })
})
