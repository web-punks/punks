import { AppOrganizationEntity } from "../../database/core/entities/appOrganization.entity"
import { AppOrganizationReferenceDto } from "./appOrganization.dto"

export const toAppOrganizationReferenceDto = (
  entity: AppOrganizationEntity
): AppOrganizationReferenceDto => ({
  id: entity.id,
  name: entity.name,
  uid: entity.uid,
})
