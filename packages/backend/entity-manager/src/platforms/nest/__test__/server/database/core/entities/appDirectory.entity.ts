import {
  Entity,
  Column,
  CreateDateColumn,
  PrimaryColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
  Unique,
} from "typeorm"
import { AppTenantEntity } from "./appTenant.entity"
import { AppUserEntity } from "./appUser.entity"
import { WpEntity } from "../../../../../decorators"
import { IAppDirectory } from "../../../../../extensions"

export type AppDirectoryEntityId = string

@Entity("appDirectories")
@WpEntity("appDirectory")
@Unique("directoryUid", ["tenant", "uid"])
export class AppDirectoryEntity implements IAppDirectory {
  @PrimaryColumn({ type: "uuid" })
  id: AppDirectoryEntityId

  @Column({ type: "varchar", length: 255 })
  uid: string

  @Column({ type: "varchar", length: 255 })
  name: string

  @Column({ type: "boolean", default: false })
  default: boolean

  @ManyToOne(() => AppTenantEntity, (tenant) => tenant.directories)
  tenant: AppTenantEntity

  @OneToMany(() => AppUserEntity, (user) => user.directory)
  users?: AppUserEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
