import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "../../../../../.."
import {
  AppApiKeyCreateDto,
  AppApiKeyDto,
  AppApiKeyListItemDto,
  AppApiKeyUpdateDto,
} from "./appApiKey.dto"
import { AppApiKeyEntity } from "../../database/core/entities/appApiKey.entity"
import { toAppOrganizationReferenceDto } from "../appOrganizations/appOrganization.utils"

@WpEntityConverter("appApiKey")
export class AppApiKeyConverter
  implements
    IEntityConverter<
      AppApiKeyEntity,
      AppApiKeyDto,
      AppApiKeyListItemDto,
      AppApiKeyCreateDto,
      AppApiKeyUpdateDto
    >
{
  toListItemDto(entity: AppApiKeyEntity): AppApiKeyListItemDto {
    return {
      ...entity,
      organization: entity.organization
        ? toAppOrganizationReferenceDto(entity.organization)
        : undefined,
    }
  }

  toEntityDto(entity: AppApiKeyEntity): AppApiKeyDto {
    return {
      ...this.toListItemDto(entity),
    }
  }

  createDtoToEntity(input: AppApiKeyCreateDto): DeepPartial<AppApiKeyEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppApiKeyUpdateDto): DeepPartial<AppApiKeyEntity> {
    return {
      ...input,
    }
  }
}
