import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  CrmContactCreateData,
  CrmContactCursor,
  CrmContactEntityId,
  CrmContactFacets,
  CrmContactSorting,
  CrmContactUpdateData,
} from "./crmContact.models"
import {
  CrmContactSearchParameters,
  CrmContactDeleteParameters,
} from "./crmContact.types"
import { CrmContactEntity } from "../../database/core/entities/crmContact.entity"

@WpEntityManager("crmContact")
export class CrmContactEntityManager extends NestEntityManager<
  CrmContactEntity,
  CrmContactEntityId,
  CrmContactCreateData,
  CrmContactUpdateData,
  CrmContactDeleteParameters,
  CrmContactSearchParameters,
  CrmContactSorting,
  CrmContactCursor,
  CrmContactFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("crmContact", registry)
  }
}
