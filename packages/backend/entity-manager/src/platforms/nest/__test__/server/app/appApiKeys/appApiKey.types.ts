import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "../../../../../.."
import {
  AppApiKeySearchParameters,
  AppApiKeySearchResults,
  AppApiKeyVersionsSearchParameters,
  AppApiKeyVersionsSearchResults,
} from "../../entities/appApiKeys/appApiKey.types"
import { AppApiKeyListItemDto } from "./appApiKey.dto"

export class AppApiKeySearchRequest {
  @ApiProperty()
  params: AppApiKeySearchParameters
}

export class AppApiKeySearchResponse extends AppApiKeySearchResults<AppApiKeyListItemDto> {
  @ApiProperty({ type: [AppApiKeyListItemDto] })
  items: AppApiKeyListItemDto[]
}

export class AppApiKeyEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppApiKeyExportRequest {
  @ApiProperty()
  options: AppApiKeyEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppApiKeySearchParameters
}

export class AppApiKeyExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppApiKeySampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppApiKeySampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppApiKeyVersionsSearchRequest {
  @ApiProperty()
  params: AppApiKeyVersionsSearchParameters
}

export class AppApiKeyVersionsSearchResponse extends AppApiKeyVersionsSearchResults<AppApiKeyListItemDto> {
  @ApiProperty({ type: [AppApiKeyListItemDto] })
  items: AppApiKeyListItemDto[]
}
