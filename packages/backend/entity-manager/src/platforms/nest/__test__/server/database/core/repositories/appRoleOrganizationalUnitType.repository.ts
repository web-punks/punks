import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { WpEntityRepository, NestTypeOrmRepository } from "../../../../.."
import {
  AppRoleOrganizationalUnitTypeEntity,
  AppRoleOrganizationalUnitTypeEntityId,
} from "../entities/appRoleOrganizationalUnitType.entity"

@WpEntityRepository("appRoleOrganizationalUnitType")
export class AppRoleOrganizationalUnitTypeRepository extends NestTypeOrmRepository<
  AppRoleOrganizationalUnitTypeEntity,
  AppRoleOrganizationalUnitTypeEntityId
> {
  constructor(
    @InjectRepository(AppRoleOrganizationalUnitTypeEntity, "core")
    repository: Repository<AppRoleOrganizationalUnitTypeEntity>
  ) {
    super(repository)
  }
}
