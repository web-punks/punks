import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppPermissionCreateData,
  AppPermissionCursor,
  AppPermissionFacets,
  AppPermissionSorting,
  AppPermissionUpdateData,
} from "./appPermission.models"
import {
  AppPermissionSearchParameters,
  AppPermissionDeleteParameters,
} from "./appPermission.types"
import {
  AppPermissionEntity,
  AppPermissionEntityId,
} from "../../database/core/entities/appPermission.entity"

@WpEntityManager("appPermission")
export class AppPermissionEntityManager extends NestEntityManager<
  AppPermissionEntity,
  AppPermissionEntityId,
  AppPermissionCreateData,
  AppPermissionUpdateData,
  AppPermissionDeleteParameters,
  AppPermissionSearchParameters,
  AppPermissionSorting,
  AppPermissionCursor,
  AppPermissionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appPermission", registry)
  }
}
