import { IEntityConverter, WpEntityConverter } from "../../../../../.."
import {
  AppUserGroupMemberCreateDto,
  AppUserGroupMemberDto,
  AppUserGroupMemberListItemDto,
  AppUserGroupMemberUpdateDto,
} from "./appUserGroupMember.dto"
import { AppUserGroupMemberEntity } from "../../database/core/entities/appUserGroupMember.entity"

@WpEntityConverter("appUserGroupMember")
export class AppUserGroupMemberConverter
  implements
    IEntityConverter<
      AppUserGroupMemberEntity,
      AppUserGroupMemberDto,
      AppUserGroupMemberListItemDto,
      AppUserGroupMemberCreateDto,
      AppUserGroupMemberUpdateDto
    >
{
  toListItemDto(
    entity: AppUserGroupMemberEntity
  ): AppUserGroupMemberListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppUserGroupMemberEntity): AppUserGroupMemberDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(
    input: AppUserGroupMemberCreateDto
  ): Partial<AppUserGroupMemberEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(
    input: AppUserGroupMemberUpdateDto
  ): Partial<AppUserGroupMemberEntity> {
    return {
      ...input,
    }
  }
}
