import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../../.."
import { AppDivisionEntity } from "../../database/core/entities/appDivision.entity"
import {
  AppDivisionEntityId,
  AppDivisionFacets,
  AppDivisionSearchParameters,
  AppDivisionSorting,
} from "./appDivision.models"
import {
  AppUserContext,
  AppAuthContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appDivision")
export class AppDivisionQueryBuilder extends NestTypeOrmQueryBuilder<
  AppDivisionEntity,
  AppDivisionEntityId,
  AppDivisionSearchParameters,
  AppDivisionSorting,
  AppDivisionFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDivision", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext | undefined
  ):
    | FindOptionsWhere<AppDivisionEntity>
    | FindOptionsWhere<AppDivisionEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppDivisionSearchParameters
  ): FindOptionsOrder<AppDivisionEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppDivisionSearchParameters
  ):
    | FindOptionsWhere<AppDivisionEntity>
    | FindOptionsWhere<AppDivisionEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppDivisionSearchParameters,
    context?: AppAuthContext
  ): Promise<AppDivisionFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
