import {
  DeepPartial,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppUserEntity } from "../../database/core/entities/appUser.entity"
import { IEntitiesDeleteParameters } from "../../../../../../abstractions/commands"

export type AppUserEntityId = string

export type AppUserCreateData = DeepPartial<Omit<AppUserEntity, "id">>
export type AppUserUpdateData = DeepPartial<Omit<AppUserEntity, "id">>

export class AppUserFacets {}

export class AppUserSearchFilters {
  @ApiProperty({ required: false })
  email?: string

  @ApiProperty({ required: false })
  userName?: string

  @ApiProperty({ required: false })
  organizationId?: string

  @ApiProperty({ required: false })
  organizationUid?: string

  @ApiProperty({ required: false })
  tenantId?: string

  @ApiProperty({ required: false })
  tenantUid?: string

  @ApiProperty({ required: false })
  directoryId?: string

  @ApiProperty({ required: false })
  directoryUid?: string
}

export enum AppUserSorting {
  Name = "Name",
}

export type AppUserCursor = number

export class AppUserSearchSortingField
  implements ISearchSortingField<AppUserSorting>
{
  @ApiProperty({ enum: AppUserSorting })
  field: AppUserSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppUserQuerySorting implements ISearchSorting<AppUserSorting> {
  @ApiProperty({ required: false, type: [AppUserSearchSortingField] })
  fields: AppUserSearchSortingField[]
}

export class AppUserQueryPaging implements ISearchRequestPaging<AppUserCursor> {
  @ApiProperty({ required: false })
  cursor?: AppUserCursor

  @ApiProperty()
  pageSize: number
}

export class AppUserSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppUserSearchParameters
  implements
    IEntitySearchParameters<AppUserEntity, AppUserSorting, AppUserCursor>
{
  @ApiProperty({ required: false })
  filters?: AppUserSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserQuerySorting

  @ApiProperty({ required: false })
  paging?: AppUserQueryPaging

  @ApiProperty({ required: false })
  options?: AppUserSearchOptions
}

export class AppUserSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppUserCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserCursor
}

export class AppUserSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppUserEntity,
      AppUserSearchParameters,
      TResult,
      AppUserSorting,
      AppUserCursor,
      AppUserFacets
    >
{
  @ApiProperty()
  request: AppUserSearchParameters

  @ApiProperty()
  facets?: AppUserFacets

  @ApiProperty()
  paging?: AppUserSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppUserDeleteParameters
  implements IEntitiesDeleteParameters<AppUserSorting>
{
  @ApiProperty({ required: false })
  filters?: AppUserSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserQuerySorting
}

export type AppUserSheetItem = AppUserEntity
