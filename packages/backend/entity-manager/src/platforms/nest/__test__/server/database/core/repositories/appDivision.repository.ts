import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { NestTypeOrmRepository, WpEntityRepository } from "../../../../.."
import { AppDivisionEntity } from "../entities/appDivision.entity"
import { AppDivisionEntityId } from "../../../entities/appDivisions/appDivision.models"

@WpEntityRepository("appDivision")
export class AppDivisionRepository extends NestTypeOrmRepository<
  AppDivisionEntity,
  AppDivisionEntityId
> {
  constructor(
    @InjectRepository(AppDivisionEntity, "core")
    repository: Repository<AppDivisionEntity>
  ) {
    super(repository)
  }
}
