import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppDivisionCreateDto,
  AppDivisionDto,
  AppDivisionListItemDto,
  AppDivisionUpdateDto,
} from "./appDivision.dto"
import { AppDivisionEntity } from "../../database/core/entities/appDivision.entity"
import {
  AppDivisionCursor,
  AppDivisionDeleteParameters,
  AppDivisionEntityId,
  AppDivisionFacets,
  AppDivisionSearchParameters,
  AppDivisionSorting,
} from "../../entities/appDivisions/appDivision.models"

@WpEntityActions("appDivision")
export class AppDivisionActions extends NestEntityActions<
  AppDivisionEntity,
  AppDivisionEntityId,
  AppDivisionCreateDto,
  AppDivisionUpdateDto,
  AppDivisionDto,
  AppDivisionListItemDto,
  AppDivisionDeleteParameters,
  AppDivisionSearchParameters,
  AppDivisionSorting,
  AppDivisionCursor,
  AppDivisionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDivision", registry)
  }
}
