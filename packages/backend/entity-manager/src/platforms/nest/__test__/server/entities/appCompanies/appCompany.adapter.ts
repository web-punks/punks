import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppCompanyEntity } from "../../database/core/entities/appCompany.entity"
import { AppCompanyCreateData, AppCompanyUpdateData } from "./appCompany.models"

@WpEntityAdapter("appCompany")
export class AppCompanyAdapter
  implements
    IEntityAdapter<
      AppCompanyEntity,
      AppCompanyCreateData,
      AppCompanyUpdateData
    >
{
  createDataToEntity(
    data: AppCompanyCreateData
  ): DeepPartial<AppCompanyEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppCompanyUpdateData
  ): DeepPartial<AppCompanyEntity> {
    return {
      ...data,
    }
  }
}
