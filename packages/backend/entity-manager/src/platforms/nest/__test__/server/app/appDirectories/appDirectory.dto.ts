import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "../../../../../.."
import {
  AppDirectorySearchParameters,
  AppDirectorySearchResults,
  AppDirectoryVersionsSearchParameters,
  AppDirectoryVersionsSearchResults,
} from "../../entities/appDirectories/appDirectory.types"

export class AppDirectoryDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppDirectoryListItemDto {
  @ApiProperty()
  id: string
}

export class AppDirectoryCreateDto {}

export class AppDirectoryUpdateDto {
  @ApiProperty()
  id: string
}

export class AppDirectorySearchRequest {
  @ApiProperty()
  params: AppDirectorySearchParameters
}

export class AppDirectorySearchResponse extends AppDirectorySearchResults<AppDirectoryListItemDto> {
  @ApiProperty({ type: [AppDirectoryListItemDto] })
  items: AppDirectoryListItemDto[]
}

export class AppDirectoryEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppDirectoryExportRequest {
  @ApiProperty()
  options: AppDirectoryEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppDirectorySearchParameters
}

export class AppDirectoryExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppDirectorySampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppDirectorySampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppDirectoryVersionsSearchRequest {
  @ApiProperty()
  params: AppDirectoryVersionsSearchParameters
}

export class AppDirectoryVersionsSearchResponse extends AppDirectoryVersionsSearchResults<AppDirectoryListItemDto> {
  @ApiProperty({ type: [AppDirectoryListItemDto] })
  items: AppDirectoryListItemDto[]
}
