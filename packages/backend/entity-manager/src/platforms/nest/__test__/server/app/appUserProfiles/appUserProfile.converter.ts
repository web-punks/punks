import { IEntityConverter, WpEntityConverter } from "../../../../../.."
import {
  AppUserProfileCreateDto,
  AppUserProfileDto,
  AppUserProfileListItemDto,
  AppUserProfileUpdateDto,
} from "./appUserProfile.dto"
import { AppUserProfileEntity } from "../../database/core/entities/appUserProfile.entity"

@WpEntityConverter("appUserProfile")
export class AppUserProfileConverter
  implements
    IEntityConverter<
      AppUserProfileEntity,
      AppUserProfileDto,
      AppUserProfileListItemDto,
      AppUserProfileCreateDto,
      AppUserProfileUpdateDto
    >
{
  toListItemDto(entity: AppUserProfileEntity): AppUserProfileListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppUserProfileEntity): AppUserProfileDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(
    input: AppUserProfileCreateDto
  ): Partial<AppUserProfileEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(
    input: AppUserProfileUpdateDto
  ): Partial<AppUserProfileEntity> {
    return {
      ...input,
    }
  }
}
