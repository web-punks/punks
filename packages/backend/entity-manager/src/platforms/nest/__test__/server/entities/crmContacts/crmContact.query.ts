import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../../.."
import { CrmContactEntity } from "../../database/core/entities/crmContact.entity"
import {
  CrmContactEntityId,
  CrmContactFacets,
  CrmContactSorting,
} from "./crmContact.models"
import { CrmContactSearchParameters } from "./crmContact.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("crmContact")
export class CrmContactQueryBuilder extends NestTypeOrmQueryBuilder<
  CrmContactEntity,
  CrmContactEntityId,
  CrmContactSearchParameters,
  CrmContactSorting,
  CrmContactFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("crmContact", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<CrmContactEntity> | FindOptionsWhere<CrmContactEntity>[] {
    return {
      ...(context?.userContext?.organizationId
        ? {
            organization: {
              id: context?.userContext?.organizationId,
            },
          }
        : {}),
    }
  }

  protected buildSortingClause(
    request: CrmContactSearchParameters
  ): FindOptionsOrder<CrmContactEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: CrmContactSearchParameters
  ): FindOptionsWhere<CrmContactEntity> | FindOptionsWhere<CrmContactEntity>[] {
    return {}
  }

  protected async calculateFacets(
    request: CrmContactSearchParameters,
    context?: AppAuthContext
  ): Promise<CrmContactFacets> {
    return {
      email: await this.calculateStringFieldFacets({
        field: "email",
        request,
        context,
      }),
    }
  }
}
