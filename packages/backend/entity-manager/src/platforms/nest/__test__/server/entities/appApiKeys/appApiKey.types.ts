import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppApiKeyEntity } from "../../database/core/entities/appApiKey.entity"
import {
  AppApiKeySorting,
  AppApiKeyCursor,
  AppApiKeySearchFilters,
  AppApiKeyFacets,
} from "./appApiKey.models"
import {
  EntityVersionsReference,
  EntityVersionsFilters,
  EntityVersionsSorting,
  EntityVersionsCursor,
} from "../../shared/api/versioning"

export class AppApiKeySearchSortingField
  implements ISearchSortingField<AppApiKeySorting>
{
  @ApiProperty({ enum: AppApiKeySorting })
  field: AppApiKeySorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppApiKeyQuerySorting implements ISearchSorting<AppApiKeySorting> {
  @ApiProperty({ required: false, type: [AppApiKeySearchSortingField] })
  fields: AppApiKeySearchSortingField[]
}

export class AppApiKeyQueryPaging
  implements ISearchRequestPaging<AppApiKeyCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppApiKeyCursor

  @ApiProperty()
  pageSize: number
}

export class AppApiKeySearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppApiKeyFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppApiKeySearchParameters
  implements
    IEntitySearchParameters<AppApiKeyEntity, AppApiKeySorting, AppApiKeyCursor>
{
  @ApiProperty({ required: false })
  query?: AppApiKeyFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppApiKeySearchFilters

  @ApiProperty({ required: false })
  sorting?: AppApiKeyQuerySorting

  @ApiProperty({ required: false })
  paging?: AppApiKeyQueryPaging

  @ApiProperty({ required: false })
  options?: AppApiKeySearchOptions

  relations?: ISearchQueryRelations<AppApiKeyEntity>
}

export class AppApiKeySearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppApiKeyCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppApiKeyCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppApiKeyCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppApiKeyCursor
}

export class AppApiKeySearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppApiKeyEntity,
      AppApiKeySearchParameters,
      TResult,
      AppApiKeySorting,
      AppApiKeyCursor,
      AppApiKeyFacets
    >
{
  @ApiProperty()
  request: AppApiKeySearchParameters

  @ApiProperty()
  facets?: AppApiKeyFacets

  @ApiProperty()
  paging?: AppApiKeySearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppApiKeyDeleteParameters
  implements IEntitiesDeleteParameters<AppApiKeySorting>
{
  @ApiProperty({ required: false })
  filters?: AppApiKeySearchFilters

  @ApiProperty({ required: false })
  sorting?: AppApiKeyQuerySorting
}

export class AppApiKeyVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppApiKeyCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppApiKeyCursor>
}

export class AppApiKeyVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppApiKeyCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppApiKeyCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppApiKeyCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppApiKeyCursor
}

export class AppApiKeyVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppApiKeyCursor>
{
  @ApiProperty()
  paging?: AppApiKeyVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
