import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppOrganizationActions } from "./appOrganization.actions"
import { AppOrganizationController } from "./appOrganization.controller"
import { AppOrganizationConverter } from "./appOrganization.converter"

@Module({
  imports: [SharedModule],
  providers: [AppOrganizationActions, AppOrganizationConverter],
  controllers: [AppOrganizationController],
})
export class AppOrganizationAppModule {}
