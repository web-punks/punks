import { IEntityConverter, WpEntityConverter } from "../../../../../.."
import {
  AppOrganizationCreateDto,
  AppOrganizationDto,
  AppOrganizationListItemDto,
  AppOrganizationReferenceDto,
  AppOrganizationUpdateDto,
} from "./appOrganization.dto"
import { AppOrganizationEntity } from "../../database/core/entities/appOrganization.entity"

export const toAppOrganizationReferenceDto = (
  entity: AppOrganizationEntity
): AppOrganizationReferenceDto => ({
  id: entity.id,
  name: entity.name,
  uid: entity.uid,
})

@WpEntityConverter("appOrganization")
export class AppOrganizationConverter
  implements
    IEntityConverter<
      AppOrganizationEntity,
      AppOrganizationDto,
      AppOrganizationListItemDto,
      AppOrganizationCreateDto,
      AppOrganizationUpdateDto
    >
{
  toListItemDto(entity: AppOrganizationEntity): AppOrganizationListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppOrganizationEntity): AppOrganizationDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(
    input: AppOrganizationCreateDto
  ): Partial<AppOrganizationEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(
    input: AppOrganizationUpdateDto
  ): Partial<AppOrganizationEntity> {
    return {
      ...input,
    }
  }
}
