import { PipelineDefinition } from "../../../../../../../../types"
import { WpPipeline } from "../../../../../../decorators"
import { AuthenticationService } from "../../../../../../extensions"
import {
  IPipelineTemplateBuilder,
  NestPipelineTemplate,
} from "../../../../../../pipelines"
import { AppUserEntity } from "../../../../database/core/entities/appUser.entity"
import { AppUserProfileEntityManager } from "../../../../entities/appUserProfiles/appUserProfile.manager"
import { AppUserEntityManager } from "../../../../entities/appUsers/appUser.manager"
import { AppAuthContext } from "../../../../infrastructure/authentication"
import { UserUpdatePipelineInput } from "./models"

@WpPipeline("UserUpdate")
export class UserUpdateTemplate extends NestPipelineTemplate<
  UserUpdatePipelineInput,
  void,
  AppAuthContext
> {
  constructor(
    private readonly auth: AuthenticationService,
    private readonly users: AppUserEntityManager,
    private readonly userProfiles: AppUserProfileEntityManager
  ) {
    super()
  }

  protected isAuthorized(context: AppAuthContext) {
    return true
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<UserUpdatePipelineInput, AppAuthContext>
  ): PipelineDefinition<UserUpdatePipelineInput, void, AppAuthContext> {
    return builder
      .addStep((step) => {
        step.addOperation({
          name: "User update",
          action: async (input) =>
            this.users.manager.update.execute(input.userId, {
              email: input.data.email,
              userName: input.data.userName,
            }),
        })
      })
      .addStep<AppUserEntity>((step) => {
        step.addOperation({
          name: "User get",
          action: async (_, state) =>
            (await this.users.manager.get.execute(
              state.pipelineInput.userId
            )) as AppUserEntity,
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "User profile update",
          action: async (input, state) =>
            this.userProfiles.manager.update.execute(input.profile.id, {
              firstName: state.pipelineInput.data.fistName,
              lastName: state.pipelineInput.data.lastName,
            }),
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Role update",
          action: async (_, state) =>
            this.auth.userRolesService.addUserToRoleByUid(
              state.pipelineInput.userId,
              state.pipelineInput.data.roleUid
            ),
        })
      })
      .complete()
  }
}
