import { ApiProperty } from "@nestjs/swagger"
import {
  AppUserGroupSearchParameters,
  AppUserGroupSearchResults,
} from "../../entities/appUserGroups/appUserGroup.models"

export class AppUserGroupDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserGroupListItemDto {
  @ApiProperty()
  id: string
}

export class AppUserGroupCreateDto {}

export class AppUserGroupUpdateDto {
  @ApiProperty()
  id: string
}

export class AppUserGroupSearchRequest {
  @ApiProperty()
  params: AppUserGroupSearchParameters
}

export class AppUserGroupSearchResponse extends AppUserGroupSearchResults<AppUserGroupListItemDto> {
  @ApiProperty({ type: [AppUserGroupListItemDto] })
  items: AppUserGroupListItemDto[]
}
