import { ApiProperty } from "@nestjs/swagger"
import {
  AppRoleSearchParameters,
  AppRoleSearchResults,
} from "../../entities/appRoles/appRole.models"

export class AppRoleDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppRoleListItemDto {
  @ApiProperty()
  id: string
}

export class AppRoleCreateDto {}

export class AppRoleUpdateDto {
  @ApiProperty()
  id: string
}

export class AppRoleSearchRequest {
  @ApiProperty()
  params: AppRoleSearchParameters
}

export class AppRoleSearchResponse extends AppRoleSearchResults<AppRoleListItemDto> {
  @ApiProperty({ type: [AppRoleListItemDto] })
  items: AppRoleListItemDto[]
}
