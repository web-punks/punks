import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppOrganizationalUnitCreateData,
  AppOrganizationalUnitCursor,
  AppOrganizationalUnitFacets,
  AppOrganizationalUnitSorting,
  AppOrganizationalUnitUpdateData,
} from "./appOrganizationalUnit.models"
import {
  AppOrganizationalUnitSearchParameters,
  AppOrganizationalUnitDeleteParameters,
} from "./appOrganizationalUnit.types"
import {
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitEntityId,
} from "../../database/core/entities/appOrganizationalUnit.entity"

@WpEntityManager("appOrganizationalUnit")
export class AppOrganizationalUnitEntityManager extends NestEntityManager<
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitEntityId,
  AppOrganizationalUnitCreateData,
  AppOrganizationalUnitUpdateData,
  AppOrganizationalUnitDeleteParameters,
  AppOrganizationalUnitSearchParameters,
  AppOrganizationalUnitSorting,
  AppOrganizationalUnitCursor,
  AppOrganizationalUnitFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganizationalUnit", registry)
  }
}
