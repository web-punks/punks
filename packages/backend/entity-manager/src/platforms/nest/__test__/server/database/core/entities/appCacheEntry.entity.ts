import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from "typeorm"

@Entity("appCacheEntry")
export class AppCacheEntry {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255 })
  instance: string

  @Column({ type: "varchar", length: 255 })
  key: string

  @Column({
    type: "jsonb",
    nullable: true,
  })
  data: any

  @Column({ type: "timestamptz" })
  expirationTime: Date

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
