import { Module } from "@nestjs/common"
import { AppRoleAdapter } from "./appRole.adapter"
import { AppRoleAuthMiddleware } from "./appRole.authentication"
import { AppRoleEntityManager } from "./appRole.manager"
import { AppRoleQueryBuilder } from "./appRole.query"
import { EntityManagerModule } from "../../../.."
import { AppRoleSerializer } from "./appRole.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppRoleAdapter,
    AppRoleAuthMiddleware,
    AppRoleEntityManager,
    AppRoleQueryBuilder,
    AppRoleSerializer,
  ],
  exports: [AppRoleEntityManager],
})
export class AppRoleEntityModule {}
