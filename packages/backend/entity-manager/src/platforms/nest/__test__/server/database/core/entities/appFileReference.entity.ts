import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "../../../../../decorators"

export type AppFileReferenceEntityId = string

@Entity("appFileReferences")
@WpEntity("appFileReference", {
  versioning: {
    enabled: true,
  },
})
export class AppFileReferenceEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar" })
  reference: string

  @Column({ type: "varchar", length: 255 })
  provider: string

  @Column({ type: "varchar" })
  folder: string

  @Column({ type: "varchar" })
  fileName: string

  @Column({ type: "varchar", length: 255 })
  contentType: string

  @Column({ type: "bigint" })
  fileSize: number

  @Column({
    type: "jsonb",
    nullable: true,
  })
  metadata?: any

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
