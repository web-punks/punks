import { ApiProperty } from "@nestjs/swagger"

export class AuthUserRegistrationData {
  @ApiProperty()
  firstName: string

  @ApiProperty()
  lastName: string

  @ApiProperty()
  birthDate: string
}

export class AuthUserContext {
  @ApiProperty()
  tenantUid: string

  @ApiProperty({ required: false })
  organizationUid?: string
}
