import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppUserRoleEntity } from "../../database/core/entities/appUserRole.entity"
import {
  AppUserRoleCreateData,
  AppUserRoleUpdateData,
} from "./appUserRole.models"

@WpEntityAdapter("appUserRole")
export class AppUserRoleAdapter
  implements
    IEntityAdapter<
      AppUserRoleEntity,
      AppUserRoleCreateData,
      AppUserRoleUpdateData
    >
{
  createDataToEntity(
    data: AppUserRoleCreateData
  ): DeepPartial<AppUserRoleEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppUserRoleUpdateData
  ): DeepPartial<AppUserRoleEntity> {
    return {
      ...data,
    }
  }
}
