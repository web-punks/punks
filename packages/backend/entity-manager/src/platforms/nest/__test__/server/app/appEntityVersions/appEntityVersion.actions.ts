import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../.."
import {
  AppEntityVersionCreateDto,
  AppEntityVersionDto,
  AppEntityVersionListItemDto,
  AppEntityVersionUpdateDto,
} from "./appEntityVersion.dto"
import { AppEntityVersionEntity } from "../../database/core/entities/appEntityVersion.entity"
import {
  AppEntityVersionCursor,
  AppEntityVersionEntityId,
  AppEntityVersionFacets,
  AppEntityVersionSorting,
} from "../../entities/appEntityVersions/appEntityVersion.models"
import {
  AppEntityVersionDeleteParameters,
  AppEntityVersionSearchParameters,
} from "../../entities/appEntityVersions/appEntityVersion.types"

@WpEntityActions("appEntityVersion")
export class AppEntityVersionActions extends NestEntityActions<
  AppEntityVersionEntity,
  AppEntityVersionEntityId,
  AppEntityVersionCreateDto,
  AppEntityVersionUpdateDto,
  AppEntityVersionDto,
  AppEntityVersionListItemDto,
  AppEntityVersionDeleteParameters,
  AppEntityVersionSearchParameters,
  AppEntityVersionSorting,
  AppEntityVersionCursor,
  AppEntityVersionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEntityVersion", registry)
  }
}
