import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { AppOrganizationalUnitTypeAdapter } from "./appOrganizationalUnitType.adapter"
import { AppOrganizationalUnitTypeAuthMiddleware } from "./appOrganizationalUnitType.authentication"
import { AppOrganizationalUnitTypeEntityManager } from "./appOrganizationalUnitType.manager"
import { AppOrganizationalUnitTypeQueryBuilder } from "./appOrganizationalUnitType.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppOrganizationalUnitTypeAdapter,
    AppOrganizationalUnitTypeAuthMiddleware,
    AppOrganizationalUnitTypeEntityManager,
    AppOrganizationalUnitTypeQueryBuilder,
  ],
  exports: [AppOrganizationalUnitTypeEntityManager],
})
export class AppOrganizationalUnitTypeEntityModule {}
