import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { WpEntityRepository, NestTypeOrmRepository } from "../../../../.."
import {
  AppApiKeyEntity,
  AppApiKeyEntityId,
} from "../entities/appApiKey.entity"

@WpEntityRepository("appApiKey")
export class AppApiKeyRepository extends NestTypeOrmRepository<
  AppApiKeyEntity,
  AppApiKeyEntityId
> {
  constructor(
    @InjectRepository(AppApiKeyEntity, "core")
    repository: Repository<AppApiKeyEntity>
  ) {
    super(repository)
  }
}
