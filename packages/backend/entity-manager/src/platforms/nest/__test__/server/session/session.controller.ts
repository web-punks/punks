import { Body, Controller, Get, Post } from "@nestjs/common"
import { AsyncLocalStorage } from "async_hooks"
import { sleep } from "@punks/backend-core"
import { AppSessionService } from "../../../services"

// Define the types for the context and result
interface Context {
  data: any
}

// Create an instance of AsyncLocalStorage to store the context
const asyncLocalStorage = new AsyncLocalStorage<Context>()

@Controller("session")
export class SessionController {
  constructor(private readonly session: AppSessionService) {}

  @Post("getAndSetUserSessionDataFromService")
  async getAndSetUserSessionDataFromService(@Body() data: any) {
    this.session.setValue("data", data)
    await sleep(500)
    return this.session.getValue("data")
  }

  @Post("getAndSetUserSessionData")
  async getAndSetUserSessionData(@Body() data: any) {
    return this.runWithContext({ data }, async () => {
      await sleep(500)
      return this.getContext().data
    })
  }

  // Helper method to execute a function within a specific context
  private async runWithContext(context: Context, fn: () => Promise<any>) {
    return asyncLocalStorage.run(context, async () => {
      return await fn()
    })
  }

  // Helper method to get the current context
  private getContext(): Context {
    const context = asyncLocalStorage.getStore()
    if (!context) {
      throw new Error("No active context found")
    }
    return context
  }
}
