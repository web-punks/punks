import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppCompanyCreateData,
  AppCompanyCursor,
  AppCompanyDeleteParameters,
  AppCompanyEntityId,
  AppCompanyFacets,
  AppCompanySearchParameters,
  AppCompanySorting,
  AppCompanyUpdateData,
} from "./appCompany.models"
import { AppCompanyEntity } from "../../database/core/entities/appCompany.entity"

@WpEntityManager("appCompany")
export class AppCompanyEntityManager extends NestEntityManager<
  AppCompanyEntity,
  AppCompanyEntityId,
  AppCompanyCreateData,
  AppCompanyUpdateData,
  AppCompanyDeleteParameters,
  AppCompanySearchParameters,
  AppCompanySorting,
  AppCompanyCursor,
  AppCompanyFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appCompany", registry)
  }
}
