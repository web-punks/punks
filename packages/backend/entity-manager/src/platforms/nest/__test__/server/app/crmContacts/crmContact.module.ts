import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { CrmContactActions } from "./crmContact.actions"
import { CrmContactController } from "./crmContact.controller"
import { CrmContactConverter } from "./crmContact.converter"
import { CrmContactEntityModule } from "../../entities/crmContacts/crmContact.module"

@Module({
  imports: [SharedModule, CrmContactEntityModule],
  providers: [CrmContactActions, CrmContactConverter],
  controllers: [CrmContactController],
})
export class CrmContactAppModule {}
