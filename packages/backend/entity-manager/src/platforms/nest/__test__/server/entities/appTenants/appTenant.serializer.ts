import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { AppTenantEntity } from "../../database/core/entities/appTenant.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppTenantCreateData,
  AppTenantCursor,
  AppTenantEntityId,
  AppTenantSearchParameters,
  AppTenantSheetItem,
  AppTenantSorting,
  AppTenantUpdateData,
} from "./appTenant.models"

@WpEntitySerializer("appTenant")
export class AppTenantSerializer extends NestEntitySerializer<
  AppTenantEntity,
  AppTenantEntityId,
  AppTenantCreateData,
  AppTenantUpdateData,
  AppTenantSearchParameters,
  AppTenantSorting,
  AppTenantCursor,
  AppTenantSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appTenant", registry)
  }

  protected async loadEntities(
    filters: AppTenantSearchParameters
  ): Promise<AppTenantEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppTenantEntity[]
  ): Promise<AppTenantEntity[]> {
    return entities
  }

  protected async importItem(
    item: AppTenantSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppTenantEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
