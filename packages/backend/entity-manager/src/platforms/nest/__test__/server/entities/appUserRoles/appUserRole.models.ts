import {
  DeepPartial,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppUserRoleEntity } from "../../database/core/entities/appUserRole.entity"
import { IEntitiesDeleteParameters } from "../../../../../../abstractions/commands"

export type AppUserRoleEntityId = string

export type AppUserRoleCreateData = DeepPartial<Omit<AppUserRoleEntity, "id">>
export type AppUserRoleUpdateData = DeepPartial<Omit<AppUserRoleEntity, "id">>

export class AppUserRoleFacets {}

export class AppUserRoleSearchFilters {
  @ApiProperty({ required: false })
  userId?: string

  @ApiProperty({ required: false, type: [String] })
  roleIds?: string[]
}

export enum AppUserRoleSorting {
  Name = "Name",
}

export type AppUserRoleCursor = number

export class AppUserRoleSearchSortingField
  implements ISearchSortingField<AppUserRoleSorting>
{
  @ApiProperty({ enum: AppUserRoleSorting })
  field: AppUserRoleSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppUserRoleQuerySorting
  implements ISearchSorting<AppUserRoleSorting>
{
  @ApiProperty({ required: false, type: [AppUserRoleSearchSortingField] })
  fields: AppUserRoleSearchSortingField[]
}

export class AppUserRoleQueryPaging
  implements ISearchRequestPaging<AppUserRoleCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppUserRoleCursor

  @ApiProperty()
  pageSize: number
}

export class AppUserRoleSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppUserRoleSearchParameters
  implements
    IEntitySearchParameters<
      AppUserRoleEntity,
      AppUserRoleSorting,
      AppUserRoleCursor
    >
{
  @ApiProperty({ required: false })
  filters?: AppUserRoleSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserRoleQuerySorting

  @ApiProperty({ required: false })
  paging?: AppUserRoleQueryPaging

  @ApiProperty({ required: false })
  options?: AppUserRoleSearchOptions
}

export class AppUserRoleSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppUserRoleCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserRoleCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserRoleCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserRoleCursor
}

export class AppUserRoleSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppUserRoleEntity,
      AppUserRoleSearchParameters,
      TResult,
      AppUserRoleSorting,
      AppUserRoleCursor,
      AppUserRoleFacets
    >
{
  @ApiProperty()
  request: AppUserRoleSearchParameters

  @ApiProperty()
  facets?: AppUserRoleFacets

  @ApiProperty()
  paging?: AppUserRoleSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppUserRoleDeleteParameters
  implements IEntitiesDeleteParameters<AppUserRoleSorting>
{
  @ApiProperty({ required: false })
  filters?: AppUserRoleSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserRoleQuerySorting
}

export type AppUserRoleSheetItem = AppUserRoleEntity
