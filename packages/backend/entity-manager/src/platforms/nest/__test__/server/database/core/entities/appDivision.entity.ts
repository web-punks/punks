import {
  Entity,
  Column,
  CreateDateColumn,
  ManyToOne,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "../../../../.."
import { AppCompanyEntity } from "./appCompany.entity"

@Entity("appDivisions")
@WpEntity("appDivision")
@Unique("divisionUid", ["company", "uid"])
export class AppDivisionEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255 })
  uid: string

  @Column({ type: "varchar", length: 255 })
  name: string

  @ManyToOne(() => AppCompanyEntity, (company) => company.divisions)
  company: AppCompanyEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
