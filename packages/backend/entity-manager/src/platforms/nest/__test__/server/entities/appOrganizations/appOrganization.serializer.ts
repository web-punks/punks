import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { AppOrganizationEntity } from "../../database/core/entities/appOrganization.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppOrganizationCreateData,
  AppOrganizationCursor,
  AppOrganizationEntityId,
  AppOrganizationSearchParameters,
  AppOrganizationSheetItem,
  AppOrganizationSorting,
  AppOrganizationUpdateData,
} from "./appOrganization.models"

@WpEntitySerializer("appOrganization")
export class AppOrganizationSerializer extends NestEntitySerializer<
  AppOrganizationEntity,
  AppOrganizationEntityId,
  AppOrganizationCreateData,
  AppOrganizationUpdateData,
  AppOrganizationSearchParameters,
  AppOrganizationSorting,
  AppOrganizationCursor,
  AppOrganizationSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganization", registry)
  }

  protected async loadEntities(
    filters: AppOrganizationSearchParameters
  ): Promise<AppOrganizationEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppOrganizationEntity[]
  ): Promise<AppOrganizationEntity[]> {
    return entities
  }

  protected async importItem(
    item: AppOrganizationSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppOrganizationEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
