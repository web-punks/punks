import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../../.."
import { AppUserGroupMemberEntity } from "../../database/core/entities/appUserGroupMember.entity"
import {
  AppUserGroupMemberEntityId,
  AppUserGroupMemberFacets,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSorting,
} from "./appUserGroupMember.models"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appUserGroupMember")
export class AppUserGroupMemberQueryBuilder extends NestTypeOrmQueryBuilder<
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSorting,
  AppUserGroupMemberFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroupMember", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext | undefined
  ):
    | FindOptionsWhere<AppUserGroupMemberEntity>
    | FindOptionsWhere<AppUserGroupMemberEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppUserGroupMemberSearchParameters
  ): FindOptionsOrder<AppUserGroupMemberEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppUserGroupMemberSearchParameters
  ):
    | FindOptionsWhere<AppUserGroupMemberEntity>
    | FindOptionsWhere<AppUserGroupMemberEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppUserGroupMemberSearchParameters,
    context?: AppAuthContext
  ): Promise<AppUserGroupMemberFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
