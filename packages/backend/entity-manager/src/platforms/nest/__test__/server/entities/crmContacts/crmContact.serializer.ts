import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { CrmContactEntity } from "../../database/core/entities/crmContact.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  CrmContactCreateData,
  CrmContactCursor,
  CrmContactEntityId,
  CrmContactSheetItem,
  CrmContactSorting,
  CrmContactUpdateData,
} from "./crmContact.models"
import { CrmContactSearchParameters } from "./crmContact.types"

@WpEntitySerializer("crmContact")
export class CrmContactSerializer extends NestEntitySerializer<
  CrmContactEntity,
  CrmContactEntityId,
  CrmContactCreateData,
  CrmContactUpdateData,
  CrmContactSearchParameters,
  CrmContactSorting,
  CrmContactCursor,
  CrmContactSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("crmContact", registry)
  }

  protected async loadEntities(
    filters: CrmContactSearchParameters
  ): Promise<CrmContactEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: CrmContactEntity[]
  ): Promise<CrmContactSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: CrmContactSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<CrmContactEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
