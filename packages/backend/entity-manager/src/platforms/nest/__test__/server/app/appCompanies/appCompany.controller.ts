import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { AppCompanyActions } from "./appCompany.actions"
import {
  AppCompanyCreateDto,
  AppCompanyDto,
  AppCompanySearchRequest,
  AppCompanySearchResponse,
  AppCompanyUpdateDto,
} from "./appCompany.dto"

@Controller("v1/appCompany")
export class AppCompanyController {
  constructor(private readonly actions: AppCompanyActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appCompanyGet",
  })
  @ApiOkResponse({
    type: AppCompanyDto,
  })
  async item(@Param("id") id: string): Promise<AppCompanyDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appCompanyCreate",
  })
  @ApiOkResponse({
    type: AppCompanyDto,
  })
  async create(@Body() data: AppCompanyCreateDto): Promise<AppCompanyDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppCompanyDto,
  })
  @ApiOperation({
    operationId: "appCompanyUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppCompanyUpdateDto
  ): Promise<AppCompanyDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appCompanyDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appCompanySearch",
  })
  @ApiOkResponse({
    type: AppCompanySearchResponse,
  })
  async search(
    @Body() request: AppCompanySearchRequest
  ): Promise<AppCompanySearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }
}
