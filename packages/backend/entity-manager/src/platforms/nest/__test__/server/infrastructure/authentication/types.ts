import {
  IAuthenticationContext,
  IAuthenticationOrganizationalUnit,
} from "../../../../../.."

export type AppUserInfo = {
  email: string
  firstName: string
  lastName: string
}

export type AppUserContext = {
  organizationId?: string
  organizationUid?: string
  tenantId: string
  tenantUid: string
}

export type AppUserRole = {
  uid: string
}

export class AppAuthContext implements IAuthenticationContext<AppUserContext> {
  isAuthenticated: boolean
  isAnonymous: boolean
  userId?: string
  userContext?: AppUserContext
  userInfo?: AppUserInfo
  userRoles?: AppUserRole[]
  userOrganizationalUnits?: IAuthenticationOrganizationalUnit[]
}
