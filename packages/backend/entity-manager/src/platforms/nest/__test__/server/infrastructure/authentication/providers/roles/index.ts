import { IAuthRoleService, WpRolesService } from "../../../../../../extensions"
import { AppRoleEntityManager } from "../../../../entities/appRoles/appRole.manager"
import { AppRoleEntity } from "../../../../database/core/entities/appRole.entity"
import { DeepPartial } from "../../../../../../../../types"
import { AppPermissionEntity } from "../../../../database/core/entities/appPermission.entity"
import { AppRolePermissionEntityManager } from "../../../../entities/appRolePermissions/appRolePermission.manager"
import { AppPermissionEntityManager } from "../../../../entities/appPermissions/appPermission.manager"

@WpRolesService()
export class AuthRolesService
  implements IAuthRoleService<AppRoleEntity, AppPermissionEntity>
{
  constructor(
    private readonly rolesManager: AppRoleEntityManager,
    private readonly permissionsManager: AppPermissionEntityManager,
    private readonly rolePermissionManager: AppRolePermissionEntityManager
  ) {}

  async ensure(
    uid: string,
    data: DeepPartial<Omit<AppRoleEntity, "uid">>
  ): Promise<AppRoleEntity> {
    const role = await this.getByUid(uid)
    if (role) {
      return role
    }
    return await this.create({
      ...data,
      uid,
    })
  }

  async create(data: DeepPartial<AppRoleEntity>): Promise<AppRoleEntity> {
    const ref = await this.rolesManager.manager.create.execute(data)
    const role = await this.rolesManager.manager.get.execute(ref.id)
    if (!role) {
      throw new Error(`Role not found with id ${ref.id}`)
    }
    return role
  }

  async update(id: string, data: DeepPartial<AppRoleEntity>): Promise<void> {
    await this.rolesManager.manager.update.execute(id, data)
  }

  async delete(id: string): Promise<void> {
    await this.rolesManager.manager.delete.execute(id)
  }

  async getById(id: string): Promise<AppRoleEntity | undefined> {
    return await this.rolesManager.manager.get.execute(id)
  }

  async getByUid(uid: string): Promise<AppRoleEntity | undefined> {
    const { items } = await this.rolesManager.manager.search.execute({
      filters: {
        uid: [uid],
      },
    })
    return items[0]
  }

  async addPermission(uid: string, permissionUid: string): Promise<void> {
    const role = await this.getByUid(uid)
    if (!role) {
      throw new Error(`Role not found with uid ${uid}`)
    }
    const permission = await this.getPermissionByUid(permissionUid)
    if (!permission) {
      throw new Error(`Permission not found with uid ${permissionUid}`)
    }

    await this.rolePermissionManager.manager.create.execute({
      role: {
        id: role.id,
      },
      permission: {
        id: permission.id,
      },
    })
  }

  async removePermission(uid: string, permissionUid: string): Promise<void> {
    const role = await this.getByUid(uid)
    if (!role) {
      throw new Error(`Role not found with uid ${uid}`)
    }
    const permission = await this.getPermissionByUid(permissionUid)
    if (!permission) {
      throw new Error(`Permission not found with uid ${permissionUid}`)
    }

    await this.rolePermissionManager.manager.deleteItems.execute({
      filters: {
        permissionIds: [permission.id],
        roleIds: [role.id],
      },
    })
  }

  async getPermissions(uid: string): Promise<AppPermissionEntity[]> {
    const { items } = await this.rolePermissionManager.manager.search.execute({
      filters: {
        roleIds: [uid],
      },
      relations: {
        permission: true,
      },
    })
    return items.map((x) => x.permission)
  }

  async getPermissionByUid(
    uid: string
  ): Promise<AppPermissionEntity | undefined> {
    return await this.permissionsManager.manager.find.execute({
      filters: {
        uid: {
          eq: uid,
        },
      },
    })
  }
}
