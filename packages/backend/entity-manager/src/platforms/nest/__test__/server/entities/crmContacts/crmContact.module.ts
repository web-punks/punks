import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { CrmContactAdapter } from "./crmContact.adapter"
import { CrmContactAuthMiddleware } from "./crmContact.authentication"
import { CrmContactEntityManager } from "./crmContact.manager"
import { CrmContactQueryBuilder } from "./crmContact.query"
import { CrmContactSerializer } from "./crmContact.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    CrmContactAdapter,
    CrmContactAuthMiddleware,
    CrmContactEntityManager,
    CrmContactQueryBuilder,
    CrmContactSerializer,
  ],
  exports: [CrmContactEntityManager],
})
export class CrmContactEntityModule {}
