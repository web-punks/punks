import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppDirectoryEntity } from "../../database/core/entities/appDirectory.entity"
import {
  AppDirectoryCreateData,
  AppDirectoryUpdateData,
} from "./appDirectory.models"

@WpEntityAdapter("appDirectory")
export class AppDirectoryAdapter
  implements
    IEntityAdapter<
      AppDirectoryEntity,
      AppDirectoryCreateData,
      AppDirectoryUpdateData
    >
{
  createDataToEntity(
    data: AppDirectoryCreateData
  ): DeepPartial<AppDirectoryEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppDirectoryUpdateData
  ): DeepPartial<AppDirectoryEntity> {
    return {
      ...data,
    }
  }
}
