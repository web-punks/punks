import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Unique,
} from "typeorm"
import { AppOrganizationalUnitTypeEntity } from "./appOrganizationalUnitType.entity"
import { WpEntity } from "../../../../../decorators"

export type AppOrganizationalUnitTypeChildEntityId = string

@Entity("appOrganizationalUnitTypeChildren")
@Unique("organizationalUnitTypeChild", ["parentType", "childType"])
@WpEntity("appOrganizationalUnitTypeChild")
export class AppOrganizationalUnitTypeChildEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppOrganizationalUnitTypeChildEntityId

  @ManyToOne(() => AppOrganizationalUnitTypeEntity, {
    nullable: false,
  })
  parentType: AppOrganizationalUnitTypeEntity

  @ManyToOne(() => AppOrganizationalUnitTypeEntity, {
    nullable: false,
  })
  childType: AppOrganizationalUnitTypeEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
