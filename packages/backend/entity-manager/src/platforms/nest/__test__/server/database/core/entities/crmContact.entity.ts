import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  OneToMany,
} from "typeorm"
import { WpEntity } from "../../../../../decorators"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { IMultiOrganizationEntity } from "../../../../../extensions"
import { FooEntity } from "./foo.entity"

@Entity("crmContacts")
@WpEntity("crmContact")
export class CrmContactEntity implements IMultiOrganizationEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255 })
  email: string

  @Column({ type: "varchar", length: 255 })
  firstName: string

  @Column({ type: "varchar", length: 255 })
  lastName: string

  @ManyToOne(() => AppOrganizationEntity, {
    eager: true,
  })
  organization: AppOrganizationEntity

  @OneToMany(() => FooEntity, (foos) => foos.contact)
  foos?: FooEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
