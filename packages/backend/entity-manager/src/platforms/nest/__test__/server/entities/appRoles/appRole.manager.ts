import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppRoleCreateData,
  AppRoleCursor,
  AppRoleDeleteParameters,
  AppRoleEntityId,
  AppRoleFacets,
  AppRoleSearchParameters,
  AppRoleSorting,
  AppRoleUpdateData,
} from "./appRole.models"
import { AppRoleEntity } from "../../database/core/entities/appRole.entity"

@WpEntityManager("appRole")
export class AppRoleEntityManager extends NestEntityManager<
  AppRoleEntity,
  AppRoleEntityId,
  AppRoleCreateData,
  AppRoleUpdateData,
  AppRoleDeleteParameters,
  AppRoleSearchParameters,
  AppRoleSorting,
  AppRoleCursor,
  AppRoleFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRole", registry)
  }

  async getByUid(uid: string): Promise<AppRoleEntity | undefined> {
    const { items } = await this.manager.search.execute({
      filters: { uid: [uid] },
    })
    return items[0]
  }
}
