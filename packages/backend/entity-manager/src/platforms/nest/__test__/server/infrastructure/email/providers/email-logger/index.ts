import { WpEmailLogger } from "../../../../../.."
import {
  HtmlEmailInput,
  IEmailLogger,
  TemplatedEmailInput,
} from "../../../../../../../.."
import { AppEmailLogEntityManager } from "../../../../entities/appEmailLogs/appEmailLog.manager"

@WpEmailLogger()
export class EmailLoggerProvider implements IEmailLogger {
  constructor(private readonly emailLogs: AppEmailLogEntityManager) {}

  async logTemplatedEmail<TPayload>(
    input: TemplatedEmailInput<TPayload>
  ): Promise<void> {
    await this.emailLogs.manager.create.execute({
      emailType: input.templateId,
      to: input.to,
      bcc: input.bcc,
      cc: input.cc,
      payload: input.payload,
    })
  }

  async logHtmlEmail<TPayload>(input: HtmlEmailInput<TPayload>): Promise<void> {
    await this.emailLogs.manager.create.execute({
      emailType: "custom",
      to: input.to,
      bcc: input.bcc,
      cc: input.cc,
      payload: input.payload,
    })
  }
}
