import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { CacheProviders } from "./providers"

@Module({
  imports: [SharedModule],
  providers: [...CacheProviders],
  exports: [],
})
export class CacheInfrastructureModule {}
