import { ApiProperty } from "@nestjs/swagger"
import { FileData } from "../../../../../../../../abstractions"

export interface FileUploadInput {
  file: FileData
}

export class FileUploadData {
  @ApiProperty()
  folderPath: string

  @ApiProperty()
  metadata?: Record<string, any>
}

export class FileUploadResponse {
  @ApiProperty()
  fileId: string
}
