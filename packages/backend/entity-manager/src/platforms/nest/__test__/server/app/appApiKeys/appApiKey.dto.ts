import { ApiProperty } from "@nestjs/swagger"
import { AppOrganizationReferenceDto } from "../appOrganizations/appOrganization.dto"

export class AppApiKeyDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  key: string

  @ApiProperty()
  name: string

  @ApiProperty()
  organization?: AppOrganizationReferenceDto

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppApiKeyListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  key: string

  @ApiProperty()
  name: string

  @ApiProperty()
  organization?: AppOrganizationReferenceDto

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppApiKeyCreateDto {
  @ApiProperty()
  organizationId: string

  @ApiProperty()
  name: string
}

export class AppApiKeyUpdateDto {
  @ApiProperty()
  organizationId: string

  @ApiProperty()
  name: string
}
