import { newUuid } from "@punks/backend-core"
import { IEntityConverter } from "../../../../../../abstractions"
import { WpEntityConverter } from "../../../../decorators"
import { FooEntity } from "../../database/core/entities/foo.entity"
import { FooCreateDto, FooDto, FooListItemDto, FooUpdateDto } from "./foo.dto"

@WpEntityConverter("foo")
export class FooConverter
  implements
    IEntityConverter<
      FooEntity,
      FooDto,
      FooListItemDto,
      FooCreateDto,
      FooUpdateDto
    >
{
  toListItemDto(entity: FooEntity): FooListItemDto {
    return {
      id: entity.id,
      profile: {
        name: entity.name,
        age: entity.age,
      },
    }
  }

  toEntityDto(entity: FooEntity): FooDto {
    return {
      id: entity.id,
      profile: {
        name: entity.name,
        age: entity.age,
      },
      updatedOn: entity.updatedOn,
    }
  }

  createDtoToEntity(input: FooCreateDto): Partial<FooEntity> {
    return {
      age: input.profile.age,
      name: input.profile.name,
      id: newUuid(),
      updatedOn: new Date(),
    }
  }

  updateDtoToEntity(input: FooUpdateDto): Partial<FooEntity> {
    return {
      id: input.id,
      age: input.profile.age,
      name: input.profile.name,
      updatedOn: new Date(),
    }
  }
}
