import request from "supertest"
import { Repository } from "typeorm"
import { createTestServer } from "../../../.."
import { NestTestServerPorts } from "../../../../../tests/ports"
import { AppTenantEntity } from "../../../../database/core/entities/appTenant.entity"
import { AppUserGroupEntityManager } from "../../../../entities/appUserGroups/appUserGroup.manager"
import { TenantInitializeRequest } from "./models"
import { AppUserEntity } from "../../../../database/core/entities/appUser.entity"
import { AppRoleEntity } from "../../../../database/core/entities/appRole.entity"
import { TenantInitializeTemplate } from "."
import { InstanceInitializeTemplate } from "../instance-initialize"
import { AppRoles } from "../../../../infrastructure/authentication/roles"

describe("Test Tenant Initialize Pipeline", () => {
  let httpServer: any
  let tenantsRepo: Repository<AppTenantEntity>
  let usersRepo: Repository<AppUserEntity>
  let rolesRepo: Repository<AppRoleEntity>
  let userGroupsManager: AppUserGroupEntityManager
  let template: TenantInitializeTemplate
  let instanceInit: InstanceInitializeTemplate
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { server, dataSource, app, terminate } = await createTestServer({
      useAuthentication: true,
      createWebServer: true,
      serverStartPort: NestTestServerPorts.TenantInitPipeline,
    })
    httpServer = app.getHttpServer()
    tenantsRepo = dataSource.getRepository(AppTenantEntity)
    usersRepo = dataSource.getRepository(AppUserEntity)
    rolesRepo = dataSource.getRepository(AppRoleEntity)
    userGroupsManager = server.get(AppUserGroupEntityManager)
    template = server.get(TenantInitializeTemplate)
    instanceInit = server.get(InstanceInitializeTemplate)
    terminateServer = terminate
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should run the pipeline template successfully", async () => {
    await instanceInit.invoke({
      roles: Object.values(AppRoles),
    })
    const result = await template.execute({
      input: {
        tenant: {
          name: "Test Tenant",
          uid: "test-tenant",
        },
        directory: {
          name: "Global",
          uid: "global",
        },
        defaultAdmin: {
          email: "prova@prova.it",
          firstName: "Test",
          lastName: "Test2",
          password: "Prova123!",
        },
      },
      context: {
        isAnonymous: true,
        isAuthenticated: false,
      },
    })

    expect(result.type).toBe("success")
  })

  it("should run the pipeline api successfully", async () => {
    const response = await request(httpServer)
      .post("/v1/appAdmin/tenantInitialize")
      .send({
        tenant: {
          name: "Test Tenant",
          uid: "test-tenant",
        },
        directory: {
          name: "Global",
          uid: "global",
        },
        defaultAdmin: {
          email: "prova@prova.it",
          firstName: "Test",
          lastName: "Test2",
          password: "Prova123!",
        },
      } as TenantInitializeRequest)
      .expect(201)

    expect(response.body).toMatchObject({
      tenantId: expect.any(String),
    })

    const tenant = await tenantsRepo.findOne({
      where: {
        id: response.body.tenantId,
      },
    })
    expect(tenant).toMatchObject({
      id: expect.any(String),
      name: "Test Tenant",
      uid: "test-tenant",
    })

    const user = await usersRepo.findOne({
      where: {
        email: "prova@prova.it",
      },
      relations: ["tenant", "profile", "userRoles", "userRoles.role"],
    })

    expect(user).toMatchObject({
      id: expect.any(String),
      tenant: {
        id: tenant!.id,
      },
      profile: {
        firstName: "Test",
        lastName: "Test2",
      },
      userRoles: [
        {
          role: {
            uid: "tenant-admin",
          },
        },
      ],
    })
  })
})
