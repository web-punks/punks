import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { WpEntityRepository, NestTypeOrmRepository } from "../../../../.."
import {
  AppEmailLogEntity,
  AppEmailLogEntityId,
} from "../entities/appEmailLog.entity"

@WpEntityRepository("appEmailLog")
export class AppEmailLogRepository extends NestTypeOrmRepository<
  AppEmailLogEntity,
  AppEmailLogEntityId
> {
  constructor(
    @InjectRepository(AppEmailLogEntity, "core")
    repository: Repository<AppEmailLogEntity>
  ) {
    super(repository)
  }
}
