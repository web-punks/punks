import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppUserGroupEntity } from "../../database/core/entities/appUserGroup.entity"
import {
  AppUserGroupCreateData,
  AppUserGroupUpdateData,
} from "./appUserGroup.models"

@WpEntityAdapter("appUserGroup")
export class AppUserGroupAdapter
  implements
    IEntityAdapter<
      AppUserGroupEntity,
      AppUserGroupCreateData,
      AppUserGroupUpdateData
    >
{
  createDataToEntity(
    data: AppUserGroupCreateData
  ): DeepPartial<AppUserGroupEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppUserGroupUpdateData
  ): DeepPartial<AppUserGroupEntity> {
    return {
      ...data,
    }
  }
}
