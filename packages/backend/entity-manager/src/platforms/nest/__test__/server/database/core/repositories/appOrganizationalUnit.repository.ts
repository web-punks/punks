import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { WpEntityRepository, NestTypeOrmRepository } from "../../../../.."
import {
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitEntityId,
} from "../entities/appOrganizationalUnit.entity"

@WpEntityRepository("appOrganizationalUnit")
export class AppOrganizationalUnitRepository extends NestTypeOrmRepository<
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitEntityId
> {
  constructor(
    @InjectRepository(AppOrganizationalUnitEntity, "core")
    repository: Repository<AppOrganizationalUnitEntity>
  ) {
    super(repository)
  }
}
