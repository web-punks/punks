import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppUserGroupMemberCreateData,
  AppUserGroupMemberCursor,
  AppUserGroupMemberDeleteParameters,
  AppUserGroupMemberEntityId,
  AppUserGroupMemberFacets,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSorting,
  AppUserGroupMemberUpdateData,
} from "./appUserGroupMember.models"
import { AppUserGroupMemberEntity } from "../../database/core/entities/appUserGroupMember.entity"

@WpEntityManager("appUserGroupMember")
export class AppUserGroupMemberEntityManager extends NestEntityManager<
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
  AppUserGroupMemberCreateData,
  AppUserGroupMemberUpdateData,
  AppUserGroupMemberDeleteParameters,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSorting,
  AppUserGroupMemberCursor,
  AppUserGroupMemberFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroupMember", registry)
  }
}
