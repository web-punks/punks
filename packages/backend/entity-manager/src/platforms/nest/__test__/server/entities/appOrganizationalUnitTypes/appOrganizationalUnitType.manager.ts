import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppOrganizationalUnitTypeCreateData,
  AppOrganizationalUnitTypeCursor,
  AppOrganizationalUnitTypeFacets,
  AppOrganizationalUnitTypeSorting,
  AppOrganizationalUnitTypeUpdateData,
} from "./appOrganizationalUnitType.models"
import {
  AppOrganizationalUnitTypeSearchParameters,
  AppOrganizationalUnitTypeDeleteParameters,
} from "./appOrganizationalUnitType.types"
import {
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId,
} from "../../database/core/entities/appOrganizationalUnitType.entity"

@WpEntityManager("appOrganizationalUnitType")
export class AppOrganizationalUnitTypeEntityManager extends NestEntityManager<
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeEntityId,
  AppOrganizationalUnitTypeCreateData,
  AppOrganizationalUnitTypeUpdateData,
  AppOrganizationalUnitTypeDeleteParameters,
  AppOrganizationalUnitTypeSearchParameters,
  AppOrganizationalUnitTypeSorting,
  AppOrganizationalUnitTypeCursor,
  AppOrganizationalUnitTypeFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganizationalUnitType", registry)
  }
}
