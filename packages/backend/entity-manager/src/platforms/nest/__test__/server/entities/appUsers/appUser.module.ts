import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../.."
import { AppUserAdapter } from "./appUser.adapter"
import { AppUserAuthMiddleware } from "./appUser.authentication"
import { AppUserEntityManager } from "./appUser.manager"
import { AppUserQueryBuilder } from "./appUser.query"
import { AppUserSerializer } from "./appUser.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppUserAdapter,
    AppUserAuthMiddleware,
    AppUserEntityManager,
    AppUserQueryBuilder,
    AppUserSerializer,
  ],
  exports: [AppUserEntityManager],
})
export class AppUserEntityModule {}
