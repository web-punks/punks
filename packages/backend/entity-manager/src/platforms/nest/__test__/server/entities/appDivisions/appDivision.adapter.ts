import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppDivisionEntity } from "../../database/core/entities/appDivision.entity"
import {
  AppDivisionCreateData,
  AppDivisionUpdateData,
} from "./appDivision.models"

@WpEntityAdapter("appDivision")
export class AppDivisionAdapter
  implements
    IEntityAdapter<
      AppDivisionEntity,
      AppDivisionCreateData,
      AppDivisionUpdateData
    >
{
  createDataToEntity(
    data: AppDivisionCreateData
  ): DeepPartial<AppDivisionEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppDivisionUpdateData
  ): DeepPartial<AppDivisionEntity> {
    return {
      ...data,
    }
  }
}
