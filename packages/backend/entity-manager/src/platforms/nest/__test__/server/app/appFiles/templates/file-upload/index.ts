import { PipelineDefinition } from "../../../../../../../../types"
import { WpPipeline } from "../../../../../../decorators"
import { IPipelineTemplateBuilder } from "../../../../../../pipelines/builder/types"
import { NestPipelineTemplate } from "../../../../../../pipelines/template"
import { AppAuthContext } from "../../../../infrastructure/authentication"
import { FileUploadInput, FileUploadResponse } from "./models"
import { FilesManager } from "../../../../../../services"

@WpPipeline("FileUpload")
export class FileUploadTemplate extends NestPipelineTemplate<
  FileUploadInput,
  FileUploadResponse,
  AppAuthContext
> {
  constructor(private readonly files: FilesManager) {
    super()
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<FileUploadInput, AppAuthContext>
  ): PipelineDefinition<FileUploadInput, FileUploadResponse, AppAuthContext> {
    return builder
      .addStep<FileUploadResponse>((step) => {
        step.addOperation({
          name: "File upload",
          action: async (input) => {
            const result = await this.files.uploadFile(input.file)
            return {
              fileId: result.fileId,
            }
          },
        })
      })
      .complete()
  }
}
