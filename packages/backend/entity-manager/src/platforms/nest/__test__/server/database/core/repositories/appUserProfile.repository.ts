import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { NestTypeOrmRepository, WpEntityRepository } from "../../../../.."
import { AppUserProfileEntity } from "../entities/appUserProfile.entity"
import { AppUserProfileEntityId } from "../../../entities/appUserProfiles/appUserProfile.models"

@WpEntityRepository("appUserProfile")
export class AppUserProfileRepository extends NestTypeOrmRepository<
  AppUserProfileEntity,
  AppUserProfileEntityId
> {
  constructor(
    @InjectRepository(AppUserProfileEntity, "core")
    repository: Repository<AppUserProfileEntity>
  ) {
    super(repository)
  }
}
