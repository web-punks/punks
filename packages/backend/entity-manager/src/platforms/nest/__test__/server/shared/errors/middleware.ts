import { Catch } from "@nestjs/common"
import {
  AppExceptionsFilterBase,
  RuntimeErrorInformation,
} from "../../../../middlewares"
import { Log } from "@punks/backend-core"

@Catch()
export class AppExceptionsFilter extends AppExceptionsFilterBase {
  protected async logError(info: RuntimeErrorInformation): Promise<void> {
    Log.getLogger("[AppExceptionsFilter]").error(
      "Internal Server Error",
      info.exception
    )
  }

  protected getCustomErrorStatusCode(exception: any): number | undefined {
    return undefined
  }
}
