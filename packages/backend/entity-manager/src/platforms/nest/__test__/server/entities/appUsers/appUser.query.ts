import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../../.."
import { AppUserEntity } from "../../database/core/entities/appUser.entity"
import {
  AppUserEntityId,
  AppUserFacets,
  AppUserSearchParameters,
  AppUserSorting,
} from "./appUser.models"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appUser")
export class AppUserQueryBuilder extends NestTypeOrmQueryBuilder<
  AppUserEntity,
  AppUserEntityId,
  AppUserSearchParameters,
  AppUserSorting,
  AppUserFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUser", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext | undefined
  ): FindOptionsWhere<AppUserEntity> | FindOptionsWhere<AppUserEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppUserSearchParameters
  ): FindOptionsOrder<AppUserEntity> {
    switch (request.sorting?.fields[0]?.field) {
      case AppUserSorting.Name:
        return {
          email: request.sorting?.fields[0].direction ?? "ASC",
        }
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppUserSearchParameters
  ): FindOptionsWhere<AppUserEntity> | FindOptionsWhere<AppUserEntity>[] {
    return {
      ...(request.filters?.email ? { email: request.filters.email } : {}),
      ...(request.filters?.userName
        ? { userName: request.filters.userName }
        : {}),
      ...(request.filters?.organizationId
        ? {
            organization: {
              id: request.filters.organizationId,
            },
          }
        : {}),
      ...(request.filters?.organizationUid
        ? {
            organization: {
              uid: request.filters.organizationUid,
            },
          }
        : {}),
    }
  }

  protected calculateFacets(
    request: AppUserSearchParameters,
    context?: AppAuthContext
  ): Promise<AppUserFacets> {
    return Promise.resolve({})
  }
}
