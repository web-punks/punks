import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { AppUserRoleEntity } from "../../database/core/entities/appUserRole.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppUserRoleCreateData,
  AppUserRoleCursor,
  AppUserRoleEntityId,
  AppUserRoleSearchParameters,
  AppUserRoleSheetItem,
  AppUserRoleSorting,
  AppUserRoleUpdateData,
} from "./appUserRole.models"

@WpEntitySerializer("appUserRole")
export class AppUserRoleSerializer extends NestEntitySerializer<
  AppUserRoleEntity,
  AppUserRoleEntityId,
  AppUserRoleCreateData,
  AppUserRoleUpdateData,
  AppUserRoleSearchParameters,
  AppUserRoleSorting,
  AppUserRoleCursor,
  AppUserRoleSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserRole", registry)
  }

  protected async loadEntities(
    filters: AppUserRoleSearchParameters
  ): Promise<AppUserRoleEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppUserRoleEntity[]
  ): Promise<AppUserRoleEntity[]> {
    return entities
  }

  protected async importItem(
    item: AppUserRoleSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppUserRoleEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
