import { applyDecorators, UseInterceptors } from "@nestjs/common"
import { FileInterceptor } from "@nestjs/platform-express"
import { MulterOptions } from "@nestjs/platform-express/multer/interfaces/multer-options.interface"
import { ApiBody, ApiConsumes, ApiProperty } from "@nestjs/swagger"

export const ApiFile =
  (): PropertyDecorator => (target: any, propertyKey: string | symbol) => {
    ApiProperty({
      type: "file",
      properties: {
        [propertyKey]: {
          type: "string",
          format: "binary",
        },
      },
    })(target, propertyKey)
  }

export function ApiBodyFile(
  fieldName = "file",
  required = false,
  localOptions?: MulterOptions
) {
  return applyDecorators(
    UseInterceptors(FileInterceptor(fieldName, localOptions)),
    ApiConsumes("multipart/form-data"),
    ApiBody({
      schema: {
        type: "object",
        required: required ? [fieldName] : [],
        properties: {
          [fieldName]: {
            type: "string",
            format: "binary",
          },
        },
      },
    })
  )
}
