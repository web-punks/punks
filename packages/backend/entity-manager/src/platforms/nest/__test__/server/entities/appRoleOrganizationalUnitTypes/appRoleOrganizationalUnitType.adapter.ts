import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppRoleOrganizationalUnitTypeEntity } from "../../database/core/entities/appRoleOrganizationalUnitType.entity"
import {
  AppRoleOrganizationalUnitTypeCreateData,
  AppRoleOrganizationalUnitTypeUpdateData,
} from "./appRoleOrganizationalUnitType.models"

@WpEntityAdapter("appRoleOrganizationalUnitType")
export class AppRoleOrganizationalUnitTypeAdapter
  implements
    IEntityAdapter<
      AppRoleOrganizationalUnitTypeEntity,
      AppRoleOrganizationalUnitTypeCreateData,
      AppRoleOrganizationalUnitTypeUpdateData
    >
{
  createDataToEntity(
    data: AppRoleOrganizationalUnitTypeCreateData
  ): DeepPartial<AppRoleOrganizationalUnitTypeEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppRoleOrganizationalUnitTypeUpdateData
  ): DeepPartial<AppRoleOrganizationalUnitTypeEntity> {
    return {
      ...data,
    }
  }
}
