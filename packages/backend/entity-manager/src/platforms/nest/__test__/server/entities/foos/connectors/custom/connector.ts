import {
  ConnectorMode,
  IEntityConnector,
} from "../../../../../../../../abstractions"
import { WpEntityConnector } from "../../../../../../decorators"
import {
  FooEntity,
  FooEntityId,
} from "../../../../database/core/entities/foo.entity"
import { FooMappedModel } from "./model"

@WpEntityConnector("foo", {
  connectorName: "custom",
  mode: ConnectorMode.Sync,
})
export class FooCustomConnector
  implements IEntityConnector<FooEntity, FooEntityId, FooMappedModel>
{
  private readonly syncQueue = [] as FooMappedModel[]
  private readonly deleteQueue = [] as FooEntityId[]

  constructor() {}

  async syncEntity(entity: FooMappedModel): Promise<void> {
    this.syncQueue.push(entity)
  }

  async deleteEntity(id: FooEntityId): Promise<void> {
    this.deleteQueue.push(id)
  }

  getSynchedElements() {
    return this.syncQueue
  }

  getDeletedElements() {
    return this.deleteQueue
  }
}
