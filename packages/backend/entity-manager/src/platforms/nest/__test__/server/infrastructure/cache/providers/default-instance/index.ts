import { InjectRepository } from "@nestjs/typeorm"
import { WpCacheInstance } from "../../../../../.."
import { TypeormCacheInstance } from "../../../../../../../../integrations"
import { AppCacheEntry } from "../../../../database/core/entities/appCacheEntry.entity"
import { Repository } from "typeorm"

@WpCacheInstance("default")
export class DefaultCacheInstance extends TypeormCacheInstance<AppCacheEntry> {
  constructor(
    @InjectRepository(AppCacheEntry, "core")
    private repository: Repository<AppCacheEntry>
  ) {
    super("default")
  }

  protected getRepository(): Repository<AppCacheEntry> {
    return this.repository
  }
}
