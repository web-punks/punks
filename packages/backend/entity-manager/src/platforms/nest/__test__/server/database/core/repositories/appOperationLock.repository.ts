import { Injectable } from "@nestjs/common"
import { InjectRepository } from "@nestjs/typeorm"
import { Repository } from "typeorm"
import { TypeormOperationLockRepository } from "../../../../../services/operations"
import { AppOperationLockItemEntry } from "../entities/appOperationLockItem.entity"

@Injectable()
export class AppOperationLockItemRepository extends TypeormOperationLockRepository<AppOperationLockItemEntry> {
  constructor(
    @InjectRepository(AppOperationLockItemEntry, "core")
    repo: Repository<AppOperationLockItemEntry>
  ) {
    super(repo, AppOperationLockItemEntry)
  }
}
