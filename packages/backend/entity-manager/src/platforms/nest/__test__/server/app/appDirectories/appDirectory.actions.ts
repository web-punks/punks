import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppDirectoryCreateDto,
  AppDirectoryDto,
  AppDirectoryListItemDto,
  AppDirectoryUpdateDto,
} from "./appDirectory.dto"
import { AppDirectoryEntity } from "../../database/core/entities/appDirectory.entity"
import {
  AppDirectoryCursor,
  AppDirectoryEntityId,
  AppDirectoryFacets,
  AppDirectorySorting,
} from "../../entities/appDirectories/appDirectory.models"
import {
  AppDirectoryDeleteParameters,
  AppDirectorySearchParameters,
} from "../../entities/appDirectories/appDirectory.types"

@WpEntityActions("appDirectory")
export class AppDirectoryActions extends NestEntityActions<
  AppDirectoryEntity,
  AppDirectoryEntityId,
  AppDirectoryCreateDto,
  AppDirectoryUpdateDto,
  AppDirectoryDto,
  AppDirectoryListItemDto,
  AppDirectoryDeleteParameters,
  AppDirectorySearchParameters,
  AppDirectorySorting,
  AppDirectoryCursor,
  AppDirectoryFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDirectory", registry)
  }
}
