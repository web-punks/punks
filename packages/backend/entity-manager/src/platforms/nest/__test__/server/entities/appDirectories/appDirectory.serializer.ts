import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import {
  AppDirectoryEntity,
  AppDirectoryEntityId,
} from "../../database/core/entities/appDirectory.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppDirectoryCreateData,
  AppDirectoryCursor,
  AppDirectorySheetItem,
  AppDirectorySorting,
  AppDirectoryUpdateData,
} from "./appDirectory.models"
import { AppDirectorySearchParameters } from "./appDirectory.types"

@WpEntitySerializer("appDirectory")
export class AppDirectorySerializer extends NestEntitySerializer<
  AppDirectoryEntity,
  AppDirectoryEntityId,
  AppDirectoryCreateData,
  AppDirectoryUpdateData,
  AppDirectorySearchParameters,
  AppDirectorySorting,
  AppDirectoryCursor,
  AppDirectorySheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDirectory", registry)
  }

  protected async loadEntities(
    filters: AppDirectorySearchParameters
  ): Promise<AppDirectoryEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppDirectoryEntity[]
  ): Promise<AppDirectoryEntity[]> {
    return entities
  }

  protected async importItem(
    item: AppDirectorySheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppDirectoryEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
