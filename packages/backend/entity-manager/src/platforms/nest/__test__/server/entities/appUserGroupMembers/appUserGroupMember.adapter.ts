import { IEntityAdapter, WpEntityAdapter, newUuid } from "../../../../../.."
import { AppUserGroupMemberEntity } from "../../database/core/entities/appUserGroupMember.entity"
import {
  AppUserGroupMemberCreateData,
  AppUserGroupMemberUpdateData,
} from "./appUserGroupMember.models"

@WpEntityAdapter("appUserGroupMember")
export class AppUserGroupMemberAdapter
  implements
    IEntityAdapter<
      AppUserGroupMemberEntity,
      AppUserGroupMemberCreateData,
      AppUserGroupMemberUpdateData
    >
{
  createDataToEntity(
    data: AppUserGroupMemberCreateData
  ): Partial<AppUserGroupMemberEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppUserGroupMemberUpdateData
  ): Partial<AppUserGroupMemberEntity> {
    return {
      ...data,
    }
  }
}
