import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppUserProfileCreateDto,
  AppUserProfileDto,
  AppUserProfileListItemDto,
  AppUserProfileUpdateDto,
} from "./appUserProfile.dto"
import { AppUserProfileEntity } from "../../database/core/entities/appUserProfile.entity"
import {
  AppUserProfileCursor,
  AppUserProfileDeleteParameters,
  AppUserProfileEntityId,
  AppUserProfileFacets,
  AppUserProfileSearchParameters,
  AppUserProfileSorting,
} from "../../entities/appUserProfiles/appUserProfile.models"

@WpEntityActions("appUserProfile")
export class AppUserProfileActions extends NestEntityActions<
  AppUserProfileEntity,
  AppUserProfileEntityId,
  AppUserProfileCreateDto,
  AppUserProfileUpdateDto,
  AppUserProfileDto,
  AppUserProfileListItemDto,
  AppUserProfileDeleteParameters,
  AppUserProfileSearchParameters,
  AppUserProfileSorting,
  AppUserProfileCursor,
  AppUserProfileFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserProfile", registry)
  }
}
