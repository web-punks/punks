import { InstanceInitializeTemplate } from "./instance-initialize"
import { OrganizationRegisterTemplate } from "./organization-register"
import { TenantInitializeTemplate } from "./tenant-initialize"
import { UserUpdateTemplate } from "./user-update"

export const SetupTemplates = [
  InstanceInitializeTemplate,
  OrganizationRegisterTemplate,
  TenantInitializeTemplate,
  UserUpdateTemplate,
]
