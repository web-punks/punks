import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppUserRoleCreateData,
  AppUserRoleCursor,
  AppUserRoleDeleteParameters,
  AppUserRoleEntityId,
  AppUserRoleFacets,
  AppUserRoleSearchParameters,
  AppUserRoleSorting,
  AppUserRoleUpdateData,
} from "./appUserRole.models"
import { AppUserRoleEntity } from "../../database/core/entities/appUserRole.entity"

@WpEntityManager("appUserRole")
export class AppUserRoleEntityManager extends NestEntityManager<
  AppUserRoleEntity,
  AppUserRoleEntityId,
  AppUserRoleCreateData,
  AppUserRoleUpdateData,
  AppUserRoleDeleteParameters,
  AppUserRoleSearchParameters,
  AppUserRoleSorting,
  AppUserRoleCursor,
  AppUserRoleFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserRole", registry)
  }
}
