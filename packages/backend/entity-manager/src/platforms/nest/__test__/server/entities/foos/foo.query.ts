import {
  FindOptionsWhere,
  Between,
  MoreThanOrEqual,
  LessThanOrEqual,
  FindOptionsOrder,
} from "typeorm"
import { FooEntity } from "../../database/core/entities/foo.entity"
import {
  FooEntityId,
  FooFacets,
  FooSearchParameters,
  FooSorting,
} from "./foo.models"
import { WpEntityQueryBuilder } from "../../../../decorators"
import { NestTypeOrmQueryBuilder } from "../../../../integrations/typeorm"
import { EntityManagerRegistry } from "../../../../ioc/registry"
import {
  AppUserContext,
  AppAuthContext,
} from "../../infrastructure/authentication"

@WpEntityQueryBuilder("foo")
export class FooQueryBuilder extends NestTypeOrmQueryBuilder<
  FooEntity,
  FooEntityId,
  FooSearchParameters,
  FooSorting,
  FooFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("foo", registry)
  }

  // protected buildTextSearchClause(
  //   request: FooSearchParameters
  // ): FindOptionsWhere<FooEntity> | FindOptionsWhere<FooEntity>[] {
  //   if (!request.query?.term) {
  //     return {}
  //   }

  //   return [
  //     {
  //       name: ILike(`%${request.query.term}%`),
  //     },
  //     {
  //       uid: ILike(`%${request.query.term}%`),
  //     },
  //   ]
  // }

  protected buildContextFilter(
    context?: AppAuthContext | undefined
  ): FindOptionsWhere<FooEntity> | FindOptionsWhere<FooEntity>[] {
    return {
      ...(context?.isAnonymous
        ? {
            id: "anonymous",
          }
        : {}),
      ...(context?.userContext?.organizationId
        ? {
            contact: {
              organization: {
                id: context?.userContext?.organizationId,
              },
            },
          }
        : {}),
    }
  }

  protected buildSortingClause(
    request: FooSearchParameters
  ): FindOptionsOrder<FooEntity> {
    switch (request.sorting?.fields[0]?.field) {
      case FooSorting.Age:
        return {
          age: request.sorting.fields[0].direction,
        }
      default:
      case FooSorting.Name:
        return {
          name: request.sorting?.fields[0].direction ?? "ASC",
        }
    }
  }

  protected buildWhereClause(
    request: FooSearchParameters
  ): FindOptionsWhere<FooEntity> | FindOptionsWhere<FooEntity>[] {
    return {
      ...(!!request.filters?.minAge || !!request.filters?.maxAge
        ? {
            age: this.buildAgeWhereClause(request),
          }
        : {}),
    }
  }

  private buildAgeWhereClause(request: FooSearchParameters) {
    if (!!request.filters?.minAge && !!request.filters?.maxAge) {
      return Between(request.filters.minAge, request.filters.maxAge)
    }

    if (!!request.filters?.minAge) {
      return MoreThanOrEqual(request.filters.minAge)
    }

    if (!!request.filters?.maxAge) {
      return LessThanOrEqual(request.filters.maxAge)
    }

    return undefined
  }

  protected async calculateFacets(
    request: FooSearchParameters,
    context?: AppAuthContext
  ): Promise<FooFacets> {
    return {
      age: await this.calculateFacet({
        field: "age",
        request,
        context,
        relations: {
          columns: ["contact"],
        },
      }),
    }
  }
}
