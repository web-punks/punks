import {
  EntityManagerRegistry,
  WpEntitySnapshotService,
} from "../../../../../.."
import { NestEntitySnapshotService } from "../../../../base/entitySnapshotService"
import { FooEntity, FooEntityId } from "../../database/core/entities/foo.entity"
import { FooRepository } from "../../database/core/repositories/foo.repository"

@WpEntitySnapshotService("foo")
export class FooEntitySnapshotService extends NestEntitySnapshotService<
  FooEntity,
  FooEntityId,
  FooRepository,
  FooEntity
> {
  constructor(registry: EntityManagerRegistry) {
    super("foo", registry)
  }

  async getSnapshot(id: FooEntityId): Promise<FooEntity> {
    return await this.repository.getInnerRepository().findOneOrFail({
      where: {
        id,
      },
      relations: {
        tenant: true,
      },
    })
  }
}
