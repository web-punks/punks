import { Module } from "@nestjs/common"
import { AppDivisionAdapter } from "./appDivision.adapter"
import { AppDivisionAuthMiddleware } from "./appDivision.authentication"
import { AppDivisionEntityManager } from "./appDivision.manager"
import { AppDivisionQueryBuilder } from "./appDivision.query"
import { EntityManagerModule } from "../../../.."
import { AppDivisionSerializer } from "./appDivision.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppDivisionAdapter,
    AppDivisionAuthMiddleware,
    AppDivisionEntityManager,
    AppDivisionQueryBuilder,
    AppDivisionSerializer,
  ],
  exports: [AppDivisionEntityManager],
})
export class AppDivisionEntityModule {}
