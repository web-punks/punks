import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { NestTypeOrmRepository, WpEntityRepository } from "../../../../.."
import { AppUserRoleEntity } from "../entities/appUserRole.entity"
import { AppUserRoleEntityId } from "../../../entities/appUserRoles/appUserRole.models"

@WpEntityRepository("appUserRole")
export class AppUserRoleRepository extends NestTypeOrmRepository<
  AppUserRoleEntity,
  AppUserRoleEntityId
> {
  constructor(
    @InjectRepository(AppUserRoleEntity, "core")
    repository: Repository<AppUserRoleEntity>
  ) {
    super(repository)
  }
}
