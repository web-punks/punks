import { AppCompanyEntity } from "./entities/appCompany.entity"
import { AppDivisionEntity } from "./entities/appDivision.entity"
import { FooEntity } from "./entities/foo.entity"
import { AppOrganizationEntity } from "./entities/appOrganization.entity"
import { AppRoleEntity } from "./entities/appRole.entity"
import { AppTenantEntity } from "./entities/appTenant.entity"
import { AppUserEntity } from "./entities/appUser.entity"
import { AppUserGroupEntity } from "./entities/appUserGroup.entity"
import { AppUserGroupMemberEntity } from "./entities/appUserGroupMember.entity"
import { AppUserProfileEntity } from "./entities/appUserProfile.entity"
import { AppUserRoleEntity } from "./entities/appUserRole.entity"
import { AppEntityVersionEntity } from "./entities/appEntityVersion.entity"
import { AppDirectoryEntity } from "./entities/appDirectory.entity"
import { CrmContactEntity } from "./entities/crmContact.entity"
import { AppFileReferenceEntity } from "./entities/appFileReference.entity"
import { AppEmailLogEntity } from "./entities/appEmailLog.entity"
import { AppPermissionEntity } from "./entities/appPermission.entity"
import { AppRolePermissionEntity } from "./entities/appRolePermission.entity"
import { AppOrganizationalUnitEntity } from "./entities/appOrganizationalUnit.entity"
import { AppOrganizationalUnitTypeEntity } from "./entities/appOrganizationalUnitType.entity"
import { AppOrganizationalUnitTypeChildEntity } from "./entities/appOrganizationalUnitTypeChild.entity"
import { AppRoleOrganizationalUnitTypeEntity } from "./entities/appRoleOrganizationalUnitType.entity"
import { AppCacheEntry } from "./entities/appCacheEntry.entity"
import { AppApiKeyEntity } from "./entities/appApiKey.entity"
import { AppOperationLockItemEntry } from "./entities/appOperationLockItem.entity"

export const CoreDatabaseEntities = [
  AppApiKeyEntity,
  AppCacheEntry,
  AppCompanyEntity,
  AppDirectoryEntity,
  AppDivisionEntity,
  AppEmailLogEntity,
  AppEntityVersionEntity,
  AppFileReferenceEntity,
  AppOrganizationEntity,
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitTypeEntity,
  AppOrganizationalUnitTypeChildEntity,
  AppOperationLockItemEntry,
  AppRoleOrganizationalUnitTypeEntity,
  AppRoleEntity,
  AppRolePermissionEntity,
  AppPermissionEntity,
  AppTenantEntity,
  AppUserEntity,
  AppUserGroupEntity,
  AppUserGroupMemberEntity,
  AppUserProfileEntity,
  AppUserRoleEntity,
  CrmContactEntity,
  FooEntity,
]
