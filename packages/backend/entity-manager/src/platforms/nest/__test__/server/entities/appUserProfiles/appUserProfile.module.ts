import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../.."
import { AppUserProfileAdapter } from "./appUserProfile.adapter"
import { AppUserProfileAuthMiddleware } from "./appUserProfile.authentication"
import { AppUserProfileEntityManager } from "./appUserProfile.manager"
import { AppUserProfileQueryBuilder } from "./appUserProfile.query"
import { AppUserProfileSerializer } from "./appUserProfile.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppUserProfileAdapter,
    AppUserProfileAuthMiddleware,
    AppUserProfileEntityManager,
    AppUserProfileQueryBuilder,
    AppUserProfileSerializer,
  ],
  exports: [AppUserProfileEntityManager],
})
export class AppUserProfileEntityModule {}
