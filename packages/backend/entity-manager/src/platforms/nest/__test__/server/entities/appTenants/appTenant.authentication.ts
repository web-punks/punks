import { IAuthorizationResult, WpEntityAuthMiddleware } from "../../../../../.."
import {
  AppAuthContext,
  AppEntityAuthorizationMiddlewareBase,
} from "../../infrastructure/authentication/index"
import { AppTenantEntity } from "../../database/core/entities/appTenant.entity"

@WpEntityAuthMiddleware("appTenant")
export class AppTenantAuthMiddleware extends AppEntityAuthorizationMiddlewareBase<AppTenantEntity> {
  async canSearch(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.defaultCanSearch(context)
  }

  async canRead(
    entity: Partial<AppTenantEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanRead(entity, context)
  }

  async canCreate(
    entity: Partial<AppTenantEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanCreate(entity, context)
  }

  async canUpdate(
    entity: Partial<AppTenantEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanUpdate(entity, context)
  }

  async canDelete(
    entity: Partial<AppTenantEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanDelete(entity, context)
  }

  async canDeleteItems(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.defaultCanDeleteItems(context)
  }
}
