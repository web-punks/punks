import { IAuthorizationResult, WpEntityAuthMiddleware } from "../../../../../.."
import {
  AppAuthContext,
  AppEntityAuthorizationMiddlewareBase,
} from "../../infrastructure/authentication/index"
import { CrmContactEntity } from "../../database/core/entities/crmContact.entity"

@WpEntityAuthMiddleware("crmContact")
export class CrmContactAuthMiddleware extends AppEntityAuthorizationMiddlewareBase<CrmContactEntity> {
  async canSearch(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.organizationEntityCanSearch(context)
  }

  async canRead(
    entity: CrmContactEntity,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.organizationEntityCanRead(entity, context)
  }

  async canCreate(
    entity: CrmContactEntity,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.organizationEntityCanCreate(entity, context)
  }

  async canUpdate(
    entity: CrmContactEntity,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.organizationEntityCanUpdate(entity, context)
  }

  async canDelete(
    entity: CrmContactEntity,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.organizationEntityCanDelete(entity, context)
  }

  async canDeleteItems(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.organizationEntityCanDeleteItems(context)
  }
}
