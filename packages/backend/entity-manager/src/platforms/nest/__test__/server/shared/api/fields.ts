import { ApiProperty } from "@nestjs/swagger"

export class NumericFilter {
  @ApiProperty({ required: false, type: [Number] })
  in?: number[]

  @ApiProperty({ required: false })
  eq?: number

  @ApiProperty({ required: false })
  gt?: number

  @ApiProperty({ required: false })
  gte?: number

  @ApiProperty({ required: false })
  lt?: number

  @ApiProperty({ required: false })
  lte?: number

  @ApiProperty({ required: false })
  isNull?: boolean
}

export class StringFilter {
  @ApiProperty({ required: false })
  gt?: string

  @ApiProperty({ required: false })
  gte?: string

  @ApiProperty({ required: false })
  lt?: string

  @ApiProperty({ required: false })
  lte?: string;

  @ApiProperty({ required: false })
  in?: string[]

  @ApiProperty({ required: false })
  eq?: string

  @ApiProperty({ required: false })
  ieq?: string

  @ApiProperty({ required: false })
  like?: string

  @ApiProperty({ required: false })
  ne?: string

  @ApiProperty({ required: false })
  ine?: string

  @ApiProperty({ required: false })
  notIn?: string[]

  @ApiProperty({ required: false })
  notLike?: string

  @ApiProperty({ required: false })
  isNull?: boolean

  @ApiProperty({ required: false })
  isEmpty?: boolean
}

export class IdFilter {
  @ApiProperty({ required: false })
  in?: string[]

  @ApiProperty({ required: false })
  eq?: string

  @ApiProperty({ required: false })
  ne?: string

  @ApiProperty({ required: false })
  notIn?: string[]

  @ApiProperty({ required: false })
  isNull?: boolean
}

export class EnumFilter<T> {
  @ApiProperty({ required: false })
  in?: T[]

  @ApiProperty({ required: false })
  eq?: T

  @ApiProperty({ required: false })
  ne?: T

  @ApiProperty({ required: false })
  notIn?: T[]
}

export class BooleanFilter {
  @ApiProperty({ required: false })
  eq?: boolean

  @ApiProperty({ required: false })
  ne?: boolean

  @ApiProperty({ required: false })
  isNull?: boolean
}

export class DateFilter {
  @ApiProperty({ required: false, type: [Date] })
  in?: Date[]

  @ApiProperty({ required: false })
  eq?: Date

  @ApiProperty({ required: false })
  gt?: Date

  @ApiProperty({ required: false })
  gte?: Date

  @ApiProperty({ required: false })
  lt?: Date

  @ApiProperty({ required: false })
  lte?: Date

  @ApiProperty({ required: false })
  isNull?: boolean
}
