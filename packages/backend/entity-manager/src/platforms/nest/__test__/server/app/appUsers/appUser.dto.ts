import { ApiProperty } from "@nestjs/swagger"
import {
  AppUserSearchParameters,
  AppUserSearchResults,
} from "../../entities/appUsers/appUser.models"

export class AppUserDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserListItemDto {
  @ApiProperty()
  id: string
}

export class AppUserCreateDto {}

export class AppUserUpdateDto {
  @ApiProperty()
  id: string
}

export class AppUserSearchRequest {
  @ApiProperty()
  params: AppUserSearchParameters
}

export class AppUserSearchResponse extends AppUserSearchResults<AppUserListItemDto> {
  @ApiProperty({ type: [AppUserListItemDto] })
  items: AppUserListItemDto[]
}
