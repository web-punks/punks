import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import {
  AppDirectorySorting,
  AppDirectoryCursor,
  AppDirectorySearchFilters,
  AppDirectoryFacets,
} from "./appDirectory.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "../../shared/api/versioning"
import { AppDirectoryEntity } from "../../database/core/entities/appDirectory.entity"

export class AppDirectorySearchSortingField
  implements ISearchSortingField<AppDirectorySorting>
{
  @ApiProperty({ enum: AppDirectorySorting })
  field: AppDirectorySorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppDirectoryQuerySorting
  implements ISearchSorting<AppDirectorySorting>
{
  @ApiProperty({ required: false, type: [AppDirectorySearchSortingField] })
  fields: AppDirectorySearchSortingField[]
}

export class AppDirectoryQueryPaging
  implements ISearchRequestPaging<AppDirectoryCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppDirectoryCursor

  @ApiProperty()
  pageSize: number
}

export class AppDirectorySearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppDirectoryFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppDirectorySearchParameters
  implements
    IEntitySearchParameters<
      AppDirectoryEntity,
      AppDirectorySorting,
      AppDirectoryCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppDirectoryFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppDirectorySearchFilters

  @ApiProperty({ required: false })
  sorting?: AppDirectoryQuerySorting

  @ApiProperty({ required: false })
  paging?: AppDirectoryQueryPaging

  @ApiProperty({ required: false })
  options?: AppDirectorySearchOptions
}

export class AppDirectorySearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppDirectoryCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppDirectoryCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppDirectoryCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppDirectoryCursor
}

export class AppDirectorySearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppDirectoryEntity,
      AppDirectorySearchParameters,
      TResult,
      AppDirectorySorting,
      AppDirectoryCursor,
      AppDirectoryFacets
    >
{
  @ApiProperty()
  request: AppDirectorySearchParameters

  @ApiProperty()
  facets?: AppDirectoryFacets

  @ApiProperty()
  paging?: AppDirectorySearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppDirectoryDeleteParameters
  implements IEntitiesDeleteParameters<AppDirectorySorting>
{
  @ApiProperty({ required: false })
  filters?: AppDirectorySearchFilters

  @ApiProperty({ required: false })
  sorting?: AppDirectoryQuerySorting
}

export class AppDirectoryVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppDirectoryCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppDirectoryCursor>
}

export class AppDirectoryVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppDirectoryCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppDirectoryCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppDirectoryCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppDirectoryCursor
}

export class AppDirectoryVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppDirectoryCursor>
{
  @ApiProperty()
  paging?: AppDirectoryVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
