import { ApiProperty } from "@nestjs/swagger"
import {
  AppTenantSearchParameters,
  AppTenantSearchResults,
} from "../../entities/appTenants/appTenant.models"

export class AppTenantDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppTenantListItemDto {
  @ApiProperty()
  id: string
}

export class AppTenantCreateDto {}

export class AppTenantUpdateDto {
  @ApiProperty()
  id: string
}

export class AppTenantSearchRequest {
  @ApiProperty()
  params: AppTenantSearchParameters
}

export class AppTenantSearchResponse extends AppTenantSearchResults<AppTenantListItemDto> {
  @ApiProperty({ type: [AppTenantListItemDto] })
  items: AppTenantListItemDto[]
}
