import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import {
  AppEntityVersionSorting,
  AppEntityVersionCursor,
  AppEntityVersionSearchFilters,
  AppEntityVersionFacets,
} from "./appEntityVersion.models"
import { AppEntityVersionEntity } from "../../database/core/entities/appEntityVersion.entity"

export class AppEntityVersionSearchSortingField
  implements ISearchSortingField<AppEntityVersionSorting>
{
  @ApiProperty({ enum: AppEntityVersionSorting })
  field: AppEntityVersionSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppEntityVersionQuerySorting
  implements ISearchSorting<AppEntityVersionSorting>
{
  @ApiProperty({ required: false, type: [AppEntityVersionSearchSortingField] })
  fields: AppEntityVersionSearchSortingField[]
}

export class AppEntityVersionQueryPaging
  implements ISearchRequestPaging<AppEntityVersionCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppEntityVersionCursor

  @ApiProperty()
  pageSize: number
}

export class AppEntityVersionSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppEntityVersionFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppEntityVersionSearchParameters
  implements
    IEntitySearchParameters<
      AppEntityVersionEntity,
      AppEntityVersionSorting,
      AppEntityVersionCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppEntityVersionFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppEntityVersionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppEntityVersionQuerySorting

  @ApiProperty({ required: false })
  paging?: AppEntityVersionQueryPaging

  @ApiProperty({ required: false })
  options?: AppEntityVersionSearchOptions
}

export class AppEntityVersionSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppEntityVersionCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppEntityVersionCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppEntityVersionCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppEntityVersionCursor
}

export class AppEntityVersionSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppEntityVersionEntity,
      AppEntityVersionSearchParameters,
      TResult,
      AppEntityVersionSorting,
      AppEntityVersionCursor,
      AppEntityVersionFacets
    >
{
  @ApiProperty()
  request: AppEntityVersionSearchParameters

  @ApiProperty()
  facets?: AppEntityVersionFacets

  @ApiProperty()
  paging?: AppEntityVersionSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppEntityVersionDeleteParameters
  implements IEntitiesDeleteParameters<AppEntityVersionSorting>
{
  @ApiProperty({ required: false })
  filters?: AppEntityVersionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppEntityVersionQuerySorting
}
