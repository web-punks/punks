import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../../.."
import { AppCompanyEntity } from "../../database/core/entities/appCompany.entity"
import {
  AppCompanyEntityId,
  AppCompanyFacets,
  AppCompanySearchParameters,
  AppCompanySorting,
} from "./appCompany.models"
import {
  AppUserContext,
  AppAuthContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appCompany")
export class AppCompanyQueryBuilder extends NestTypeOrmQueryBuilder<
  AppCompanyEntity,
  AppCompanyEntityId,
  AppCompanySearchParameters,
  AppCompanySorting,
  AppCompanyFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("company", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext | undefined
  ): FindOptionsWhere<AppCompanyEntity> | FindOptionsWhere<AppCompanyEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppCompanySearchParameters
  ): FindOptionsOrder<AppCompanyEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppCompanySearchParameters
  ): FindOptionsWhere<AppCompanyEntity> | FindOptionsWhere<AppCompanyEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppCompanySearchParameters,
    context?: AppAuthContext
  ): Promise<AppCompanyFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
