import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { AppEntityVersionEntity } from "../../database/core/entities/appEntityVersion.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppEntityVersionCreateData,
  AppEntityVersionCursor,
  AppEntityVersionEntityId,
  AppEntityVersionSheetItem,
  AppEntityVersionSorting,
  AppEntityVersionUpdateData,
} from "./appEntityVersion.models"
import { AppEntityVersionSearchParameters } from "./appEntityVersion.types"

@WpEntitySerializer("appEntityVersion")
export class AppEntityVersionSerializer extends NestEntitySerializer<
  AppEntityVersionEntity,
  AppEntityVersionEntityId,
  AppEntityVersionCreateData,
  AppEntityVersionUpdateData,
  AppEntityVersionSearchParameters,
  AppEntityVersionSorting,
  AppEntityVersionCursor,
  AppEntityVersionSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEntityVersion", registry)
  }

  protected async loadEntities(
    filters: AppEntityVersionSearchParameters
  ): Promise<AppEntityVersionEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppEntityVersionEntity[]
  ): Promise<AppEntityVersionSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppEntityVersionSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppEntityVersionEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
