import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppRoleEntity } from "../../database/core/entities/appRole.entity"
import { AppRoleCreateData, AppRoleUpdateData } from "./appRole.models"

@WpEntityAdapter("appRole")
export class AppRoleAdapter
  implements
    IEntityAdapter<AppRoleEntity, AppRoleCreateData, AppRoleUpdateData>
{
  createDataToEntity(data: AppRoleCreateData): DeepPartial<AppRoleEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppRoleUpdateData): DeepPartial<AppRoleEntity> {
    return {
      ...data,
    }
  }
}
