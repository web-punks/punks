import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { AppFileReferenceAdapter } from "./appFileReference.adapter"
import { AppFileReferenceAuthMiddleware } from "./appFileReference.authentication"
import { AppFileReferenceEntityManager } from "./appFileReference.manager"
import { AppFileReferenceQueryBuilder } from "./appFileReference.query"
import { AppFileReferenceSerializer } from "./appFileReference.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppFileReferenceAdapter,
    AppFileReferenceAuthMiddleware,
    AppFileReferenceEntityManager,
    AppFileReferenceQueryBuilder,
    AppFileReferenceSerializer,
  ],
  exports: [AppFileReferenceEntityManager],
})
export class AppFileReferenceEntityModule {}
