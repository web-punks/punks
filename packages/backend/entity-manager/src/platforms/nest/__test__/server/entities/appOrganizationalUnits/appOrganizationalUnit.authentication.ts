import { IAuthorizationResult, WpEntityAuthMiddleware } from "../../../../../.."
import {
  AppAuthContext,
  AppEntityAuthorizationMiddlewareBase,
} from "../../infrastructure/authentication/index"
import { AppOrganizationalUnitEntity } from "../../database/core/entities/appOrganizationalUnit.entity"

@WpEntityAuthMiddleware("appOrganizationalUnit")
export class AppOrganizationalUnitAuthMiddleware extends AppEntityAuthorizationMiddlewareBase<AppOrganizationalUnitEntity> {
  async canSearch(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.defaultCanSearch(context)
  }

  async canRead(
    entity: Partial<AppOrganizationalUnitEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanRead(entity, context)
  }

  async canCreate(
    entity: Partial<AppOrganizationalUnitEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanCreate(entity, context)
  }

  async canUpdate(
    entity: Partial<AppOrganizationalUnitEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanUpdate(entity, context)
  }

  async canDelete(
    entity: Partial<AppOrganizationalUnitEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanDelete(entity, context)
  }

  async canDeleteItems(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.defaultCanDeleteItems(context)
  }
}
