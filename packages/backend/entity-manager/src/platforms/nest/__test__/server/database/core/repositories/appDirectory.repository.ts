import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"

import { AppDirectoryEntity } from "../entities/appDirectory.entity"
import { WpEntityRepository } from "../../../../../decorators"
import { NestTypeOrmRepository } from "../../../../../integrations"
import { AppDirectoryEntityId } from "../../../entities/appDirectories/appDirectory.models"

@WpEntityRepository("appDirectory")
export class AppDirectoryRepository extends NestTypeOrmRepository<
  AppDirectoryEntity,
  AppDirectoryEntityId
> {
  constructor(
    @InjectRepository(AppDirectoryEntity, "core")
    repository: Repository<AppDirectoryEntity>
  ) {
    super(repository)
  }
}
