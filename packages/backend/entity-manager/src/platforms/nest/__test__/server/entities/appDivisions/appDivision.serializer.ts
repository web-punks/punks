import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { AppDivisionEntity } from "../../database/core/entities/appDivision.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppDivisionCreateData,
  AppDivisionCursor,
  AppDivisionEntityId,
  AppDivisionSearchParameters,
  AppDivisionSheetItem,
  AppDivisionSorting,
  AppDivisionUpdateData,
} from "./appDivision.models"

@WpEntitySerializer("appDivision")
export class AppDivisionSerializer extends NestEntitySerializer<
  AppDivisionEntity,
  AppDivisionEntityId,
  AppDivisionCreateData,
  AppDivisionUpdateData,
  AppDivisionSearchParameters,
  AppDivisionSorting,
  AppDivisionCursor,
  AppDivisionSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDivision", registry)
  }

  protected async loadEntities(
    filters: AppDivisionSearchParameters
  ): Promise<AppDivisionEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppDivisionEntity[]
  ): Promise<AppDivisionEntity[]> {
    return entities
  }

  protected async importItem(
    item: AppDivisionSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppDivisionEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
