import { ApiProperty } from "@nestjs/swagger"

export class OrganizationRegistrationInfo {
  @ApiProperty()
  tenantId: string

  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string
}

export class CompanyRegistrationInfo {
  @ApiProperty()
  name: string

  @ApiProperty()
  uid: string
}

export class OrganizationInitializeRequest {
  @ApiProperty()
  organization: OrganizationRegistrationInfo

  @ApiProperty()
  company: CompanyRegistrationInfo
}

export class OrganizationInitializeResponse {
  @ApiProperty()
  organizationId: string

  @ApiProperty()
  companyId: string
}
