import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { AppUserEntity } from "../../database/core/entities/appUser.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppUserCreateData,
  AppUserCursor,
  AppUserEntityId,
  AppUserSearchParameters,
  AppUserSheetItem,
  AppUserSorting,
  AppUserUpdateData,
} from "./appUser.models"

@WpEntitySerializer("appUser")
export class AppUserSerializer extends NestEntitySerializer<
  AppUserEntity,
  AppUserEntityId,
  AppUserCreateData,
  AppUserUpdateData,
  AppUserSearchParameters,
  AppUserSorting,
  AppUserCursor,
  AppUserSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUser", registry)
  }

  protected async loadEntities(
    filters: AppUserSearchParameters
  ): Promise<AppUserEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppUserEntity[]
  ): Promise<AppUserEntity[]> {
    return entities
  }

  protected async importItem(item: AppUserSheetItem, context: AppAuthContext) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppUserEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
