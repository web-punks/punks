import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppUserGroupMemberCreateDto,
  AppUserGroupMemberDto,
  AppUserGroupMemberListItemDto,
  AppUserGroupMemberUpdateDto,
} from "./appUserGroupMember.dto"
import { AppUserGroupMemberEntity } from "../../database/core/entities/appUserGroupMember.entity"
import {
  AppUserGroupMemberCursor,
  AppUserGroupMemberDeleteParameters,
  AppUserGroupMemberEntityId,
  AppUserGroupMemberFacets,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSorting,
} from "../../entities/appUserGroupMembers/appUserGroupMember.models"

@WpEntityActions("appUserGroupMember")
export class AppUserGroupMemberActions extends NestEntityActions<
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
  AppUserGroupMemberCreateDto,
  AppUserGroupMemberUpdateDto,
  AppUserGroupMemberDto,
  AppUserGroupMemberListItemDto,
  AppUserGroupMemberDeleteParameters,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSorting,
  AppUserGroupMemberCursor,
  AppUserGroupMemberFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroupMember", registry)
  }
}
