import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppApiKeyCreateData,
  AppApiKeyCursor,
  AppApiKeyFacets,
  AppApiKeySorting,
  AppApiKeyUpdateData,
} from "./appApiKey.models"
import {
  AppApiKeySearchParameters,
  AppApiKeyDeleteParameters,
} from "./appApiKey.types"
import {
  AppApiKeyEntity,
  AppApiKeyEntityId,
} from "../../database/core/entities/appApiKey.entity"

@WpEntityManager("appApiKey")
export class AppApiKeyEntityManager extends NestEntityManager<
  AppApiKeyEntity,
  AppApiKeyEntityId,
  AppApiKeyCreateData,
  AppApiKeyUpdateData,
  AppApiKeyDeleteParameters,
  AppApiKeySearchParameters,
  AppApiKeySorting,
  AppApiKeyCursor,
  AppApiKeyFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appApiKey", registry)
  }
}
