import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { AppUserRoleActions } from "./appUserRole.actions"
import {
  AppUserRoleCreateDto,
  AppUserRoleDto,
  AppUserRoleSearchRequest,
  AppUserRoleSearchResponse,
  AppUserRoleUpdateDto,
} from "./appUserRole.dto"

@Controller("v1/appUserRole")
export class AppUserRoleController {
  constructor(private readonly actions: AppUserRoleActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appUserRoleGet",
  })
  @ApiOkResponse({
    type: AppUserRoleDto,
  })
  async item(@Param("id") id: string): Promise<AppUserRoleDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appUserRoleCreate",
  })
  @ApiOkResponse({
    type: AppUserRoleDto,
  })
  async create(@Body() data: AppUserRoleCreateDto): Promise<AppUserRoleDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppUserRoleDto,
  })
  @ApiOperation({
    operationId: "appUserRoleUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppUserRoleUpdateDto
  ): Promise<AppUserRoleDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appUserRoleDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appUserRoleSearch",
  })
  @ApiOkResponse({
    type: AppUserRoleSearchResponse,
  })
  async search(
    @Body() request: AppUserRoleSearchRequest
  ): Promise<AppUserRoleSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }
}
