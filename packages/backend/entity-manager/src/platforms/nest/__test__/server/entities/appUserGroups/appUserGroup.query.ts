import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../../.."
import { AppUserGroupEntity } from "../../database/core/entities/appUserGroup.entity"
import {
  AppUserGroupEntityId,
  AppUserGroupFacets,
  AppUserGroupSearchParameters,
  AppUserGroupSorting,
} from "./appUserGroup.models"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appUserGroup")
export class AppUserGroupQueryBuilder extends NestTypeOrmQueryBuilder<
  AppUserGroupEntity,
  AppUserGroupEntityId,
  AppUserGroupSearchParameters,
  AppUserGroupSorting,
  AppUserGroupFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroup", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext | undefined
  ):
    | FindOptionsWhere<AppUserGroupEntity>
    | FindOptionsWhere<AppUserGroupEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppUserGroupSearchParameters
  ): FindOptionsOrder<AppUserGroupEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppUserGroupSearchParameters
  ):
    | FindOptionsWhere<AppUserGroupEntity>
    | FindOptionsWhere<AppUserGroupEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppUserGroupSearchParameters,
    context?: AppAuthContext
  ): Promise<AppUserGroupFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
