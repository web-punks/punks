import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { WpEntityRepository, NestTypeOrmRepository } from "../../../../.."
import { AppEntityVersionEntity } from "../entities/appEntityVersion.entity"
import { AppEntityVersionEntityId } from "../../../entities/appEntityVersions/appEntityVersion.models"

@WpEntityRepository("appEntityVersion")
export class AppEntityVersionRepository extends NestTypeOrmRepository<
  AppEntityVersionEntity,
  AppEntityVersionEntityId
> {
  constructor(
    @InjectRepository(AppEntityVersionEntity, "core")
    repository: Repository<AppEntityVersionEntity>
  ) {
    super(repository)
  }
}
