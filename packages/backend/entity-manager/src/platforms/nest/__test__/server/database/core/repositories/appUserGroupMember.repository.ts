import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { NestTypeOrmRepository, WpEntityRepository } from "../../../../.."
import { AppUserGroupMemberEntity } from "../entities/appUserGroupMember.entity"
import { AppUserGroupMemberEntityId } from "../../../entities/appUserGroupMembers/appUserGroupMember.models"

@WpEntityRepository("appUserGroupMember")
export class AppUserGroupMemberRepository extends NestTypeOrmRepository<
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId
> {
  constructor(
    @InjectRepository(AppUserGroupMemberEntity, "core")
    repository: Repository<AppUserGroupMemberEntity>
  ) {
    super(repository)
  }
}
