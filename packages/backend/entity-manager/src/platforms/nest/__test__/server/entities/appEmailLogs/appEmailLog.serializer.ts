import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import {
  AppEmailLogEntity,
  AppEmailLogEntityId,
} from "../../database/core/entities/appEmailLog.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppEmailLogCreateData,
  AppEmailLogCursor,
  AppEmailLogSheetItem,
  AppEmailLogSorting,
  AppEmailLogUpdateData,
} from "./appEmailLog.models"
import { AppEmailLogSearchParameters } from "./appEmailLog.types"

@WpEntitySerializer("appEmailLog")
export class AppEmailLogSerializer extends NestEntitySerializer<
  AppEmailLogEntity,
  AppEmailLogEntityId,
  AppEmailLogCreateData,
  AppEmailLogUpdateData,
  AppEmailLogSearchParameters,
  AppEmailLogSorting,
  AppEmailLogCursor,
  AppEmailLogSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEmailLog", registry)
  }

  protected async loadEntities(
    filters: AppEmailLogSearchParameters
  ): Promise<AppEmailLogEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppEmailLogEntity[]
  ): Promise<AppEmailLogSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppEmailLogSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppEmailLogEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
