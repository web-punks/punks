import {
  Entity,
  Column,
  CreateDateColumn,
  PrimaryColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
  Unique,
} from "typeorm"
import { WpEntity } from "../../../../.."
import { AppCompanyEntity } from "./appCompany.entity"
import { AppTenantEntity } from "./appTenant.entity"
import { AppUserEntity } from "./appUser.entity"
import { AppUserGroupEntity } from "./appUserGroup.entity"

@Entity("appOrganizations")
@WpEntity("appOrganization")
@Unique("organizationUid", ["tenant", "uid"])
export class AppOrganizationEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255 })
  uid: string

  @Column({ type: "varchar", length: 255 })
  name: string

  @ManyToOne(() => AppTenantEntity, (tenant) => tenant.organizations)
  tenant: AppTenantEntity

  @OneToMany(() => AppCompanyEntity, (company) => company.organization)
  companies?: AppCompanyEntity[]

  @OneToMany(() => AppUserEntity, (user) => user.organization)
  users?: AppUserEntity[]

  @OneToMany(() => AppUserGroupEntity, (userGroup) => userGroup.organization)
  userGroups?: AppUserGroupEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
