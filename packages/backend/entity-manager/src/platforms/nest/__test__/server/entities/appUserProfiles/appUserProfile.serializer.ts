import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { AppUserProfileEntity } from "../../database/core/entities/appUserProfile.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppUserProfileCreateData,
  AppUserProfileCursor,
  AppUserProfileEntityId,
  AppUserProfileSearchParameters,
  AppUserProfileSheetItem,
  AppUserProfileSorting,
  AppUserProfileUpdateData,
} from "./appUserProfile.models"

@WpEntitySerializer("appUserProfile")
export class AppUserProfileSerializer extends NestEntitySerializer<
  AppUserProfileEntity,
  AppUserProfileEntityId,
  AppUserProfileCreateData,
  AppUserProfileUpdateData,
  AppUserProfileSearchParameters,
  AppUserProfileSorting,
  AppUserProfileCursor,
  AppUserProfileSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserProfile", registry)
  }

  protected async loadEntities(
    filters: AppUserProfileSearchParameters
  ): Promise<AppUserProfileEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppUserProfileEntity[]
  ): Promise<AppUserProfileEntity[]> {
    return entities
  }

  protected async importItem(
    item: AppUserProfileSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppUserProfileEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
