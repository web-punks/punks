import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { FooActions } from "./foo.actions"
import {
  FooCreateDto,
  FooDto,
  FooExportRequest,
  FooExportResponse,
  FooImportRequest,
  FooParseRequest,
  FooParseResponse,
  FooSampleDownloadRequest,
  FooSampleDownloadResponse,
  FooSearchRequest,
  FooSearchResponse,
  FooUpdateDto,
  FooVersionsSearchRequest,
  FooVersionsSearchResponse,
} from "./foo.dto"
import { Public } from "../../../.."
import { ApiBodyFile } from "../../shared/interceptors/files"
import { EntitySerializationFormat } from "../../../../../../abstractions/serializer"

@Public()
@Controller("v1/foo")
export class FooController {
  constructor(private readonly actions: FooActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "fooGet",
  })
  @ApiOkResponse({
    type: FooDto,
  })
  async item(@Param("id") id: string): Promise<FooDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "fooCreate",
  })
  @ApiOkResponse({
    type: FooDto,
  })
  async create(@Body() data: FooCreateDto): Promise<FooDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: FooDto,
  })
  @ApiOperation({
    operationId: "fooUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: FooUpdateDto
  ): Promise<FooDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "fooDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "fooSearch",
  })
  @ApiOkResponse({
    type: FooSearchResponse,
  })
  async search(@Body() request: FooSearchRequest): Promise<FooSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("parse")
  @ApiOkResponse({
    type: FooParseResponse,
  })
  @ApiOperation({
    operationId: "fooParse",
  })
  @ApiBodyFile("file")
  async parse(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ): Promise<FooParseResponse> {
    return await this.actions.manager.parse.execute({
      format,
      file: {
        content: file.buffer,
        contentType: file.mimetype,
        fileName: file.originalname,
      },
    })
  }

  @Post("import")
  @ApiOperation({
    operationId: "fooImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute({
      format,
      file: {
        content: file.buffer,
        contentType: file.mimetype,
        fileName: file.originalname,
      },
    })
  }

  @Post("export")
  @ApiOperation({
    operationId: "fooExport",
  })
  @ApiOkResponse({
    type: FooExportResponse,
  })
  async export(@Body() request: FooExportRequest): Promise<FooExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "fooSampleDownload",
  })
  @ApiOkResponse({
    type: FooSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: FooSampleDownloadRequest
  ): Promise<FooSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "fooVersion",
  })
  @ApiOkResponse({
    type: FooSearchResponse,
  })
  async versions(
    @Body() request: FooVersionsSearchRequest
  ): Promise<FooVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
