import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppRoleCreateDto,
  AppRoleDto,
  AppRoleListItemDto,
  AppRoleUpdateDto,
} from "./appRole.dto"
import { AppRoleEntity } from "../../database/core/entities/appRole.entity"
import {
  AppRoleCursor,
  AppRoleDeleteParameters,
  AppRoleEntityId,
  AppRoleFacets,
  AppRoleSearchParameters,
  AppRoleSorting,
} from "../../entities/appRoles/appRole.models"

@WpEntityActions("appRole")
export class AppRoleActions extends NestEntityActions<
  AppRoleEntity,
  AppRoleEntityId,
  AppRoleCreateDto,
  AppRoleUpdateDto,
  AppRoleDto,
  AppRoleListItemDto,
  AppRoleDeleteParameters,
  AppRoleSearchParameters,
  AppRoleSorting,
  AppRoleCursor,
  AppRoleFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRole", registry)
  }
}
