import { getRepositoryToken } from "@nestjs/typeorm"
import { DataSource } from "typeorm"
import { DynamicModule, Module } from "@nestjs/common"
import { CoreDatabaseRepositories } from "./repositories"
import { CoreDatabaseEntities } from "./entities"

export const mockDatabaseProviders = (dataSource: DataSource) => {
  return CoreDatabaseEntities.map((entity) => ({
    provide: getRepositoryToken(entity, "core"),
    useValue: dataSource.getRepository(entity),
  }))
}

@Module({})
export class CoreDatabaseModule {
  static forRoot(dataSource: any): DynamicModule {
    return {
      module: CoreDatabaseModule,
      providers: [
        ...mockDatabaseProviders(dataSource),
        ...CoreDatabaseRepositories,
      ],
    }
  }
}
