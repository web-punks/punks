import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppRolePermissionEntity } from "../../database/core/entities/appRolePermission.entity"
import {
  AppRolePermissionSorting,
  AppRolePermissionCursor,
  AppRolePermissionSearchFilters,
  AppRolePermissionFacets,
} from "./appRolePermission.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "../../shared/api/versioning"

export class AppRolePermissionSearchSortingField
  implements ISearchSortingField<AppRolePermissionSorting>
{
  @ApiProperty({ enum: AppRolePermissionSorting })
  field: AppRolePermissionSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppRolePermissionQuerySorting
  implements ISearchSorting<AppRolePermissionSorting>
{
  @ApiProperty({ required: false, type: [AppRolePermissionSearchSortingField] })
  fields: AppRolePermissionSearchSortingField[]
}

export class AppRolePermissionQueryPaging
  implements ISearchRequestPaging<AppRolePermissionCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppRolePermissionCursor

  @ApiProperty()
  pageSize: number
}

export class AppRolePermissionSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppRolePermissionFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppRolePermissionSearchParameters
  implements
    IEntitySearchParameters<
      AppRolePermissionEntity,
      AppRolePermissionSorting,
      AppRolePermissionCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppRolePermissionFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppRolePermissionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppRolePermissionQuerySorting

  @ApiProperty({ required: false })
  paging?: AppRolePermissionQueryPaging

  @ApiProperty({ required: false })
  options?: AppRolePermissionSearchOptions

  relations?: ISearchQueryRelations<AppRolePermissionEntity>
}

export class AppRolePermissionSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppRolePermissionCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppRolePermissionCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppRolePermissionCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppRolePermissionCursor
}

export class AppRolePermissionSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppRolePermissionEntity,
      AppRolePermissionSearchParameters,
      TResult,
      AppRolePermissionSorting,
      AppRolePermissionCursor,
      AppRolePermissionFacets
    >
{
  @ApiProperty()
  request: AppRolePermissionSearchParameters

  @ApiProperty()
  facets?: AppRolePermissionFacets

  @ApiProperty()
  paging?: AppRolePermissionSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppRolePermissionDeleteParameters
  implements IEntitiesDeleteParameters<AppRolePermissionSorting>
{
  @ApiProperty({ required: false })
  filters?: AppRolePermissionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppRolePermissionQuerySorting
}

export class AppRolePermissionVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppRolePermissionCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppRolePermissionCursor>
}

export class AppRolePermissionVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppRolePermissionCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppRolePermissionCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppRolePermissionCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppRolePermissionCursor
}

export class AppRolePermissionVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppRolePermissionCursor>
{
  @ApiProperty()
  paging?: AppRolePermissionVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
