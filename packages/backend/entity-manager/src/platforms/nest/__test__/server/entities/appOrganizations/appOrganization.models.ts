import {
  DeepPartial,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppOrganizationEntity } from "../../database/core/entities/appOrganization.entity"
import { IEntitiesDeleteParameters } from "../../../../../../abstractions/commands"

export type AppOrganizationEntityId = string

export type AppOrganizationCreateData = DeepPartial<
  Omit<AppOrganizationEntity, "id">
>
export type AppOrganizationUpdateData = DeepPartial<
  Omit<AppOrganizationEntity, "id">
>

export class AppOrganizationFacets {}

export class AppOrganizationSearchFilters {
  uid?: string
}

export enum AppOrganizationSorting {
  Name = "Name",
}

export type AppOrganizationCursor = number

export class AppOrganizationSearchSortingField
  implements ISearchSortingField<AppOrganizationSorting>
{
  @ApiProperty({ enum: AppOrganizationSorting })
  field: AppOrganizationSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppOrganizationQuerySorting
  implements ISearchSorting<AppOrganizationSorting>
{
  @ApiProperty({ required: false, type: [AppOrganizationSearchSortingField] })
  fields: AppOrganizationSearchSortingField[]
}

export class AppOrganizationQueryPaging
  implements ISearchRequestPaging<AppOrganizationCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppOrganizationCursor

  @ApiProperty()
  pageSize: number
}

export class AppOrganizationSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppOrganizationSearchParameters
  implements
    IEntitySearchParameters<
      AppOrganizationEntity,
      AppOrganizationSorting,
      AppOrganizationCursor
    >
{
  @ApiProperty({ required: false })
  filters?: AppOrganizationSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppOrganizationQuerySorting

  @ApiProperty({ required: false })
  paging?: AppOrganizationQueryPaging

  @ApiProperty({ required: false })
  options?: AppOrganizationSearchOptions
}

export class AppOrganizationSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppOrganizationCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppOrganizationCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppOrganizationCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppOrganizationCursor
}

export class AppOrganizationSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppOrganizationEntity,
      AppOrganizationSearchParameters,
      TResult,
      AppOrganizationSorting,
      AppOrganizationCursor,
      AppOrganizationFacets
    >
{
  @ApiProperty()
  request: AppOrganizationSearchParameters

  @ApiProperty()
  facets?: AppOrganizationFacets

  @ApiProperty()
  paging?: AppOrganizationSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppOrganizationDeleteParameters
  implements IEntitiesDeleteParameters<AppOrganizationSorting>
{
  @ApiProperty({ required: false })
  filters?: AppOrganizationSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppOrganizationQuerySorting
}

export type AppOrganizationSheetItem = AppOrganizationEntity
