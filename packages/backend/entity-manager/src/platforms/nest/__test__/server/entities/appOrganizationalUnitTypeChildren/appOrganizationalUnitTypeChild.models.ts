import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "../../../../../.."
import { AppOrganizationalUnitTypeChildEntity } from "../../database/core/entities/appOrganizationalUnitTypeChild.entity"

export type AppOrganizationalUnitTypeChildCreateData = DeepPartial<
  Omit<AppOrganizationalUnitTypeChildEntity, "id">
>
export type AppOrganizationalUnitTypeChildUpdateData = DeepPartial<
  Omit<AppOrganizationalUnitTypeChildEntity, "id">
>

export enum AppOrganizationalUnitTypeChildSorting {
  Name = "Name",
}

export type AppOrganizationalUnitTypeChildCursor = number

export class AppOrganizationalUnitTypeChildSearchFilters {
  @ApiProperty({ required: false })
  parentTypeId?: string
}

export class AppOrganizationalUnitTypeChildFacets {}

export type AppOrganizationalUnitTypeChildSheetItem =
  AppOrganizationalUnitTypeChildEntity
