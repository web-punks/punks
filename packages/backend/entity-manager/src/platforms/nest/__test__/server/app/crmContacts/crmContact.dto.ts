import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "../../../../../.."
import {
  CrmContactSearchParameters,
  CrmContactSearchResults,
  CrmContactVersionsSearchParameters,
  CrmContactVersionsSearchResults,
} from "../../entities/crmContacts/crmContact.types"

export class CrmContactDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class CrmContactListItemDto {
  @ApiProperty()
  id: string
}

export class CrmContactCreateDto {
  email: string
  firstName: string
  lastName: string
  organizationId: string
}

export class CrmContactUpdateDto {
  email: string
  firstName: string
  lastName: string
  organizationId: string
}

export class CrmContactSearchRequest {
  @ApiProperty()
  params: CrmContactSearchParameters
}

export class CrmContactSearchResponse extends CrmContactSearchResults<CrmContactListItemDto> {
  @ApiProperty({ type: [CrmContactListItemDto] })
  items: CrmContactListItemDto[]
}

export class CrmContactEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class CrmContactExportRequest {
  @ApiProperty()
  options: CrmContactEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: CrmContactSearchParameters
}

export class CrmContactExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class CrmContactSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class CrmContactSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class CrmContactVersionsSearchRequest {
  @ApiProperty()
  params: CrmContactVersionsSearchParameters
}

export class CrmContactVersionsSearchResponse extends CrmContactVersionsSearchResults<CrmContactListItemDto> {
  @ApiProperty({ type: [CrmContactListItemDto] })
  items: CrmContactListItemDto[]
}
