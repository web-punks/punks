import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { AppRoleEntity } from "../../database/core/entities/appRole.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppRoleCreateData,
  AppRoleCursor,
  AppRoleEntityId,
  AppRoleSearchParameters,
  AppRoleSheetItem,
  AppRoleSorting,
  AppRoleUpdateData,
} from "./appRole.models"

@WpEntitySerializer("appRole")
export class AppRoleSerializer extends NestEntitySerializer<
  AppRoleEntity,
  AppRoleEntityId,
  AppRoleCreateData,
  AppRoleUpdateData,
  AppRoleSearchParameters,
  AppRoleSorting,
  AppRoleCursor,
  AppRoleSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRole", registry)
  }

  protected async loadEntities(
    filters: AppRoleSearchParameters
  ): Promise<AppRoleEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppRoleEntity[]
  ): Promise<AppRoleEntity[]> {
    return entities
  }

  protected async importItem(item: AppRoleSheetItem, context: AppAuthContext) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppRoleEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
