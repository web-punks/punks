import {
  DeepPartial,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppTenantEntity } from "../../database/core/entities/appTenant.entity"
import { IEntitiesDeleteParameters } from "../../../../../../abstractions/commands"

export type AppTenantEntityId = string

export type AppTenantCreateData = DeepPartial<Omit<AppTenantEntity, "id">>
export type AppTenantUpdateData = DeepPartial<Omit<AppTenantEntity, "id">>

export class AppTenantFacets {}

export class AppTenantSearchFilters {
  @ApiProperty({ required: false })
  id?: string

  @ApiProperty({ required: false })
  uid?: string
}

export enum AppTenantSorting {
  Name = "Name",
}

export type AppTenantCursor = number

export class AppTenantSearchSortingField
  implements ISearchSortingField<AppTenantSorting>
{
  @ApiProperty({ enum: AppTenantSorting })
  field: AppTenantSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppTenantQuerySorting implements ISearchSorting<AppTenantSorting> {
  @ApiProperty({ required: false, type: [AppTenantSearchSortingField] })
  fields: AppTenantSearchSortingField[]
}

export class AppTenantQueryPaging
  implements ISearchRequestPaging<AppTenantCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppTenantCursor

  @ApiProperty()
  pageSize: number
}

export class AppTenantSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppTenantSearchParameters
  implements
    IEntitySearchParameters<AppTenantEntity, AppTenantSorting, AppTenantCursor>
{
  @ApiProperty({ required: false })
  filters?: AppTenantSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppTenantQuerySorting

  @ApiProperty({ required: false })
  paging?: AppTenantQueryPaging

  @ApiProperty({ required: false })
  options?: AppTenantSearchOptions
}

export class AppTenantSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppTenantCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppTenantCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppTenantCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppTenantCursor
}

export class AppTenantSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppTenantEntity,
      AppTenantSearchParameters,
      TResult,
      AppTenantSorting,
      AppTenantCursor,
      AppTenantFacets
    >
{
  @ApiProperty()
  request: AppTenantSearchParameters

  @ApiProperty()
  facets?: AppTenantFacets

  @ApiProperty()
  paging?: AppTenantSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppTenantDeleteParameters
  implements IEntitiesDeleteParameters<AppTenantSorting>
{
  @ApiProperty({ required: false })
  filters?: AppTenantSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppTenantQuerySorting
}

export type AppTenantSheetItem = AppTenantEntity
