import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { AppRoleActions } from "./appRole.actions"
import {
  AppRoleCreateDto,
  AppRoleDto,
  AppRoleSearchRequest,
  AppRoleSearchResponse,
  AppRoleUpdateDto,
} from "./appRole.dto"

@Controller("v1/appRole")
export class AppRoleController {
  constructor(private readonly actions: AppRoleActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appRoleGet",
  })
  @ApiOkResponse({
    type: AppRoleDto,
  })
  async item(@Param("id") id: string): Promise<AppRoleDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appRoleCreate",
  })
  @ApiOkResponse({
    type: AppRoleDto,
  })
  async create(@Body() data: AppRoleCreateDto): Promise<AppRoleDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppRoleDto,
  })
  @ApiOperation({
    operationId: "appRoleUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppRoleUpdateDto
  ): Promise<AppRoleDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appRoleDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appRoleSearch",
  })
  @ApiOkResponse({
    type: AppRoleSearchResponse,
  })
  async search(
    @Body() request: AppRoleSearchRequest
  ): Promise<AppRoleSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }
}
