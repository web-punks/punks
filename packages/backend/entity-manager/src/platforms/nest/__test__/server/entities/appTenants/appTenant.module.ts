import { Module } from "@nestjs/common"
import { AppTenantAdapter } from "./appTenant.adapter"
import { AppTenantAuthMiddleware } from "./appTenant.authentication"
import { AppTenantEntityManager } from "./appTenant.manager"
import { AppTenantQueryBuilder } from "./appTenant.query"
import { EntityManagerModule } from "../../../.."
import { AppTenantSerializer } from "./appTenant.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppTenantAdapter,
    AppTenantAuthMiddleware,
    AppTenantEntityManager,
    AppTenantQueryBuilder,
    AppTenantSerializer,
  ],
  exports: [AppTenantEntityManager],
})
export class AppTenantEntityModule {}
