import { FileGetDownloadUrlTemplate } from "./file-get-download-url"
import { FileUploadTemplate } from "./file-upload"

export const FileTemplates = [FileGetDownloadUrlTemplate, FileUploadTemplate]
