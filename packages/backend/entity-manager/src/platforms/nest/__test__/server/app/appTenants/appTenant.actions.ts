import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppTenantCreateDto,
  AppTenantDto,
  AppTenantListItemDto,
  AppTenantUpdateDto,
} from "./appTenant.dto"
import { AppTenantEntity } from "../../database/core/entities/appTenant.entity"
import {
  AppTenantCursor,
  AppTenantDeleteParameters,
  AppTenantEntityId,
  AppTenantFacets,
  AppTenantSearchParameters,
  AppTenantSorting,
} from "../../entities/appTenants/appTenant.models"

@WpEntityActions("appTenant")
export class AppTenantActions extends NestEntityActions<
  AppTenantEntity,
  AppTenantEntityId,
  AppTenantCreateDto,
  AppTenantUpdateDto,
  AppTenantDto,
  AppTenantListItemDto,
  AppTenantDeleteParameters,
  AppTenantSearchParameters,
  AppTenantSorting,
  AppTenantCursor,
  AppTenantFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appTenant", registry)
  }
}
