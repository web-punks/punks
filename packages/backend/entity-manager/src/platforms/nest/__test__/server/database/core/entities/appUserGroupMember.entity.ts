import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "../../../../.."
import { AppUserEntity } from "./appUser.entity"
import { AppUserGroupEntity } from "./appUserGroup.entity"

@Unique("groupMember", ["group", "user"])
@Entity("appUserGroupMembers")
@WpEntity("appUserGroupMember")
export class AppUserGroupMemberEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @ManyToOne(() => AppUserGroupEntity, (group) => group.userMembers)
  group: AppUserGroupEntity

  @ManyToOne(() => AppUserEntity, (user) => user.userGroups)
  user: AppUserEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
