import { EntityVersioningProvider } from "./entity-versioning"

export const VersioningProviders = [EntityVersioningProvider]
