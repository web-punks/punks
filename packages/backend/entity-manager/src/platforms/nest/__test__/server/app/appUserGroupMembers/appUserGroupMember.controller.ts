import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { AppUserGroupMemberActions } from "./appUserGroupMember.actions"
import {
  AppUserGroupMemberCreateDto,
  AppUserGroupMemberDto,
  AppUserGroupMemberSearchRequest,
  AppUserGroupMemberSearchResponse,
  AppUserGroupMemberUpdateDto,
} from "./appUserGroupMember.dto"

@Controller("v1/appUserGroupMember")
export class AppUserGroupMemberController {
  constructor(private readonly actions: AppUserGroupMemberActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appUserGroupMemberGet",
  })
  @ApiOkResponse({
    type: AppUserGroupMemberDto,
  })
  async item(
    @Param("id") id: string
  ): Promise<AppUserGroupMemberDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appUserGroupMemberCreate",
  })
  @ApiOkResponse({
    type: AppUserGroupMemberDto,
  })
  async create(
    @Body() data: AppUserGroupMemberCreateDto
  ): Promise<AppUserGroupMemberDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppUserGroupMemberDto,
  })
  @ApiOperation({
    operationId: "appUserGroupMemberUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppUserGroupMemberUpdateDto
  ): Promise<AppUserGroupMemberDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appUserGroupMemberDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appUserGroupMemberSearch",
  })
  @ApiOkResponse({
    type: AppUserGroupMemberSearchResponse,
  })
  async search(
    @Body() request: AppUserGroupMemberSearchRequest
  ): Promise<AppUserGroupMemberSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }
}
