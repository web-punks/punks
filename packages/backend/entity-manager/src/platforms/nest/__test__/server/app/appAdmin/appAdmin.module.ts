import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { SetupTemplates } from "./templates"
import { AppAdminController } from "./appAdmin.controller"

@Module({
  imports: [SharedModule],
  providers: [...SetupTemplates],
  controllers: [AppAdminController],
})
export class AppAdminAppModule {}
