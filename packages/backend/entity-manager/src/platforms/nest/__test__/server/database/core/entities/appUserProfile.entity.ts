import {
  CreateDateColumn,
  Column,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "../../../../.."

@Entity("appUserProfiles")
@WpEntity("appUserProfile")
export class AppUserProfileEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255 })
  firstName: string

  @Column({ type: "varchar", length: 255 })
  lastName: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
