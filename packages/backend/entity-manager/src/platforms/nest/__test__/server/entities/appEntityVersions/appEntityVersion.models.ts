import { DeepPartial } from "../../../../../.."
import { AppEntityVersionEntity } from "../../database/core/entities/appEntityVersion.entity"

export type AppEntityVersionEntityId = string

export type AppEntityVersionCreateData = DeepPartial<
  Omit<AppEntityVersionEntity, "id">
>
export type AppEntityVersionUpdateData = DeepPartial<
  Omit<AppEntityVersionEntity, "id">
>

export enum AppEntityVersionSorting {
  Name = "Name",
}

export type AppEntityVersionCursor = number

export class AppEntityVersionSearchFilters {}

export class AppEntityVersionFacets {}

export type AppEntityVersionSheetItem = AppEntityVersionEntity
