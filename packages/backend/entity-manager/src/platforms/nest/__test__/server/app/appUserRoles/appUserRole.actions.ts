import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppUserRoleCreateDto,
  AppUserRoleDto,
  AppUserRoleListItemDto,
  AppUserRoleUpdateDto,
} from "./appUserRole.dto"
import { AppUserRoleEntity } from "../../database/core/entities/appUserRole.entity"
import {
  AppUserRoleCursor,
  AppUserRoleDeleteParameters,
  AppUserRoleEntityId,
  AppUserRoleFacets,
  AppUserRoleSearchParameters,
  AppUserRoleSorting,
} from "../../entities/appUserRoles/appUserRole.models"

@WpEntityActions("appUserRole")
export class AppUserRoleActions extends NestEntityActions<
  AppUserRoleEntity,
  AppUserRoleEntityId,
  AppUserRoleCreateDto,
  AppUserRoleUpdateDto,
  AppUserRoleDto,
  AppUserRoleListItemDto,
  AppUserRoleDeleteParameters,
  AppUserRoleSearchParameters,
  AppUserRoleSorting,
  AppUserRoleCursor,
  AppUserRoleFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserRole", registry)
  }
}
