import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { AppCompanyEntity } from "../../database/core/entities/appCompany.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppCompanyCreateData,
  AppCompanyCursor,
  AppCompanyEntityId,
  AppCompanySearchParameters,
  AppCompanySheetItem,
  AppCompanySorting,
  AppCompanyUpdateData,
} from "./appCompany.models"

@WpEntitySerializer("appCompany")
export class AppCompanySerializer extends NestEntitySerializer<
  AppCompanyEntity,
  AppCompanyEntityId,
  AppCompanyCreateData,
  AppCompanyUpdateData,
  AppCompanySearchParameters,
  AppCompanySorting,
  AppCompanyCursor,
  AppCompanySheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appCompany", registry)
  }

  protected async loadEntities(
    filters: AppCompanySearchParameters
  ): Promise<AppCompanyEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppCompanyEntity[]
  ): Promise<AppCompanyEntity[]> {
    return entities
  }

  protected async importItem(
    item: AppCompanySheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppCompanyEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
