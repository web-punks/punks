import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "../../../../../.."
import {
  AppEntityVersionSearchParameters,
  AppEntityVersionSearchResults,
} from "../../entities/appEntityVersions/appEntityVersion.types"

export class AppEntityVersionDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppEntityVersionListItemDto {
  @ApiProperty()
  id: string
}

export class AppEntityVersionCreateDto {}

export class AppEntityVersionUpdateDto {
  @ApiProperty()
  id: string
}

export class AppEntityVersionSearchRequest {
  @ApiProperty()
  params: AppEntityVersionSearchParameters
}

export class AppEntityVersionSearchResponse extends AppEntityVersionSearchResults<AppEntityVersionListItemDto> {
  @ApiProperty({ type: [AppEntityVersionListItemDto] })
  items: AppEntityVersionListItemDto[]
}

export class AppEntityVersionEntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppEntityVersionExportRequest {
  @ApiProperty()
  options: AppEntityVersionEntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: AppEntityVersionSearchParameters
}

export class AppEntityVersionExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class AppEntityVersionSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class AppEntityVersionSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}
