import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { AppUserGroupEntity } from "../../database/core/entities/appUserGroup.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppUserGroupCreateData,
  AppUserGroupCursor,
  AppUserGroupEntityId,
  AppUserGroupSearchParameters,
  AppUserGroupSheetItem,
  AppUserGroupSorting,
  AppUserGroupUpdateData,
} from "./appUserGroup.models"

@WpEntitySerializer("appUserGroup")
export class AppUserGroupSerializer extends NestEntitySerializer<
  AppUserGroupEntity,
  AppUserGroupEntityId,
  AppUserGroupCreateData,
  AppUserGroupUpdateData,
  AppUserGroupSearchParameters,
  AppUserGroupSorting,
  AppUserGroupCursor,
  AppUserGroupSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroup", registry)
  }

  protected async loadEntities(
    filters: AppUserGroupSearchParameters
  ): Promise<AppUserGroupEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppUserGroupEntity[]
  ): Promise<AppUserGroupEntity[]> {
    return entities
  }

  protected async importItem(
    item: AppUserGroupSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppUserGroupEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
