import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../../.."
import {
  AppPermissionEntity,
  AppPermissionEntityId,
} from "../../database/core/entities/appPermission.entity"
import {
  AppPermissionFacets,
  AppPermissionSorting,
} from "./appPermission.models"
import { AppPermissionSearchParameters } from "./appPermission.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appPermission")
export class AppPermissionQueryBuilder extends NestTypeOrmQueryBuilder<
  AppPermissionEntity,
  AppPermissionEntityId,
  AppPermissionSearchParameters,
  AppPermissionSorting,
  AppPermissionFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appPermission", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ):
    | FindOptionsWhere<AppPermissionEntity>
    | FindOptionsWhere<AppPermissionEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppPermissionSearchParameters
  ): FindOptionsOrder<AppPermissionEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppPermissionSearchParameters
  ):
    | FindOptionsWhere<AppPermissionEntity>
    | FindOptionsWhere<AppPermissionEntity>[] {
    return {
      ...(request.filters?.uid
        ? {
            uid: this.clause.stringFilter(request.filters.uid),
          }
        : {}),
    }
  }

  protected calculateFacets(
    request: AppPermissionSearchParameters,
    context?: AppAuthContext
  ): Promise<AppPermissionFacets> {
    return Promise.resolve({})
  }
}
