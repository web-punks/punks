import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  AppRolePermissionEntity,
  AppRolePermissionEntityId,
} from "../entities/appRolePermission.entity"
import { WpEntityRepository } from "../../../../../decorators"
import { NestTypeOrmRepository } from "../../../../../integrations"

@WpEntityRepository("appRolePermission")
export class AppRolePermissionRepository extends NestTypeOrmRepository<
  AppRolePermissionEntity,
  AppRolePermissionEntityId
> {
  constructor(
    @InjectRepository(AppRolePermissionEntity, "core")
    repository: Repository<AppRolePermissionEntity>
  ) {
    super(repository)
  }
}
