import { Module } from "@nestjs/common"
import { EmailTemplates } from "./email"

@Module({
  providers: [...EmailTemplates],
})
export class MessagingModule {}
