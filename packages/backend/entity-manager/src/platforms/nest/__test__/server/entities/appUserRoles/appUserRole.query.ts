import {
  FindOptionsOrder,
  FindOptionsRelations,
  FindOptionsWhere,
  In,
} from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../../.."
import { AppUserRoleEntity } from "../../database/core/entities/appUserRole.entity"
import {
  AppUserRoleEntityId,
  AppUserRoleFacets,
  AppUserRoleSearchParameters,
  AppUserRoleSorting,
} from "./appUserRole.models"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appUserRole")
export class AppUserRoleQueryBuilder extends NestTypeOrmQueryBuilder<
  AppUserRoleEntity,
  AppUserRoleEntityId,
  AppUserRoleSearchParameters,
  AppUserRoleSorting,
  AppUserRoleFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserRole", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext | undefined
  ):
    | FindOptionsWhere<AppUserRoleEntity>
    | FindOptionsWhere<AppUserRoleEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppUserRoleSearchParameters
  ): FindOptionsOrder<AppUserRoleEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppUserRoleSearchParameters
  ):
    | FindOptionsWhere<AppUserRoleEntity>
    | FindOptionsWhere<AppUserRoleEntity>[] {
    return {
      ...(request.filters?.userId
        ? {
            user: {
              id: request.filters.userId,
            },
          }
        : {}),
      ...(request.filters?.roleIds
        ? {
            role: {
              id: In(request.filters.roleIds),
            },
          }
        : undefined),
    }
  }

  protected calculateFacets(
    request: AppUserRoleSearchParameters,
    context?: AppAuthContext
  ): Promise<AppUserRoleFacets> {
    return Promise.resolve({})
  }

  protected getRelationsToLoad(
    request: AppUserRoleSearchParameters,
    context: AppAuthContext | undefined
  ): FindOptionsRelations<AppUserRoleEntity> | undefined {
    return {
      role: true,
      user: true,
    }
  }
}
