import { IAuthUserService, WpUserService } from "../../../../../../extensions"
import { AppUserEntity } from "../../../../database/core/entities/appUser.entity"
import { AppUserEntityManager } from "../../../../entities/appUsers/appUser.manager"
import { AppUserContext } from "../.."
import { AppOrganizationEntityManager } from "../../../../entities/appOrganizations/appOrganization.manager"
import { AppUserProfileEntityManager } from "../../../../entities/appUserProfiles/appUserProfile.manager"
import { AuthUserRegistrationData } from "../../models"
import { AppTenantEntityManager } from "../../../../entities/appTenants/appTenant.manager"
import { AppDirectoryEntityManager } from "../../../../entities/appDirectories/appDirectory.manager"

@WpUserService()
export class AuthUserService
  implements
    IAuthUserService<AppUserEntity, AppUserContext, AuthUserRegistrationData>
{
  constructor(
    private readonly userManager: AppUserEntityManager,
    private readonly userProfileManager: AppUserProfileEntityManager,
    private readonly organizationsManager: AppOrganizationEntityManager,
    private readonly directoriesManager: AppDirectoryEntityManager,
    private readonly tenantsManager: AppTenantEntityManager
  ) {}

  async getById(id: string): Promise<AppUserEntity> {
    const user = await this.userManager.manager.get.execute(id)
    if (!user) {
      throw new Error("User not found")
    }
    return user
  }

  async getByEmail(
    email: string,
    context?: AppUserContext | undefined
  ): Promise<AppUserEntity> {
    const user = await this.userManager.manager.search.execute({
      filters: {
        email,
        organizationUid: context?.organizationUid,
      },
    })
    return user.items[0]
  }

  async getByUserName(
    userName: string,
    context?: AppUserContext | undefined
  ): Promise<AppUserEntity> {
    const user = await this.userManager.manager.search.execute({
      filters: {
        userName,
        organizationUid: context?.organizationUid,
      },
    })
    return user.items[0]
  }

  async update(id: string, data: Partial<AppUserEntity>): Promise<void> {
    await this.userManager.manager.update.execute(id, data)
  }

  async delete(id: string): Promise<void> {
    await this.userManager.manager.delete.execute(id)
  }

  async create(
    email: string,
    userName: string,
    data: AuthUserRegistrationData,
    context?: AppUserContext | undefined
  ): Promise<AppUserEntity> {
    const organization = context?.organizationUid
      ? await this.resolveOrganization(context.organizationUid)
      : undefined

    const tenant = context?.tenantUid
      ? await this.resolveTenant(context.tenantUid)
      : undefined

    const directory = tenant
      ? await this.resolveTenantDefaultDirectory(tenant.uid)
      : undefined

    const userProfile = await this.userProfileManager.manager.create.execute({
      firstName: data.firstName,
      lastName: data.lastName,
    })

    const userRef = await this.userManager.manager.create.execute({
      email,
      profile: userProfile,
      verified: false,
      userName,
      temporaryPassword: false,
      disabled: false,
      passwordHash: "",
      passwordUpdateTimestamp: new Date(),
      organization: organization
        ? {
            id: organization.id,
          }
        : undefined,
      directory: directory ? { id: directory.id } : undefined,
      tenant: tenant ? { id: tenant.id } : undefined,
    })

    const user = await this.userManager.manager.get.execute(userRef.id)
    if (!user) {
      throw new Error(`Newly created user not found - userName:${userName}`)
    }

    return user
  }

  private async resolveTenantDefaultDirectory(tenantUid: string) {
    const result = await this.directoriesManager.manager.search.execute({
      filters: {
        tenantUid,
        default: true,
      },
    })
    if (!result.items.length) {
      throw new Error(`Default directory for tenant uid ${tenantUid} not found`)
    }
    return result.items[0]
  }

  private async resolveOrganization(organizationUid: string) {
    const result = await this.organizationsManager.manager.search.execute({
      filters: {
        uid: organizationUid,
      },
    })
    if (!result.items.length) {
      throw new Error(`Organization with uid ${organizationUid} not found`)
    }
    return result.items[0]
  }

  private async resolveTenant(tenantUid: string) {
    const result = await this.tenantsManager.manager.search.execute({
      filters: {
        uid: tenantUid,
      },
    })
    if (!result.items.length) {
      throw new Error(`Tenant with uid ${tenantUid} not found`)
    }
    return result.items[0]
  }
}
