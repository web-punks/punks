import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import {
  AppPermissionEntity,
  AppPermissionEntityId,
} from "../entities/appPermission.entity"
import { WpEntityRepository } from "../../../../../decorators"
import { NestTypeOrmRepository } from "../../../../../integrations"

@WpEntityRepository("appPermission")
export class AppPermissionRepository extends NestTypeOrmRepository<
  AppPermissionEntity,
  AppPermissionEntityId
> {
  constructor(
    @InjectRepository(AppPermissionEntity, "core")
    repository: Repository<AppPermissionEntity>
  ) {
    super(repository)
  }
}
