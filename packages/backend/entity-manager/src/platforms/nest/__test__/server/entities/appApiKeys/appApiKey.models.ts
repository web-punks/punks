import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "../../../../../.."
import { AppApiKeyEntity } from "../../database/core/entities/appApiKey.entity"
import { IdFilter } from "../../shared/api/fields"

export type AppApiKeyCreateData = DeepPartial<Omit<AppApiKeyEntity, "id">>
export type AppApiKeyUpdateData = DeepPartial<Omit<AppApiKeyEntity, "id">>

export enum AppApiKeySorting {
  Name = "Name",
}

export type AppApiKeyCursor = number

export class AppApiKeySearchFilters {
  @ApiProperty({ required: false })
  id?: IdFilter

  @ApiProperty({ required: false })
  key?: IdFilter
}

export class AppApiKeyFacets {}

export type AppApiKeySheetItem = AppApiKeyEntity
