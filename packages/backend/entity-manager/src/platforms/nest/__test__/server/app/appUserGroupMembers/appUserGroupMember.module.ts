import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppUserGroupMemberActions } from "./appUserGroupMember.actions"
import { AppUserGroupMemberController } from "./appUserGroupMember.controller"
import { AppUserGroupMemberConverter } from "./appUserGroupMember.converter"
import { AppUserGroupMemberEntityModule } from "../../entities/appUserGroupMembers/appUserGroupMember.module"

@Module({
  imports: [SharedModule, AppUserGroupMemberEntityModule],
  providers: [AppUserGroupMemberActions, AppUserGroupMemberConverter],
  controllers: [AppUserGroupMemberController],
})
export class AppUserGroupMemberAppModule {}
