import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { AppRoleOrganizationalUnitTypeAdapter } from "./appRoleOrganizationalUnitType.adapter"
import { AppRoleOrganizationalUnitTypeAuthMiddleware } from "./appRoleOrganizationalUnitType.authentication"
import { AppRoleOrganizationalUnitTypeEntityManager } from "./appRoleOrganizationalUnitType.manager"
import { AppRoleOrganizationalUnitTypeQueryBuilder } from "./appRoleOrganizationalUnitType.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppRoleOrganizationalUnitTypeAdapter,
    AppRoleOrganizationalUnitTypeAuthMiddleware,
    AppRoleOrganizationalUnitTypeEntityManager,
    AppRoleOrganizationalUnitTypeQueryBuilder,
  ],
  exports: [AppRoleOrganizationalUnitTypeEntityManager],
})
export class AppRoleOrganizationalUnitTypeEntityModule {}
