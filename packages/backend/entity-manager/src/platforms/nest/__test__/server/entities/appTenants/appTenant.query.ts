import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../../.."
import { AppTenantEntity } from "../../database/core/entities/appTenant.entity"
import {
  AppTenantEntityId,
  AppTenantFacets,
  AppTenantSearchParameters,
  AppTenantSorting,
} from "./appTenant.models"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appTenant")
export class AppTenantQueryBuilder extends NestTypeOrmQueryBuilder<
  AppTenantEntity,
  AppTenantEntityId,
  AppTenantSearchParameters,
  AppTenantSorting,
  AppTenantFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appTenant", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext | undefined
  ): FindOptionsWhere<AppTenantEntity> | FindOptionsWhere<AppTenantEntity>[] {
    // todo: implement authentication context filtering
    return {}
  }

  protected buildSortingClause(
    request: AppTenantSearchParameters
  ): FindOptionsOrder<AppTenantEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppTenantSearchParameters
  ): FindOptionsWhere<AppTenantEntity> | FindOptionsWhere<AppTenantEntity>[] {
    // todo: implement query filters
    return {}
  }

  protected calculateFacets(
    request: AppTenantSearchParameters,
    context?: AppAuthContext
  ): Promise<AppTenantFacets> {
    // todo: implement search facet queries
    return Promise.resolve({})
  }
}
