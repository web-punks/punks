import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppDivisionCreateData,
  AppDivisionCursor,
  AppDivisionDeleteParameters,
  AppDivisionEntityId,
  AppDivisionFacets,
  AppDivisionSearchParameters,
  AppDivisionSorting,
  AppDivisionUpdateData,
} from "./appDivision.models"
import { AppDivisionEntity } from "../../database/core/entities/appDivision.entity"

@WpEntityManager("appDivision")
export class AppDivisionEntityManager extends NestEntityManager<
  AppDivisionEntity,
  AppDivisionEntityId,
  AppDivisionCreateData,
  AppDivisionUpdateData,
  AppDivisionDeleteParameters,
  AppDivisionSearchParameters,
  AppDivisionSorting,
  AppDivisionCursor,
  AppDivisionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appDivision", registry)
  }
}
