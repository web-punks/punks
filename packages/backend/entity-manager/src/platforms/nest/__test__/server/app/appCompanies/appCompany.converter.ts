import { IEntityConverter, WpEntityConverter } from "../../../../../.."
import {
  AppCompanyCreateDto,
  AppCompanyDto,
  AppCompanyListItemDto,
  AppCompanyUpdateDto,
} from "./appCompany.dto"
import { AppCompanyEntity } from "../../database/core/entities/appCompany.entity"

@WpEntityConverter("appCompany")
export class AppCompanyConverter
  implements
    IEntityConverter<
      AppCompanyEntity,
      AppCompanyDto,
      AppCompanyListItemDto,
      AppCompanyCreateDto,
      AppCompanyUpdateDto
    >
{
  toListItemDto(entity: AppCompanyEntity): AppCompanyListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppCompanyEntity): AppCompanyDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppCompanyCreateDto): Partial<AppCompanyEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppCompanyUpdateDto): Partial<AppCompanyEntity> {
    return {
      ...input,
    }
  }
}
