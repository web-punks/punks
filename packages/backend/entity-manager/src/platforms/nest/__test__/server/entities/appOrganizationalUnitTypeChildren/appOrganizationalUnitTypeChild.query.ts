import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../../.."
import {
  AppOrganizationalUnitTypeChildEntity,
  AppOrganizationalUnitTypeChildEntityId,
} from "../../database/core/entities/appOrganizationalUnitTypeChild.entity"
import {
  AppOrganizationalUnitTypeChildFacets,
  AppOrganizationalUnitTypeChildSorting,
} from "./appOrganizationalUnitTypeChild.models"
import { AppOrganizationalUnitTypeChildSearchParameters } from "./appOrganizationalUnitTypeChild.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appOrganizationalUnitTypeChild")
export class AppOrganizationalUnitTypeChildQueryBuilder extends NestTypeOrmQueryBuilder<
  AppOrganizationalUnitTypeChildEntity,
  AppOrganizationalUnitTypeChildEntityId,
  AppOrganizationalUnitTypeChildSearchParameters,
  AppOrganizationalUnitTypeChildSorting,
  AppOrganizationalUnitTypeChildFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganizationalUnitTypeChild", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ):
    | FindOptionsWhere<AppOrganizationalUnitTypeChildEntity>
    | FindOptionsWhere<AppOrganizationalUnitTypeChildEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppOrganizationalUnitTypeChildSearchParameters
  ): FindOptionsOrder<AppOrganizationalUnitTypeChildEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppOrganizationalUnitTypeChildSearchParameters
  ):
    | FindOptionsWhere<AppOrganizationalUnitTypeChildEntity>
    | FindOptionsWhere<AppOrganizationalUnitTypeChildEntity>[] {
    return {
      ...(request.filters?.parentTypeId
        ? {
            parentType: {
              id: request.filters.parentTypeId,
            },
          }
        : {}),
    }
  }

  protected calculateFacets(
    request: AppOrganizationalUnitTypeChildSearchParameters,
    context?: AppAuthContext
  ): Promise<AppOrganizationalUnitTypeChildFacets> {
    return Promise.resolve({})
  }
}
