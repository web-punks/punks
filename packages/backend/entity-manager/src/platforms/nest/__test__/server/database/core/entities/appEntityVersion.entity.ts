import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryColumn,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "../../../../../decorators"
import { EntityVersionOperation } from "../../../../../../../abstractions/versioning"

@Entity("appEntityVersions")
@WpEntity("appEntityVersion")
@Index("entityId_entityType", ["entityId", "entityType"])
export class AppEntityVersionEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255 })
  entityId: string

  @Column({ type: "varchar", length: 255 })
  entityType: string

  @Column({
    type: "jsonb",
    nullable: true,
  })
  data: any

  @Column({ type: "enum", enum: EntityVersionOperation })
  operationType: EntityVersionOperation

  @Column({ type: "uuid", nullable: true })
  operationUserId?: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
