import { RolesGuardOptions } from "../../../../../extensions"
import { buildRolesGuard } from "../../../../../extensions/authentication/decorators/guards"
import { AppRoles, InheritedRoles } from "../roles"

export const CompanyUsers = (options?: RolesGuardOptions) =>
  buildRolesGuard(
    {
      mainRole: AppRoles.CompanyUser,
      secondaryRoles: [],
    },
    options
  )

export const CompanyManagers = (options?: RolesGuardOptions) =>
  buildRolesGuard(
    {
      mainRole: AppRoles.CompanyManager,
      secondaryRoles: InheritedRoles.CompanyManager,
    },
    options
  )

export const CompanyAdmins = (options?: RolesGuardOptions) =>
  buildRolesGuard(
    {
      mainRole: AppRoles.CompanyAdmin,
      secondaryRoles: InheritedRoles.CompanyAdmin,
    },
    options
  )

export const OrganizationUsers = (options?: RolesGuardOptions) =>
  buildRolesGuard(
    {
      mainRole: AppRoles.OrganizationUser,
      secondaryRoles: InheritedRoles.OrganizationAdmin,
    },
    options
  )

export const OrganizationManagers = (options?: RolesGuardOptions) =>
  buildRolesGuard(
    {
      mainRole: AppRoles.OrganizationManager,
      secondaryRoles: InheritedRoles.OrganizationManager,
    },
    options
  )

export const OrganizationAdmins = (options?: RolesGuardOptions) =>
  buildRolesGuard(
    {
      mainRole: AppRoles.OrganizationAdmin,
      secondaryRoles: InheritedRoles.OrganizationAdmin,
    },
    options
  )

export const TenantAdmins = (options?: RolesGuardOptions) =>
  buildRolesGuard(
    {
      mainRole: AppRoles.TenantAdmin,
      secondaryRoles: InheritedRoles.TenantAdmin,
    },
    options
  )
