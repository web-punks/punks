import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { AppDivisionActions } from "./appDivision.actions"
import {
  AppDivisionCreateDto,
  AppDivisionDto,
  AppDivisionSearchRequest,
  AppDivisionSearchResponse,
  AppDivisionUpdateDto,
} from "./appDivision.dto"

@Controller("v1/appDivision")
export class AppDivisionController {
  constructor(private readonly actions: AppDivisionActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appDivisionGet",
  })
  @ApiOkResponse({
    type: AppDivisionDto,
  })
  async item(@Param("id") id: string): Promise<AppDivisionDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appDivisionCreate",
  })
  @ApiOkResponse({
    type: AppDivisionDto,
  })
  async create(@Body() data: AppDivisionCreateDto): Promise<AppDivisionDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppDivisionDto,
  })
  @ApiOperation({
    operationId: "appDivisionUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppDivisionUpdateDto
  ): Promise<AppDivisionDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appDivisionDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appDivisionSearch",
  })
  @ApiOkResponse({
    type: AppDivisionSearchResponse,
  })
  async search(
    @Body() request: AppDivisionSearchRequest
  ): Promise<AppDivisionSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }
}
