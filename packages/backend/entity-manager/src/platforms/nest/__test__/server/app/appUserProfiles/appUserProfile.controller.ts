import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { AppUserProfileActions } from "./appUserProfile.actions"
import {
  AppUserProfileCreateDto,
  AppUserProfileDto,
  AppUserProfileSearchRequest,
  AppUserProfileSearchResponse,
  AppUserProfileUpdateDto,
} from "./appUserProfile.dto"

@Controller("v1/appUserProfile")
export class AppUserProfileController {
  constructor(private readonly actions: AppUserProfileActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appUserProfileGet",
  })
  @ApiOkResponse({
    type: AppUserProfileDto,
  })
  async item(@Param("id") id: string): Promise<AppUserProfileDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appUserProfileCreate",
  })
  @ApiOkResponse({
    type: AppUserProfileDto,
  })
  async create(
    @Body() data: AppUserProfileCreateDto
  ): Promise<AppUserProfileDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppUserProfileDto,
  })
  @ApiOperation({
    operationId: "appUserProfileUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppUserProfileUpdateDto
  ): Promise<AppUserProfileDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appUserProfileDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appUserProfileSearch",
  })
  @ApiOkResponse({
    type: AppUserProfileSearchResponse,
  })
  async search(
    @Body() request: AppUserProfileSearchRequest
  ): Promise<AppUserProfileSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }
}
