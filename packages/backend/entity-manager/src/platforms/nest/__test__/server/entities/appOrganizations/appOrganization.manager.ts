import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppOrganizationCreateData,
  AppOrganizationCursor,
  AppOrganizationDeleteParameters,
  AppOrganizationEntityId,
  AppOrganizationFacets,
  AppOrganizationSearchParameters,
  AppOrganizationSorting,
  AppOrganizationUpdateData,
} from "./appOrganization.models"
import { AppOrganizationEntity } from "../../database/core/entities/appOrganization.entity"

@WpEntityManager("appOrganization")
export class AppOrganizationEntityManager extends NestEntityManager<
  AppOrganizationEntity,
  AppOrganizationEntityId,
  AppOrganizationCreateData,
  AppOrganizationUpdateData,
  AppOrganizationDeleteParameters,
  AppOrganizationSearchParameters,
  AppOrganizationSorting,
  AppOrganizationCursor,
  AppOrganizationFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganization", registry)
  }
}
