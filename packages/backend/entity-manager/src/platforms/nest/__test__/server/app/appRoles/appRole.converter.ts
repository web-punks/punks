import { IEntityConverter, WpEntityConverter } from "../../../../../.."
import {
  AppRoleCreateDto,
  AppRoleDto,
  AppRoleListItemDto,
  AppRoleUpdateDto,
} from "./appRole.dto"
import { AppRoleEntity } from "../../database/core/entities/appRole.entity"

@WpEntityConverter("appRole")
export class AppRoleConverter
  implements
    IEntityConverter<
      AppRoleEntity,
      AppRoleDto,
      AppRoleListItemDto,
      AppRoleCreateDto,
      AppRoleUpdateDto
    >
{
  toListItemDto(entity: AppRoleEntity): AppRoleListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppRoleEntity): AppRoleDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppRoleCreateDto): Partial<AppRoleEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppRoleUpdateDto): Partial<AppRoleEntity> {
    return {
      ...input,
    }
  }
}
