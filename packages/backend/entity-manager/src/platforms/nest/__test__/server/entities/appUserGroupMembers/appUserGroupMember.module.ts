import { Module } from "@nestjs/common"
import { AppUserGroupMemberAdapter } from "./appUserGroupMember.adapter"
import { AppUserGroupMemberAuthMiddleware } from "./appUserGroupMember.authentication"
import { AppUserGroupMemberEntityManager } from "./appUserGroupMember.manager"
import { AppUserGroupMemberQueryBuilder } from "./appUserGroupMember.query"
import { AppUserGroupMemberSerializer } from "./appUserGroupMember.serializer"
import { EntityManagerModule } from "../../../.."

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppUserGroupMemberAdapter,
    AppUserGroupMemberAuthMiddleware,
    AppUserGroupMemberEntityManager,
    AppUserGroupMemberQueryBuilder,
    AppUserGroupMemberSerializer,
  ],
  exports: [AppUserGroupMemberEntityManager],
})
export class AppUserGroupMemberEntityModule {}
