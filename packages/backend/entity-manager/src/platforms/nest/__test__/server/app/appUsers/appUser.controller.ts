import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { AppUserActions } from "./appUser.actions"
import {
  AppUserCreateDto,
  AppUserDto,
  AppUserSearchRequest,
  AppUserSearchResponse,
  AppUserUpdateDto,
} from "./appUser.dto"

@Controller("v1/appUser")
export class AppUserController {
  constructor(private readonly actions: AppUserActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appUserGet",
  })
  @ApiOkResponse({
    type: AppUserDto,
  })
  async item(@Param("id") id: string): Promise<AppUserDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appUserCreate",
  })
  @ApiOkResponse({
    type: AppUserDto,
  })
  async create(@Body() data: AppUserCreateDto): Promise<AppUserDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppUserDto,
  })
  @ApiOperation({
    operationId: "appUserUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppUserUpdateDto
  ): Promise<AppUserDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appUserDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appUserSearch",
  })
  @ApiOkResponse({
    type: AppUserSearchResponse,
  })
  async search(
    @Body() request: AppUserSearchRequest
  ): Promise<AppUserSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }
}
