import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppDirectoryActions } from "./appDirectory.actions"
import { AppDirectoryController } from "./appDirectory.controller"
import { AppDirectoryConverter } from "./appDirectory.converter"
import { AppDirectoryEntityModule } from "../../entities/appDirectories/appDirectory.module"

@Module({
  imports: [SharedModule, AppDirectoryEntityModule],
  providers: [AppDirectoryActions, AppDirectoryConverter],
  controllers: [AppDirectoryController],
})
export class AppDirectoryAppModule {}
