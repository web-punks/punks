import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppAuthActions } from "./appAuth.actions"
import { AppAuthController } from "./appAuth.controller"
import { AuthenticationModule } from "../../../../extensions"
import { EntitiesModule } from "../../entities"
import { AppAuthTestController } from "./appAuth.test-controller"

@Module({
  imports: [SharedModule, AuthenticationModule, EntitiesModule],
  providers: [AppAuthActions],
  controllers: [AppAuthController, AppAuthTestController],
})
export class AppAuthAppModule {}
