import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../.."
import {
  AppApiKeyEntity,
  AppApiKeyEntityId,
} from "../../database/core/entities/appApiKey.entity"
import { AppApiKeyFacets, AppApiKeySorting } from "./appApiKey.models"
import { AppApiKeySearchParameters } from "./appApiKey.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appApiKey")
export class AppApiKeyQueryBuilder extends NestTypeOrmQueryBuilder<
  AppApiKeyEntity,
  AppApiKeyEntityId,
  AppApiKeySearchParameters,
  AppApiKeySorting,
  AppApiKeyFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appApiKey", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ): FindOptionsWhere<AppApiKeyEntity> | FindOptionsWhere<AppApiKeyEntity>[] {
    return {
      ...(context?.userContext?.organizationId
        ? {
            organization: {
              id: context?.userContext?.organizationId,
            },
          }
        : {}),
    }
  }

  protected buildSortingClause(
    request: AppApiKeySearchParameters
  ): FindOptionsOrder<AppApiKeyEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppApiKeySearchParameters
  ): FindOptionsWhere<AppApiKeyEntity> | FindOptionsWhere<AppApiKeyEntity>[] {
    return {
      ...(request.filters?.id && {
        id: this.clause.idFilter(request.filters.id),
      }),
      ...(request.filters?.key && {
        key: this.clause.idFilter(request.filters.key),
      }),
    }
  }

  protected calculateFacets(
    request: AppApiKeySearchParameters,
    context?: AppAuthContext
  ): Promise<AppApiKeyFacets> {
    return Promise.resolve({})
  }
}
