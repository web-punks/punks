import { Body, Controller, Post } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  TenantInitializeRequest,
  TenantInitializeResponse,
} from "./templates/tenant-initialize/models"
import { TenantInitializeTemplate } from "./templates/tenant-initialize"
import { InstanceInitializeTemplate } from "./templates/instance-initialize"
import { AppRoles } from "../../infrastructure/authentication/roles"
import {
  OrganizationInitializeRequest,
  OrganizationInitializeResponse,
} from "./templates/organization-register/models"
import { OrganizationRegisterTemplate } from "./templates/organization-register"
import { Public } from "../../../../extensions"
import { TenantAdmins } from "../../infrastructure/authentication/guards"

@Controller("v1/appAdmin")
export class AppAdminController {
  constructor(
    private readonly tenantInit: TenantInitializeTemplate,
    private readonly instanceInit: InstanceInitializeTemplate,
    private readonly organizationInit: OrganizationRegisterTemplate
  ) {}

  @Public()
  @Post("tenantInitialize")
  @ApiOkResponse({
    type: TenantInitializeResponse,
  })
  @ApiOperation({
    operationId: "tenantInitialize",
  })
  async tenantInitialize(
    @Body() request: TenantInitializeRequest
  ): Promise<TenantInitializeResponse> {
    await this.instanceInit.invoke({
      roles: Object.values(AppRoles),
    })
    return await this.tenantInit.invoke(request)
  }

  @TenantAdmins()
  @Post("organizationInitialize")
  @ApiOkResponse({
    type: OrganizationInitializeResponse,
  })
  @ApiOperation({
    operationId: "organizationInitialize",
  })
  async organizationInitialize(
    @Body() request: OrganizationInitializeRequest
  ): Promise<OrganizationInitializeResponse> {
    return await this.organizationInit.invoke(request)
  }
}
