import { ApiProperty } from "@nestjs/swagger"
import {
  AppOrganizationSearchParameters,
  AppOrganizationSearchResults,
} from "../../entities/appOrganizations/appOrganization.models"

export class AppOrganizationReferenceDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  uid: string

  @ApiProperty()
  name: string
}

export class AppOrganizationDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppOrganizationListItemDto {
  @ApiProperty()
  id: string
}

export class AppOrganizationCreateDto {}

export class AppOrganizationUpdateDto {
  @ApiProperty()
  id: string
}

export class AppOrganizationSearchRequest {
  @ApiProperty()
  params: AppOrganizationSearchParameters
}

export class AppOrganizationSearchResponse extends AppOrganizationSearchResults<AppOrganizationListItemDto> {
  @ApiProperty({ type: [AppOrganizationListItemDto] })
  items: AppOrganizationListItemDto[]
}
