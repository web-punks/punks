import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppEmailLogEntity } from "../../database/core/entities/appEmailLog.entity"
import {
  AppEmailLogCreateData,
  AppEmailLogUpdateData,
} from "./appEmailLog.models"

@WpEntityAdapter("appEmailLog")
export class AppEmailLogAdapter
  implements
    IEntityAdapter<
      AppEmailLogEntity,
      AppEmailLogCreateData,
      AppEmailLogUpdateData
    >
{
  createDataToEntity(
    data: AppEmailLogCreateData
  ): DeepPartial<AppEmailLogEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppEmailLogUpdateData
  ): DeepPartial<AppEmailLogEntity> {
    return {
      ...data,
    }
  }
}
