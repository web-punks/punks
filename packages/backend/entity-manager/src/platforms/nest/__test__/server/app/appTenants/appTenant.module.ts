import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppTenantActions } from "./appTenant.actions"
import { AppTenantController } from "./appTenant.controller"
import { AppTenantConverter } from "./appTenant.converter"
import { AppTenantEntityModule } from "../../entities/appTenants/appTenant.module"

@Module({
  imports: [SharedModule, AppTenantEntityModule],
  providers: [AppTenantActions, AppTenantConverter],
  controllers: [AppTenantController],
})
export class AppTenantAppModule {}
