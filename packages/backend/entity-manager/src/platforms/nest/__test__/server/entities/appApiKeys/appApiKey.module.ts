import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { AppApiKeyAdapter } from "./appApiKey.adapter"
import { AppApiKeyAuthMiddleware } from "./appApiKey.authentication"
import { AppApiKeyEntityManager } from "./appApiKey.manager"
import { AppApiKeyQueryBuilder } from "./appApiKey.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppApiKeyAdapter,
    AppApiKeyAuthMiddleware,
    AppApiKeyEntityManager,
    AppApiKeyQueryBuilder,
  ],
  exports: [AppApiKeyEntityManager],
})
export class AppApiKeyEntityModule {}
