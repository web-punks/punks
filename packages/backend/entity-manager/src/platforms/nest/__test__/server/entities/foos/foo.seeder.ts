import { FindOneOptions } from "typeorm"
import { IEntitySeedEntry } from "../../../../../../abstractions/seed"
import { WpEntitySeeder } from "../../../../decorators"
import { FooEntity } from "../../database/core/entities/foo.entity"
import { NestTypeOrmEntitySeeder } from "../../../../integrations"
import { EntityManagerRegistry } from "../../../../ioc"
import { newUuid } from "@punks/backend-core"

@WpEntitySeeder("foo")
export class FooSeeder extends NestTypeOrmEntitySeeder<FooEntity> {
  constructor(registry: EntityManagerRegistry) {
    super(registry)
  }

  protected async getEntries(): Promise<
    IEntitySeedEntry<FooEntity, FindOneOptions<FooEntity>>[]
  > {
    return [
      {
        identifier: {
          where: {
            name: "foo",
          },
        },
        data: () => ({
          id: newUuid(),
          age: 20,
          name: "foo",
        }),
      },
    ]
  }
}
