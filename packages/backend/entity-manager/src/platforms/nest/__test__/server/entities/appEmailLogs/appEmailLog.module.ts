import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { AppEmailLogAdapter } from "./appEmailLog.adapter"
import { AppEmailLogAuthMiddleware } from "./appEmailLog.authentication"
import { AppEmailLogEntityManager } from "./appEmailLog.manager"
import { AppEmailLogQueryBuilder } from "./appEmailLog.query"
import { AppEmailLogSerializer } from "./appEmailLog.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppEmailLogAdapter,
    AppEmailLogAuthMiddleware,
    AppEmailLogEntityManager,
    AppEmailLogQueryBuilder,
    AppEmailLogSerializer,
  ],
  exports: [AppEmailLogEntityManager],
})
export class AppEmailLogEntityModule {}
