import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import { AppUserGroupMemberEntity } from "../../database/core/entities/appUserGroupMember.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppUserGroupMemberCreateData,
  AppUserGroupMemberCursor,
  AppUserGroupMemberEntityId,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSheetItem,
  AppUserGroupMemberSorting,
  AppUserGroupMemberUpdateData,
} from "./appUserGroupMember.models"

@WpEntitySerializer("appUserGroupMember")
export class AppUserGroupMemberSerializer extends NestEntitySerializer<
  AppUserGroupMemberEntity,
  AppUserGroupMemberEntityId,
  AppUserGroupMemberCreateData,
  AppUserGroupMemberUpdateData,
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSorting,
  AppUserGroupMemberCursor,
  AppUserGroupMemberSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroupMember", registry)
  }

  protected async loadEntities(
    filters: AppUserGroupMemberSearchParameters
  ): Promise<AppUserGroupMemberEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppUserGroupMemberEntity[]
  ): Promise<AppUserGroupMemberEntity[]> {
    return entities
  }

  protected async importItem(
    item: AppUserGroupMemberSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppUserGroupMemberEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
