import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppUserGroupCreateDto,
  AppUserGroupDto,
  AppUserGroupListItemDto,
  AppUserGroupUpdateDto,
} from "./appUserGroup.dto"
import { AppUserGroupEntity } from "../../database/core/entities/appUserGroup.entity"
import {
  AppUserGroupCursor,
  AppUserGroupDeleteParameters,
  AppUserGroupEntityId,
  AppUserGroupFacets,
  AppUserGroupSearchParameters,
  AppUserGroupSorting,
} from "../../entities/appUserGroups/appUserGroup.models"

@WpEntityActions("appUserGroup")
export class AppUserGroupActions extends NestEntityActions<
  AppUserGroupEntity,
  AppUserGroupEntityId,
  AppUserGroupCreateDto,
  AppUserGroupUpdateDto,
  AppUserGroupDto,
  AppUserGroupListItemDto,
  AppUserGroupDeleteParameters,
  AppUserGroupSearchParameters,
  AppUserGroupSorting,
  AppUserGroupCursor,
  AppUserGroupFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroup", registry)
  }
}
