import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppApiKeyActions } from "./appApiKey.actions"
import { AppApiKeyController } from "./appApiKey.controller"
import { AppApiKeyConverter } from "./appApiKey.converter"
import { AppApiKeyEntityModule } from "../../entities/appApiKeys/appApiKey.module"
import { AppApiKeySerializer } from "./appApiKey.serializer"

@Module({
  imports: [SharedModule, AppApiKeyEntityModule],
  providers: [AppApiKeyActions, AppApiKeyConverter, AppApiKeySerializer],
  controllers: [AppApiKeyController],
})
export class AppApiKeyAppModule {}
