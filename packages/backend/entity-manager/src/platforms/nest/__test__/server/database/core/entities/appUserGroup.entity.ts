import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "../../../../.."
import { AppOrganizationEntity } from "./appOrganization.entity"
import { AppUserGroupMemberEntity } from "./appUserGroupMember.entity"

@Unique("userGroupUid", ["organization", "uid"])
@Entity("appUserGroups")
@WpEntity("appUserGroup")
export class AppUserGroupEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255 })
  uid: string

  @Column({ type: "varchar", length: 255 })
  name: string

  @ManyToOne(
    () => AppOrganizationEntity,
    (organization) => organization.userGroups
  )
  organization: AppOrganizationEntity

  @OneToMany(() => AppUserGroupMemberEntity, (member) => member.group)
  userMembers: AppUserGroupMemberEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
