import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { EmailProviders } from "./providers"
import { AppEmailLogEntityModule } from "../../entities/appEmailLogs/appEmailLog.module"

@Module({
  imports: [SharedModule, AppEmailLogEntityModule],
  providers: [...EmailProviders],
  exports: [],
})
export class EmailInfrastructureModule {}
