import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { AppCompanyEntity } from "../entities/appCompany.entity"
import { AppCompanyEntityId } from "../../../entities/appCompanies/appCompany.models"
import { NestTypeOrmRepository, WpEntityRepository } from "../../../../.."

@WpEntityRepository("appCompany")
export class AppCompanyRepository extends NestTypeOrmRepository<
  AppCompanyEntity,
  AppCompanyEntityId
> {
  constructor(
    @InjectRepository(AppCompanyEntity, "core")
    repository: Repository<AppCompanyEntity>
  ) {
    super(repository)
  }
}
