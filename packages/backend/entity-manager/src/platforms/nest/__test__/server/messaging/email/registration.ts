import { WpEmailTemplate } from "../../../../decorators"

@WpEmailTemplate("registration")
export class RegistrationEmailTemplate {}
