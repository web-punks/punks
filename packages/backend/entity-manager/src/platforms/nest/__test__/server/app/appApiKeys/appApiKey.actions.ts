import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppApiKeyCreateDto,
  AppApiKeyDto,
  AppApiKeyListItemDto,
  AppApiKeyUpdateDto,
} from "./appApiKey.dto"
import {
  AppApiKeyEntity,
  AppApiKeyEntityId,
} from "../../database/core/entities/appApiKey.entity"
import {
  AppApiKeyCursor,
  AppApiKeyFacets,
  AppApiKeySorting,
} from "../../entities/appApiKeys/appApiKey.models"
import {
  AppApiKeyDeleteParameters,
  AppApiKeySearchParameters,
} from "../../entities/appApiKeys/appApiKey.types"

@WpEntityActions("appApiKey")
export class AppApiKeyActions extends NestEntityActions<
  AppApiKeyEntity,
  AppApiKeyEntityId,
  AppApiKeyCreateDto,
  AppApiKeyUpdateDto,
  AppApiKeyDto,
  AppApiKeyListItemDto,
  AppApiKeyDeleteParameters,
  AppApiKeySearchParameters,
  AppApiKeySorting,
  AppApiKeyCursor,
  AppApiKeyFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appApiKey", registry)
  }
}
