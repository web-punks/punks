import { AppRole } from "../../../../../../extensions"

export interface InstanceInitializeInput {
  roles: AppRole[]
}
