import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppUserGroupCreateData,
  AppUserGroupCursor,
  AppUserGroupDeleteParameters,
  AppUserGroupEntityId,
  AppUserGroupFacets,
  AppUserGroupSearchParameters,
  AppUserGroupSorting,
  AppUserGroupUpdateData,
} from "./appUserGroup.models"
import { AppUserGroupEntity } from "../../database/core/entities/appUserGroup.entity"

@WpEntityManager("appUserGroup")
export class AppUserGroupEntityManager extends NestEntityManager<
  AppUserGroupEntity,
  AppUserGroupEntityId,
  AppUserGroupCreateData,
  AppUserGroupUpdateData,
  AppUserGroupDeleteParameters,
  AppUserGroupSearchParameters,
  AppUserGroupSorting,
  AppUserGroupCursor,
  AppUserGroupFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserGroup", registry)
  }
}
