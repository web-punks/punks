import express from "express"
import { Test } from "@nestjs/testing"
import { ExpressAdapter } from "@nestjs/platform-express"
import { EntityManagerModule } from "../../module"
import { EntityManagerInitializer } from "../../processors/initializer"
import { EntitiesModule } from "./entities"
import { mockPostgresDatabase } from "../../../../__test__/providers/typeorm/mock"
import { EventEmitterModule } from "@nestjs/event-emitter"
import { APP_FILTER, APP_GUARD, ModulesContainer } from "@nestjs/core"
import { AuthGuard, AuthenticationModule, TasksModule } from "../../extensions"
import { AppModules } from "./app"
import { AppExceptionsFilter } from "./shared/errors/middleware"
import { InfrastructureModules } from "./infrastructure"
import { AppAuthContextProvider } from "./infrastructure/authentication"
import { AppAuthAppModule } from "./app/appAuth/appAuth.module"
import { mockDatabaseProviders } from "./database/core"
import { CoreDatabaseEntities } from "./database/core/entities"
import { CoreDatabaseRepositories } from "./database/core/repositories"
import { MessagingModule } from "./messaging/module"
import {
  ConsoleLogger,
  Log,
  LogLevel,
  MetaSerializationType,
} from "@punks/backend-core"
import { InMemoryEmailProvider } from "../../plugins/email/testing/mock"
import { InMemoryBucketProvider } from "../../plugins/buckets/testing"
import { EntityModules } from "./entities/modules"
import { FooController } from "./app/foos/foo.controller"
import { FooActions } from "./app/foos/foo.actions"
import { FooConverter } from "./app/foos/foo.converter"
import { FooAuthMiddleware } from "./entities/foos/foo.authentication"
import { FooAdapter } from "./entities/foos/foo.adapter"
import { FooEntityManager } from "./entities/foos/foo.manager"
import { FooQueryBuilder } from "./entities/foos/foo.query"
import { FooSerializer } from "./entities/foos/foo.serializer"
import { EntityVersioningProvider } from "./infrastructure/versioning/providers/entity-versioning"
import { EntityManagerRegistry } from "../../ioc"
import { InMemoryFileProvider } from "../../plugins/buckets/testing/mock"
import { getEntityManagerProviderToken } from "../../ioc/tokens/symbols"
import { AppOperationLockItemRepository } from "./database/core/repositories/appOperationLock.repository"
import { OperationLockService } from "../../services/operations"
import { Global, Module } from "@nestjs/common"
import { DefaultCacheInstance } from "./infrastructure/cache/providers/default-instance"
import { ScheduleModule } from "@nestjs/schedule"

const startPort = 9000
let currentPort = startPort

const getAvailablePort = () => {
  return currentPort++
}

const FooProviders = [
  FooActions,
  FooConverter,
  FooAdapter,
  FooEntityManager,
  FooQueryBuilder,
  FooSerializer,
]

const PublicProviders = [
  {
    provide: getEntityManagerProviderToken("OperationsLockRepository"),
    useClass: AppOperationLockItemRepository,
  },
  // {
  //   provide: getEntityManagerProviderToken("JobInstancesRepository"),
  //   useClass: AppJobInstanceRepository,
  // },
  // {
  //   provide: getEntityManagerProviderToken("JobDefinitionsRepository"),
  //   useClass: AppJobDefinitionRepository,
  // },
]

export const createTestServer = async (options?: {
  createWebServer?: boolean
  serverStartPort?: number
  useAuthentication?: boolean
  useAuthorization?: boolean
  useVersioning?: boolean
  extraControllers?: any[]
  extraProviders?: any[]
  extraGlobalProviders?: any[]
  extraModules?: any[]
  enableLogging?: boolean
}) => {
  if (options?.enableLogging) {
    Log.configure({
      provider: new ConsoleLogger(),
      options: {
        enabled: true,
        level: LogLevel.Debug,
        serialization: MetaSerializationType.None,
      },
    })
  }
  if (options?.serverStartPort) {
    currentPort = options?.serverStartPort
  }
  const { dataSource } = await mockPostgresDatabase({
    entities: CoreDatabaseEntities,
  })

  @Global()
  @Module({
    providers: [
      ...PublicProviders,
      ...mockDatabaseProviders(dataSource),
      ...CoreDatabaseEntities,
      ...CoreDatabaseRepositories,
      DefaultCacheInstance,
      ...(options?.extraGlobalProviders ?? []),
    ],
    exports: [...CoreDatabaseRepositories, ...PublicProviders],
  })
  class PublicModule {}

  const server = await Test.createTestingModule({
    imports: [
      PublicModule,
      EntityManagerModule,
      EventEmitterModule.forRoot(),
      ScheduleModule.forRoot(),
      TasksModule,
      ...(options?.useAuthentication || options?.useAuthorization
        ? [
            AuthenticationModule.forRoot({
              jwtSecret: "$2a$10$ZWHlR0hxcjwSnNoziP66lO",
              passwordSalt: "$2a$10$MLrSVgluidIHp8IXN0XJNu",
            }),
            AppAuthAppModule,
          ]
        : []),
      ...InfrastructureModules,
      ...EntityModules,
      ...AppModules,
      EntitiesModule,
      MessagingModule,
      ...(options?.extraModules ? options.extraModules : []),
    ],
    controllers: [FooController, ...(options?.extraControllers ?? [])],
    providers: [
      {
        provide: APP_FILTER,
        useClass: AppExceptionsFilter,
      },
      ...(options?.useAuthorization
        ? [
            {
              provide: APP_GUARD,
              useClass: AuthGuard,
            },
          ]
        : []),
      OperationLockService,
      EntityVersioningProvider,
      InMemoryEmailProvider,
      InMemoryBucketProvider,
      InMemoryFileProvider,
      ...FooProviders,
      ...(options?.useAuthentication || options?.useAuthorization
        ? [AppAuthContextProvider, FooAuthMiddleware]
        : []),
      ...(options?.extraProviders ?? []),
    ],
  }).compile()

  const expressServer = express()
  const app = server.createNestApplication(new ExpressAdapter(expressServer))

  if (options?.createWebServer) {
    await app.listen(getAvailablePort())
  }

  await server.init()

  const initializer = app.get(EntityManagerInitializer)
  await initializer.initialize(app, {
    modulesContainer: app.get(ModulesContainer),
    authenticationProvider:
      options?.useAuthentication || options?.useAuthorization
        ? app.get(AppAuthContextProvider)
        : undefined,
    settings: {
      importExport: {
        exportBucket: {
          bucket: "test",
          publicLinksExpirationMinutes: 60,
          rootFolderPath: "test",
        },
      },
      defaultBucketProvider: "inMemory",
      defaultFilesProvider: "inMemory",
      defaultMediaProvider: "inMemory",
    },
  })

  if (options?.useVersioning) {
    const registry = app.get(EntityManagerRegistry)
    const configuration = registry
      .getContainer()
      .getEntityServicesLocator("foo")
      .resolveEntityConfiguration()

    configuration.versioning = {
      enabled: true,
    }
  }

  return {
    app,
    server,
    dataSource,
    terminate: async () => {
      await app.close()
    },
  }
}
