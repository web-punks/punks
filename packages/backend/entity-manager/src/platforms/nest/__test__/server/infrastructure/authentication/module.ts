import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AuthProviders } from "./providers"
import { EntityModules } from "../../entities/modules"

@Module({
  imports: [SharedModule, ...EntityModules],
  providers: [...AuthProviders],
  exports: [],
})
export class AuthenticationInfrastructureModule {}
