import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppTenantEntity } from "../../database/core/entities/appTenant.entity"
import { AppTenantCreateData, AppTenantUpdateData } from "./appTenant.models"

@WpEntityAdapter("appTenant")
export class AppTenantAdapter
  implements
    IEntityAdapter<AppTenantEntity, AppTenantCreateData, AppTenantUpdateData>
{
  createDataToEntity(data: AppTenantCreateData): DeepPartial<AppTenantEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(data: AppTenantUpdateData): DeepPartial<AppTenantEntity> {
    return {
      ...data,
    }
  }
}
