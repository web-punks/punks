import { Controller, Get } from "@nestjs/common"
import {
  ApiKeyAccess,
  Authenticated,
  Public,
  Roles,
} from "../../../../extensions/authentication/decorators/guards"

@Controller("authTest")
export class AppAuthTestController {
  @Public()
  @Get("getPublicData")
  getPublicData() {
    return {
      data: "foo",
    }
  }

  @ApiKeyAccess({
    strict: true,
  })
  @Get("getApiData")
  getApiData() {
    return {
      data: "foo",
    }
  }

  @ApiKeyAccess()
  @Authenticated()
  @Get("getAuthenticatedData")
  getAuthenticatedData() {
    return {
      data: "foo",
    }
  }

  @Roles("admin")
  @Get("getAdminData")
  getAdminData() {
    return {
      data: "foo",
    }
  }
}
