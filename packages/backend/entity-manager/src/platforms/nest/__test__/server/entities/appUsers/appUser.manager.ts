import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppUserCreateData,
  AppUserCursor,
  AppUserDeleteParameters,
  AppUserEntityId,
  AppUserFacets,
  AppUserSearchParameters,
  AppUserSorting,
  AppUserUpdateData,
} from "./appUser.models"
import { AppUserEntity } from "../../database/core/entities/appUser.entity"

@WpEntityManager("appUser")
export class AppUserEntityManager extends NestEntityManager<
  AppUserEntity,
  AppUserEntityId,
  AppUserCreateData,
  AppUserUpdateData,
  AppUserDeleteParameters,
  AppUserSearchParameters,
  AppUserSorting,
  AppUserCursor,
  AppUserFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUser", registry)
  }
}
