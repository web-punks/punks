import { AppCompanyAppModule } from "./appCompanies/appCompany.module"
import { AppDivisionAppModule } from "./appDivisions/appDivision.module"
import { AppOrganizationAppModule } from "./appOrganizations/appOrganization.module"
import { AppRoleAppModule } from "./appRoles/appRole.module"
import { AppAdminAppModule } from "./appAdmin/appAdmin.module"
import { AppTenantAppModule } from "./appTenants/appTenant.module"
import { AppUserGroupMemberAppModule } from "./appUserGroupMembers/appUserGroupMember.module"
import { AppUserGroupAppModule } from "./appUserGroups/appUserGroup.module"
import { AppUserProfileAppModule } from "./appUserProfiles/appUserProfile.module"
import { AppUserRoleAppModule } from "./appUserRoles/appUserRole.module"
import { AppUserAppModule } from "./appUsers/appUser.module"
import { AppEntityVersionAppModule } from "./appEntityVersions/appEntityVersion.module"
import { AppDirectoryAppModule } from "./appDirectories/appDirectory.module"
import { CrmContactAppModule } from "./crmContacts/crmContact.module"
import { AppApiKeyAppModule } from "./appApiKeys/appApiKey.module"

export const AppModules = [
  AppApiKeyAppModule,
  AppAdminAppModule,
  AppCompanyAppModule,
  AppDirectoryAppModule,
  AppDivisionAppModule,
  AppEntityVersionAppModule,
  AppOrganizationAppModule,
  AppRoleAppModule,
  AppTenantAppModule,
  AppUserGroupMemberAppModule,
  AppUserGroupAppModule,
  AppUserProfileAppModule,
  AppUserRoleAppModule,
  AppUserAppModule,
  CrmContactAppModule,
]
