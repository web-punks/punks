import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../.."
import { AppUserGroupAdapter } from "./appUserGroup.adapter"
import { AppUserGroupAuthMiddleware } from "./appUserGroup.authentication"
import { AppUserGroupEntityManager } from "./appUserGroup.manager"
import { AppUserGroupQueryBuilder } from "./appUserGroup.query"
import { AppUserGroupSerializer } from "./appUserGroup.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppUserGroupAdapter,
    AppUserGroupAuthMiddleware,
    AppUserGroupEntityManager,
    AppUserGroupQueryBuilder,
    AppUserGroupSerializer,
  ],
  exports: [AppUserGroupEntityManager],
})
export class AppUserGroupEntityModule {}
