import {
  FindOptionsOrder,
  FindOptionsRelations,
  FindOptionsWhere,
  In,
  IsNull,
} from "typeorm"
import {
  EntityManagerRegistry,
  IAuthenticationContext,
  NestTypeOrmQueryBuilder,
  QueryBuilderOperation,
  WpEntityQueryBuilder,
} from "../../../../../.."
import {
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitEntityId,
} from "../../database/core/entities/appOrganizationalUnit.entity"
import {
  AppOrganizationalUnitFacets,
  AppOrganizationalUnitSorting,
} from "./appOrganizationalUnit.models"
import { AppOrganizationalUnitSearchParameters } from "./appOrganizationalUnit.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appOrganizationalUnit")
export class AppOrganizationalUnitQueryBuilder extends NestTypeOrmQueryBuilder<
  AppOrganizationalUnitEntity,
  AppOrganizationalUnitEntityId,
  AppOrganizationalUnitSearchParameters,
  AppOrganizationalUnitSorting,
  AppOrganizationalUnitFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganizationalUnit", registry)
  }

  protected getRelationsToLoad(
    request: AppOrganizationalUnitSearchParameters,
    context: IAuthenticationContext<AppUserContext>,
    operation: QueryBuilderOperation
  ): FindOptionsRelations<AppOrganizationalUnitEntity> {
    return {
      type: true,
      parent: true,
    }
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ):
    | FindOptionsWhere<AppOrganizationalUnitEntity>
    | FindOptionsWhere<AppOrganizationalUnitEntity>[] {
    return {
      ...(context?.userContext?.organizationId
        ? {
            organization: {
              id: context?.userContext?.organizationId,
            },
          }
        : {}),
    }
  }

  protected buildSortingClause(
    request: AppOrganizationalUnitSearchParameters
  ): FindOptionsOrder<AppOrganizationalUnitEntity> {
    switch (request.sorting?.fields[0]?.field) {
      case AppOrganizationalUnitSorting.Name:
      default:
        return {
          name: request.sorting?.fields[0]?.direction ?? "asc",
        }
    }
  }

  protected buildWhereClause(
    request: AppOrganizationalUnitSearchParameters
  ):
    | FindOptionsWhere<AppOrganizationalUnitEntity>
    | FindOptionsWhere<AppOrganizationalUnitEntity>[] {
    return {
      ...(request.traverse?.rootOnly
        ? {
            parent: IsNull(),
          }
        : {}),
      ...(request.traverse?.parentIds
        ? {
            parent: {
              id: In(request.traverse.parentIds),
            },
          }
        : {}),
      ...(request.filters?.typeIds
        ? {
            type: {
              id: In(request.filters?.typeIds),
            },
          }
        : {}),
    }
  }

  protected calculateFacets(
    request: AppOrganizationalUnitSearchParameters,
    context?: AppAuthContext
  ): Promise<AppOrganizationalUnitFacets> {
    return Promise.resolve({})
  }

  protected async calculateChildrenMap(
    items: AppOrganizationalUnitEntity[]
  ): Promise<{ [x: string]: { count: number } }> {
    return await this.queryChildrenMap({
      idField: "id",
      parentField: "parent",
      nodeIds: items.map((item) => item.id),
    })
  }
}
