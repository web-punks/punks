import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "../../../../../.."
import { ApiBodyFile } from "../../shared/interceptors/files"
import { CrmContactActions } from "./crmContact.actions"
import {
  CrmContactCreateDto,
  CrmContactDto,
  CrmContactExportRequest,
  CrmContactExportResponse,
  CrmContactSampleDownloadRequest,
  CrmContactSampleDownloadResponse,
  CrmContactSearchRequest,
  CrmContactSearchResponse,
  CrmContactUpdateDto,
  CrmContactVersionsSearchRequest,
  CrmContactVersionsSearchResponse,
} from "./crmContact.dto"

@Controller("v1/crmContact")
export class CrmContactController {
  constructor(private readonly actions: CrmContactActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "crmContactGet",
  })
  @ApiOkResponse({
    type: CrmContactDto,
  })
  async item(@Param("id") id: string): Promise<CrmContactDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "crmContactCreate",
  })
  @ApiOkResponse({
    type: CrmContactDto,
  })
  async create(@Body() data: CrmContactCreateDto): Promise<CrmContactDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: CrmContactDto,
  })
  @ApiOperation({
    operationId: "crmContactUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: CrmContactUpdateDto
  ): Promise<CrmContactDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "crmContactDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "crmContactSearch",
  })
  @ApiOkResponse({
    type: CrmContactSearchResponse,
  })
  async search(
    @Body() request: CrmContactSearchRequest
  ): Promise<CrmContactSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "crmContactImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "crmContactExport",
  })
  @ApiOkResponse({
    type: CrmContactExportResponse,
  })
  async export(
    @Body() request: CrmContactExportRequest
  ): Promise<CrmContactExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "crmContactSampleDownload",
  })
  @ApiOkResponse({
    type: CrmContactSampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: CrmContactSampleDownloadRequest
  ): Promise<CrmContactSampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "crmContactVersions",
  })
  @ApiOkResponse({
    type: CrmContactVersionsSearchResponse,
  })
  async versions(
    @Body() request: CrmContactVersionsSearchRequest
  ): Promise<CrmContactVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
