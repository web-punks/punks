import { Module } from "@nestjs/common"
import { EntityModules } from "./modules"
import { SharedModule } from "../shared/module"

@Module({
  imports: [SharedModule, ...EntityModules],
  exports: [...EntityModules],
})
export class EntitiesModule {}
