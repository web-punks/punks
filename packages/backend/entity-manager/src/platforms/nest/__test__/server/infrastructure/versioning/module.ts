import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { VersioningProviders } from "./providers"
import { CoreDatabaseModule } from "../../database/core"

@Module({
  imports: [SharedModule, CoreDatabaseModule],
  providers: [...VersioningProviders],
  exports: [],
})
export class VersioningInfrastructureModule {}
