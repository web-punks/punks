import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppUserProfileCreateData,
  AppUserProfileCursor,
  AppUserProfileDeleteParameters,
  AppUserProfileEntityId,
  AppUserProfileFacets,
  AppUserProfileSearchParameters,
  AppUserProfileSorting,
  AppUserProfileUpdateData,
} from "./appUserProfile.models"
import { AppUserProfileEntity } from "../../database/core/entities/appUserProfile.entity"

@WpEntityManager("appUserProfile")
export class AppUserProfileEntityManager extends NestEntityManager<
  AppUserProfileEntity,
  AppUserProfileEntityId,
  AppUserProfileCreateData,
  AppUserProfileUpdateData,
  AppUserProfileDeleteParameters,
  AppUserProfileSearchParameters,
  AppUserProfileSorting,
  AppUserProfileCursor,
  AppUserProfileFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUserProfile", registry)
  }
}
