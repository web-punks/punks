import {
  DeepPartial,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppUserProfileEntity } from "../../database/core/entities/appUserProfile.entity"
import { IEntitiesDeleteParameters } from "../../../../../../abstractions/commands"

export type AppUserProfileEntityId = string

export type AppUserProfileCreateData = DeepPartial<
  Omit<AppUserProfileEntity, "id">
>
export type AppUserProfileUpdateData = DeepPartial<
  Omit<AppUserProfileEntity, "id">
>

export class AppUserProfileFacets {}

export class AppUserProfileSearchFilters {}

export enum AppUserProfileSorting {
  Name = "Name",
}

export type AppUserProfileCursor = number

export class AppUserProfileSearchSortingField
  implements ISearchSortingField<AppUserProfileSorting>
{
  @ApiProperty({ enum: AppUserProfileSorting })
  field: AppUserProfileSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppUserProfileQuerySorting
  implements ISearchSorting<AppUserProfileSorting>
{
  @ApiProperty({ required: false, type: [AppUserProfileSearchSortingField] })
  fields: AppUserProfileSearchSortingField[]
}

export class AppUserProfileQueryPaging
  implements ISearchRequestPaging<AppUserProfileCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppUserProfileCursor

  @ApiProperty()
  pageSize: number
}

export class AppUserProfileSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppUserProfileSearchParameters
  implements
    IEntitySearchParameters<
      AppUserProfileEntity,
      AppUserProfileSorting,
      AppUserProfileCursor
    >
{
  @ApiProperty({ required: false })
  filters?: AppUserProfileSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserProfileQuerySorting

  @ApiProperty({ required: false })
  paging?: AppUserProfileQueryPaging

  @ApiProperty({ required: false })
  options?: AppUserProfileSearchOptions
}

export class AppUserProfileSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppUserProfileCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserProfileCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserProfileCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserProfileCursor
}

export class AppUserProfileSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppUserProfileEntity,
      AppUserProfileSearchParameters,
      TResult,
      AppUserProfileSorting,
      AppUserProfileCursor,
      AppUserProfileFacets
    >
{
  @ApiProperty()
  request: AppUserProfileSearchParameters

  @ApiProperty()
  facets?: AppUserProfileFacets

  @ApiProperty()
  paging?: AppUserProfileSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppUserProfileDeleteParameters
  implements IEntitiesDeleteParameters<AppUserProfileSorting>
{
  @ApiProperty({ required: false })
  filters?: AppUserProfileSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserProfileQuerySorting
}

export type AppUserProfileSheetItem = AppUserProfileEntity
