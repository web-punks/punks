import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { AppUserGroupActions } from "./appUserGroup.actions"
import {
  AppUserGroupCreateDto,
  AppUserGroupDto,
  AppUserGroupSearchRequest,
  AppUserGroupSearchResponse,
  AppUserGroupUpdateDto,
} from "./appUserGroup.dto"

@Controller("v1/appUserGroup")
export class AppUserGroupController {
  constructor(private readonly actions: AppUserGroupActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appUserGroupGet",
  })
  @ApiOkResponse({
    type: AppUserGroupDto,
  })
  async item(@Param("id") id: string): Promise<AppUserGroupDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appUserGroupCreate",
  })
  @ApiOkResponse({
    type: AppUserGroupDto,
  })
  async create(@Body() data: AppUserGroupCreateDto): Promise<AppUserGroupDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppUserGroupDto,
  })
  @ApiOperation({
    operationId: "appUserGroupUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppUserGroupUpdateDto
  ): Promise<AppUserGroupDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appUserGroupDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appUserGroupSearch",
  })
  @ApiOkResponse({
    type: AppUserGroupSearchResponse,
  })
  async search(
    @Body() request: AppUserGroupSearchRequest
  ): Promise<AppUserGroupSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }
}
