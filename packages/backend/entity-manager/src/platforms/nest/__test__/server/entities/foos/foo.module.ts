import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { FooAdapter } from "./foo.adapter"
import { FooAuthMiddleware } from "./foo.authentication"
import { FooEntityManager } from "./foo.manager"
import { FooQueryBuilder } from "./foo.query"
import { FooSerializer } from "./foo.serializer"
import { FooSeeder } from "./foo.seeder"

@Module({
  imports: [EntityManagerModule],
  providers: [
    FooAdapter,
    FooAuthMiddleware,
    FooEntityManager,
    FooQueryBuilder,
    FooSerializer,
    FooSeeder,
  ],
  exports: [FooEntityManager],
})
export class FooEntityModule {}
