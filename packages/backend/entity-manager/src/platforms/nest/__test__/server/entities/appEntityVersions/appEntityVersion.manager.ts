import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppEntityVersionCreateData,
  AppEntityVersionCursor,
  AppEntityVersionEntityId,
  AppEntityVersionFacets,
  AppEntityVersionSorting,
  AppEntityVersionUpdateData,
} from "./appEntityVersion.models"
import {
  AppEntityVersionSearchParameters,
  AppEntityVersionDeleteParameters,
} from "./appEntityVersion.types"
import { AppEntityVersionEntity } from "../../database/core/entities/appEntityVersion.entity"

@WpEntityManager("appEntityVersion")
export class AppEntityVersionEntityManager extends NestEntityManager<
  AppEntityVersionEntity,
  AppEntityVersionEntityId,
  AppEntityVersionCreateData,
  AppEntityVersionUpdateData,
  AppEntityVersionDeleteParameters,
  AppEntityVersionSearchParameters,
  AppEntityVersionSorting,
  AppEntityVersionCursor,
  AppEntityVersionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appEntityVersion", registry)
  }
}
