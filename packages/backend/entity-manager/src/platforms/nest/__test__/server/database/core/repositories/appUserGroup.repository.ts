import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { NestTypeOrmRepository, WpEntityRepository } from "../../../../.."
import { AppUserGroupEntity } from "../entities/appUserGroup.entity"
import { AppUserGroupEntityId } from "../../../entities/appUserGroups/appUserGroup.models"

@WpEntityRepository("appUserGroup")
export class AppUserGroupRepository extends NestTypeOrmRepository<
  AppUserGroupEntity,
  AppUserGroupEntityId
> {
  constructor(
    @InjectRepository(AppUserGroupEntity, "core")
    repository: Repository<AppUserGroupEntity>
  ) {
    super(repository)
  }
}
