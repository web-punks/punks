import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "../../../../../.."
import { AppOrganizationalUnitEntity } from "../../database/core/entities/appOrganizationalUnit.entity"

export type AppOrganizationalUnitCreateData = DeepPartial<
  Omit<AppOrganizationalUnitEntity, "id">
>
export type AppOrganizationalUnitUpdateData = DeepPartial<
  Omit<AppOrganizationalUnitEntity, "id">
>

export enum AppOrganizationalUnitSorting {
  Name = "Name",
}

export type AppOrganizationalUnitCursor = number

export class AppOrganizationalUnitSearchFilters {
  @ApiProperty({ type: [String], required: false })
  typeIds?: string[]
}

export class AppOrganizationalUnitFacets {}

export type AppOrganizationalUnitSheetItem = AppOrganizationalUnitEntity
