import { IEntityConverter, WpEntityConverter } from "../../../../../.."
import {
  AppUserRoleCreateDto,
  AppUserRoleDto,
  AppUserRoleListItemDto,
  AppUserRoleUpdateDto,
} from "./appUserRole.dto"
import { AppUserRoleEntity } from "../../database/core/entities/appUserRole.entity"

@WpEntityConverter("appUserRole")
export class AppUserRoleConverter
  implements
    IEntityConverter<
      AppUserRoleEntity,
      AppUserRoleDto,
      AppUserRoleListItemDto,
      AppUserRoleCreateDto,
      AppUserRoleUpdateDto
    >
{
  toListItemDto(entity: AppUserRoleEntity): AppUserRoleListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppUserRoleEntity): AppUserRoleDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppUserRoleCreateDto): Partial<AppUserRoleEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppUserRoleUpdateDto): Partial<AppUserRoleEntity> {
    return {
      ...input,
    }
  }
}
