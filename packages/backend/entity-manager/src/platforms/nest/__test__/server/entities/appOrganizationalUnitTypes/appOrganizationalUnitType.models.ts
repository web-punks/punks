import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "../../../../../.."
import { AppOrganizationalUnitTypeEntity } from "../../database/core/entities/appOrganizationalUnitType.entity"

export type AppOrganizationalUnitTypeCreateData = DeepPartial<
  Omit<AppOrganizationalUnitTypeEntity, "id">
>
export type AppOrganizationalUnitTypeUpdateData = DeepPartial<
  Omit<AppOrganizationalUnitTypeEntity, "id">
>

export enum AppOrganizationalUnitTypeSorting {
  Name = "Name",
}

export type AppOrganizationalUnitTypeCursor = number

export class AppOrganizationalUnitTypeSearchFilters {}

export class AppOrganizationalUnitTypeFacets {}

export type AppOrganizationalUnitTypeSheetItem = AppOrganizationalUnitTypeEntity
