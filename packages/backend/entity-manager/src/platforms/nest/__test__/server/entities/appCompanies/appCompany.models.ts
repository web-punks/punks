import {
  DeepPartial,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppCompanyEntity } from "../../database/core/entities/appCompany.entity"
import { IEntitiesDeleteParameters } from "../../../../../../abstractions/commands"

export type AppCompanyEntityId = string

export type AppCompanyCreateData = DeepPartial<Omit<AppCompanyEntity, "id">>
export type AppCompanyUpdateData = DeepPartial<Omit<AppCompanyEntity, "id">>

export class AppCompanyFacets {}

export class AppCompanySearchFilters {}

export enum AppCompanySorting {
  Name = "Name",
}

export type AppCompanyCursor = number

export class AppCompanySearchSortingField
  implements ISearchSortingField<AppCompanySorting>
{
  @ApiProperty({ enum: AppCompanySorting })
  field: AppCompanySorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppCompanyQuerySorting
  implements ISearchSorting<AppCompanySorting>
{
  @ApiProperty({ required: false, type: [AppCompanySearchSortingField] })
  fields: AppCompanySearchSortingField[]
}

export class AppCompanyQueryPaging
  implements ISearchRequestPaging<AppCompanyCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppCompanyCursor

  @ApiProperty()
  pageSize: number
}

export class AppCompanySearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppCompanySearchParameters
  implements
    IEntitySearchParameters<
      AppCompanyEntity,
      AppCompanySorting,
      AppCompanyCursor
    >
{
  @ApiProperty({ required: false })
  filters?: AppCompanySearchFilters

  @ApiProperty({ required: false })
  sorting?: AppCompanyQuerySorting

  @ApiProperty({ required: false })
  paging?: AppCompanyQueryPaging

  @ApiProperty({ required: false })
  options?: AppCompanySearchOptions
}

export class AppCompanySearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppCompanyCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppCompanyCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppCompanyCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppCompanyCursor
}

export class AppCompanySearchResults<TEntity, TResult>
  implements
    IEntitiesSearchResults<
      TEntity,
      AppCompanySearchParameters,
      TResult,
      AppCompanySorting,
      AppCompanyCursor,
      AppCompanyFacets
    >
{
  @ApiProperty()
  request: AppCompanySearchParameters

  @ApiProperty()
  facets?: AppCompanyFacets

  @ApiProperty()
  paging?: AppCompanySearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppCompanyDeleteParameters
  implements IEntitiesDeleteParameters<AppCompanySorting>
{
  @ApiProperty({ required: false })
  filters?: AppCompanySearchFilters

  @ApiProperty({ required: false })
  sorting?: AppCompanyQuerySorting
}

export type AppCompanySheetItem = AppCompanyEntity
