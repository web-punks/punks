import { AppApiKeyRepository } from "./repositories/appApiKey.repository"
import { AppCompanyRepository } from "./repositories/appCompany.repository"
import { AppDirectoryRepository } from "./repositories/appDirectory.repository"
import { AppDivisionRepository } from "./repositories/appDivision.repository"
import { AppEmailLogRepository } from "./repositories/appEmailLog.repository"
import { AppEntityVersionRepository } from "./repositories/appEntityVersion.repository"
import { AppFileReferenceRepository } from "./repositories/appFileReference.repository"
import { AppOperationLockItemRepository } from "./repositories/appOperationLock.repository"
import { AppOrganizationRepository } from "./repositories/appOrganization.repository"
import { AppOrganizationalUnitRepository } from "./repositories/appOrganizationalUnit.repository"
import { AppOrganizationalUnitTypeRepository } from "./repositories/appOrganizationalUnitType.repository"
import { AppOrganizationalUnitTypeChildRepository } from "./repositories/appOrganizationalUnitTypeChild.repository"
import { AppPermissionRepository } from "./repositories/appPermission.repository"
import { AppRoleRepository } from "./repositories/appRole.repository"
import { AppRoleOrganizationalUnitTypeRepository } from "./repositories/appRoleOrganizationalUnitType.repository"
import { AppRolePermissionRepository } from "./repositories/appRolePermission.repository"
import { AppTenantRepository } from "./repositories/appTenant.repository"
import { AppUserRepository } from "./repositories/appUser.repository"
import { AppUserGroupRepository } from "./repositories/appUserGroup.repository"
import { AppUserGroupMemberRepository } from "./repositories/appUserGroupMember.repository"
import { AppUserProfileRepository } from "./repositories/appUserProfile.repository"
import { AppUserRoleRepository } from "./repositories/appUserRole.repository"
import { CrmContactRepository } from "./repositories/crmContact.repository"
import { FooRepository } from "./repositories/foo.repository"

export const CoreDatabaseRepositories = [
  AppApiKeyRepository,
  AppCompanyRepository,
  AppDivisionRepository,
  AppDirectoryRepository,
  AppEmailLogRepository,
  AppEntityVersionRepository,
  AppFileReferenceRepository,
  AppOperationLockItemRepository,
  AppOrganizationRepository,
  AppOrganizationalUnitRepository,
  AppOrganizationalUnitTypeRepository,
  AppOrganizationalUnitTypeChildRepository,
  AppRoleRepository,
  AppRoleOrganizationalUnitTypeRepository,
  AppRolePermissionRepository,
  AppPermissionRepository,
  AppTenantRepository,
  AppUserRepository,
  AppUserGroupRepository,
  AppUserGroupMemberRepository,
  AppUserProfileRepository,
  AppUserRoleRepository,
  CrmContactRepository,
  FooRepository,
]
