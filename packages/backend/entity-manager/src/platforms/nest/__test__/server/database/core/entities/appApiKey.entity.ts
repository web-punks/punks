import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Index,
  Column,
} from "typeorm"

import { AppOrganizationEntity } from "./appOrganization.entity"
import { WpEntity } from "../../../../../decorators"

export type AppApiKeyEntityId = string

@Entity("appApiKeys")
@WpEntity("appApiKey")
export class AppApiKeyEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppApiKeyEntityId

  @Index()
  @Column({ type: "varchar", length: 255, unique: true })
  key: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  name: string

  @Column({ type: "boolean", default: false })
  disabled: boolean

  @ManyToOne(() => AppOrganizationEntity, {
    nullable: true,
  })
  organization?: AppOrganizationEntity

  @Column({ nullable: true })
  organizationId?: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
