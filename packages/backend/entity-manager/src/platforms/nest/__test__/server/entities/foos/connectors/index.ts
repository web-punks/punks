import { FooCustomConnector } from "./custom/connector"
import { FooConnectorMapper as FooCustomConnectorMapper } from "./custom/mapper"

export const FooConnectors = [FooCustomConnector, FooCustomConnectorMapper]
