import { DeepPartial } from "../../../../../.."
import { AppEmailLogEntity } from "../../database/core/entities/appEmailLog.entity"

export type AppEmailLogCreateData = DeepPartial<Omit<AppEmailLogEntity, "id">>
export type AppEmailLogUpdateData = DeepPartial<Omit<AppEmailLogEntity, "id">>

export enum AppEmailLogSorting {
  Timestamp = "Timestamp",
}

export type AppEmailLogCursor = number

export class AppEmailLogSearchFilters {}

export class AppEmailLogFacets {}

export type AppEmailLogSheetItem = AppEmailLogEntity
