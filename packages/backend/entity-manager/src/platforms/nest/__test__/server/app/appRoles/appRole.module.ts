import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppRoleActions } from "./appRole.actions"
import { AppRoleController } from "./appRole.controller"
import { AppRoleConverter } from "./appRole.converter"
import { AppRoleEntityModule } from "../../entities/appRoles/appRole.module"

@Module({
  imports: [SharedModule, AppRoleEntityModule],
  providers: [AppRoleActions, AppRoleConverter],
  controllers: [AppRoleController],
})
export class AppRoleAppModule {}
