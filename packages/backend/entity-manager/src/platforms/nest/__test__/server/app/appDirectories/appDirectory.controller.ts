import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "../../../../../.."
import { ApiBodyFile } from "../../shared/interceptors/files"
import { AppDirectoryActions } from "./appDirectory.actions"
import {
  AppDirectoryCreateDto,
  AppDirectoryDto,
  AppDirectoryExportRequest,
  AppDirectoryExportResponse,
  AppDirectorySampleDownloadRequest,
  AppDirectorySampleDownloadResponse,
  AppDirectorySearchRequest,
  AppDirectorySearchResponse,
  AppDirectoryUpdateDto,
  AppDirectoryVersionsSearchRequest,
  AppDirectoryVersionsSearchResponse,
} from "./appDirectory.dto"

@Controller("v1/appDirectory")
export class AppDirectoryController {
  constructor(private readonly actions: AppDirectoryActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appDirectoryGet",
  })
  @ApiOkResponse({
    type: AppDirectoryDto,
  })
  async item(@Param("id") id: string): Promise<AppDirectoryDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appDirectoryCreate",
  })
  @ApiOkResponse({
    type: AppDirectoryDto,
  })
  async create(@Body() data: AppDirectoryCreateDto): Promise<AppDirectoryDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppDirectoryDto,
  })
  @ApiOperation({
    operationId: "appDirectoryUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppDirectoryUpdateDto
  ): Promise<AppDirectoryDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appDirectoryDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appDirectorySearch",
  })
  @ApiOkResponse({
    type: AppDirectorySearchResponse,
  })
  async search(
    @Body() request: AppDirectorySearchRequest
  ): Promise<AppDirectorySearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appDirectoryImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appDirectoryExport",
  })
  @ApiOkResponse({
    type: AppDirectoryExportResponse,
  })
  async export(
    @Body() request: AppDirectoryExportRequest
  ): Promise<AppDirectoryExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appDirectorySampleDownload",
  })
  @ApiOkResponse({
    type: AppDirectorySampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppDirectorySampleDownloadRequest
  ): Promise<AppDirectorySampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appDirectoryVersions",
  })
  @ApiOkResponse({
    type: AppDirectoryVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppDirectoryVersionsSearchRequest
  ): Promise<AppDirectoryVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
