import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { NestTypeOrmRepository, WpEntityRepository } from "../../../../.."
import { AppUserEntity } from "../entities/appUser.entity"
import { AppUserEntityId } from "../../../entities/appUsers/appUser.models"

@WpEntityRepository("appUser")
export class AppUserRepository extends NestTypeOrmRepository<
  AppUserEntity,
  AppUserEntityId
> {
  constructor(
    @InjectRepository(AppUserEntity, "core")
    repository: Repository<AppUserEntity>
  ) {
    super(repository)
  }
}
