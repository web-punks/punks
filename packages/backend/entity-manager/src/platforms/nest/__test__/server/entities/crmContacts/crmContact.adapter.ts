import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { CrmContactEntity } from "../../database/core/entities/crmContact.entity"
import { CrmContactCreateData, CrmContactUpdateData } from "./crmContact.models"

@WpEntityAdapter("crmContact")
export class CrmContactAdapter
  implements
    IEntityAdapter<
      CrmContactEntity,
      CrmContactCreateData,
      CrmContactUpdateData
    >
{
  createDataToEntity(
    data: CrmContactCreateData
  ): DeepPartial<CrmContactEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: CrmContactUpdateData
  ): DeepPartial<CrmContactEntity> {
    return {
      ...data,
    }
  }
}
