import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { AppPermissionAdapter } from "./appPermission.adapter"
import { AppPermissionAuthMiddleware } from "./appPermission.authentication"
import { AppPermissionEntityManager } from "./appPermission.manager"
import { AppPermissionQueryBuilder } from "./appPermission.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppPermissionAdapter,
    AppPermissionAuthMiddleware,
    AppPermissionEntityManager,
    AppPermissionQueryBuilder,
  ],
  exports: [AppPermissionEntityManager],
})
export class AppPermissionEntityModule {}
