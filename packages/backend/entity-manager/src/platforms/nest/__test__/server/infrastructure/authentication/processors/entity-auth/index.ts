import {
  IMultiOrganizationEntity,
  NestEntityAuthorizationMiddleware,
} from "../../../../../.."
import { IAuthorizationResult } from "../../../../../../../.."
import { AppAuthContext, AppUserContext } from "../../types"

export abstract class AppEntityAuthorizationMiddlewareBase<
  TEntity
> extends NestEntityAuthorizationMiddleware<
  TEntity,
  AppAuthContext,
  AppUserContext
> {
  // default
  protected async defaultCanSearch(
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  protected async defaultCanRead(
    entity: Partial<TEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  protected async defaultCanCreate(
    entity: Partial<TEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  protected async defaultCanUpdate(
    entity: Partial<TEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  protected async defaultCanDelete(
    entity: Partial<TEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  protected async defaultCanDeleteItems(
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  // organization

  protected async organizationEntityCanSearch(
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  protected async organizationEntityCanRead(
    entity: IMultiOrganizationEntity,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: this.isOrganizationMatching(context, entity),
    }
  }

  protected async organizationEntityCanCreate(
    entity: IMultiOrganizationEntity,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: this.isOrganizationMatching(context, entity),
    }
  }

  protected async organizationEntityCanUpdate(
    entity: IMultiOrganizationEntity,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: this.isOrganizationMatching(context, entity),
    }
  }

  protected async organizationEntityCanDelete(
    entity: IMultiOrganizationEntity,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: this.isOrganizationMatching(context, entity),
    }
  }

  protected async organizationEntityCanDeleteItems(
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  private isOrganizationMatching(
    context: AppAuthContext,
    entity: IMultiOrganizationEntity
  ): boolean {
    return (
      !!context.userContext &&
      (!context.userContext?.organizationId ||
        entity.organization.id === context.userContext.organizationId)
    )
  }
}
