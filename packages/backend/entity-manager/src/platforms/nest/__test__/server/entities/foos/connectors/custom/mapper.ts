import { IEntityMapper } from "../../../../../../../../abstractions"
import { WpEntityConnectorMapper } from "../../../../../../decorators"
import { FooEntity } from "../../../../database/core/entities/foo.entity"
import { FooMappedModel } from "./model"

@WpEntityConnectorMapper("foo", {
  connectorName: "custom",
})
export class FooConnectorMapper
  implements IEntityMapper<FooEntity, FooMappedModel>
{
  async mapEntity(entity: FooEntity): Promise<FooMappedModel> {
    return {
      id: entity.id,
      displayName: entity.name,
    }
  }
}
