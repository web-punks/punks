import { ApiProperty } from "@nestjs/swagger"
import {
  AppUserGroupMemberSearchParameters,
  AppUserGroupMemberSearchResults,
} from "../../entities/appUserGroupMembers/appUserGroupMember.models"

export class AppUserGroupMemberDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserGroupMemberListItemDto {
  @ApiProperty()
  id: string
}

export class AppUserGroupMemberCreateDto {}

export class AppUserGroupMemberUpdateDto {
  @ApiProperty()
  id: string
}

export class AppUserGroupMemberSearchRequest {
  @ApiProperty()
  params: AppUserGroupMemberSearchParameters
}

export class AppUserGroupMemberSearchResponse extends AppUserGroupMemberSearchResults<AppUserGroupMemberListItemDto> {
  @ApiProperty({ type: [AppUserGroupMemberListItemDto] })
  items: AppUserGroupMemberListItemDto[]
}
