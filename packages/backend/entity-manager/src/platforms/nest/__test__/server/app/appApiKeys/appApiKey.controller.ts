import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "../../../../../.."
import { AppApiKeyActions } from "./appApiKey.actions"
import {
  AppApiKeyDto,
  AppApiKeyCreateDto,
  AppApiKeyUpdateDto,
} from "./appApiKey.dto"
import {
  AppApiKeyExportRequest,
  AppApiKeyExportResponse,
  AppApiKeySampleDownloadRequest,
  AppApiKeySampleDownloadResponse,
  AppApiKeySearchRequest,
  AppApiKeySearchResponse,
  AppApiKeyVersionsSearchRequest,
  AppApiKeyVersionsSearchResponse,
} from "./appApiKey.types"
import { ApiBodyFile } from "../../shared/interceptors/files"
import { OrganizationManagers } from "../../infrastructure/authentication/guards"

@OrganizationManagers()
@Controller("v1/appApiKey")
export class AppApiKeyController {
  constructor(private readonly actions: AppApiKeyActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appApiKeyGet",
  })
  @ApiOkResponse({
    type: AppApiKeyDto,
  })
  async item(@Param("id") id: string): Promise<AppApiKeyDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appApiKeyCreate",
  })
  @ApiOkResponse({
    type: AppApiKeyDto,
  })
  async create(@Body() data: AppApiKeyCreateDto): Promise<AppApiKeyDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppApiKeyDto,
  })
  @ApiOperation({
    operationId: "appApiKeyUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppApiKeyUpdateDto
  ): Promise<AppApiKeyDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appApiKeyDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appApiKeySearch",
  })
  @ApiOkResponse({
    type: AppApiKeySearchResponse,
  })
  async search(
    @Body() request: AppApiKeySearchRequest
  ): Promise<AppApiKeySearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("import")
  @ApiOperation({
    operationId: "appApiKeyImport",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "appApiKeyExport",
  })
  @ApiOkResponse({
    type: AppApiKeyExportResponse,
  })
  async export(
    @Body() request: AppApiKeyExportRequest
  ): Promise<AppApiKeyExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "appApiKeySampleDownload",
  })
  @ApiOkResponse({
    type: AppApiKeySampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: AppApiKeySampleDownloadRequest
  ): Promise<AppApiKeySampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "appApiKeyVersions",
  })
  @ApiOkResponse({
    type: AppApiKeyVersionsSearchResponse,
  })
  async versions(
    @Body() request: AppApiKeyVersionsSearchRequest
  ): Promise<AppApiKeyVersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
