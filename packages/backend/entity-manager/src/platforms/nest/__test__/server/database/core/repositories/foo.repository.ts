import { Repository } from "typeorm"
import { WpEntityRepository } from "../../../../../decorators"
import { FooEntity } from "../entities/foo.entity"
import { NestTypeOrmRepository } from "../../../../../integrations"
import { InjectRepository } from "@nestjs/typeorm"
import { FooEntityId } from "../../../entities/foos/foo.models"

@WpEntityRepository("foo")
export class FooRepository extends NestTypeOrmRepository<
  FooEntity,
  FooEntityId
> {
  constructor(
    @InjectRepository(FooEntity, "core")
    repository: Repository<FooEntity>
  ) {
    super(repository)
  }
}
