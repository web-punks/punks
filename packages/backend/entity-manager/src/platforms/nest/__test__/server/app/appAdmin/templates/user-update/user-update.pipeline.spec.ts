import { AppAdminAppModule } from "../../appAdmin.module"
import { UserUpdateTemplate } from "."

import { INestApplication } from "@nestjs/common"
import { DataSource } from "typeorm"
import { createTestServer } from "../../../.."
import { AuthenticationService } from "../../../../../../extensions"
import {
  createAuthenticationContext,
  createUser,
} from "../../../../testing/authentication"
import { AppRoles } from "../../../../infrastructure/authentication/roles"

describe("Test User Update Pipeline", () => {
  let app: INestApplication<any>
  let dataSource: DataSource
  let template: UserUpdateTemplate
  let auth: AuthenticationService

  beforeEach(async () => {
    const server = await createTestServer({
      extraModules: [AppAdminAppModule],
      useAuthentication: true,
    })
    app = server.app
    dataSource = server.dataSource
    auth = app.get(AuthenticationService)
    template = app.get(UserUpdateTemplate)
  })

  it("a tenant admin should update a user", async () => {
    const context = await createAuthenticationContext(app, dataSource)
    const user = await createUser(app, context, {
      roleUid: "company-user",
    })
    const result = await template.execute({
      input: {
        userId: user.id,
        data: {
          email: "newemail@prova.it",
          userName: "newemail@prova.it",
          fistName: "newname",
          lastName: "newlastname",
          roleUid: "company-manager",
        },
      },
      context: {
        isAnonymous: false,
        isAuthenticated: true,
        userRoles: [AppRoles.TenantAdmin],
      },
    })

    expect(result.type).toBe("success")

    const current = await auth.usersService.getById(user.id)

    expect(current).toMatchObject({})
  })
})
