import { ApiProperty } from "@nestjs/swagger"
import { EntitySerializationFormat } from "../../../../../../abstractions"
import {
  FooSearchParameters,
  FooSearchResults,
  FooVersionsSearchParameters,
  FooVersionsSearchResults,
} from "../../entities/foos/foo.types"
import { EntityParseResult } from "../../../../models/parsing"

export class FooProfile {
  @ApiProperty()
  name: string

  @ApiProperty({ required: false })
  age?: number
}

export class FooDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  profile: FooProfile

  @ApiProperty()
  updatedOn: Date
}

export class FooListItemDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  profile: FooProfile
}

export class FooCreateDto {
  @ApiProperty()
  profile: FooProfile
}

export class FooUpdateDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  profile: FooProfile
}

export class FooSearchRequest {
  @ApiProperty()
  params: FooSearchParameters
}

export class FooSearchResponse extends FooSearchResults<FooListItemDto> {
  @ApiProperty({ type: [FooListItemDto] })
  items: FooListItemDto[]
}

export class FooImportRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class FooParseRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class FooParseResponse {
  @ApiProperty({ type: [EntityParseResult] })
  entries: EntityParseResult[]
}

export class EntitiesExportOptions {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class FooExportRequest {
  @ApiProperty()
  options: EntitiesExportOptions

  @ApiProperty({ required: false })
  filter?: FooSearchParameters
}

export class FooExportResponse {
  @ApiProperty()
  downloadUrl: string
}

export class FooSampleDownloadRequest {
  @ApiProperty({ enum: EntitySerializationFormat })
  format: EntitySerializationFormat
}

export class FooSampleDownloadResponse {
  @ApiProperty()
  downloadUrl: string
}

export class FooVersionsSearchRequest {
  @ApiProperty()
  params: FooVersionsSearchParameters
}

export class FooVersionsSearchResponse extends FooVersionsSearchResults<FooListItemDto> {
  @ApiProperty({ type: [FooListItemDto] })
  items: FooListItemDto[]
}
