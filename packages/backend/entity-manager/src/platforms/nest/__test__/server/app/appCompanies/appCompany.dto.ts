import { ApiProperty } from "@nestjs/swagger"
import {
  AppCompanySearchParameters,
  AppCompanySearchResults,
} from "../../entities/appCompanies/appCompany.models"
import { AppCompanyEntity } from "../../database/core/entities/appCompany.entity"

export class AppCompanyDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppCompanyListItemDto {
  @ApiProperty()
  id: string
}

export class AppCompanyCreateDto {}

export class AppCompanyUpdateDto {
  @ApiProperty()
  id: string
}

export class AppCompanySearchRequest {
  @ApiProperty()
  params: AppCompanySearchParameters
}

export class AppCompanySearchResponse extends AppCompanySearchResults<
  AppCompanyEntity,
  AppCompanyListItemDto
> {
  @ApiProperty({ type: [AppCompanyListItemDto] })
  items: AppCompanyListItemDto[]
}
