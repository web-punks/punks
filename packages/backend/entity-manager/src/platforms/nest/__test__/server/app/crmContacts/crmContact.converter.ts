import {
  DeepPartial,
  IEntityConverter,
  WpEntityConverter,
} from "../../../../../.."
import {
  CrmContactCreateDto,
  CrmContactDto,
  CrmContactListItemDto,
  CrmContactUpdateDto,
} from "./crmContact.dto"
import { CrmContactEntity } from "../../database/core/entities/crmContact.entity"

@WpEntityConverter("crmContact")
export class CrmContactConverter
  implements
    IEntityConverter<
      CrmContactEntity,
      CrmContactDto,
      CrmContactListItemDto,
      CrmContactCreateDto,
      CrmContactUpdateDto
    >
{
  toListItemDto(entity: CrmContactEntity): CrmContactListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: CrmContactEntity): CrmContactDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: CrmContactCreateDto): DeepPartial<CrmContactEntity> {
    const { organizationId, ...params } = input
    return {
      ...params,
      organization: organizationId
        ? {
            id: organizationId,
          }
        : undefined,
    }
  }

  updateDtoToEntity(input: CrmContactUpdateDto): DeepPartial<CrmContactEntity> {
    const { organizationId, ...params } = input
    return {
      ...params,
      organization: organizationId
        ? {
            id: organizationId,
          }
        : undefined,
    }
  }
}
