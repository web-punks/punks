import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppRolePermissionCreateData,
  AppRolePermissionCursor,
  AppRolePermissionFacets,
  AppRolePermissionSorting,
  AppRolePermissionUpdateData,
} from "./appRolePermission.models"
import {
  AppRolePermissionSearchParameters,
  AppRolePermissionDeleteParameters,
} from "./appRolePermission.types"
import {
  AppRolePermissionEntity,
  AppRolePermissionEntityId,
} from "../../database/core/entities/appRolePermission.entity"

@WpEntityManager("appRolePermission")
export class AppRolePermissionEntityManager extends NestEntityManager<
  AppRolePermissionEntity,
  AppRolePermissionEntityId,
  AppRolePermissionCreateData,
  AppRolePermissionUpdateData,
  AppRolePermissionDeleteParameters,
  AppRolePermissionSearchParameters,
  AppRolePermissionSorting,
  AppRolePermissionCursor,
  AppRolePermissionFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRolePermission", registry)
  }
}
