import { Body, Controller, Post } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { AppAuthActions } from "./appAuth.actions"
import {
  UserEmailVerifyCompleteRequest,
  UserEmailVerifyRequest,
  UserLoginRequest,
  UserLoginResponse,
  UserPasswordResetCompleteRequest,
  UserPasswordResetRequest,
  UserRegisterRequest,
  UserRegisterResponse,
} from "./appAuth.dto"
import { Public } from "../../../../extensions"

@Public()
@Controller("v1/appAuth")
export class AppAuthController {
  constructor(private readonly actions: AppAuthActions) {}

  @Post("login")
  @ApiOperation({
    operationId: "userLogin",
  })
  @ApiOkResponse({
    type: UserLoginResponse,
  })
  async login(@Body() data: UserLoginRequest): Promise<UserLoginResponse> {
    return await this.actions.login(data)
  }

  @Post("register")
  @ApiOperation({
    operationId: "userRegister",
  })
  @ApiOkResponse({
    type: UserRegisterResponse,
  })
  async register(
    @Body() data: UserRegisterRequest
  ): Promise<UserRegisterResponse> {
    return await this.actions.register(data)
  }

  @Post("verify")
  @ApiOperation({
    operationId: "userVerify",
  })
  async verify(@Body() data: UserEmailVerifyRequest): Promise<void> {
    await this.actions.userVerifyRequest(data)
  }

  @Post("verifyComplete")
  @ApiOperation({
    operationId: "userVerifyComplete",
  })
  async verifyComplete(
    @Body() data: UserEmailVerifyCompleteRequest
  ): Promise<void> {
    await this.actions.userVerifyComplete(data)
  }

  @Post("passwordReset")
  @ApiOperation({
    operationId: "userPasswordReset",
  })
  async passwordReset(@Body() data: UserPasswordResetRequest): Promise<void> {
    await this.actions.passwordReset(data)
  }

  @Post("passwordResetComplete")
  @ApiOperation({
    operationId: "userPasswordResetComplete",
  })
  async passwordResetComplete(
    @Body() data: UserPasswordResetCompleteRequest
  ): Promise<void> {
    await this.actions.passwordResetComplete(data)
  }
}
