import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { AppOrganizationalUnitAdapter } from "./appOrganizationalUnit.adapter"
import { AppOrganizationalUnitAuthMiddleware } from "./appOrganizationalUnit.authentication"
import { AppOrganizationalUnitEntityManager } from "./appOrganizationalUnit.manager"
import { AppOrganizationalUnitQueryBuilder } from "./appOrganizationalUnit.query"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppOrganizationalUnitAdapter,
    AppOrganizationalUnitAuthMiddleware,
    AppOrganizationalUnitEntityManager,
    AppOrganizationalUnitQueryBuilder,
  ],
  exports: [AppOrganizationalUnitEntityManager],
})
export class AppOrganizationalUnitEntityModule {}
