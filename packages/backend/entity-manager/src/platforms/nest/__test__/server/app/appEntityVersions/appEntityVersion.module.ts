import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppEntityVersionActions } from "./appEntityVersion.actions"
import { AppEntityVersionController } from "./appEntityVersion.controller"
import { AppEntityVersionConverter } from "./appEntityVersion.converter"
import { AppEntityVersionEntityModule } from "../../entities/appEntityVersions/appEntityVersion.module"

@Module({
  imports: [SharedModule, AppEntityVersionEntityModule],
  providers: [AppEntityVersionActions, AppEntityVersionConverter],
  controllers: [AppEntityVersionController],
})
export class AppEntityVersionAppModule {}
