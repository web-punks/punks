import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "../../../../.."
import { AppRoleEntity } from "./appRole.entity"
import { AppUserEntity } from "./appUser.entity"

@Unique("userRole", ["user", "role"])
@Entity("appUserRoles")
@WpEntity("appUserRole")
export class AppUserRoleEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @ManyToOne(() => AppUserEntity, (user) => user.userRoles)
  user: AppUserEntity

  @ManyToOne(() => AppRoleEntity, (roles) => roles.userRoles)
  role: AppRoleEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
