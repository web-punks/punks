import { ApiKeysService } from "./apiKey"
import { AppAuthContextProvider } from "./context"
import { AuthPermissionService } from "./permissions"
import { AuthRolesService } from "./roles"
import { AuthUserRolesService } from "./userRoles"
import { AuthUserService } from "./users"

export const AuthProviders = [
  ApiKeysService,
  AppAuthContextProvider,
  AuthPermissionService,
  AuthRolesService,
  AuthUserRolesService,
  AuthUserService,
]
