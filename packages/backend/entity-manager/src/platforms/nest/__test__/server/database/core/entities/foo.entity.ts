import {
  Column,
  Entity,
  ManyToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "../../../../../decorators"
import { AppTenantEntity } from "./appTenant.entity"
import { CrmContactEntity } from "./crmContact.entity"

export type FooEntityId = string

@WpEntity("foo")
@Entity()
export class FooEntity {
  @PrimaryColumn({ type: "varchar" })
  id: FooEntityId

  @Column({ type: "varchar" })
  name: string

  @Column({ type: "varchar", nullable: true, length: 255 })
  uid?: string

  @Column({ type: "int", nullable: true })
  age?: number

  @ManyToOne(() => AppTenantEntity, {
    nullable: true,
  })
  tenant?: AppTenantEntity

  @ManyToOne(() => CrmContactEntity, (contact) => contact.foos, {
    nullable: true,
  })
  contact: CrmContactEntity

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
