import {
  IEntitiesDeleteParameters,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  IFullTextQuery,
  ISearchOptions,
  ISearchQueryRelations,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  IEntityVersionsSearchParameters,
  IEntityVersionsResultsPaging,
  IEntityVersionsSearchResults,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppPermissionEntity } from "../../database/core/entities/appPermission.entity"
import {
  AppPermissionSorting,
  AppPermissionCursor,
  AppPermissionSearchFilters,
  AppPermissionFacets,
} from "./appPermission.models"
import {
  EntityVersionsCursor,
  EntityVersionsFilters,
  EntityVersionsReference,
  EntityVersionsSorting,
} from "../../shared/api/versioning"

export class AppPermissionSearchSortingField
  implements ISearchSortingField<AppPermissionSorting>
{
  @ApiProperty({ enum: AppPermissionSorting })
  field: AppPermissionSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppPermissionQuerySorting
  implements ISearchSorting<AppPermissionSorting>
{
  @ApiProperty({ required: false, type: [AppPermissionSearchSortingField] })
  fields: AppPermissionSearchSortingField[]
}

export class AppPermissionQueryPaging
  implements ISearchRequestPaging<AppPermissionCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppPermissionCursor

  @ApiProperty()
  pageSize: number
}

export class AppPermissionSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppPermissionFullTextQuery implements IFullTextQuery {
  @ApiProperty()
  term: string

  @ApiProperty()
  fields: string[]
}

export class AppPermissionSearchParameters
  implements
    IEntitySearchParameters<
      AppPermissionEntity,
      AppPermissionSorting,
      AppPermissionCursor
    >
{
  @ApiProperty({ required: false })
  query?: AppPermissionFullTextQuery

  @ApiProperty({ required: false })
  filters?: AppPermissionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppPermissionQuerySorting

  @ApiProperty({ required: false })
  paging?: AppPermissionQueryPaging

  @ApiProperty({ required: false })
  options?: AppPermissionSearchOptions

  relations?: ISearchQueryRelations<AppPermissionEntity>
}

export class AppPermissionSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppPermissionCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppPermissionCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppPermissionCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppPermissionCursor
}

export class AppPermissionSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppPermissionEntity,
      AppPermissionSearchParameters,
      TResult,
      AppPermissionSorting,
      AppPermissionCursor,
      AppPermissionFacets
    >
{
  @ApiProperty()
  request: AppPermissionSearchParameters

  @ApiProperty()
  facets?: AppPermissionFacets

  @ApiProperty()
  paging?: AppPermissionSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppPermissionDeleteParameters
  implements IEntitiesDeleteParameters<AppPermissionSorting>
{
  @ApiProperty({ required: false })
  filters?: AppPermissionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppPermissionQuerySorting
}

export class AppPermissionVersionsSearchParameters
  implements IEntityVersionsSearchParameters<AppPermissionCursor>
{
  @ApiProperty()
  entity: EntityVersionsReference

  @ApiProperty({ required: false })
  filters?: EntityVersionsFilters

  @ApiProperty({ required: false })
  sorting?: EntityVersionsSorting

  @ApiProperty({ required: false })
  paging?: EntityVersionsCursor<AppPermissionCursor>
}

export class AppPermissionVersionsResultsPaging
  implements IEntityVersionsResultsPaging<AppPermissionCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppPermissionCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppPermissionCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppPermissionCursor
}

export class AppPermissionVersionsSearchResults<TResult>
  implements IEntityVersionsSearchResults<TResult, AppPermissionCursor>
{
  @ApiProperty()
  paging?: AppPermissionVersionsResultsPaging

  @ApiProperty()
  items: TResult[]
}
