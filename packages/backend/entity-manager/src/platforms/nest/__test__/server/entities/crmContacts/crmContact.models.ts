import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "../../../../../.."
import { CrmContactEntity } from "../../database/core/entities/crmContact.entity"
import { StringFacets } from "../../shared/api/facets"

export type CrmContactEntityId = string

export type CrmContactCreateData = DeepPartial<Omit<CrmContactEntity, "id">>
export type CrmContactUpdateData = DeepPartial<Omit<CrmContactEntity, "id">>

export enum CrmContactSorting {
  Name = "Name",
}

export type CrmContactCursor = number

export class CrmContactSearchFilters {}

export class CrmContactFacets {
  email: StringFacets
}

export type CrmContactSheetItem = CrmContactEntity
