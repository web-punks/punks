import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { WpEntityRepository, NestTypeOrmRepository } from "../../../../.."
import {
  AppFileReferenceEntity,
  AppFileReferenceEntityId,
} from "../entities/appFileReference.entity"
import { WpFileReferenceRepository } from "../../../../../decorators/files"
import {
  FileReferenceRecord,
  FilesReferenceData,
  IFilesReferenceRepository,
} from "../../../../../../../abstractions"
import { newUuid } from "@punks/backend-core"

@WpFileReferenceRepository()
@WpEntityRepository("appFileReference")
export class AppFileReferenceRepository
  extends NestTypeOrmRepository<
    AppFileReferenceEntity,
    AppFileReferenceEntityId
  >
  implements IFilesReferenceRepository
{
  constructor(
    @InjectRepository(AppFileReferenceEntity, "core")
    repository: Repository<AppFileReferenceEntity>
  ) {
    super(repository)
  }

  async searchReference({
    reference,
    providerId,
  }: {
    reference: string
    providerId: string
  }): Promise<FileReferenceRecord> {
    const items = await this.find({
      where: {
        reference,
        provider: providerId,
      },
    })
    const record = items[0]
    return {
      fileId: record.id,
      contentType: record.contentType,
      fileName: record.fileName,
      filePath: [record.folder, record.fileName].join("/"),
      fileSize: record.fileSize,
      providerId: record.provider,
      reference: record.reference,
      metadata: record.metadata,
      createdOn: record.createdOn,
      updatedOn: record.updatedOn,
    }
  }

  async getReference(fileId: string): Promise<FileReferenceRecord> {
    const record = await this.get(fileId)
    if (!record) {
      throw new Error(`File reference with id ${fileId} not found`)
    }
    return {
      fileId: record.id,
      contentType: record.contentType,
      fileName: record.fileName,
      filePath: [record.folder, record.fileName].join("/"),
      fileSize: record.fileSize,
      providerId: record.provider,
      reference: record.reference,
      metadata: record.metadata,
      createdOn: record.createdOn,
      updatedOn: record.updatedOn,
    }
  }

  async createReference(
    file: FilesReferenceData
  ): Promise<FileReferenceRecord> {
    const id = newUuid()
    await this.create({
      id,
      contentType: file.contentType,
      fileName: file.fileName,
      fileSize: file.fileSize,
      metadata: file.metadata,
      reference: file.reference,
      folder: file.filePath,
      provider: file.providerId,
    })
    return await this.getReference(id)
  }

  async updateReference(
    fileId: string,
    file: FilesReferenceData
  ): Promise<FileReferenceRecord> {
    await this.create({
      id: fileId,
      contentType: file.contentType,
      fileName: file.fileName,
      fileSize: file.fileSize,
      metadata: file.metadata,
      reference: file.reference,
      folder: file.filePath,
      provider: file.providerId,
    })
    return await this.getReference(fileId)
  }

  deleteReference(fileId: string): Promise<void> {
    return this.delete(fileId)
  }
}
