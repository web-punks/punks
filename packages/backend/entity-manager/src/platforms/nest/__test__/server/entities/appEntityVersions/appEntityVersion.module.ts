import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../../../.."
import { AppEntityVersionAdapter } from "./appEntityVersion.adapter"
import { AppEntityVersionAuthMiddleware } from "./appEntityVersion.authentication"
import { AppEntityVersionEntityManager } from "./appEntityVersion.manager"
import { AppEntityVersionQueryBuilder } from "./appEntityVersion.query"
import { AppEntityVersionSerializer } from "./appEntityVersion.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppEntityVersionAdapter,
    AppEntityVersionAuthMiddleware,
    AppEntityVersionEntityManager,
    AppEntityVersionQueryBuilder,
    AppEntityVersionSerializer,
  ],
  exports: [AppEntityVersionEntityManager],
})
export class AppEntityVersionEntityModule {}
