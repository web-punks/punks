import { AppApiKeyEntityModule } from "./appApiKeys/appApiKey.module"
import { AppCompanyEntityModule } from "./appCompanies/appCompany.module"
import { AppDirectoryEntityModule } from "./appDirectories/appDirectory.module"
import { AppDivisionEntityModule } from "./appDivisions/appDivision.module"
import { AppEmailLogEntityModule } from "./appEmailLogs/appEmailLog.module"
import { AppEntityVersionEntityModule } from "./appEntityVersions/appEntityVersion.module"
import { AppFileReferenceEntityModule } from "./appFileReferences/appFileReference.module"
import { AppOrganizationalUnitTypeChildEntityModule } from "./appOrganizationalUnitTypeChildren/appOrganizationalUnitTypeChild.module"
import { AppOrganizationalUnitTypeEntityModule } from "./appOrganizationalUnitTypes/appOrganizationalUnitType.module"
import { AppOrganizationalUnitEntityModule } from "./appOrganizationalUnits/appOrganizationalUnit.module"
import { AppOrganizationEntityModule } from "./appOrganizations/appOrganization.module"
import { AppPermissionEntityModule } from "./appPermissions/appPermission.module"
import { AppRoleOrganizationalUnitTypeEntityModule } from "./appRoleOrganizationalUnitTypes/appRoleOrganizationalUnitType.module"
import { AppRolePermissionEntityModule } from "./appRolePermissions/appRolePermission.module"
import { AppRoleEntityModule } from "./appRoles/appRole.module"
import { AppTenantEntityModule } from "./appTenants/appTenant.module"
import { AppUserGroupMemberEntityModule } from "./appUserGroupMembers/appUserGroupMember.module"
import { AppUserGroupEntityModule } from "./appUserGroups/appUserGroup.module"
import { AppUserProfileEntityModule } from "./appUserProfiles/appUserProfile.module"
import { AppUserRoleEntityModule } from "./appUserRoles/appUserRole.module"
import { AppUserEntityModule } from "./appUsers/appUser.module"
import { CrmContactEntityModule } from "./crmContacts/crmContact.module"
import { FooEntityModule } from "./foos/foo.module"

export const EntityModules = [
  AppApiKeyEntityModule,
  AppCompanyEntityModule,
  AppDivisionEntityModule,
  AppDirectoryEntityModule,
  AppEmailLogEntityModule,
  AppEntityVersionEntityModule,
  AppFileReferenceEntityModule,
  AppOrganizationEntityModule,
  AppOrganizationalUnitEntityModule,
  AppOrganizationalUnitTypeChildEntityModule,
  AppOrganizationalUnitTypeEntityModule,
  AppRoleEntityModule,
  AppPermissionEntityModule,
  AppRolePermissionEntityModule,
  AppRoleOrganizationalUnitTypeEntityModule,
  AppTenantEntityModule,
  AppUserGroupMemberEntityModule,
  AppUserGroupEntityModule,
  AppUserProfileEntityModule,
  AppUserRoleEntityModule,
  AppUserEntityModule,
  CrmContactEntityModule,
  FooEntityModule,
]
