import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "../../../../.."
import { AppUserRoleEntity } from "./appUserRole.entity"

export type AppRoleEntityId = string

@Entity("appRoles")
@WpEntity("appRole")
export class AppRoleEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppRoleEntityId

  @Column({ type: "varchar", length: 255, unique: true })
  uid: string

  @Column({ type: "varchar", length: 255 })
  name: string

  @OneToMany(() => AppUserRoleEntity, (role) => role.user)
  userRoles?: AppUserRoleEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
