import {
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppUserGroupMemberEntity } from "../../database/core/entities/appUserGroupMember.entity"
import { IEntitiesDeleteParameters } from "../../../../../../abstractions/commands"

export type AppUserGroupMemberEntityId = string

export type AppUserGroupMemberCreateData = Partial<
  Omit<AppUserGroupMemberEntity, "id">
>
export type AppUserGroupMemberUpdateData = Partial<
  Omit<AppUserGroupMemberEntity, "id">
>

export class AppUserGroupMemberFacets {}

export class AppUserGroupMemberSearchFilters {}

export enum AppUserGroupMemberSorting {
  Name = "Name",
}

export type AppUserGroupMemberCursor = number

export class AppUserGroupMemberSearchSortingField
  implements ISearchSortingField<AppUserGroupMemberSorting>
{
  @ApiProperty({ enum: AppUserGroupMemberSorting })
  field: AppUserGroupMemberSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppUserGroupMemberQuerySorting
  implements ISearchSorting<AppUserGroupMemberSorting>
{
  @ApiProperty({
    required: false,
    type: [AppUserGroupMemberSearchSortingField],
  })
  fields: AppUserGroupMemberSearchSortingField[]
}

export class AppUserGroupMemberQueryPaging
  implements ISearchRequestPaging<AppUserGroupMemberCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppUserGroupMemberCursor

  @ApiProperty()
  pageSize: number
}

export class AppUserGroupMemberSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppUserGroupMemberSearchParameters
  implements
    IEntitySearchParameters<
      AppUserGroupMemberEntity,
      AppUserGroupMemberSorting,
      AppUserGroupMemberCursor
    >
{
  @ApiProperty({ required: false })
  filters?: AppUserGroupMemberSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserGroupMemberQuerySorting

  @ApiProperty({ required: false })
  paging?: AppUserGroupMemberQueryPaging

  @ApiProperty({ required: false })
  options?: AppUserGroupMemberSearchOptions
}

export class AppUserGroupMemberSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppUserGroupMemberCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppUserGroupMemberCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppUserGroupMemberCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppUserGroupMemberCursor
}

export class AppUserGroupMemberSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppUserGroupMemberEntity,
      AppUserGroupMemberSearchParameters,
      TResult,
      AppUserGroupMemberSorting,
      AppUserGroupMemberCursor,
      AppUserGroupMemberFacets
    >
{
  @ApiProperty()
  request: AppUserGroupMemberSearchParameters

  @ApiProperty()
  facets?: AppUserGroupMemberFacets

  @ApiProperty()
  paging?: AppUserGroupMemberSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppUserGroupMemberDeleteParameters
  implements IEntitiesDeleteParameters<AppUserGroupMemberSorting>
{
  @ApiProperty({ required: false })
  filters?: AppUserGroupMemberSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppUserGroupMemberQuerySorting
}

export type AppUserGroupMemberSheetItem = AppUserGroupMemberEntity
