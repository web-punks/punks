import { IEntityConverter, WpEntityConverter } from "../../../../../.."
import {
  AppEntityVersionCreateDto,
  AppEntityVersionDto,
  AppEntityVersionListItemDto,
  AppEntityVersionUpdateDto,
} from "./appEntityVersion.dto"
import { AppEntityVersionEntity } from "../../database/core/entities/appEntityVersion.entity"

@WpEntityConverter("appEntityVersion")
export class AppEntityVersionConverter
  implements
    IEntityConverter<
      AppEntityVersionEntity,
      AppEntityVersionDto,
      AppEntityVersionListItemDto,
      AppEntityVersionCreateDto,
      AppEntityVersionUpdateDto
    >
{
  toListItemDto(entity: AppEntityVersionEntity): AppEntityVersionListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppEntityVersionEntity): AppEntityVersionDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(
    input: AppEntityVersionCreateDto
  ): Partial<AppEntityVersionEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(
    input: AppEntityVersionUpdateDto
  ): Partial<AppEntityVersionEntity> {
    return {
      ...input,
    }
  }
}
