import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import {
  AppFileReferenceEntity,
  AppFileReferenceEntityId,
} from "../../database/core/entities/appFileReference.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  AppFileReferenceCreateData,
  AppFileReferenceCursor,
  AppFileReferenceSheetItem,
  AppFileReferenceSorting,
  AppFileReferenceUpdateData,
} from "./appFileReference.models"
import { AppFileReferenceSearchParameters } from "./appFileReference.types"

@WpEntitySerializer("appFileReference")
export class AppFileReferenceSerializer extends NestEntitySerializer<
  AppFileReferenceEntity,
  AppFileReferenceEntityId,
  AppFileReferenceCreateData,
  AppFileReferenceUpdateData,
  AppFileReferenceSearchParameters,
  AppFileReferenceSorting,
  AppFileReferenceCursor,
  AppFileReferenceSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appFileReference", registry)
  }

  protected async loadEntities(
    filters: AppFileReferenceSearchParameters
  ): Promise<AppFileReferenceEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppFileReferenceEntity[]
  ): Promise<AppFileReferenceSheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppFileReferenceSheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<AppFileReferenceEntity>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
