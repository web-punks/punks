import {
  NestEntityManager,
  EntityManagerRegistry,
  WpEntityManager,
} from "../../../../../.."
import {
  AppOrganizationalUnitTypeChildCreateData,
  AppOrganizationalUnitTypeChildCursor,
  AppOrganizationalUnitTypeChildFacets,
  AppOrganizationalUnitTypeChildSorting,
  AppOrganizationalUnitTypeChildUpdateData,
} from "./appOrganizationalUnitTypeChild.models"
import {
  AppOrganizationalUnitTypeChildSearchParameters,
  AppOrganizationalUnitTypeChildDeleteParameters,
} from "./appOrganizationalUnitTypeChild.types"
import {
  AppOrganizationalUnitTypeChildEntity,
  AppOrganizationalUnitTypeChildEntityId,
} from "../../database/core/entities/appOrganizationalUnitTypeChild.entity"

@WpEntityManager("appOrganizationalUnitTypeChild")
export class AppOrganizationalUnitTypeChildEntityManager extends NestEntityManager<
  AppOrganizationalUnitTypeChildEntity,
  AppOrganizationalUnitTypeChildEntityId,
  AppOrganizationalUnitTypeChildCreateData,
  AppOrganizationalUnitTypeChildUpdateData,
  AppOrganizationalUnitTypeChildDeleteParameters,
  AppOrganizationalUnitTypeChildSearchParameters,
  AppOrganizationalUnitTypeChildSorting,
  AppOrganizationalUnitTypeChildCursor,
  AppOrganizationalUnitTypeChildFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganizationalUnitTypeChild", registry)
  }
}
