import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppOrganizationalUnitTypeChildEntity } from "../../database/core/entities/appOrganizationalUnitTypeChild.entity"
import {
  AppOrganizationalUnitTypeChildCreateData,
  AppOrganizationalUnitTypeChildUpdateData,
} from "./appOrganizationalUnitTypeChild.models"

@WpEntityAdapter("appOrganizationalUnitTypeChild")
export class AppOrganizationalUnitTypeChildAdapter
  implements
    IEntityAdapter<
      AppOrganizationalUnitTypeChildEntity,
      AppOrganizationalUnitTypeChildCreateData,
      AppOrganizationalUnitTypeChildUpdateData
    >
{
  createDataToEntity(
    data: AppOrganizationalUnitTypeChildCreateData
  ): DeepPartial<AppOrganizationalUnitTypeChildEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppOrganizationalUnitTypeChildUpdateData
  ): DeepPartial<AppOrganizationalUnitTypeChildEntity> {
    return {
      ...data,
    }
  }
}
