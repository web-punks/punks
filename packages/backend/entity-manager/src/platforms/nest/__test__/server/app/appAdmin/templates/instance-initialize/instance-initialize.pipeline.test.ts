import { Repository } from "typeorm"
import { createTestServer } from "../../../.."
import { NestTestServerPorts } from "../../../../../tests/ports"
import { InstanceInitializeTemplate } from "."
import { AppRoleEntity } from "../../../../database/core/entities/appRole.entity"

describe("Test Instance Initialize Pipeline", () => {
  let pipeline: InstanceInitializeTemplate
  let rolesRepo: Repository<AppRoleEntity>

  beforeEach(async () => {
    const { dataSource, app } = await createTestServer({
      useAuthentication: true,
      serverStartPort: NestTestServerPorts.TenantInitPipeline,
    })

    pipeline = app.get(InstanceInitializeTemplate)
    rolesRepo = dataSource.getRepository(AppRoleEntity)
  })

  it("should run the pipeline successfully", async () => {
    await pipeline.invoke({
      roles: [
        {
          uid: "test-role",
          name: "Test Role",
        },
        {
          uid: "test-role-2",
          name: "Test Role 2",
        },
      ],
    })

    const roles = await rolesRepo.find()
    expect(roles).toMatchObject([
      {
        id: expect.any(String),
        uid: "test-role",
        name: "Test Role",
        createdOn: expect.any(Date),
        updatedOn: expect.any(Date),
      },
      {
        id: expect.any(String),
        uid: "test-role-2",
        name: "Test Role 2",
        createdOn: expect.any(Date),
        updatedOn: expect.any(Date),
      },
    ])
  })
})
