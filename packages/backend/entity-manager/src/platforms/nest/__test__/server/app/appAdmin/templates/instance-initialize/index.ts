import { PipelineDefinition } from "../../../../../../../../types"
import { WpPipeline } from "../../../../../../decorators"
import { IPipelineTemplateBuilder } from "../../../../../../pipelines/builder/types"
import { NestPipelineTemplate } from "../../../../../../pipelines/template"
import { AppAuthContext } from "../../../../infrastructure/authentication"
import { AuthenticationService } from "../../../../../../extensions"
import { InstanceInitializeInput } from "./models"
import { AppTenantEntityManager } from "../../../../entities/appTenants/appTenant.manager"

@WpPipeline("InstanceInitialize")
export class InstanceInitializeTemplate extends NestPipelineTemplate<
  InstanceInitializeInput,
  void,
  AppAuthContext
> {
  constructor(
    private readonly auth: AuthenticationService,
    private readonly tenants: AppTenantEntityManager
  ) {
    super()
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<InstanceInitializeInput, AppAuthContext>
  ): PipelineDefinition<InstanceInitializeInput, void, AppAuthContext> {
    return builder
      .addStep((step) => {
        step.addOperation({
          name: "Roles ensure",
          action: async (input) => {
            for (const role of input.roles) {
              await this.auth.rolesService.ensure(role.uid, {
                name: role.name,
              })
            }
          },
          precondition: async () =>
            (await this.tenants.manager.count.execute({})) === 0,
        })
      })
      .complete()
  }
}
