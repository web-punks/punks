import {
  DeepPartial,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppDivisionEntity } from "../../database/core/entities/appDivision.entity"
import { IEntitiesDeleteParameters } from "../../../../../../abstractions/commands"

export type AppDivisionEntityId = string

export type AppDivisionCreateData = DeepPartial<Omit<AppDivisionEntity, "id">>
export type AppDivisionUpdateData = DeepPartial<Omit<AppDivisionEntity, "id">>

export class AppDivisionFacets {}

export class AppDivisionSearchFilters {}

export enum AppDivisionSorting {
  Name = "Name",
}

export type AppDivisionCursor = number

export class AppDivisionSearchSortingField
  implements ISearchSortingField<AppDivisionSorting>
{
  @ApiProperty({ enum: AppDivisionSorting })
  field: AppDivisionSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppDivisionQuerySorting
  implements ISearchSorting<AppDivisionSorting>
{
  @ApiProperty({ required: false, type: [AppDivisionSearchSortingField] })
  fields: AppDivisionSearchSortingField[]
}

export class AppDivisionQueryPaging
  implements ISearchRequestPaging<AppDivisionCursor>
{
  @ApiProperty({ required: false })
  cursor?: AppDivisionCursor

  @ApiProperty()
  pageSize: number
}

export class AppDivisionSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppDivisionSearchParameters
  implements
    IEntitySearchParameters<
      AppDivisionEntity,
      AppDivisionSorting,
      AppDivisionCursor
    >
{
  @ApiProperty({ required: false })
  filters?: AppDivisionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppDivisionQuerySorting

  @ApiProperty({ required: false })
  paging?: AppDivisionQueryPaging

  @ApiProperty({ required: false })
  options?: AppDivisionSearchOptions
}

export class AppDivisionSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppDivisionCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppDivisionCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppDivisionCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppDivisionCursor
}

export class AppDivisionSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppDivisionEntity,
      AppDivisionSearchParameters,
      TResult,
      AppDivisionSorting,
      AppDivisionCursor,
      AppDivisionFacets
    >
{
  @ApiProperty()
  request: AppDivisionSearchParameters

  @ApiProperty()
  facets?: AppDivisionFacets

  @ApiProperty()
  paging?: AppDivisionSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppDivisionDeleteParameters
  implements IEntitiesDeleteParameters<AppDivisionSorting>
{
  @ApiProperty({ required: false })
  filters?: AppDivisionSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppDivisionQuerySorting
}

export type AppDivisionSheetItem = AppDivisionEntity
