import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { AppTenantActions } from "./appTenant.actions"
import {
  AppTenantCreateDto,
  AppTenantDto,
  AppTenantSearchRequest,
  AppTenantSearchResponse,
  AppTenantUpdateDto,
} from "./appTenant.dto"
import { AppRoles } from "../../infrastructure/authentication/roles"

@Controller("v1/appTenant")
export class AppTenantController {
  constructor(private readonly actions: AppTenantActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "appTenantGet",
  })
  @ApiOkResponse({
    type: AppTenantDto,
  })
  async item(@Param("id") id: string): Promise<AppTenantDto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "appTenantCreate",
  })
  @ApiOkResponse({
    type: AppTenantDto,
  })
  async create(@Body() data: AppTenantCreateDto): Promise<AppTenantDto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: AppTenantDto,
  })
  @ApiOperation({
    operationId: "appTenantUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: AppTenantUpdateDto
  ): Promise<AppTenantDto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "appTenantDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "appTenantSearch",
  })
  @ApiOkResponse({
    type: AppTenantSearchResponse,
  })
  async search(
    @Body() request: AppTenantSearchRequest
  ): Promise<AppTenantSearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }
}
