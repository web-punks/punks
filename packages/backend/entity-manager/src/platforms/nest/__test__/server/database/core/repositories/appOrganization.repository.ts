import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { NestTypeOrmRepository, WpEntityRepository } from "../../../../.."
import { AppOrganizationEntity } from "../entities/appOrganization.entity"
import { AppOrganizationEntityId } from "../../../entities/appOrganizations/appOrganization.models"

@WpEntityRepository("appOrganization")
export class AppOrganizationRepository extends NestTypeOrmRepository<
  AppOrganizationEntity,
  AppOrganizationEntityId
> {
  constructor(
    @InjectRepository(AppOrganizationEntity, "core")
    repository: Repository<AppOrganizationEntity>
  ) {
    super(repository)
  }

  getOrganization(organizationId: string) {
    return this.innerRepository.findOne({
      where: {
        id: organizationId,
      },
      relations: {
        tenant: true,
      },
    })
  }

  getTenantOrganizations(tenantId: string) {
    return this.innerRepository.find({
      where: {
        tenant: {
          id: tenantId,
        },
      },
      relations: {
        tenant: true,
      },
    })
  }
}
