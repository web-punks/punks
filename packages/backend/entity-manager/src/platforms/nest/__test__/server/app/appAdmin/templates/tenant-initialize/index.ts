import { PipelineDefinition } from "../../../../../../../../types"
import { WpPipeline } from "../../../../../../decorators"
import { IPipelineTemplateBuilder } from "../../../../../../pipelines/builder/types"
import { NestPipelineTemplate } from "../../../../../../pipelines/template"
import { AppAuthContext } from "../../../../infrastructure/authentication"
import { AppTenantEntityManager } from "../../../../entities/appTenants/appTenant.manager"
import { TenantInitializeRequest, TenantInitializeResponse } from "./models"
import { AppTenantEntity } from "../../../../database/core/entities/appTenant.entity"
import {
  AuthenticationService,
  UserCreationResult,
} from "../../../../../../extensions"
import {
  AuthUserContext,
  AuthUserRegistrationData,
} from "../../../../infrastructure/authentication/models"
import { AppRoles } from "../../../../infrastructure/authentication/roles"
import {
  AppDirectoryEntity,
  AppDirectoryEntityId,
} from "../../../../database/core/entities/appDirectory.entity"
import { AppDirectoryEntityManager } from "../../../../entities/appDirectories/appDirectory.manager"
import { AppTenantEntityId } from "../../../../entities/appTenants/appTenant.models"
import { EntityReference } from "../../../../../../../../models"

@WpPipeline("TenantInitialize")
export class TenantInitializeTemplate extends NestPipelineTemplate<
  TenantInitializeRequest,
  TenantInitializeResponse,
  AppAuthContext
> {
  constructor(
    private readonly auth: AuthenticationService,
    private readonly tenants: AppTenantEntityManager,
    private readonly directories: AppDirectoryEntityManager
  ) {
    super()
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<TenantInitializeRequest, AppAuthContext>
  ): PipelineDefinition<
    TenantInitializeRequest,
    TenantInitializeResponse,
    AppAuthContext
  > {
    return builder
      .addStep<EntityReference<AppTenantEntityId>>((step) => {
        step
          .addOperation({
            name: "Tenant create",
            action: async (input) =>
              this.tenants.manager.create.execute({
                name: input.tenant.name,
                uid: input.tenant.uid,
              }),
            precondition: async (input) =>
              !(await this.tenants.manager.exists.execute({
                uid: input.tenant.uid,
              })),
          })
          .withRollback({
            name: "Tenant create rollback",
            action: async (_, output) =>
              output && this.tenants.manager.delete.execute(output.id),
          })
      })
      .addStep<EntityReference<AppDirectoryEntityId>>((step) => {
        step
          .addOperation({
            name: "Directory create",
            action: async (step, state) =>
              this.directories.manager.create.execute({
                name: state.pipelineInput.directory.name,
                uid: state.pipelineInput.directory.uid,
                default: true,
                tenant: {
                  id: step.id,
                },
              }),
            precondition: async (_, state) =>
              !(await this.directories.manager.exists.execute({
                uid: state.pipelineInput.directory.uid,
              })),
          })
          .withRollback({
            name: "Directory create rollback",
            action: async (_, output) =>
              output && this.directories.manager.delete.execute(output.id),
          })
      })
      .addStep<UserCreationResult>((step) => {
        step
          .addOperation({
            name: "Admin create",
            action: async (input, state) =>
              this.auth.userCreate<AuthUserRegistrationData, AuthUserContext>({
                email: state.pipelineInput.defaultAdmin.email,
                password: state.pipelineInput.defaultAdmin.password,
                userName: state.pipelineInput.defaultAdmin.email,
                registrationInfo: {
                  firstName: state.pipelineInput.defaultAdmin.firstName,
                  lastName: state.pipelineInput.defaultAdmin.lastName,
                  birthDate: "",
                },
                context: {
                  tenantUid: state.pipelineInput.tenant.uid,
                },
              }),
          })
          .withRollback({
            name: "Admin create rollback",
            action: async (_, output) => {
              output?.userId &&
                (await this.auth.userDelete({
                  userId: output.userId,
                }))
            },
          })
      })
      .addStep<void>((step) => {
        step
          .addOperation({
            name: "User role assign",
            action: async (input, state) =>
              this.auth.userRolesService.addUserToRoleByUid(
                input.userId!,
                AppRoles.TenantAdmin.uid
              ),
          })
          .withRollback({
            name: "User role assign rollback",
            action: async (input) =>
              this.auth.userRolesService.removeUserFromRoleByUid(
                input.userId!,
                AppRoles.TenantAdmin.uid
              ),
          })
      })
      .addStep<TenantInitializeResponse>((step) => {
        step.addOperation({
          name: "Map result",
          action: (_, state) => ({
            tenantId: this.utils.getStepOutput<AppTenantEntity>(state, 0).id,
          }),
        })
      })
      .complete()
  }
}
