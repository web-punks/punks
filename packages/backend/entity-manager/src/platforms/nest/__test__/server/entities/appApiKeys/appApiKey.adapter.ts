import {
  DeepPartial,
  IAuthenticationContext,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppApiKeyEntity } from "../../database/core/entities/appApiKey.entity"
import { AppApiKeyCreateData, AppApiKeyUpdateData } from "./appApiKey.models"

@WpEntityAdapter("appApiKey")
export class AppApiKeyAdapter
  implements
    IEntityAdapter<AppApiKeyEntity, AppApiKeyCreateData, AppApiKeyUpdateData>
{
  createDataToEntity(
    data: AppApiKeyCreateData,
    context: IAuthenticationContext<unknown>
  ): DeepPartial<AppApiKeyEntity> {
    return {
      id: newUuid(),
      key: `pk_${newUuid().replaceAll("-", "")}`,
      ...data,
    }
  }

  updateDataToEntity(
    data: AppApiKeyUpdateData,
    context: IAuthenticationContext<unknown>
  ): DeepPartial<AppApiKeyEntity> {
    return {
      ...data,
    }
  }
}
