import {
  DeepPartial,
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../.."
import { ApiProperty } from "@nestjs/swagger"
import { AppRoleEntity } from "../../database/core/entities/appRole.entity"
import { IEntitiesDeleteParameters } from "../../../../../../abstractions/commands"

export type AppRoleEntityId = string

export type AppRoleCreateData = DeepPartial<Omit<AppRoleEntity, "id">>
export type AppRoleUpdateData = DeepPartial<Omit<AppRoleEntity, "id">>

export class AppRoleFacets {}

export class AppRoleSearchFilters {
  @ApiProperty({ required: false })
  uid?: string[]
}

export enum AppRoleSorting {
  Name = "Name",
}

export type AppRoleCursor = number

export class AppRoleSearchSortingField
  implements ISearchSortingField<AppRoleSorting>
{
  @ApiProperty({ enum: AppRoleSorting })
  field: AppRoleSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class AppRoleQuerySorting implements ISearchSorting<AppRoleSorting> {
  @ApiProperty({ required: false, type: [AppRoleSearchSortingField] })
  fields: AppRoleSearchSortingField[]
}

export class AppRoleQueryPaging implements ISearchRequestPaging<AppRoleCursor> {
  @ApiProperty({ required: false })
  cursor?: AppRoleCursor

  @ApiProperty()
  pageSize: number
}

export class AppRoleSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class AppRoleSearchParameters
  implements
    IEntitySearchParameters<AppRoleEntity, AppRoleSorting, AppRoleCursor>
{
  @ApiProperty({ required: false })
  filters?: AppRoleSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppRoleQuerySorting

  @ApiProperty({ required: false })
  paging?: AppRoleQueryPaging

  @ApiProperty({ required: false })
  options?: AppRoleSearchOptions
}

export class AppRoleSearchResultsPaging
  implements IEntitiesSearchResultsPaging<AppRoleCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: AppRoleCursor

  @ApiProperty({ required: false })
  currentPageCursor?: AppRoleCursor

  @ApiProperty({ required: false })
  prevPageCursor?: AppRoleCursor
}

export class AppRoleSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      AppRoleEntity,
      AppRoleSearchParameters,
      TResult,
      AppRoleSorting,
      AppRoleCursor,
      AppRoleFacets
    >
{
  @ApiProperty()
  request: AppRoleSearchParameters

  @ApiProperty()
  facets?: AppRoleFacets

  @ApiProperty()
  paging?: AppRoleSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class AppRoleDeleteParameters
  implements IEntitiesDeleteParameters<AppRoleSorting>
{
  @ApiProperty({ required: false })
  filters?: AppRoleSearchFilters

  @ApiProperty({ required: false })
  sorting?: AppRoleQuerySorting
}

export type AppRoleSheetItem = AppRoleEntity
