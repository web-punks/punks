import { ApiProperty } from "@nestjs/swagger"
import {
  AppUserProfileSearchParameters,
  AppUserProfileSearchResults,
} from "../../entities/appUserProfiles/appUserProfile.models"

export class AppUserProfileDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserProfileListItemDto {
  @ApiProperty()
  id: string
}

export class AppUserProfileCreateDto {}

export class AppUserProfileUpdateDto {
  @ApiProperty()
  id: string
}

export class AppUserProfileSearchRequest {
  @ApiProperty()
  params: AppUserProfileSearchParameters
}

export class AppUserProfileSearchResponse extends AppUserProfileSearchResults<AppUserProfileListItemDto> {
  @ApiProperty({ type: [AppUserProfileListItemDto] })
  items: AppUserProfileListItemDto[]
}
