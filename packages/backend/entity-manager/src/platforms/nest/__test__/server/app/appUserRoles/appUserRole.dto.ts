import { ApiProperty } from "@nestjs/swagger"
import {
  AppUserRoleSearchParameters,
  AppUserRoleSearchResults,
} from "../../entities/appUserRoles/appUserRole.models"

export class AppUserRoleDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppUserRoleListItemDto {
  @ApiProperty()
  id: string
}

export class AppUserRoleCreateDto {}

export class AppUserRoleUpdateDto {
  @ApiProperty()
  id: string
}

export class AppUserRoleSearchRequest {
  @ApiProperty()
  params: AppUserRoleSearchParameters
}

export class AppUserRoleSearchResponse extends AppUserRoleSearchResults<AppUserRoleListItemDto> {
  @ApiProperty({ type: [AppUserRoleListItemDto] })
  items: AppUserRoleListItemDto[]
}
