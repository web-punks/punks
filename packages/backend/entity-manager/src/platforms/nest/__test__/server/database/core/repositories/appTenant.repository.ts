import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { NestTypeOrmRepository, WpEntityRepository } from "../../../../.."
import { AppTenantEntity } from "../entities/appTenant.entity"
import { AppTenantEntityId } from "../../../entities/appTenants/appTenant.models"

@WpEntityRepository("appTenant")
export class AppTenantRepository extends NestTypeOrmRepository<
  AppTenantEntity,
  AppTenantEntityId
> {
  constructor(
    @InjectRepository(AppTenantEntity, "core")
    repository: Repository<AppTenantEntity>
  ) {
    super(repository)
  }
}
