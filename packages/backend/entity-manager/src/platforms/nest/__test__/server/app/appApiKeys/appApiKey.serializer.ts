import {
  WpEntitySerializer,
  NestEntitySerializer,
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
} from "../../../../../.."
import {
  AppApiKeyEntity,
  AppApiKeyEntityId,
} from "../../database/core/entities/appApiKey.entity"
import { AppApiKeySearchParameters } from "../../entities/appApiKeys/appApiKey.types"
import {
  AppApiKeyCreateData,
  AppApiKeyUpdateData,
  AppApiKeySorting,
  AppApiKeyCursor,
  AppApiKeySheetItem,
} from "../../entities/appApiKeys/appApiKey.models"
import { AppAuthContext } from "../../infrastructure/authentication"

@WpEntitySerializer("appApiKey")
export class AppApiKeySerializer extends NestEntitySerializer<
  AppApiKeyEntity,
  AppApiKeyEntityId,
  AppApiKeyCreateData,
  AppApiKeyUpdateData,
  AppApiKeySearchParameters,
  AppApiKeySorting,
  AppApiKeyCursor,
  AppApiKeySheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appApiKey", registry)
  }

  protected async loadEntities(
    filters: AppApiKeySearchParameters
  ): Promise<AppApiKeyEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: AppApiKeyEntity[]
  ): Promise<AppApiKeySheetItem[]> {
    return entities
  }

  protected async importItem(
    item: AppApiKeySheetItem,
    context: AppAuthContext
  ) {
    return item.id
      ? await this.manager.update.execute(item.id, item)
      : await this.manager.create.execute(item)
  }

  protected async getDefinition(
    context: AppAuthContext
  ): Promise<EntitySerializerSheetDefinition<AppApiKeyEntity>> {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
      ],
    }
  }
}
