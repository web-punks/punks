import {
  DeepPartial,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "../../../../../.."
import { AppPermissionEntity } from "../../database/core/entities/appPermission.entity"
import {
  AppPermissionCreateData,
  AppPermissionUpdateData,
} from "./appPermission.models"

@WpEntityAdapter("appPermission")
export class AppPermissionAdapter
  implements
    IEntityAdapter<
      AppPermissionEntity,
      AppPermissionCreateData,
      AppPermissionUpdateData
    >
{
  createDataToEntity(
    data: AppPermissionCreateData
  ): DeepPartial<AppPermissionEntity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: AppPermissionUpdateData
  ): DeepPartial<AppPermissionEntity> {
    return {
      ...data,
    }
  }
}
