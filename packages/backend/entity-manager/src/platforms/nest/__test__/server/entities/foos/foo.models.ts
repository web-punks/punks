import { ApiProperty } from "@nestjs/swagger"
import {
  IEntitySearchParameters,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "../../../../../../abstractions"
import {
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
} from "../../../../../../models"
import { FooEntity } from "../../database/core/entities/foo.entity"
import { IEntitiesDeleteParameters } from "../../../../../../abstractions/commands"
import { NumberFacetsType } from "../../../../../../integrations/repository/typeorm/facets"

export type FooEntityId = string

export type FooCreateData = Partial<FooEntity>
export type FooUpdateData = Partial<FooEntity>

export interface FooSearchParameters
  extends IEntitySearchParameters<FooEntity, FooSorting, FooCursor> {
  filters?: FooSearchFilters
}

export interface FooFacets {
  age: NumberFacetsType
}

export interface FooSearchFilters {
  minAge?: number
  maxAge?: number
}

export enum FooSorting {
  Name = "Name",
  Age = "Age",
}

export type FooCursor = number

export class FooSearchSortingField implements ISearchSortingField<FooSorting> {
  @ApiProperty({ enum: FooSorting })
  field: FooSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class FooQuerySorting implements ISearchSorting<FooSorting> {
  @ApiProperty({ required: false, type: [FooSearchSortingField] })
  fields: FooSearchSortingField[]
}

export class FooSearchResultsPaging
  implements IEntitiesSearchResultsPaging<FooCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: FooCursor

  @ApiProperty({ required: false })
  currentPageCursor?: FooCursor

  @ApiProperty({ required: false })
  prevPageCursor?: FooCursor
}

export class FooSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      FooEntity,
      FooSearchParameters,
      TResult,
      FooSorting,
      FooCursor,
      FooFacets
    >
{
  @ApiProperty()
  request: FooSearchParameters

  @ApiProperty()
  facets?: FooFacets

  @ApiProperty()
  paging?: FooSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}

export class FooDeleteParameters
  implements IEntitiesDeleteParameters<FooSorting>
{
  @ApiProperty({ required: false })
  filters?: FooSearchFilters

  @ApiProperty({ required: false })
  sorting?: FooQuerySorting
}

export type FooSheetItem = {
  id: FooEntityId
  name: string
  uid?: string
  age?: number
  tenantUid?: string
  timestamp: Date
}
