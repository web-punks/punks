import { Injectable } from "@nestjs/common"
import { AppUserContext } from "../../types"
import {
  AppSessionService,
  IAuthApiKey,
  IAuthOrganizationalUnit,
  IAuthPermission,
  IAuthRole,
  IAuthUser,
  IAuthenticationContext,
  IAuthenticationContextProvider,
} from "../../../../../../../.."
import { AppOrganizationEntityManager } from "../../../../entities/appOrganizations/appOrganization.manager"
import { AppOrganizationRepository } from "../../../../database/core/repositories/appOrganization.repository"

@Injectable()
export class AppAuthContextProvider
  implements IAuthenticationContextProvider<AppUserContext>
{
  constructor(
    private readonly appSessionService: AppSessionService,
    private readonly organizations: AppOrganizationEntityManager
  ) {}

  async getContext(): Promise<IAuthenticationContext<AppUserContext>> {
    const auth = this.getRequestAuth()

    if (!auth) {
      return Promise.resolve({
        isAuthenticated: false,
        isAnonymous: true,
        userId: undefined,
        userContext: undefined,
        userInfo: undefined,
        userRoles: [],
        userPermissions: [],
        userOrganizationalUnits: [],
      })
    }

    const organizationId = auth.organizationId ?? auth.user?.organization?.id
    const organization = organizationId
      ? await this.organizations
          .getRepository<AppOrganizationRepository>()
          .getOrganization(organizationId)
      : undefined

    if (auth.apiKey) {
      return Promise.resolve({
        isAuthenticated: true,
        isAnonymous: false,
        apiKeyId: auth.apiKey.id,
        userContext: auth.apiKey.organization
          ? {
              organizationId: auth.apiKey.organization.id,
              organizationUid: auth.apiKey.organization.uid,
              tenantId: auth.apiKey.organization.tenant.id,
              tenantUid: auth.apiKey.organization.tenant.uid,
            }
          : undefined,
      })
    }

    if (auth.user) {
      return Promise.resolve({
        isAuthenticated: true,
        isAnonymous: false,
        userId: auth.user.id,
        userContext: organization
          ? {
              organizationId: organization.id,
              organizationUid: organization.uid,
              tenantId: organization.tenant.id,
              tenantUid: organization.tenant.uid,
              descendantOrganizationalUnits: auth.descendantOrganizationalUnits,
            }
          : undefined,
        userInfo: auth.user.profile
          ? {
              email: auth.user.email,
              firstName: auth.user.profile.firstName,
              lastName: auth.user.profile.lastName,
            }
          : undefined,
        userRoles:
          auth.roles?.map((x) => ({
            id: x.id,
            uid: x.uid,
          })) ?? [],
        userPermissions:
          auth.permissions?.map((x) => ({
            id: x.id,
            uid: x.uid,
          })) ?? [],
        userOrganizationalUnits:
          auth.organizationalUnits?.map((x) => ({
            id: x.id,
            uid: x.uid,
          })) ?? [],
      })
    }

    return Promise.resolve({
      isAuthenticated: false,
      isAnonymous: true,
      userId: undefined,
      userContext: undefined,
      userInfo: undefined,
      userRoles: [],
      userPermissions: [],
      userOrganizationalUnits: [],
    })
  }

  private getRequestAuth() {
    const request = this.appSessionService.retrieveRequest() as any
    const auth = request?.auth
    if (!auth) {
      return undefined
    }

    return {
      apiKey: auth.apiKey as IAuthApiKey | undefined,
      user: auth.user as IAuthUser | undefined,
      roles: auth.roles as IAuthRole[] | undefined,
      permissions: auth.permissions as IAuthPermission[] | undefined,
      organizationalUnits: auth.organizationalUnits as
        | IAuthOrganizationalUnit[]
        | undefined,
      descendantOrganizationalUnits: auth.descendantOrganizationalUnits as
        | IAuthOrganizationalUnit[]
        | undefined,
      organizationId: request.headers["app-organization-id"] as string,
    }
  }
}
