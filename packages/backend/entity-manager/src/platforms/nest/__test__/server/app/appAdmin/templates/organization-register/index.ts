import { PipelineDefinition } from "../../../../../../../../types"
import { WpPipeline } from "../../../../../../decorators"
import { IPipelineTemplateBuilder } from "../../../../../../pipelines/builder/types"
import { NestPipelineTemplate } from "../../../../../../pipelines/template"
import { AppCompanyEntity } from "../../../../database/core/entities/appCompany.entity"
import { AppCompanyEntityManager } from "../../../../entities/appCompanies/appCompany.manager"
import { AppAuthContext } from "../../../../infrastructure/authentication"
import { AppUserGroupEntity } from "../../../../database/core/entities/appUserGroup.entity"
import { AppUserGroupEntityManager } from "../../../../entities/appUserGroups/appUserGroup.manager"
import { AppOrganizationEntityManager } from "../../../../entities/appOrganizations/appOrganization.manager"
import { AppOrganizationEntity } from "../../../../database/core/entities/appOrganization.entity"
import { AppTenantEntityManager } from "../../../../entities/appTenants/appTenant.manager"
import {
  OrganizationInitializeRequest,
  OrganizationInitializeResponse,
} from "./models"
import { EntityReference } from "../../../../../../../../models"
import { AppOrganizationEntityId } from "../../../../entities/appOrganizations/appOrganization.models"
import { AppCompanyEntityId } from "../../../../entities/appCompanies/appCompany.models"
import { AppUserGroupEntityId } from "../../../../entities/appUserGroups/appUserGroup.models"

@WpPipeline("OrganizationRegister")
export class OrganizationRegisterTemplate extends NestPipelineTemplate<
  OrganizationInitializeRequest,
  OrganizationInitializeResponse,
  AppAuthContext
> {
  constructor(
    private readonly organizations: AppOrganizationEntityManager,
    private readonly tenants: AppTenantEntityManager,
    private readonly companies: AppCompanyEntityManager,
    private readonly userGroups: AppUserGroupEntityManager
  ) {
    super()
  }

  protected isAuthorized(context: AppAuthContext) {
    return true
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<
      OrganizationInitializeRequest,
      AppAuthContext
    >
  ): PipelineDefinition<
    OrganizationInitializeRequest,
    OrganizationInitializeResponse,
    AppAuthContext
  > {
    return builder
      .addStep<EntityReference<AppOrganizationEntityId>>((step) => {
        step
          .addOperation({
            name: "Organization create",
            action: async (input) =>
              this.organizations.manager.create.execute({
                name: input.organization.name,
                uid: input.organization.uid,
                tenant: { id: input.organization.tenantId },
              }),
            precondition: async (input) =>
              this.tenants.manager.exists.execute({
                id: input.organization.tenantId,
              }),
          })
          .withRollback({
            name: "Organization create rollback",
            action: async (_, output) =>
              output && this.organizations.manager.delete.execute(output.id),
          })
      })
      .addStep<EntityReference<AppCompanyEntityId>>((step) => {
        step
          .addOperation({
            name: "Company create",
            action: async (input, state) =>
              this.companies.manager.create.execute({
                name: state.pipelineInput.company.name,
                uid: state.pipelineInput.company.uid,
                organization: { id: input.id },
              }),
          })
          .withRollback({
            name: "Company create rollback",
            action: async (_, output) =>
              output && this.companies.manager.delete.execute(output.id),
          })
      })
      .addStep<EntityReference<AppUserGroupEntityId>>((step) => {
        step
          .addOperation({
            name: "User group create",
            action: async (input, state) =>
              this.userGroups.manager.create.execute({
                name: `${state.pipelineInput.organization.name} Admins`,
                uid: `${state.pipelineInput.organization.uid}-admins`,
              }),
          })
          .withRollback({
            name: "User group create rollback",
            action: async (_, output) =>
              output && this.userGroups.manager.delete.execute(output.id),
          })
      })
      .addStep<OrganizationInitializeResponse>((step) => {
        step.addOperation({
          name: "Map result",
          action: (_, state) => ({
            companyId: this.utils.getStepOutput<AppOrganizationEntity>(state, 1)
              .id,
            organizationId: this.utils.getStepOutput<AppCompanyEntity>(state, 0)
              .id,
          }),
        })
      })
      .complete()
  }
}
