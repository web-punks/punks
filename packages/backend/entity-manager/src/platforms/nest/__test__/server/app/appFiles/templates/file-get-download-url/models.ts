import { ApiProperty } from "@nestjs/swagger"

export interface FileGetDownloadUrlInput {
  fileId: string
}

export class FileGetDownloadUrlResult {
  @ApiProperty()
  url: string
}
