import { ApiProperty } from "@nestjs/swagger"

export class UserUpdateRequest {
  @ApiProperty()
  email: string

  @ApiProperty()
  userName: string

  @ApiProperty()
  fistName: string

  @ApiProperty()
  lastName: string

  @ApiProperty()
  roleUid: string

  @ApiProperty({ required: false })
  birthDate?: string
}

export type UserUpdatePipelineInput = {
  userId: string
  data: UserUpdateRequest
}
