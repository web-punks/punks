import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "../../../../../.."
import { AppRolePermissionEntity } from "../../database/core/entities/appRolePermission.entity"

export type AppRolePermissionCreateData = DeepPartial<
  Omit<AppRolePermissionEntity, "id">
>
export type AppRolePermissionUpdateData = DeepPartial<
  Omit<AppRolePermissionEntity, "id">
>

export enum AppRolePermissionSorting {
  Name = "Name",
}

export type AppRolePermissionCursor = number

export class AppRolePermissionSearchFilters {
  @ApiProperty({ required: false })
  roleIds?: string[]

  @ApiProperty({ required: false })
  permissionIds?: string[]
}

export class AppRolePermissionFacets {}

export type AppRolePermissionSheetItem = AppRolePermissionEntity
