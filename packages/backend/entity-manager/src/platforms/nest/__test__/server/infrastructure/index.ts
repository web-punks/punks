import { AuthenticationInfrastructureModule } from "./authentication/module"
import { EmailInfrastructureModule } from "./email/module"

export const InfrastructureModules = [
  AuthenticationInfrastructureModule,
  EmailInfrastructureModule,
]
