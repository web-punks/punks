import request from "supertest"
import { Repository } from "typeorm"
import { newUuid } from "@punks/backend-core"
import { createTestServer } from "../../../.."
import { NestTestServerPorts } from "../../../../../tests/ports"
import { AppCompanyEntity } from "../../../../database/core/entities/appCompany.entity"
import { AppOrganizationEntity } from "../../../../database/core/entities/appOrganization.entity"
import { AppTenantEntity } from "../../../../database/core/entities/appTenant.entity"
import { AppUserGroupEntityManager } from "../../../../entities/appUserGroups/appUserGroup.manager"
import { OrganizationInitializeRequest } from "./models"

describe("Test Organization Initialize Pipeline", () => {
  let httpServer: any
  let tenantsRepo: Repository<AppTenantEntity>
  let organizationsRepo: Repository<AppOrganizationEntity>
  let companiesRepo: Repository<AppCompanyEntity>
  let userGroupsManager: AppUserGroupEntityManager
  let terminateServer: () => Promise<void>

  beforeEach(async () => {
    const { server, dataSource, app, terminate } = await createTestServer({
      useAuthentication: true,
      createWebServer: true,
      serverStartPort: NestTestServerPorts.CompanyInitPipeline,
    })
    httpServer = app.getHttpServer()
    tenantsRepo = dataSource.getRepository(AppTenantEntity)
    organizationsRepo = dataSource.getRepository(AppOrganizationEntity)
    companiesRepo = dataSource.getRepository(AppCompanyEntity)
    userGroupsManager = server.get(AppUserGroupEntityManager)
    terminateServer = terminate
  })

  afterEach(async () => {
    await terminateServer()
  })

  it("should run a organization creation pipeline template successfully", async () => {
    const tenantId = newUuid()
    await tenantsRepo.insert({
      id: tenantId,
      name: "Test Tenant",
      uid: "test-tenant",
    })

    const response = await request(httpServer)
      .post("/v1/appAdmin/organizationInitialize")
      .send({
        company: {
          name: "Test Company",
          uid: "test-company",
        },
        organization: {
          tenantId,
          name: "Test Organization",
          uid: "test-organization",
        },
      } as OrganizationInitializeRequest)
      .expect(201)

    const organization = await organizationsRepo.findOne({
      where: {
        uid: "test-organization",
      },
      relations: ["tenant"],
    })
    expect(organization).toMatchObject({
      id: expect.any(String),
      name: "Test Organization",
      uid: "test-organization",
      tenant: {
        id: tenantId,
      },
    })

    const company = await companiesRepo.findOne({
      where: {
        uid: "test-company",
      },
      relations: ["organization"],
    })
    expect(company).toMatchObject({
      id: expect.any(String),
      name: "Test Company",
      uid: "test-company",
      organization: {
        id: organization!.id,
      },
    })

    expect(response.body).toMatchObject({
      companyId: company!.id,
      organizationId: organization!.id,
    })
  })

  it("should run a organization creation pipeline template, fail and roll back", async () => {
    const tenantId = newUuid()
    await tenantsRepo.insert({
      id: tenantId,
      name: "Test Tenant",
      uid: "test-tenant",
    })

    jest
      .spyOn(userGroupsManager.manager.create, "execute")
      .mockImplementationOnce(() => {
        throw new Error("Test error")
      })

    await request(httpServer)
      .post("/v1/appAdmin/organizationInitialize")
      .send({
        company: {
          name: "Test Company",
          uid: "test-company",
        },
        organization: {
          tenantId,
          name: "Test Organization",
          uid: "test-organization",
        },
      } as OrganizationInitializeRequest)
      .expect(500)

    const organization = await organizationsRepo.findOne({
      where: {
        uid: "test-organization",
      },
    })
    expect(organization).toBeNull()

    const company = await companiesRepo.findOne({
      where: {
        uid: "test-company",
      },
      relations: ["organization"],
    })
    expect(company).toBeNull()
  })

  it("should fail on first step precondition check", async () => {
    const tenantId = newUuid()
    await request(httpServer)
      .post("/v1/appAdmin/organizationInitialize")
      .send({
        company: {
          name: "Test Company",
          uid: "test-company",
        },
        organization: {
          tenantId,
          name: "Test Organization",
          uid: "test-organization",
        },
      } as OrganizationInitializeRequest)
      .expect(412)

    const organization = await organizationsRepo.findOne({
      where: {
        uid: "test-organization",
      },
    })
    expect(organization).toBeNull()

    const company = await companiesRepo.findOne({
      where: {
        uid: "test-company",
      },
      relations: ["organization"],
    })
    expect(company).toBeNull()
  })
})
