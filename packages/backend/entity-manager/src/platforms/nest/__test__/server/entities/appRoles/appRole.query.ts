import {
  FindOptionsOrder,
  FindOptionsRelations,
  FindOptionsWhere,
  In,
} from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "../../../../../.."
import { AppRoleEntity } from "../../database/core/entities/appRole.entity"
import {
  AppRoleEntityId,
  AppRoleFacets,
  AppRoleSearchParameters,
  AppRoleSorting,
} from "./appRole.models"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appRole")
export class AppRoleQueryBuilder extends NestTypeOrmQueryBuilder<
  AppRoleEntity,
  AppRoleEntityId,
  AppRoleSearchParameters,
  AppRoleSorting,
  AppRoleFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appRole", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext | undefined
  ): FindOptionsWhere<AppRoleEntity> | FindOptionsWhere<AppRoleEntity>[] {
    return {}
  }

  protected buildSortingClause(
    request: AppRoleSearchParameters
  ): FindOptionsOrder<AppRoleEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppRoleSearchParameters,
    context?: AppAuthContext | undefined
  ): FindOptionsWhere<AppRoleEntity> | FindOptionsWhere<AppRoleEntity>[] {
    return {
      ...(request.filters?.uid ? { uid: In(request.filters.uid) } : {}),
    }
  }

  protected calculateFacets(
    request: AppRoleSearchParameters
  ): Promise<AppRoleFacets> {
    return Promise.resolve({})
  }

  protected getRelationsToLoad(
    request: AppRoleSearchParameters,
    context?: AppAuthContext | undefined
  ): FindOptionsRelations<AppRoleEntity> | undefined {
    return undefined
  }
}
