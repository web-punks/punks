import { faker } from "@faker-js/faker"
import { INestApplication } from "@nestjs/common"
import { DataSource } from "typeorm"
import { ApplicationAuthenticationContext } from "./types"
import { AppAuthContextProvider } from "../../infrastructure/authentication"
import { AuthGuard, AuthenticationService } from "../../../../extensions"
import { InstanceInitializeTemplate } from "../../app/appAdmin/templates/instance-initialize"
import { AppRoles } from "../../infrastructure/authentication/roles"
import { newUuid } from "@punks/backend-core"
import { AppTenantEntity } from "../../database/core/entities/appTenant.entity"
import { AppOrganizationEntity } from "../../database/core/entities/appOrganization.entity"
import { AppDirectoryEntity } from "../../database/core/entities/appDirectory.entity"
import {
  AuthUserContext,
  AuthUserRegistrationData,
} from "../../infrastructure/authentication/models"

export const mockAuthenticatedUser = (
  auth: AppAuthContextProvider,
  guard: AuthGuard
) => {
  jest.spyOn(auth, "getContext").mockImplementation(async () => ({
    isAnonymous: false,
    isAuthenticated: true,
  }))
  jest.spyOn(guard, "canActivate").mockImplementation(() => true)
}

export const createAuthenticationContext = async (
  app: INestApplication<any>,
  dataSource: DataSource
): Promise<ApplicationAuthenticationContext> => {
  await app.get(InstanceInitializeTemplate).invoke({
    roles: Object.values(AppRoles),
  })

  const tenantId = newUuid()
  const tenantsRepo = dataSource.getRepository(AppTenantEntity)
  const organizationsRepo = dataSource.getRepository(AppOrganizationEntity)
  const directoriesRepo = dataSource.getRepository(AppDirectoryEntity)

  await tenantsRepo.insert({
    id: tenantId,
    name: "Test Tenant",
    uid: "test-tenant",
  })
  const tenant = await tenantsRepo.findOneByOrFail({
    id: tenantId,
  })

  await directoriesRepo.insert({
    id: newUuid(),
    name: "Test Directory",
    uid: "test-directory",
    default: true,
    tenant,
  })

  const directory = await directoriesRepo.findOneByOrFail({
    uid: "test-directory",
  })

  const organizationId = newUuid()
  await organizationsRepo.insert({
    id: organizationId,
    name: "Test Organization",
    uid: "test-organization",
    tenant,
  })

  const organization = await organizationsRepo.findOneByOrFail({
    id: organizationId,
  })

  return {
    tenant,
    organization,
    directory,
  }
}

export const createUser = async (
  app: INestApplication<any>,
  context: ApplicationAuthenticationContext,
  data: {
    roleUid?: string
  }
) => {
  const auth = app.get(AuthenticationService)
  const email = faker.internet.email()
  const user = await auth.userCreate<AuthUserRegistrationData, AuthUserContext>(
    {
      email,
      password: newUuid(),
      userName: email,
      registrationInfo: {
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
        birthDate: faker.date.past().toISOString(),
      },
      context: {
        tenantUid: context.tenant.uid,
        organizationUid: context.organization.uid,
      },
    }
  )

  if (!user.success) {
    throw new Error("User creation failed")
  }

  if (data.roleUid) {
    await auth.userRolesService.addUserToRoleByUid(
      user.userId as string,
      data.roleUid
    )
  }

  return await auth.usersService.getById(user.userId as string)
}
