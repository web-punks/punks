import { ApiProperty } from "@nestjs/swagger"
import {
  AppDivisionSearchParameters,
  AppDivisionSearchResults,
} from "../../entities/appDivisions/appDivision.models"

export class AppDivisionDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  createdOn: Date

  @ApiProperty()
  updatedOn: Date
}

export class AppDivisionListItemDto {
  @ApiProperty()
  id: string
}

export class AppDivisionCreateDto {}

export class AppDivisionUpdateDto {
  @ApiProperty()
  id: string
}

export class AppDivisionSearchRequest {
  @ApiProperty()
  params: AppDivisionSearchParameters
}

export class AppDivisionSearchResponse extends AppDivisionSearchResults<AppDivisionListItemDto> {
  @ApiProperty({ type: [AppDivisionListItemDto] })
  items: AppDivisionListItemDto[]
}
