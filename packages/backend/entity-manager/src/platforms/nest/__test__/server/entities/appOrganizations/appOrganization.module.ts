import { Module } from "@nestjs/common"
import { AppOrganizationAdapter } from "./appOrganization.adapter"
import { AppOrganizationAuthMiddleware } from "./appOrganization.authentication"
import { AppOrganizationEntityManager } from "./appOrganization.manager"
import { AppOrganizationQueryBuilder } from "./appOrganization.query"
import { EntityManagerModule } from "../../../.."
import { AppOrganizationSerializer } from "./appOrganization.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppOrganizationAdapter,
    AppOrganizationAuthMiddleware,
    AppOrganizationEntityManager,
    AppOrganizationQueryBuilder,
    AppOrganizationSerializer,
  ],
  exports: [AppOrganizationEntityManager],
})
export class AppOrganizationEntityModule {}
