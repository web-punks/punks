import {
  IAuthUserRolesService,
  WpUserRolesService,
} from "../../../../../../extensions"
import { AppUserEntityManager } from "../../../../entities/appUsers/appUser.manager"
import { AppUserRoleEntityManager } from "../../../../entities/appUserRoles/appUserRole.manager"
import { AppRoleEntityManager } from "../../../../entities/appRoles/appRole.manager"
import { AppRoleEntity } from "../../../../database/core/entities/appRole.entity"
import { AppUserEntity } from "../../../../database/core/entities/appUser.entity"
import { AppPermissionEntity } from "../../../../database/core/entities/appPermission.entity"
import { AppOrganizationalUnitEntity } from "../../../../database/core/entities/appOrganizationalUnit.entity"

@WpUserRolesService()
export class AuthUserRolesService
  implements
    IAuthUserRolesService<
      AppUserEntity,
      AppRoleEntity,
      AppPermissionEntity,
      AppOrganizationalUnitEntity
    >
{
  constructor(
    private readonly rolesManager: AppRoleEntityManager,
    private readonly userManager: AppUserEntityManager,
    private readonly userRoleManager: AppUserRoleEntityManager
  ) {}

  async getUserOrganizationalUnits(
    userId: string
  ): Promise<AppOrganizationalUnitEntity[]> {
    // todo: implement
    return []
  }

  async getUserPermissions(userId: string): Promise<AppPermissionEntity[]> {
    // todo: implement
    return []
  }

  async getRoleUsers(roleId: string): Promise<AppUserEntity[]> {
    const { items } = await this.userRoleManager.manager.search.execute({
      filters: {
        roleIds: [roleId],
      },
    })
    return items.map((x) => x.user)
  }

  async getUserRoles(userId: string): Promise<AppRoleEntity[]> {
    const { items } = await this.userRoleManager.manager.search.execute({
      filters: {
        userId,
      },
    })
    return items.map((x) => x.role)
  }

  async getUserRolesByUid(roleUid: string): Promise<AppRoleEntity[]> {
    const role = await this.rolesManager.getByUid(roleUid)
    if (!role) {
      throw new Error(`Role ${roleUid} not found`)
    }

    return await this.getUserRoles(role.id)
  }

  async addUserToRole(userId: string, roleId: string): Promise<void> {
    const user = await this.userManager.manager.get.execute(userId)
    if (!user) {
      throw new Error(`User ${userId} not found`)
    }

    if (await this.isUserInRole(userId, roleId)) {
      return
    }

    await this.userRoleManager.manager.create.execute({
      user,
      role: {
        id: roleId,
      } as any,
    })
  }

  async addUserToRoleByUid(userId: string, roleUid: string): Promise<void> {
    const role = await this.rolesManager.getByUid(roleUid)
    if (!role) {
      throw new Error(`Role ${roleUid} not found`)
    }

    await this.addUserToRole(userId, role.id)
  }

  async removeUserFromRole(userId: string, roleId: string): Promise<void> {
    await this.userRoleManager.manager.deleteItems.execute({
      filters: {
        userId,
        roleIds: [roleId],
      },
    })
  }

  async removeUserFromRoleByUid(
    userId: string,
    roleUid: string
  ): Promise<void> {
    const role = await this.rolesManager.getByUid(roleUid)
    if (!role) {
      throw new Error(`Role ${roleUid} not found`)
    }

    await this.userRoleManager.manager.deleteItems.execute({
      filters: {
        userId,
        roleIds: [role.id],
      },
    })
  }

  async clearUserRoles(userId: string): Promise<void> {
    await this.userRoleManager.manager.deleteItems.execute({
      filters: {
        userId,
      },
    })
  }

  async isUserInRole(userId: string, roleId: string): Promise<boolean> {
    const userRoles = await this.getUserRoles(userId)
    return userRoles.some((x) => x.id === roleId)
  }
}
