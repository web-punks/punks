import { ApiProperty } from "@nestjs/swagger"
import { SortDirection } from "../../../../../../abstractions"

export class EntityVersionsReference {
  @ApiProperty()
  entityId: string
}

export class EntityVersionsFilters {
  @ApiProperty({ required: false })
  timestampFrom?: Date

  @ApiProperty({ required: false })
  timestampTo?: Date
}

export class EntityVersionsSorting {
  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class EntityVersionsCursor<TCursor> {
  @ApiProperty({ required: false })
  cursor?: TCursor

  @ApiProperty()
  pageSize: number
}
