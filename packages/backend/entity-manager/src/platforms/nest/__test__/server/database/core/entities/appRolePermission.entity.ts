import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from "typeorm"
import { AppPermissionEntity } from "./appPermission.entity"
import { AppRoleEntity } from "./appRole.entity"
import { WpEntity } from "../../../../../decorators"

export type AppRolePermissionEntityId = string

@Entity("appRolePermissions")
@WpEntity("appRolePermission")
export class AppRolePermissionEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppRolePermissionEntityId

  @ManyToOne(() => AppRoleEntity, {
    nullable: false,
  })
  role: AppRoleEntity

  @ManyToOne(() => AppPermissionEntity, {
    nullable: false,
  })
  permission: AppPermissionEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
