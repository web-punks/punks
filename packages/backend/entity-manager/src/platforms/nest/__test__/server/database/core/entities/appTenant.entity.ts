import {
  Entity,
  Column,
  CreateDateColumn,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "../../../../.."
import { AppOrganizationEntity } from "./appOrganization.entity"
import { AppUserEntity } from "./appUser.entity"
import { AppDirectoryEntity } from "./appDirectory.entity"

@Entity("appTenants")
@WpEntity("appTenant")
export class AppTenantEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255, unique: true })
  uid: string

  @Column({ type: "varchar", length: 255 })
  name: string

  @OneToMany(() => AppOrganizationEntity, (organization) => organization.tenant)
  organizations?: AppOrganizationEntity[]

  @OneToMany(() => AppUserEntity, (user) => user.tenant)
  users?: AppUserEntity[]

  @OneToMany(() => AppDirectoryEntity, (directory) => directory.tenant)
  directories?: AppDirectoryEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
