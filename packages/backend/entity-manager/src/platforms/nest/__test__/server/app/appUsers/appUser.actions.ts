import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppUserCreateDto,
  AppUserDto,
  AppUserListItemDto,
  AppUserUpdateDto,
} from "./appUser.dto"
import { AppUserEntity } from "../../database/core/entities/appUser.entity"
import {
  AppUserCursor,
  AppUserDeleteParameters,
  AppUserEntityId,
  AppUserFacets,
  AppUserSearchParameters,
  AppUserSorting,
} from "../../entities/appUsers/appUser.models"

@WpEntityActions("appUser")
export class AppUserActions extends NestEntityActions<
  AppUserEntity,
  AppUserEntityId,
  AppUserCreateDto,
  AppUserUpdateDto,
  AppUserDto,
  AppUserListItemDto,
  AppUserDeleteParameters,
  AppUserSearchParameters,
  AppUserSorting,
  AppUserCursor,
  AppUserFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appUser", registry)
  }
}
