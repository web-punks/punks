import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppUserProfileActions } from "./appUserProfile.actions"
import { AppUserProfileController } from "./appUserProfile.controller"
import { AppUserProfileConverter } from "./appUserProfile.converter"
import { AppUserProfileEntityModule } from "../../entities/appUserProfiles/appUserProfile.module"

@Module({
  imports: [SharedModule, AppUserProfileEntityModule],
  providers: [AppUserProfileActions, AppUserProfileConverter],
  controllers: [AppUserProfileController],
})
export class AppUserProfileAppModule {}
