import { Repository } from "typeorm"
import { InjectRepository } from "@nestjs/typeorm"
import { NestTypeOrmRepository, WpEntityRepository } from "../../../../.."
import { AppRoleEntity } from "../entities/appRole.entity"
import { AppRoleEntityId } from "../../../entities/appRoles/appRole.models"

@WpEntityRepository("appRole")
export class AppRoleRepository extends NestTypeOrmRepository<
  AppRoleEntity,
  AppRoleEntityId
> {
  constructor(
    @InjectRepository(AppRoleEntity, "core")
    repository: Repository<AppRoleEntity>
  ) {
    super(repository)
  }
}
