import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppCompanyCreateDto,
  AppCompanyDto,
  AppCompanyListItemDto,
  AppCompanyUpdateDto,
} from "./appCompany.dto"
import { AppCompanyEntity } from "../../database/core/entities/appCompany.entity"
import {
  AppCompanyCursor,
  AppCompanyDeleteParameters,
  AppCompanyEntityId,
  AppCompanyFacets,
  AppCompanySearchParameters,
  AppCompanySorting,
} from "../../entities/appCompanies/appCompany.models"

@WpEntityActions("appCompany")
export class AppCompanyActions extends NestEntityActions<
  AppCompanyEntity,
  AppCompanyEntityId,
  AppCompanyCreateDto,
  AppCompanyUpdateDto,
  AppCompanyDto,
  AppCompanyListItemDto,
  AppCompanyDeleteParameters,
  AppCompanySearchParameters,
  AppCompanySorting,
  AppCompanyCursor,
  AppCompanyFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appCompany", registry)
  }
}
