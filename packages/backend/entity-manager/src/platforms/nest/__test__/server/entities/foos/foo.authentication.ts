import { IAuthorizationResult } from "../../../../../../abstractions"
import { NestEntityAuthorizationMiddleware } from "../../../../authentication"
import { WpEntityAuthMiddleware } from "../../../../decorators"
import { FooEntity } from "../../database/core/entities/foo.entity"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication"

@WpEntityAuthMiddleware("foo")
export class FooAuthMiddleware extends NestEntityAuthorizationMiddleware<
  FooEntity,
  AppAuthContext,
  AppUserContext
> {
  canSearch(context: AppAuthContext): Promise<IAuthorizationResult> {
    return Promise.resolve({
      isAuthorized: context?.isAuthenticated ?? true,
    })
  }

  canRead(
    entity: Partial<FooEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return Promise.resolve({
      isAuthorized: context?.isAuthenticated ?? true,
    })
  }
  canCreate(
    entity: Partial<FooEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return Promise.resolve({
      isAuthorized: context?.isAuthenticated ?? true,
    })
  }
  canUpdate(
    entity: Partial<FooEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return Promise.resolve({
      isAuthorized: context?.isAuthenticated ?? true,
    })
  }
  canDelete(
    entity: Partial<FooEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return Promise.resolve({
      isAuthorized: context?.isAuthenticated ?? true,
    })
  }

  canDeleteItems(context: AppAuthContext): Promise<IAuthorizationResult> {
    return Promise.resolve({
      isAuthorized: context?.isAuthenticated ?? true,
    })
  }
}
