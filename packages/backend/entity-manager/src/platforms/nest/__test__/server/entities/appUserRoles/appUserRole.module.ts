import { Module } from "@nestjs/common"
import { EntityManagerModule } from "../../../.."
import { AppUserRoleAdapter } from "./appUserRole.adapter"
import { AppUserRoleAuthMiddleware } from "./appUserRole.authentication"
import { AppUserRoleEntityManager } from "./appUserRole.manager"
import { AppUserRoleQueryBuilder } from "./appUserRole.query"
import { AppUserRoleSerializer } from "./appUserRole.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppUserRoleAdapter,
    AppUserRoleAuthMiddleware,
    AppUserRoleEntityManager,
    AppUserRoleQueryBuilder,
    AppUserRoleSerializer,
  ],
  exports: [AppUserRoleEntityManager],
})
export class AppUserRoleEntityModule {}
