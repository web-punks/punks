import { IEntityConverter, WpEntityConverter } from "../../../../../.."
import {
  AppUserGroupCreateDto,
  AppUserGroupDto,
  AppUserGroupListItemDto,
  AppUserGroupUpdateDto,
} from "./appUserGroup.dto"
import { AppUserGroupEntity } from "../../database/core/entities/appUserGroup.entity"

@WpEntityConverter("appUserGroup")
export class AppUserGroupConverter
  implements
    IEntityConverter<
      AppUserGroupEntity,
      AppUserGroupDto,
      AppUserGroupListItemDto,
      AppUserGroupCreateDto,
      AppUserGroupUpdateDto
    >
{
  toListItemDto(entity: AppUserGroupEntity): AppUserGroupListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppUserGroupEntity): AppUserGroupDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppUserGroupCreateDto): Partial<AppUserGroupEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppUserGroupUpdateDto): Partial<AppUserGroupEntity> {
    return {
      ...input,
    }
  }
}
