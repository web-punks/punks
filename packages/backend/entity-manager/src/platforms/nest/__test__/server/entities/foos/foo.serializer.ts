import {
  EntityManagerRegistry,
  EntitySerializerSheetDefinition,
  NestEntitySerializer,
  WpEntitySerializer,
} from "../../../../../.."
import { FooEntity } from "../../database/core/entities/foo.entity"
import { AppAuthContext } from "../../infrastructure/authentication"
import {
  FooCreateData,
  FooCursor,
  FooEntityId,
  FooSheetItem,
  FooSorting,
  FooUpdateData,
} from "./foo.models"
import { FooSearchParameters } from "./foo.types"

@WpEntitySerializer("foo")
export class FooSerializer extends NestEntitySerializer<
  FooEntity,
  FooEntityId,
  FooCreateData,
  FooUpdateData,
  FooSearchParameters,
  FooSorting,
  FooCursor,
  FooSheetItem,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("foo", registry)
  }

  protected async loadEntities(
    filters: FooSearchParameters
  ): Promise<FooEntity[]> {
    const results = await this.manager.search.execute(filters)
    return results.items
  }

  protected async convertToSheetItems(
    entities: FooEntity[]
  ): Promise<FooSheetItem[]> {
    return entities.map((entity) => ({
      id: entity.id,
      name: entity.name,
      uid: entity.uid,
      age: entity.age,
      timestamp: entity.updatedOn,
      tenantUid: entity.tenant?.uid,
    }))
  }

  protected async importItem(item: FooSheetItem, context: AppAuthContext) {
    if (!item.id) {
      return await this.manager.create.execute({
        uid: item.uid,
        age: item.age,
        id: item.id,
        name: item.name,
      })
    }

    return await this.manager.update.execute(item.id, {
      uid: item.uid,
      age: item.age,
      id: item.id,
      name: item.name,
    })
  }

  protected async getDefinition(): Promise<
    EntitySerializerSheetDefinition<FooSheetItem>
  > {
    return {
      columns: [
        {
          name: "Id",
          key: "id",
          selector: "id",
        },
        {
          name: "Name",
          key: "name",
          selector: "name",
          parser: (value) => (value?.toString() as string).toUpperCase(),
          converter: (value) => (value?.toString() as string).toLowerCase(),
          sampleValue: "Foo",
        },
        {
          name: "Uid",
          key: "uid",
          selector: "uid",
          sampleValue: "foo",
        },
        {
          name: "Age",
          key: "age",
          selector: "age",
          parser: (value) => parseInt(value?.toString() as string),
          sampleValue: 18,
        },
      ],
    }
  }
}
