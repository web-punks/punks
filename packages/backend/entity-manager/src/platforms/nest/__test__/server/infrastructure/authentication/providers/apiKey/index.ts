import {
  IAuthApiKeysService,
  WpApiKeysService,
} from "../../../../../../extensions"
import { AppApiKeyEntity } from "../../../../database/core/entities/appApiKey.entity"
import { AppApiKeyEntityManager } from "../../../../entities/appApiKeys/appApiKey.manager"

@WpApiKeysService()
export class ApiKeysService implements IAuthApiKeysService<AppApiKeyEntity> {
  constructor(private readonly apiKeysManager: AppApiKeyEntityManager) {}

  async find(apiKey: string): Promise<AppApiKeyEntity | undefined> {
    return await this.apiKeysManager.manager.find.execute({
      filters: {
        key: {
          eq: apiKey,
        },
      },
      relations: {
        organization: {
          tenant: true,
        },
      },
    })
  }

  async create(data: {
    key: string
    name: string
    organizationId?: string | undefined
  }): Promise<AppApiKeyEntity> {
    const ref = await this.apiKeysManager.manager.create.execute({
      key: data.key,
      name: data.name,
      organizationId: data.organizationId,
    })
    return (await this.apiKeysManager.manager.get.execute(
      ref.id
    )) as AppApiKeyEntity
  }

  async enable(id: string): Promise<void> {
    await this.apiKeysManager.manager.update.execute(id, {
      disabled: false,
    })
  }

  async disable(id: string): Promise<void> {
    await this.apiKeysManager.manager.update.execute(id, {
      disabled: true,
    })
  }

  async delete(id: string): Promise<void> {
    await this.apiKeysManager.manager.delete.execute(id)
  }
}
