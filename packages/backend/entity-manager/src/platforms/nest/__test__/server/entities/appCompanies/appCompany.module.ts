import { Module } from "@nestjs/common"
import { AppCompanyAdapter } from "./appCompany.adapter"
import { AppCompanyAuthMiddleware } from "./appCompany.authentication"
import { AppCompanyEntityManager } from "./appCompany.manager"
import { AppCompanyQueryBuilder } from "./appCompany.query"
import { EntityManagerModule } from "../../../.."
import { AppCompanySerializer } from "./appCompany.serializer"

@Module({
  imports: [EntityManagerModule],
  providers: [
    AppCompanyAdapter,
    AppCompanyAuthMiddleware,
    AppCompanyEntityManager,
    AppCompanyQueryBuilder,
    AppCompanySerializer,
  ],
  exports: [AppCompanyEntityManager],
})
export class AppCompanyEntityModule {}
