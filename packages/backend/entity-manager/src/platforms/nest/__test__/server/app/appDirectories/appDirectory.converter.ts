import { IEntityConverter, WpEntityConverter } from "../../../../../.."
import {
  AppDirectoryCreateDto,
  AppDirectoryDto,
  AppDirectoryListItemDto,
  AppDirectoryUpdateDto,
} from "./appDirectory.dto"
import { AppDirectoryEntity } from "../../database/core/entities/appDirectory.entity"

@WpEntityConverter("appDirectory")
export class AppDirectoryConverter
  implements
    IEntityConverter<
      AppDirectoryEntity,
      AppDirectoryDto,
      AppDirectoryListItemDto,
      AppDirectoryCreateDto,
      AppDirectoryUpdateDto
    >
{
  toListItemDto(entity: AppDirectoryEntity): AppDirectoryListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: AppDirectoryEntity): AppDirectoryDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: AppDirectoryCreateDto): Partial<AppDirectoryEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: AppDirectoryUpdateDto): Partial<AppDirectoryEntity> {
    return {
      ...input,
    }
  }
}
