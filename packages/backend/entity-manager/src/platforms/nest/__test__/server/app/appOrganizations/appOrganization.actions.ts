import {
  EntityManagerRegistry,
  NestEntityActions,
  WpEntityActions,
} from "../../../../../.."
import {
  AppOrganizationCreateDto,
  AppOrganizationDto,
  AppOrganizationListItemDto,
  AppOrganizationUpdateDto,
} from "./appOrganization.dto"
import { AppOrganizationEntity } from "../../database/core/entities/appOrganization.entity"
import {
  AppOrganizationCursor,
  AppOrganizationDeleteParameters,
  AppOrganizationEntityId,
  AppOrganizationFacets,
  AppOrganizationSearchParameters,
  AppOrganizationSorting,
} from "../../entities/appOrganizations/appOrganization.models"

@WpEntityActions("appOrganization")
export class AppOrganizationActions extends NestEntityActions<
  AppOrganizationEntity,
  AppOrganizationEntityId,
  AppOrganizationCreateDto,
  AppOrganizationUpdateDto,
  AppOrganizationDto,
  AppOrganizationListItemDto,
  AppOrganizationDeleteParameters,
  AppOrganizationSearchParameters,
  AppOrganizationSorting,
  AppOrganizationCursor,
  AppOrganizationFacets
> {
  constructor(registry: EntityManagerRegistry) {
    super("appOrganization", registry)
  }
}
