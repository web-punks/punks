import { Module } from "@nestjs/common"
import { SharedModule } from "../../shared/module"
import { AppCompanyActions } from "./appCompany.actions"
import { AppCompanyController } from "./appCompany.controller"
import { AppCompanyConverter } from "./appCompany.converter"
import { AppCompanyEntityModule } from "../../entities/appCompanies/appCompany.module"

@Module({
  imports: [SharedModule, AppCompanyEntityModule],
  providers: [AppCompanyActions, AppCompanyConverter],
  controllers: [AppCompanyController],
})
export class AppCompanyAppModule {}
