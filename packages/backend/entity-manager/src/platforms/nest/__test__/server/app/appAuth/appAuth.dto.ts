import { ApiProperty } from "@nestjs/swagger"
import { UserRegistrationError } from "../../../../extensions"
import {
  AuthUserContext,
  AuthUserRegistrationData,
} from "../../infrastructure/authentication/models"

export class OperationCallbackTemplate {
  @ApiProperty()
  urlTemplate: string

  @ApiProperty()
  tokenPlaceholder: string
}

export class UserLoginRequest {
  @ApiProperty()
  userName: string

  @ApiProperty({ required: false })
  userContext?: AuthUserContext

  @ApiProperty()
  password: string
}

export class UserLoginResponse {
  @ApiProperty()
  success: boolean

  @ApiProperty({ required: false })
  token?: string
}

export class UserRegisterRequest {
  @ApiProperty()
  email: string

  @ApiProperty()
  userName: string

  @ApiProperty()
  password: string

  @ApiProperty()
  data: AuthUserRegistrationData

  @ApiProperty({ required: false })
  userContext?: AuthUserContext

  @ApiProperty()
  callback: OperationCallbackTemplate

  @ApiProperty()
  languageId: string
}

export class UserRegisterResponse {
  @ApiProperty()
  success: boolean

  @ApiProperty({ enum: UserRegistrationError, required: false })
  error?: UserRegistrationError
}

export class UserPasswordResetRequest {
  @ApiProperty()
  userName: string

  @ApiProperty({ required: false })
  userContext?: AuthUserContext

  @ApiProperty()
  callback: OperationCallbackTemplate

  @ApiProperty()
  languageCode: string
}

export class UserPasswordResetCompleteRequest {
  @ApiProperty()
  token: string

  @ApiProperty()
  newPassword: string
}

export class UserEmailVerifyCompleteRequest {
  @ApiProperty()
  token: string
}

export class UserEmailVerifyRequest {
  @ApiProperty()
  email: string

  @ApiProperty({ required: false })
  userContext?: AuthUserContext

  @ApiProperty()
  callback: OperationCallbackTemplate

  @ApiProperty()
  languageId: string
}
