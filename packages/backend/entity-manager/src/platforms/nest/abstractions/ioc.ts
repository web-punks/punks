import { DiscoveredClassWithMeta, MetaKey } from "../ioc/discovery"

export interface IDiscoveryService {
  providersWithMetaAtKey<T>(
    metaKey: MetaKey
  ): Promise<DiscoveredClassWithMeta<T>[]>
}
