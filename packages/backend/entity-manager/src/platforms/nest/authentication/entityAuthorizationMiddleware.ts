import {
  IAuthenticationContext,
  IAuthorizationResult,
  IEntityAuthorizationMiddleware,
} from "../../../abstractions"
import { PermissionsChecker } from "../../../integrations/repository/typeorm/permissionsChecker"

export abstract class NestEntityAuthorizationMiddleware<
  TEntity,
  TAuthenticationContext extends IAuthenticationContext<TUserContext>,
  TUserContext
> implements
    IEntityAuthorizationMiddleware<
      TEntity,
      TAuthenticationContext,
      TUserContext
    >
{
  protected permissions = new PermissionsChecker<TUserContext>()

  abstract canSearch(
    context: TAuthenticationContext
  ): Promise<IAuthorizationResult>

  abstract canRead(
    entity: Partial<TEntity>,
    context: TAuthenticationContext
  ): Promise<IAuthorizationResult>

  abstract canCreate(
    entity: Partial<TEntity>,
    context: TAuthenticationContext
  ): Promise<IAuthorizationResult>

  abstract canUpdate(
    entity: Partial<TEntity>,
    context: TAuthenticationContext
  ): Promise<IAuthorizationResult>

  abstract canDelete(
    entity: Partial<TEntity>,
    context: TAuthenticationContext
  ): Promise<IAuthorizationResult>

  abstract canDeleteItems(
    context: TAuthenticationContext
  ): Promise<IAuthorizationResult>
}
