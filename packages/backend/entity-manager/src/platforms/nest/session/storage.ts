import { AsyncLocalStorage } from "async_hooks"
import { Request, Response } from "express"
import { AppSession } from "./session"

export const sessionStorage = new AsyncLocalStorage<
  AppSession<Request, Response>
>()
