export class AppSession<TRequest, TResponse> {
  private readonly _request: TRequest
  private readonly _response: TResponse
  private readonly _map = new Map<string, any>()

  constructor(request: TRequest, response: TResponse) {
    this._request = request
    this._response = response
  }

  get<T>(key: string): T {
    return this._map.get(key)
  }

  set<T>(key: string, value: T) {
    this._map.set(key, value)
  }

  delete(key: string) {
    this._map.delete(key)
  }

  clear() {
    this._map.clear()
  }

  get request() {
    return this._request
  }

  get response() {
    return this._response
  }
}
