import { Request, Response, NextFunction } from "express"
import { Injectable, NestMiddleware } from "@nestjs/common"
import { sessionStorage } from "./storage"
import { AppSession } from "./session"

@Injectable()
export class AppSessionMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    sessionStorage.run(new AppSession(req, res), () => {
      next()
    })
  }
}

export function appSessionMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
) {
  sessionStorage.run(new AppSession(req, res), () => {
    next()
  })
}
