import { Module, OnModuleInit } from "@nestjs/common"
import { CustomDiscoveryModule } from "./ioc/discovery"
import { Services } from "./services/providers"
import { EntityManagerRegistry } from "./ioc/registry"
import { IoC } from "./ioc/providers"
import { Processors } from "./processors/providers"
import { EventEmitterModule } from "@nestjs/event-emitter"
import { Providers } from "./providers"
import { PipelineProviders } from "./pipelines/providers"
import { initializeIoCContext } from "./ioc/storage"

const ModuleData = {
  imports: [CustomDiscoveryModule, EventEmitterModule],
  providers: [
    ...IoC,
    ...Providers,
    ...Processors,
    ...Services,
    ...PipelineProviders,
  ],
  exports: [EntityManagerRegistry, ...Services],
}

@Module({
  imports: ModuleData.imports,
  providers: ModuleData.providers,
  exports: ModuleData.exports,
})
export class EntityManagerModule implements OnModuleInit {
  constructor(private readonly registry: EntityManagerRegistry) {}

  onModuleInit() {
    initializeIoCContext({
      registry: this.registry,
    })
  }
}
