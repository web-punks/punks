import { INestApplicationContext, Injectable } from "@nestjs/common"
import { createContainer } from "../../../../configuration"
import {
  EntityAuthMiddlewareProps,
  EntityConnectorMapperProps,
  EntityConnectorProps,
  EntityConverterProps,
  EntityProps,
  EntityQueryBuilderProps,
  EntityRepositoryProps,
  EntitySerializerProps,
  EntitySnapshotServiceProps,
} from "../../decorators"
import {
  EntityManagerSettings,
  IEntitiesQueryBuilder,
  IEntityAuthorizationMiddleware,
  IEntityConverter,
  IEntityMapper,
  IEntityRepository,
  IEntitySerializer,
  IEntitySnapshotService,
} from "../../../../abstractions"
import { EntityServiceLocator } from "../../../../providers/services"
import { DiscoveredClassWithMeta } from "../discovery"
import { IEntityAdapter } from "../../../../abstractions/adapters"
import { EntityAdapterProps } from "../../decorators/adapter"
import { IEventEmitter } from "../../../../abstractions/events"
import { IEntityConnector } from "../../../../abstractions/connectors"
import { toDict } from "@punks/backend-core"

@Injectable()
export class EntityManagerRegistry {
  private readonly container = createContainer()

  getContainer() {
    return this.container
  }

  async registerGlobalServices({
    eventEmitter,
  }: {
    eventEmitter?: IEventEmitter
  }) {
    if (eventEmitter) {
      this.container
        .getEntitiesServicesLocator()
        .registerEventEmitter(eventEmitter)
    }
  }

  async registerDiscoveredEntity(
    app: INestApplicationContext,
    {
      adapter,
      entityName,
      entity,
      converter,
      queryBuilder,
      repository,
      authMiddleware,
      serializer,
      snapshotService,
      connectors,
      connectorMappers,
      settings,
    }: {
      entityName: string
      entity: DiscoveredClassWithMeta<EntityProps>
      repository: DiscoveredClassWithMeta<EntityRepositoryProps>
      converter: DiscoveredClassWithMeta<EntityConverterProps>
      adapter: DiscoveredClassWithMeta<EntityAdapterProps>
      queryBuilder: DiscoveredClassWithMeta<EntityQueryBuilderProps>
      serializer: DiscoveredClassWithMeta<EntitySerializerProps>
      snapshotService?: DiscoveredClassWithMeta<EntitySnapshotServiceProps>
      authMiddleware?: DiscoveredClassWithMeta<EntityAuthMiddlewareProps>
      connectors?: DiscoveredClassWithMeta<EntityConnectorProps>[]
      connectorMappers?: DiscoveredClassWithMeta<EntityConnectorMapperProps>[]
      settings: EntityManagerSettings
    }
  ) {
    if (!repository.discoveredClass.injectType) {
      throw new Error(
        `No inject type found for entity repository: ${entityName}`
      )
    }
    const repositoryInstance = (await app.resolve(
      repository.discoveredClass.injectType
    )) as IEntityRepository<
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown
    >

    const registration = this.container.registerEntity(
      entity.meta,
      repositoryInstance
    )

    if (serializer?.discoveredClass?.injectType) {
      const serializerInstance = (await app.resolve(
        serializer.discoveredClass.injectType
      )) as IEntitySerializer<unknown, unknown, unknown, unknown>
      registration.addSerializer(serializerInstance)
    }

    if (!queryBuilder.discoveredClass.injectType) {
      throw new Error(
        `No inject type found for entity query builder: ${entityName}`
      )
    }

    const queryBuilderInstance = (await app.resolve(
      queryBuilder.discoveredClass.injectType
    )) as IEntitiesQueryBuilder<any, unknown, any, any, any, any, unknown>
    registration.mapCrudOperations({
      queryBuilder: queryBuilderInstance,
      settings,
    })

    const converterInstance = converter?.discoveredClass.injectType
      ? ((await app.resolve(
          converter.discoveredClass.injectType
        )) as IEntityConverter<unknown, unknown, unknown, unknown, unknown>)
      : undefined
    if (converterInstance) {
      registration.addConverter(converterInstance)
    }

    const adapterInstance = adapter?.discoveredClass.injectType
      ? ((await app.resolve(
          adapter.discoveredClass.injectType
        )) as IEntityAdapter<unknown, unknown, unknown>)
      : undefined
    if (adapterInstance) {
      registration.addAdapter(adapterInstance)
    }

    const snapshotServiceInstance = snapshotService?.discoveredClass.injectType
      ? ((await app.resolve(
          snapshotService.discoveredClass.injectType
        )) as IEntitySnapshotService<unknown, unknown>)
      : undefined
    if (snapshotServiceInstance) {
      registration.addSnapshotService(snapshotServiceInstance)
    }

    const authMiddlewareInstance = authMiddleware?.discoveredClass.injectType
      ? ((await app.resolve(
          authMiddleware.discoveredClass.injectType
        )) as IEntityAuthorizationMiddleware<unknown, any, unknown>)
      : undefined
    if (authMiddlewareInstance) {
      registration.addAuthorization({
        middleware: authMiddlewareInstance,
      })
    }

    const mappers = toDict(connectorMappers ?? [], (x) => x.meta.connectorName)
    for (const connector of connectors ?? []) {
      if (!connector.discoveredClass.injectType) {
        throw new Error(
          `No inject type found for entity connector: ${entityName}`
        )
      }

      const mapper = mappers[connector.meta.connectorName]
      if (!mapper) {
        throw new Error(
          `No connector mapper found for entity connector: entityName:${entityName} - connectorName:${connector.meta.connectorName}`
        )
      }

      if (!mapper.discoveredClass.injectType) {
        throw new Error(
          `No inject type found for entity connector mapper: ${entityName} - ${connector.meta.connectorName}`
        )
      }

      const connectorInstance = connector.discoveredClass
        .instance as IEntityConnector<unknown, unknown, unknown>
      const mapperInstance = mapper.discoveredClass.instance as IEntityMapper<
        unknown,
        unknown
      >

      registration.withSynchronization({
        name: connector.meta.connectorName,
        mapper: mapperInstance,
        connector: connectorInstance,
        options: {
          mode: connector.meta.mode,
          enabled: connector.meta.enabled,
        },
      })
    }

    // todo: configure replica
  }

  resolveEntityServicesCollection<TEntity>(
    entityName: string
  ): EntityServiceLocator<TEntity, unknown> {
    return this.container.getEntityServicesLocator(entityName)
  }
}
