// import { AsyncLocalStorage } from "async_hooks"
import { EntityManagerRegistry } from "./registry"

export type IoCContext = {
  registry: EntityManagerRegistry
}

// export const iocStorage = new AsyncLocalStorage<IocContext>()

let _context: IoCContext

export const initializeIoCContext = (context: IoCContext) => {
  _context = context
}

export const getIoCContext = () => {
  if (!_context) {
    throw new Error("IoC context not initialized")
  }
  return _context
}
