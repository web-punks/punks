const LIB_DI_TOKEN_NAMESPACE = "WP"

export const buildProviderToken = (name: string) =>
  `${LIB_DI_TOKEN_NAMESPACE}.PROVIDER.${name}`
