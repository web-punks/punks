import { trimStart } from "lodash"
import { buildProviderToken } from "./builder"

export const EntityManagerProviderToken = {
  DiscoveryService: {},
  OperationsLockRepository: {},
  JobInstancesRepository: {},
  JobDefinitionsRepository: {},
}

const toSnakeUpperCase = (str: string) =>
  trimStart(str.replace(/([A-Z])/g, (match) => `_${match}`).toUpperCase(), "_")

export const getEntityManagerProviderToken = (
  name: keyof typeof EntityManagerProviderToken
) => buildProviderToken(toSnakeUpperCase(name))
