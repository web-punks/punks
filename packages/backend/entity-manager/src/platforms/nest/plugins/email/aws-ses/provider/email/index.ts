import { SESClient, SendEmailCommand } from "@aws-sdk/client-ses"
import {
  EmailSendOptions,
  HtmlEmailInput,
  IEmailProvider,
  IEmailTemplate,
  TemplatedEmailInput,
} from "../../../../../../../abstractions"
import { WpEmailProvider } from "../../../../../decorators/email"
import { awsSesSettings, AwsSesSettings } from "../../settings"
import { renderHandlebarsTemplate } from "../../../../../../../utils"
import { AwsSesEmailTemplateData } from "../../abstractions"

const createClient = (settings: AwsSesSettings) =>
  new SESClient({
    region: settings.region,
    credentials:
      settings.awsAccessKeyId && settings.awsSecretAccessKey
        ? {
            accessKeyId: settings.awsAccessKeyId,
            secretAccessKey: settings.awsSecretAccessKey,
          }
        : undefined,
  })

@WpEmailProvider("awsSes")
export class AwsSesEmailProvider
  implements IEmailProvider<AwsSesEmailTemplateData>
{
  private readonly client: SESClient

  constructor() {
    this.client = createClient(awsSesSettings.value)
  }

  async sendTemplatedEmail<TPayload, TAugmentedPayload>(
    input: TemplatedEmailInput<TPayload>,
    template: IEmailTemplate<
      AwsSesEmailTemplateData,
      TPayload,
      TAugmentedPayload
    >,
    options?: EmailSendOptions
  ): Promise<void> {
    const processedPayload = await template.processPayload(input.payload)
    const templateData = await template.getTemplateData(processedPayload)

    await this.sendHtmlEmail({
      bodyTemplate: templateData.bodyTemplate,
      payload: processedPayload,
      subjectTemplate: templateData.subjectTemplate,
      to: input.to ?? templateData.to,
      bcc: input.bcc ?? templateData.bcc,
      cc: input.cc ?? templateData.cc,
      from: input.from ?? templateData.from,
      attachments: input.attachments,
    })
  }

  async sendHtmlEmail<TPayload>(
    input: HtmlEmailInput<TPayload>,
    options?: EmailSendOptions
  ): Promise<void> {
    await this.client.send(
      new SendEmailCommand({
        Source: input.from ?? input.from ?? awsSesSettings.value.defaultSender,
        Destination: {
          ToAddresses: input.to ?? input.to ?? [],
          CcAddresses: input.cc ?? awsSesSettings.value.defaultCc ?? [],
          BccAddresses: input.bcc ?? awsSesSettings.value.defaultBcc ?? [],
        },
        Message: {
          Subject: {
            Data: renderHandlebarsTemplate({
              template: input.subjectTemplate,
              context: input.payload as object,
            }),
          },
          Body: {
            Html: {
              Data: renderHandlebarsTemplate({
                template: input.bodyTemplate,
                context: input.payload as object,
              }),
            },
          },
        },
      })
    )
  }
}
