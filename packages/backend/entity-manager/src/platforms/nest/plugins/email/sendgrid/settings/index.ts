import { AppInMemorySettings } from "../../../../../../settings"

export type SendgridSettings = {
  apiKey: string
  defaultSender: string
  defaultReplyTo?: string
  defaultCc?: string[]
  defaultBcc?: string[]
  loggingEnabled?: boolean
  sandboxMode?: boolean
  forceDelivery?: boolean
}

export const sendgridSettings = new AppInMemorySettings<SendgridSettings>(
  "sendgridSettings"
)
