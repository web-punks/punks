import { IEmailTemplate } from "../../../../../../abstractions"
import { AwsSesEmailTemplateData } from "../abstractions"
import { AwsSesModuleSymbols } from "../symbols"
import { getInstanceDecoratorData } from "../../../../../../reflection/decorators"

export abstract class AwsSesEmailTemplate<
  TPayload,
  TAugmentedPayload = TPayload
> implements
    IEmailTemplate<AwsSesEmailTemplateData, TPayload, TAugmentedPayload>
{
  async processPayload(payload: TPayload): Promise<TAugmentedPayload> {
    return payload as any
  }

  async getTemplateData(
    payload: TAugmentedPayload
  ): Promise<AwsSesEmailTemplateData> {
    return getInstanceDecoratorData(
      AwsSesModuleSymbols.EmailTemplate,
      this
    ) as AwsSesEmailTemplateData
  }
}
