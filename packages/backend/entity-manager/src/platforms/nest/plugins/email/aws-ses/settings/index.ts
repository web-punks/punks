import { AppInMemorySettings } from "../../../../../../settings"

export type AwsSesSettings = {
  awsAccessKeyId?: string
  awsSecretAccessKey?: string
  region?: string
  defaultSender: string
  defaultCc?: string[]
  defaultBcc?: string[]
}

export const awsSesSettings = new AppInMemorySettings<AwsSesSettings>(
  "awsSesSettings"
)
