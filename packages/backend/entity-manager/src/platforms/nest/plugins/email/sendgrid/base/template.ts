import { IEmailTemplate } from "../../../../../../abstractions"
import { SendgridEmailTemplateData } from "../abstractions"
import { SendgridModuleSymbols } from "../symbols"
import { getInstanceDecoratorData } from "../../../../../../reflection/decorators"

export abstract class SendgridEmailTemplate<
  TPayload,
  TAugmentedPayload = TPayload
> implements
    IEmailTemplate<SendgridEmailTemplateData, TPayload, TAugmentedPayload>
{
  async processPayload(payload: TPayload): Promise<TAugmentedPayload> {
    return payload as any
  }

  async getTemplateData(
    payload: TAugmentedPayload
  ): Promise<SendgridEmailTemplateData> {
    return getInstanceDecoratorData(
      SendgridModuleSymbols.EmailTemplate,
      this
    ) as SendgridEmailTemplateData
  }
}
