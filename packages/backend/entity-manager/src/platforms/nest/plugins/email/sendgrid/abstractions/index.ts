import { LocalizedTexts } from "../../../../../../abstractions"

export type SendgridTemplateType = "template" | "html"

export type SendgridTemplateBaseData = {
  from?: string
  to?: string[]
  cc?: string[]
  bcc?: string[]
  replyTo?: string
}

export type SendgridEmailTemplateData = SendgridTemplateBaseData &
  (
    | {
        type: "template"
        sendgridTemplateId: string
      }
    | {
        type: "html"
        subjectTemplate: string | LocalizedTexts
        bodyTemplate: string | LocalizedTexts
      }
  )
