import { Log, removeUndefinedProps } from "@punks/backend-core"
import { MailDataRequired, MailService } from "@sendgrid/mail"
import {
  EmailSendOptions,
  HtmlEmailInput,
  IEmailProvider,
  IEmailTemplate,
  TemplatedEmailInput,
} from "../../../../../../../abstractions"
import { WpEmailProvider } from "../../../../../decorators/email"
import { sendgridSettings } from "../../settings"
import {
  getLocalizedText,
  renderHandlebarsTemplate,
} from "../../../../../../../utils"
import { SendgridEmailTemplateData } from "../../abstractions"
import { EventsService } from "../../../../../services"
import {
  PlatformEvents,
  MessagingEmailSentPayload,
} from "../../../../../../../events"

const sanitizeValue = (value: string | undefined): string | undefined =>
  value?.trim() ? value?.trim() : undefined

const sanitizeArray = (value: string[] | undefined): string[] | undefined =>
  (value?.length ?? 0) > 0 ? value : undefined

@WpEmailProvider("sendgrid")
export class SendgridEmailProvider
  implements IEmailProvider<SendgridEmailTemplateData>
{
  private readonly client: MailService
  private readonly logger = Log.getLogger("Sendgrid")

  constructor(private readonly eventsService: EventsService) {
    this.client = new MailService()
    this.client.setApiKey(sendgridSettings.value.apiKey)
  }

  async sendTemplatedEmail<TPayload, TAugmentedPayload>(
    input: TemplatedEmailInput<TPayload>,
    template: IEmailTemplate<
      SendgridEmailTemplateData,
      TPayload,
      TAugmentedPayload
    >,
    options?: EmailSendOptions
  ): Promise<void> {
    if (!input.to?.length && !input.cc?.length && !input.bcc?.length) {
      throw new Error(`No recipient specified for email ${input.templateId}`)
    }

    const processedPayload = await template.processPayload(input.payload)
    const templateData = await template.getTemplateData(processedPayload)

    if (templateData.type === "html") {
      const body = getLocalizedText(
        templateData.bodyTemplate,
        input.languageCode
      )
      if (!body) {
        throw new Error(
          `Missing body template for language ${input.languageCode}`
        )
      }

      const subject = getLocalizedText(
        templateData.subjectTemplate,
        input.languageCode
      )
      if (!subject) {
        throw new Error(
          `Missing subject template for language ${input.languageCode}`
        )
      }

      await this.sendHtmlEmail(
        {
          bodyTemplate: body,
          subjectTemplate: subject,
          payload: processedPayload,
          bcc: input.bcc ?? templateData.bcc,
          cc: input.cc ?? templateData.cc,
          from: input.from ?? templateData.from,
          to: input.to ?? templateData.to,
          attachments: input.attachments,
        },
        options
      )
      return
    }

    const request = removeUndefinedProps({
      from:
        input.from ?? templateData.from ?? sendgridSettings.value.defaultSender,
      to: sanitizeArray(input.to ?? templateData.to),
      cc: sanitizeArray([
        ...(input.cc ?? templateData.cc ?? []),
        ...(sendgridSettings.value.defaultCc ?? []),
      ]),
      bcc: sanitizeArray([
        ...(input.bcc ?? templateData.bcc ?? []),
        ...(sendgridSettings.value.defaultBcc ?? []),
      ]),
      subject: input.subjectTemplate
        ? renderHandlebarsTemplate({
            template: input.subjectTemplate,
            context: processedPayload as object,
          })
        : undefined,
      replyTo: sanitizeValue(
        templateData.replyTo ?? sendgridSettings.value.defaultReplyTo
      ),
      dynamicTemplateData: {
        ...(processedPayload as any),
      },
      templateId: templateData.sendgridTemplateId,
      attachments: input.attachments?.map((attachment) => ({
        ...attachment,
        content:
          attachment.content instanceof Buffer
            ? attachment.content.toString("base64")
            : attachment.content,
      })),
      mailSettings: {
        ...(options?.sandboxMode || sendgridSettings.value.sandboxMode
          ? {
              sandboxMode: {
                enable: true,
              },
            }
          : {}),
        ...(options?.forceDelivery || sendgridSettings.value.forceDelivery
          ? {
              bypassBounceManagement: {
                enable: true,
              },
              bypassUnsubscribeManagement: {
                enable: true,
              },
              bypassSpamManagement: {
                enable: true,
              },
            }
          : {}),
      },
    } satisfies MailDataRequired)

    if (sendgridSettings.value.loggingEnabled) {
      this.logger.info("Sending templated email", {
        input,
        request,
      })
    }

    await this.client.send(request)
    await this.eventsService.emitEvent<MessagingEmailSentPayload>(
      PlatformEvents.Messaging.EmailSent,
      {
        payload: {
          from: request.from,
          to: request.to,
          cc: request.cc,
          bcc: request.bcc,
          payload: processedPayload,
          request,
        },
      }
    )

    if (sendgridSettings.value.loggingEnabled) {
      this.logger.info("Templated email sent", {
        input,
        request,
      })
    }
  }

  async sendHtmlEmail<TPayload>(
    input: HtmlEmailInput<TPayload>,
    options?: EmailSendOptions
  ): Promise<void> {
    const request = removeUndefinedProps({
      from: input.from ?? sendgridSettings.value.defaultSender,
      to: sanitizeArray(input.to),
      cc: sanitizeArray(input.cc),
      bcc: sanitizeArray([
        ...(input.bcc ?? []),
        ...(sendgridSettings.value.defaultBcc ?? []),
      ]),
      subject: renderHandlebarsTemplate({
        template: input.subjectTemplate,
        context: input.payload as object,
      }),
      html: renderHandlebarsTemplate({
        template: input.bodyTemplate,
        context: input.payload as object,
      }),
      replyTo: sanitizeValue(
        input.replyTo ?? sendgridSettings.value.defaultReplyTo
      ),
      attachments: input.attachments?.map((attachment) => ({
        ...attachment,
        content:
          attachment.content instanceof Buffer
            ? attachment.content.toString("base64")
            : attachment.content,
      })),
      mailSettings: {
        ...(options?.sandboxMode || sendgridSettings.value.sandboxMode
          ? {
              sandboxMode: {
                enable: true,
              },
            }
          : {}),
        ...(options?.forceDelivery || sendgridSettings.value.forceDelivery
          ? {
              bypassBounceManagement: {
                enable: true,
              },
              bypassUnsubscribeManagement: {
                enable: true,
              },
              bypassSpamManagement: {
                enable: true,
              },
            }
          : {}),
      },
    } satisfies MailDataRequired)

    if (sendgridSettings.value.loggingEnabled) {
      this.logger.info("Sending html email", {
        input,
        request,
      })
    }

    await this.client.send(request)
    await this.eventsService.emitEvent<MessagingEmailSentPayload>(
      PlatformEvents.Messaging.EmailSent,
      {
        payload: {
          from: request.from,
          to: request.to,
          cc: request.cc,
          bcc: request.bcc,
          body: request.html,
          subject: request.subject,
          request,
        },
      }
    )

    if (sendgridSettings.value.loggingEnabled) {
      this.logger.info("Html email sent", {
        input,
        request,
      })
    }
  }
}
