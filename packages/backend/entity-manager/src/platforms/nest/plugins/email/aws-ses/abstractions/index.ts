export type AwsSesEmailTemplateData = {
  type: "html"
  subjectTemplate: string
  bodyTemplate: string
  from?: string
  to?: string[]
  cc?: string[]
  bcc?: string[]
  replyTo?: string
}
