import { DynamicModule, Module } from "@nestjs/common"
import { AwsSesEmailProvider } from "./provider/email"
import { AwsSesSettings, awsSesSettings } from "./settings"

const ModuleData = {
  providers: [AwsSesEmailProvider],
}

@Module({
  ...ModuleData,
})
export class AwsEmailModule {
  /**
   * @deprecated use initialize instead
   */
  static forRoot(input: AwsSesSettings): DynamicModule {
    awsSesSettings.initialize(input)
    return {
      module: AwsEmailModule,
      ...ModuleData,
    }
  }

  static initialize(input: AwsSesSettings) {
    awsSesSettings.initialize(input)
  }
}
