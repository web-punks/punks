import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import {
  EmailTemplateProps,
  EntityManagerSymbols,
} from "../../../../decorators"
import { SendgridEmailTemplateData } from "../abstractions"
import { SendgridModuleSymbols } from "../symbols"

export const WpSendgridEmailTemplate = (
  templateId: string,
  sendgridTemplateData: SendgridEmailTemplateData
) =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, EmailTemplateProps>(EntityManagerSymbols.EmailTemplate, {
      templateId,
    }),
    SetMetadata<any, SendgridEmailTemplateData>(
      SendgridModuleSymbols.EmailTemplate,
      sendgridTemplateData
    )
  )
