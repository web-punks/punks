import { DynamicModule, Module } from "@nestjs/common"
import { SendgridEmailProvider } from "./provider/email"
import { SendgridSettings, sendgridSettings } from "./settings"
import { EntityManagerModule } from "../../../module"

const ModuleData = {
  imports: [EntityManagerModule],
  providers: [SendgridEmailProvider],
}

@Module({
  ...ModuleData,
})
export class SendgridEmailModule {
  /**
   * @deprecated use initialize instead
   */
  static forRoot(input: SendgridSettings): DynamicModule {
    sendgridSettings.initialize(input)
    return {
      module: SendgridEmailModule,
      ...ModuleData,
    }
  }

  static initialize(input: SendgridSettings) {
    sendgridSettings.initialize(input)
  }
}
