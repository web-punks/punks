import {
  EmailSendOptions,
  HtmlEmailInput,
  IEmailProvider,
  IEmailTemplate,
  TemplatedEmailInput,
} from "../../../../../abstractions"
import { WpEmailProvider } from "../../../decorators/email"

type SentEmail =
  | {
      input: TemplatedEmailInput<unknown>
      template: IEmailTemplate<void, unknown, unknown>
      type: "templated"
    }
  | {
      input: HtmlEmailInput<unknown>
      type: "html"
    }

@WpEmailProvider("in-memory")
export class InMemoryEmailProvider implements IEmailProvider<void> {
  private readonly sentEmails: Array<SentEmail> = []

  async sendTemplatedEmail<TPayload, TAugmentedPayload>(
    input: TemplatedEmailInput<TPayload>,
    template: IEmailTemplate<void, TPayload, TAugmentedPayload>,
    options?: EmailSendOptions
  ): Promise<void> {
    this.sentEmails.push({
      input,
      template,
      type: "templated",
    })
  }

  async sendHtmlEmail<TPayload>(
    input: HtmlEmailInput<TPayload>
  ): Promise<void> {
    this.sentEmails.push({
      input,
      type: "html",
    })
  }

  getSentEmails() {
    return this.sentEmails
  }
}
