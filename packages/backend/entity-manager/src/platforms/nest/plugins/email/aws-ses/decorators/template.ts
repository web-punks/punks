import { Injectable, SetMetadata, applyDecorators } from "@nestjs/common"
import {
  EmailTemplateProps,
  EntityManagerSymbols,
} from "../../../../decorators"
import { AwsSesEmailTemplateData } from "../abstractions"
import { AwsSesModuleSymbols } from "../symbols"

export const WpAwsSesEmailTemplate = (
  templateId: string,
  templateData: AwsSesEmailTemplateData
) =>
  applyDecorators(
    Injectable(),
    SetMetadata<any, EmailTemplateProps>(EntityManagerSymbols.EmailTemplate, {
      templateId,
    }),
    SetMetadata<any, AwsSesEmailTemplateData>(
      AwsSesModuleSymbols.EmailTemplate,
      templateData
    )
  )
