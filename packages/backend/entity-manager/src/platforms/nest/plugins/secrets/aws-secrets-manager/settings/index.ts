import { AppInMemorySettings } from "../../../../../../settings"

export type AwsSecretsSettings = {
  awsAccessKeyId?: string
  awsSecretAccessKey?: string
  region?: string
  secretsRootPath: string
}

export const awsSecretsSettings = new AppInMemorySettings<AwsSecretsSettings>(
  "awsSecretsSettings"
)
