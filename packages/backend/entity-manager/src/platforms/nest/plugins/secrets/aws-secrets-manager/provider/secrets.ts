import {
  CreateSecretCommand,
  DescribeSecretCommand,
  GetSecretValueCommand,
  SecretsManagerClient,
  UpdateSecretCommand,
} from "@aws-sdk/client-secrets-manager"
import {
  AppSecret,
  AppSecretDefinition,
  AppSecretInput,
  AppSecretsPageMetadata,
  ISecretsProvider,
} from "../../../../../../abstractions"
import { AwsSecretsSettings, awsSecretsSettings } from "../settings"
import { WpSecretsProvider } from "../../../../decorators/secrets"
import { Log } from "@punks/backend-core"

const createClient = (settings: AwsSecretsSettings) =>
  new SecretsManagerClient({
    region: settings.region,
    credentials:
      settings.awsAccessKeyId && settings.awsSecretAccessKey
        ? {
            accessKeyId: settings.awsAccessKeyId,
            secretAccessKey: settings.awsSecretAccessKey,
          }
        : undefined,
  })

type SecretsPage = {
  [key: string]: Omit<AppSecret<any>, "key">
}

@WpSecretsProvider("awsSecretsManager")
export class AwsSecretsProvider implements ISecretsProvider {
  private readonly client: SecretsManagerClient
  private readonly logger = Log.getLogger("AwsSecretsManager")

  constructor() {
    this.client = createClient(awsSecretsSettings.value)
  }

  async getSecretValue<TValue>(
    pageName: string,
    key: string
  ): Promise<TValue | undefined> {
    const secretsPage = await this.getSecretsPage(pageName)
    return secretsPage[key]?.value
  }

  async getSecrets(pageName: string): Promise<AppSecret<any>[]> {
    const secretsPage = await this.getSecretsPage(pageName)
    return Object.keys(secretsPage).map((key) => ({
      key,
      ...secretsPage[key],
    }))
  }

  async setSecret<TValue>(
    pageName: string,
    secret: AppSecretInput<TValue>
  ): Promise<void> {
    this.logger.info("setSecret -> started", {
      pageName,
      secret: {
        key: secret.key,
      },
    })
    const secretsPage = await this.getSecretsPage(pageName)
    const currentSecret = secretsPage[secret.key]
    if (!currentSecret) {
      throw new Error(`Secret ${secret.key} not found in page ${pageName}`)
    }
    await this.saveSecretsPage(pageName, {
      ...secretsPage,
      [secret.key]: {
        ...currentSecret,
        value: secret.value,
        timestamp: Date.now(),
      },
    })
    this.logger.info("setSecret -> completed", {
      pageName,
      secret: {
        key: secret.key,
      },
    })
  }

  async defineSecret(
    pageName: string,
    definition: AppSecretDefinition
  ): Promise<void> {
    this.logger.info("defineSecret -> started", {
      pageName,
      secret: {
        key: definition.key,
        type: definition.type,
        hidden: definition.hidden,
      },
    })

    const secretsPage = await this.getSecretsPage(pageName)
    const currentSecret = secretsPage[definition.key]
    await this.saveSecretsPage(pageName, {
      ...secretsPage,
      [definition.key]: {
        type: definition.type,
        hidden: definition.hidden,
        value: currentSecret?.value ?? definition.defaultValue ?? null,
        timestamp: Date.now(),
      },
    })

    this.logger.info("defineSecret -> completed", {
      pageName,
      secret: {
        key: definition.key,
        type: definition.type,
        hidden: definition.hidden,
      },
    })
  }

  async pageEnsure(
    pageName: string,
    metadata: AppSecretsPageMetadata
  ): Promise<void> {
    if (!(await this.pageExists(pageName))) {
      await this.pageInitialize(pageName, metadata)
    }
  }

  async pageExists(pageName: string): Promise<boolean> {
    try {
      this.logger.info("pageExists -> started", {
        pageName,
      })
      const secret = await this.client.send(
        new DescribeSecretCommand({
          SecretId: this.buildPagePath(pageName),
        })
      )
      const exists = !!secret?.ARN
      this.logger.info("pageExists -> completed", {
        pageName,
        exists,
      })
      return exists
    } catch (e: any) {
      if (e.name === "ResourceNotFoundException") {
        this.logger.info("pageExists -> completed", {
          pageName,
          exists: false,
        })
        return false
      }
      throw e
    }
  }

  async pageInitialize(
    pageName: string,
    metadata: AppSecretsPageMetadata
  ): Promise<void> {
    this.logger.info("pageInitialize -> started", {
      pageName,
      metadata,
    })
    const result = await this.client.send(
      new CreateSecretCommand({
        Name: this.buildPagePath(pageName),
        SecretString: "{}",
        Tags: metadata.tags
          ? metadata.tags.map((tag) => ({
              Key: tag.key,
              Value: tag.value,
            }))
          : [],
      })
    )
    this.logger.info("pageInitialize -> completed", {
      pageName,
      metadata,
      result,
    })
  }

  private async saveSecretsPage(
    pageName: string,
    secretsPage: SecretsPage
  ): Promise<void> {
    this.logger.info("saveSecretsPage -> started", {
      pageName,
    })
    const result = await this.client.send(
      new UpdateSecretCommand({
        SecretId: this.buildPagePath(pageName),
        SecretString: JSON.stringify(secretsPage),
      })
    )
    this.logger.info("saveSecretsPage -> completed", {
      pageName,
      result,
    })
  }

  private async getSecretsPage(pageName: string): Promise<SecretsPage> {
    this.logger.info("getSecretsPage -> started", {
      pageName,
    })
    const secrets = await this.client.send(
      new GetSecretValueCommand({
        SecretId: this.buildPagePath(pageName),
      })
    )
    if (!secrets.SecretString) {
      throw new Error("Secrets not found")
    }

    const content = JSON.parse(secrets.SecretString)
    this.logger.info("getSecretsPage -> completed", {
      pageName,
    })
    return content
  }

  private buildPagePath(pageName: string) {
    return `${awsSecretsSettings.value.secretsRootPath}-${pageName}`
  }
}
