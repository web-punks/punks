import { DynamicModule, Module } from "@nestjs/common"
import { AwsSecretsSettings, awsSecretsSettings } from "./settings"
import { AwsSecretsProvider } from "./provider/secrets"

const ModuleData = {
  providers: [AwsSecretsProvider],
}

@Module({
  ...ModuleData,
})
export class AwsSecretsModule {
  /**
   * @deprecated use initialize instead
   */
  static forRoot(input: AwsSecretsSettings): DynamicModule {
    awsSecretsSettings.initialize(input)
    return {
      module: AwsSecretsModule,
      ...ModuleData,
    }
  }

  static initialize(input: AwsSecretsSettings) {
    awsSecretsSettings.initialize(input)
  }
}
