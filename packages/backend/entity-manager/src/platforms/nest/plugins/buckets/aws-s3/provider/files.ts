import {
  FileProviderDownloadData,
  FileProviderDownloadUrl,
  FileProviderReference,
  FileProviderUploadData,
  IFileProvider,
} from "../../../../../../abstractions"
import { AwsS3BucketProvider } from "./bucket"
import { awsBucketSettings } from "../settings"
import {
  ensureStartSlash,
  ensureTailingSlash,
  newUuid,
} from "@punks/backend-core"
import { WpFileProvider } from "../../../../decorators/files"

const createDayPath = (d: Date) =>
  `${d.getFullYear()}/${(d.getMonth() + 1).toString().padStart(2, "0")}/${d
    .getDate()
    .toString()
    .padStart(2, "0")}`

@WpFileProvider("awsS3")
export class AwsS3FileProvider implements IFileProvider {
  constructor(private readonly bucket: AwsS3BucketProvider) {}

  getProviderId(): string {
    return "awsS3"
  }

  async uploadFile(
    file: FileProviderUploadData
  ): Promise<FileProviderReference> {
    const path = file.filePath
      ? this.getBucketFileUploadPath(file.fileName, file.filePath)
      : this.getBucketFileUploadDayPath(file.fileName, new Date())
    await this.bucket.fileUpload({
      bucket: file.bucket ?? this.defaultBucket,
      filePath: path,
      content: file.content,
      contentType: file.contentType,
    })
    return {
      reference: path,
    }
  }

  async deleteFile(reference: FileProviderReference): Promise<void> {
    await this.bucket.fileDelete({
      bucket: reference.bucket ?? this.defaultBucket,
      filePath: reference.reference,
    })
  }

  async downloadFile(
    reference: FileProviderReference
  ): Promise<FileProviderDownloadData> {
    const content = await this.bucket.fileDownload({
      bucket: reference.bucket ?? this.defaultBucket,
      filePath: reference.reference,
    })
    return {
      content,
    }
  }

  async getFileProviderDownloadUrl(
    reference: FileProviderReference
  ): Promise<FileProviderDownloadUrl> {
    const url = await this.bucket.filePublicUrlCreate({
      bucket: reference.bucket ?? this.defaultBucket,
      expirationMinutes: awsBucketSettings.value.publicLinksExpirationMinutes,
      filePath: reference.reference,
    })
    return {
      downloadUrl: url,
    }
  }

  private getBucketFileUploadPath(fileName: string, filePath: string) {
    return `${ensureTailingSlash(filePath)}${fileName}`
  }

  private getBucketFileUploadDayPath(fileName: string, date: Date) {
    return `${ensureTailingSlash(
      awsBucketSettings.value.paths.filesUpload
    )}${createDayPath(date)}${ensureStartSlash(newUuid())}_${fileName}`
  }

  get defaultBucket() {
    return awsBucketSettings.value.defaultBucket
  }
}
