import type { Readable } from "stream"

export const streamToBuffer = (stream: Readable) =>
  new Promise<Buffer>((resolve, reject) => {
    const chunks: Buffer[] = []
    stream.on("data", (chunk) => chunks.push(chunk))
    stream.once("end", () => resolve(Buffer.concat(chunks)))
    stream.once("error", reject)
  })
