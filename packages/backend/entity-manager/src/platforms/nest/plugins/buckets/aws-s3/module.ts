import { DynamicModule, Module } from "@nestjs/common"
import { AwsS3BucketProvider } from "./provider/bucket"
import { AwsBucketSettings, awsBucketSettings } from "./settings"
import { AwsS3FileProvider } from "./provider"

const ModuleData = {
  providers: [AwsS3BucketProvider, AwsS3FileProvider],
  exports: [AwsS3BucketProvider, AwsS3FileProvider],
}

@Module({
  ...ModuleData,
})
export class AwsBucketModule {
  /**
   * @deprecated use initialize instead
   */
  static forRoot(input: AwsBucketSettings): DynamicModule {
    awsBucketSettings.initialize(input)
    return {
      module: AwsBucketModule,
      ...ModuleData,
    }
  }

  static initialize(input: AwsBucketSettings) {
    awsBucketSettings.initialize(input)
  }
}
