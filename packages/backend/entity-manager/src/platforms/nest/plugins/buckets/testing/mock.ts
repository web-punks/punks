import { newUuid } from "@punks/backend-core"
import {
  FileProviderDownloadData,
  FileProviderDownloadUrl,
  FileProviderReference,
  FileProviderUploadData,
  IFileProvider,
} from "../../../../../abstractions"
import {
  BucketContentItem,
  BucketFileDeleteInput,
  BucketFileDownloadInput,
  BucketFileExistsInput,
  BucketFilePublicUrlCreateInput,
  BucketFileUploadInput,
  BucketFolderEnsureInput,
  BucketFolderExistsInput,
  BucketFolderListInput,
  BucketFolderListResult,
  IBucketProvider,
} from "../../../../../abstractions/buckets"
import { WpBucketProvider } from "../../../decorators/bucket"
import { WpFileProvider } from "../../../decorators/files"

@WpFileProvider("inMemory")
export class InMemoryFileProvider implements IFileProvider {
  private readonly provider = new InMemoryBucketProvider()

  constructor() {}

  getProviderId(): string {
    return "inMemory"
  }

  async uploadFile(
    file: FileProviderUploadData
  ): Promise<FileProviderReference> {
    const filePath = `${newUuid()}/${file.fileName}`
    await this.provider.fileUpload({
      bucket: "test",
      content: file.content,
      contentType: file.contentType,
      filePath,
    })
    return {
      reference: filePath,
    }
  }

  async deleteFile(reference: FileProviderReference): Promise<void> {
    await this.provider.fileDelete({
      bucket: "test",
      filePath: reference.reference,
    })
  }

  async downloadFile(
    reference: FileProviderReference
  ): Promise<FileProviderDownloadData> {
    const content = await this.provider.fileDownload({
      bucket: "test",
      filePath: reference.reference,
    })
    return {
      content,
    }
  }

  async getFileProviderDownloadUrl(
    reference: FileProviderReference
  ): Promise<FileProviderDownloadUrl> {
    return {
      downloadUrl: await this.provider.filePublicUrlCreate({
        bucket: "test",
        filePath: reference.reference,
        expirationMinutes: 60,
      }),
    }
  }

  get defaultBucket(): string {
    return "in-memory-bucket"
  }
}

@WpBucketProvider("inMemory")
export class InMemoryBucketProvider implements IBucketProvider {
  private inMemoryStorage: Record<string, Buffer> = {}
  private inMemoryFolders: Record<string, boolean> = {}

  async folderList(
    input: BucketFolderListInput
  ): Promise<BucketFolderListResult> {
    // Implement folderList logic here
    // You may need to filter and return folder contents from inMemoryStorage
    const folderContents: BucketContentItem[] = []

    // Your implementation to populate folderContents goes here

    return { items: folderContents }
  }

  async folderEnsure(input: BucketFolderEnsureInput): Promise<void> {
    // Implement folderEnsure logic here
    // You may need to create a folder in inMemoryStorage
    // based on the input bucket and path
    const folderKey = `${input.bucket}/${input.path}`

    // Mock the folder creation
    this.inMemoryFolders[folderKey] = true
  }

  async folderExists(input: BucketFolderExistsInput): Promise<boolean> {
    // Implement folderExists logic here
    // Check if the folder exists in inMemoryStorage
    const folderKey = `${input.bucket}/${input.path}`

    return !!this.inMemoryFolders[folderKey]
  }

  async fileExists(input: BucketFileExistsInput): Promise<boolean> {
    // Implement fileExists logic here
    // Check if the file exists in inMemoryStorage
    const fileKey = `${input.bucket}/${input.filePath}`
    return !!this.inMemoryStorage[fileKey]
  }

  async fileDownload(input: BucketFileDownloadInput): Promise<Buffer> {
    // Implement fileDownload logic here
    // Retrieve the file content from inMemoryStorage based on the filePath
    const fileKey = `${input.bucket}/${input.filePath}`

    // Mock the file download
    const fileContent = this.inMemoryStorage[fileKey]
    if (!fileContent) {
      throw new Error(`File not found: ${fileKey}`)
    }

    return fileContent
  }

  async filePublicUrlCreate(
    input: BucketFilePublicUrlCreateInput
  ): Promise<string> {
    // Implement filePublicUrlCreate logic here
    // Generate a public URL for the file based on the input parameters
    const fileKey = `${input.bucket}/${input.filePath}`

    // Mock the public URL creation
    const publicUrl = `${fileKey}`
    return publicUrl
  }

  async fileUpload(input: BucketFileUploadInput): Promise<void> {
    // Implement fileUpload logic here
    // Upload the file content to inMemoryStorage based on the bucket and filePath
    const fileKey = `${input.bucket}/${input.filePath}`

    // Mock the file upload
    this.inMemoryStorage[fileKey] = input.content
  }

  async fileDelete(input: BucketFileDeleteInput): Promise<void> {
    // Implement fileDelete logic here
    // Delete the file from inMemoryStorage based on the bucket and filePath
    const fileKey = `${input.bucket}/${input.filePath}`

    // Mock the file deletion
    delete this.inMemoryStorage[fileKey]
  }
}
