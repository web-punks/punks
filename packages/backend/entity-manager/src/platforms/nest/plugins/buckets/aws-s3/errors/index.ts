export class AwsS3BucketError extends Error {
  constructor(message: string) {
    super(message)
    this.name = "AwsS3BucketError"
  }
}
