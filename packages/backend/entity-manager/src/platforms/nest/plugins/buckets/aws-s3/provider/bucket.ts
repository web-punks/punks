import {
  S3Client,
  PutObjectCommand,
  ListObjectsCommand,
  GetObjectCommand,
  CopyObjectCommand,
  DeleteObjectCommand,
} from "@aws-sdk/client-s3"
import { getSignedUrl } from "@aws-sdk/s3-request-presigner"
import { AwsBucketSettings, awsBucketSettings } from "../settings"
import { WpBucketProvider } from "../../../../decorators"
import {
  BucketFileCopyInput,
  BucketFileDeleteInput,
  BucketFileDownloadInput,
  BucketFileExistsInput,
  BucketFileMoveInput,
  BucketFilePublicUrlCreateInput,
  BucketFileUploadInput,
  BucketFolderEnsureInput,
  BucketFolderExistsInput,
  BucketFolderListInput,
  BucketFolderListResult,
  IBucketProvider,
} from "../../../../../../abstractions/buckets"
import { AwsS3BucketError } from "../errors"
import { ensureTailingSlash } from "@punks/backend-core"
import { streamToBuffer } from "../converters/stream"
import type { Readable } from "stream"

const createClient = (settings: AwsBucketSettings) =>
  new S3Client({
    region: settings.region,
    credentials:
      settings.awsAccessKeyId && settings.awsSecretAccessKey
        ? {
            accessKeyId: settings.awsAccessKeyId,
            secretAccessKey: settings.awsSecretAccessKey,
          }
        : undefined,
  })

@WpBucketProvider("awsS3")
export class AwsS3BucketProvider implements IBucketProvider {
  private readonly client: S3Client

  constructor() {
    this.client = createClient(awsBucketSettings.value)
  }

  async folderList(
    input: BucketFolderListInput
  ): Promise<BucketFolderListResult> {
    try {
      return await this.client.send(
        new ListObjectsCommand({
          Bucket: input.bucket,
          MaxKeys: input.maxResults,
          Prefix: input.path,
        })
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AWS BUCKET | listObjectsV2 | ${input.bucket} | ${input.path} | Error -> ${e.message}`
      )
    }
  }

  async folderEnsure(input: BucketFolderEnsureInput): Promise<void> {
    if (await this.folderExists(input)) {
      return
    }
    await this.folderCreate(input)
  }

  async folderCreate(input: BucketFolderEnsureInput): Promise<void> {
    try {
      await this.client.send(
        new PutObjectCommand({
          Bucket: input.bucket,
          Key: ensureTailingSlash(input.path),
        })
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AwsS3Bucket | folderCreate | ${input.bucket} | ${input.path} | Error -> ${e.message}`
      )
    }
  }

  async folderExists(input: BucketFolderExistsInput): Promise<boolean> {
    try {
      const result = await this.client.send(
        new GetObjectCommand({
          Bucket: input.bucket,
          Key: ensureTailingSlash(input.path),
        })
      )
      return (result.ContentLength ?? 0) > 0
    } catch (e: any) {
      if (e.Code === "NoSuchKey") {
        return false
      }
      throw new AwsS3BucketError(
        `AwsS3Bucket | folderExists | Error -> ${e.message}`
      )
    }
  }

  async fileDownload(input: BucketFileDownloadInput): Promise<Buffer> {
    try {
      const result = await this.client.send(
        new GetObjectCommand({
          Bucket: input.bucket,
          Key: input.filePath,
        })
      )
      return streamToBuffer(result.Body as Readable)
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AWS BUCKET | downloadFile | ${input.bucket} | ${input.filePath} | Error -> ${e.message}`
      )
    }
  }

  async fileExists(input: BucketFileExistsInput): Promise<boolean> {
    try {
      const result = await this.client.send(
        new GetObjectCommand({
          Bucket: input.bucket,
          Key: input.filePath,
        })
      )
      return (result.ContentLength ?? 0) > 0
    } catch (e: any) {
      if (e.Code === "NoSuchKey") {
        return false
      }
      throw new AwsS3BucketError(
        `AwsS3Bucket | fileExists | Error -> ${e.message}`
      )
    }
  }

  async filePublicUrlCreate(
    input: BucketFilePublicUrlCreateInput
  ): Promise<string> {
    try {
      return await getSignedUrl(
        this.client,
        new GetObjectCommand({
          Bucket: input.bucket,
          Key: input.filePath,
        }),
        {
          expiresIn: input.expirationMinutes * 60,
        }
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AwsS3Bucket | filePublicUrlCreate | ${input.bucket} | ${input.filePath} | Error -> ${e.message}`
      )
    }
  }

  async fileUpload(input: BucketFileUploadInput): Promise<void> {
    try {
      await this.client.send(
        new PutObjectCommand({
          Bucket: input.bucket,
          Key: input.filePath,
          Body: input.content,
          ContentType: input.contentType,
        })
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AwsS3Bucket | fileUpload | ${input.bucket} | ${input.filePath} | Error -> ${e.message}`
      )
    }
  }

  async fileDelete(input: BucketFileDeleteInput): Promise<void> {
    try {
      await this.client.send(
        new DeleteObjectCommand({
          Bucket: input.bucket,
          Key: input.filePath,
        })
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AwsS3Bucket | fileDelete | ${input.bucket} | ${input.filePath} | Error -> ${e.message}`
      )
    }
  }

  async fileCopy(input: BucketFileCopyInput): Promise<void> {
    try {
      await this.client.send(
        new CopyObjectCommand({
          CopySource: `${input.bucket}/${input.filePath}`,
          Bucket: input.targetBucket,
          Key: input.targetPath,
        })
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AwsS3Bucket | fileCopy | ${input.bucket} | ${input.filePath} | Error -> ${e.message}`
      )
    }
  }

  async fileMove(input: BucketFileMoveInput): Promise<void> {
    try {
      await this.client.send(
        new CopyObjectCommand({
          CopySource: `${input.bucket}/${input.filePath}`,
          Bucket: input.targetBucket,
          Key: input.targetPath,
        })
      )
      await this.client.send(
        new DeleteObjectCommand({
          Bucket: input.bucket,
          Key: input.filePath,
        })
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AwsS3Bucket | fileMove | ${input.bucket} | ${input.filePath} | Error -> ${e.message}`
      )
    }
  }
}
