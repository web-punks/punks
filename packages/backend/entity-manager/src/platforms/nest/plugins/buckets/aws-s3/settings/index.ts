import { AppInMemorySettings } from "../../../../../../settings"

export type AwsBucketPaths = {
  filesUpload: string
}

export type AwsBucketSettings = {
  awsAccessKeyId?: string
  awsSecretAccessKey?: string
  region?: string
  defaultBucket: string
  publicLinksExpirationMinutes: number
  paths: AwsBucketPaths
}

export const awsBucketSettings = new AppInMemorySettings<AwsBucketSettings>(
  "awsBucketSettings"
)
