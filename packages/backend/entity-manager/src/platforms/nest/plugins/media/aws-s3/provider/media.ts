import {
  S3Client,
  PutObjectCommand,
  GetObjectCommand,
  DeleteObjectCommand,
} from "@aws-sdk/client-s3"
import { AwsMediaSettings, awsMediaSettings } from "../settings"
import {
  IMediaProvider,
  MediaReference,
  MediaUploadInput,
} from "../../../../../../abstractions"
import { WpMediaProvider } from "../../../../decorators"
import { AwsS3MediaError } from "../errors"
import { createDayPath } from "../../../../../../commands/export"
import { newUuid } from "@punks/backend-core"
import { streamToBuffer } from "../../../buckets/aws-s3/converters/stream"
import type { Readable } from "stream"

const createClient = (settings: AwsMediaSettings) =>
  new S3Client({
    region: settings.region,
    credentials:
      settings.awsAccessKeyId && settings.awsSecretAccessKey
        ? {
            accessKeyId: settings.awsAccessKeyId,
            secretAccessKey: settings.awsSecretAccessKey,
          }
        : undefined,
  })

const buildRef = (bucket: string, path: string) => {
  return `s3://${bucket}/${path}`
}

const parseRef = (ref: string) => {
  const parts = ref.split("/")
  return {
    bucket: parts[2],
    path: parts.slice(3).join("/"),
  }
}

@WpMediaProvider("awsS3")
export class AwsS3MediaProvider implements IMediaProvider {
  private readonly client: S3Client

  constructor() {
    this.client = createClient(awsMediaSettings.value)
  }

  getProviderId(): string {
    return "awsS3"
  }

  getFileName(input: MediaReference): string {
    const parsedRef = parseRef(input.ref)
    return parsedRef.path.split("/").pop() ?? ""
  }

  async mediaUpload(input: MediaUploadInput): Promise<MediaReference> {
    try {
      const path = this.buildUploadPath(input.fileName)
      const bucket = awsMediaSettings.value.defaultBucket

      await this.client.send(
        new PutObjectCommand({
          Bucket: bucket,
          Key: path,
          Body: input.content,
          ContentType: input.contentType,
          ACL: "public-read",
        })
      )
      return {
        ref: buildRef(bucket, path),
      }
    } catch (e: any) {
      throw new AwsS3MediaError(
        `AwsS3MediaProvider | mediaUpload | ${input.fileName} | Error -> ${e.message}`
      )
    }
  }

  async mediaDelete(input: MediaReference): Promise<void> {
    try {
      const parsedRef = parseRef(input.ref)
      await this.client.send(
        new DeleteObjectCommand({
          Bucket: parsedRef.bucket,
          Key: parsedRef.path,
        })
      )
    } catch (e: any) {
      throw new AwsS3MediaError(
        `AwsS3MediaProvider | mediaDelete | ${input.ref} | Error -> ${e.message}`
      )
    }
  }

  async mediaDownload(input: MediaReference): Promise<Buffer> {
    try {
      const parsedRef = parseRef(input.ref)
      const result = await this.client.send(
        new GetObjectCommand({
          Bucket: parsedRef.bucket,
          Key: parsedRef.path,
        })
      )
      return streamToBuffer(result.Body as Readable)
    } catch (e: any) {
      throw new AwsS3MediaError(
        `AwsS3MediaProvider | mediaDownload | ${input.ref} | Error -> ${e.message}`
      )
    }
  }

  private buildUploadPath(fileName: string): string {
    return [
      awsMediaSettings.value.paths.filesUpload,
      createDayPath(new Date()),
      newUuid(),
      fileName,
    ].join("/")
  }
}
