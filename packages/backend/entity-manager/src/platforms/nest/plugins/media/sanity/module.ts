import { DynamicModule, Module } from "@nestjs/common"
import { SanityMediaProvider } from "./provider/media"
import { SanityMediaSettings, sanityMediaSettings } from "./settings"

const ModuleData = {
  providers: [SanityMediaProvider],
}

@Module({
  ...ModuleData,
})
export class SanityMediaModule {
  /**
   * @deprecated use initialize instead
   */
  static forRoot(input: SanityMediaSettings): DynamicModule {
    sanityMediaSettings.initialize(input)
    return {
      module: SanityMediaModule,
      ...ModuleData,
    }
  }

  static initialize(input: SanityMediaSettings) {
    sanityMediaSettings.initialize(input)
  }
}
