import { AppInMemorySettings } from "../../../../../../settings"

export type SanityMediaSettings = {
  projectId: string
  dataset: string
  token: string
  apiVersion: string
}

export const sanityMediaSettings = new AppInMemorySettings<SanityMediaSettings>(
  "sanityMediaSettings"
)
