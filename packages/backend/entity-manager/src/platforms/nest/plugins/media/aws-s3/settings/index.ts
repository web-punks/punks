import { AppInMemorySettings } from "../../../../../../settings"

export type AwsBucketPaths = {
  filesUpload: string
}

export type AwsMediaSettings = {
  awsAccessKeyId?: string
  awsSecretAccessKey?: string
  region?: string
  defaultBucket: string
  paths: AwsBucketPaths
}

export const awsMediaSettings = new AppInMemorySettings<AwsMediaSettings>(
  "awsMediaSettings"
)
