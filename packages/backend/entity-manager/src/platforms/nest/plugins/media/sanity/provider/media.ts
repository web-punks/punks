import * as axios from "axios"
import { SanityClient, createClient } from "@sanity/client"
import { sanityMediaSettings } from "../settings"
import {
  IMediaProvider,
  MediaReference,
  MediaUploadInput,
} from "../../../../../../abstractions"
import { WpMediaProvider } from "../../../../decorators"
import { SanityMediaError } from "../errors"

const buildRef = ({
  dataset,
  projectId,
  id,
}: {
  projectId: string
  dataset: string
  id: string
}) => {
  return `sanity://${projectId}/${dataset}/${id}`
}

const parseRef = (ref: string) => {
  const parts = ref.split("/")
  return {
    projectId: parts[2],
    dataset: parts[3],
    id: parts.slice(4).join("/"),
  }
}

const isImage = (contentType: string) => {
  return contentType.startsWith("image/")
}

const buildFolderTag = (folderId?: string) => {
  return folderId ? `folder:${folderId}` : "folder:root"
}

@WpMediaProvider("sanity")
export class SanityMediaProvider implements IMediaProvider {
  private readonly client: SanityClient

  constructor() {
    this.client = createClient({
      projectId: sanityMediaSettings.value.projectId,
      dataset: sanityMediaSettings.value.dataset,
      token: sanityMediaSettings.value.token,
      apiVersion: sanityMediaSettings.value.apiVersion,
    })
  }

  getProviderId(): string {
    return "sanity"
  }

  getFileName(input: MediaReference): string {
    const parsedRef = parseRef(input.ref)
    return parsedRef.id.replace("_", ".")
  }

  async mediaUpload(input: MediaUploadInput): Promise<MediaReference> {
    try {
      const asses = await this.client.assets.upload(
        input.contentType && isImage(input.contentType) ? "image" : "file",
        input.content,
        {
          contentType: input.contentType,
          filename: input.fileName,
          source: {
            id: buildFolderTag(input.folderId),
            name: "media-upload",
          },
          tag: "punks-media",
        }
      )

      return {
        ref: buildRef({
          dataset: sanityMediaSettings.value.dataset,
          projectId: sanityMediaSettings.value.projectId,
          id: asses._id,
        }),
      }
    } catch (e: any) {
      throw new SanityMediaError(
        `SanityMediaProvider | mediaUpload | ${input.fileName} | Error -> ${e.message}`
      )
    }
  }

  async mediaDelete(input: MediaReference): Promise<void> {
    try {
      const parsedRef = parseRef(input.ref)
      await this.client.delete(parsedRef.id)
    } catch (e: any) {
      throw new SanityMediaError(
        `SanityMediaProvider | mediaDelete | ${input.ref} | Error -> ${e.message}`
      )
    }
  }

  async mediaDownload(input: MediaReference): Promise<Buffer> {
    try {
      const parsedRef = parseRef(input.ref)
      const asset = await this.client.getDocument(parsedRef.id)
      if (!asset) {
        throw new Error(`Asset not found id: ${parsedRef.id}`)
      }
      const response = await axios.default.get(asset.url, {
        responseType: "arraybuffer",
      })
      return Buffer.from(response.data, "binary")
    } catch (e: any) {
      throw new SanityMediaError(
        `SanityMediaProvider | mediaDownload | ${input.ref} | Error -> ${e.message}`
      )
    }
  }
}
