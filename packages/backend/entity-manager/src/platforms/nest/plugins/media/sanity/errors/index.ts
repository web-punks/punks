export class SanityMediaError extends Error {
  constructor(message: string) {
    super(message)
    this.name = "SanityMediaError"
  }
}
