import { DynamicModule, Module } from "@nestjs/common"
import { AwsS3MediaProvider } from "./provider/media"
import { AwsMediaSettings, awsMediaSettings } from "./settings"

const ModuleData = {
  providers: [AwsS3MediaProvider],
}

@Module({
  ...ModuleData,
})
export class AwsS3MediaModule {
  /**
   * @deprecated use initialize instead
   */
  static forRoot(input: AwsMediaSettings): DynamicModule {
    awsMediaSettings.initialize(input)
    return {
      module: AwsS3MediaModule,
      ...ModuleData,
    }
  }

  static initialize(input: AwsMediaSettings) {
    awsMediaSettings.initialize(input)
  }
}
