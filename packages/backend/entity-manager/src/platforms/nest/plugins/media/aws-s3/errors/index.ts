export class AwsS3MediaError extends Error {
  constructor(message: string) {
    super(message)
    this.name = "AwsS3MediaError"
  }
}
