import { newUuid } from "@punks/backend-core"
import {
  IMediaProvider,
  MediaReference,
  MediaUploadInput,
} from "../../../../../abstractions"
import { WpMediaProvider } from "../../../decorators"
import { InMemoryBucketProvider } from "../../buckets/testing"

@WpMediaProvider("inMemory")
export class InMemoryMediaProvider implements IMediaProvider {
  private readonly provider = new InMemoryBucketProvider()

  async mediaUpload(input: MediaUploadInput): Promise<MediaReference> {
    const path = [newUuid(), input.fileName].join("/")
    await this.provider.fileUpload({
      bucket: "default",
      content: input.content,
      contentType: input.contentType,
      filePath: path,
    })

    return {
      ref: `inMemory://${path}`,
    }
  }

  async mediaDelete(input: MediaReference): Promise<void> {
    await this.provider.fileDelete({
      bucket: "default",
      filePath: input.ref.split("://")[1],
    })
  }

  async mediaDownload(input: MediaReference): Promise<Buffer> {
    return this.provider.fileDownload({
      bucket: "default",
      filePath: input.ref.split("://")[1],
    })
  }

  getFileName(input: MediaReference): string {
    return input.ref.split("/").pop() ?? ""
  }

  getProviderId(): string {
    return "inMemory"
  }
}
