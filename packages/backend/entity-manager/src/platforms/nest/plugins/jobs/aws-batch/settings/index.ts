import { AppInMemorySettings } from "../../../../../../settings"

export type AwsBatchSettings = {
  awsAccessKeyId?: string
  awsSecretAccessKey?: string
  region?: string
  defaultFargateBatchComputeEnvironment: string
  defaultEc2BatchComputeEnvironment?: string
  batchExecutionRole: string
  batchResourcesPrefix: string
}

export const awsBatchSettings = new AppInMemorySettings<AwsBatchSettings>(
  "awsBatchSettings"
)
