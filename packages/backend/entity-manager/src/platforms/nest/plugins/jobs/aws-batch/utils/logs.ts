import {
  CloudWatchLogsClient,
  CreateLogGroupCommand,
  DescribeLogGroupsCommand,
} from "@aws-sdk/client-cloudwatch-logs"
import { AwsBatchSettings } from "../settings"

export const ensureJobLogGroup = async (
  logGroupName: string,
  awsSettings: AwsBatchSettings
) => {
  const client = new CloudWatchLogsClient({
    ...(awsSettings.awsSecretAccessKey && awsSettings.awsSecretAccessKey
      ? {
          accessKeyId: awsSettings.awsSecretAccessKey,
          secretAccessKey: awsSettings.awsSecretAccessKey,
        }
      : {}),
  })

  const describeCommand = new DescribeLogGroupsCommand({
    logGroupNamePrefix: logGroupName,
  })
  const describeResponse = await client.send(describeCommand)

  const existingLogGroup = describeResponse.logGroups?.find(
    (group) => group.logGroupName === logGroupName
  )

  if (!existingLogGroup) {
    const createCommand = new CreateLogGroupCommand({
      logGroupName,
    })
    await client.send(createCommand)
  }
}
