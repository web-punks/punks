import { Injectable } from "@nestjs/common"
import { JobStatus as AwsJobStatus } from "@aws-sdk/client-batch"
import { JobProviderState } from "../../../../extensions"
import { JobProvider } from "../../../../extensions/jobs/decorators/decorators"
import {
  CancelJobStatusInput,
  GetJobStatusInput,
  JobDispatchInput,
  JobProviderInstance,
  JobStatusResult,
  UpsertJobDefinitionInput,
} from "../../../../extensions/jobs/abstractions/runner"
import { AwsBatchService } from "../manager"
import {
  AwsBatchInfrastructureParams,
  AwsBatchInvocationParams,
} from "../models"

const mapJobStatus = (awsJobStatus: AwsJobStatus): JobProviderState => {
  switch (awsJobStatus) {
    case AwsJobStatus.RUNNABLE:
    case AwsJobStatus.STARTING:
    case AwsJobStatus.SUBMITTED:
    case AwsJobStatus.PENDING:
      return JobProviderState.Initializing
    case AwsJobStatus.FAILED:
      return JobProviderState.CompletedWithErrors
    case AwsJobStatus.RUNNING:
      return JobProviderState.Running
    case AwsJobStatus.SUCCEEDED:
      return JobProviderState.Completed
  }
}

const replacePayload = (command: string, payload: unknown): string =>
  command.replaceAll("$(payload)", JSON.stringify(payload ?? {}))

const replacePlaceholders = (
  command: string,
  placeholders?: Record<string, string>
): string =>
  Object.entries(placeholders ?? {}).reduce(
    (acc, [key, value]) => acc.replaceAll(`$(${key})`, value),
    command
  )

@JobProvider("awsBatch")
@Injectable()
export class AwsJobsProvider implements JobProviderInstance {
  constructor(private readonly awsBatchService: AwsBatchService) {}

  async dispatch(input: JobDispatchInput): Promise<void> {
    const { definition, schedule, instanceId, payload, commandPlaceholders } =
      input

    const awsInvocationParams =
      (schedule?.invocationOverrides as AwsBatchInvocationParams) ??
      (definition.invocationParams as AwsBatchInvocationParams)

    const awsInfrastructureParams =
      definition.infrastructureParams as AwsBatchInfrastructureParams

    await this.awsBatchService.submitJob({
      instanceId,
      jobUid: definition.uid,
      overrides: {
        command: awsInvocationParams.startCommand?.map((x) =>
          replacePlaceholders(replacePayload(x, payload), commandPlaceholders)
        ),
        batchComputeEnvironment:
          awsInfrastructureParams.batchComputeEnvironment,
      },
      variables: input.variables,
    })
  }

  async upsertJobDefinition(input: UpsertJobDefinitionInput): Promise<void> {
    const { definition } = input
    const infrastructureParams =
      definition.infrastructureParams as AwsBatchInfrastructureParams
    const invocationParams =
      definition.invocationParams as AwsBatchInvocationParams
    await this.awsBatchService.ensureJobDefinition({
      jobUid: definition.uid,
      infrastructureParams,
      invocationParams,
    })
  }

  async getJobStatus(input: GetJobStatusInput): Promise<JobStatusResult> {
    const job = await this.awsBatchService.getJobInstance({
      instanceId: input.instanceId,
      jobUid: input.definition.uid,
    })

    if (!job) {
      throw new Error(
        `Aws job instance not found -> ${input.instanceId} -> ${input.definition.uid}`
      )
    }

    return {
      state: mapJobStatus(job.status as AwsJobStatus),
      error: job.status === AwsJobStatus.FAILED ? job.statusReason : undefined,
    }
  }

  async cancelJob(input: CancelJobStatusInput): Promise<void> {
    await this.awsBatchService.cancelJobInput()
  }
}
