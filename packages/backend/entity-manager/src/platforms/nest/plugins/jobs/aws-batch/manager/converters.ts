import { KeyValuePair } from "@aws-sdk/client-batch"
import { BatchJobVariables } from "./types"

const replaceVariablesInValue = (
  value: string,
  variables: BatchJobVariables
) => {
  // Example matches:
  // Input: "Hello ${name} and $(user)!" with variables {name: "World", user: "John"}
  // Output: "Hello World and John!"
  //
  // Input: "The ${animal} jumps over $(object)" with variables {animal: "fox", object: "fence"}
  // Output: "The fox jumps over fence"
  return value.replace(
    /[\$][\{\(]([^\}\)]+)[\}\)]/g,
    (match, p1) => variables[p1]
  )
}

export const replaceVariable = (
  environmentValue: KeyValuePair,
  variables: BatchJobVariables
): KeyValuePair => {
  return {
    name: environmentValue.name,
    value: environmentValue.value
      ? replaceVariablesInValue(environmentValue.value, variables)
      : environmentValue.value,
  }
}

export const replaceVariables = (
  environment: KeyValuePair[],
  variables: BatchJobVariables
): KeyValuePair[] => {
  return environment.map((e) => replaceVariable(e, variables))
}
