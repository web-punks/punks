import {
  AwsBatchInfrastructureParams,
  AwsBatchInvocationParams,
} from "../models"

export type BathJobDefinition = {
  jobUid: string
  image: string
  memory: number
  vcpu: number
  gpuMemory?: number
  defaultCommand: string[]
}

export type BatchJobOverrides = {
  command?: string[]
  batchComputeEnvironment?: string
}

export type BatchJobVariables = Record<string, string>

export type SubmitBatchJobInput = {
  overrides?: BatchJobOverrides
  jobUid: string
  instanceId: string
  variables?: BatchJobVariables
}

export type GetBatchJobStatusInput = {
  jobUid: string
  instanceId: string
}

export type AwsJobDefinition = {
  jobUid: string
  infrastructureParams: AwsBatchInfrastructureParams
  invocationParams: AwsBatchInvocationParams
}

export type AwsBatchCreateQueueOptions = {
  batchComputeEnvironment: string
}
