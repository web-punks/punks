import { DynamicModule, Module } from "@nestjs/common"
import { AwsBatchSettings, awsBatchSettings } from "./settings"
import { AwsJobsProvider } from "./provider"
import { AwsBatchService } from "./manager"

const ModuleData = {
  providers: [AwsBatchService, AwsJobsProvider],
  exports: [AwsJobsProvider],
}

@Module({
  imports: [],
  providers: [...ModuleData.providers],
  exports: [...ModuleData.exports],
})
export class AwsJobsModule {
  /**
   * @deprecated use initialize instead
   */
  static forRoot(input: AwsBatchSettings): DynamicModule {
    awsBatchSettings.initialize(input)
    return {
      module: AwsJobsModule,
      ...ModuleData,
    }
  }

  static initialize(input: AwsBatchSettings) {
    awsBatchSettings.initialize(input)
  }
}
