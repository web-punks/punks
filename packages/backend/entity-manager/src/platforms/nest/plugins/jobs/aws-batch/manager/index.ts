import { Injectable } from "@nestjs/common"
import { orderBy } from "lodash"
import {
  BatchClient,
  DescribeJobQueuesCommand,
  CancelJobCommand,
  SubmitJobCommand,
  CreateJobQueueCommand,
  DescribeJobDefinitionsCommand,
  RegisterJobDefinitionCommand,
  DeregisterJobDefinitionCommand,
  JobDefinitionType,
  ResourceType,
  PlatformCapability,
  ListJobsCommand,
  AssignPublicIp,
  UpdateJobQueueCommand,
  SubmitJobCommandInput,
} from "@aws-sdk/client-batch"
import {
  AwsBatchCreateQueueOptions,
  AwsJobDefinition,
  GetBatchJobStatusInput,
  SubmitBatchJobInput,
} from "./types"
import { Log, sleep } from "@punks/backend-core"
import { awsBatchSettings } from "../settings"
import { ensureJobLogGroup } from "../utils/logs"
import { replaceVariables } from "./converters"

const jobDefinitionName = (jobUid: string) =>
  `${awsBatchSettings.value.batchResourcesPrefix}-job-${jobUid}`

const jobInstanceName = (jobUid: string, instanceId: string) =>
  `${awsBatchSettings.value.batchResourcesPrefix}-job-${jobUid}-${instanceId}`

const jobQueueName = (jobUid: string) =>
  `${awsBatchSettings.value.batchResourcesPrefix}-queue-${jobUid}`

@Injectable()
export class AwsBatchService {
  private readonly logger = Log.getLogger(AwsBatchService.name)

  async cancelJobInput() {
    await this.client().send(
      new CancelJobCommand({
        jobId: "",
        reason: "",
      })
    )
  }

  async getJobInstance(input: GetBatchJobStatusInput) {
    const jobName = jobInstanceName(input.jobUid, input.instanceId)
    this.logger.info(`AWS JOB -> fetching job ${jobName}`)
    const result = await this.client().send(
      new ListJobsCommand({
        jobQueue: jobQueueName(input.jobUid),
        maxResults: 1,
        filters: [
          {
            name: "JOB_NAME",
            values: [jobName],
          },
        ],
      })
    )
    return result.jobSummaryList?.[0]
  }

  async submitJob(input: SubmitBatchJobInput) {
    this.logger.info(`AWS JOB -> submitting job triggered`, {
      input,
    })

    const jobDefName = jobDefinitionName(input.jobUid)
    const jobDefinition = await this.getLatestJobDefinition(jobDefName)
    if (!jobDefinition) {
      throw new Error(`Job definition not found -> ${jobDefName}`)
    }

    const platformType = jobDefinition.platformCapabilities?.[0]
    if (!platformType) {
      throw new Error(`Platform type not found -> ${jobDefName}`)
    }

    const defaultComputeEnvironment =
      platformType === PlatformCapability.FARGATE
        ? this.awsSettings.defaultFargateBatchComputeEnvironment
        : this.awsSettings.defaultEc2BatchComputeEnvironment

    const batchComputeEnvironment =
      input.overrides?.batchComputeEnvironment ?? defaultComputeEnvironment
    if (!batchComputeEnvironment) {
      throw new Error(`Compute environment not found -> ${jobDefName}`)
    }

    const queue = await this.ensureQueue(jobQueueName(input.jobUid), {
      batchComputeEnvironment,
    })

    const jobName = jobInstanceName(input.jobUid, input.instanceId)

    const submitJobRequest = {
      jobQueue: queue.jobQueueArn,
      jobDefinition: jobDefinition.jobDefinitionArn,
      jobName,
      containerOverrides: {
        ...(input.overrides?.command ||
        jobDefinition.containerProperties?.command
          ? {
              command:
                input.overrides?.command ??
                jobDefinition.containerProperties!.command,
            }
          : {}),
        environment:
          jobDefinition.containerProperties?.environment && input.variables
            ? replaceVariables(
                jobDefinition.containerProperties.environment,
                input.variables
              )
            : jobDefinition.containerProperties?.environment ?? [],
      },
    } satisfies SubmitJobCommandInput

    this.logger.info(
      `AWS JOB -> invoking job ${jobName} ${queue.jobQueueArn}`,
      {
        request: submitJobRequest,
      }
    )
    const result = await this.client().send(
      new SubmitJobCommand(submitJobRequest)
    )
    this.logger.info(
      `AWS JOB -> job submitted ${jobName} ${queue.jobQueueArn}`,
      {
        result,
      }
    )
  }

  async ensureJobDefinition(definition: AwsJobDefinition) {
    const jobName = jobDefinitionName(definition.jobUid)
    this.logger.debug(`AWS JOB -> ensuring job definition ${jobName}`, {
      definition,
    })
    const jobs = await this.getActiveJobDefinitions(jobName)

    for (const job of jobs) {
      this.logger.debug(`AWS JOB -> unregistering job definition`, {
        job,
      })
      await this.unregisterJobDefinition(job.jobDefinitionArn!)
    }

    this.logger.debug(`AWS JOB -> registering job definition`, {
      definition,
    })
    await this.registerJobDefinition(jobName, definition)
    const updatedDefinition = await this.getLatestJobDefinition(jobName)
    this.logger.debug(`AWS JOB -> job definition updated`, {
      updatedDefinition,
    })

    return updatedDefinition
  }

  private async unregisterJobDefinition(jobDefinitionArn: string) {
    await this.client().send(
      new DeregisterJobDefinitionCommand({
        jobDefinition: jobDefinitionArn,
      })
    )
  }

  private async registerJobDefinition(
    jobName: string,
    definition: AwsJobDefinition
  ) {
    const logGroup = await this.ensureJobLogDriver(jobName, definition)

    await this.client().send(
      new RegisterJobDefinitionCommand({
        jobDefinitionName: jobName,
        type: JobDefinitionType.Container,
        platformCapabilities:
          definition.infrastructureParams.computePlatformType === "ec2"
            ? [PlatformCapability.EC2]
            : [PlatformCapability.FARGATE],
        containerProperties: {
          environment: definition.infrastructureParams.environmentVariables,
          privileged: definition.infrastructureParams.privileged ?? false,
          command: definition.invocationParams.startCommand,
          image: definition.infrastructureParams.dockerImage,
          executionRoleArn: this.awsSettings.batchExecutionRole,
          ephemeralStorage: definition.infrastructureParams.ephemeralStorageGB
            ? {
                sizeInGiB: definition.infrastructureParams.ephemeralStorageGB,
              }
            : undefined,
          resourceRequirements: [
            {
              type: ResourceType.MEMORY,
              value: definition.infrastructureParams.memory.toString(),
            },
            {
              type: ResourceType.VCPU,
              value: definition.infrastructureParams.vcpu.toString(),
            },
            ...(definition.infrastructureParams.gpu
              ? [
                  {
                    type: ResourceType.GPU,
                    value: definition.infrastructureParams.gpu.toString(),
                  },
                ]
              : []),
          ],
          networkConfiguration: definition.infrastructureParams.assignPublicIp
            ? {
                assignPublicIp: AssignPublicIp.ENABLED,
              }
            : undefined,
          logConfiguration: logGroup
            ? {
                logDriver: "awslogs",
                options: {
                  "awslogs-group": logGroup.name,
                  "awslogs-region": await this.client().config.region(),
                  "awslogs-stream-prefix": "ecs",
                },
              }
            : undefined,
        },
      })
    )
  }

  private async ensureJobLogDriver(
    jobName: string,
    definition: AwsJobDefinition
  ) {
    const logGroup = definition.infrastructureParams.logGroupName
      ? {
          name: `/aws/batch/${awsBatchSettings.value.batchResourcesPrefix}-job-${definition.infrastructureParams.logGroupName}`,
        }
      : undefined

    if (!logGroup) {
      return
    }

    await ensureJobLogGroup(logGroup.name, this.awsSettings)
    return logGroup
  }

  private async getLatestJobDefinition(jobName: string) {
    const jobs = await this.getActiveJobDefinitions(jobName)
    this.logger.debug(`AWS JOB -> latest job definition`, {
      jobName,
      jobs,
    })
    return orderBy(jobs, (x) => x.revision, "desc")[0]
  }

  private async getActiveJobDefinitions(jobName: string) {
    const result = await this.client().send(
      new DescribeJobDefinitionsCommand({
        jobDefinitionName: jobName,
        status: "ACTIVE",
      })
    )
    return result.jobDefinitions ?? []
  }

  private async ensureQueue(
    queueName: string,
    options: AwsBatchCreateQueueOptions
  ) {
    if (!queueName) {
      throw new Error(`Queue name not provided`)
    }

    this.logger.debug(`AWS JOB -> ensuring queue ${queueName}`)
    const queue = await this.getQueue(queueName)
    if (queue) {
      if (
        queue.computeEnvironmentOrder?.[0].computeEnvironment ===
        options.batchComputeEnvironment
      ) {
        this.logger.debug(`AWS JOB -> queue exists ${queueName}`)
        return queue
      }

      this.logger.debug(
        `AWS JOB -> queue exists with different compute environment ${queueName}`,
        {
          queue,
          options,
        }
      )
      await this.updateQueue(queueName, options)
      this.logger.debug(`AWS JOB -> queue updated ${queueName}`)

      // Wait for the queue to be active
      await sleep(10000)
      const updatedQueue = await this.getQueue(queueName)
      if (!updatedQueue) {
        throw new Error(`Queue not updated -> ${queueName}`)
      }
      return updatedQueue
    }
    this.logger.debug(`AWS JOB -> creating queue ${queueName}`)
    await this.createQueue(queueName, options)
    this.logger.debug(`AWS JOB -> queue created ${queueName}`)

    // Wait for the queue to be active
    await sleep(10000)

    const currentQueue = await this.getQueue(queueName)
    if (!currentQueue) {
      throw new Error(`Queue not created -> ${queueName}`)
    }

    return currentQueue
  }

  private async createQueue(
    queueName: string,
    options?: AwsBatchCreateQueueOptions
  ) {
    if (
      !this.awsSettings.defaultFargateBatchComputeEnvironment &&
      !options?.batchComputeEnvironment
    ) {
      throw new Error(`No compute environment defined`)
    }
    await this.client().send(
      new CreateJobQueueCommand({
        jobQueueName: queueName,
        priority: 0,
        computeEnvironmentOrder: [
          {
            computeEnvironment:
              options?.batchComputeEnvironment ||
              this.awsSettings.defaultFargateBatchComputeEnvironment,
            order: 0,
          },
        ],
      })
    )
  }

  private async updateQueue(
    queueName: string,
    options?: AwsBatchCreateQueueOptions
  ) {
    if (
      !this.awsSettings.defaultFargateBatchComputeEnvironment &&
      !options?.batchComputeEnvironment
    ) {
      throw new Error(`No compute environment defined`)
    }
    await this.client().send(
      new UpdateJobQueueCommand({
        jobQueue: queueName,
        priority: 0,
        computeEnvironmentOrder: [
          {
            computeEnvironment:
              options?.batchComputeEnvironment ||
              this.awsSettings.defaultFargateBatchComputeEnvironment,
            order: 0,
          },
        ],
      })
    )
  }

  private async getQueue(queueName: string) {
    const result = await this.client().send(
      new DescribeJobQueuesCommand({
        jobQueues: [queueName],
      })
    )
    return result.jobQueues?.[0]
  }

  private client = (): BatchClient =>
    new BatchClient({
      ...(this.awsSettings.awsSecretAccessKey &&
      this.awsSettings.awsSecretAccessKey
        ? {
            accessKeyId: this.awsSettings.awsSecretAccessKey,
            secretAccessKey: this.awsSettings.awsSecretAccessKey,
          }
        : {}),
    })

  private get awsSettings() {
    return awsBatchSettings.value
  }
}
