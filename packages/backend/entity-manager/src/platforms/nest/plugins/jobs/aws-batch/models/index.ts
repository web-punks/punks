import { JobDefinition } from "../../../../extensions/jobs/abstractions/definition"

export type AwsJobComputePlatformType = "ec2" | "fargate"

export type AwsJobEnvironmentVariable = {
  name: string
  value: string
}

export type AwsBatchInfrastructureParams = {
  vcpu: number
  memory: number
  gpu?: number
  ephemeralStorageGB?: number
  dockerImage: string
  environmentVariables?: AwsJobEnvironmentVariable[]
  computePlatformType?: AwsJobComputePlatformType
  assignPublicIp?: boolean
  logGroupName?: string
  privileged?: boolean
  batchComputeEnvironment?: string
}

export type AwsBatchInvocationParams = {
  startCommand: string[]
}

export type AwsJobDefinition = JobDefinition<
  AwsBatchInfrastructureParams,
  AwsBatchInvocationParams
>
