import { AppInMemorySettings } from "../../../../../settings"

export type AwsDynamoDbSettings = {
  awsAccessKeyId?: string
  awsSecretAccessKey?: string
  region?: string
}

export const awsDynamoDbSettings = new AppInMemorySettings<AwsDynamoDbSettings>(
  "awsDynamoDbSettings"
)
