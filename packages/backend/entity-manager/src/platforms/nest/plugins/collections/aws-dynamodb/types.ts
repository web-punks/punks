import { AttributeValue } from "@aws-sdk/client-dynamodb"

export type DynamoItem = {
  [key: string]: AttributeValue
}

export type RawScanDynamoTableInput = {
  index?: string
  pageSize: number
  filter?: string
  callback: (data: DynamoItem[]) => void
}

export type ScanDynamoTableInput<T> = {
  index?: string
  pageSize: number
  filter?: string
  callback: (data: T[]) => void
}

export type ScanDynamoTableItemsInput = {
  index?: string
  pageSize: number
  filter?: string
}

export type SortDirection = "asc" | "desc"

export type QueryDynamoTablePageInputBase = {
  index?: string
  pageSize: number
  cursor?: string
  direction?: SortDirection
  keyFilter?: string // this filter is applied on specified (or default if unspecified) dynamo index
  dataFilter?: string // this filter could be applied on all record fields (but it is more expensive than keyFilter)
}

export type QueryDynamoTablePageInputRaw = QueryDynamoTablePageInputBase & {
  filterAttributes?: DynamoItem // the collection of attributes specified ether in keyFilter or dataFilter
}

export type QueryDynamoTablePageInput = QueryDynamoTablePageInputBase & {
  filterAttributes?: any
}

export type QueryDynamoTablePageRawResult = {
  items: DynamoItem[]
  cursor?: string
}

export type QueryDynamoTablePageResult<T> = {
  items: T[]
  cursor?: string
}

export type RawQueryDynamoTableInput = {
  index?: string
  pageSize: number
  keyFilter?: string
  attributesFilter?: string
  filterAttributes?: DynamoItem
  callback: (data: DynamoItem[]) => void
}

export type QueryDynamoTableInput<T> = {
  index?: string
  pageSize: number
  keyFilter?: string
  callback: (data: T[]) => void
}

export type QueryDynamoTableItemsInput = {
  index?: string
  pageSize: number
  keyFilter?: string
  attributesFilter?: string
  filterAttributes?: DynamoItem
  direction?: SortDirection
}
