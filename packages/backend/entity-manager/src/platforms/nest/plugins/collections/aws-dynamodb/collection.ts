import {
  DeleteItemCommand,
  DynamoDBClient,
  GetItemCommand,
  PutItemCommand,
  QueryCommand,
  ScanCommand,
  ScanCommandOutput,
} from "@aws-sdk/client-dynamodb"
import { marshall, unmarshall } from "@aws-sdk/util-dynamodb"
import {
  DynamoItem,
  QueryDynamoTableInput,
  QueryDynamoTableItemsInput,
  QueryDynamoTablePageInput,
  QueryDynamoTablePageInputRaw,
  QueryDynamoTablePageRawResult,
  QueryDynamoTablePageResult,
  RawQueryDynamoTableInput,
  RawScanDynamoTableInput,
  ScanDynamoTableInput,
  ScanDynamoTableItemsInput,
} from "./types"
import { awsDynamoDbSettings } from "./settings"

export type DynamoDbPrimaryKey = {
  partitionKey: string
  sortKey?: string
}

export type DynamoDbCollectionOptions = {
  tableName: string
  primaryKey: DynamoDbPrimaryKey
}

export class DynamoDbCollection {
  private client: DynamoDBClient
  constructor(private readonly settings: DynamoDbCollectionOptions) {
    this.client = this.getClient()
  }

  async queryTableItems<T>(input: QueryDynamoTableItemsInput): Promise<T[]> {
    const results: T[] = []
    await this.queryTable<T>({
      ...input,
      callback: (items) => {
        results.push(...items)
      },
    })
    return results
  }

  async queryTable<T>(input: QueryDynamoTableInput<T>) {
    const { callback, ...data } = input
    return await this.rawQueryTable({
      ...data,
      callback: (items) =>
        callback(items.map((x) => this.deserializeItem<T>(x))),
    })
  }

  protected async rawQueryTable(input: RawQueryDynamoTableInput) {
    const { callback } = input
    let cursor = undefined
    while (true) {
      const buildCommand = new QueryCommand({
        TableName: this.settings.tableName,
        IndexName: input.index,
        Limit: input.pageSize,
        ExclusiveStartKey: cursor,
        KeyConditionExpression: input.keyFilter,
        FilterExpression: input.attributesFilter,
        ExpressionAttributeValues: input.filterAttributes,
      })
      const result: any = await this.client.send(buildCommand)
      callback(result.Items)
      cursor = result.LastEvaluatedKey
      if (!result.LastEvaluatedKey) {
        return
      }
    }
  }

  async queryTablePage<T>(
    input: QueryDynamoTablePageInput
  ): Promise<QueryDynamoTablePageResult<T>> {
    const result = await this.queryTablePageRaw({
      ...input,
      filterAttributes: input.filterAttributes
        ? marshall(input.filterAttributes, {
            removeUndefinedValues: true,
          })
        : undefined,
    })
    return {
      items: result.items.map((x) => this.deserializeItem<T>(x)),
      cursor: result.cursor,
    }
  }

  protected async queryTablePageRaw(
    input: QueryDynamoTablePageInputRaw
  ): Promise<QueryDynamoTablePageRawResult> {
    const result = await this.client.send(
      new QueryCommand({
        TableName: this.settings.tableName,
        IndexName: input.index,
        Limit: input.pageSize,
        ExclusiveStartKey: input.cursor
          ? this.deserializeCursor(input.cursor)
          : undefined,
        KeyConditionExpression: input.keyFilter,
        FilterExpression: input.dataFilter,
        ExpressionAttributeValues: input.filterAttributes,
        ScanIndexForward: input.direction !== "desc",
      })
    )
    return {
      items: result.Items ?? [],
      cursor: result.LastEvaluatedKey
        ? this.serializeCursor(result.LastEvaluatedKey)
        : undefined,
    }
  }

  async scanTableItems<T>(input: ScanDynamoTableItemsInput) {
    const items: T[] = []
    await this.scanTable<T>({
      ...input,
      callback: (x: T[]) => items.push(...x),
    })
    return items
  }

  async scanTable<T>(input: ScanDynamoTableInput<T>) {
    const { callback, ...data } = input
    return await this.rawScanTable({
      ...data,
      callback: (items) =>
        callback(items.map((x) => this.deserializeItem<T>(x))),
    })
  }

  protected async rawScanTable(input: RawScanDynamoTableInput) {
    const { callback } = input
    let cursor = undefined
    while (true) {
      const result: ScanCommandOutput = await this.client.send(
        new ScanCommand({
          TableName: this.settings.tableName,
          IndexName: input.index,
          Limit: input.pageSize,
          FilterExpression: input.filter,
          ExclusiveStartKey: cursor,
        })
      )

      if (result.Items) {
        callback(result.Items)
      }
      cursor = result.LastEvaluatedKey
      if (!result.LastEvaluatedKey) {
        return
      }
    }
  }

  async itemExists(key: DynamoItem) {
    return !!(await this.getItemRaw(key))
  }

  async getItem<T>(key: DynamoItem): Promise<T | undefined> {
    const item = await this.getItemRaw(key)
    const el = item ? this.deserializeItem<T>(item) : undefined
    return el
  }

  protected async getItemRaw(key: DynamoItem): Promise<DynamoItem | undefined> {
    const result = await this.client.send(
      new GetItemCommand({
        TableName: this.settings.tableName,
        Key: key,
      })
    )
    return result.Item
  }

  async putItem<T>(item: T) {
    await this.putItemRaw(this.serializeItem(item))
  }

  protected async putItemRaw(item: DynamoItem) {
    await this.getClient().send(
      new PutItemCommand({
        TableName: this.settings.tableName,
        Item: item,
      })
    )
  }

  async deleteItem(key: DynamoItem) {
    await this.getClient().send(
      new DeleteItemCommand({
        TableName: this.settings.tableName,
        Key: key,
      })
    )
  }

  // *** Utility methods ***

  private deserializeItem = <T>(item: DynamoItem) => unmarshall(item) as T
  private serializeItem = <T>(item: T) =>
    marshall(item, {
      removeUndefinedValues: true,
    })

  private serializeCursor = (cursor: DynamoItem) =>
    cursor ? JSON.stringify(cursor) : undefined
  private deserializeCursor = (cursor: string) =>
    cursor ? (JSON.parse(cursor) as DynamoItem) : undefined

  private getClient() {
    return new DynamoDBClient({
      credentials:
        awsDynamoDbSettings.value.awsAccessKeyId &&
        awsDynamoDbSettings.value.awsSecretAccessKey
          ? {
              accessKeyId: awsDynamoDbSettings.value.awsAccessKeyId,
              secretAccessKey: awsDynamoDbSettings.value.awsSecretAccessKey,
            }
          : undefined,
      region: awsDynamoDbSettings.value.region,
    })
  }
}
