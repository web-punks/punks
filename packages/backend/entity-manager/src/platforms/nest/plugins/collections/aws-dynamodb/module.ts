import { DynamicModule, Module } from "@nestjs/common"
import { AwsDynamoDbSettings, awsDynamoDbSettings } from "./settings"

const ModuleData = {
  providers: [],
  exports: [],
}

@Module({
  ...ModuleData,
})
export class AwsDynamoDbModule {
  /**
   * @deprecated use initialize instead
   */
  static forRoot(input: AwsDynamoDbSettings): DynamicModule {
    awsDynamoDbSettings.initialize(input)
    return {
      module: AwsDynamoDbModule,
      ...ModuleData,
    }
  }

  static initialize(input: AwsDynamoDbSettings) {
    awsDynamoDbSettings.initialize(input)
  }
}
