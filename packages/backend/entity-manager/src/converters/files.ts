import { EntitiesImportInput } from "../abstractions"

export type toEntitiesImportInputOptions<TPayload> = Omit<
  EntitiesImportInput<TPayload>,
  "file"
> & {
  file: Express.Multer.File
}
export const toEntitiesImportInput = <TPayload>(
  request: toEntitiesImportInputOptions<TPayload>
): EntitiesImportInput<TPayload> => ({
  format: request.format,
  file: {
    content: request.file.buffer,
    contentType: request.file.mimetype,
    fileName: request.file.originalname,
  },
  payload: request.payload,
  options: request.options,
})
