# Authentication

## Session

Request scoped session is handled by `AsyncLocalStorage` from node `async_hooks` inside `AppSessionMiddleware` class based middleware or `appSessionMiddleware` functional middleware.
