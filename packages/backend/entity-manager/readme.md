# Punks Entity Manager package

This library contains the entity manager of the Punks backend.

## Installation

```bash
npm install @punks/backend-entity-manager @punks/backend-core class-transformer class-validator
```

## Add authentication

```bash
npm install express async_hooks bcrypt @nestjs/platform-express @nestjs/jwt
```

## Supported ORMs:

- [TypeORM](https://typeorm.io/)

```bash
npm install typeorm
```

## Supported email providers:

- [SendGrid](https://sendgrid.com/)

```bash
mpm install @sendgrid/mail
```

- [AWS SES](https://aws.amazon.com/ses/)

```bash
npm install @aws-sdk/client-ses
```

## Supported bucket providers:

- [AWS S3](https://aws.amazon.com/s3/)

```bash
npm install @aws-sdk/client-s3 @aws-sdk/s3-request-presigner
```

## Supported platforms:

- [NestJS](https://nestjs.com/)

```bash
# NestJS with TypeORM
npm install @nestjs/core @nestjs/common @nestjs/event-emitter @nestjs/typeorm @nestjs/swagger
npm install -D pg-mem supertest
```

## Library structure

```
├─ extensions
|  |
|  └─ plugins
|      |
|      └─ email: email plugin
|
├─ platforms
|  |
|  └─ nestjs: nestjs core module
|
├─ actions: entity manager actions
|  |
|  ├── commands
|  |
|  └── queries
|
├─ ioc: dependency injection
|
└─ abstractions: common abstractions
```
