import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppOrganizationalUnitTypeCreateDto,
  AppOrganizationalUnitTypeDto,
  AppOrganizationalUnitTypeListItemDto,
  AppOrganizationalUnitTypeUpdateDto,
} from "./appOrganizationalUnitType.dto"
import { AppOrganizationalUnitTypeEntity } from "../../database/core/entities/appOrganizationalUnitType.entity"
import { toAppOrganizationReferenceDto } from "../appOrganizations/appOrganization.utils"

@WpEntityConverter("appOrganizationalUnitType")
export class AppOrganizationalUnitTypeConverter
  implements
    IEntityConverter<
      AppOrganizationalUnitTypeEntity,
      AppOrganizationalUnitTypeDto,
      AppOrganizationalUnitTypeListItemDto,
      AppOrganizationalUnitTypeCreateDto,
      AppOrganizationalUnitTypeUpdateDto
    >
{
  toListItemDto(
    entity: AppOrganizationalUnitTypeEntity
  ): AppOrganizationalUnitTypeListItemDto {
    return this.toEntityDto(entity)
  }

  toEntityDto(
    entity: AppOrganizationalUnitTypeEntity
  ): AppOrganizationalUnitTypeDto {
    const { allowedChildrenTypes, ...other } = entity
    return {
      ...other,
      allowedChildrenTypes: allowedChildrenTypes.map((x) => ({
        id: x.childType.id,
        name: x.childType.name,
        uid: x.childType.uid,
      })),
      organization: entity.organization
        ? toAppOrganizationReferenceDto(entity.organization)
        : undefined,
    }
  }

  createDtoToEntity(
    input: AppOrganizationalUnitTypeCreateDto
  ): DeepPartial<AppOrganizationalUnitTypeEntity> {
    const { organizationId, allowedChildrenTypes, ...params } = input
    return {
      ...params,
      allowedChildrenTypes: allowedChildrenTypes.map((x) => ({
        id: x.id,
      })),
      organization: organizationId
        ? {
            id: organizationId,
          }
        : undefined,
    }
  }

  updateDtoToEntity(
    input: AppOrganizationalUnitTypeUpdateDto
  ): DeepPartial<AppOrganizationalUnitTypeEntity> {
    const { allowedChildrenTypes, ...params } = input
    return {
      ...params,
      allowedChildrenTypes: allowedChildrenTypes.map((x) => ({
        id: x.id,
      })),
    }
  }
}
