import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppMediaFolderCreateDto,
  AppMediaFolderDto,
  AppMediaFolderListItemDto,
  AppMediaFolderUpdateDto,
} from "./appMediaFolder.dto"
import { AppMediaFolderEntity } from "../../database/core/entities/appMediaFolder.entity"
import { toAppOrganizationReferenceDto } from "../appOrganizations/appOrganization.utils"

@WpEntityConverter("appMediaFolder")
export class AppMediaFolderConverter
  implements
    IEntityConverter<
      AppMediaFolderEntity,
      AppMediaFolderDto,
      AppMediaFolderListItemDto,
      AppMediaFolderCreateDto,
      AppMediaFolderUpdateDto
    >
{
  toListItemDto(entity: AppMediaFolderEntity): AppMediaFolderListItemDto {
    return {
      ...entity,
      organization: entity.organization
        ? toAppOrganizationReferenceDto(entity.organization)
        : undefined,
    }
  }

  toEntityDto(entity: AppMediaFolderEntity): AppMediaFolderDto {
    return {
      ...this.toListItemDto(entity),
    }
  }

  createDtoToEntity(
    input: AppMediaFolderCreateDto
  ): DeepPartial<AppMediaFolderEntity> {
    const { organizationId, ...params } = input
    return {
      ...params,
      parent: input.parentId ? { id: input.parentId } : undefined,
      organization: organizationId
        ? {
            id: organizationId,
          }
        : undefined,
    }
  }

  updateDtoToEntity(
    input: AppMediaFolderUpdateDto
  ): DeepPartial<AppMediaFolderEntity> {
    const { organizationId, ...params } = input
    return {
      ...params,
      parent: input.parentId ? { id: input.parentId } : undefined,
      organization: organizationId
        ? {
            id: organizationId,
          }
        : undefined,
    }
  }
}
