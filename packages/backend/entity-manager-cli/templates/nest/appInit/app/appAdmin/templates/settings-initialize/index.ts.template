import {
  IPipelineTemplateBuilder,
  NestPipelineTemplate,
  PipelineDefinition,
  WpPipeline,
} from "@punks/backend-entity-manager"
import { AppAuthContext } from "@/infrastructure/authentication"
import { AppOrganizationEntityManager } from "@/entities/appOrganizations/appOrganization.manager"
import { OrganizationSecretsService } from "@/middleware/settings/services/secrets-service"
import { OrganizationSettingsService } from "@/middleware/settings/services/settings-service"
import { DefaultSettings } from "../../appAdmin.settings.defaults"

@WpPipeline("SettingsInitialize")
export class SettingsInitializeTemplate extends NestPipelineTemplate<
  void,
  void,
  AppAuthContext
> {
  constructor(
    private readonly organizations: AppOrganizationEntityManager,
    private readonly secrets: OrganizationSecretsService,
    private readonly settings: OrganizationSettingsService
  ) {
    super()
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<void, AppAuthContext>
  ): PipelineDefinition<void, void, AppAuthContext> {
    return builder
      .addStep((step) => {
        step.addOperation({
          name: "Organization settings init",
          action: async () => {
            const organizations =
              await this.organizations.manager.search.execute({})
            for (const organization of organizations.items) {
              await this.secrets.initializeOrganizationSettings(organization.id)

              await this.settings.setInternalNotificationRecipients(
                DefaultSettings.InternalNotificationRecipients
              )
            }
          },
        })
      })
      .complete()
  }
}
