import {
  IPipelineTemplateBuilder,
  NestPipelineTemplate,
  PipelineDefinition,
  WpPipeline,
} from "@punks/backend-entity-manager"
import { AppAuthContext } from "@/infrastructure/authentication"
import { OrganizationSeedInput } from "./models"
import { AppOrganizationalUnitTypeActions } from "@/app/appOrganizationalUnitTypes/appOrganizationalUnitType.actions"
import { AppOrganizationalUnitTypeEntityManager } from "@/entities/appOrganizationalUnitTypes/appOrganizationalUnitType.manager"
import { AppLanguageEntityManager } from "@/entities/appLanguages/appLanguage.manager"
import { AppCurrencyEntityManager } from "@/entities/appCurrencies/appCurrency.manager"

@WpPipeline("OrganizationSeed")
export class OrganizationSeedTemplate extends NestPipelineTemplate<
  OrganizationSeedInput,
  void,
  AppAuthContext
> {
  constructor(
    private readonly organizationalUnitTypeManager: AppOrganizationalUnitTypeEntityManager,
    private readonly organizationalUnitTypeActions: AppOrganizationalUnitTypeActions,
    private readonly languages: AppLanguageEntityManager,
    private readonly currencies: AppCurrencyEntityManager
  ) {
    super()
  }

  protected buildTemplate(
    builder: IPipelineTemplateBuilder<OrganizationSeedInput, AppAuthContext>
  ): PipelineDefinition<OrganizationSeedInput, void, AppAuthContext> {
    return builder
      .addStep((step) => {
        step.addOperation({
          name: "Organizational unit types ensure",
          action: async (_, state) => {
            for (const organizationalUnitType of state.pipelineInput
              .organizationalUnitTypes) {
              const current =
                await this.organizationalUnitTypeManager.findByUid({
                  uid: organizationalUnitType.uid,
                  organizationId: state.pipelineInput.organizationId,
                })
              if (!current) {
                await this.organizationalUnitTypeActions.create({
                  allowAsRoot: organizationalUnitType.allowAsRoot,
                  name: organizationalUnitType.name,
                  uid: organizationalUnitType.uid,
                  organizationId: state.pipelineInput.organizationId,
                })
              }
            }

            for (const organizationalUnitType of state.pipelineInput
              .organizationalUnitTypes) {
              const current =
                await this.organizationalUnitTypeManager.findByUid({
                  uid: organizationalUnitType.uid,
                  organizationId: state.pipelineInput.organizationId,
                })
              await this.organizationalUnitTypeActions.update(current.id, {
                allowAsRoot: organizationalUnitType.allowAsRoot,
                name: organizationalUnitType.name,
                uid: organizationalUnitType.uid,
                allowedChildrenTypes: await Promise.all(
                  organizationalUnitType.allowedChildren.map((x) =>
                    this.organizationalUnitTypeManager.findByUid({
                      uid: x,
                      organizationId: state.pipelineInput.organizationId,
                    })
                  )
                ),
              })
            }
          },
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Organizational languages ensure",
          action: async (_, state) => {
            for (const language of state.pipelineInput.organizationLanguages) {
              await this.languages.manager.upsertBy.execute({
                filter: {
                  uid: {
                    eq: language.uid,
                  },
                },
                data: {
                  name: language.name,
                  uid: language.uid,
                  organizationId: state.pipelineInput.organizationId,
                },
              })
            }
          },
        })
      })
      .addStep((step) => {
        step.addOperation({
          name: "Organizational currencies ensure",
          action: async (_, state) => {
            for (const currency of state.pipelineInput.organizationCurrencies) {
              await this.currencies.manager.upsertBy.execute({
                filter: {
                  uid: {
                    eq: currency.uid,
                  },
                },
                data: {
                  name: currency.name,
                  uid: currency.uid,
                  code: currency.code,
                  symbol: currency.symbol,
                  organizationId: state.pipelineInput.organizationId,
                },
              })
            }
          },
        })
      })
      .complete()
  }
}
