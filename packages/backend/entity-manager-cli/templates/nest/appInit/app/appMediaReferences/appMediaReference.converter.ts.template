import {
  IEntityConverter,
  WpEntityConverter,
  DeepPartial,
} from "@punks/backend-entity-manager"
import {
  AppMediaReferenceCreateDto,
  AppMediaReferenceDto,
  AppMediaReferenceListItemDto,
  AppMediaReferenceUpdateDto,
} from "./appMediaReference.dto"
import { AppMediaReferenceEntity } from "../../database/core/entities/appMediaReference.entity"
import { toAppOrganizationReferenceDto } from "../appOrganizations/appOrganization.utils"

@WpEntityConverter("appMediaReference")
export class AppMediaReferenceConverter
  implements
    IEntityConverter<
      AppMediaReferenceEntity,
      AppMediaReferenceDto,
      AppMediaReferenceListItemDto,
      AppMediaReferenceCreateDto,
      AppMediaReferenceUpdateDto
    >
{
  toListItemDto(entity: AppMediaReferenceEntity): AppMediaReferenceListItemDto {
    return {
      ...entity,
      organization: entity.organization
        ? toAppOrganizationReferenceDto(entity.organization)
        : undefined,
    }
  }

  toEntityDto(entity: AppMediaReferenceEntity): AppMediaReferenceDto {
    return {
      ...this.toListItemDto(entity),
    }
  }

  createDtoToEntity(
    input: AppMediaReferenceCreateDto
  ): DeepPartial<AppMediaReferenceEntity> {
    const { organizationId, ...params } = input
    return {
      ...params,
      organization: organizationId
        ? {
            id: organizationId,
          }
        : undefined,
    }
  }

  updateDtoToEntity(
    input: AppMediaReferenceUpdateDto
  ): DeepPartial<AppMediaReferenceEntity> {
    const { organizationId, ...params } = input
    return {
      ...params,
      organization: organizationId
        ? {
            id: organizationId,
          }
        : undefined,
    }
  }
}
