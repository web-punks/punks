import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryColumn,
  UpdateDateColumn,
} from "typeorm"
import { EntityVersionOperation, WpEntity } from "{{ packageName }}"

export type AppEntityVersionEntityId = string

@Entity("appEntityVersions")
@WpEntity("appEntityVersion")
@Index("entityId_entityType", ["entityId", "entityType"])
export class AppEntityVersionEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppEntityVersionEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  entityId: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  entityType: string

  @Column({
    type: "jsonb",
    nullable: true,
  })
  data: any

  @Column({ type: "enum", enum: EntityVersionOperation })
  operationType: EntityVersionOperation

  @Column({ type: "uuid", nullable: true })
  operationUserId?: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
