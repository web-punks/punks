import { INestApplication } from "@nestjs/common"
import {
  ConsoleLogger,
  Log,
  LogLevel,
  MetaSerializationType,
} from "@punks/backend-core"
import { ModulesContainer } from "@nestjs/core"
import { EntityManagerInitializer } from "@punks/backend-entity-manager"
import { setupSwagger } from "./infrastructure/swagger/initialize"
import { getEntityManagerSettings } from "./settings"
import { AppAuthContextProvider } from "./app/appAuth/context"

export const setupServer = async (app: INestApplication) => {
  try {
    const logLevel = (process.env.LOG_LEVEL as LogLevel) ?? LogLevel.Info
    Log.configure({
      provider: new ConsoleLogger(),
      options: {
        enabled: true,
        level: logLevel,
        serialization:
          process.env.LOGGING_SERIALIZE_META === "true"
            ? MetaSerializationType.JSON
            : MetaSerializationType.None,
      },
    })
    Log.configure({
      provider: new DatadogLogger({
        datadog: {
          apiKey: process.env.DATADOG_API_KEY,
          site: process.env.DATADOG_SITE,
        },
        service: {
          appName: process.env.APP_NAME,
          roleName: process.env.ROLE_NAME,
          serviceName: process.env.SERVICE_NAME,
          environmentName: process.env.ENVIRONMENT_NAME,
        },
      }),
      options: {
        enabled: process.env.DATADOG_ENABLED === "true",
        level: logLevel,
        serialization: MetaSerializationType.None,
      },
    })
    Log.configure({
      provider: new FileLogger({
        service: {
          appName: process.env.APP_NAME,
          roleName: process.env.ROLE_NAME,
          serviceName: process.env.SERVICE_NAME,
          environmentName: process.env.ENVIRONMENT_NAME,
        },
      }),
      options: {
        enabled: process.env.FILE_LOGGING_ENABLED === "true",
        level: logLevel,
        serialization: MetaSerializationType.None,
      },
    })

    Log.getLogger("Main").info(`Logging initialized -> level:${logLevel}`)

    if (process.env.SWAGGER_ENABLED?.toString() === "true") {
      Log.getLogger("Main").info("Setting up swagger")
      setupSwagger(app, {
        apiPath: "swagger",
        description: "Punks Platform Api",
        title: "Punks Platform Api",
        version: "1.0",
      })
    } else {
      Log.getLogger("Main").info("Swagger is disabled")
    }

    await app.get(EntityManagerInitializer).initialize(app, {
      modulesContainer: app.get(ModulesContainer),
      authenticationProvider: app.get(AppAuthContextProvider),
      settings: getEntityManagerSettings(),
    })
    await app.init()

    return {
      success: true,
    }
  } catch (error) {
    Log.getLogger("Main").exception(
      "Failed to initialize entity manager",
      error
    )
    return {
      success: false,
      reason: error.message,
    }
  }
}
