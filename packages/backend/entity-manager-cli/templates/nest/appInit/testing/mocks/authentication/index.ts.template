import { faker } from "@faker-js/faker"
import { InstanceInitializeTemplate } from "@/app/appAdmin/templates/instance-initialize"
import { AppDirectoryEntity } from "@/database/core/entities/appDirectory.entity"
import { AppOrganizationEntity } from "@/database/core/entities/appOrganization.entity"
import { AppTenantEntity } from "@/database/core/entities/appTenant.entity"
import { INestApplication } from "@nestjs/common"
import {
  AuthGuard,
  AuthenticationService,
  newUuid,
} from "@punks/backend-entity-manager"
import { DataSource } from "typeorm"
import { ApplicationAuthenticationContext } from "./types"
import {
  AuthUserContext,
  AuthUserRegistrationData,
} from "@/infrastructure/authentication/models"
import { AppLanguageEntity } from "@/database/core/entities/appLanguage.entity"
import { AppRoles } from "@/middleware/authentication/roles"
import { AppPermissions } from "@/middleware/authentication/permissions"
import { AppAuthContextProvider } from "@/app/appAuth/context"

export const mockAuthenticatedUser = (
  auth: AppAuthContextProvider,
  guard: AuthGuard
) => {
  jest.spyOn(auth, "getContext").mockImplementation(async () => ({
    isAnonymous: false,
    isAuthenticated: true,
  }))
  jest.spyOn(guard, "canActivate").mockImplementation(() => true)
}

export const createAuthenticationContext = async (
  app: INestApplication<any>,
  dataSource: DataSource
) => {
  await app.get(InstanceInitializeTemplate).invoke({
    roles: Object.values(AppRoles),
    permission: Object.values(AppPermissions),
  })

  const tenantId = newUuid()
  const tenantsRepo = dataSource.getRepository(AppTenantEntity)
  const organizationsRepo = dataSource.getRepository(AppOrganizationEntity)
  const directoriesRepo = dataSource.getRepository(AppDirectoryEntity)
  const languagesRepo = dataSource.getRepository(AppLanguageEntity)

  await tenantsRepo.insert({
    id: tenantId,
    name: "Test Tenant",
    uid: "test-tenant",
  })
  const tenant = await tenantsRepo.findOneByOrFail({
    id: tenantId,
  })

  await directoriesRepo.insert({
    id: newUuid(),
    name: "Test Directory",
    uid: "test-directory",
    default: true,
    tenant,
  })

  const directory = await directoriesRepo.findOneByOrFail({
    uid: "test-directory",
  })

  const organizationId = newUuid()
  await organizationsRepo.insert({
    id: organizationId,
    name: "Test Organization",
    uid: "test-organization",
    tenant,
  })

  const organization = await organizationsRepo.findOneByOrFail({
    id: organizationId,
  })

  await languagesRepo.insert({
    id: newUuid(),
    name: "Italiano",
    uid: "it",
    organization,
  })
  await languagesRepo.insert({
    id: newUuid(),
    name: "English",
    uid: "en",
    organization,
  })
  const languages = await languagesRepo.find({
    where: {
      organization: {
        id: organizationId,
      },
    },
  })

  return {
    tenant,
    organization,
    directory,
    languages,
  }
}

export const createUser = async (
  app: INestApplication<any>,
  context: ApplicationAuthenticationContext,
  data: {
    roleUid?: string
  }
) => {
  const auth = app.get(AuthenticationService)
  const email = faker.internet.email()
  const user = await auth.userCreate<AuthUserRegistrationData, AuthUserContext>(
    {
      email,
      password: newUuid(),
      userName: email,
      registrationInfo: {
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
        birthDate: faker.date.past().toISOString(),
      },
      context: {
        tenantUid: context.tenant.uid,
        organizationUid: context.organization.uid,
      },
    }
  )

  if (!user.success) {
    throw new Error("User creation failed")
  }

  if (data.roleUid) {
    await auth.userRolesService.addUserToRoleByUid(user.userId, data.roleUid)
  }

  return await auth.usersService.getById(user.userId)
}
