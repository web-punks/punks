import { AppMediaFolderEntity } from "@/database/core/entities/appMediaFolder.entity"
import { AppMediaFolderEntityManager } from "@/entities/appMediaFolders/appMediaFolder.manager"
import { AuthenticationContextService } from "@/infrastructure/authentication/services/context"
import {
  IMediaFolderRepository,
  MediaFolderRecord,
  WpMediaFolderRepository,
} from "@punks/backend-entity-manager"

const toMediaFolderRecord = (
  entity: AppMediaFolderEntity
): MediaFolderRecord => ({
  id: entity.id,
  name: entity.name,
  createdOn: entity.createdOn,
  updatedOn: entity.updatedOn,
})

@WpMediaFolderRepository()
export class MediaFolderRepositoryManager implements IMediaFolderRepository {
  constructor(
    private readonly folders: AppMediaFolderEntityManager,
    private readonly context: AuthenticationContextService
  ) {}

  async folderCreate(input: {
    name: string
    parentId?: string
  }): Promise<MediaFolderRecord> {
    const item = await this.folders.manager.create.execute({
      name: input.name,
      parent: input.parentId ? { id: input.parentId } : null,
      organization: {
        id: (await this.context.getContext()).userContext.organizationId,
      },
    })
    const folder = await this.getFolderEntity(item.id)
    return toMediaFolderRecord(folder)
  }

  async folderRename(id: string, name: string): Promise<void> {
    await this.folders.manager.update.execute(id, { name })
  }

  async folderMove(id: string, parentId?: string): Promise<void> {
    await this.folders.manager.update.execute(id, {
      parent: parentId ? { id: parentId } : null,
    })
  }

  async folderDelete(id: string): Promise<void> {
    await this.folders.manager.delete.execute(id)
  }

  async folderFind(
    name: string,
    parentId?: string
  ): Promise<MediaFolderRecord> {
    const folder = await this.folders.manager.find.execute({
      filters: {
        parentId,
        name: {
          ieq: name,
        },
      },
    })
    return folder ? toMediaFolderRecord(folder) : undefined
  }

  async foldersList(parentId?: string): Promise<MediaFolderRecord[]> {
    const { items } = await this.folders.manager.search.execute({
      filters: {
        parentId,
        rootFolder: !parentId,
      },
    })
    return items.map((x) => toMediaFolderRecord(x))
  }

  private async getFolderEntity(id: string) {
    return await this.folders.manager.get.execute(id)
  }
}
