import { ApiProperty } from "@nestjs/swagger"
import { IEntityFacet, IEntityFacetValue } from "{{ packageName }}"

export class NumericFacetValue implements IEntityFacetValue<number> {
  @ApiProperty()
  value: number

  @ApiProperty()
  label: string

  @ApiProperty()
  count: number
}

export class NumericFacets implements IEntityFacet<number> {
  @ApiProperty({ type: [NumericFacetValue] })
  values: NumericFacetValue[]
}

export class BooleanFacetValue implements IEntityFacetValue<boolean> {
  @ApiProperty()
  value: boolean

  @ApiProperty()
  label: string

  @ApiProperty()
  count: number
}

export class BooleanFacets implements IEntityFacet<boolean> {
  @ApiProperty({ type: [BooleanFacetValue] })
  values: BooleanFacetValue[]
}

export class StringFacetValue implements IEntityFacetValue<string> {
  @ApiProperty()
  value: string

  @ApiProperty()
  label: string

  @ApiProperty()
  count: number
}

export class StringFacets implements IEntityFacet<string> {
  @ApiProperty({ type: [StringFacetValue] })
  values: StringFacetValue[]
}
