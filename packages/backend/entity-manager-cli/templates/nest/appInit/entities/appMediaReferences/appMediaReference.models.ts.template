import { ApiProperty } from "@nestjs/swagger"
import { DeepPartial } from "@punks/backend-entity-manager"
import { AppMediaReferenceEntity } from "../../database/core/entities/appMediaReference.entity"

export type AppMediaReferenceCreateData = DeepPartial<
  Omit<AppMediaReferenceEntity, "id">
>
export type AppMediaReferenceUpdateData = DeepPartial<
  Omit<AppMediaReferenceEntity, "id">
>

export enum AppMediaReferenceSorting {
  Name = "Name",
}

export type AppMediaReferenceCursor = number

export class AppMediaReferenceSearchFilters {
  @ApiProperty({ required: false })
  ref?: string

  @ApiProperty({ required: false })
  folderId?: string

  @ApiProperty({ required: false })
  rootFolder?: boolean
}

export class AppMediaReferenceFacets {}

export type AppMediaReferenceSheetItem = AppMediaReferenceEntity
