import { FindOptionsOrder, FindOptionsWhere } from "typeorm"
import {
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import {
  AppLanguageEntity,
  AppLanguageEntityId,
} from "../../database/core/entities/appLanguage.entity"
import { AppLanguageFacets, AppLanguageSorting } from "./appLanguage.models"
import { AppLanguageSearchParameters } from "./appLanguage.types"
import {
  AppAuthContext,
  AppUserContext,
} from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("appLanguage")
export class AppLanguageQueryBuilder extends NestTypeOrmQueryBuilder<
  AppLanguageEntity,
  AppLanguageEntityId,
  AppLanguageSearchParameters,
  AppLanguageSorting,
  AppLanguageFacets,
  AppUserContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("appLanguage", registry)
  }

  protected buildContextFilter(
    context?: AppAuthContext
  ):
    | FindOptionsWhere<AppLanguageEntity>
    | FindOptionsWhere<AppLanguageEntity>[] {
    return {
      ...(context?.userContext?.organizationId
        ? {
            organization: {
              id: context?.userContext?.organizationId,
            },
          }
        : {}),
    }
  }

  protected buildSortingClause(
    request: AppLanguageSearchParameters
  ): FindOptionsOrder<AppLanguageEntity> {
    switch (request.sorting?.fields[0]?.field) {
      default:
        return {}
    }
  }

  protected buildWhereClause(
    request: AppLanguageSearchParameters
  ):
    | FindOptionsWhere<AppLanguageEntity>
    | FindOptionsWhere<AppLanguageEntity>[] {
    return {
      ...(request.filters?.id && {
        id: this.clause.idFilter(request.filters.id),
      }),
      ...(request.filters?.uid && {
        uid: this.clause.idFilter(request.filters.uid),
      }),
    }
  }

  protected calculateFacets(
    request: AppLanguageSearchParameters,
    context?: AppAuthContext
  ): Promise<AppLanguageFacets> {
    return Promise.resolve({})
  }
}
