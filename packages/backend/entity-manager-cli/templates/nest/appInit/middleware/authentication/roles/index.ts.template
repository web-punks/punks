import { AppOrganizationalUnitType } from "../ou"
import { AppPermissions } from "../permissions"

export enum AppRoleType {
  TenantAdmin = "tenant-admin",
  OrganizationAdmin = "organization-admin",
  OrganizationManager = "organization-manager",
}

export type AppRoleDefinition = {
  uid: AppRoleType
  name: string
  assignableRoles: AppRoleType[]
  organizationalUnit?: {
    allowAsRoot: boolean
    allowedOrganizationalUnits: AppOrganizationalUnitType[]
  }
}

export const AppRoles = {
  TenantAdmin: {
    uid: AppRoleType.TenantAdmin,
    name: "Tenant Admin",
    permissions: [
      AppPermissions.AccountsFullAccess,
      AppPermissions.AuditFullAccess,
      AppPermissions.JsonInspect,
      AppPermissions.OrganizationFullAccess,
      AppPermissions.TenantFullAccess,
      AppPermissions.UserImpersonate,
    ],
    assignableRoles: [
      AppRoleType.TenantAdmin,
      AppRoleType.OrganizationAdmin,
      AppRoleType.OrganizationManager,
    ],
    organizationalUnit: {
      allowAsRoot: true,
      allowedOrganizationalUnits: [],
    },
  },
  OrganizationAdmin: {
    uid: AppRoleType.OrganizationAdmin,
    name: "Organization Admin",
    permissions: [
      AppPermissions.AccountsFullAccess,
      AppPermissions.AuditFullAccess,
      AppPermissions.JsonInspect,
      AppPermissions.OrganizationFullAccess,
    ],
    assignableRoles: [
      AppRoleType.OrganizationAdmin,
      AppRoleType.OrganizationManager,
    ],
    organizationalUnit: {
      allowAsRoot: true,
      allowedOrganizationalUnits: [],
    },
  },
  OrganizationManager: {
    uid: AppRoleType.OrganizationManager,
    name: "Organization Manager",
    permissions: [
      AppPermissions.AccountsFullAccess,
      AppPermissions.AuditFullAccess,
      AppPermissions.OrganizationFullAccess,
    ],
    assignableRoles: [
      AppRoleType.OrganizationManager,
    ],
    organizationalUnit: {
      allowAsRoot: true,
      allowedOrganizationalUnits: [],
    },
  },
}
