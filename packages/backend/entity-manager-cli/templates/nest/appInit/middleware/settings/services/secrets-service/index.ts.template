import { Injectable } from "@nestjs/common"
import { SecretsService } from "@punks/backend-entity-manager"
import { DefaultSettings } from "../../default-settings"

@Injectable()
export class OrganizationSecretsService {
  constructor(private readonly settingsService: SecretsService) {}

  async getOrganizationSettings(organizationId: string) {
    return await this.settingsService.secretsProvider.getSecrets(
      this.getOrganizationSettingsPath(organizationId)
    )
  }

  async initializeOrganizationSettings(organizationId: string) {
    await this.settingsService.secretsProvider.pageEnsure(
      this.getOrganizationSettingsPath(organizationId),
      {
        tags: [{ key: "organizationId", value: organizationId }],
      }
    )

    for (const secretKey of Object.keys(DefaultSettings)) {
      await this.settingsService.secretsProvider.defineSecret(
        this.getOrganizationSettingsPath(organizationId),
        {
          ...DefaultSettings[secretKey],
          key: secretKey,
        }
      )
    }
  }

  private getOrganizationSettingsPath(organizationId: string) {
    return `organization/${organizationId}`
  }
}
