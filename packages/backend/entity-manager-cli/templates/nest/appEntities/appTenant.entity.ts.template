import {
  Entity,
  Column,
  CreateDateColumn,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
  Index,
} from "typeorm"
import { IAppTenant, WpEntity } from "{{ packageName }}"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { AppUserEntity } from "./appUser.entity"
import { AppDirectoryEntity } from "./appDirectory.entity"

export type AppTenantEntityId = string

@Entity("{{ tableName }}")
@WpEntity("{{ entityId }}")
export class AppTenantEntity implements IAppTenant {
  @PrimaryColumn({ type: "uuid" })
  id: AppTenantEntityId

  @Index()
  @Column({ type: "varchar", length: 255, unique: true })
  uid: string

  @Index()
  @Column({ type: "varchar", length: 255 })
  name: string

  @OneToMany(() => AppOrganizationEntity, (organization) => organization.tenant)
  organizations?: AppOrganizationEntity[]

  @OneToMany(() => AppUserEntity, (user) => user.tenant)
  users?: AppUserEntity[]

  @OneToMany(() => AppDirectoryEntity, (directory) => directory.tenant)
  directories?: AppDirectoryEntity[]

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
