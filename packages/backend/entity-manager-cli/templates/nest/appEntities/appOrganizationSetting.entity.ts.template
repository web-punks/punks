import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Column,
  Index,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "./appOrganization.entity"

export type AppOrganizationSettingEntityId = string

@Entity("appOrganizationSettings")
@WpEntity("appOrganizationSetting", {
  versioning: {
    enabled: true,
  },
})
export class AppOrganizationSettingEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppOrganizationSettingEntityId

  @Index()
  @Column({ type: "varchar", length: 255 })
  uid: string

  @Column({ type: "varchar" })
  value: string

  @ManyToOne(() => AppOrganizationEntity, {
    eager: true,
    nullable: false,
  })
  organization: AppOrganizationEntity

  @Column()
  organizationId: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
