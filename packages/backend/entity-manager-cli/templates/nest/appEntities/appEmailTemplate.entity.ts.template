import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Column,
  Unique,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { LocalizedField } from "@punks/backend-core"
import { ApiProperty } from "@nestjs/swagger"
import { EmailAddresses } from "../models/email"
import { AppEmailLayoutEntity } from "./appEmailLayout.entity"

export type AppEmailTemplateEntityId = string

export class AppEmailTemplateContents {
  @ApiProperty()
  languageCode: string

  @ApiProperty()
  addresses: EmailAddresses

  @ApiProperty({ required: false })
  bodyTemplate?: string

  @ApiProperty({ required: false })
  subjectTemplate?: string
}

@Entity("appEmailTemplates")
@WpEntity("appEmailTemplate")
@Unique("templateUid", ["organization", "uid"])
export class AppEmailTemplateEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppEmailTemplateEntityId

  @Column({ type: "varchar", length: 255 })
  name: string

  @Column({ type: "varchar", length: 255 })
  uid: string

  @Column({ type: "jsonb" })
  contents: LocalizedField<AppEmailTemplateContents>

  @ManyToOne(() => AppEmailLayoutEntity, {
    nullable: false,
  })
  layout: AppEmailLayoutEntity

  @ManyToOne(() => AppOrganizationEntity, {
    eager: true,
    nullable: false,
  })
  organization: AppOrganizationEntity

  @Column()
  organizationId: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
