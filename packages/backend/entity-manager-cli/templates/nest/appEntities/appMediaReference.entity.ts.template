import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Column,
} from "typeorm"
import { WpEntity } from "{{ packageName }}"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { AppMediaFolderEntity } from "./appMediaFolder.entity"

export type AppMediaReferenceEntityId = string

@Entity("appMediaReferences")
@WpEntity("appMediaReference")
export class AppMediaReferenceEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppMediaReferenceEntityId

  @Column({ type: "varchar" })
  fileName: string

  @Column({ type: "varchar", length: 255 })
  fileRef: string

  @Column({ type: "varchar", length: 255, nullable: true })
  contentType: string

  @Column({ type: "varchar", length: 255, nullable: true })
  providerId: string

  @ManyToOne(() => AppMediaFolderEntity, {
    nullable: true,
  })
  folder?: AppMediaFolderEntity

  @Column()
  folderId?: string

  @ManyToOne(() => AppOrganizationEntity, {
    eager: true,
    nullable: false,
  })
  organization: AppOrganizationEntity

  @Column()
  organizationId: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
