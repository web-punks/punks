import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Column,
  Unique,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { AppOrganizationEntity } from "./appOrganization.entity"
import { LocalizedField } from "@punks/backend-core"
import { EmailAddresses } from "../models/email"
import { ApiProperty } from "@nestjs/swagger"

export type AppEmailLayoutEntityId = string

export class AppEmailLayoutContents {
  @ApiProperty()
  languageCode: string

  @ApiProperty()
  defaultAddresses: EmailAddresses

  @ApiProperty({ required: false })
  layoutTemplate?: string
}

@Entity("appEmailLayouts")
@WpEntity("appEmailLayout")
@Unique("layoutUid", ["organization", "uid"])
export class AppEmailLayoutEntity {
  @PrimaryColumn({ type: "uuid" })
  id: AppEmailLayoutEntityId

  @Column({ type: "varchar", length: 255 })
  name: string

  @Column({ type: "varchar", length: 255 })
  uid: string

  @Column({ type: "jsonb" })
  contents: LocalizedField<AppEmailLayoutContents>

  @ManyToOne(() => AppOrganizationEntity, {
    eager: true,
    nullable: false,
  })
  organization: AppOrganizationEntity

  @Column()
  organizationId: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
