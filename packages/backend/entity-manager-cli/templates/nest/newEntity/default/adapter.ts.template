import {
  DeepPartial,
  IAuthenticationContext,
  IEntityAdapter,
  WpEntityAdapter,
  newUuid,
} from "{{ packageName }}"
import { {{ entityName }}Entity } from "{{ imports.entity.entity }}"
import { {{ entityName }}CreateData, {{ entityName }}UpdateData } from "{{ imports.entity.models }}"

@WpEntityAdapter("{{ entityId }}")
export class {{ entityName }}Adapter
  implements
    IEntityAdapter<
      {{ entityName }}Entity,
      {{ entityName }}CreateData,
      {{ entityName }}UpdateData
    >
{
  createDataToEntity(
    data: {{ entityName }}CreateData, 
    context: IAuthenticationContext<unknown>
  ): DeepPartial<{{ entityName }}Entity> {
    return {
      id: newUuid(),
      ...data,
    }
  }

  updateDataToEntity(
    data: {{ entityName }}UpdateData,
    context: IAuthenticationContext<unknown>
  ): DeepPartial<{{ entityName }}Entity> {
    return {
      ...data,
    }
  }
}
