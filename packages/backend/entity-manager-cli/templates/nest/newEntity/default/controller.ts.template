import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import {
  toEntitiesImportInput,
  EntitySerializationFormat,
} from "{{ packageName }}"
import { ApiBodyFile } from "@/shared/interceptors/files"
import { {{ entityName }}Actions } from "{{ imports.entity.actions }}"
import {
  {{ entityName }}Dto,
  {{ entityName }}CreateDto,
  {{ entityName }}UpdateDto,
} from "{{ imports.entity.dto }}"
import {
  {{ entityName }}ExportRequest,
  {{ entityName }}ExportResponse,
  {{ entityName }}ParseResponse,
  {{ entityName }}SampleDownloadRequest,
  {{ entityName }}SampleDownloadResponse,
  {{ entityName }}SearchRequest,
  {{ entityName }}SearchResponse,
  {{ entityName }}VersionsSearchRequest,
  {{ entityName }}VersionsSearchResponse,
} from "{{ imports.entity.appTypes }}"
import { OrganizationManagers } from "@/middleware/authentication/guards"

@OrganizationManagers()
@Controller("{{ endpointPath }}")
export class {{ entityName }}Controller {
  constructor(private readonly actions: {{ entityName }}Actions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "{{ entityId }}Get",
  })
  @ApiOkResponse({
    type: {{ entityName }}Dto,
  })
  async item(@Param("id") id: string): Promise<{{ entityName }}Dto | undefined> {
    return await this.actions.manager.get.execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "{{ entityId }}Create",
  })
  @ApiOkResponse({
    type: {{ entityName }}Dto,
  })
  async create(@Body() data: {{ entityName }}CreateDto): Promise<{{ entityName }}Dto> {
    return await this.actions.manager.create.execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: {{ entityName }}Dto,
  })
  @ApiOperation({
    operationId: "{{ entityId }}Update",
  })
  async update(
    @Param("id") id: string,
    @Body() item: {{ entityName }}UpdateDto
  ): Promise<{{ entityName }}Dto> {
    return await this.actions.manager.update.execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "{{ entityId }}Delete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.manager.delete.execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "{{ entityId }}Search",
  })
  @ApiOkResponse({
    type: {{ entityName }}SearchResponse,
  })
  async search(
    @Body() request: {{ entityName }}SearchRequest
  ): Promise<{{ entityName }}SearchResponse> {
    return await this.actions.manager.search.execute(request.params)
  }

  @Post("parse")
  @ApiOkResponse({
    type: {{ entityName }}ParseResponse,
  })
  @ApiOperation({
    operationId: "{{ entityId }}Parse",
  })
  @ApiBodyFile("file")
  async parse(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ): Promise<{{ entityName }}ParseResponse> {
    return await this.actions.manager.parse.execute({
      format,
      file: {
        content: file.buffer,
        contentType: file.mimetype,
        fileName: file.originalname,
      },
    })
  }

  @Post("import")
  @ApiOperation({
    operationId: "{{ entityId }}Import",
  })
  @ApiBodyFile("file")
  async import(
    @Query("format") format: EntitySerializationFormat,
    @UploadedFile() file: Express.Multer.File
  ) {
    await this.actions.manager.import.execute(
      toEntitiesImportInput({
        file,
        format,
      })
    )
  }

  @Post("export")
  @ApiOperation({
    operationId: "{{ entityId }}Export",
  })
  @ApiOkResponse({
    type: {{ entityName }}ExportResponse,
  })
  async export(
    @Body() request: {{ entityName }}ExportRequest
  ): Promise<{{ entityName }}ExportResponse> {
    const { downloadUrl } = await this.actions.manager.export.execute(request)
    return {
      downloadUrl,
    }
  }

  @Post("sampleDownload")
  @ApiOperation({
    operationId: "{{ entityId }}SampleDownload",
  })
  @ApiOkResponse({
    type: {{ entityName }}SampleDownloadResponse,
  })
  async sampleDownload(
    @Body() request: {{ entityName }}SampleDownloadRequest
  ): Promise<{{ entityName }}SampleDownloadResponse> {
    const { downloadUrl } = await this.actions.manager.sampleDownload.execute(
      request
    )
    return {
      downloadUrl,
    }
  }

  @Post("versions")
  @ApiOperation({
    operationId: "{{ entityId }}Versions",
  })
  @ApiOkResponse({
    type: {{ entityName }}VersionsSearchResponse,
  })
  async versions(
    @Body() request: {{ entityName }}VersionsSearchRequest
  ): Promise<{{ entityName }}VersionsSearchResponse> {
    return await this.actions.manager.versions.execute(request)
  }
}
