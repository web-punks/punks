import {
  Column,
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from "typeorm"
import { WpEntity } from "{{ packageName }}"
import { AppOrganizationEntity } from "./appOrganization.entity"

export type {{ entityName }}EntityId = string

@Entity("{{ tableName }}")
@WpEntity("{{ entityId }}")
export class {{ entityName }}Entity {
  @PrimaryColumn({ type: "uuid" })
  id: {{ entityName }}EntityId

  @ManyToOne(() => AppOrganizationEntity, {
    nullable: false,
  })
  organization: AppOrganizationEntity

  @Column()
  organizationId: string

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
