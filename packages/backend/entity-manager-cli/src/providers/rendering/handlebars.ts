import { AbstractRenderingProvider } from "./base"
import Handlebars from "handlebars"

export class HandlebarsRenderingProvider<
  TContext
> extends AbstractRenderingProvider<TContext> {
  async render(template: string, context: TContext) {
    Handlebars.registerHelper("uppercase", function (str) {
      return str.toUpperCase()
    })

    const compiled = Handlebars.compile(template)
    return compiled(context)
  }
}
