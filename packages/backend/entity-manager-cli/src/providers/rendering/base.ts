export abstract class AbstractRenderingProvider<TContext> {
  abstract render(template: string, context: TContext): Promise<string>
}
