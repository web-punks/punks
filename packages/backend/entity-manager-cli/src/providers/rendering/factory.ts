import { AbstractRenderingProvider } from "./base"
import { HandlebarsRenderingProvider } from "./handlebars"

export type RenderingProviderType = "handlebars"

export class RenderingProviderFactory {
  static resolve<TContext>(
    provider: RenderingProviderType
  ): AbstractRenderingProvider<TContext> {
    switch (provider) {
      case "handlebars":
        return new HandlebarsRenderingProvider<TContext>()
      default:
        throw new Error(`Rendering provider ${provider} not found`)
    }
  }
}
