#!/usr/bin/env node
import "source-map-support/register"
import yargs from "yargs/yargs"
import { hideBin } from "yargs/helpers"
import { nestEntityAddCommand } from "./commands/entity-add"
import { pluralize, toCamelCase } from "@punks/backend-core"
import { DEFAULT_PACKAGE_NAME, DEFAULT_RENDERING_PROVIDER } from "./consts"
import { nestApplicationInitCommand } from "./commands/application-init"
import { DefaultAppEntitiesData } from "./commands/application-init/defaults"
import { EntityRepository } from "./types/nest"
import { DefaultNestProjectStructure } from "./templates/nest/structure"
import { EntityScope } from "./types/multiTenancy"

const DEFAULT_REPOSITORY = "typeorm"
const DEFAULT_ENDPOINT_PREFIX = "v1"

const parser = yargs(hideBin(process.argv))
  .scriptName("cli")
  .command(
    "nest-entity-add",
    "Nest Entity Add",
    (args) =>
      args.options({
        name: { type: "string", alias: "n", demandOption: true },
        pluralName: { type: "string", alias: "pn", demandOption: false },
        databaseName: { type: "string", alias: "db", demandOption: true },
        scope: {
          type: "string",
          alias: "sc",
          choices: ["multiOrganization", "multiTenant", "multiLanguage"],
        },
        repository: {
          type: "string",
          choices: ["typeorm"],
          coerce: (x) => x as EntityRepository,
          alias: "r",
          default: DEFAULT_REPOSITORY,
        },
        // connectors: {
        //   type: "string",
        //   choices: ["firestore"],
        //   coerce: (x) => x as EntityConnector[],
        //   array: true,
        //   alias: "c",
        //   demandOption: false,
        // },
        skipAppModule: { type: "boolean", alias: "noAm", demandOption: false },
        workDir: { type: "string", alias: "wd", demandOption: false },
      }),
    async (argv) => {
      await nestEntityAddCommand(
        {
          databaseName: argv.databaseName,
          id: toCamelCase(argv.name),
          name: argv.name,
          pluralName: argv.pluralName ?? pluralize(argv.name),
          pluralId: toCamelCase(argv.pluralName ?? pluralize(argv.name)),
          tableName: toCamelCase(argv.pluralName ?? pluralize(argv.name)),
          repository: argv.repository,
          scope: argv.scope as EntityScope,
          // connectors: argv.connectors,
          // firestoreDocument: argv.firestoreDocument,
        },
        {
          renderingProvider: DEFAULT_RENDERING_PROVIDER,
          packageName: DEFAULT_PACKAGE_NAME,
          endpointPrefix: DEFAULT_ENDPOINT_PREFIX,
          paths: DefaultNestProjectStructure,
          skipAppModule: argv.skipAppModule,
          workingDirectory: argv.workDir,
        }
      )
    }
  )
  .command(
    "nest-application-init",
    "Nest Application Init",
    (args) =>
      args.options({
        appName: { type: "string", alias: "app", demandOption: true },
        databaseName: { type: "string", alias: "db", demandOption: true },
        repository: {
          type: "string",
          choices: ["typeorm"],
          coerce: (x) => x as EntityRepository,
          alias: "r",
          default: DEFAULT_REPOSITORY,
        },
        workDir: { type: "string", alias: "wd", demandOption: false },
      }),
    async (argv) => {
      await nestApplicationInitCommand(
        {
          appName: argv.appName,
          databaseName: argv.databaseName,
          repository: argv.repository,
          authenticationEntities: DefaultAppEntitiesData,
        },
        {
          renderingProvider: DEFAULT_RENDERING_PROVIDER,
          packageName: DEFAULT_PACKAGE_NAME,
          paths: DefaultNestProjectStructure,
          endpointPrefix: DEFAULT_ENDPOINT_PREFIX,
          workingDirectory: argv.workDir,
        }
      )
    }
  )
  .showHelpOnFail(true)

parser
  .parseAsync()
  .then(() => {})
  .catch((e) => console.error(e))
