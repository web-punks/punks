import { BuildCommandInput } from "../types/commands"

export abstract class AbstractBuilder<TContext> {
  abstract buildCommand(): Promise<BuildCommandInput<TContext>>
}
