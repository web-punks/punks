import {
  getDirectoryFilePaths,
  joinPath,
  last,
  splitPath,
} from "@punks/backend-core"
import {
  NewDatabaseTemplates,
  NewEntityTemplates,
  NewMultiLanguageEntityTemplates,
  NewMultiOrganizationEntityTemplates,
} from "./sources"
import { DefaultNestProjectStructure } from "./structure"
import {
  NestFileTemplate,
  NewDatabaseTemplatePaths,
  NewEntityTemplatePaths,
} from "./types"

export const buildEntityPaths = (
  templates: NewEntityTemplatePaths
): NestFileTemplate[] => [
  {
    templateFilePath: templates.appModule,
    outputFilePath: DefaultNestProjectStructure.entity.appModule,
  },
  {
    templateFilePath: templates.appTypes,
    outputFilePath: DefaultNestProjectStructure.entity.appTypes,
  },
  {
    templateFilePath: templates.actions,
    outputFilePath: DefaultNestProjectStructure.entity.actions,
  },
  {
    templateFilePath: templates.adapter,
    outputFilePath: DefaultNestProjectStructure.entity.adapter,
  },
  {
    templateFilePath: templates.authentication,
    outputFilePath: DefaultNestProjectStructure.entity.authentication,
  },
  {
    templateFilePath: templates.controller,
    outputFilePath: DefaultNestProjectStructure.entity.controller,
  },
  {
    templateFilePath: templates.converter,
    outputFilePath: DefaultNestProjectStructure.entity.converter,
  },
  {
    templateFilePath: templates.dto,
    outputFilePath: DefaultNestProjectStructure.entity.dto,
  },
  {
    templateFilePath: templates.entity,
    outputFilePath: DefaultNestProjectStructure.entity.entity,
  },
  {
    templateFilePath: templates.entityModule,
    outputFilePath: DefaultNestProjectStructure.entity.entityModule,
  },
  {
    templateFilePath: templates.manager,
    outputFilePath: DefaultNestProjectStructure.entity.manager,
  },
  {
    templateFilePath: templates.models,
    outputFilePath: DefaultNestProjectStructure.entity.models,
  },
  {
    templateFilePath: templates.query,
    outputFilePath: DefaultNestProjectStructure.entity.query,
  },
  {
    templateFilePath: templates.repository,
    outputFilePath: DefaultNestProjectStructure.entity.repository,
  },
  {
    templateFilePath: templates.serializer,
    outputFilePath: DefaultNestProjectStructure.entity.serializer,
  },
  {
    templateFilePath: templates.entityTypes,
    outputFilePath: DefaultNestProjectStructure.entity.entityTypes,
  },
]

export const DefaultEntityAddTemplates = buildEntityPaths(NewEntityTemplates)

export const MultiOrganizationEntityAddTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  ...NewMultiOrganizationEntityTemplates,
})

export const MultiLanguageEntityAddTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  ...NewMultiLanguageEntityTemplates,
})

export const buildDatabasePaths = (
  templates: NewDatabaseTemplatePaths
): NestFileTemplate[] => [
  {
    templateFilePath: templates.dataSource,
    outputFilePath: DefaultNestProjectStructure.database.dataSource,
  },
  {
    templateFilePath: templates.entities,
    outputFilePath: DefaultNestProjectStructure.database.entities,
  },
  {
    templateFilePath: templates.module,
    outputFilePath: DefaultNestProjectStructure.database.module,
  },
  {
    templateFilePath: templates.repositories,
    outputFilePath: DefaultNestProjectStructure.database.repositories,
  },
]
export const DefaultDatabaseAddTemplates =
  buildDatabasePaths(NewDatabaseTemplates)

export const getDynamicFolderPaths = async (
  rootDirectory: string
): Promise<NestFileTemplate[]> => {
  const filePaths = await getDirectoryFilePaths(rootDirectory, {
    recursive: true,
  })
  const templatePaths = filePaths.filter((x) => x.endsWith(".template"))
  return templatePaths.map((x) => ({
    outputFilePath: joinPath(
      "src",
      ...splitPath(x.replace(rootDirectory, "").replace(".template", ""))
    ),
    templateFilePath: joinPath(
      last(splitPath(rootDirectory)),
      ...splitPath(x.replace(rootDirectory, ""))
    ),
  }))
}
