import { NewDatabaseTemplates, NewEntityTemplates } from "./sources"
import { DefaultNestProjectStructure } from "./structure"

export type NestFileTemplate = {
  templateFilePath: string
  outputFilePath: string
}

export interface NestFilePathPlaceholders {
  databaseName: string
  entityId: string
  pluralizedEntityId: string
}

export type NestProjectStructure = typeof DefaultNestProjectStructure

export type NewEntityTemplatePaths = typeof NewEntityTemplates
export type NewDatabaseTemplatePaths = typeof NewDatabaseTemplates
