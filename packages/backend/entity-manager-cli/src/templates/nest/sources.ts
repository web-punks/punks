export const NewEntityTemplates = {
  appModule: "newEntity/default/appModule.ts.template",
  appTypes: "newEntity/default/appTypes.ts.template",
  actions: "newEntity/default/actions.ts.template",
  adapter: "newEntity/default/adapter.ts.template",
  authentication: "newEntity/default/authentication.ts.template",
  controller: "newEntity/default/controller.ts.template",
  converter: "newEntity/default/converter.ts.template",
  dto: "newEntity/default/dto.ts.template",
  entity: "newEntity/default/entity.ts.template",
  entityModule: "newEntity/default/entityModule.ts.template",
  models: "newEntity/default/models.ts.template",
  entityTypes: "newEntity/default/entityTypes.ts.template",
  manager: "newEntity/default/manager.ts.template",
  query: "newEntity/default/query.ts.template",
  repository: "newEntity/default/repository.ts.template",
  serializer: "newEntity/default/serializer.ts.template",
}

export const NewMultiOrganizationEntityTemplates = {
  converter: "newEntity/multiOrganization/converter.ts.template",
  dto: "newEntity/multiOrganization/dto.ts.template",
  entity: "newEntity/multiOrganization/entity.ts.template",
  query: "newEntity/multiOrganization/query.ts.template",
}

export const NewMultiLanguageEntityTemplates = {
  converter: "newEntity/multiLanguage/converter.ts.template",
  dto: "newEntity/multiLanguage/dto.ts.template",
  entity: "newEntity/multiLanguage/entity.ts.template",
  models: "newEntity/multiLanguage/models.ts.template",
  query: "newEntity/multiLanguage/query.ts.template",
}

export const NewDatabaseTemplates = {
  dataSource: "newDatabase/typeorm-dataSource.ts.template",
  entities: "newDatabase/entities.ts.template",
  module: "newDatabase/module.ts.template",
  repositories: "newDatabase/repository.ts.template",
}
