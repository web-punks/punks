export const DefaultNestProjectStructure = {
  entity: {
    appModule: "src/app/{pluralizedEntityId}/{entityId}.module.ts",
    appTypes: "src/app/{pluralizedEntityId}/{entityId}.types.ts",
    serializer: "src/app/{pluralizedEntityId}/{entityId}.serializer.ts",
    entity: "src/database/{databaseName}/entities/{entityId}.entity.ts",
    entityModule: "src/entities/{pluralizedEntityId}/{entityId}.module.ts",
    repository:
      "src/database/{databaseName}/repositories/{entityId}.repository.ts",
    actions: "src/app/{pluralizedEntityId}/{entityId}.actions.ts",
    controller: "src/app/{pluralizedEntityId}/{entityId}.controller.ts",
    converter: "src/app/{pluralizedEntityId}/{entityId}.converter.ts",
    dto: "src/app/{pluralizedEntityId}/{entityId}.dto.ts",
    adapter: "src/entities/{pluralizedEntityId}/{entityId}.adapter.ts",
    authentication:
      "src/entities/{pluralizedEntityId}/{entityId}.authentication.ts",
    manager: "src/entities/{pluralizedEntityId}/{entityId}.manager.ts",
    models: "src/entities/{pluralizedEntityId}/{entityId}.models.ts",
    query: "src/entities/{pluralizedEntityId}/{entityId}.query.ts",
    entityTypes: "src/entities/{pluralizedEntityId}/{entityId}.types.ts",
  },
  database: {
    dataSource: "src/database/{databaseName}/dataSource.ts",
    entities: "src/database/{databaseName}/entities.ts",
    module: "src/database/{databaseName}/module.ts",
    repositories: "src/database/{databaseName}/repositories.ts",
  },
  infrastructure: {
    authentication: {
      index: "src/infrastructure/authentication/index.ts",
      types: "src/infrastructure/authentication/types.ts",
    },
  },
  shared: {
    filters: {
      exceptionsFilter: "src/shared/filters/exceptionsFilter.ts",
    },
    module: "src/shared/module.ts",
  },
}
