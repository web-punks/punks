export type EntityScope = "multiOrganization" | "multiTenant" | "multiLanguage"
