import { RenderingProviderType } from "../providers/rendering/factory"
import { NestProjectStructure } from "../templates/nest/types"

export type EntityRepository = "typeorm" | "sequelize" | "firestore"
export type EntityConnector = "firestore"

export type NestProjectSettings = {
  renderingProvider: RenderingProviderType
  packageName: string
  endpointPrefix?: string
  workingDirectory?: string
  fileSuffix?: string
  skipAppModule?: boolean
  paths: NestProjectStructure
}
