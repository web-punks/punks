export type BuildCommandInput<TContext> = {
  templateFile: string
  outputFile: string
  workingDirectory?: string
  fileSuffix?: string
  context: TContext
}
