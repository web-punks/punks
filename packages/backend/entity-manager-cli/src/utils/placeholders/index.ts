const replaceStringPlaceholders = (
  str: string,
  placeholders: Record<string, string>
): string => {
  return str.replace(/{([^{}]+)}/g, (match, placeholder) => {
    return placeholders.hasOwnProperty(placeholder)
      ? placeholders[placeholder]
      : match
  })
}

export const replacePlaceholdersRecursively = <T>(
  item: T,
  placeholders: Record<string, string>
): T => {
  if (typeof item === "string") {
    return replaceStringPlaceholders(item, placeholders) as T
  }

  if (!item || typeof item !== "object") {
    return item
  }

  const replacedObject: any = {}
  for (const [key, value] of Object.entries(item as any)) {
    if (typeof value === "string") {
      replacedObject[key] = replaceStringPlaceholders(value, placeholders)
    } else if (typeof item === "object" && item !== null) {
      replacedObject[key] = replacePlaceholdersRecursively(value, placeholders)
    } else {
      replacedObject[key] = value
    }
  }
  return replacedObject as T
}
