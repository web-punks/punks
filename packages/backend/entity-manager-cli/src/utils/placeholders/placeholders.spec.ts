import { replacePlaceholdersRecursively } from "."

describe("replacePlaceholdersRecursively", () => {
  it("should replace placeholders in a string", () => {
    const item = "Hello, {name}! How are you, {place}?"
    const placeholders = {
      name: "John",
      place: "London",
    }

    const result = replacePlaceholdersRecursively(item, placeholders)

    const expectedResult = "Hello, John! How are you, London?"
    expect(result).toBe(expectedResult)
  })

  it("should replace placeholders in an object recursively", () => {
    const item = {
      greeting: "Hello, {name}!",
      location: {
        city: "Welcome to {place}!",
        country: "Greetings from {country}!",
      },
    }

    const placeholders = {
      name: "Alice",
      place: "New York",
      country: "USA",
    }

    const result = replacePlaceholdersRecursively(item, placeholders)

    const expectedResult = {
      greeting: "Hello, Alice!",
      location: {
        city: "Welcome to New York!",
        country: "Greetings from USA!",
      },
    }

    expect(result).toEqual(expectedResult)
  })

  it("should not replace placeholders that do not exist in the placeholders object", async () => {
    const item = "Hello, {name}! How are you, {place}?"
    const placeholders = {
      name: "Jane",
    }

    const result = replacePlaceholdersRecursively(item, placeholders)

    const expectedResult = "Hello, Jane! How are you, {place}?"
    expect(result).toBe(expectedResult)
  })

  it("should handle nested objects with null and undefined values", () => {
    const item = {
      title: "This is a {type} item",
      content: null,
      nested: {
        property: "Nested property with {value}",
        nestedNull: null,
      },
    }

    const placeholders = {
      type: "sample",
      value: "some value",
    }

    const result = replacePlaceholdersRecursively(item, placeholders)

    const expectedResult = {
      title: "This is a sample item",
      content: null,
      nested: {
        property: "Nested property with some value",
        nestedNull: null,
      },
    }

    expect(result).toEqual(expectedResult)
  })
})
