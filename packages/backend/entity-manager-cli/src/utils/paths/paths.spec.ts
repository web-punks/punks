import { createRelativeImportPath } from "."

describe("createRelativePath", () => {
  it("should create the correct relative import path when paths have different parent directories", () => {
    const sourcePath = "/path/to/source/file1"
    const targetPath = "/another/path/to/target/file2"

    const result = createRelativeImportPath(sourcePath, targetPath)
    expect(result).toEqual("../../../another/path/to/target/file2")
  })

  it("should create the correct relative import path when paths have the same parent directories except for the leaf file", () => {
    const sourcePath = "/path/to/source/file"
    const targetPath = "/path/to/target/another-file"

    const result = createRelativeImportPath(sourcePath, targetPath)
    expect(result).toEqual("../target/another-file")
  })

  it("should create the correct relative import path when paths are identical", () => {
    const sourcePath = "/path/to/source/file1"
    const targetPath = "/path/to/source/file2"

    const result = createRelativeImportPath(sourcePath, targetPath)
    expect(result).toEqual("./file2")
  })
})
