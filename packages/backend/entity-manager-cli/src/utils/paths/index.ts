function createRelativeImportPath(
  sourcePath: string,
  targetPath: string
): string {
  // Remove the file names from the path
  let sourceFolders = sourcePath.split("/").slice(0, -1)
  let targetFolders = targetPath.split("/").slice(0, -1)

  // Find the common parent folder
  while (
    sourceFolders.length > 0 &&
    targetFolders.length > 0 &&
    sourceFolders[0] === targetFolders[0]
  ) {
    sourceFolders.shift()
    targetFolders.shift()
  }

  // For each remaining folder in the source path, we need to go up one level
  let goUpLevels = sourceFolders.length

  let relativePath = ""
  // Add go up levels
  for (let i = 0; i < goUpLevels; i++) {
    relativePath += "../"
  }

  if (!relativePath) {
    relativePath = "."
  }

  // Append remaining target folders
  relativePath += targetFolders.join("/")

  // Append target file name
  relativePath +=
    (relativePath.endsWith("/") ? "" : "/") + targetPath.split("/").pop()

  return relativePath
}

export { createRelativeImportPath }
