export const mapObjectRecursively = <T>(
  item: T,
  mapFn: (item: string) => string
): T => {
  if (typeof item === "string") {
    return mapFn(item) as T
  }

  const replacedObject: any = {}
  for (const [key, value] of Object.entries(item as any)) {
    if (typeof value === "string") {
      replacedObject[key] = mapFn(value)
    } else if (typeof item === "object" && item !== null) {
      replacedObject[key] = mapObjectRecursively(value, mapFn)
    }
  }
  return replacedObject as T
}
