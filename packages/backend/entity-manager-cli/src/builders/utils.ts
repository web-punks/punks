import { join } from "path"

export const normalizeFilePath = (filePath: string) =>
  join(...filePath.split("/"))
