import { EntityRepository } from "../../../types/nest"

export type AppInitBuilderContext = {
  appName: string
  packageName: string
  databaseName: string
  databaseClassName: string
  databaseEnvVariable: string
}

export type AppInitBuilderInput = {
  appName: string
  databaseName: string
  repository: EntityRepository
}
