import { NestAbstractBuilder } from "../../base"
import { NestProjectSettings } from "../../../types/nest"
import { NestFileTemplate } from "../../../templates/nest/types"
import { AppInitBuilderContext, AppInitBuilderInput } from "./types"
import { toTitleCase } from "@punks/backend-core"

export class NestAppInitBuilder extends NestAbstractBuilder<
  AppInitBuilderInput,
  AppInitBuilderContext
> {
  constructor(
    input: AppInitBuilderInput,
    settings: NestProjectSettings,
    template: NestFileTemplate
  ) {
    super(input, settings, template)
  }

  protected getPathPlaceholders() {
    return {
      databaseName: this.input.databaseName,
    }
  }

  protected buildContext(): AppInitBuilderContext {
    return {
      appName: this.input.appName,
      packageName: this.settings.packageName,
      databaseName: this.input.databaseName,
      databaseClassName: toTitleCase(this.input.databaseName),
      databaseEnvVariable: `${this.input.databaseName.toUpperCase()}_DATABASE_URL`,
    }
  }
}
