import { NestProjectStructure } from "../../../templates/nest/types"
import { EntityConnector, EntityRepository } from "../../../types/nest"

export type TemplateFileImports = {
  [key: string]: string
}

export type TemplateFilePaths = {
  imports: TemplateFileImports
}

export type EntityAddCommandContext = {
  packageName: string
  databaseName: string
  databaseClassName: string
  entityId: string
  entityName: string
  tableName: string
  pluralizedEntityName: string
  endpointPath: string
  imports: NestProjectStructure
  configureFirestoreConnector: boolean
  firestoreDocumentType?: string
}

export interface EntityAddBuilderInput {
  id: string
  name: string
  databaseName: string
  pluralName: string
  tableName: string
  pluralId: string
  repository: EntityRepository
  connectors?: EntityConnector[]
  firestoreDocument?: string
}
