import { NestAbstractBuilder } from "../../base"
import { EntityAddCommandContext, EntityAddBuilderInput } from "./types"
import { NestProjectSettings } from "../../../types/nest"
import {
  NestFileTemplate,
  NestProjectStructure,
} from "../../../templates/nest/types"
import { toTitleCase } from "@punks/backend-core"

export class NestEntityAddBuilder extends NestAbstractBuilder<
  EntityAddBuilderInput,
  EntityAddCommandContext
> {
  constructor(
    input: EntityAddBuilderInput,
    settings: NestProjectSettings,
    template: NestFileTemplate
  ) {
    super(input, settings, template)
  }

  protected getPathPlaceholders() {
    return {
      databaseName: this.input.databaseName,
      entityId: this.input.id,
      pluralizedEntityId: this.input.pluralId,
    }
  }

  protected buildContext(
    importPaths: NestProjectStructure
  ): EntityAddCommandContext {
    const configureFirestoreConnector =
      this.input.connectors?.includes("firestore") ?? false

    if (configureFirestoreConnector && !this.input.firestoreDocument) {
      throw new Error(
        "Firestore document type is required when using firestore connector"
      )
    }

    return {
      packageName: this.settings.packageName,
      databaseName: this.input.databaseName,
      databaseClassName: toTitleCase(this.input.databaseName),
      entityId: this.input.id,
      entityName: this.input.name,
      pluralizedEntityName: this.input.pluralName,
      tableName: this.input.tableName,
      endpointPath: this.settings.endpointPrefix
        ? [this.settings.endpointPrefix, this.input.id].join("/")
        : this.input.id,
      configureFirestoreConnector,
      firestoreDocumentType: configureFirestoreConnector
        ? this.input.firestoreDocument
        : undefined,
      imports: importPaths,
    }
  }
}
