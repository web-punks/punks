import { NestAbstractBuilder } from "../../base"
import { NestProjectSettings } from "../../../types/nest"
import { NestFileTemplate } from "../../../templates/nest/types"
import { DatabaseAddBuilderContext, DatabaseAddBuilderInput } from "./types"
import { toTitleCase } from "@punks/backend-core"

export class NestDatabaseAddBuilder extends NestAbstractBuilder<
  DatabaseAddBuilderInput,
  DatabaseAddBuilderContext
> {
  constructor(
    input: DatabaseAddBuilderInput,
    settings: NestProjectSettings,
    template: NestFileTemplate
  ) {
    super(input, settings, template)
  }

  protected getPathPlaceholders() {
    return {
      databaseName: this.input.databaseName,
    }
  }

  protected buildContext(): DatabaseAddBuilderContext {
    return {
      databaseName: this.input.databaseName,
      databaseClassName: toTitleCase(this.input.databaseName),
      packageName: this.settings.packageName,
    }
  }
}
