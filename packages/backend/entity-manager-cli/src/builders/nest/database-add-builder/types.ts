import { EntityRepository } from "../../../types/nest"

export type DatabaseAddBuilderContext = {
  packageName: string
  databaseName: string
  databaseClassName: string
}

export type DatabaseAddBuilderInput = {
  databaseName: string
  repository: EntityRepository
  firestoreDocument?: string
}
