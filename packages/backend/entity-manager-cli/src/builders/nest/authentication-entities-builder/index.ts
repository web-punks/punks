import { NestAbstractBuilder } from "../../base"
import { NestProjectSettings } from "../../../types/nest"
import { NestFileTemplate } from "../../../templates/nest/types"
import { AppEntitiesBuilderContext, AppEntitiesBuilderInput } from "./types"

export class NestAuthenticationEntitiesBuilder extends NestAbstractBuilder<
  AppEntitiesBuilderInput,
  AppEntitiesBuilderContext
> {
  constructor(
    input: AppEntitiesBuilderInput,
    settings: NestProjectSettings,
    template: NestFileTemplate
  ) {
    super(input, settings, template)
  }

  protected getPathPlaceholders() {
    return {
      databaseName: this.input.databaseName,
    }
  }

  protected buildContext(): AppEntitiesBuilderContext {
    return {
      entities: this.input.entities,
      packageName: this.settings.packageName,
    }
  }
}
