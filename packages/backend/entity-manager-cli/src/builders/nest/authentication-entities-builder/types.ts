import { EntityConnector, EntityRepository } from "../../../types/nest"

export type AppEntityData = {
  id: string
  pluralId: string
}

export type AppEntitiesData = {
  apiKey: AppEntityData
  company: AppEntityData
  currencies: AppEntityData
  division: AppEntityData
  organization: AppEntityData
  organizationSettings: AppEntityData
  organizationalUnit: AppEntityData
  organizationalUnitType: AppEntityData
  organizationalUnitTypeChild: AppEntityData
  directory: AppEntityData
  emailLayout: AppEntityData
  emailLog: AppEntityData
  emailTemplate: AppEntityData
  fileReference: AppEntityData
  language: AppEntityData
  mediaFolder: AppEntityData
  mediaReference: AppEntityData
  permission: AppEntityData
  role: AppEntityData
  roleAssignableRole: AppEntityData
  roleOrganizationalUnitType: AppEntityData
  rolePermission: AppEntityData
  tenant: AppEntityData
  user: AppEntityData
  userGroup: AppEntityData
  userGroupMember: AppEntityData
  userProfile: AppEntityData
  userRole: AppEntityData
}

export type AppEntitiesBuilderContext = {
  packageName: string
  entities: AppEntitiesData
}

export type AppEntitiesBuilderInput = {
  databaseName: string
  repository: EntityRepository
  connectors?: EntityConnector[]
  firestoreDocument?: string
  entities: AppEntitiesData
}
