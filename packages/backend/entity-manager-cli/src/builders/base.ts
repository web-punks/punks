import path from "path"
import { trimEnd } from "@punks/backend-core"
import { AbstractBuilder } from "../abstractions/builder"
import { createRelativeImportPath } from "../utils/paths"
import { normalizeFilePath } from "./utils"
import { replacePlaceholdersRecursively } from "../utils/placeholders"
import { mapObjectRecursively } from "../utils/mapper"
import { NestFileTemplate, NestProjectStructure } from "../templates/nest/types"
import { BuildCommandInput } from "../types/commands"
import { NestProjectSettings } from "../types/nest"

export abstract class NestAbstractBuilder<
  TInput,
  TContext
> extends AbstractBuilder<TContext> {
  constructor(
    protected readonly input: TInput,
    protected readonly settings: NestProjectSettings,
    protected readonly template: NestFileTemplate
  ) {
    super()
  }

  async buildCommand(): Promise<BuildCommandInput<TContext>> {
    return {
      workingDirectory: this.settings.workingDirectory,
      fileSuffix: this.settings.fileSuffix,
      templateFile: this.templatePath({
        file: this.template.templateFilePath,
      }),
      outputFile: this.getOutputPath(this.template.outputFilePath),
      context: this.buildContext(
        this.importFilePaths(this.template.outputFilePath, this.settings.paths)
      ),
    }
  }

  protected templatePath({ file }: { file: string }) {
    return path.isAbsolute(file)
      ? file
      : path.join("templates", "nest", ...file.split("/"))
  }

  protected getOutputPath(fileTemplate: string): string {
    return replacePlaceholdersRecursively(
      fileTemplate,
      this.getPathPlaceholders()
    )
  }

  protected importFilePaths(
    pathTemplate: string,
    paths: NestProjectStructure
  ): NestProjectStructure {
    const pathsWithPlaceholders = replacePlaceholdersRecursively(
      paths,
      this.getPathPlaceholders()
    )
    const outputPath = this.getOutputPath(pathTemplate)
    return mapObjectRecursively(pathsWithPlaceholders, (value) => {
      return this.createModuleRelativeImportPath(outputPath, value)
    })
  }

  protected createModuleRelativeImportPath(
    outputFile: string,
    targetFile: string
  ) {
    return createRelativeImportPath(
      normalizeFilePath(outputFile),
      trimEnd(targetFile, ".ts")
    )
  }

  protected abstract getPathPlaceholders(): any
  protected abstract buildContext(importPaths: NestProjectStructure): TContext
}
