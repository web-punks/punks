import { readFileSync, writeFileSync } from "fs"
import { RenderingProviderFactory } from "../providers/rendering/factory"
import { BuildCommandInput } from "../types/commands"
import { discoverCliBasePath } from "../services/folders"
import {
  Log,
  ensureDirectory,
  getDirectoryPath,
  joinPath,
} from "@punks/backend-core"
import { isAbsolute } from "path"

export class RenderingCommandHandler {
  async execute<TContext>(input: BuildCommandInput<TContext>) {
    const output = await this.render(input)
    const path = joinPath(
      input.workingDirectory ?? process.cwd(),
      input.fileSuffix
        ? `${input.outputFile}${input.fileSuffix}`
        : input.outputFile
    )
    ensureDirectory(getDirectoryPath(path))
    writeFileSync(path, output)
    Log.getLogger("Main").info(`${input.outputFile} created ⚡`)
  }

  render<TContext>(input: BuildCommandInput<TContext>) {
    const provider = this.resolveProvider<TContext>()
    const template = readFileSync(
      isAbsolute(input.templateFile)
        ? input.templateFile
        : joinPath(discoverCliBasePath(), input.templateFile),
      "utf-8"
    )
    return provider.render(template, input.context)
  }

  protected resolveProvider<TContext>() {
    return RenderingProviderFactory.resolve<TContext>("handlebars")
  }
}
