import path from "path"
import fs from "fs"

const ENTITY_MANAGER_FOLDER_NAME = "entity-manager-cli"
const ENTITY_MANAGER_FILE_MARK = "em-cli"
const ENTITY_MANAGER_PACKAGE_NAME = "@punks/backend-entity-manager-cli"

const getParentDirectory = (currentPath: string): string => {
  const parentDirectory = path.dirname(currentPath)
  if (parentDirectory === currentPath) {
    return ""
  }
  return parentDirectory
}

export const discoverCliBasePath = (
  currentPath: string = __dirname
): string => {
  const nodeModulesFolder = `node_modules/${ENTITY_MANAGER_PACKAGE_NAME}`
  if (fs.existsSync(nodeModulesFolder)) {
    return nodeModulesFolder
  }

  const currentFolder = path.basename(currentPath)
  if (
    currentFolder === ENTITY_MANAGER_FOLDER_NAME &&
    fs.existsSync(path.join(currentPath, ENTITY_MANAGER_FILE_MARK))
  ) {
    return currentPath
  }

  const baseDirectory = getParentDirectory(currentPath)
  if (!baseDirectory) {
    throw new Error("Unable to find cli base directory")
  }
  return discoverCliBasePath(baseDirectory)
}
