import { EntityScope } from "../../types/multiTenancy"
import { EntityConnector, EntityRepository } from "../../types/nest"

export interface EntityAddCommandInput {
  id: string
  name: string
  databaseName: string
  pluralName: string
  tableName: string
  pluralId: string
  repository: EntityRepository
  connectors?: EntityConnector[]
  firestoreDocument?: string
  scope?: EntityScope
}
