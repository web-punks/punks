import { NestAbstractBuilder } from "../../../../builders/base"
import { RenderingCommandHandler } from "../../../../renderer"

export const renderTemplate = async (
  builder: NestAbstractBuilder<unknown, unknown>
) => {
  const handler = new RenderingCommandHandler()
  const command = await builder.buildCommand()
  return await handler.render(command)
}
