import { NestEntityAddBuilder } from "../../../../builders/nest/entity-add-builder"
import { DefaultNestProjectStructure } from "../../../../templates/nest/structure"
import { renderTemplate } from "./common"

describe("it should render controller", () => {
  it("should render controller", async () => {
    const result = await renderTemplate(
      new NestEntityAddBuilder(
        {
          databaseName: "core",
          id: "appUser",
          name: "AppUser",
          pluralId: "appUsers",
          pluralName: "AppUsers",
          tableName: "appUsers",
          repository: "typeorm",
        },
        {
          renderingProvider: "handlebars",
          packageName: "@punks/backend-entity-manager",
          paths: DefaultNestProjectStructure,
        },
        {
          templateFilePath: "newEntity/default/controller.ts.template",
          outputFilePath: DefaultNestProjectStructure.entity.controller,
        }
      )
    )
    expect(result).toMatchSnapshot()
  })
})
