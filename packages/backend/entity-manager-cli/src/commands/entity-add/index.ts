import {
  DefaultEntityAddTemplates,
  MultiLanguageEntityAddTemplates,
  MultiOrganizationEntityAddTemplates,
} from "../../templates/nest/paths"
import { NestEntityAddBuilder } from "../../builders/nest/entity-add-builder"
import { RenderingCommandHandler } from "../../renderer"
import { NestProjectSettings } from "../../types/nest"
import { EntityAddCommandInput } from "./types"
import { EntityScope } from "../../types/multiTenancy"
import { NestFileTemplate } from "../../templates/nest/types"

export const isAppModuleContent = (file: NestFileTemplate) =>
  file.outputFilePath.startsWith("src/app/")

export const getEntityAddTemplates = (scope?: EntityScope) => {
  switch (scope) {
    case "multiOrganization":
      return MultiOrganizationEntityAddTemplates
    case "multiLanguage":
      return MultiLanguageEntityAddTemplates
    default:
      return DefaultEntityAddTemplates
  }
}

export const nestEntityAddCommand = async (
  input: EntityAddCommandInput,
  settings: NestProjectSettings
) => {
  const builders = [
    ...getEntityAddTemplates(input.scope)
      .filter((x) => (settings.skipAppModule ? !isAppModuleContent(x) : true))
      .map((x) => new NestEntityAddBuilder(input, settings, x)),
    // ...(input.connectors?.includes("firestore")
    //   ? [
    //       new NestFirestoreConnectorBuilder(input, settings),
    //       new NestEntityFirestoreMapperBuilder(input, settings),
    //     ]
    //   : []),
  ]
  const handler = new RenderingCommandHandler()
  for (const builder of builders) {
    const command = await builder.buildCommand()
    await handler.execute(command)
  }
}
