import { join } from "path"
import { nestApplicationInitCommand } from ".."
import {
  DEFAULT_PACKAGE_NAME,
  DEFAULT_RENDERING_PROVIDER,
} from "../../../consts"
import { DefaultNestProjectStructure } from "../../../templates/nest/structure"
import { DefaultAppEntitiesData } from "../defaults"
import { readFileSync } from "fs"
import { Log, getDirectoryFilePaths } from "@punks/backend-core"

Log.disable()

describe("it should render authentication init files", () => {
  it("should render application init files", async () => {
    const outputFolder = join(__dirname, "output")
    await nestApplicationInitCommand(
      {
        appName: "TestApp",
        databaseName: "core",
        repository: "typeorm",
        authenticationEntities: DefaultAppEntitiesData,
      },
      {
        renderingProvider: DEFAULT_RENDERING_PROVIDER,
        packageName: DEFAULT_PACKAGE_NAME,
        paths: DefaultNestProjectStructure,
        workingDirectory: outputFolder,
        fileSuffix: ".test",
      }
    )

    const outputFiles = await getDirectoryFilePaths(outputFolder, {
      recursive: true,
    })
    for (const filePath of outputFiles) {
      const fileContent = readFileSync(filePath, "utf-8")
      expect({
        filePath: filePath.split("/output/")[1],
        fileContent,
      }).toMatchSnapshot()
    }
  })
})
