import { AppAuthContext, AppUserContext } from "../../types"
import {
  NestEntityAuthorizationMiddleware,
  IAuthorizationResult,
} from "@punks/backend-entity-manager"

export abstract class AppEntityAuthorizationMiddlewareBase<
  TEntity
> extends NestEntityAuthorizationMiddleware<
  TEntity,
  AppAuthContext,
  AppUserContext
> {
  protected async defaultCanSearch(
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  protected async defaultCanRead(
    entity: Partial<TEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  protected async defaultCanCreate(
    entity: Partial<TEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  protected async defaultCanUpdate(
    entity: Partial<TEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }

  protected async defaultCanDelete(
    entity: Partial<TEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return {
      isAuthorized: true,
    }
  }
}
