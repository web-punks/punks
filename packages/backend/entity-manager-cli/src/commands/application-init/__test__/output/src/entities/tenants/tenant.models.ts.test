import {
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { TenantEntity } from "../../database/core/entities/tenant.entity"

export type TenantEntityId = string

export type TenantCreateData = Partial<Omit<TenantEntity, "id">>
export type TenantUpdateData = Partial<Omit<TenantEntity, "id">>

export class TenantFacets {}

export class TenantSearchFilters {}

export enum TenantSorting {
  Name = "Name",
}

export type TenantCursor = number

export class TenantSearchSortingField
  implements ISearchSortingField<TenantSorting>
{
  @ApiProperty({ enum: TenantSorting })
  field: TenantSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class TenantQuerySorting implements ISearchSorting<TenantSorting> {
  @ApiProperty({ required: false, type: [TenantSearchSortingField] })
  fields: TenantSearchSortingField[]
}

export class TenantQueryPaging
  implements ISearchRequestPaging<TenantCursor>
{
  @ApiProperty({ required: false })
  cursor?: TenantCursor

  @ApiProperty()
  pageSize: number
}

export class TenantSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class TenantSearchParameters
  implements IEntitySearchParameters<TenantSorting, TenantCursor>
{
  @ApiProperty({ required: false })
  filters?: TenantSearchFilters

  @ApiProperty({ required: false })
  sorting?: TenantQuerySorting

  @ApiProperty({ required: false })
  paging?: TenantQueryPaging

  @ApiProperty({ required: false })
  options?: TenantSearchOptions
}

export class TenantSearchResultsPaging
  implements IEntitiesSearchResultsPaging<TenantCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: TenantCursor

  @ApiProperty({ required: false })
  currentPageCursor?: TenantCursor

  @ApiProperty({ required: false })
  prevPageCursor?: TenantCursor
}

export class TenantSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      TenantSearchParameters,
      TResult,
      TenantSorting,
      TenantCursor,
      TenantFacets
    >
{
  @ApiProperty()
  request: TenantSearchParameters

  @ApiProperty()
  facets?: TenantFacets

  @ApiProperty()
  paging?: TenantSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}
