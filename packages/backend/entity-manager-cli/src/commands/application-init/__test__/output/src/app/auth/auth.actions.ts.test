import { Injectable } from "@nestjs/common"
import { AuthenticationService } from "@punks/backend-entity-manager"
import {
  UserEmailVerifyCompleteRequest,
  UserEmailVerifyRequest,
  UserLoginRequest,
  UserPasswordResetCompleteRequest,
  UserPasswordResetRequest,
  UserRegisterRequest,
} from "./auth.dto"

@Injectable()
export class AuthActions {
  constructor(private readonly auth: AuthenticationService) {}

  async login(data: UserLoginRequest) {
    return await this.auth.userLogin({
      userName: data.userName,
      password: data.password,
      context: data.userContext,
    })
  }

  async register(data: UserRegisterRequest) {
    return await this.auth.userRegister({
      email: data.email,
      password: data.password,
      registrationInfo: data.data,
      context: data.userContext,
      callback: data.callback,
      languageId: data.languageId,
    })
  }

  async passwordReset(data: UserPasswordResetRequest) {
    return await this.auth.userPasswordResetRequest({
      email: data.userName,
      context: data.userContext,
      callback: data.callback,
      languageId: data.languageId,
    })
  }

  async passwordResetComplete(data: UserPasswordResetCompleteRequest) {
    return await this.auth.userPasswordResetFinalize({
      token: data.token,
      newPassword: data.newPassword,
      temporary: false,
    })
  }

  async userVerifyRequest(data: UserEmailVerifyRequest) {
    return await this.auth.userVerifyRequest({
      email: data.email,
      context: data.userContext,
      callback: data.callback,
      languageId: data.languageId,
    })
  }

  async userVerifyComplete(data: UserEmailVerifyCompleteRequest) {
    return await this.auth.userVerifyComplete({
      token: data.token,
    })
  }
}
