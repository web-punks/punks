import {
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { UserEntity } from "../../database/core/entities/user.entity"

export type UserEntityId = string

export type UserCreateData = Partial<Omit<UserEntity, "id">>
export type UserUpdateData = Partial<Omit<UserEntity, "id">>

export class UserFacets {}

export class UserSearchFilters {
  @ApiProperty({ required: false })
  email?: string

  @ApiProperty({ required: false })
  userName?: string

  @ApiProperty({ required: false })
  organizationId?: string

  @ApiProperty({ required: false })
  organizationUid?: string

  @ApiProperty({ required: false })
  tenantId?: string

  @ApiProperty({ required: false })
  tenantUid?: string
}

export enum UserSorting {
  Name = "Name",
}

export type UserCursor = number

export class UserSearchSortingField
  implements ISearchSortingField<UserSorting>
{
  @ApiProperty({ enum: UserSorting })
  field: UserSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class UserQuerySorting implements ISearchSorting<UserSorting> {
  @ApiProperty({ required: false, type: [UserSearchSortingField] })
  fields: UserSearchSortingField[]
}

export class UserQueryPaging implements ISearchRequestPaging<UserCursor> {
  @ApiProperty({ required: false })
  cursor?: UserCursor

  @ApiProperty()
  pageSize: number
}

export class UserSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class UserSearchParameters
  implements IEntitySearchParameters<UserSorting, UserCursor>
{
  @ApiProperty({ required: false })
  filters?: UserSearchFilters

  @ApiProperty({ required: false })
  sorting?: UserQuerySorting

  @ApiProperty({ required: false })
  paging?: UserQueryPaging

  @ApiProperty({ required: false })
  options?: UserSearchOptions
}

export class UserSearchResultsPaging
  implements IEntitiesSearchResultsPaging<UserCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: UserCursor

  @ApiProperty({ required: false })
  currentPageCursor?: UserCursor

  @ApiProperty({ required: false })
  prevPageCursor?: UserCursor
}

export class UserSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      UserSearchParameters,
      TResult,
      UserSorting,
      UserCursor,
      UserFacets
    >
{
  @ApiProperty()
  request: UserSearchParameters

  @ApiProperty()
  facets?: UserFacets

  @ApiProperty()
  paging?: UserSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}
