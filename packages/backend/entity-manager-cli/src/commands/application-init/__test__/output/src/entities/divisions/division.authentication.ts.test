import {
  IAuthorizationResult,
  WpEntityAuthMiddleware,
} from "@punks/backend-entity-manager"
import {
  AppAuthContext,
  AppEntityAuthorizationMiddlewareBase,
} from "../../infrastructure/authentication/index"
import { DivisionEntity } from "../../database/core/entities/division.entity"

@WpEntityAuthMiddleware("division")
export class DivisionAuthMiddleware extends AppEntityAuthorizationMiddlewareBase<DivisionEntity> {
  async canSearch(context: AppAuthContext): Promise<IAuthorizationResult> {
    return this.defaultCanSearch(context)
  }

  async canRead(
    entity: Partial<DivisionEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanRead(entity, context)
  }

  async canCreate(
    entity: Partial<DivisionEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanCreate(entity, context)
  }

  async canUpdate(
    entity: Partial<DivisionEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanUpdate(entity, context)
  }

  async canDelete(
    entity: Partial<DivisionEntity>,
    context: AppAuthContext
  ): Promise<IAuthorizationResult> {
    return this.defaultCanDelete(entity, context)
  }
}
