import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { UserActions } from "./user.actions"
import {
  UserCreateDto,
  UserDto,
  UserSearchRequest,
  UserSearchResponse,
  UserUpdateDto,
} from "./user.dto"

@Controller("user")
export class UserController {
  constructor(private readonly actions: UserActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "userGet",
  })
  @ApiOkResponse({
    type: UserDto,
  })
  async item(@Param("id") id: string): Promise<UserDto | undefined> {
    return await this.actions.provider.get().execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "userCreate",
  })
  @ApiOkResponse({
    type: UserDto,
  })
  async create(@Body() data: UserCreateDto): Promise<UserDto> {
    return await this.actions.provider.create().execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: UserDto,
  })
  @ApiOperation({
    operationId: "userUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: UserUpdateDto
  ): Promise<UserDto> {
    return await this.actions.provider.update().execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "userDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.provider.delete().execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "userSearch",
  })
  @ApiOkResponse({
    type: UserSearchResponse,
  })
  async search(
    @Body() request: UserSearchRequest
  ): Promise<UserSearchResponse> {
    return await this.actions.provider.search().execute(request.params)
  }
}
