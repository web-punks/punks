import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { UserGroupMemberActions } from "./userGroupMember.actions"
import {
  UserGroupMemberCreateDto,
  UserGroupMemberDto,
  UserGroupMemberSearchRequest,
  UserGroupMemberSearchResponse,
  UserGroupMemberUpdateDto,
} from "./userGroupMember.dto"

@Controller("userGroupMember")
export class UserGroupMemberController {
  constructor(private readonly actions: UserGroupMemberActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "userGroupMemberGet",
  })
  @ApiOkResponse({
    type: UserGroupMemberDto,
  })
  async item(@Param("id") id: string): Promise<UserGroupMemberDto | undefined> {
    return await this.actions.provider.get().execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "userGroupMemberCreate",
  })
  @ApiOkResponse({
    type: UserGroupMemberDto,
  })
  async create(@Body() data: UserGroupMemberCreateDto): Promise<UserGroupMemberDto> {
    return await this.actions.provider.create().execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: UserGroupMemberDto,
  })
  @ApiOperation({
    operationId: "userGroupMemberUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: UserGroupMemberUpdateDto
  ): Promise<UserGroupMemberDto> {
    return await this.actions.provider.update().execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "userGroupMemberDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.provider.delete().execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "userGroupMemberSearch",
  })
  @ApiOkResponse({
    type: UserGroupMemberSearchResponse,
  })
  async search(
    @Body() request: UserGroupMemberSearchRequest
  ): Promise<UserGroupMemberSearchResponse> {
    return await this.actions.provider.search().execute(request.params)
  }
}
