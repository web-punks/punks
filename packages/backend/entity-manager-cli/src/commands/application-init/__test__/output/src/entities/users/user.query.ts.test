import { FindOptionsWhere } from "typeorm"
import {
  IAuthenticationContext,
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import { UserEntity } from "../../database/core/entities/user.entity"
import { UserFacets, UserSearchParameters, UserSorting } from "./user.models"
import { AppAuthContext } from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("user")
export class UserQueryBuilder extends NestTypeOrmQueryBuilder<
  UserEntity,
  UserSearchParameters,
  UserSorting,
  UserFacets,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("user", registry)
  }

  protected buildContextFilter(
    context?: IAuthenticationContext<AppAuthContext> | undefined
  ): FindOptionsWhere<UserEntity> | FindOptionsWhere<UserEntity>[] {
    return {}
  }

  protected buildWhereClause(
    request: UserSearchParameters
  ): FindOptionsWhere<UserEntity> | FindOptionsWhere<UserEntity>[] {
    return {
      ...(request.filters?.email ? { email: request.filters.email } : {}),
      ...(request.filters?.userName
        ? { userName: request.filters.userName }
        : {}),
      ...(request.filters?.organizationId
        ? {
            organization: {
              id: request.filters.organizationId,
            },
          }
        : {}),
      ...(request.filters?.organizationUid
        ? {
            organization: {
              uid: request.filters.organizationUid,
            },
          }
        : {}),
    }
  }

  protected calculateFacets(
    request: UserSearchParameters
  ): Promise<UserFacets | undefined> {
    return Promise.resolve(undefined)
  }
}
