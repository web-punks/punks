import {
  IEntityConverter,
  WpEntityConverter,
} from "@punks/backend-entity-manager"
import {
  OrganizationCreateDto,
  OrganizationDto,
  OrganizationListItemDto,
  OrganizationUpdateDto,
} from "./organization.dto"
import { OrganizationEntity } from "../../database/core/entities/organization.entity"

@WpEntityConverter("organization")
export class OrganizationConverter
  implements
    IEntityConverter<
      OrganizationEntity,
      OrganizationDto,
      OrganizationListItemDto,
      OrganizationCreateDto,
      OrganizationUpdateDto
    >
{
  toListItemDto(entity: OrganizationEntity): OrganizationListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: OrganizationEntity): OrganizationDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: OrganizationCreateDto): Partial<OrganizationEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: OrganizationUpdateDto): Partial<OrganizationEntity> {
    return {
      ...input,
    }
  }
}
