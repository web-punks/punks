import {
  IEntitiesSearchResults,
  IEntitiesSearchResultsPaging,
  IEntitySearchParameters,
  ISearchOptions,
  ISearchRequestPaging,
  ISearchSorting,
  ISearchSortingField,
  SortDirection,
} from "@punks/backend-entity-manager"
import { ApiProperty } from "@nestjs/swagger"
import { OrganizationEntity } from "../../database/core/entities/organization.entity"

export type OrganizationEntityId = string

export type OrganizationCreateData = Partial<Omit<OrganizationEntity, "id">>
export type OrganizationUpdateData = Partial<Omit<OrganizationEntity, "id">>

export class OrganizationFacets {}

export class OrganizationSearchFilters {
  uid?: string
}

export enum OrganizationSorting {
  Name = "Name",
}

export type OrganizationCursor = number

export class OrganizationSearchSortingField
  implements ISearchSortingField<OrganizationSorting>
{
  @ApiProperty({ enum: OrganizationSorting })
  field: OrganizationSorting

  @ApiProperty({ enum: SortDirection })
  direction: SortDirection
}

export class OrganizationQuerySorting
  implements ISearchSorting<OrganizationSorting>
{
  @ApiProperty({ required: false, type: [OrganizationSearchSortingField] })
  fields: OrganizationSearchSortingField[]
}

export class OrganizationQueryPaging
  implements ISearchRequestPaging<OrganizationCursor>
{
  @ApiProperty({ required: false })
  cursor?: OrganizationCursor

  @ApiProperty()
  pageSize: number
}

export class OrganizationSearchOptions implements ISearchOptions {
  @ApiProperty({ required: false })
  includeFacets?: boolean
}

export class OrganizationSearchParameters
  implements IEntitySearchParameters<OrganizationSorting, OrganizationCursor>
{
  @ApiProperty({ required: false })
  filters?: OrganizationSearchFilters

  @ApiProperty({ required: false })
  sorting?: OrganizationQuerySorting

  @ApiProperty({ required: false })
  paging?: OrganizationQueryPaging

  @ApiProperty({ required: false })
  options?: OrganizationSearchOptions
}

export class OrganizationSearchResultsPaging
  implements IEntitiesSearchResultsPaging<OrganizationCursor>
{
  @ApiProperty()
  pageIndex: number

  @ApiProperty()
  pageSize: number

  @ApiProperty()
  totPageItems: number

  @ApiProperty()
  totPages: number

  @ApiProperty()
  totItems: number

  @ApiProperty({ required: false })
  nextPageCursor?: OrganizationCursor

  @ApiProperty({ required: false })
  currentPageCursor?: OrganizationCursor

  @ApiProperty({ required: false })
  prevPageCursor?: OrganizationCursor
}

export class OrganizationSearchResults<TResult>
  implements
    IEntitiesSearchResults<
      OrganizationSearchParameters,
      TResult,
      OrganizationSorting,
      OrganizationCursor,
      OrganizationFacets
    >
{
  @ApiProperty()
  request: OrganizationSearchParameters

  @ApiProperty()
  facets?: OrganizationFacets

  @ApiProperty()
  paging?: OrganizationSearchResultsPaging

  @ApiProperty()
  items: TResult[]
}
