import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common"
import { ApiOkResponse, ApiOperation } from "@nestjs/swagger"
import { UserGroupActions } from "./userGroup.actions"
import {
  UserGroupCreateDto,
  UserGroupDto,
  UserGroupSearchRequest,
  UserGroupSearchResponse,
  UserGroupUpdateDto,
} from "./userGroup.dto"

@Controller("userGroup")
export class UserGroupController {
  constructor(private readonly actions: UserGroupActions) {}

  @Get("item/:id")
  @ApiOperation({
    operationId: "userGroupGet",
  })
  @ApiOkResponse({
    type: UserGroupDto,
  })
  async item(@Param("id") id: string): Promise<UserGroupDto | undefined> {
    return await this.actions.provider.get().execute(id)
  }

  @Put("create")
  @ApiOperation({
    operationId: "userGroupCreate",
  })
  @ApiOkResponse({
    type: UserGroupDto,
  })
  async create(@Body() data: UserGroupCreateDto): Promise<UserGroupDto> {
    return await this.actions.provider.create().execute(data)
  }

  @Post("update/:id")
  @ApiOkResponse({
    type: UserGroupDto,
  })
  @ApiOperation({
    operationId: "userGroupUpdate",
  })
  async update(
    @Param("id") id: string,
    @Body() item: UserGroupUpdateDto
  ): Promise<UserGroupDto> {
    return await this.actions.provider.update().execute(id, item)
  }

  @Delete(":id")
  @ApiOperation({
    operationId: "userGroupDelete",
  })
  async delete(@Param("id") id: string) {
    await this.actions.provider.delete().execute(id)
  }

  @Post("search")
  @ApiOperation({
    operationId: "userGroupSearch",
  })
  @ApiOkResponse({
    type: UserGroupSearchResponse,
  })
  async search(
    @Body() request: UserGroupSearchRequest
  ): Promise<UserGroupSearchResponse> {
    return await this.actions.provider.search().execute(request.params)
  }
}
