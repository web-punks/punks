import { FindOptionsWhere } from "typeorm"
import {
  IAuthenticationContext,
  EntityManagerRegistry,
  NestTypeOrmQueryBuilder,
  WpEntityQueryBuilder,
} from "@punks/backend-entity-manager"
import { OrganizationEntity } from "../../database/core/entities/organization.entity"
import {
  OrganizationFacets,
  OrganizationSearchParameters,
  OrganizationSorting,
} from "./organization.models"
import { AppAuthContext } from "../../infrastructure/authentication/types"

@WpEntityQueryBuilder("organization")
export class OrganizationQueryBuilder extends NestTypeOrmQueryBuilder<
  OrganizationEntity,
  OrganizationSearchParameters,
  OrganizationSorting,
  OrganizationFacets,
  AppAuthContext
> {
  constructor(registry: EntityManagerRegistry) {
    super("organization", registry)
  }

  protected buildContextFilter(
    context?: IAuthenticationContext<AppAuthContext> | undefined
  ):
    | FindOptionsWhere<OrganizationEntity>
    | FindOptionsWhere<OrganizationEntity>[] {
    return {}
  }

  protected buildWhereClause(
    request: OrganizationSearchParameters
  ):
    | FindOptionsWhere<OrganizationEntity>
    | FindOptionsWhere<OrganizationEntity>[] {
    return {
      ...(request.filters?.uid ? { uid: request.filters.uid } : {}),
    }
  }

  protected calculateFacets(
    request: OrganizationSearchParameters
  ): Promise<OrganizationFacets | undefined> {
    return Promise.resolve(undefined)
  }
}
