import { Module } from "@nestjs/common"
import { TypeOrmModule } from "@nestjs/typeorm"
import { ConfigModule } from "@nestjs/config"
import { APP_FILTER } from "@nestjs/core"
import { EventEmitterModule } from "@nestjs/event-emitter"
import {
  AuthenticationModule,
  AwsBucketModule,
  SendgridEmailModule,
} from "@punks/backend-entity-manager"
import { AppController } from "./app.controller"
import { ormConnectionOptions } from "./app.ormconfig"
import { SharedModule } from "./shared/module"
import { AppExceptionsFilter } from "./shared/errors/middleware"
import { AppModules } from "./app"
import { DatabaseModules } from "./database"
import { EntitiesModule } from "./entities"
import { FeatureModules } from "./features"
import { InfrastructureModules } from "./infrastructure"
import {
  getAuthSettings,
  getSendgridSettings,
  getAwsBucketSettings,
} from "./settings"
import { MessagingModule } from "./messaging/module"

@Module({
  imports: [
    SharedModule,
    ConfigModule.forRoot(),
    EventEmitterModule.forRoot({
      delimiter: ":",
      wildcard: true,
    }),
    TypeOrmModule.forRoot(ormConnectionOptions.core()),
    SendgridEmailModule.forRoot(getSendgridSettings()),
    AwsBucketModule.forRoot(getAwsBucketSettings()),
    AuthenticationModule.forRoot(getAuthSettings()),
    EntitiesModule,
    MessagingModule,
    ...AppModules,
    ...FeatureModules,
    ...InfrastructureModules,
    ...DatabaseModules,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AppExceptionsFilter,
    },
  ],
})
export class AppModule {}
