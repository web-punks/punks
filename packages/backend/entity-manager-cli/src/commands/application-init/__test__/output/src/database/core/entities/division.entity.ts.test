import {
  Entity,
  Column,
  CreateDateColumn,
  ManyToOne,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
} from "typeorm"
import { WpEntity } from "@punks/backend-entity-manager"
import { CompanyEntity } from "./company.entity"

@Entity("divisions")
@WpEntity("division")
@Unique("divisionUid", ["company", "uid"])
export class DivisionEntity {
  @PrimaryColumn({ type: "uuid" })
  id: string

  @Column({ type: "varchar", length: 255 })
  uid: string

  @Column({ type: "varchar", length: 255 })
  name: string

  @ManyToOne(() => CompanyEntity, (company) => company.divisions)
  company: CompanyEntity

  @CreateDateColumn({ type: "timestamptz" })
  createdOn: Date

  @UpdateDateColumn({ type: "timestamptz" })
  updatedOn: Date
}
