import {
  IEntityConverter,
  WpEntityConverter,
} from "@punks/backend-entity-manager"
import {
  CompanyCreateDto,
  CompanyDto,
  CompanyListItemDto,
  CompanyUpdateDto,
} from "./company.dto"
import { CompanyEntity } from "../../database/core/entities/company.entity"

@WpEntityConverter("company")
export class CompanyConverter
  implements
    IEntityConverter<
      CompanyEntity,
      CompanyDto,
      CompanyListItemDto,
      CompanyCreateDto,
      CompanyUpdateDto
    >
{
  toListItemDto(entity: CompanyEntity): CompanyListItemDto {
    return {
      ...entity,
    }
  }

  toEntityDto(entity: CompanyEntity): CompanyDto {
    return {
      ...entity,
    }
  }

  createDtoToEntity(input: CompanyCreateDto): Partial<CompanyEntity> {
    return {
      ...input,
    }
  }

  updateDtoToEntity(input: CompanyUpdateDto): Partial<CompanyEntity> {
    return {
      ...input,
    }
  }
}
