import { AppEntitiesData } from "../../builders/nest/authentication-entities-builder/types"

const AUTH_ENTITY_PREFIX = "app"

const buildIdentifiers = (singleID: string, pluralId: string) => ({
  id: `${AUTH_ENTITY_PREFIX}${singleID}`,
  pluralId: `${AUTH_ENTITY_PREFIX}${pluralId}`,
})

export const DefaultAppEntitiesData: AppEntitiesData = {
  apiKey: buildIdentifiers("ApiKey", "ApiKeys"),
  company: buildIdentifiers("Company", "Companies"),
  currencies: buildIdentifiers("Currency", "Currencies"),
  division: buildIdentifiers("Division", "Divisions"),
  fileReference: buildIdentifiers("FileReference", "FileReferences"),
  organization: buildIdentifiers("Organization", "Organizations"),
  organizationalUnit: buildIdentifiers(
    "OrganizationalUnit",
    "OrganizationalUnits"
  ),
  organizationalUnitType: buildIdentifiers(
    "OrganizationalUnitType",
    "OrganizationalUnitTypes"
  ),
  organizationalUnitTypeChild: buildIdentifiers(
    "OrganizationalUnitTypeChild",
    "OrganizationalUnitTypeChildren"
  ),
  organizationSettings: buildIdentifiers(
    "OrganizationSetting",
    "OrganizationSettings"
  ),
  permission: buildIdentifiers("Permission", "Permissions"),
  role: buildIdentifiers("Role", "Roles"),
  roleAssignableRole: buildIdentifiers(
    "RoleAssignableRole",
    "RoleAssignableRoles"
  ),
  roleOrganizationalUnitType: buildIdentifiers(
    "RoleOrganizationalUnitType",
    "RoleOrganizationalUnitTypes"
  ),
  rolePermission: buildIdentifiers("RolePermission", "RolePermissions"),
  tenant: buildIdentifiers("Tenant", "Tenants"),
  directory: buildIdentifiers("Directory", "Directories"),
  user: buildIdentifiers("User", "Users"),
  userGroup: buildIdentifiers("UserGroup", "UserGroups"),
  userGroupMember: buildIdentifiers("UserGroupMember", "UserGroupMembers"),
  userProfile: buildIdentifiers("UserProfile", "UserProfiles"),
  userRole: buildIdentifiers("UserRole", "UserRoles"),
  language: buildIdentifiers("Language", "Languages"),
  mediaFolder: buildIdentifiers("MediaFolder", "MediaFolders"),
  mediaReference: buildIdentifiers("MediaReference", "MediaReferences"),
  emailLayout: buildIdentifiers("EmailLayout", "EmailLayouts"),
  emailLog: buildIdentifiers("EmailLog", "EmailLogs"),
  emailTemplate: buildIdentifiers("EmailTemplate", "EmailTemplates"),
}
