import { buildEntityPaths } from "../../templates/nest/paths"
import {
  NewEntityTemplates,
  NewMultiOrganizationEntityTemplates,
} from "../../templates/nest/sources"
import { isAppModuleContent } from "../entity-add"

export const AppApiKeyEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  ...NewMultiOrganizationEntityTemplates,
  entity: "appEntities/appApiKey.entity.ts.template",
})

export const CompanyEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appCompany.entity.ts.template",
})

export const CurrencyEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  ...NewMultiOrganizationEntityTemplates,
  entity: "appEntities/appCurrency.entity.ts.template",
})

export const DivisionEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appDivision.entity.ts.template",
})

export const FileReferenceEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appFileReference.entity.ts.template",
}).filter((x) => !isAppModuleContent(x))

export const MediaFolderEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appMediaFolder.entity.ts.template",
})

export const MediaReferenceEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appMediaReference.entity.ts.template",
})

export const OrganizationEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appOrganization.entity.ts.template",
})

export const OrganizationalUnitEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  ...NewMultiOrganizationEntityTemplates,
  entity: "appEntities/appOrganizationalUnit.entity.ts.template",
})

export const OrganizationalUnitTypesEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  ...NewMultiOrganizationEntityTemplates,
  entity: "appEntities/appOrganizationalUnitType.entity.ts.template",
})

export const OrganizationalUnitTypeChildrenEntitiesTemplates = buildEntityPaths(
  {
    ...NewEntityTemplates,
    entity: "appEntities/appOrganizationalUnitTypeChild.entity.ts.template",
  }
).filter((x) => !isAppModuleContent(x))

export const OrganizationSettingsEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appOrganizationSetting.entity.ts.template",
})

export const PermissionEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appPermission.entity.ts.template",
})

export const RoleEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appRole.entity.ts.template",
})

export const RoleAssignableRoleEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appRoleAssignableRole.entity.ts.template",
}).filter((x) => !isAppModuleContent(x))

export const RoleOrganizationalUnitTypeEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appRoleOrganizationalUnitType.entity.ts.template",
}).filter((x) => !isAppModuleContent(x))

export const RolePermissionEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appRolePermission.entity.ts.template",
}).filter((x) => !isAppModuleContent(x))

export const TenantEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appTenant.entity.ts.template",
})

export const DirectoryEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appDirectory.entity.ts.template",
})

export const UserEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appUser.entity.ts.template",
})

export const UserGroupEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appUserGroup.entity.ts.template",
}).filter((x) => !isAppModuleContent(x))

export const UserGroupMemberEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appUserGroupMember.entity.ts.template",
}).filter((x) => !isAppModuleContent(x))

export const UserProfileEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appUserProfile.entity.ts.template",
}).filter((x) => !isAppModuleContent(x))

export const UserRoleEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appUserRole.entity.ts.template",
}).filter((x) => !isAppModuleContent(x))

export const LanguageEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  ...NewMultiOrganizationEntityTemplates,
  entity: "appEntities/appLanguage.entity.ts.template",
})

export const EmailLayoutEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appEmailLayout.entity.ts.template",
})

export const EmailLogEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appEmailLog.entity.ts.template",
})

export const EmailTemplateEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
  entity: "appEntities/appEmailTemplate.entity.ts.template",
})

// infrastructure
export const CacheEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
}).filter((x) => !isAppModuleContent(x))

export const EventLogEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
})

export const EntityVersionsEntitiesTemplates = buildEntityPaths({
  ...NewEntityTemplates,
}).filter((x) => !isAppModuleContent(x))
