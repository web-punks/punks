import { AppEntitiesData } from "../../builders/nest/authentication-entities-builder/types"
import { EntityConnector, EntityRepository } from "../../types/nest"

export interface NestApplicationInitInput {
  appName: string
  databaseName: string
  repository: EntityRepository
  connectors?: EntityConnector[]
  firestoreDocument?: string
  authenticationEntities: AppEntitiesData
}
