import { joinPath, toTitleCase } from "@punks/backend-core"
import { AppEntityData } from "../../builders/nest/authentication-entities-builder/types"
import { NestEntityAddBuilder } from "../../builders/nest/entity-add-builder"
import { EntityAddBuilderInput } from "../../builders/nest/entity-add-builder/types"
import { RenderingCommandHandler } from "../../renderer"
import { NestProjectSettings } from "../../types/nest"
import {
  AppApiKeyEntitiesTemplates,
  CacheEntitiesTemplates,
  CompanyEntitiesTemplates,
  CurrencyEntitiesTemplates,
  DirectoryEntitiesTemplates,
  DivisionEntitiesTemplates,
  EmailLayoutEntitiesTemplates,
  EmailLogEntitiesTemplates,
  EmailTemplateEntitiesTemplates,
  EntityVersionsEntitiesTemplates,
  EventLogEntitiesTemplates,
  FileReferenceEntitiesTemplates,
  LanguageEntitiesTemplates,
  MediaFolderEntitiesTemplates,
  MediaReferenceEntitiesTemplates,
  OrganizationEntitiesTemplates,
  OrganizationSettingsEntitiesTemplates,
  OrganizationalUnitEntitiesTemplates,
  OrganizationalUnitTypeChildrenEntitiesTemplates,
  OrganizationalUnitTypesEntitiesTemplates,
  PermissionEntitiesTemplates,
  RoleAssignableRoleEntitiesTemplates,
  RoleEntitiesTemplates,
  RoleOrganizationalUnitTypeEntitiesTemplates,
  RolePermissionEntitiesTemplates,
  TenantEntitiesTemplates,
  UserEntitiesTemplates,
  UserGroupEntitiesTemplates,
  UserGroupMemberEntitiesTemplates,
  UserProfileEntitiesTemplates,
  UserRoleEntitiesTemplates,
} from "./paths"
import { NestApplicationInitInput } from "./types"
import { NestDatabaseAddBuilder } from "../../builders/nest/database-add-builder"
import {
  DefaultDatabaseAddTemplates,
  getDynamicFolderPaths,
} from "../../templates/nest/paths"
import { DatabaseAddBuilderInput } from "../../builders/nest/database-add-builder/types"
import { NestAbstractBuilder } from "../../builders/base"
import { NestAppInitBuilder } from "../../builders/nest/app-init-builder"
import { AppInitBuilderInput } from "../../builders/nest/app-init-builder/types"

const buildEntityAddInput = (
  input: NestApplicationInitInput,
  entityInfo: AppEntityData
): EntityAddBuilderInput => ({
  ...input,
  id: entityInfo.id,
  name: toTitleCase(entityInfo.id),
  pluralId: entityInfo.pluralId,
  pluralName: toTitleCase(entityInfo.pluralId),
  tableName: entityInfo.pluralId,
})

const buildDatabaseAddInput = (
  input: NestApplicationInitInput
): DatabaseAddBuilderInput => ({
  databaseName: input.databaseName,
  repository: input.repository,
})

const toAppInitBuilderInput = (
  input: NestApplicationInitInput
): AppInitBuilderInput => ({
  appName: input.appName,
  databaseName: input.databaseName,
  repository: input.repository,
})

export const nestApplicationInitCommand = async (
  input: NestApplicationInitInput,
  settings: NestProjectSettings
) => {
  const appInitTemplates = await getDynamicFolderPaths(
    joinPath(__dirname, "..", "..", "..", "templates", "nest", "appInit")
  )

  const builders: NestAbstractBuilder<unknown, unknown>[] = [
    // common modules
    ...DefaultDatabaseAddTemplates.map(
      (x) =>
        new NestDatabaseAddBuilder(buildDatabaseAddInput(input), settings, x)
    ),
    // entities
    ...AppApiKeyEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.apiKey),
          settings,
          x
        )
    ),
    ...CompanyEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.company),
          settings,
          x
        )
    ),
    ...CurrencyEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.currencies),
          settings,
          x
        )
    ),
    ...DivisionEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.division),
          settings,
          x
        )
    ),
    ...FileReferenceEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(
            input,
            input.authenticationEntities.fileReference
          ),
          settings,
          x
        )
    ),
    ...MediaFolderEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.mediaFolder),
          settings,
          x
        )
    ),
    ...MediaReferenceEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(
            input,
            input.authenticationEntities.mediaReference
          ),
          settings,
          x
        )
    ),
    ...OrganizationEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.organization),
          settings,
          x
        )
    ),
    ...OrganizationalUnitEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(
            input,
            input.authenticationEntities.organizationalUnit
          ),
          settings,
          x
        )
    ),
    ...OrganizationalUnitTypesEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(
            input,
            input.authenticationEntities.organizationalUnitType
          ),
          settings,
          x
        )
    ),
    ...OrganizationalUnitTypeChildrenEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(
            input,
            input.authenticationEntities.organizationalUnitTypeChild
          ),
          settings,
          x
        )
    ),
    ...OrganizationSettingsEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(
            input,
            input.authenticationEntities.organizationSettings
          ),
          settings,
          x
        )
    ),
    ...PermissionEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.permission),
          settings,
          x
        )
    ),
    ...RoleEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.role),
          settings,
          x
        )
    ),
    ...RoleAssignableRoleEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(
            input,
            input.authenticationEntities.roleAssignableRole
          ),
          settings,
          x
        )
    ),
    ...RoleOrganizationalUnitTypeEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(
            input,
            input.authenticationEntities.roleOrganizationalUnitType
          ),
          settings,
          x
        )
    ),
    ...RolePermissionEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(
            input,
            input.authenticationEntities.rolePermission
          ),
          settings,
          x
        )
    ),
    ...TenantEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.tenant),
          settings,
          x
        )
    ),
    ...DirectoryEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.directory),
          settings,
          x
        )
    ),
    ...UserEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.user),
          settings,
          x
        )
    ),
    ...UserGroupEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.userGroup),
          settings,
          x
        )
    ),
    ...UserGroupMemberEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(
            input,
            input.authenticationEntities.userGroupMember
          ),
          settings,
          x
        )
    ),
    ...UserProfileEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.userProfile),
          settings,
          x
        )
    ),
    ...UserRoleEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.userRole),
          settings,
          x
        )
    ),
    ...LanguageEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.language),
          settings,
          x
        )
    ),
    ...EmailLayoutEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.emailLayout),
          settings,
          x
        )
    ),
    ...EmailLogEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, input.authenticationEntities.emailLog),
          settings,
          x
        )
    ),
    ...EmailTemplateEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(
            input,
            input.authenticationEntities.emailTemplate
          ),
          settings,
          x
        )
    ),
    // infrastructure
    ...CacheEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, {
            id: "appCacheEntry",
            pluralId: "appCacheEntries",
          }),
          settings,
          x
        )
    ),
    ...EventLogEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, {
            id: "appEventLog",
            pluralId: "appEventLogs",
          }),
          settings,
          x
        )
    ),
    ...EntityVersionsEntitiesTemplates.map(
      (x) =>
        new NestEntityAddBuilder(
          buildEntityAddInput(input, {
            id: "appEntityVersion",
            pluralId: "appEntityVersions",
          }),
          settings,
          x
        )
    ),

    // app
    ...appInitTemplates.map(
      (x) => new NestAppInitBuilder(toAppInitBuilderInput(input), settings, x)
    ),
  ]
  const handler = new RenderingCommandHandler()
  for (const builder of builders) {
    const command = await builder.buildCommand()
    await handler.execute(command)
  }
}
