import { exec } from "shelljs"

export const runShellCommand = (command: string, whatIf = false) => {
  console.log(`CMD RUN -> ${command}`)
  if (!whatIf) {
    exec(command)
  }
}
