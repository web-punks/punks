import {
  S3Client,
  PutObjectCommand,
  ListObjectsCommand,
  GetObjectCommand,
  DeleteObjectCommand,
} from "@aws-sdk/client-s3"
import { getSignedUrl } from "@aws-sdk/s3-request-presigner"
import { ensureTailingSlash } from "@punks/backend-core"
import type { Readable } from "stream"
import {
  BucketFileDeleteInput,
  BucketFileDownloadInput,
  BucketFilePublicUrlCreateInput,
  BucketFileUploadInput,
  BucketFolderEnsureInput,
  BucketFolderExistsInput,
  BucketFolderListInput,
  BucketFolderListResult,
  IBucketProvider,
  S3BucketSettings,
} from "./types"
import { AwsS3BucketError } from "./errors"

const createClient = (settings: S3BucketSettings) =>
  new S3Client({
    region: settings.region,
    credentials:
      settings.awsAccessKeyId && settings.awsSecretAccessKey
        ? {
            accessKeyId: settings.awsAccessKeyId,
            secretAccessKey: settings.awsSecretAccessKey,
          }
        : undefined,
  })

export class AwsS3BucketProvider implements IBucketProvider {
  private readonly client: S3Client

  constructor(settings: S3BucketSettings) {
    this.client = createClient(settings)
  }

  async folderList(
    input: BucketFolderListInput
  ): Promise<BucketFolderListResult> {
    try {
      return await this.client.send(
        new ListObjectsCommand({
          Bucket: input.bucket,
          MaxKeys: input.maxResults,
          Prefix: input.path,
        })
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AWS BUCKET | listObjectsV2 | ${input.bucket} | ${input.path} | Error -> ${e.message}`
      )
    }
  }

  async folderEnsure(input: BucketFolderEnsureInput): Promise<void> {
    if (await this.folderExists(input)) {
      return
    }
    await this.folderCreate(input)
  }

  async folderCreate(input: BucketFolderEnsureInput): Promise<void> {
    try {
      await this.client.send(
        new PutObjectCommand({
          Bucket: input.bucket,
          Key: ensureTailingSlash(input.path),
        })
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AwsS3Bucket | folderCreate | ${input.bucket} | ${input.path} | Error -> ${e.message}`
      )
    }
  }

  async folderExists(input: BucketFolderExistsInput): Promise<boolean> {
    try {
      const result = await this.client.send(
        new GetObjectCommand({
          Bucket: input.bucket,
          Key: ensureTailingSlash(input.path),
        })
      )
      return (result.ContentLength ?? 0) > 0
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AwsS3Bucket | folderExists | Error -> ${e.message}`
      )
    }
  }

  async fileDownload(input: BucketFileDownloadInput): Promise<Buffer> {
    try {
      const result = await this.client.send(
        new GetObjectCommand({
          Bucket: input.bucket,
          Key: input.filePath,
        })
      )
      return streamToBuffer(result.Body as Readable)
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AWS BUCKET | downloadFile | ${input.bucket} | ${input.filePath} | Error -> ${e.message}`
      )
    }
  }

  async filePublicUrlCreate(
    input: BucketFilePublicUrlCreateInput
  ): Promise<string> {
    try {
      return await getSignedUrl(
        this.client,
        new GetObjectCommand({
          Bucket: input.bucket,
          Key: input.filePath,
        }),
        {
          expiresIn: input.expirationMinutes * 60,
        }
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AwsS3Bucket | filePublicUrlCreate | ${input.bucket} | ${input.filePath} | Error -> ${e.message}`
      )
    }
  }

  async fileUpload(input: BucketFileUploadInput): Promise<void> {
    try {
      await this.client.send(
        new PutObjectCommand({
          Bucket: input.bucket,
          Key: input.filePath,
          Body: input.content,
          ContentType: input.contentType,
        })
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AwsS3Bucket | fileUpload | ${input.bucket} | ${input.filePath} | Error -> ${e.message}`
      )
    }
  }

  async fileDelete(input: BucketFileDeleteInput): Promise<void> {
    try {
      await this.client.send(
        new DeleteObjectCommand({
          Bucket: input.bucket,
          Key: input.filePath,
        })
      )
    } catch (e: any) {
      throw new AwsS3BucketError(
        `AwsS3Bucket | fileDelete | ${input.bucket} | ${input.filePath} | Error -> ${e.message}`
      )
    }
  }
}
function streamToBuffer(arg0: Readable): Buffer | PromiseLike<Buffer> {
  throw new Error("Function not implemented.")
}
