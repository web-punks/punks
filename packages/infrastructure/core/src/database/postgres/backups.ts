import { runShellCommand } from "../../modules/shell"

export type PostgresBackupInput = {
  connectionString: string
  fileName: string
  whatIf: boolean
}

export const createPostgresRolesBackup = async (input: PostgresBackupInput) => {
  const { connectionString, fileName, whatIf } = input

  runShellCommand(
    `pg_dumpall --roles-only --dbname ${connectionString} > ${fileName} | gzip ${fileName}`,
    whatIf
  )

  return {
    fileName: `${fileName}.gz`,
  }
}

export const createPostgresBackup = async (input: PostgresBackupInput) => {
  const { connectionString, fileName, whatIf } = input
  runShellCommand(
    `pg_dump --dbname ${connectionString} --format=tar --file ${fileName}`,
    whatIf
  )
  return {
    fileName: `${fileName}.gz`,
  }
}
