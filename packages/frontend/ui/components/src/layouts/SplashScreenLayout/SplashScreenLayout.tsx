import { setCssVar } from "@punks/ui-core"
import { Col, Row } from "../../structure"
import styles from "./SplashScreenLayout.module.css"

export interface SplashScreenLayoutProps {
  children?: any
  imageUrl?: string
  logo?: React.ReactNode
}

const SplashScreenLayout = ({
  children,
  imageUrl,
  logo,
}: SplashScreenLayoutProps) => {
  return (
    <Row
      gap={0}
      style={{
        ...setCssVar("--bg-image", `url(${imageUrl ?? ""})`),
      }}
      className={styles.root}
      cols={14}
    >
      <Col
        size={{
          xs: 0,
          md: 6,
        }}
        className={styles.bg}
      ></Col>
      <Col
        size={{
          xs: 14,
          md: 8,
        }}
        className={styles.content}
      >
        <div className={styles.col}>
          <div className={styles.body}>
            {logo && (
              <div className={styles.logoContainer}>
                {logo && <div className={styles.logoElement}>{logo}</div>}
              </div>
            )}
            {children}
          </div>
        </div>
      </Col>
    </Row>
  )
}

export default SplashScreenLayout
