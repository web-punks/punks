import type { Meta, StoryObj } from "@storybook/react"

import { SplashScreenLayout } from "."
import { Typography } from "../../atoms"

const meta = {
  title: "Ui/Layouts/SplashScreenLayout",
  component: SplashScreenLayout,
  tags: ["autodocs"],
  parameters: {
    layout: "fullscreen",
  },
  args: {
    children: (
      <Typography my={8} align="center" variant="h3">
        PAGE CONTENT
      </Typography>
    ),
    logo: (
      <img
        src="https://a.storyblok.com/f/196735/601x601/29667fe0ad/fav10.png"
        style={{
          width: 100,
        }}
      />
    ),
    imageUrl: "https://source.unsplash.com/random/900%C3%97700/?nature",
  },
} satisfies Meta<typeof SplashScreenLayout>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {}

export const PageWithVeryLongContent: Story = {
  args: {
    children: (
      <div
        style={{
          minHeight: "150vh",
        }}
      >
        <Typography my={8} align="center" variant="h3">
          PAGE CONTENT
        </Typography>
      </div>
    ),
  },
}
