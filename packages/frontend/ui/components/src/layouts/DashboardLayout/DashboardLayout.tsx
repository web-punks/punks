import { DashboardHeader, Sidebar, SidebarNavItem } from "../../organisms"
import {
  DashboardHeaderLogoPosition,
  DashboardUserInfo,
  SidebarTrigger,
} from "../../organisms/DashboardHeader/DashboardHeader"
import { DashboardAppTemplate } from "../../templates/DashboardAppTemplate"
import { Box } from "../../structure"
import { Spinner } from "../../animations"
import { ClassValue } from "@punks/ui-core"

export type DashboardHeaderData = {
  logo: React.ReactNode
  logoPosition?: DashboardHeaderLogoPosition
  user: DashboardUserInfo
  startControls?: React.ReactNode
  middleControls?: React.ReactNode
  endControls?: React.ReactNode
  sidebarTrigger?: SidebarTrigger
}

export type DashboardLayoutClasses = {
  root?: string
  content?: string
  body?: string
  header?: string
  headerColCentral?: string
  headerColLeft?: string
  headerColRight?: string
  sidebar?: string
  sidebarNavItems?: string
  sidebarNavItem?: string
  sidebarTrigger?: ClassValue
  main?: string
  footer?: string
}

export interface DashboardLayoutProps {
  header?: DashboardHeaderData
  headerMenu?: React.ReactNode
  classes?: DashboardLayoutClasses
  navItems?: SidebarNavItem[]
  children?: React.ReactNode
  footer?: React.ReactNode
  initialized?: boolean
}

export const DashboardLayout = ({
  header,
  headerMenu,
  classes,
  navItems,
  children,
  footer,
  initialized,
}: DashboardLayoutProps) => {
  return (
    <DashboardAppTemplate
      classes={{
        root: classes?.root,
        body: classes?.body,
        main: classes?.main,
        content: classes?.content,
      }}
      header={
        header ? (
          <DashboardHeader
            className={classes?.header}
            classes={{
              central: classes?.headerColCentral,
              left: classes?.headerColLeft,
              right: classes?.headerColRight,
            }}
            logo={header.logo}
            logoPosition={header.logoPosition}
            userInfo={header.user}
            userMenu={headerMenu}
            endControls={header.endControls}
            middleControls={header.middleControls}
            startControls={header.startControls}
            sidebarTrigger={{
              children: header.sidebarTrigger?.children,
              className: classes?.sidebarTrigger,
            }}
          />
        ) : undefined
      }
      sidebar={
        navItems ? (
          <Sidebar
            classes={{
              root: classes?.sidebar,
              navItems: classes?.sidebarNavItems,
              navItem: classes?.sidebarNavItem,
            }}
            navItems={navItems}
            color="light"
          />
        ) : undefined
      }
      footer={footer}
    >
      {initialized === false ? (
        <Box my={8} flex justifyContent="center" alignItems="center">
          <Spinner size="lg" />
        </Box>
      ) : (
        children
      )}
    </DashboardAppTemplate>
  )
}

export default DashboardLayout
