import { Typography } from "../../atoms"
import styles from "./FormLabel.module.css"
import { ClassValue, FontWeight, classNames } from "@punks/ui-core"

export interface FormLabelProps {
  children: any
  className?: ClassValue
  weight?: FontWeight
  style?: React.CSSProperties
  required?: boolean
  requiredMarker?: React.ReactNode
}

const FormLabel = ({
  children,
  className,
  required,
  requiredMarker = "*",
  ...other
}: FormLabelProps) => {
  return (
    <Typography
      variant="body1"
      className={classNames(styles.root, className)}
      {...other}
    >
      {children}
      {required && <span className={styles.required}>{requiredMarker}</span>}
    </Typography>
  )
}

export default FormLabel
