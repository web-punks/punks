import { classNames, setCssVar } from "@punks/ui-core"
import styles from "./FormRow.module.css"

export type InlineBreakpoint = "sm" | "md" | "lg" | "xl"

export interface FormRowProps {
  className?: string
  children: any
  inline?: boolean | InlineBreakpoint
  noMargin?: boolean
  fullWidth?: boolean
  justify?: "start" | "center" | "end" | "between"
  items?: "start" | "center" | "end"
  gap?: number
}

const FormRow = ({
  className,
  children,
  inline,
  noMargin,
  justify,
  fullWidth,
  items,
  gap = 4,
}: FormRowProps) => {
  return (
    <div
      style={{
        ...setCssVar("--gap", gap),
      }}
      className={classNames(
        {
          [styles.margin]: !noMargin,
          [styles.inline]: inline === true,
          [styles.inlineSm]: inline === "sm",
          [styles.inlineMd]: inline === "md",
          [styles.inlineLg]: inline === "lg",
          [styles.inlineXl]: inline === "xl",
          [styles.fullWidth]: fullWidth,
          [styles.justifyBetween]: justify === "between",
          [styles.justifyCenter]: justify === "center",
          [styles.justifyEnd]: justify === "end",
          [styles.justifyStart]: justify === "start",
          [styles.itemsCenter]: items === "center",
          [styles.itemsEnd]: items === "end",
        },
        className
      )}
    >
      {children}
    </div>
  )
}

export default FormRow
