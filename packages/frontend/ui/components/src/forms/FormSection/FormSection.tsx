import React from "react"
import { FormSeparator } from "../FormSeparator"
import { Box } from "../../structure"

export interface FormSectionProps {
  title: React.ReactNode
  children: any
  className?: string
}

const FormSection = ({ children, title, className }: FormSectionProps) => {
  return (
    <Box className={className} my={2}>
      <FormSeparator title={title} />
      <Box pt={2} pb={4}>
        {children}
      </Box>
    </Box>
  )
}

export default FormSection
