import type { Meta, StoryObj } from "@storybook/react"
import { FormSection } from "."

const meta = {
  title: "UI/Forms/FormSection",
  component: FormSection,
  args: {
    title: "Title",
    children: "FORM CONTENT",
  },
} satisfies Meta<typeof FormSection>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
}
