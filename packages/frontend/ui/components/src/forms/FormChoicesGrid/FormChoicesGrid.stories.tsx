import { Meta, StoryObj } from "@storybook/react"
import FormChoicesGrid from "./FormChoicesGrid"

const meta = {
  title: "UI/Forms/FormChoicesGrid",
  component: FormChoicesGrid,
  args: {
    rows: [
      { id: "row1", label: "Riga 1" },
      { id: "row2", label: "Riga 2" },
    ],
    columns: [
      { id: "column1", label: "Colonna 1" },
      { id: "column2", label: "Colonna 2" },
    ],
    renderCell: ({ row, column }) => {
      return (
        <div>
          {row.id} - {column.id}
        </div>
      )
    },
  },
} satisfies Meta<typeof FormChoicesGrid>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
}
