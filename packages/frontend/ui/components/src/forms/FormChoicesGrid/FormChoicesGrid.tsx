import { ReactNode, useState } from "react"
import styles from "./FormChoicesGrid.module.css"
import { classNames } from "@punks/ui-core"
import { Typography } from "../../atoms"

export type RowGrid = {
  id: string
  label: string
}

export type ColumnGrid = {
  id: string
  label: string
}

export type LayoutOptions = {
  cellAlignment: "left" | "center" | "right"
}

export interface FormChoicesGridProps {
  rows: RowGrid[]
  columns: ColumnGrid[]
  renderCell: (context: {
    row: { id: string; index: number }
    column: { id: string; index: number }
  }) => ReactNode
  layoutOptions?: LayoutOptions
  className?: string
}

const FormChoicesGrid = ({
  rows,
  columns,
  renderCell,
  layoutOptions = { cellAlignment: "center" },
  className,
}: FormChoicesGridProps) => {
  const cellClasses = classNames({
    [styles.cellCentered]: layoutOptions.cellAlignment === "center",
    [styles.cellLeft]: layoutOptions.cellAlignment === "left",
    [styles.cellRight]: layoutOptions.cellAlignment === "right",
  })

  return (
    <div className={className}>
      <table className={styles.gridTable}>
        <thead>
          <tr>
            <th></th>
            {columns.map((column, colIndex) => (
              <th key={column.id}>
                <div className={classNames(styles.columnHead, cellClasses)}>
                  <Typography>{column.label}</Typography>
                </div>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {rows.map((row, rowIndex) => (
            <tr key={row.id}>
              <td>
                <div className={styles.rowHead}>
                  <Typography>{row.label}</Typography>
                </div>
              </td>
              {columns.map((column, columnIndex) => (
                <td key={`${row.id}_${column.id}`} className={styles.gridCell}>
                  <div className={cellClasses}>
                    {renderCell({
                      row: { id: row.id, index: rowIndex },
                      column: { id: column.id, index: columnIndex },
                    })}
                  </div>
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default FormChoicesGrid
