import type { Meta, StoryObj } from "@storybook/react"
import { FormSeparator } from "."

const meta = {
  title: "UI/Forms/FormSeparator",
  component: FormSeparator,
  args: {
    title: "Title",
  },
} satisfies Meta<typeof FormSeparator>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
}
