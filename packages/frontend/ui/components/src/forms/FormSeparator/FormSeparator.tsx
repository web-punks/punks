import { Typography } from "../../atoms"
import { Separator } from "../../structure/Separator"
import React from "react"
import styles from "./FormSeparator.module.css"
import { classNames } from "@punks/ui-core"

export interface FormSeparatorProps {
  title: React.ReactNode
  className?: string
}

const FormSeparator = ({ title, className }: FormSeparatorProps) => {
  return (
    <div className={classNames(styles.root, className)}>
      <Separator className={styles.separator} />
      <Typography variant="h5" px={4}>
        {title}
      </Typography>
      <Separator className={styles.separator} />
    </div>
  )
}

export default FormSeparator
