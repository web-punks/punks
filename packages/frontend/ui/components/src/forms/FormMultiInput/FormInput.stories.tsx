import type { Meta, StoryObj } from "@storybook/react"
import { FormMultiInput } from "."
import { TextInput } from "../../atoms"
import { FormInput } from "../FormInput"

const meta = {
  title: "UI/Forms/FormMultiInput",
  component: FormMultiInput,
  args: {
    label: "Label",
    render: ({ item, index }) => (
      <FormInput>
        <TextInput placeholder={`input ${index + 1}`} value={item.value} />
      </FormInput>
    ),
  },
} satisfies Meta<typeof FormMultiInput>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    label: "Contacts emails",
    items: [
      { id: "1", value: "tes1t@test.com" },
      { id: "2", value: "tes2t@test.com" },
      { id: "3", value: "tes3t@test.com" },
    ],
  },
}
