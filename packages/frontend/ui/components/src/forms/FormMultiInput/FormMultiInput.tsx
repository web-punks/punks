import styles from "./FormMultiInput.module.css"
import { classNames, ClassValue } from "@punks/ui-core"
import { FormLabel } from "../FormLabel"
import { Button } from "../../atoms"
import { AddIcon, TrashIcon } from "../../ui"

type InputItem = { value?: string }
type RenderInputItem = {
  item: InputItem & { id: string }
  index: number
}
export interface FormMultiInputProps {
  className?: ClassValue
  label?: React.ReactNode
  fillLabel?: boolean
  labelMinWidth?: string | number
  required?: boolean
  requiredMarker?: React.ReactNode
  items: Array<InputItem & { id: string }>
  append: (obj: InputItem) => void
  remove: (index?: number | number[]) => void
  render: ({ item, index }: RenderInputItem) => React.ReactNode
  readOnly?: boolean
}

export const FormMultiInput: React.FC<FormMultiInputProps> = ({
  className,
  items,
  label,
  fillLabel,
  labelMinWidth,
  required,
  requiredMarker,
  render,
  append,
  remove,
  readOnly,
}) => {
  if (readOnly && !items?.length) {
    return <></>
  }
  return (
    <div className={classNames(styles.root, className)}>
      {(label || fillLabel) && (
        <FormLabel
          className={styles.label}
          required={required}
          requiredMarker={requiredMarker}
          style={{
            ...(labelMinWidth ? { minWidth: labelMinWidth } : {}),
          }}
        >
          {label ?? ""}
        </FormLabel>
      )}
      <div className={styles.list}>
        {items.map((item, index) => (
          <div className={styles.item} key={item.id}>
            {render({ item, index })}
            {index > 0 && !readOnly && (
              <Button
                variant="filled"
                color="error"
                iconOnly
                onClick={() => remove(index)}
              >
                <TrashIcon width={14} />
              </Button>
            )}
          </div>
        ))}
        {!readOnly && (
          <Button
            variant="filled"
            color="primary"
            iconOnly
            onClick={() => append({ value: "" })}
          >
            <AddIcon width={14} />
          </Button>
        )}
      </div>
    </div>
  )
}
