export interface FormHelpProps {
  children: any
}

const FormHelp = ({ children }: FormHelpProps) => {
  return <small className="">{children}</small>
}

export default FormHelp
