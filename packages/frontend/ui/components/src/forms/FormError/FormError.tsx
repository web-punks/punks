import React from "react"
import { Typography } from "../../atoms"
import styles from "./FormError.module.css"
import { classNames } from "@punks/ui-core"

export interface FormErrorProps {
  show?: boolean
  children: any
  className?: string
}

const FormError = ({ children, show, className }: FormErrorProps) => {
  if (show === false) {
    return <></>
  }
  return (
    <Typography
      variant="body1"
      color="error"
      className={classNames(styles.root, className)}
    >
      {children}
    </Typography>
  )
}

export default FormError
