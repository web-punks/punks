import { ReactNode } from "react"
import styles from "./FormHead.module.css"
import { Typography } from "../../atoms"
import { classNames } from "@punks/ui-core"

export type FormHeadClasses = {
  root?: string
  startControl?: string
  endControl?: string
  title?: string
}

export interface FormHeadProps {
  title?: ReactNode
  startControl?: ReactNode
  endControl?: ReactNode
  noBorder?: boolean
  classes?: FormHeadClasses
}

const FormHead = ({
  startControl,
  endControl,
  title,
  noBorder,
  classes,
}: FormHeadProps) => {
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.bordered]: !noBorder,
        },
        classes?.root
      )}
    >
      <div className={classNames(styles.control, classes?.startControl)}>
        {startControl}
      </div>
      <div className={classNames(styles.title, classes?.title)}>
        <Typography variant="h3">{title}</Typography>
      </div>
      <div
        className={classNames(
          styles.control,
          styles.endControl,
          classes?.endControl
        )}
      >
        {endControl}
      </div>
    </div>
  )
}

export default FormHead
