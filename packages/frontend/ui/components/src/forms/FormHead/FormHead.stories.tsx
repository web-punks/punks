import type { Meta, StoryObj } from "@storybook/react"
import { FormHead } from "."
import { Button } from "../../atoms"

const meta = {
  title: "UI/Forms/FormHead",
  component: FormHead,
  args: {
    title: "Title",
  },
} satisfies Meta<typeof FormHead>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
}

export const WithControls: Story = {
  args: {
    endControl: (
      <Button variant="filled" color="primary">
        Click me
      </Button>
    ),
  },
}
