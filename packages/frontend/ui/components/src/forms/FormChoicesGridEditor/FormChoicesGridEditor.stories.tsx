import { Meta, StoryObj } from "@storybook/react"
import FormChoicesGridEditor, {
  FormChoicesGridEditorProps,
} from "./FormChoicesGridEditor"
import { Checkbox, Radio } from "../../atoms"

const meta = {
  title: "UI/Forms/FormChoicesGridEditor",
  component: FormChoicesGridEditor,
  args: {
    rows: [
      { id: "row1", label: "Riga 1" },
      { id: "row2", label: "Riga 2" },
      { id: "row2", label: "Riga 3" },
    ],
    columns: [
      { id: "col1", label: "Colonna 1" },
      { id: "col2", label: "Colonna 2" },
      { id: "col2", label: "Colonna 3" },
    ],
    newRowText: "",
    newColumnText: "",
    onRowsChange: () => console.log("onRowsChange"),
    onColumnsChange: () => console.log("onColumnsChange"),
    onNewRowTextChange: () => console.log("onNewRowTextChange"),
    onNewColumnTextChange: () => console.log("onNewColumnTextChange"),
    onAddRow: () => console.log("onAddRow"),
    onAddColumn: () => console.log("onAddColumn"),
    onDeleteRow: () => console.log("onDeleteRow"),
    onDeleteColumn: () => console.log("onDeleteColumn"),
    labels: {
      addColumn: "Add column",
      addRow: "Add row",
    },
  },
} satisfies Meta<FormChoicesGridEditorProps>
export default meta

type Story = StoryObj<typeof meta>

export const RadioGrid: Story = {
  args: {
    renderCell: ({ row, column }) => (
      <Radio
        disabled
        name={row.id}
        value={column.id}
        onChange={() => console.log("onChange")}
      />
    ),
  },
}

export const CheckboxGrid: Story = {
  args: {
    renderCell: ({ row, column }) => (
      <Checkbox
        disabled
        name={row.id}
        value={column.id}
        onChange={() => console.log("onChange")}
      />
    ),
  },
}

export const CustomGrid: Story = {
  args: {
    renderCell: ({ row, column }) => (
      <div>
        {row.index + 1} - {column.index + 1}
      </div>
    ),
    renderEmptyCell: ({ row, column }) => (
      <div>
        New cell: {row.index + 1} - {column.index + 1}
      </div>
    ),
  },
}
