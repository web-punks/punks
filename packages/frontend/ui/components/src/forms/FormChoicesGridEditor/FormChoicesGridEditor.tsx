import React, { ReactNode } from "react"
import styles from "./FormChoicesGridEditor.module.css"
import { TextInput } from "../../atoms"
import { classNames } from "@punks/ui-core"
import { CloseIcon } from "../../ui"

export type RowGrid = {
  id: string
  label: string
  placeholder?: string
}

export type ColumnGrid = {
  id: string
  label: string
  placeholder?: string
}

export type LayoutOptions = {
  cellAlignment: "left" | "center" | "right"
}

export type FormChoicesGridEditorLabels = {
  addColumn?: string
  addRow?: string
}

export type FormChoicesGridEditorProps = {
  rows: RowGrid[]
  columns: ColumnGrid[]
  newRowText: string
  newColumnText: string
  onRowsChange?: (rows: RowGrid[]) => void
  onColumnsChange?: (columns: ColumnGrid[]) => void
  onRowChange?: (index: number, row: RowGrid) => void
  onColumnChange?: (index: number, row: ColumnGrid) => void
  onNewRowTextChange?: (text: string) => void
  onNewColumnTextChange?: (text: string) => void
  onAddRow?: () => void
  onAddColumn?: () => void
  onDeleteRow: (id: string, index: number) => void
  onDeleteColumn: (id: string, index: number) => void
  renderCell: (context: {
    row: { id: string; index: number }
    column: { id: string; index: number }
  }) => ReactNode
  renderEmptyCell?: (context: {
    row: { index: number }
    column: { index: number }
  }) => ReactNode
  newColumnControl?: ReactNode
  newRowControl?: ReactNode
  layoutOptions?: LayoutOptions
  labels: FormChoicesGridEditorLabels
  className?: string
}

const FormChoicesGridEditor: React.FC<FormChoicesGridEditorProps> = ({
  rows,
  columns,
  newRowText,
  newColumnText,
  onRowsChange,
  onRowChange,
  onColumnsChange,
  onColumnChange,
  onNewRowTextChange,
  onNewColumnTextChange,
  onAddRow,
  onAddColumn,
  onDeleteRow,
  onDeleteColumn,
  renderCell,
  renderEmptyCell = () => <></>,
  layoutOptions = { cellAlignment: "center" },
  labels,
  newColumnControl,
  newRowControl,
  className,
}) => {
  const cellClasses = classNames({
    [styles.cellCentered]: layoutOptions.cellAlignment === "center",
    [styles.cellLeft]: layoutOptions.cellAlignment === "left",
    [styles.cellRight]: layoutOptions.cellAlignment === "right",
  })

  return (
    <div className={className}>
      <table className={styles.gridTable}>
        <thead>
          <tr>
            <th></th>
            {columns.map((column, colIndex) => (
              <th key={column.id}>
                <div className={styles.columnHead}>
                  <TextInput
                    value={column.label}
                    placeholder={column.placeholder}
                    onChange={(e) => {
                      onColumnsChange?.(
                        columns.map((col) =>
                          col.id === column.id
                            ? { ...col, label: e.target.value }
                            : col
                        )
                      )
                      onColumnChange?.(colIndex, {
                        ...column,
                        label: e.target.value,
                      })
                    }}
                    className={styles.editableText}
                    fullWidth
                    endAdornment={
                      <button
                        onClick={() => onDeleteColumn(column.id, colIndex)}
                        className={styles.deleteButton}
                      >
                        <CloseIcon />
                      </button>
                    }
                  />
                </div>
              </th>
            ))}
            <th>
              {newColumnControl ?? (
                <TextInput
                  value={newColumnText}
                  onChange={(e) => onNewColumnTextChange?.(e.target.value)}
                  onBlur={onAddColumn}
                  className={styles.editableText}
                  placeholder={labels.addColumn}
                />
              )}
            </th>
          </tr>
        </thead>
        <tbody>
          {rows.map((row, rowIndex) => (
            <tr key={row.id}>
              <td>
                <div className={styles.rowHead}>
                  <TextInput
                    value={row.label}
                    placeholder={row.placeholder}
                    onChange={(e) => {
                      onRowsChange?.(
                        rows.map((r) =>
                          r.id === row.id ? { ...r, label: e.target.value } : r
                        )
                      )
                      onRowChange?.(rowIndex, { ...row, label: e.target.value })
                    }}
                    className={styles.editableText}
                    endAdornment={
                      <button
                        onClick={() => onDeleteRow(row.id, rowIndex)}
                        className={styles.deleteButton}
                      >
                        <CloseIcon />
                      </button>
                    }
                  />
                </div>
              </td>
              {columns.map((column, columnIndex) => (
                <td key={`${row.id}_${column.id}`} className={styles.gridCell}>
                  <div className={cellClasses}>
                    {renderCell({
                      row: { id: row.id, index: rowIndex },
                      column: { id: column.id, index: columnIndex },
                    })}
                  </div>
                </td>
              ))}
              <td className={styles.gridCell}>
                <div className={cellClasses}>
                  {renderEmptyCell({
                    row: { index: rowIndex },
                    column: { index: columns.length },
                  })}
                </div>
              </td>
            </tr>
          ))}
          <tr>
            <td>
              <div className={styles.addRowHead}>
                {newRowControl ?? (
                  <TextInput
                    value={newRowText}
                    onChange={(e) => onNewRowTextChange?.(e.target.value)}
                    onBlur={onAddRow}
                    className={styles.editableText}
                    placeholder={labels.addRow}
                  />
                )}
              </div>
            </td>
            {columns.map((column, columnIndex) => (
              <td key={`new_${column.id}`} className={styles.gridCell}>
                <div className={cellClasses}>
                  {renderEmptyCell({
                    row: { index: rows.length },
                    column: { index: columnIndex },
                  })}
                </div>
              </td>
            ))}
            <td className={styles.gridCell}>
              <div className={cellClasses}>
                {renderEmptyCell({
                  row: { index: rows.length },
                  column: { index: columns.length },
                })}
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default FormChoicesGridEditor
