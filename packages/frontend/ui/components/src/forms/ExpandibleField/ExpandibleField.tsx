import { FontWeight, TextVariant, classNames } from "@punks/ui-core"
import { Typography } from "../../atoms"
import { Collapsible } from "../../panels"
import styles from "./ExpandibleField.module.css"

export type ExpandibleFieldClasses = {
  root?: string
  headLabel?: string
  content?: string
}

export interface ExpandibleFieldProps {
  head: React.ReactNode
  headVariant?: TextVariant
  headWeight?: FontWeight
  children: React.ReactNode
  defaultExpanded?: boolean
  classes?: ExpandibleFieldClasses
  className?: string
}

const ExpandibleField = ({
  head,
  headVariant = "subtitle1",
  headWeight = "bolder",
  children,
  defaultExpanded,
  classes,
  className,
}: ExpandibleFieldProps) => {
  return (
    <Collapsible
      className={classNames(styles.root, classes?.root, className)}
      header={
        <Typography
          variant={headVariant}
          weight={headWeight}
          className={classNames(styles.headLabel, classes?.headLabel)}
        >
          {head}
        </Typography>
      }
      defaultOpen={defaultExpanded}
      iconHidden
    >
      <div className={classNames(styles.content, classes?.content)}>
        {children}
      </div>
    </Collapsible>
  )
}

export default ExpandibleField
