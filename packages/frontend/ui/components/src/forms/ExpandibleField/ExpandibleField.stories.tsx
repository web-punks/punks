import type { Meta, StoryObj } from "@storybook/react"
import { ExpandibleField } from "."
import { FormInput } from "../FormInput"
import { Select, TextInput } from "../../atoms"

const meta = {
  title: "UI/Forms/ExpandibleField",
  component: ExpandibleField,
  args: {
    head: "Title",
    children: (
      <>
        <FormInput label="Label">
          <Select
            placeholder="insert value"
            options={[
              {
                label: "Option 1",
                value: "1",
              },
              {
                label: "Option 2",
                value: "2",
              },
              {
                label: "Option 3",
                value: "3",
              },
            ]}
          />
        </FormInput>
      </>
    ),
  },
} satisfies Meta<typeof ExpandibleField>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {}
