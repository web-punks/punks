import { classNames } from "@punks/ui-core"
import styles from "./FormContainer.module.css"

export interface FormContainerProps {
  children: any
  className?: string
}

const FormContainer = ({ children, className }: FormContainerProps) => {
  return <div className={classNames(styles.root, className)}>{children}</div>
}

export default FormContainer
