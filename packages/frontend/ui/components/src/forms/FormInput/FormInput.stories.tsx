import type { Meta, StoryObj } from "@storybook/react"
import { FormInput } from "."
import { TextInput } from "../../atoms"

const meta = {
  title: "UI/Forms/FormInput",
  component: FormInput,
  args: {
    label: "Label",
    children: <TextInput placeholder="input" />,
  },
} satisfies Meta<typeof FormInput>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
}

export const Inline: Story = {
  args: {
    inline: true,
  },
}

export const InlineContent: Story = {
  args: {
    inlineContent: true,
  },
}
