import React from "react"
import styles from "./FormInput.module.css"
import FormLabel from "../FormLabel/FormLabel"
import { FormHelp } from "../FormHelp"
import { classNames, ClassValue } from "@punks/ui-core"

export interface FormInputProps {
  className?: ClassValue
  children: any
  label?: React.ReactNode
  helpText?: React.ReactNode
  fill?: boolean | number
  inline?: boolean
  inlineBetween?: boolean
  inlineContent?: boolean
  bottom?: boolean
  my?: boolean | number
  fillLabel?: boolean
  labelMinWidth?: string | number
  required?: boolean
  requiredMarker?: React.ReactNode
}

const normalizeFill = (value?: boolean | number) => {
  if (value === true) {
    return 1
  }
  return value
}

const FormInput = ({
  className,
  children,
  label,
  helpText,
  fill,
  inline,
  inlineBetween,
  inlineContent,
  bottom,
  my = true,
  fillLabel,
  labelMinWidth,
  required,
  requiredMarker,
}: FormInputProps) => {
  const normalizedFill = normalizeFill(fill)
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.bottom]: bottom,
          [styles.my]: my === true,
          [styles.inline]: inline || inlineBetween,
          [styles.inlineBetween]: inlineBetween,
        },
        className
      )}
      style={{
        flex: normalizedFill ? `${normalizedFill} 0 0%` : undefined,
      }}
    >
      {(label || fillLabel) && (
        <FormLabel
          className={styles.label}
          required={required}
          requiredMarker={requiredMarker}
          style={{
            ...(labelMinWidth ? { minWidth: labelMinWidth } : {}),
          }}
        >
          {label ?? ""}
        </FormLabel>
      )}
      {inlineContent && <div className={styles.inline}>{children}</div>}
      {!inlineContent && children}
      {helpText && <FormHelp>{helpText}</FormHelp>}
    </div>
  )
}

export default FormInput
