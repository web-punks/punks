import { ThemeColor, ThemeColorVariant } from "@punks/ui-core"

export type ComponentMarginProps = {
  mb?: number
  mt?: number
  ml?: number
  mr?: number
  mx?: number
  my?: number
  m?: number
}

export type ComponentPaddingProps = {
  pb?: number
  pt?: number
  pl?: number
  pr?: number
  px?: number
  py?: number
  p?: number
}

export type ComponentColorProps = {
  color?: ThemeColor
  colorVariant?: ThemeColorVariant
}

export type ComponentBackgroundColorProps = {
  bgColor?: ThemeColor
  bgColorVariant?: ThemeColorVariant
}

export type AlignItems = "center" | "start" | "end"
export type JustifyContent = "center" | "start" | "end" | "between" | "around"
export type FlexDirection = "row" | "column"

export type ComponentFlexProps = {
  inlineFlex?: boolean
  flex?: boolean
  flexFill?: boolean
  flexDirection?: FlexDirection
  alignItems?: AlignItems
  justifyContent?: JustifyContent
  gap?: number
}
