import classes from "./Skeleton.module.css"
import { classNames } from "@punks/ui-core"

export type SkeletonVariant = "text" | "rect" | "circle"

export interface SkeletonProps {
  height?: number | string
  animation?: "pulse" | "wave" | false
  variant?: SkeletonVariant
  width?: number | string
  className?: string
}

const Skeleton = ({
  animation = "wave",
  variant,
  className,
  height,
  width,
}: SkeletonProps) => {
  return (
    <div
      style={{
        height,
        width,
      }}
      className={classNames(
        classes.root,
        variant && classes[variant],
        animation && classes[animation],
        className
      )}
    ></div>
  )
}

export default Skeleton
