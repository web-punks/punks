import styles from "./Spinner.module.css"
import { classNames, normalizePxSize, setCssVar } from "@punks/ui-core"

export type SpinnerSize = "sm" | "md" | "lg"

export type SpinnerProps = {
  className?: string
  size?: SpinnerSize
  sizeValue?: number
}

export const Spinner = ({
  size = "md",
  sizeValue,
  className,
}: SpinnerProps) => {
  return (
    <svg
      style={{
        ...(sizeValue ? setCssVar("--w", normalizePxSize(sizeValue)) : {}),
      }}
      className={classNames(
        styles.root,
        styles[size],
        {
          [styles.customSize]: !!sizeValue,
        },
        className
      )}
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
    >
      <circle
        className={styles.circle1}
        cx="12"
        cy="12"
        r="10"
        stroke="currentColor"
        strokeWidth="4"
      ></circle>
      <path
        className={styles.circle2}
        fill="currentColor"
        d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
      ></path>
    </svg>
  )
}
