import type { Meta, StoryObj } from "@storybook/react"
import { useConfirmation, ConfirmationProvider } from "."
import { UserConfirmationDefinition } from "./types"
import { Button } from "../../atoms/Button"

const ConformationControl = (definition: UserConfirmationDefinition) => {
  const { withConfirmation } = useConfirmation()
  return <Button onClick={() => withConfirmation(definition)}>Click me</Button>
}

const ConfirmationWrapper = (definition: UserConfirmationDefinition) => {
  return (
    <ConfirmationProvider>
      <ConformationControl {...definition} />
    </ConfirmationProvider>
  )
}

const meta = {
  title: "UI/Notifications/Confirmation",
  component: ConfirmationWrapper,
  tags: ["autodocs"],
  args: {},
} satisfies Meta<typeof ConfirmationWrapper>
export default meta

// const Template: ComponentStory<typeof ConfirmationWrapper> = (args) => (
//   <div>
//     <ConfirmationWrapper {...args} />
//   </div>
// )

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    id: "user-deletion",
    title: "User Deletion",
    message: "Are you sure?",
    confirm: {
      label: "Yes",
      onClick: () => console.log("confirmed"),
    },
    cancel: {
      label: "No",
      onClick: () => console.log("cancelled"),
    },
  },
}
