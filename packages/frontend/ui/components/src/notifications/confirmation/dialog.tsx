import { UserConfirmationDefinition } from "./types"
import { uiRegistry } from "../../registry"
import { useConfirmationContext } from "./context"

export interface ConfirmationDialogAdapterProps {
  modal: UserConfirmationDefinition
  onCancel: () => void
  onConfirm: () => void
}

export const ConfirmationDialogAdapter = ({
  modal,
  onCancel,
  onConfirm,
}: ConfirmationDialogAdapterProps) => {
  const context = useConfirmationContext()
  if (!context?.dialogComponent) {
    throw new Error("Confirmation dialog component not found")
  }

  const Component = context.dialogComponent
  return (
    <Component
      open
      onClose={onCancel}
      title={modal.title}
      message={modal.message}
      confirm={{
        label: modal.confirm.label,
        onClick: onConfirm,
      }}
      cancel={{
        label: modal.cancel.label,
        onClick: onCancel,
      }}
    />
  )
}
