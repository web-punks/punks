import React from "react"
import { ConfirmationContext } from "./context"
import { ConfirmationProvider } from "./provider"
import { UserConfirmationDefinition } from "./types"

export const useConfirmation = () => {
  const context = React.useContext(ConfirmationContext)

  const open = React.useCallback(
    (modal: UserConfirmationDefinition) => {
      if (!context) {
        throw new Error("Confirmation provider not initialized")
      }
      context.openModal(modal)
    },
    [context]
  )

  return {
    withConfirmation: open,
  }
}

export { ConfirmationProvider }
