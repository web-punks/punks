import { createContext, useContext } from "react"
import { UserConfirmationDefinition } from "./types"
import { ConfirmationDialogProps } from "../../organisms/ConfirmationDialog"

export interface ConfirmationData {
  openModal: (modal: UserConfirmationDefinition) => void
  closeModal: (id: string) => void
  dialogComponent: React.ComponentType<ConfirmationDialogProps>
}

export const ConfirmationContext = createContext<ConfirmationData | null>(null)

export const useConfirmationContext = () => useContext(ConfirmationContext)
