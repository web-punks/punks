import React from "react"
import { ConfirmationContext } from "./context"
import { ConfirmationDialogAdapter } from "./dialog"
import { UserConfirmationDefinition } from "./types"
import {
  ConfirmationDialog,
  ConfirmationDialogProps,
} from "../../organisms/ConfirmationDialog"

export interface ConfirmationProviderProps {
  children: any
  dialogComponent?: React.ComponentType<ConfirmationDialogProps>
}

export const ConfirmationProvider = ({
  children,
  dialogComponent,
}: ConfirmationProviderProps) => {
  const [modals, setModals] = React.useState<UserConfirmationDefinition[]>([])

  const openModal = (modal: UserConfirmationDefinition) =>
    setModals([...modals, modal])

  const closeModal = (id: string) =>
    setModals(modals.filter((m) => m.id !== id))

  const handleModalConfirm = (modal: UserConfirmationDefinition) => {
    closeModal(modal.id)
    modal.confirm.onClick?.()
  }

  const handleModalCancel = (modal: UserConfirmationDefinition) => {
    closeModal(modal.id)
    modal.cancel.onClick?.()
  }

  return (
    <ConfirmationContext.Provider
      value={{
        openModal,
        closeModal,
        dialogComponent: dialogComponent ?? ConfirmationDialog,
      }}
    >
      <>{children}</>
      <>
        {modals.map((modal) => (
          <ConfirmationDialogAdapter
            key={modal.id}
            modal={modal}
            onConfirm={() => handleModalConfirm(modal)}
            onCancel={() => handleModalCancel(modal)}
          />
        ))}
      </>
    </ConfirmationContext.Provider>
  )
}
