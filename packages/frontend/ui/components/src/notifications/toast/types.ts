export type INotifiedOperationHandler = (
    operation: () => Promise<void>,
    messages: {
      pending: React.ReactNode
      success: React.ReactNode
      error: React.ReactNode
    }
  ) => Promise<void>
  
  export type NotificationType = "success" | "error" | "info" | "warning"
  
  export type AlertInput = {
    type: NotificationType
    content: React.ReactNode
  }
  
  export type IAlertHandler = ({ content, type }: AlertInput) => void
  