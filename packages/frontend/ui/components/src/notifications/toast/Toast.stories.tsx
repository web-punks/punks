import React from "react"
import type { Meta, StoryObj } from "@storybook/react"
import { showAlert, ToastRoot } from "./Toast"
import { Button } from "../../atoms/Button"
import { NotificationType } from "./types"

interface ToastWrapperProps {
  type: NotificationType
  content: React.ReactNode
}

const ToastWrapper = ({ content, type }: ToastWrapperProps) => {
  return (
    <div>
      <Button
        onClick={() =>
          showAlert({
            type,
            content,
          })
        }
      >
        Click me
      </Button>
      <ToastRoot />
    </div>
  )
}

const meta = {
  title: "UI/Notifications/Toast",
  component: ToastWrapper,
  tags: ["autodocs"],
  args: {},
} satisfies Meta<typeof ToastWrapper>
export default meta

type Story = StoryObj<typeof meta>

// const Template: ComponentStory<typeof ToastWrapper> = (args) => (
//   <div>
//     <ToastWrapper {...args} />
//   </div>
// )

export const Info: Story = {
  args: {
    type: "info",
    content: "Info message",
  },
}

export const Success: Story = {
  args: {
    type: "success",
    content: "Success message",
  },
}

export const Error: Story = {
  args: {
    type: "error",
    content: "Error message",
  },
}

export const Warning: Story = {
  args: {
    type: "warning",
    content: "Warning message",
  },
}
