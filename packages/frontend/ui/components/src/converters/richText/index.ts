import {
  ArbitraryTypedObject,
  PortableTextBlock,
  PortableTextSpan,
} from "@portabletext/types"

export type RichText = PortableTextBlock[]

export function htmlToRichText(html: string): RichText {
  const dom = new DOMParser().parseFromString(html, "text/html")
  const body = dom.body

  function generateKey(): string {
    return Math.random().toString(36).substring(2, 10)
  }

  function extractTextAlign(element: HTMLElement): string | undefined {
    switch (element.style.textAlign) {
      case "center":
        return "textCenter"
      case "right":
        return "textRight"
      default:
        return undefined
    }
  }

  function extractTextDecoration(element: HTMLElement): string | undefined {
    switch (element.style.textDecoration) {
      case "underline":
        return "underline"
      default:
        return undefined
    }
  }

  function extractTextMarks(element: HTMLElement): string[] {
    const marksSet: Set<string> = new Set()

    const textAlign = extractTextAlign(element)
    if (textAlign) {
      marksSet.add(textAlign)
    }

    const textDecoration = extractTextDecoration(element)
    if (textDecoration) {
      marksSet.add(textDecoration)
    }

    return [...marksSet]
  }

  function extractChildNodes(element: HTMLElement, textMarks?: string[]) {
    const children: (ArbitraryTypedObject | PortableTextSpan)[] = []
    Array.from(element.childNodes).forEach((childNode) => {
      const marks: string[] = textMarks ?? []
      if (childNode.nodeType === Node.TEXT_NODE) {
        children.push({
          _type: "span",
          _key: generateKey(),
          text: childNode.textContent || "",
          marks,
        })
      } else if (childNode.nodeType === Node.ELEMENT_NODE) {
        const childElement = childNode as HTMLElement
        const childElementMarks = extractTextMarks(childElement)
        switch (childElement.tagName.toLowerCase()) {
          case "strong":
            children.push({
              _type: "span",
              _key: generateKey(),
              text: childElement.textContent || "",
              marks: [...marks, ...childElementMarks, "strong"],
            })
            break
          case "i":
            children.push({
              _type: "span",
              _key: generateKey(),
              text: childElement.textContent || "",
              marks: [...marks, ...childElementMarks, "em"],
            })
            break
          case "a":
            children.push({
              _type: "span",
              _key: generateKey(),
              text: childElement.textContent || "",
              marks: [...marks, ...childElementMarks],
            })
            break
          default:
            children.push({
              _type: "span",
              _key: generateKey(),
              text: childElement.textContent || "",
              marks: [...marks, ...childElementMarks],
            })
            break
        }
      }
    })
    return children
  }

  const result: RichText = []
  Array.from(body.childNodes).forEach((node) => {
    if (node.nodeType === Node.ELEMENT_NODE) {
      const element = node as HTMLElement
      const textMarks = extractTextMarks(element)

      switch (element.tagName.toLowerCase()) {
        case "p":
          result.push({
            _type: "block",
            _key: generateKey(),
            style: "normal",
            children: extractChildNodes(element, textMarks),
          })
          break
        case "h1":
        case "h2":
        case "h3":
        case "h4":
        case "h5":
        case "h6":
          result.push({
            _type: "block",
            _key: generateKey(),
            style: element.tagName.toLowerCase(),
            children: extractChildNodes(element, textMarks),
          })
          break
        case "ul":
          Array.from(element.children).forEach((listItem) => {
            result.push({
              _type: "block",
              _key: generateKey(),
              style: "normal",
              listItem: "bullet",
              children: extractChildNodes(listItem as HTMLElement, textMarks),
            })
          })
          break
      }
    }
  })

  return result
}

export function richTextToHtml(richText: RichText): string {
  function renderTextSpan(span: PortableTextSpan): string {
    let content = span.text

    if (span.marks) {
      span.marks.forEach((mark) => {
        switch (mark) {
          case "strong":
            content = `<strong>${content}</strong>`
            break
          case "em":
            content = `<i>${content}</i>`
            break
          case "underline":
            content = `<span style="text-decoration: underline;">${content}</span>`
            break
          case "textCenter":
            content = `<span style="text-align: center;">${content}</span>`
            break
          case "textRight":
            content = `<span style="text-align: right;">${content}</span>`
            break
        }
      })
    }

    return content
  }

  function renderBlock(block: PortableTextBlock): string {
    let content = block.children
      .map((child) => {
        if ((child as PortableTextSpan)._type === "span") {
          return renderTextSpan(child as PortableTextSpan)
        }
        return ""
      })
      .join("")

    let wrapperStartTag = ""
    let wrapperEndTag = ""

    switch (block.style) {
      case "normal":
        if (block.listItem === "bullet") {
          wrapperStartTag = `<li>`
          wrapperEndTag = `</li>`
        } else {
          wrapperStartTag = `<p>`
          wrapperEndTag = `</p>`
        }
        break
      case "h1":
      case "h2":
      case "h3":
      case "h4":
      case "h5":
      case "h6":
        wrapperStartTag = `<${block.style}>`
        wrapperEndTag = `</${block.style}>`
        break
      default:
        wrapperStartTag = `<p>`
        wrapperEndTag = `</p>`
        break
    }

    if (
      block.children &&
      block.children[0] &&
      "marks" in (block.children[0] as PortableTextSpan) &&
      Array.isArray((block.children[0] as PortableTextSpan).marks)
    ) {
      const marks = (block.children[0] as PortableTextSpan).marks!
      if (marks.includes("textCenter")) {
        wrapperStartTag = `<div style="text-align: center;">${wrapperStartTag}`
        wrapperEndTag = `${wrapperEndTag}</div>`
      } else if (marks.includes("textRight")) {
        wrapperStartTag = `<div style="text-align: right;">${wrapperStartTag}`
        wrapperEndTag = `${wrapperEndTag}</div>`
      } else if (marks.includes("textLeft")) {
        wrapperStartTag = `<div style="text-align: left;">${wrapperStartTag}`
        wrapperEndTag = `${wrapperEndTag}</div>`
      } else if (marks.includes("justify")) {
        wrapperStartTag = `<div style="text-align: justify;">${wrapperStartTag}`
        wrapperEndTag = `${wrapperEndTag}</div>`
      }
    }

    return `${wrapperStartTag}${content}${wrapperEndTag}`
  }

  let htmlContent = richText.map((block) => renderBlock(block)).join("")

  // Wrap list items in <ul> tags if necessary
  if (htmlContent.includes("<li>")) {
    htmlContent = `<ul>${htmlContent}</ul>`
  }

  return htmlContent
}
