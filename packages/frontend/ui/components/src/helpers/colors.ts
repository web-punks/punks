import { ThemeColor, ThemeColorVariant, setCssVar } from "@punks/ui-core"

export const getColorClasses = (
  styles: any,
  color?: ThemeColor,
  variant?: ThemeColorVariant
) => {
  if (!color) {
    return {}
  }
  return {
    [styles[`${color}`]]: !variant || variant === "default",
    [styles[`${color}-contrast`]]: variant === "contrast",
    [styles[`${color}-light`]]: variant === "light",
    [styles[`${color}-dark`]]: variant === "dark",
  }
}

export const getBgColorClasses = (
  styles: any,
  color?: ThemeColor,
  variant?: ThemeColorVariant
) => {
  if (!color) {
    return {}
  }
  return {
    [styles[`bg-${color}`]]: !variant || variant === "default",
    [styles[`bg-${color}-contrast`]]: variant === "contrast",
    [styles[`bg-${color}-light`]]: variant === "light",
    [styles[`bg-${color}-dark`]]: variant === "dark",
  }
}
