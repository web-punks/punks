import { ThemeSpace, setCssVar } from "@punks/ui-core"

export type MarginsInput = {
  mt?: number
  mb?: number
  ml?: number
  mr?: number
  mx?: number
  my?: number
  m?: number
}

export type PaddingsInput = {
  pt?: number
  pb?: number
  pl?: number
  pr?: number
  px?: number
  py?: number
  p?: number
}

const isEmptySpace = (value?: number) => value === undefined || value === null

const hasValue = (value?: number) => !isEmptySpace(value) && value !== 0

export const getMarginClasses = (styles: any, spacings: MarginsInput) => {
  return {
    [styles.ml]: hasValue(spacings.ml ?? spacings.mx ?? spacings.m),
    [styles.mr]: hasValue(spacings.mr ?? spacings.mx ?? spacings.m),
    [styles.mt]: hasValue(spacings.mt ?? spacings.my ?? spacings.m),
    [styles.mb]: hasValue(spacings.mb ?? spacings.my ?? spacings.m),
  }
}

export const getPaddingClasses = (styles: any, spacings: PaddingsInput) => {
  return {
    [styles.pl]: hasValue(spacings.pl ?? spacings.px ?? spacings.p),
    [styles.pr]: hasValue(spacings.pr ?? spacings.px ?? spacings.p),
    [styles.pt]: hasValue(spacings.pt ?? spacings.py ?? spacings.p),
    [styles.pb]: hasValue(spacings.pb ?? spacings.py ?? spacings.p),
  }
}

export const getMarginStyles = (margins: MarginsInput): React.CSSProperties => {
  return {
    ...(hasValue(margins.mb ?? margins.my ?? margins.m)
      ? setCssVar("--mb", margins.mb ?? margins.my ?? margins.m)
      : {}),
    ...(hasValue(margins.mt ?? margins.my ?? margins.m)
      ? setCssVar("--mt", margins.mt ?? margins.my ?? margins.m)
      : {}),
    ...(hasValue(margins.ml ?? margins.mx ?? margins.m)
      ? setCssVar("--ml", margins.ml ?? margins.mx ?? margins.m)
      : {}),
    ...(hasValue(margins.mr ?? margins.mx ?? margins.m)
      ? setCssVar("--mr", margins.mr ?? margins.mx ?? margins.m)
      : {}),
  }
}

export const getPaddingStyles = (
  paddings: PaddingsInput
): React.CSSProperties => {
  return {
    ...(hasValue(paddings.pb ?? paddings.py ?? paddings.p)
      ? setCssVar("--pb", paddings.pb ?? paddings.py ?? paddings.p)
      : {}),
    ...(hasValue(paddings.pt ?? paddings.py ?? paddings.p)
      ? setCssVar("--pt", paddings.pt ?? paddings.py ?? paddings.p)
      : {}),
    ...(hasValue(paddings.pl ?? paddings.px ?? paddings.p)
      ? setCssVar("--pl", paddings.pl ?? paddings.px ?? paddings.p)
      : {}),
    ...(hasValue(paddings.pr ?? paddings.px ?? paddings.p)
      ? setCssVar("--pr", paddings.pr ?? paddings.px ?? paddings.p)
      : {}),
  }
}
