import { setCssVar } from "@punks/ui-core"
import { ComponentFlexProps } from "../types"

export const getFlexClasses = (styles: any, flexProps: ComponentFlexProps) => {
  return {
    [styles.gap]: flexProps.gap,
    [styles.flex]: flexProps.flex,
    [styles.flexFill]: flexProps.flexFill,
    [styles.inlineFlex]: flexProps.inlineFlex,
    [styles[`flex-${flexProps.flexDirection}`]]: flexProps.flexDirection,
    [styles[`items-${flexProps.alignItems}`]]: flexProps.alignItems,
    [styles[`justify-${flexProps.justifyContent}`]]: flexProps.justifyContent,
  }
}

export const getFlexStyles = (
  flexProps: ComponentFlexProps
): React.CSSProperties => {
  return {
    ...setCssVar("--gap", flexProps.gap),
  }
}
