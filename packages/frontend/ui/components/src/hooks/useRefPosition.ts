import { useState, useEffect, RefObject } from "react"

export const useRefPosition = <T extends HTMLElement>(
  ref: RefObject<T>,
  options?: { watch?: boolean }
) => {
  const [rect, setRect] = useState<DOMRect | null>(null)

  useEffect(() => {
    const handleResize = () => {
      if (ref.current) {
        setRect(ref.current.getBoundingClientRect())
      }
    }

    handleResize()

    if (options?.watch !== false) {
      window.addEventListener("resize", handleResize)
      window.addEventListener("scroll", handleResize)
    }

    return () => {
      window.removeEventListener("resize", handleResize)
      window.removeEventListener("scroll", handleResize)
    }
  }, [options?.watch])

  return { rect }
}
