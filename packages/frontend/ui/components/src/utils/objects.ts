export const toDict = <TVal>(
  array: TVal[],
  keySelector: (value: TVal) => string
): Record<string, TVal> => {
  const data: Record<string, TVal> = {}
  for (const item of array) {
    data[keySelector(item)] = item
  }
  return data
}
