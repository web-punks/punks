import loadable from "@loadable/component"
const ReactJson = loadable(() => import("react-json-view"))

export interface ReactJsonViewProps {
  src: any
}

const ReactJsonView = ({ src }: ReactJsonViewProps) => {
  return (
    <>
      <ReactJson src={src} />
    </>
  )
}

export default ReactJsonView
