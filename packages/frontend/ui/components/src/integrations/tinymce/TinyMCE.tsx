import { classNames } from "@punks/ui-core"
import { Editor } from "@tinymce/tinymce-react"
import styles from "./TinyMCE.module.css"
import { useState } from "react"

export interface TinyMCEProps {
  initialValue: string
  onChange: (value: string) => void
  height?: string | number
  tinymceScriptSrc?: string
  darkMode?: boolean
  className?: string
}

export const TinyMCE = ({
  initialValue,
  onChange,
  height = 400,
  tinymceScriptSrc = "/plugins/tinymce/tinymce.min.js",
  darkMode,
  className,
}: TinyMCEProps) => {
  const [focus, setFocus] = useState(false)
  return (
    <div
      className={classNames(className, styles.root, {
        [styles.focus]: focus,
      })}
    >
      <Editor
        tinymceScriptSrc={tinymceScriptSrc}
        initialValue={initialValue}
        onFocusIn={() => setFocus(true)}
        onFocusOut={() => setFocus(false)}
        init={{
          height,
          menubar: false,
          // plugins: [
          //   "advlist autolink lists link image charmap print preview anchor",
          //   "searchreplace visualblocks code fullscreen",
          //   "insertdatetime media table paste code help wordcount",
          // ],
          skin: darkMode ? "oxide-dark" : "oxide",
          content_css: darkMode ? "dark" : "default",
          toolbar:
            "blocks | bold italic underline forecolor formatpainter | alignleft aligncenter alignright alignjustify | numlist bullist | link insertfile image | checklist | casechange",
          statusbar: false,
        }}
        onEditorChange={onChange}
      />
    </div>
  )
}
