import { classNames, normalizePxSize, setCssVar } from "@punks/ui-core"
import styles from "./ScrollView.module.css"

export interface ScrollViewProps {
  children: any
  maxHeight?: number | string
  className?: string
}

const ScrollView = ({ children, maxHeight, className }: ScrollViewProps) => {
  return (
    <div
      style={{
        ...setCssVar("--max-h", normalizePxSize(maxHeight)),
      }}
      className={classNames(styles.root, className)}
    >
      {children}
    </div>
  )
}

export default ScrollView
