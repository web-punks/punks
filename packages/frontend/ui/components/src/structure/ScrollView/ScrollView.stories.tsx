import type { Meta, StoryObj } from "@storybook/react"

import { ScrollView } from "."
import { Typography } from "../../atoms"
import { range } from "@punks/ui-core"

const meta = {
  title: "Ui/Structure/ScrollView",
  component: ScrollView,
  tags: ["autodocs"],
  argTypes: {},
} satisfies Meta<typeof ScrollView>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    maxHeight: 300,
    children: (
      <>
        {range(100).map((i) => (
          <Typography>Lorem ipsium</Typography>
        ))}
      </>
    ),
  },
}
