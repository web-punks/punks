import React from "react"
import { createPortal } from "react-dom"

export type PortalProps = {
  children: any
  container?: Element | DocumentFragment
}

const BodyPortal = ({ children, container }: PortalProps) => {
  const [mounted, setMounted] = React.useState(false)

  React.useEffect(() => {
    setMounted(true)
  }, [])

  return <>{mounted && createPortal(children, container ?? document.body)}</>
}

export default BodyPortal
