import styles from "./Separator.module.css"
import { classNames } from "@punks/ui-core"

export interface SeparatorProps {
  children?: any
  className?: string
  margin?: "none" | "xs" | "sm" | "md" | "lg"
}

const Separator = ({ children, className, margin = "sm" }: SeparatorProps) => {
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.marginXs]: margin === "xs",
          [styles.marginSm]: margin === "sm",
          [styles.marginMd]: margin === "md",
          [styles.marginLg]: margin === "lg",
        },
        className
      )}
    >
      {children}
    </div>
  )
}

export default Separator
