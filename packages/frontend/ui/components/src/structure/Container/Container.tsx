import React from "react"
import styles from "./Container.module.css"
import {
  getMarginStyles,
  getPaddingStyles,
  getMarginClasses,
  getPaddingClasses,
} from "../../helpers"
import { classNames } from "@punks/ui-core"
import { ComponentMarginProps, ComponentPaddingProps } from "../../types"

export interface ContainerProps
  extends ComponentMarginProps,
    ComponentPaddingProps {
  minHScreen?: boolean
  component?: keyof JSX.IntrinsicElements
  className?: string
  children?: any
  style?: React.CSSProperties
}

const Container = ({
  children,
  className,
  component,
  m,
  mb,
  minHScreen,
  ml,
  mr,
  mt,
  mx,
  my,
  p,
  pb,
  pl,
  pr,
  pt,
  px,
  py,
  style,
}: ContainerProps) => {
  const Tag = component ?? "div"
  const spacings = {
    mb,
    mt,
    ml,
    mr,
    mx,
    my,
    m,
    pb,
    pt,
    pl,
    pr,
    px,
    py,
    p,
  }
  return (
    <Tag
      style={{
        ...getMarginStyles(spacings),
        ...getPaddingStyles(spacings),
        ...style,
      }}
      className={classNames(
        styles.root,
        {
          ...getMarginClasses(styles, spacings),
          ...getPaddingClasses(styles, spacings),
          [styles.minHScreen]: minHScreen,
        },
        className
      )}
    >
      {children}
    </Tag>
  )
}

export default Container
