import type { Meta, StoryObj } from "@storybook/react"

import { Anchor } from "."
import { Button } from "../../atoms"

const meta = {
  title: "Ui/Structure/Anchor",
  component: Anchor,
  tags: ["autodocs"],
  args: {
    children: (
      <Button variant="filled" color="primary">
        Click me
      </Button>
    ),
    content: <div>Content</div>,
  },
} satisfies Meta<typeof Anchor>
export default meta

type Story = StoryObj<typeof meta>

export const AnchorDefault: Story = {
  args: {},
}

export const AnchorBottomRight: Story = {
  args: {
    position: "bottomRight",
  },
}
