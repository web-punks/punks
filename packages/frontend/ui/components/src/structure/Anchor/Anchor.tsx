import ReactDOM from "react-dom"
import styles from "./Anchor.module.css"
import { classNames } from "@punks/ui-core"
import { useRef, useState } from "react"
import { useRefPosition } from "../../hooks/useRefPosition"

export type AnchorClasses = {
  root?: string
  trigger?: string
  content?: string
  backdrop?: string
  anchor?: string
}

export type AnchorPosition =
  | "topLeft"
  | "topRight"
  | "bottomLeft"
  | "bottomRight"

export type AnchorOffset = {
  x?: number
  y?: number
}

export interface AnchorProps {
  children: React.ReactNode
  content: React.ReactNode
  open?: boolean
  defaultOpen?: boolean
  onOpenChange?: (value: boolean) => void
  classes?: AnchorClasses
  className?: string
  zIndex?: number
  offset?: AnchorOffset
  position?: AnchorPosition
  backdrop?: boolean
}

const getContentRelativePosition = (
  position: AnchorPosition,
  triggerSize: DOMRect,
  offset?: AnchorOffset
): React.CSSProperties => {
  switch (position) {
    case "topLeft":
      return {
        left: 0,
        bottom: triggerSize.height + (offset?.y ?? 0),
      }
    case "topRight":
      return {
        right: 0,
        bottom: triggerSize.height + (offset?.y ?? 0),
      }
    case "bottomRight":
      return {
        right: 0,
        top: triggerSize.height + (offset?.y ?? 0),
      }
    default:
    case "bottomLeft":
      return {
        left: 0,
        top: triggerSize.height + (offset?.y ?? 0),
      }
  }
}

export const Anchor = ({
  children,
  content,
  open,
  defaultOpen,
  onOpenChange,
  className,
  classes,
  zIndex,
  offset,
  position = "bottomLeft",
  backdrop = true,
}: AnchorProps) => {
  const [uncontrolledOpen, setUncontrolledOpen] = useState(defaultOpen ?? false)
  const isControlled = open !== undefined
  const triggerRef = useRef<HTMLDivElement>(null)

  const isOpen = isControlled ? open : uncontrolledOpen

  const triggerPosition = useRefPosition(triggerRef, {
    watch: isOpen,
  })

  const handleClick = () => {
    if (isControlled) {
      onOpenChange?.(!open)
    } else {
      setUncontrolledOpen(!uncontrolledOpen)
    }
  }

  return (
    <div className={classNames(styles.root, classes?.root, className)}>
      <div
        ref={triggerRef}
        className={classNames(styles.trigger, classes?.trigger)}
        onClick={handleClick}
      >
        {children}
      </div>
      {isOpen &&
        ReactDOM.createPortal(
          <div
            {...(backdrop
              ? {
                  className: classNames(styles.backdrop, classes?.backdrop),
                  onClick: handleClick,
                  style: { zIndex: zIndex ? zIndex - 1 : undefined },
                }
              : {})}
          >
            <div
              className={classNames(styles.anchor, classes?.anchor)}
              style={{
                transform: `translate(${triggerPosition.rect?.x}px, ${triggerPosition.rect?.y}px)`,
                width: triggerPosition.rect?.width,
                height: triggerPosition.rect?.height,
                zIndex,
              }}
            >
              <div className={classNames(styles.anchorInner)}>
                <div
                  onClick={(e) => e.stopPropagation()}
                  className={classNames(styles.content, classes?.content)}
                  style={
                    triggerPosition.rect
                      ? getContentRelativePosition(
                          position,
                          triggerPosition.rect,
                          offset
                        )
                      : undefined
                  }
                >
                  {content}
                </div>
              </div>
            </div>
          </div>,
          document.body
        )}
    </div>
  )
}
