export interface RawHtmlProps {
  html: string
  className?: string
  styles?: React.CSSProperties
  component?: keyof JSX.IntrinsicElements
}

const RawHtml = ({ html, className, component, styles }: RawHtmlProps) => {
  const Component = component || "div"
  return (
    <Component
      style={styles}
      className={className}
      dangerouslySetInnerHTML={{
        __html: html,
      }}
    ></Component>
  )
}

export default RawHtml
