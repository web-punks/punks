import type { Meta, StoryObj } from "@storybook/react"

import { Frame } from "."

const meta = {
  title: "Ui/Structure/Frame",
  component: Frame,
  tags: ["autodocs"],
  decorators: [
    (Story) => (
      <div style={{ width: 400 }}>
        <Story />
      </div>
    ),
  ],
} satisfies Meta<typeof Frame>
export default meta

type Story = StoryObj<typeof meta>

export const FrameSquare: Story = {
  args: {
    children: (
      <img
        style={{
          width: "100%",
        }}
        src="https://images.unsplash.com/photo-1679176031571-85793af43bc2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
      />
    ),
    shape: "square",
  } as never,
}

export const FrameCircle: Story = {
  args: {
    children: (
      <img
        style={{
          width: "100%",
        }}
        src="https://images.unsplash.com/photo-1679176031571-85793af43bc2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
      />
    ),
    shape: "circle",
  } as never,
}

export const FrameRectangle: Story = {
  args: {
    children: (
      <img
        style={{
          width: "100%",
        }}
        src="https://images.unsplash.com/photo-1679176031571-85793af43bc2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
      />
    ),
    shape: "rectangle",
    ratio: 0.5,
  } as never,
}

export const FrameRectangleOffsetYAbs: Story = {
  args: {
    children: (
      <img
        style={{
          width: "100%",
        }}
        src="https://images.unsplash.com/photo-1679176031571-85793af43bc2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
      />
    ),
    shape: "rectangle",
    ratio: 0.5,
    contentOffset: {
      y: -50,
    },
  } as never,
}

export const FrameRectangleOffsetYPercent: Story = {
  args: {
    children: (
      <img
        style={{
          width: "100%",
        }}
        src="https://images.unsplash.com/photo-1679176031571-85793af43bc2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
      />
    ),
    shape: "rectangle",
    ratio: 0.5,
    contentOffset: {
      y: "-10%",
    },
  } as never,
}

export const FrameRectangleVerticalCenter: Story = {
  args: {
    children: (
      <img
        style={{
          width: "100%",
        }}
        src="https://images.unsplash.com/photo-1679176031571-85793af43bc2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
      />
    ),
    shape: "rectangle",
    ratio: 0.5,
    verticalAlignment: "center",
  } as never,
}

export const FrameRectangleVerticalCenterOffsetYAbs: Story = {
  args: {
    children: (
      <img
        style={{
          width: "100%",
        }}
        src="https://images.unsplash.com/photo-1679176031571-85793af43bc2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
      />
    ),
    shape: "rectangle",
    ratio: 0.5,
    verticalAlignment: "center",
    contentOffset: {
      y: -50,
    },
  } as never,
}

export const FrameRectangleVerticalBottom: Story = {
  args: {
    children: (
      <img
        style={{
          width: "100%",
        }}
        src="https://images.unsplash.com/photo-1679176031571-85793af43bc2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
      />
    ),
    shape: "rectangle",
    ratio: 0.5,
    verticalAlignment: "bottom",
  } as never,
}

export const FrameRectangle2: Story = {
  args: {
    children: (
      <img
        style={{
          width: "100%",
        }}
        src="https://images.unsplash.com/photo-1679176031571-85793af43bc2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
      />
    ),
    shape: "rectangle",
    ratio: 2,
  } as never,
}
