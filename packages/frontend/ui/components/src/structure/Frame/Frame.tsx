import styles from "./Frame.module.css"
import { classNames, setCssVar } from "@punks/ui-core"
import { Circle, Rectangle, Square } from "../../shapes"

export type FrameShape = "square" | "rounded" | "circle"

export type SquareFrameProps = {
  shape: "square"
}

export type RectangleFrameProps = {
  shape: "rectangle"
  ratio: number
}

export type CircleFrameProps = {
  shape: "circle"
}

export type FrameShapeProps =
  | {
      shape: "square"
    }
  | {
      shape: "rectangle"
      ratio: number
    }
  | {
      shape: "rounded"
    }
  | {
      shape: "circle"
    }

export type VerticalAlignment = "top" | "center" | "bottom"

export interface ContentOffset {
  x?: string | number
  y?: string | number
}

export type FrameProps = FrameShapeProps & {
  children: any
  className?: string
  verticalAlignment?: VerticalAlignment
  contentOffset?: ContentOffset
}

const formatSize = (value?: string | number) => {
  if (typeof value === "number") {
    return `${value}px`
  }

  return value
}

const FrameContent = (props: FrameProps) => {
  return (
    <div
      style={{
        ...setCssVar("--offset-x", formatSize(props.contentOffset?.x)),
        ...setCssVar("--offset-y", formatSize(props.contentOffset?.y)),
      }}
      className={classNames({
        [styles.offsetX]: !!props.contentOffset?.x,
        [styles.offsetY]: !!props.contentOffset?.y,
      })}
    >
      {props.children}
    </div>
  )
}

const FrameWrapper = (props: FrameProps) => {
  const rootClasses = styles.root
  const contentClasses = classNames(styles.content, {
    [styles.vTop]: props.verticalAlignment === "top",
    [styles.vCenter]: props.verticalAlignment === "center",
    [styles.vBottom]: props.verticalAlignment === "bottom",
  })

  switch (props.shape) {
    case "square":
      return (
        <Square
          className={props.className}
          classes={{
            root: rootClasses,
            content: contentClasses,
          }}
        >
          {props.children}
        </Square>
      )
    case "rectangle":
      return (
        <Rectangle
          className={props.className}
          classes={{
            root: rootClasses,
            content: contentClasses,
          }}
          ratio={props.ratio}
        >
          {props.children}
        </Rectangle>
      )
    case "circle":
      return (
        <Circle
          className={props.className}
          classes={{
            root: rootClasses,
            content: contentClasses,
          }}
        >
          {props.children}
        </Circle>
      )
    default:
      return <div className={props.className}>{props.children}</div>
  }
}

const Frame = (props: FrameProps) => {
  const { children, className, ...other } = props
  return (
    <FrameWrapper {...other} className={className}>
      <FrameContent {...other}>{children}</FrameContent>
    </FrameWrapper>
  )
}

export default Frame
