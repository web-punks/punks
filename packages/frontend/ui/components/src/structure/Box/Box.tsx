import React from "react"
import styles from "./Box.module.css"
import {
  getPaddingClasses,
  getMarginClasses,
  getMarginStyles,
  getPaddingStyles,
} from "../../helpers/spacings"
import { getBgColorClasses, getColorClasses } from "../../helpers/colors"
import { getFlexClasses, getFlexStyles } from "../../helpers/flex"
import { classNames } from "@punks/ui-core"
import {
  ComponentBackgroundColorProps,
  ComponentColorProps,
  ComponentFlexProps,
  ComponentMarginProps,
  ComponentPaddingProps,
} from "../../types"

export interface BoxProps
  extends ComponentMarginProps,
    ComponentPaddingProps,
    ComponentColorProps,
    ComponentBackgroundColorProps,
    ComponentFlexProps {
  minHScreen?: boolean
  component?: keyof JSX.IntrinsicElements
  className?: string
  children?: any
  style?: React.CSSProperties
  onClick?: () => void
  cursorPointer?: boolean
  hidden?: boolean
}

const Box = ({
  bgColor,
  bgColorVariant,
  children,
  className,
  color,
  colorVariant,
  component,
  m,
  mb,
  minHScreen,
  ml,
  mr,
  mt,
  mx,
  my,
  p,
  pb,
  pl,
  pr,
  pt,
  px,
  py,
  style,
  onClick,
  inlineFlex,
  flex,
  flexFill,
  flexDirection,
  alignItems,
  justifyContent,
  gap,
  cursorPointer,
  hidden,
}: BoxProps) => {
  const Tag = component ?? "div"
  const spacings = {
    mb,
    mt,
    ml,
    mr,
    mx,
    my,
    m,
    pb,
    pt,
    pl,
    pr,
    px,
    py,
    p,
  }
  const flexProps = {
    alignItems,
    justifyContent,
    flex,
    inlineFlex,
    flexFill,
    flexDirection,
    gap,
  }
  return (
    <Tag
      style={{
        ...getMarginStyles(spacings),
        ...getPaddingStyles(spacings),
        ...getFlexStyles(flexProps),
        ...style,
      }}
      className={classNames(
        {
          ...getColorClasses(styles, color, colorVariant),
          ...getBgColorClasses(styles, bgColor, bgColorVariant),
          ...getMarginClasses(styles, spacings),
          ...getPaddingClasses(styles, spacings),
          ...getFlexClasses(styles, flexProps),
          [styles.minHScreen]: minHScreen,
          cursorPointer,
          [styles.cursorPointer]: cursorPointer,
          [styles.hidden]: hidden,
        },
        className
      )}
      onClick={onClick}
    >
      {children}
    </Tag>
  )
}

export default Box
