import type { Meta, StoryObj } from "@storybook/react"

import { Box } from "."

const meta = {
  title: "Ui/Structure/Box",
  component: Box,
  tags: ["autodocs"],
  argTypes: {},
} satisfies Meta<typeof Box>
export default meta

type Story = StoryObj<typeof meta>

export const BoxDefault: Story = {
  args: {
    children: <div>content</div>,
  },
}
