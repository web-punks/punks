import type { Meta, StoryObj } from "@storybook/react"

import { Row } from "."
import { Col } from "../Col"

const meta = {
  title: "Ui/Structure/Row",
  component: Row,
  tags: ["autodocs"],
  argTypes: {},
} satisfies Meta<typeof Row>
export default meta

type Story = StoryObj<typeof meta>

export const RowDefault: Story = {
  args: {
    children: (
      <>
        <Col
          size={4}
          style={{
            backgroundColor: "#008f5b",
            padding: 8,
          }}
        >
          Col 1
        </Col>
        <Col
          size={4}
          style={{
            backgroundColor: "#008f5b",
            padding: 8,
          }}
        >
          Col 2
        </Col>
        <Col
          size={4}
          style={{
            backgroundColor: "#008f5b",
            padding: 8,
          }}
        >
          Col 3
        </Col>
      </>
    ),
  },
}

export const RowResponsive: Story = {
  args: {
    children: (
      <>
        <Col
          size={{
            xs: 12,
            sm: 6,
            md: 4,
          }}
          style={{
            backgroundColor: "#008f5b",
            padding: 8,
          }}
        >
          Col 1
        </Col>
        <Col
          size={{
            xs: 12,
            sm: 6,
            md: 4,
          }}
          style={{
            backgroundColor: "#008f5b",
            padding: 8,
          }}
        >
          Col 2
        </Col>
        <Col
          size={{
            xs: 12,
            sm: 6,
            md: 4,
          }}
          style={{
            backgroundColor: "#008f5b",
            padding: 8,
          }}
        >
          Col 3
        </Col>
      </>
    ),
  },
}

export const RowResponsiveGap2: Story = {
  args: {
    gap: 2,
    children: (
      <>
        <Col
          size={{
            xs: 12,
            sm: 6,
            md: 4,
          }}
          style={{
            backgroundColor: "#008f5b",
            padding: 8,
          }}
        >
          Col 1
        </Col>
        <Col
          size={{
            xs: 12,
            sm: 6,
            md: 4,
          }}
          style={{
            backgroundColor: "#008f5b",
            padding: 8,
          }}
        >
          Col 2
        </Col>
        <Col
          size={{
            xs: 12,
            sm: 6,
            md: 4,
          }}
          style={{
            backgroundColor: "#008f5b",
            padding: 8,
          }}
        >
          Col 3
        </Col>
      </>
    ),
  },
}

export const RowResponsiveGap2Fluid: Story = {
  args: {
    gap: 2,
    children: (
      <>
        <Col
          size={{
            xs: 12,
            sm: 5,
            md: 4,
          }}
          style={{
            backgroundColor: "#008f5b",
            padding: 8,
          }}
        >
          Col 1
        </Col>
        <Col
          size={{
            xs: 12,
            sm: "fluid",
            md: "fluid",
          }}
          style={{
            backgroundColor: "#008f5b",
            padding: 8,
          }}
        >
          Col 2
        </Col>
      </>
    ),
  },
}
