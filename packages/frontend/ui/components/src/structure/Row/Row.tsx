import React from "react"
import styles from "./Row.module.css"
import { classNames, setCssVar } from "@punks/ui-core"
import {
  ComponentBackgroundColorProps,
  ComponentColorProps,
  ComponentPaddingProps,
} from "../../types"
import {
  getPaddingStyles,
  getPaddingClasses,
  getColorClasses,
  getBgColorClasses,
} from "../../helpers"

export type RowGap = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10

export interface RowProps
  extends ComponentPaddingProps,
    ComponentColorProps,
    ComponentBackgroundColorProps {
  children: any
  gap?: RowGap
  gapY?: RowGap
  gapX?: RowGap
  style?: React.CSSProperties
  className?: string
  hFull?: boolean
  cols?: number
}

const Row = ({
  children,
  bgColor,
  bgColorVariant,
  color,
  colorVariant,
  p,
  pb,
  pl,
  pr,
  pt,
  px,
  py,
  gapY,
  gap,
  gapX,
  style,
  className,
  hFull,
  cols = 12,
}: RowProps) => {
  const spacings = {
    pb,
    pt,
    pl,
    pr,
    px,
    py,
    p,
  }
  return (
    <div
      style={{
        ...getPaddingStyles(spacings),
        ...style,
        ...setCssVar("--cols", cols),
      }}
      className={classNames(
        styles.row,
        {
          [styles.hFull]: hFull,
          [styles[`gapY-${gapY ?? gap}`]]: !!(gapY ?? gap),
          [styles[`gapX-${gapX ?? gap}`]]: !!(gapX ?? gap),
          ...getPaddingClasses(styles, spacings),
          ...getColorClasses(styles, color, colorVariant),
          ...getBgColorClasses(styles, bgColor, bgColorVariant),
        },
        className
      )}
    >
      {children}
    </div>
  )
}

export default Row
