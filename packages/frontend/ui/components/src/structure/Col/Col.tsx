import React from "react"
import styles from "./Col.module.css"
import {
  ResponsiveValues,
  classNames,
  getResponsiveValue,
  setCssVar,
} from "@punks/ui-core"

export type ColSize = number | "fluid"

export interface ColProps {
  size?: ColSize | ResponsiveValues<ColSize>
  children?: any
  style?: React.CSSProperties
  className?: string
}

const Col = ({ children, size, style, className }: ColProps) => {
  const xs = getResponsiveValue("xs", size)
  const sm = getResponsiveValue("sm", size) ?? xs
  const md = getResponsiveValue("md", size) ?? sm
  const lg = getResponsiveValue("lg", size) ?? md
  const xl = getResponsiveValue("xl", size) ?? lg

  return (
    <div
      style={{
        ...style,
        ...setCssVar("--col-size", xs),
        ...setCssVar("--col-size-sm", sm),
        ...setCssVar("--col-size-md", md),
        ...setCssVar("--col-size-lg", lg),
        ...setCssVar("--col-size-xl", xl),
      }}
      className={classNames(
        {
          [styles.col]: xs !== "fluid",
          [styles.colSm]: sm !== "fluid",
          [styles.colMd]: md !== "fluid",
          [styles.colLg]: lg !== "fluid",
          [styles.colXl]: xl !== "fluid",
          [styles.colFluid]: xs === "fluid",
          [styles.colFluidSm]: sm === "fluid",
          [styles.colFluidMd]: md === "fluid",
          [styles.colFluidLg]: lg === "fluid",
          [styles.colFluidXl]: xl === "fluid",
        },
        className
      )}
    >
      {children}
    </div>
  )
}

export default Col
