export {
  default as BarChart,
  BarChartProps,
  BarChartConfig as BarConfig,
} from "./BarChart"
