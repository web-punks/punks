import type { Meta, StoryObj } from "@storybook/react"
import BarChart, { BarChartProps } from "./BarChart"

const meta = {
  title: "Ui/Charts/BarChart",
  component: BarChart,
  tags: ["autodocs"],
} satisfies Meta<typeof BarChart>
export default meta

const barConfig = [
  { dataKey: "col1", fill: "primary", name: "Colonna 1" },
  { dataKey: "col2", fill: "primary", name: "Colonna 2" },
  { dataKey: "col3", fill: "primary", name: "Colonna 3" },
]

export const Default: StoryObj<BarChartProps> = {
  args: {
    data: [
      { name: "Riga 1", col1: 1, col2: 2, col3: 3 },
      { name: "Riga 2", col1: 2, col2: 1, col3: 3 },
      { name: "Riga 3", col1: 3, col2: 2, col3: 1 },
    ],
    barConfig: barConfig,
  },
}
