import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts"

export type BarChartConfig = {
  dataKey: string
  fill: string
  name?: string
}

export interface BarChartProps {
  data: any[]
  barConfig: BarChartConfig[]
  width?: number
  height?: number
}

const BarChartComponent = ({
  data,
  barConfig,
  width = 600,
  height = 300,
}: BarChartProps) => {
  return (
    <ResponsiveContainer width={width} height={height}>
      <BarChart
        data={data}
        margin={{ top: 20, right: 30, left: 20, bottom: 5 }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend verticalAlign="top" wrapperStyle={{ lineHeight: "40px" }} />
        {barConfig.map((config, index) => (
          <Bar key={index} {...config} />
        ))}
      </BarChart>
    </ResponsiveContainer>
  )
}

export default BarChartComponent
