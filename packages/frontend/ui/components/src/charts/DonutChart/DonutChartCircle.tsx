import React, { useEffect, useRef, useState } from "react"
import { DonutChartItem } from "./types"
import { getThemeColorVar } from "@punks/ui-core"

export type DonutChartAlignment = "start" | "center" | "end"

export interface DonutChartCircleProps {
  data: DonutChartItem[]
  total: number
  strokeWidth: number
  spacing: number
  animated: boolean
  animationSpeedFactor: number
  className?: string
  alignment?: DonutChartAlignment
}

type ChartSegment = {
  item: DonutChartItem
  percentageValue: number
  percentageOffset: number
}

const mapSegments = (data: DonutChartItem[], total: number) => {
  const segments: ChartSegment[] = []
  let percentageOffset = 0
  for (const item of data) {
    const percentageValue = item.value / total
    segments.push({
      item,
      percentageValue,
      percentageOffset,
    })
    percentageOffset += percentageValue
  }

  return segments
}

const calculateCx = (input: {
  width: number
  radius: number
  borderWidth: number
  alignment: DonutChartAlignment
}) => {
  switch (input.alignment) {
    case "start":
      return input.radius + input.borderWidth
    case "center":
      return input.width / 2
    case "end":
      return input.width - input.radius / 2
  }
}

const DonutChartCircle = ({
  data,
  total,
  strokeWidth,
  spacing,
  animated,
  animationSpeedFactor,
  alignment = "center",
  className,
}: DonutChartCircleProps) => {
  const [size, setSize] = useState({ width: 0, height: 0 })

  const [animationProgress, setAnimationProgress] = useState(animated ? 0 : 100)
  const ref = useRef<SVGSVGElement>(null)

  useEffect(() => {
    const handleResize = () => {
      if (ref.current) {
        const rect = ref.current.getBoundingClientRect()
        setSize({ width: rect.width, height: rect.height })
      }
    }

    handleResize()
    window.addEventListener("resize", handleResize)
    return () => {
      window.removeEventListener("resize", handleResize)
    }
  }, [])

  useEffect(() => {
    if (!animated) {
      return
    }

    const step = (100 / data.length) * animationSpeedFactor
    let newOffset = 0

    const animate = (timestamp: number) => {
      if (newOffset <= 100) {
        setAnimationProgress(newOffset)
        newOffset = Math.min(newOffset + step, 100)
        window.requestAnimationFrame(animate)
      }
    }

    window.requestAnimationFrame(animate)
  }, [data])

  const radius = Math.min(size.width, size.height) / 2 - strokeWidth / 2
  const circumference = 2 * Math.PI * radius

  return (
    <svg
      width="100%"
      height="100%"
      viewBox={`0 0 ${size.width} ${size.height}`}
      ref={ref}
      className={className}
    >
      {mapSegments(data, total).map(
        ({ item, percentageOffset, percentageValue }, index) => {
          const progressFactor = animationProgress / 100
          const dasharray = `${
            (percentageOffset + percentageValue) *
            circumference *
            progressFactor
          } ${circumference}`
          const dashoffset = -(
            percentageOffset *
            circumference *
            progressFactor
          )

          return (
            <g key={index}>
              <circle
                cx={calculateCx({
                  alignment,
                  radius,
                  borderWidth: strokeWidth,
                  width: size.width,
                })}
                cy={size.height / 2}
                r={radius > 0 ? radius : 0}
                fill="transparent"
                stroke={
                  item.color
                    ? `var(${getThemeColorVar(item.color, item.colorVariant)})`
                    : item.colorValue
                }
                strokeWidth={strokeWidth}
                strokeDasharray={dasharray}
                strokeDashoffset={!isNaN(dashoffset) ? dashoffset : undefined}
              />
              {/* {item.label && (
                <text
                  x={size.width / 2}
                  y={size.height / 2}
                  dominantBaseline="middle"
                  textAnchor="middle"
                  fontSize={strokeWidth / 2}
                >
                  {item.label}
                </text>
              )} */}
            </g>
          )
        }
      )}
    </svg>
  )
}

export default DonutChartCircle
