import { DonutChartItem } from "./types"
import DonutChartCircle from "./DonutChartCircle"
import DonutChartLabels from "./DonutChartLabels"
import { classNames } from "@punks/ui-core"
import styles from "./DonutChart.module.css"

export type DonutChartLayout = "horizontal" | "vertical"
export type DonutChartAlignment = "start" | "center" | "end"

export class DonutChartClasses {
  root?: string
  chart?: string
  circle?: string
  labels?: string
  label?: string
}

export interface DonutChartProps {
  data: DonutChartItem[]
  total: number
  strokeWidth?: number
  spacing?: number
  animated?: boolean
  animationSpeedFactor?: number
  layout?: DonutChartLayout
  alignment?: DonutChartAlignment
  classes?: DonutChartClasses
}

const DonutChart = ({
  data,
  total,
  strokeWidth = 16,
  spacing = 4,
  animated,
  animationSpeedFactor = 0.05,
  layout = "vertical",
  alignment = "center",
  classes,
}: DonutChartProps) => {
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.horizontal]: layout === "horizontal",
          [styles.vertical]: layout === "vertical",
        },
        classes?.root
      )}
    >
      <div className={classes?.chart}>
        <DonutChartCircle
          data={data}
          total={total}
          strokeWidth={strokeWidth}
          spacing={spacing}
          animated={animated ?? false}
          animationSpeedFactor={animationSpeedFactor}
          className={classes?.circle}
          alignment={alignment}
        />
      </div>
      <DonutChartLabels
        data={data}
        classes={{
          item: classes?.label,
          root: classNames(
            {
              [styles.chartStart]: alignment === "start",
              [styles.chartEnd]: alignment === "end",
            },
            classes?.labels
          ),
        }}
        layout={layout === "horizontal" ? "vertical" : "horizontal"}
      />
    </div>
  )
}

export default DonutChart
