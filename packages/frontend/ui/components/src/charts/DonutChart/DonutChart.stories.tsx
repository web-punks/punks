import type { Meta, StoryObj } from "@storybook/react"

import { DonutChart, DonutChartItem } from "."

const meta = {
  title: "Ui/Charts/DonutChart",
  component: DonutChart,
  tags: ["autodocs"],
} satisfies Meta<typeof DonutChart>
export default meta

type Story = StoryObj<typeof meta>

const data: DonutChartItem[] = [
  {
    value: 10,
    color: "primary",
    colorVariant: "dark",
    label: "label 1",
    description: "description 1",
  },
  {
    value: 25,
    color: "primary",
    colorVariant: "default",
    label: "label 2",
    description: "description 2",
  },
  {
    value: 60,
    color: "primary",
    colorVariant: "light",
    label: "label 3",
    description: "description 3",
  },
]

export const DonutChartDefault: Story = {
  args: {
    total: 100,
    data,
  },
}

export const DonutChartAnimatedVertical: Story = {
  args: {
    total: 100,
    data,
    animated: true,
    layout: "vertical",
  },
}

export const DonutChartAnimatedVerticalLeft: Story = {
  args: {
    total: 100,
    data,
    animated: true,
    layout: "vertical",
    alignment: "start",
  },
}

export const DonutChartAnimatedHorizontal: Story = {
  args: {
    total: 100,
    data,
    animated: true,
    layout: "horizontal",
  },
}

const colors = ["#FF0000", "#00FF00", "#0000FF"]

export const DonutChartCustomColors: Story = {
  args: {
    total: 100,
    data: data.map((item, i) => ({
      ...item,
      color: undefined,
      colorValue: colors[i],
    })),
  },
}
