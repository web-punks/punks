import { ThemeColor, ThemeColorVariant } from "@punks/ui-core"

export type DonutChartItem = {
  value: number
  label?: string
  description?: string
  color?: ThemeColor
  colorVariant?: ThemeColorVariant
  colorValue?: string
}
