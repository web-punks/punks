import styles from "./DonutChartLabels.module.css"
import { DonutChartItem } from "./types"
import { Typography } from "../../atoms"
import { Square } from "../../shapes"
import { classNames, setCssVar, setThemeColorVar } from "@punks/ui-core"

export interface DonutChartLabelsClasses {
  root?: string
  item?: string
}

export type DonutChartLabelsLayout = "horizontal" | "vertical"

export interface DonutChartLabelsProps {
  data: DonutChartItem[]
  classes?: DonutChartLabelsClasses
  layout?: DonutChartLabelsLayout
}

const DonutChartLabels = ({ data, classes, layout }: DonutChartLabelsProps) => {
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.horizontal]: layout === "horizontal",
          [styles.vertical]: layout === "vertical",
        },
        classes?.root
      )}
    >
      {data.map((item, i) => (
        <div key={i} className={classNames(styles.item, classes?.item)}>
          <Square
            style={{
              ...(item.color
                ? {
                    ...setThemeColorVar(
                      "--color",
                      item.color,
                      item.colorVariant
                    ),
                  }
                : {}),
              ...(item.colorValue
                ? {
                    ...setCssVar("--color", item.colorValue),
                  }
                : {}),
            }}
            className={styles.color}
          />
          <div>
            <Typography variant="caption2">{item.label}</Typography>
            <Typography variant="label3">{item.description}</Typography>
          </div>
        </div>
      ))}
    </div>
  )
}

export default DonutChartLabels
