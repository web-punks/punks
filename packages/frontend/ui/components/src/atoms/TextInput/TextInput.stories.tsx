import type { Meta, StoryObj } from "@storybook/react"

import { TextInput } from "."

const SearchIcon = () => (
  <svg
    width="15"
    height="15"
    viewBox="0 0 15 15"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M5.66667 10.3333C8.244 10.3333 10.3333 8.244 10.3333 5.66667C10.3333 3.08934 8.244 1 5.66667 1C3.08934 1 1 3.08934 1 5.66667C1 8.244 3.08934 10.3333 5.66667 10.3333Z"
      stroke="currentColor"
      strokeWidth="1.6"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.0001 13L9.00012 9"
      stroke="currentColor"
      strokeWidth="1.6"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
  </svg>
)

const meta = {
  title: "Ui/Atoms/TextInput",
  component: TextInput,
  argTypes: {},
  args: {
    placeholder: "Placeholder",
    onChange: (e) => console.log(e),
  },
  tags: ["autodocs"],
} satisfies Meta<typeof TextInput>
export default meta

type Story = StoryObj<typeof meta>

export const OutlinedDefault: Story = {
  args: {
    variant: "outlined",
  },
}

export const OutlinedNoOutline: Story = {
  args: {
    variant: "outlined",
    outline: "none",
  },
}

export const OutlinedSm: Story = {
  args: {
    variant: "outlined",
    size: "sm",
  },
}

export const OutlinedSmFullWidth: Story = {
  args: {
    variant: "outlined",
    size: "sm",
    fullWidth: true,
  },
}

export const OutlinedMd: Story = {
  args: {
    variant: "outlined",
    size: "md",
  },
}

export const OutlinedMdNumeric: Story = {
  args: {
    variant: "outlined",
    size: "md",
    type: "number",
  },
}

export const OutlinedMdDisabled: Story = {
  args: {
    variant: "outlined",
    size: "md",
    disabled: true,
  },
}

export const OutlinedMdFullWidth: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
  },
}

export const OutlinedMdStartAdornment: Story = {
  args: {
    variant: "outlined",
    size: "md",
    startAdornment: <SearchIcon />,
  },
}

export const OutlinedMdError: Story = {
  args: {
    variant: "outlined",
    size: "md",
    error: true,
  },
}

export const OutlinedLg: Story = {
  args: {
    variant: "outlined",
    size: "lg",
  },
}

export const OutlinedLgFullWidth: Story = {
  args: {
    variant: "outlined",
    size: "lg",
    fullWidth: true,
  },
}

export const FilledSm: Story = {
  args: {
    variant: "filled",
    size: "sm",
  },
}

export const FilledSmFullWidth: Story = {
  args: {
    variant: "filled",
    size: "sm",
    fullWidth: true,
  },
}

export const FilledMd: Story = {
  args: {
    variant: "filled",
    size: "md",
  },
}

export const FilledMdFullWidth: Story = {
  args: {
    variant: "filled",
    size: "md",
    fullWidth: true,
  },
}

export const FilledMdStartAdornment: Story = {
  args: {
    variant: "filled",
    size: "md",
    startAdornment: <SearchIcon />,
  },
}

export const FilledLg: Story = {
  args: {
    variant: "filled",
    size: "lg",
  },
}

export const FilledLgFullWidth: Story = {
  args: {
    variant: "filled",
    size: "lg",
    fullWidth: true,
  },
}

export const UnderlinedSm: Story = {
  args: {
    variant: "underlined",
    size: "sm",
  },
}

export const UnderlinedMd: Story = {
  args: {
    variant: "underlined",
    size: "md",
  },
}

export const UnderlinedLg: Story = {
  args: {
    variant: "underlined",
    size: "lg",
  },
}

export const UnderlinedFullWidth: Story = {
  args: {
    variant: "underlined",
    size: "md",
    fullWidth: true,
  },
}

export const UnderlinedError: Story = {
  args: {
    variant: "underlined",
    size: "md",
    error: true,
  },
}

export const UnderlinedDisabled: Story = {
  args: {
    variant: "underlined",
    size: "md",
    disabled: true,
  },
}
