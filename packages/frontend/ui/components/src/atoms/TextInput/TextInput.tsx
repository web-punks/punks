import React from "react"
import styles from "./TextInput.module.css"
import { classNames, normalizePxSize, setCssVar } from "@punks/ui-core"
import { NumberDecrementIcon, NumberIncrementIcon } from "../../ui/icons"

export type TextInputClasses = {
  root?: string
  startAdornment?: string
  endAdornment?: string
  input?: string
  numericArrows?: string
}

export type TextInputSize = "sm" | "md" | "lg"
export type TextInputVariant = "filled" | "outlined" | "underlined"

export type TextInputSelectionRange = {
  start: number
  end: number
}

export interface TextInputProps
  extends Omit<React.InputHTMLAttributes<HTMLInputElement>, "size"> {
  inputRef?: React.Ref<HTMLInputElement>
  fullWidth?: boolean
  size?: TextInputSize
  variant?: TextInputVariant
  startAdornment?: React.ReactNode
  endAdornment?: React.ReactNode
  error?: boolean
  selectionRange?: TextInputSelectionRange
  autocompleteDisabled?: boolean
  classes?: TextInputClasses
  outline?: "none"
}

const TextInput = ({
  className,
  inputRef,
  fullWidth,
  size = "md",
  variant = "outlined",
  startAdornment,
  endAdornment,
  error,
  selectionRange,
  autocompleteDisabled,
  autoComplete,
  disabled,
  width,
  classes,
  outline,
  ...props
}: TextInputProps) => {
  const ref = React.useRef<HTMLDivElement>(null)
  const getInput = () => ref.current?.getElementsByTagName("input")[0]

  React.useEffect(() => {
    if (selectionRange) {
      getInput()?.setSelectionRange(selectionRange.start, selectionRange.end)
    }
  }, [selectionRange?.start, selectionRange?.end])

  const dispatchChangeEvent = () => {
    const event = new Event("change", {
      bubbles: true,
      cancelable: true,
    })
    getInput()?.dispatchEvent(event)
  }

  const handleIncrement = () => {
    getInput()?.stepUp()
    dispatchChangeEvent()
  }

  const handleDecrement = () => {
    getInput()?.stepDown()
    dispatchChangeEvent()
  }

  return (
    <div
      ref={ref}
      style={{
        ...setCssVar("--w", normalizePxSize(width)),
      }}
      className={classNames(
        styles.root,
        styles[variant],
        styles[`${variant}-${size}`],
        {
          [styles.outlineNone]: outline === "none",
          [styles.customWidth]: !!width,
          [styles[`${variant}-disabled`]]: disabled,
          [styles.fullWidth]: fullWidth,
          [styles.error]: error,
        },
        classes?.root,
        className
      )}
    >
      {startAdornment && (
        <span
          className={classNames(styles.startAdornment, classes?.startAdornment)}
        >
          {startAdornment}
        </span>
      )}
      <input
        ref={inputRef}
        className={classNames(
          styles.input,
          styles[`input-${variant}`],
          {
            [styles.inputDisabled]: disabled,
          },
          classes?.input
        )}
        disabled={disabled}
        autoComplete={autocompleteDisabled ? "none" : autoComplete}
        {...props}
      />
      {endAdornment && (
        <span
          className={classNames(styles.endAdornment, classes?.endAdornment)}
        >
          {endAdornment}
        </span>
      )}
      {props.type === "number" && (
        <span
          className={classNames(styles.numericArrows, classes?.numericArrows)}
        >
          <NumberIncrementIcon
            className={styles.arrowIncrement}
            width={18}
            height={18}
            onClick={handleIncrement}
          />
          <NumberDecrementIcon
            className={styles.arrowDecrement}
            width={18}
            height={18}
            onClick={handleDecrement}
          />
        </span>
      )}
    </div>
  )
}

export default TextInput

export const RefTextInput = React.forwardRef<HTMLInputElement, TextInputProps>(
  (props, ref) => <TextInput {...props} inputRef={ref ?? props.inputRef} />
)
