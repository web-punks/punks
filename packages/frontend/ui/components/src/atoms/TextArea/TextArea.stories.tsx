import type { Meta, StoryObj } from "@storybook/react"

import { TextArea } from "."

const meta = {
  title: "Ui/Atoms/TextArea",
  component: TextArea,
  args: {
    placeholder: "Placeholder",
  },
  tags: ["autodocs"],
} satisfies Meta<typeof TextArea>
export default meta

type Story = StoryObj<typeof meta>

export const OutlinedSm: Story = {
  args: {
    variant: "outlined",
    size: "sm",
  },
}

export const OutlinedNoOutline: Story = {
  args: {
    variant: "outlined",
    outline: "none",
  },
}

export const OutlinedSmFullWidth: Story = {
  args: {
    variant: "outlined",
    size: "sm",
    fullWidth: true,
  },
}

export const OutlinedMd: Story = {
  args: {
    variant: "outlined",
    size: "md",
  },
}

export const OutlinedMdNoResize: Story = {
  args: {
    variant: "outlined",
    size: "md",
    resizeDisabled: true,
    fullWidth: true,
  },
}

export const OutlinedMdDisabled: Story = {
  args: {
    variant: "outlined",
    size: "md",
    disabled: true,
  },
}

export const OutlinedMdFullWidth: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
  },
}

export const OutlinedMdError: Story = {
  args: {
    variant: "outlined",
    size: "md",
    error: true,
  },
}

export const OutlinedLg: Story = {
  args: {
    variant: "outlined",
    size: "lg",
  },
}

export const OutlinedLgFullWidth: Story = {
  args: {
    variant: "outlined",
    size: "lg",
    fullWidth: true,
  },
}

export const FilledSm: Story = {
  args: {
    variant: "filled",
    size: "sm",
  },
}

export const FilledSmFullWidth: Story = {
  args: {
    variant: "filled",
    size: "sm",
    fullWidth: true,
  },
}

export const FilledMd: Story = {
  args: {
    variant: "filled",
    size: "md",
  },
}

export const FilledMdFullWidth: Story = {
  args: {
    variant: "filled",
    size: "md",
    fullWidth: true,
  },
}

export const FilledLg: Story = {
  args: {
    variant: "filled",
    size: "lg",
  },
}

export const FilledLgFullWidth: Story = {
  args: {
    variant: "filled",
    size: "lg",
    fullWidth: true,
  },
}

export const UnderlinedSm: Story = {
  args: {
    variant: "underlined",
    size: "sm",
  },
}

export const UnderlinedMd: Story = {
  args: {
    variant: "underlined",
    size: "md",
  },
}

export const UnderlinedLg: Story = {
  args: {
    variant: "underlined",
    size: "lg",
  },
}

export const UnderlinedFullWidth: Story = {
  args: {
    variant: "underlined",
    size: "md",
    fullWidth: true,
  },
}

export const UnderlinedError: Story = {
  args: {
    variant: "underlined",
    size: "md",
    error: true,
  },
}

export const UnderlinedDisabled: Story = {
  args: {
    variant: "underlined",
    size: "md",
    disabled: true,
  },
}
