import React from "react"
import styles from "./TextArea.module.css"
import { classNames } from "@punks/ui-core"

export type TextAreaSize = "sm" | "md" | "lg"
export type TextAreaVariant = "filled" | "outlined" | "underlined"

export interface TextAreaProps
  extends Omit<React.TextareaHTMLAttributes<HTMLTextAreaElement>, "size"> {
  inputRef?: React.Ref<HTMLTextAreaElement>
  fullWidth?: boolean
  size?: TextAreaSize
  variant?: TextAreaVariant
  error?: boolean
  autocompleteDisabled?: boolean
  resizeDisabled?: boolean
  outline?: "none"
}

const TextArea = ({
  className,
  inputRef,
  fullWidth,
  size = "md",
  variant = "outlined",
  error,
  autocompleteDisabled,
  autoComplete,
  disabled,
  resizeDisabled,
  rows = 3,
  outline,
  ...props
}: TextAreaProps) => {
  return (
    <textarea
      ref={inputRef}
      className={classNames(
        styles.root,
        styles[variant],
        styles[`${variant}-${size}`],
        styles.input,
        styles[`input-${variant}`],
        {
          [styles.outlineNone]: outline === "none",
          [styles.resizeDisabled]: resizeDisabled || disabled,
          [styles.inputDisabled]: disabled,
          [styles[`${variant}-disabled`]]: disabled,
          [styles.fullWidth]: fullWidth,
          [styles.error]: error,
        },
        className
      )}
      disabled={disabled}
      autoComplete={autocompleteDisabled ? "none" : autoComplete}
      rows={rows}
      {...props}
    />
  )
}

export default TextArea

export const RefTextAreaInput = React.forwardRef<
  HTMLTextAreaElement,
  TextAreaProps
>((props, ref) => <TextArea {...props} inputRef={ref ?? props.inputRef} />)
