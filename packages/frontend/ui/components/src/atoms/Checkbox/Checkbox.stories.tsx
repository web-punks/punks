import type { Meta, StoryObj } from "@storybook/react"

import { Checkbox } from "."

const meta = {
  title: "Ui/Atoms/Checkbox",
  component: Checkbox,
  argTypes: {},
} satisfies Meta<typeof Checkbox>
export default meta

type Story = StoryObj<typeof meta>

export const CheckboxSm: Story = {
  args: {
    size: "small",
    onChange: (e) => console.log(e),
  },
}

export const CheckboxMd: Story = {
  args: {
    size: "medium",
    onChange: (e) => console.log(e),
  },
}

export const CheckboxMdLabeled: Story = {
  args: {
    size: "medium",
    onChange: (e) => console.log(e),
    children: "Label",
  },
}

export const CheckboxLarge: Story = {
  args: {
    size: "large",
    onChange: (e) => console.log(e),
  },
}

export const CheckboxControlled: Story = {
  args: {
    onValueChange: (e) => console.log(e),
  },
}

export const CheckboxDisabled: Story = {
  args: {
    size: "medium",
    onChange: (e) => console.log(e),
    disabled: true,
  },
}

export const CheckboxDisabledLabelled: Story = {
  args: {
    size: "medium",
    onChange: (e) => console.log(e),
    disabled: true,
    children: "Label",
  },
}
