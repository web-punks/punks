import React, { useState } from "react"
import styles from "./Checkbox.module.css"
import { ThemeColor, classNames, setThemeColorVar } from "@punks/ui-core"

export type CheckboxSize = "small" | "medium" | "large"

export type CheckboxClasses = {
  root?: string
  input?: string
  label?: string
}

export interface CheckboxProps
  extends Omit<React.InputHTMLAttributes<HTMLInputElement>, "size" | "type"> {
  inputRef?: React.Ref<HTMLInputElement>
  size?: CheckboxSize
  onValueChange?: (value: boolean) => void
  children?: React.ReactNode
  color?: ThemeColor
  classes?: CheckboxClasses
}

const Checkbox = ({
  inputRef,
  size = "medium",
  onChange,
  onValueChange,
  defaultChecked,
  checked,
  children,
  color = "primary",
  classes,
  className,
  disabled,
  ...other
}: CheckboxProps) => {
  const [uncontrolledValue, setUncontrolledValue] = useState(
    defaultChecked ?? false
  )
  const toggle = () => {
    setUncontrolledValue(!uncontrolledValue)
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (onChange) {
      onChange(e)
    }
    if (onValueChange) {
      onValueChange(!uncontrolledValue)
    }
  }

  const checkedValue = checked ?? uncontrolledValue

  return (
    <label
      style={{
        ...setThemeColorVar("--color", color),
      }}
      className={classNames(
        styles.root,
        styles[size],
        {
          [styles.disabled]: disabled,
        },
        className,
        classes?.root
      )}
    >
      <input
        ref={inputRef}
        type="checkbox"
        checked={checkedValue}
        onChange={(e) => handleChange(e)}
        onClick={() => !disabled && toggle()}
        className={classNames(styles.input, classes?.input)}
        disabled={disabled}
        {...other}
      />
      {children && (
        <span className={classNames(styles.label, classes?.label)}>
          {children}
        </span>
      )}
    </label>
  )
}

export const RefCheckbox = React.forwardRef<HTMLInputElement, CheckboxProps>(
  (props, ref) => <Checkbox {...props} inputRef={ref ?? props.inputRef} />
)
