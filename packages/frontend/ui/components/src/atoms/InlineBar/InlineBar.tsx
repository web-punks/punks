import { Box } from "../../structure"

export interface InlineBarProps {
  children: any
  className?: string
  gap?: number
  inline?: boolean
}

const InlineBar = ({
  children,
  className,
  inline,
  gap = 2,
}: InlineBarProps) => {
  return (
    <Box
      className={className}
      inlineFlex={inline}
      flex={!inline}
      gap={gap}
      alignItems="center"
      justifyContent="between"
    >
      {children}
    </Box>
  )
}

export default InlineBar
