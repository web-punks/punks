import type { Meta, StoryObj } from "@storybook/react"

import Button from "./Button"
import { ContextMenuIcon } from "../../ui/icons"

const meta = {
  title: "Ui/Atoms/Button",
  component: Button,
  tags: ["autodocs"],
  argTypes: {
    // backgroundColor: { control: "color" },
  },
} satisfies Meta<typeof Button>
export default meta

type Story = StoryObj<typeof meta>

export const FilledPrimarySm: Story = {
  args: {
    color: "primary",
    variant: "filled",
    children: "Button",
    size: "sm",
  },
}

export const FilledPrimarySmRounded: Story = {
  args: {
    color: "primary",
    variant: "filled",
    children: "Button",
    size: "sm",
    rounded: true,
  },
}

export const FilledPrimarySmLoading: Story = {
  args: {
    color: "primary",
    variant: "filled",
    children: "Button",
    size: "sm",
    rounded: true,
    loading: true,
  },
}

export const FilledPrimaryMd: Story = {
  args: {
    color: "primary",
    variant: "filled",
    children: "Button",
  },
}

export const FilledPrimaryMdRounded: Story = {
  args: {
    color: "primary",
    variant: "filled",
    children: "Button",
    rounded: true,
  },
}

export const FilledPrimaryMdDisabled: Story = {
  args: {
    color: "primary",
    variant: "filled",
    children: "Button",
    disabled: true,
  },
}

export const FilledPrimaryMdRoundedLoading: Story = {
  args: {
    color: "primary",
    variant: "filled",
    children: "Button",
    rounded: true,
    loading: true,
  },
}

export const FilledTransparentMd: Story = {
  args: {
    color: "transparent",
    variant: "filled",
    children: "Button",
  },
}

export const FilledLightMd: Story = {
  args: {
    color: "light",
    variant: "filled",
    children: "Button",
  },
}

export const FilledPrimaryLg: Story = {
  args: {
    color: "primary",
    variant: "filled",
    children: "Button",
    size: "lg",
  },
}

export const FilledPrimaryLgRounded: Story = {
  args: {
    color: "primary",
    variant: "filled",
    children: "Button",
    size: "lg",
    rounded: true,
  },
}

export const FilledPrimaryLgRoundedLoading: Story = {
  args: {
    color: "primary",
    variant: "filled",
    children: "Button",
    size: "lg",
    rounded: true,
    loading: true,
  },
}

export const FilledErrorMd: Story = {
  args: {
    color: "error",
    variant: "filled",
    children: "Button",
  },
}

export const OutlinedPrimaryMd: Story = {
  args: {
    color: "primary",
    variant: "outlined",
    children: "Button",
  },
}

export const OutlinedLightMd: Story = {
  args: {
    color: "light",
    variant: "outlined",
    children: "Button",
  },
}

export const OutlinedLightMdNoHover: Story = {
  args: {
    color: "light",
    variant: "outlined",
    children: "Button",
    disableHoverStyles: true,
  },
}

export const OutlinedPrimaryMdRounded: Story = {
  args: {
    color: "primary",
    variant: "outlined",
    children: "Button",
    rounded: true,
  },
}
export const TextPrimaryMd: Story = {
  args: {
    color: "primary",
    variant: "text",
    children: "Button",
  },
}

export const TextPrimaryMdIconOnly: Story = {
  args: {
    color: "primary",
    variant: "text",
    children: <ContextMenuIcon width={20} />,
    iconOnly: true,
  },
}

export const TextPrimaryMdIconOnlyLoading: Story = {
  args: {
    color: "primary",
    variant: "text",
    children: <ContextMenuIcon width={20} />,
    iconOnly: true,
    loading: true,
  },
}

export const TextPrimaryMdIconOnlyRounded: Story = {
  args: {
    color: "primary",
    variant: "text",
    children: <ContextMenuIcon width={20} />,
    iconOnly: true,
    rounded: true,
  },
}

export const TextPrimaryMdRounded: Story = {
  args: {
    color: "primary",
    variant: "text",
    children: "Button",
    rounded: true,
  },
}

export const TextPrimaryMdDisabled: Story = {
  args: {
    color: "primary",
    variant: "text",
    children: "Button",
    disabled: true,
  },
}
