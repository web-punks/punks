import React from "react"
import styles from "./Button.module.css"
import {
  ThemeColor,
  classNames,
  normalizePxSize,
  setColorVar,
  setColorVarContrast,
  setCssVar,
} from "@punks/ui-core"
import { Spinner } from "../../animations/Spinner"

export type ButtonSize = "sm" | "md" | "lg"
export type ButtonVariant = "filled" | "outlined" | "text"

export interface ButtonProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  children: React.ReactNode
  color?: ThemeColor
  size?: ButtonSize
  variant?: ButtonVariant
  rounded?: boolean
  loading?: boolean
  fullWidth?: boolean
  iconOnly?: boolean
  width?: string | number
  preventDefault?: boolean
  stopPropagation?: boolean
  disableHoverStyles?: boolean
}

const Button = React.forwardRef<HTMLButtonElement, ButtonProps>(
  (
    {
      children,
      className,
      variant = "text",
      size = "md",
      color = "light",
      rounded = false,
      loading,
      fullWidth,
      iconOnly,
      style,
      width,
      preventDefault,
      stopPropagation,
      disableHoverStyles = false,
      onClick,
      ...other
    },
    ref
  ) => {
    const handleClick = (
      e: React.MouseEvent<HTMLButtonElement, MouseEvent>
    ) => {
      if (preventDefault) {
        e.preventDefault()
      }
      if (stopPropagation) {
        e.stopPropagation()
      }
      onClick?.(e)
    }

    return (
      <button
        onClick={handleClick}
        style={{
          ...style,
          ...(width ? setCssVar("--w", normalizePxSize(width)) : {}),
          ...(color ? setColorVar("--c", color) : {}),
          ...(color ? setColorVarContrast("--cc", color) : {}),
        }}
        className={classNames(
          styles.root,
          {
            [styles.rootHover]: !disableHoverStyles,
          },
          styles[variant],
          styles[`${variant}-${color}`],
          styles[`size-${size}`],
          {
            [styles[`rounded-${size}`]]: rounded ?? false,
            [styles.fullWidth]: fullWidth,
            [styles.fixedWith]: !!width,
            [styles[`iconOnly-${size}`]]: iconOnly,
            [styles.disabled]: other.disabled,
          },
          className
        )}
        ref={ref}
        {...other}
      >
        {loading && (
          <Spinner
            className={classNames(
              {
                [styles.spinner]: !iconOnly,
              },
              styles[`spinner-${size}`]
            )}
          />
        )}
        {iconOnly && loading ? null : children}
      </button>
    )
  }
)

export default Button
