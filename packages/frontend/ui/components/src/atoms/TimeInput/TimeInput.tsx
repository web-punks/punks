import React, { useState } from "react"
import styles from "./TimeInput.module.css"
import { classNames } from "@punks/ui-core"

export type TimeInputSize = "sm" | "md" | "lg"
export type TimeInputVariant = "filled" | "outlined"

export interface TimeInputProps
  extends Omit<React.InputHTMLAttributes<HTMLInputElement>, "size"> {
  inputRef?: React.Ref<HTMLInputElement>
  fullWidth?: boolean
  size?: TimeInputSize
  variant?: TimeInputVariant
  startAdornment?: React.ReactNode
  endAdornment?: React.ReactNode
  controlled?: boolean
  error?: boolean
}

const TimeInput = ({
  className,
  inputRef,
  fullWidth,
  size = "md",
  variant = "outlined",
  startAdornment,
  endAdornment,
  error,
  disabled,
  controlled,
  ...props
}: TimeInputProps) => {
  const [isControlled] = useState<boolean>(
    controlled || props.value !== undefined
  )
  const [uncontrolledValue, setUncontrolledValue] = React.useState<string>("")

  const handleTimeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUncontrolledValue(event.target.value)
    if (props.onChange) {
      props.onChange(event)
    }
  }

  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    setUncontrolledValue(event.target.value)
    if (props.onChange) {
      props.onChange(event)
    }
  }

  const handleInput = (event: React.FormEvent<HTMLInputElement>) => {
    setUncontrolledValue(event.currentTarget.value)
  }

  return (
    <div
      className={classNames(
        styles.root,
        styles[variant],
        styles[`${variant}-${size}`],
        {
          [styles[`${variant}-disabled`]]: disabled,
          [styles.fullWidth]: fullWidth,
          [styles.error]: error,
        },
        className
      )}
    >
      {startAdornment && (
        <span className={styles.startAdornment}>{startAdornment}</span>
      )}
      <input
        ref={inputRef}
        type="time"
        className={classNames(
          styles.input,
          styles[`input-${variant}`],
          {
            [styles.inputDisabled]: disabled,
          },
          className
        )}
        disabled={disabled}
        onChange={handleTimeChange}
        onBlur={handleBlur}
        onInput={handleInput}
        value={isControlled ? props.value : uncontrolledValue}
        {...props}
      />
      {endAdornment && (
        <span className={styles.endAdornment}>{endAdornment}</span>
      )}
    </div>
  )
}

export default TimeInput

export const RefTimeInput = React.forwardRef<HTMLInputElement, TimeInputProps>(
  (props, ref) => <TimeInput {...props} inputRef={ref ?? props.inputRef} />
)
