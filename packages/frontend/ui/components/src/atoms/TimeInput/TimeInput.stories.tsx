import type { Meta, StoryObj } from "@storybook/react"

import TimeInput from "./TimeInput"

const meta: Meta<typeof TimeInput> = {
  title: "Ui/Atoms/TimeInput",
  component: TimeInput,
  argTypes: {
    onChange: { action: "onChange" },
    inputRef: { table: { disable: true } },
  },
  tags: ["autodocs"],
} satisfies Meta<typeof TimeInput>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    placeholder: "Placeholder",
  },
}
