import React, { useState, useRef, SelectHTMLAttributes, useEffect } from "react"
import styles from "./Select.module.css"
import { ChevronDown, CloseIcon } from "../../ui/icons"
import { classNames, normalizePxSize, setCssVar } from "@punks/ui-core"
import { Spinner } from "../../animations"

export type SelectSize = "sm" | "md" | "lg"
export type SelectVariant = "filled" | "outlined" | "underlined"

export type SelectOption = {
  value: string
  label: string
  disabled?: boolean
  icon?: React.ReactNode
  key?: string
}

export type SelectClasses = {
  root?: string
  input?: string
  placeholder?: string
  filter?: string
  options?: string
  option?: string
  emptyOptionsContent?: string
}

export interface SelectProps
  extends Omit<SelectHTMLAttributes<HTMLInputElement>, "size"> {
  options: SelectOption[]
  value?: string
  onValueChange?: (value: string) => void
  className?: string
  placeholder?: string
  size?: SelectSize
  variant?: SelectVariant
  rounded?: boolean
  fullWidth?: boolean
  inputRef?: React.Ref<HTMLInputElement>
  classes?: SelectClasses
  maxOptionsHeight?: string | number
  filterable?: boolean
  filter?: string
  error?: boolean
  onFilterChange?: (filter: string) => void
  filterMatchFn?: (option: SelectOption, filter: string) => boolean
  controlled?: boolean
  emptyOptionsContent?: React.ReactNode
  emptyOptionsLoading?: React.ReactNode
  emptyFilterContent?: React.ReactNode
  readOnly?: boolean
  outline?: "none"
  hideCurrentOption?: boolean
  loading?: boolean
  allowClear?: boolean
}

const Select: React.FC<SelectProps> = ({
  options,
  onValueChange,
  value,
  placeholder,
  className,
  fullWidth,
  rounded,
  size = "md",
  variant = "outlined",
  inputRef,
  defaultValue,
  onChange,
  classes,
  maxOptionsHeight = 400,
  filterable,
  filter,
  error,
  disabled,
  onFilterChange,
  controlled,
  emptyOptionsContent = "No options available",
  emptyOptionsLoading = "Loading options...",
  emptyFilterContent = "No options match your search",
  readOnly,
  outline,
  loading,
  filterMatchFn = (option: SelectOption, filter: string) =>
    option?.label?.toLowerCase().includes(filter.toLowerCase()),
  hideCurrentOption,
  allowClear,
  ...other
}) => {
  const [isControlled, setIsControlled] = useState(
    controlled || value !== undefined
  )
  const [uncontrolledValue, setUncontrolledValue] = useState<string | number>(
    (defaultValue as string | number) ?? ""
  )
  const [isOpen, setIsOpen] = useState(false)
  const selectRef = useRef<HTMLDivElement | null>(null)

  const [uncontrolledFilter, setUncontrolledFilter] = useState<string>()

  useEffect(() => {
    if (isControlled) {
      return
    }
    const value = selectRef.current?.getElementsByTagName("input")[0].value
    if (value) {
      setUncontrolledValue(value)
    }
  }, [inputRef])

  useEffect(() => {
    if (value && !isControlled) {
      setIsControlled(true)
    }
  }, [value])

  useEffect(() => {
    if (filterable && !filter && value) {
      const selectedOption = options.find((o) => o.value === value)
      if (selectedOption) {
        setUncontrolledFilter(selectedOption.label)
      }
    }
  }, [value, filterable, options])

  // useEffect(() => {
  //   if (isControlled && value) {
  //     const selectedOption = options.find((o) => o.value === value)
  //   }
  // }, [value, options, isControlled])

  const toggleDropdown = () => {
    if (disabled || readOnly) {
      return
    }

    setIsOpen(!isOpen)
  }

  const handleOptionClick = (option: SelectOption) => {
    const selectedValue = option.value
    onValueChange?.(selectedValue)
    setIsOpen(false)

    if (!isControlled) {
      setUncontrolledValue(selectedValue)
    }

    onChange?.({
      type: "text",
      target: {
        name: other.name,
        value: selectedValue,
      },
    } as any)

    if (filterable) {
      setUncontrolledFilter(option.label)
      onFilterChange?.(option.label)
    }
  }

  const handleOptionClear = () => {
    onValueChange?.("")
    if (!isControlled) {
      setUncontrolledValue("")
    }
    if (filterable) {
      onFilterChange?.("")
      setUncontrolledFilter("")
    }
  }

  const handleOutsideClick = (event: MouseEvent) => {
    if (
      selectRef.current &&
      !selectRef.current.contains(event.target as Node)
    ) {
      setIsOpen(false)
    }
  }

  React.useEffect(() => {
    window.addEventListener("click", handleOutsideClick)
    return () => {
      window.removeEventListener("click", handleOutsideClick)
    }
  }, [])

  const getLabel = (value?: string | number) =>
    options?.find((o) => o.value === value)?.label

  const getIcon = (value?: string | number) =>
    options?.find((o) => o.value === value)?.icon

  const actualValue = isControlled ? value : uncontrolledValue

  const label = getLabel(actualValue)
  const icon = getIcon(actualValue)

  const isMatchingFilter = (option: SelectOption) => {
    const currentFilter = filter ?? uncontrolledFilter
    if (currentFilter) {
      return filterMatchFn(option, currentFilter)
    }
    return true
  }

  const isVisibleOption = (
    option: SelectOption,
    availableOptions: SelectOption[]
  ) => {
    if (!hideCurrentOption || availableOptions.length === 1) {
      return true
    }

    return option.value !== actualValue
  }

  const filteredOptions = options.filter(isMatchingFilter)

  return (
    <div
      className={classNames(
        styles.root,
        styles[variant],
        styles[`${variant}-${size}`],
        {
          [styles.outlineNone]: outline === "none",
          [styles.clickable]: !readOnly && !disabled,
          [styles.fullWidth]: fullWidth,
          [styles.error]: error,
          [styles.disabled]: disabled,
          [styles[`${variant}-rounded-${size}`]]: rounded,
          [styles.opened]: isOpen,
        },
        classes?.root,
        className
      )}
      ref={selectRef}
    >
      <input
        type="text"
        value={actualValue}
        ref={inputRef}
        hidden
        readOnly
        onChange={!readOnly ? onChange : undefined}
        {...other}
      />
      <div
        className={classNames(
          styles.input,
          styles[`input-${size}`],
          {
            [styles.clickable]: !readOnly && !disabled,
          },
          classes?.input
        )}
        onClick={!readOnly ? toggleDropdown : undefined}
      >
        {loading && <Spinner className={classNames(styles.loader)} />}

        {!readOnly && filterable ? (
          <input
            className={classNames(styles.filter, classes?.filter)}
            value={filter ?? uncontrolledFilter}
            placeholder={placeholder}
            onChange={(e) => {
              onFilterChange?.(e.target.value)
              setUncontrolledFilter(e.target.value)
              if (e.target.value) {
                setIsOpen(true)
              }
            }}
          />
        ) : (
          <>
            {icon && <div className={styles.selectedicon}>{icon}</div>}
            {label ? (
              <div>{label}</div>
            ) : (
              <span
                className={classNames(styles.placeholder, classes?.placeholder)}
              >
                {placeholder}
              </span>
            )}
          </>
        )}
      </div>

      {allowClear && actualValue && isOpen && (
        <CloseIcon
          className={classNames(
            styles.clearIcon,
            styles[`clearIcon-${variant}`]
          )}
          onClick={(e) => {
            e.stopPropagation()
            handleOptionClear()
          }}
        />
      )}
      {!readOnly && (
        <ChevronDown
          className={classNames(
            styles.arrowIcon,
            styles[`arrowIcon-${variant}`],
            {
              [styles.rotateIcon]: isOpen,
            }
          )}
          onClick={toggleDropdown}
        />
      )}
      {isOpen && (
        <div
          style={{
            ...setCssVar("--max-h", normalizePxSize(maxOptionsHeight)),
          }}
          className={classNames(
            styles.options,
            styles[`options-${variant}`],
            {
              [styles[`options-${size}-rounded-${size}`]]: rounded,
              [styles.optionsMaxH]: !!maxOptionsHeight,
            },
            classes?.options
          )}
        >
          {options.length === 0 && (
            <div
              className={classNames(
                styles.emptyOptionsContent,
                classes?.emptyOptionsContent
              )}
            >
              {loading ? emptyOptionsLoading : emptyOptionsContent}
            </div>
          )}
          {options.length > 0 && filteredOptions.length === 0 && (
            <div
              className={classNames(
                styles.emptyOptionsContent,
                classes?.emptyOptionsContent
              )}
            >
              {emptyFilterContent}
            </div>
          )}
          {filteredOptions
            .filter((x) => isVisibleOption(x, filteredOptions))
            .map((option) => (
              <div
                key={option?.key ?? option.value}
                className={classNames(
                  styles.option,
                  {
                    [styles.optionDisabled]: option.disabled,
                    [styles.optionSelected]: option.value === value,
                  },
                  classes?.option
                )}
                onClick={() => !option.disabled && handleOptionClick(option)}
              >
                {option.icon}
                <div>{option?.label}</div>
                {/* {allowClear && actualValue === option.value && (
                  <CloseIcon
                    className={classNames(
                      styles.clearIcon,
                      styles[`clearIcon-${variant}`]
                    )}
                    onClick={(e) => {
                      e.stopPropagation()
                      handleOptionClear()
                    }}
                  />
                )} */}
              </div>
            ))}
        </div>
      )}
    </div>
  )
}

const RefSelect = React.forwardRef<HTMLInputElement, SelectProps>(
  (props, ref) => <Select {...props} inputRef={ref ?? props.inputRef} />
)

export default RefSelect
