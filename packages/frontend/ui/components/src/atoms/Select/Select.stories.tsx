import type { Meta, StoryObj } from "@storybook/react"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import { useForm } from "react-hook-form"

import { Select } from "."
import { useRef, useState } from "react"
import { setCssVar } from "@punks/ui-core"
import { Box } from "../../structure"
import { Button } from "../Button"
import { TextIcon } from "../../ui"

const meta = {
  title: "Ui/Atoms/Select",
  component: Select,
  tags: ["autodocs"],
  args: {
    placeholder: "Select item",
    options: [
      {
        label: "Item1",
        value: "item1",
        icon: <TextIcon />,
      },
      {
        label: "Item2",
        value: "item2",
      },
      {
        label: "Item3",
        value: "item3",
      },
      {
        label: "Item4",
        value: "item4",
      },
    ],
  },
} satisfies Meta<typeof Select>
export default meta

type Story = StoryObj<typeof meta>

export const OutlinedMediumWithAuto: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: false,
  },
}

export const OutlinedMedium: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
  },
}

export const OutlinedMediumClearable: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    allowClear: true,
  },
}

export const OutlinedLoading: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    loading: true,
    options: [],
  },
}

export const OutlinedHiddenCurrentOption: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    hideCurrentOption: true,
  },
}

export const OutlinedHiddenCurrentOptionSingle: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    hideCurrentOption: true,
    options: [
      {
        label: "Item1",
        value: "item1",
      },
    ],
  },
}

export const OutlinedNoOutline: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    outline: "none",
  },
}

export const OutlinedMediumWithIcons: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    options: [
      {
        label: "Item1",
        value: "item1",
        icon: <TextIcon width={16} />,
      },
      {
        label: "Item2",
        value: "item2",
        icon: <TextIcon width={16} />,
      },
      {
        label: "Item3",
        value: "item3",
        icon: <TextIcon width={16} />,
      },
      {
        label: "Item4",
        value: "item4",
        icon: <TextIcon width={16} />,
      },
    ],
  },
}

export const OutlinedError: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    error: true,
  },
}

export const OutlinedDisabledEmpty: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    disabled: true,
  },
}

export const OutlinedDisabledFilled: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    disabled: true,
    value: "item2",
  },
}

export const OutlinedReadonly: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    readOnly: true,
    value: "item2",
  },
}

export const OutlinedMediumDisabled: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    disabled: true,
  },
}

export const OutlinedMediumError: Story = {
  args: {
    variant: "outlined",
    size: "md",
    error: true,
  },
}

export const OutlinedMediumFilterable: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    filterable: true,
  },
}

export const OutlinedMediumFilterablePreselected: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    filterable: true,
    value: "item2",
    allowClear: true,
  },
}

export const OutlinedMediumMaxHeight: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    maxOptionsHeight: 80,
  },
}

export const OutlinedMediumWithDisabledOptions: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    options: [
      {
        label: "Item1",
        value: "item1",
      },
      {
        label: "Item2",
        value: "item2",
        disabled: true,
      },
      {
        label: "Item3",
        value: "item3",
      },
      {
        label: "Item4",
        value: "item4",
        disabled: true,
      },
    ],
  },
}

export const OutlinedRoundedMedium: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
  },
  decorators: [
    (Story) => (
      <div
        style={{ width: "300px", ...setCssVar("--wp-theme-rounding", "4px") }}
      >
        <Story />
      </div>
    ),
  ],
}

export const OutlinedUncontrolled: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
  },
  decorators: [
    (Story, props) => {
      const ref = useRef<HTMLInputElement>(null)
      return (
        <div style={{ width: "300px" }}>
          <Story
            args={{
              ...props.args,
              inputRef: ref,
              onChange: (event) => {
                console.log("on change", event)
              },
            }}
          />
        </div>
      )
    },
  ],
}

export const OutlinedUncontrolledWithDefaultValue: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    defaultValue: "item2",
  },
  decorators: [
    (Story, props) => {
      const ref = useRef<HTMLInputElement>(null)
      return (
        <div style={{ width: "300px" }}>
          <Story
            args={{
              ...props.args,
              inputRef: ref,
              onChange: (event) => {
                console.log("on change", event)
              },
            }}
          />
        </div>
      )
    },
  ],
}

export const OutlinedUncontrolledWithReactHookForm: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
  },
  decorators: [
    (Story, props) => {
      const schema = yup.object().shape({
        value: yup.string().required(),
      })
      const {
        watch,
        setValue,
        register,
        handleSubmit,
        formState: { errors },
      } = useForm({
        resolver: yupResolver(schema),
        defaultValues: {
          value: "item2",
        },
      })

      const value = watch("value")
      console.log("value", value)
      return (
        <div style={{ width: "300px" }}>
          <Story
            args={{
              ...props.args,
              ...register("value"),
            }}
          />
          <Box mt={2}>
            <Button
              variant="filled"
              fullWidth
              onClick={() => setValue("value", "item2")}
            >
              reset
            </Button>
          </Box>
          <Box mt={2}>
            <Button
              variant="filled"
              color="primary"
              fullWidth
              onClick={handleSubmit((data) => console.log("submit data", data))}
            >
              submit
            </Button>
          </Box>
        </div>
      )
    },
  ],
}

export const OutlinedEmpty: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    options: [],
    emptyOptionsContent: "No options",
  },
}

export const UnderlinedSm: Story = {
  args: {
    variant: "underlined",
    size: "sm",
  },
}

export const UnderlinedMd: Story = {
  args: {
    variant: "underlined",
    size: "md",
  },
}

export const UnderlinedLg: Story = {
  args: {
    variant: "underlined",
    size: "lg",
  },
}

export const UnderlinedFullWidth: Story = {
  args: {
    variant: "underlined",
    size: "md",
    fullWidth: true,
  },
}

export const UnderlinedError: Story = {
  args: {
    variant: "underlined",
    size: "md",
    error: true,
  },
}

export const UnderlinedDisabled: Story = {
  args: {
    variant: "underlined",
    size: "md",
    disabled: true,
  },
}

export const OutlinedControlled: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
  },
  decorators: [
    (Story, props) => {
      const [value, setValue] = useState<string>()
      return (
        <div style={{ width: "300px" }}>
          <div>
            <button
              onClick={() => setValue("item2")}
              style={{ marginBottom: 30 }}
            >
              set value
            </button>
          </div>
          <div>
            <Story
              args={{
                ...props.args,
                value,
                onChange: (event) => {
                  setValue(event.target.value)
                },
              }}
            />
          </div>
        </div>
      )
    },
  ],
}
