import { classNames } from "@punks/ui-core"
import styles from "./SortingControl.module.css"
import { ChevronDownIcon, ChevronUpIcon } from "../../ui"

export type SortingValue = "asc" | "desc"

export interface SortingControlProps {
  className?: string
  value?: SortingValue
  onChange?: (value: SortingValue) => void
  size?: number
}

const SortingControl = ({
  className,
  onChange,
  value,
  size = 12,
}: SortingControlProps) => {
  return (
    <div className={classNames(styles.root, className)}>
      <ChevronUpIcon
        className={classNames(styles.control, {
          [styles.active]: value === "desc",
        })}
        onClick={() => onChange?.("desc")}
        width={size}
        height={size}
      />
      <ChevronDownIcon
        className={classNames(styles.control, {
          [styles.active]: value === "asc",
        })}
        onClick={() => onChange?.("asc")}
        width={size}
        height={size}
      />
    </div>
  )
}

export default SortingControl
