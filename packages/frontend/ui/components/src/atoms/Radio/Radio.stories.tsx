import type { Meta, StoryObj } from "@storybook/react"

import { Radio } from "."

const meta = {
  title: "Ui/Atoms/Radio",
  component: Radio,
  argTypes: {},
} satisfies Meta<typeof Radio>
export default meta

type Story = StoryObj<typeof meta>

export const RadioSm: Story = {
  args: {
    size: "small",
    onChange: (e) => console.log(e),
  },
}

export const RadioMd: Story = {
  args: {
    size: "medium",
    onChange: (e) => console.log(e),
  },
}

export const RadioMdLabeled: Story = {
  args: {
    size: "medium",
    onChange: (e) => console.log(e),
    children: "Label",
  },
}

export const RadioLarge: Story = {
  args: {
    size: "large",
    onChange: (e) => console.log(e),
  },
}

export const RadioControlled: Story = {
  args: {
    onValueChange: (e) => console.log(e),
  },
}

export const RadioPrimary: Story = {
  args: {
    size: "medium",
    onChange: (e) => console.log(e),
    color: "primary",
  },
}

export const RadioPrimaryLabelled: Story = {
  args: {
    size: "medium",
    onChange: (e) => console.log(e),
    color: "primary",
    children: "Label",
  },
}

export const RadioDisabled: Story = {
  args: {
    disabled: true,
  },
}
