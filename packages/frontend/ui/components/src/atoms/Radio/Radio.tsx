import React, { useState } from "react"
import styles from "./Radio.module.css"
import { ThemeColor, classNames, setThemeColorVar } from "@punks/ui-core"

export type RadioSize = "small" | "medium" | "large"

export type RadioClasses = {
  root?: string
  input?: string
  label?: string
}

export interface RadioProps
  extends Omit<React.InputHTMLAttributes<HTMLInputElement>, "size" | "type"> {
  inputRef?: React.Ref<HTMLInputElement>
  size?: RadioSize
  onValueChange?: (value: boolean) => void
  children?: React.ReactNode
  classes?: RadioClasses
  color?: ThemeColor
}

const Radio = ({
  inputRef,
  size = "medium",
  onChange,
  onValueChange,
  defaultChecked,
  checked,
  children,
  classes,
  className,
  style,
  color = "grey-80",
  disabled,
  ...other
}: RadioProps) => {
  const [uncontrolledValue, setUncontrolledValue] = useState(
    defaultChecked ?? false
  )
  const toggle = () => {
    setUncontrolledValue(!uncontrolledValue)
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (onChange) {
      onChange(e)
    }
    if (onValueChange) {
      onValueChange(!uncontrolledValue)
    }
  }

  const checkedValue = checked ?? uncontrolledValue

  return (
    <label
      className={classNames(
        styles.root,
        {
          [styles.small]: size === "small",
          [styles.medium]: size === "medium",
          [styles.large]: size === "large",
          [styles.disabled]: disabled,
        },
        className,
        classes?.root
      )}
    >
      <input
        style={{
          ...style,
          ...setThemeColorVar("--c", color),
        }}
        disabled={disabled}
        ref={inputRef}
        type="radio"
        checked={checkedValue}
        onChange={(e) => handleChange(e)}
        onClick={() => toggle()}
        className={classNames(styles.input, classes?.input)}
        {...other}
      />
      {children && (
        <span className={classNames(styles.label, classes?.label)}>
          {children}
        </span>
      )}
    </label>
  )
}

export const RefRadio = React.forwardRef<HTMLInputElement, RadioProps>(
  (props, ref) => <Radio {...props} inputRef={ref ?? props.inputRef} />
)
