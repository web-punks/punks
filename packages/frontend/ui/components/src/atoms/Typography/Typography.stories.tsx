import type { Meta, StoryObj } from "@storybook/react"

import { Typography } from "."

const meta = {
  title: "Ui/Atoms/Typography",
  component: Typography,
  args: {
    children: "Lorem ipsum dolor sit amet",
  },
  tags: ["autodocs"],
} satisfies Meta<typeof Typography>
export default meta

type Story = StoryObj<typeof meta>

export const H1: Story = {
  args: {
    variant: "h1",
  },
}

export const H2: Story = {
  args: {
    variant: "h2",
  },
}

export const H3: Story = {
  args: {
    variant: "h3",
  },
}

export const H4: Story = {
  args: {
    variant: "h4",
  },
}

export const H5: Story = {
  args: {
    variant: "h5",
  },
}

export const H6: Story = {
  args: {
    variant: "h6",
  },
}

export const Body1: Story = {
  args: {
    variant: "body1",
  },
}

export const Body1Margin: Story = {
  args: {
    variant: "body1",
    my: 2,
  },
}

export const Body2: Story = {
  args: {
    variant: "body2",
  },
}

export const Caption1: Story = {
  args: {
    variant: "caption1",
  },
}

export const Caption2: Story = {
  args: {
    variant: "caption2",
  },
}
