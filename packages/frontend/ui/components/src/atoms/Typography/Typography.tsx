import React from "react"
import styles from "./Typography.module.css"
import {
  getPaddingClasses,
  getMarginClasses,
  getMarginStyles,
  getPaddingStyles,
} from "../../helpers/spacings"
import {
  FontWeight,
  TextAlign,
  TextVariant,
  ThemeColor,
  ThemeColorVariant,
  ThemeSpace,
  classNames,
  isNullOrUndefined,
  setCssVar,
  setThemeColorVar,
} from "@punks/ui-core"

export interface TypographyProps {
  mb?: ThemeSpace
  mt?: ThemeSpace
  ml?: ThemeSpace
  mr?: ThemeSpace
  mx?: ThemeSpace
  my?: ThemeSpace
  m?: ThemeSpace

  pb?: ThemeSpace
  pt?: ThemeSpace
  pl?: ThemeSpace
  pr?: ThemeSpace
  px?: ThemeSpace
  py?: ThemeSpace
  p?: ThemeSpace

  color?: ThemeColor
  colorVariant?: ThemeColorVariant
  colorValue?: string

  bgColor?: ThemeColor
  bgColorVariant?: ThemeColorVariant
  bgColorValue?: string

  variant?: TextVariant
  weight?: FontWeight

  component?: keyof JSX.IntrinsicElements
  className?: string
  children?: any
  align?: TextAlign
  underline?: boolean
  italic?: boolean
  uppercase?: boolean
  capitalize?: boolean

  style?: React.CSSProperties
  onClick?: (e: React.MouseEvent<unknown>) => void
}

export const Typography = ({
  bgColor,
  bgColorVariant,
  bgColorValue,
  children,
  className,
  color,
  colorVariant,
  colorValue,
  m = 0,
  mb,
  ml,
  mr,
  mt,
  mx,
  my,
  p,
  pb,
  pl,
  pr,
  pt,
  px,
  py,
  variant = "body1",
  component,
  align,
  underline,
  italic,
  weight,
  uppercase,
  capitalize,
  style,
  onClick,
}: TypographyProps) => {
  const Tag = component ?? "p"
  const spacings = {
    mb,
    mt,
    ml,
    mr,
    mx,
    my,
    m,
    pb,
    pt,
    pl,
    pr,
    px,
    py,
    p,
  }

  return (
    <Tag
      style={{
        ...getMarginStyles(spacings),
        ...getPaddingStyles(spacings),
        ...(color
          ? {
              ...setThemeColorVar("--text-color", color, colorVariant),
            }
          : {}),
        ...(colorValue
          ? {
              ...setCssVar("--text-color", colorValue),
            }
          : {}),
        ...(bgColor
          ? {
              ...setThemeColorVar("--bg-color", bgColor, bgColorVariant),
            }
          : {}),
        ...(bgColorValue
          ? {
              ...setCssVar("--bg-color", bgColorValue),
            }
          : {}),
        ...style,
      }}
      className={classNames(
        styles.root,
        styles[`text-${variant}`],
        {
          ...getMarginClasses(styles, spacings),
          ...getPaddingClasses(styles, spacings),
          [styles.textColor]: color || colorValue,
          [styles.bgColor]: bgColor || bgColorValue,
          [styles.textNormal]: weight === "normal",
          [styles.textLight]: weight === "light",
          [styles.textLighter]: weight === "lighter",
          [styles.textBold]: weight === "bold",
          [styles.textBolder]: weight === "bolder",
          [styles.textLeft]: align === "left",
          [styles.textCenter]: align === "center",
          [styles.textRight]: align === "right",
          [styles.capitalize]: capitalize,
          [styles.uppercase]: uppercase,
          [styles.underline]: underline,
          [styles.italic]: italic,
          [styles.clickable]: !isNullOrUndefined(onClick),
        },
        className
      )}
      onClick={onClick}
    >
      {children}
    </Tag>
  )
}

export default Typography
