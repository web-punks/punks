import React, { ReactNode, useState } from "react"
import styles from "./PopoverMenu.module.css"
import { classNames } from "@punks/ui-core"
import { Typography } from "../Typography"
import { Popover, PopoverPosition } from "../../panels/Popover"
import { CardShadow } from "../../molecules/Card/Card"

export type PopoverMenuPosition = "bottom" | "top" | "left" | "right"
export type PopoverMenuAlign = "start" | "end" | "center"
export type PopoverMenuTextAlign = "left" | "right" | "center"

export type PopoverMenuItem = {
  content: ReactNode
  onClick?: () => void
  className?: string
}

export interface PopoverMenuProps {
  control: React.ReactNode
  minPanelHeight?: number | string
  maxPanelHeight?: number | string
  minPanelWidth?: number | string
  maxPanelWidth?: number | string
  items: PopoverMenuItem[]
  position?: PopoverMenuPosition
  align?: PopoverMenuAlign
  textAlign?: PopoverMenuTextAlign
  shadow?: CardShadow
  autoClose?: "none" | "click"
}

const getPositions = (position: PopoverMenuPosition) => {
  switch (position) {
    case "bottom":
      return ["bottom" as PopoverPosition, "top" as PopoverPosition]
    case "top":
      return ["top" as PopoverPosition, "bottom" as PopoverPosition]
    case "left":
      return ["left" as PopoverPosition, "right" as PopoverPosition]
    case "right":
      return ["right" as PopoverPosition, "left" as PopoverPosition]
  }
}

export const PopoverMenu = ({
  control,
  position = "bottom",
  align = "start",
  minPanelHeight,
  maxPanelHeight,
  minPanelWidth,
  maxPanelWidth,
  items,
  textAlign,
  shadow,
  autoClose = "click",
}: PopoverMenuProps) => {
  const [isPopoverOpen, setIsPopoverOpen] = useState(false)
  return (
    <Popover
      open={isPopoverOpen}
      onOpen={() => setIsPopoverOpen(true)}
      onClose={() => setIsPopoverOpen(false)}
      shadow={shadow}
      position={getPositions(position)}
      align={align}
      noGutters
      panelStyles={{
        minHeight: minPanelHeight,
        maxHeight: maxPanelHeight,
        minWidth: minPanelWidth,
        maxWidth: maxPanelWidth,
        overflow: "auto",
      }}
      content={
        <div>
          {items?.map((item, index) => (
            <div
              className={classNames(
                styles.dropItem,
                styles[`dropItem-text-${textAlign}`],
                item.className
              )}
              onClick={() => {
                item.onClick?.()
                autoClose === "click" && setIsPopoverOpen(false)
              }}
              key={index}
            >
              <Typography component="div">{item.content}</Typography>
            </div>
          ))}
        </div>
      }
    >
      {control}
    </Popover>
  )
}
