import type { Meta, StoryObj } from "@storybook/react"

import { range } from "@punks/ui-core"
import { PopoverMenu } from "./PopoverMenu"

const meta = {
  title: "Ui/Atoms/PopoverMenu",
  component: PopoverMenu,
  tags: ["autodocs"],
  args: {
    items: [
      {
        content: "Item1 Label",
      },
      {
        content: "Item2 Label",
      },
      {
        content: "Item3 Label",
      },
      {
        content: "Item4 Label",
      },
    ],
  },
} satisfies Meta<typeof PopoverMenu>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    control: "Menu",
  },
}

export const AutocloseDisabled: Story = {
  args: {
    control: "Menu",
    autoClose: "none",
  },
}

export const AutocloseLeft: Story = {
  args: {
    control: "Menu",
    align: "start",
    position: "bottom",
    items: [
      {
        content: "Item1",
      },
      {
        content: "Item2234",
      },
      {
        content: "Item3456",
      },
      {
        content: "Item4",
      },
    ],
  },
}

export const AutocloseMaxHeight: Story = {
  args: {
    control: "Menu",
    maxPanelHeight: 200,
    items: range(40).map((x) => ({
      content: x,
    })),
  },
}
