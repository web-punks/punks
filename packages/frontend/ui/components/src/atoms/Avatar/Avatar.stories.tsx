import type { Meta, StoryObj } from "@storybook/react"
import Avatar from "./Avatar"

const meta = {
  title: "UI/Atoms/Avatar",
  component: Avatar,
  tags: ["autodocs"],
  args: {
    initials: "AB",
  },
} satisfies Meta<typeof Avatar>
export default meta

type Story = StoryObj<typeof meta>

export const FilledPrimary: Story = {
  args: {
    color: "primary",
    variant: "filled",
  },
}

export const FilledSecondary: Story = {
  args: {
    color: "secondary",
    variant: "filled",
  },
}

export const FilledInfo: Story = {
  args: {
    color: "info",
    variant: "filled",
  },
}

export const FilledSuccess: Story = {
  args: {
    color: "success",
    variant: "filled",
  },
}

export const FilledWarning: Story = {
  args: {
    color: "warning",
    variant: "filled",
  },
}

export const FilledError: Story = {
  args: {
    color: "error",
    variant: "filled",
  },
}

export const OutlinedPrimary: Story = {
  args: {
    color: "primary",
    variant: "outlined",
  },
}

export const OutlinedSecondary: Story = {
  args: {
    color: "secondary",
    variant: "outlined",
  },
}

export const OutlinedInfo: Story = {
  args: {
    color: "info",
    variant: "outlined",
  },
}

export const OutlinedSuccess: Story = {
  args: {
    color: "success",
    variant: "outlined",
  },
}

export const OutlinedWarning: Story = {
  args: {
    color: "warning",
    variant: "outlined",
  },
}

export const OutlinedError: Story = {
  args: {
    color: "error",
    variant: "outlined",
  },
}
