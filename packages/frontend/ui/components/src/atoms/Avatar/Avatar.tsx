import { ReactNode } from "react"
import styles from "./Avatar.module.css"
import {
  ClassValue,
  ThemeColor,
  classNames,
  setThemeColorVar,
} from "@punks/ui-core"

export type AvatarClasses = {
  root?: string
  initials?: string
  image?: string
}

export type AvatarSize = "small" | "medium" | "large"
export type AvatarVariant = "filled" | "outlined"

export interface AvatarProps {
  color?: ThemeColor
  initials?: string
  image?: ReactNode
  className?: ClassValue
  classes?: AvatarClasses
  size?: AvatarSize
  variant?: AvatarVariant
}

const Avatar = ({
  color,
  initials,
  image,
  className,
  classes,
  size = "medium",
  variant = "outlined",
}: AvatarProps) => {
  return (
    <div
      style={{
        ...(variant === "filled"
          ? {
              ...setThemeColorVar("--bg-color", color, "default"),
              ...setThemeColorVar("--text-color", color, "contrast"),
            }
          : {}),
        ...(variant === "outlined"
          ? {
              ...setThemeColorVar("--text-color", color, "default"),
            }
          : {}),
      }}
      className={classNames(
        styles.root,
        styles[variant],
        styles[size],
        styles[`${variant}-${size}`],
        classes?.root,
        className
      )}
    >
      {initials && (
        <span className={classNames(styles.label, classes?.initials)}>
          {initials}
        </span>
      )}
      {image && (
        <div className={classNames(styles.image, classes?.image)}>{image}</div>
      )}
    </div>
  )
}

export default Avatar
