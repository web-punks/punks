import React, { CSSProperties } from "react"
import styles from "./Ring.module.css"
import { classNames } from "@punks/ui-core"

export interface RingProps {
  color?: string
  backgroundImage?: string
  size?: string | number
  thickness: string | number
  onClick?: () => void
  className?: string
  style?: CSSProperties
  children?: React.ReactNode
}

const Ring = ({
  color,
  size = "0.875rem",
  thickness,
  onClick,
  className,
  style,
  backgroundImage,
  children,
}: RingProps) => {
  return (
    <div
      className={styles.container}
      style={{
        ["--size" as any]: typeof size === "number" ? `${size}px` : size,
        ["--color" as any]: color,
        ["--thickness" as any]:
          typeof thickness === "number" ? `${thickness}px` : thickness,
        ["--background-img" as any]: backgroundImage,
      }}
    >
      <div
        className={classNames(styles.circle, className)}
        onClick={onClick}
        style={style}
      >
        {children}
      </div>
    </div>
  )
}

export default Ring
