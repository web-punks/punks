import type { Meta, StoryObj } from "@storybook/react"
import { Ring } from "."

const meta = {
  title: "UI/Atoms/Ring",
  component: Ring,
  args: {
    children: "x",
  },
} satisfies Meta<typeof Ring>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    color: "yellow",
    size: 28,
    thickness: 4,
  },
}
