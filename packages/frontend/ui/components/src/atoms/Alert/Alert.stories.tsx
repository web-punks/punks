import type { Meta, StoryObj } from "@storybook/react"
import Alert from "./Alert"

const meta = {
  title: "UI/Atoms/Alert",
  component: Alert,
  tags: ["autodocs"],
  args: {
    children: "Lorem ipsium",
  },
} satisfies Meta<typeof Alert>
export default meta

type Story = StoryObj<typeof meta>

export const Success: Story = {
  args: {
    type: "success",
  },
}

export const Info: Story = {
  args: {
    type: "info",
  },
}

export const InfoCloseable: Story = {
  args: {
    type: "info",
    onClose: () => console.log("close"),
  },
}

export const Warning: Story = {
  args: {
    type: "warning",
  },
}

export const Error: Story = {
  args: {
    type: "error",
  },
}

export const SuccessFilled: Story = {
  args: {
    type: "success",
    variant: "filled",
  },
}

export const InfoFilled: Story = {
  args: {
    type: "info",
    variant: "filled",
  },
}

export const WarningFilled: Story = {
  args: {
    type: "warning",
    variant: "filled",
  },
}

export const ErrorFilled: Story = {
  args: {
    type: "error",
    variant: "filled",
  },
}

export const SuccessOutlined: Story = {
  args: {
    type: "success",
    variant: "outlined",
  },
}

export const InfoOutlined: Story = {
  args: {
    type: "info",
    variant: "outlined",
  },
}

export const WarningOutlined: Story = {
  args: {
    type: "warning",
    variant: "outlined",
  },
}

export const ErrorOutlined: Story = {
  args: {
    type: "error",
    variant: "outlined",
  },
}
