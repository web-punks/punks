import { useState } from "react"
import {
  CloseIcon,
  ErrorIcon,
  InfoIcon,
  SuccessIcon,
  WarningIcon,
} from "../../ui/icons"
import styles from "./Alert.module.css"
import { classNames, setThemeColorVar } from "@punks/ui-core"

export type AlertSeverity = "success" | "error" | "warning" | "info"
export type AlertVariant = "light" | "filled" | "outlined"

export interface AlertProps {
  type?: AlertSeverity
  children: any
  show?: boolean
  className?: string
  variant?: AlertVariant
  onClose?: () => void
}

const AlertInfoIcon = () => <InfoIcon className={styles.icon} />
const AlertSuccessIcon = () => <SuccessIcon className={styles.icon} />
const AlertWarningIcon = () => <WarningIcon className={styles.icon} />
const AlertErrorIcon = () => <ErrorIcon className={styles.icon} />

const Alert = ({
  type = "info",
  variant = "light",
  children,
  show,
  className,
  onClose,
}: AlertProps) => {
  if (show === false) {
    return <></>
  }
  return (
    <div
      style={{
        ...(variant === "light" && {
          ...setThemeColorVar("--bg-color", type, "light"),
          ...setThemeColorVar("--text-color", type, "dark"),
        }),
        ...(variant === "filled" && {
          ...setThemeColorVar("--bg-color", type),
          ...setThemeColorVar("--text-color", type, "contrast"),
        }),
        ...(variant === "outlined" && {
          ...setThemeColorVar("--bg-color", type),
          ...setThemeColorVar("--text-color", type),
          ...setThemeColorVar("--border-color", type),
        }),
      }}
      className={classNames(
        styles.root,
        styles[variant],
        styles[`color-${type}`],
        className
      )}
      role="alert"
    >
      <div className={styles.body}>
        {type === "info" && <AlertInfoIcon />}
        {type === "success" && <AlertSuccessIcon />}
        {type === "warning" && <AlertWarningIcon />}
        {type === "error" && <AlertErrorIcon />}
        <span className={styles.content}>{children}</span>
      </div>
      {onClose && (
        <div className={styles.coseBar}>
          <CloseIcon className={styles.close} onClick={onClose} />
        </div>
      )}
    </div>
  )
}

export default Alert
