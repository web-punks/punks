import type { Meta, StoryObj } from "@storybook/react"

import { FilterTag } from "."

const meta = {
  title: "Ui/Atoms/FilterTag",
  component: FilterTag,
  tags: ["autodocs"],
  args: {
    category: "Category",
    value: "Value",
  },
} satisfies Meta<typeof FilterTag>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    color: "primary",
  },
}
