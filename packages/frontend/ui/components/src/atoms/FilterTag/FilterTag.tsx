import { ThemeColor, classNames, setColorVar, setCssVar } from "@punks/ui-core"
import { Typography } from "../Typography"
import styles from "./FilterTag.module.css"
import { CloseIcon } from "../../ui/icons"

export type FilterTagSize = "small" | "medium" | "large"

export interface FilterTagProps {
  category?: React.ReactNode
  value: React.ReactNode
  color?: ThemeColor
  size?: FilterTagSize
  className?: string
  onRemove?: () => void
}

const FilterTag = ({
  color = "info",
  category,
  value,
  size = "medium",
  className,
  onRemove,
}: FilterTagProps) => {
  return (
    <div
      style={{
        ...setColorVar("--bg", color),
      }}
      className={classNames(styles.root, styles[size], className)}
    >
      {category && (
        <Typography color={color} colorVariant="contrast" mr={1}>
          {category}:
        </Typography>
      )}
      <Typography color={color} colorVariant="contrast">
        {value}
      </Typography>
      <Typography
        color={color}
        colorVariant="contrast"
        className={styles.closeIcon}
        onClick={onRemove}
      >
        <CloseIcon />
      </Typography>
    </div>
  )
}

export default FilterTag
