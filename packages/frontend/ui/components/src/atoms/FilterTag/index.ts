export {
  default as FilterTag,
  FilterTagProps,
  FilterTagSize,
} from "./FilterTag"
