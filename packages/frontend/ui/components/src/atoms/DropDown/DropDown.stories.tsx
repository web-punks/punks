import type { Meta, StoryObj } from "@storybook/react"

import { DropDown } from "."
import { range } from "@punks/ui-core"

const meta = {
  title: "Ui/Atoms/DropDown",
  component: DropDown,
  tags: ["autodocs"],
  args: {
    items: [
      {
        content: "Item1 Label",
      },
      {
        content: "Item2 Label",
      },
      {
        content: "Item3 Label",
      },
      {
        content: "Item4 Label",
      },
    ],
  },
} satisfies Meta<typeof DropDown>
export default meta

type Story = StoryObj<typeof meta>

export const DropDownDefault: Story = {
  args: {
    control: "Menu",
  },
}

export const DropDownLeft: Story = {
  args: {
    control: "Menu",
    itemAlign: "left",
    items: [
      {
        content: "Item1",
      },
      {
        content: "Item2234",
      },
      {
        content: "Item3456",
      },
      {
        content: "Item4",
      },
    ],
  },
}

export const DropDownBordered: Story = {
  args: {
    control: "Menu",
    bordered: true,
    items: [
      {
        content: "Item1",
      },
      {
        content: "Item2234",
      },
      {
        content: "Item3456",
      },
      {
        content: "Item4",
      },
    ],
  },
}

export const DropDownMaxHeight: Story = {
  args: {
    control: "Menu",
    maxPanelHeight: 200,
    items: range(40).map((x) => ({
      content: x,
    })),
  },
}

export const DropDownBottomLeft: Story = {
  args: {
    control: "Menu",
    position: "bottomLeft",
  },
}

export const DropDownBottomRight: Story = {
  args: {
    control: "Menu",
    position: "bottomRight",
  },
}
