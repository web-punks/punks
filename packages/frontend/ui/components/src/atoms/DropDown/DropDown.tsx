import React, { ReactNode } from "react"
import styles from "./DropDown.module.css"
import { ChevronDown } from "../../ui/icons"
import { classNames, useDetectClickOutside } from "@punks/ui-core"
import { Typography } from "../Typography"
import { Card } from "../../molecules/Card"

export type DropDownPosition = "bottomLeft" | "bottomRight"

export type DropDownItem = {
  content: ReactNode
  onClick?: () => void
  className?: string
}

export interface DropDownProps {
  control: React.ReactNode
  defaultOpen?: boolean
  dropIcon?: React.ReactNode
  dropIconHidden?: boolean
  position?: DropDownPosition
  minPanelHeight?: number | string
  maxPanelHeight?: number | string
  minPanelWidth?: number | string
  maxPanelWidth?: number | string
  items: DropDownItem[]
  itemAlign?: "left" | "right" | "center"
  bordered?: boolean
  panelClassName?: string
  withTip?: boolean
}

export const DropDown = ({
  control,
  dropIcon,
  dropIconHidden,
  defaultOpen,
  position = "bottomRight",
  minPanelHeight,
  maxPanelHeight,
  minPanelWidth,
  maxPanelWidth,
  items,
  itemAlign = "center",
  bordered,
  panelClassName,
  withTip = false,
}: DropDownProps) => {
  const [open, setOpen] = React.useState(defaultOpen ?? false)
  const ref = useDetectClickOutside<HTMLDivElement>({
    onTriggered: () => setOpen(false),
  })

  const [controlWidth, setControlWidth] = React.useState(0)

  React.useLayoutEffect(() => {
    setControlWidth(ref.current?.offsetWidth ?? 0)
  }, [])

  return (
    <div
      className={classNames(styles.root, {
        [styles.open]: open,
      })}
      ref={ref}
    >
      <div className={styles.control} onClick={() => setOpen(!open)}>
        {control}
        {dropIconHidden !== true && (
          <div className={styles.dropIcon}>
            {dropIcon ? dropIcon : <ChevronDown />}
          </div>
        )}
      </div>
      <div
        className={classNames(styles.panel, {
          [styles.panelBottomLeft]: position === "bottomLeft",
          [styles.panelBottomRight]: position === "bottomRight",
        })}
      >
        {withTip && (
          <div
            className={classNames(styles.tipContainer)}
            style={{ width: controlWidth }}
          >
            <div
              className={classNames(
                bordered ? styles.tipOutlined : styles.tipShadow
              )}
            >
              <div className={classNames(styles.tipArrow)} />
            </div>
          </div>
        )}
        <Card
          noGutters
          noShadow={bordered}
          style={{
            maxWidth: maxPanelWidth,
            minWidth: minPanelWidth,
            maxHeight: maxPanelHeight,
            minHeight: minPanelHeight,
          }}
          className={classNames(
            {
              [styles.panelMaxH]: !!maxPanelHeight,
              [styles.bordered]: bordered,
            },
            panelClassName
          )}
        >
          {items?.map((item, index) => (
            <div
              className={classNames(
                styles.dropItem,
                styles[`dropItem-text-${itemAlign}`],
                item.className
              )}
              onClick={() => {
                item.onClick?.()
                setOpen(false)
              }}
              key={index}
            >
              <Typography component="div">{item.content}</Typography>
            </div>
          ))}
        </Card>
      </div>
    </div>
  )
}
