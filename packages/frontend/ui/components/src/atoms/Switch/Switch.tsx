import React, { useEffect, useRef, useState } from "react"
import styles from "./Switch.module.css"
import { classNames } from "@punks/ui-core"

export type SwitchSize = "small" | "medium" | "large"

export interface SwitchProps
  extends Omit<React.InputHTMLAttributes<HTMLInputElement>, "size"> {
  inputRef?: React.Ref<HTMLInputElement>
  size?: SwitchSize
  onValueChange?: (value: boolean) => void
}

const Switch = ({
  inputRef,
  size = "medium",
  onChange,
  onValueChange,
  defaultChecked,
  checked,
  disabled,
  ...other
}: SwitchProps) => {
  const rootRef = useRef<HTMLLabelElement | null>(null)
  const [isControlled] = useState(other.value !== undefined)
  const [uncontrolledValue, setUncontrolledValue] = useState(
    defaultChecked ?? false
  )

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (disabled) {
      return
    }

    if (onChange) {
      onChange(e)
    }
    if (onValueChange) {
      onValueChange(uncontrolledValue)
    }
  }

  const checkedValue = checked ?? uncontrolledValue

  const toggle = () => {
    if (disabled) {
      return
    }
    if (!isControlled) {
      setUncontrolledValue(!uncontrolledValue)
    }

    onChange?.({
      type: "bool",
      target: {
        name: other.name,
        value: !checkedValue,
      },
    } as any)

    if (onValueChange) {
      onValueChange(!checkedValue)
    }
  }

  useEffect(() => {
    if (isControlled) {
      return
    }

    const value = rootRef.current?.getElementsByTagName("input")[0].value
    if (value && value === "true") {
      setUncontrolledValue(true)
    }
  }, [inputRef])

  return (
    <label
      className={classNames(styles.root, {
        [styles.small]: size === "small",
        [styles.medium]: size === "medium",
        [styles.large]: size === "large",
        [styles.disabled]: disabled,
      })}
      ref={rootRef}
    >
      <input
        ref={inputRef}
        type={styles.input}
        checked={checkedValue}
        onChange={(e) => handleChange(e)}
        onClick={() => toggle()}
        value={checkedValue ? "true" : "false"}
        disabled={disabled}
        {...other}
      />
      <span
        className={classNames(styles.slider, {
          [styles.sliderUnchecked]: !checkedValue,
          [styles.sliderChecked]: checkedValue,
        })}
      ></span>
    </label>
  )
}

const RefSwitch = React.forwardRef<HTMLInputElement, SwitchProps>(
  (props, ref) => <Switch {...props} inputRef={ref ?? props.inputRef} />
)

export default RefSwitch
