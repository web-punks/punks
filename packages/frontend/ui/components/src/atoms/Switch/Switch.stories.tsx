import type { Meta, StoryObj } from "@storybook/react"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import { useForm } from "react-hook-form"

import { Switch } from "."
import { Box } from "../../structure"
import { Button } from "../Button"

const meta = {
  title: "Ui/Atoms/Switch",
  component: Switch,
  argTypes: {},
} satisfies Meta<typeof Switch>
export default meta

type Story = StoryObj<typeof meta>

export const SwitchSm: Story = {
  args: {
    size: "small",
    onChange: (e) => console.log(e),
  },
}

export const SwitchMd: Story = {
  args: {
    size: "medium",
    onChange: (e) => console.log(e),
  },
}

export const SwitchLarge: Story = {
  args: {
    size: "large",
    onChange: (e) => console.log(e),
  },
}

export const SwitchDefaultChecked: Story = {
  args: {
    defaultChecked: true,
  },
}

export const SwitchWithReactHookForm: Story = {
  args: {},
  decorators: [
    (Story, props) => {
      const schema = yup.object().shape({
        value: yup.boolean().required(),
      })
      const {
        register,
        handleSubmit,
        formState: { errors },
        getValues,
      } = useForm({
        mode: "all",
        resolver: yupResolver(schema),
        defaultValues: {
          value: true,
        },
      })
      console.log("f", getValues())
      return (
        <div style={{ width: "300px" }}>
          <Story
            args={{
              ...props.args,
              ...register("value"),
            }}
          />
          <Box mt={2}>
            <Button
              variant="filled"
              color="primary"
              fullWidth
              onClick={handleSubmit((data) => console.log("submit data", data))}
            >
              submit
            </Button>
          </Box>
        </div>
      )
    },
  ],
}

export const SwitchDisabled: Story = {
  args: {
    defaultChecked: true,
    disabled: true,
  },
}
