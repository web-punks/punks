import type { Meta, StoryObj } from "@storybook/react"

import { Tag } from "."

const meta = {
  title: "Ui/Atoms/Tag",
  component: Tag,
  tags: ["autodocs"],
  args: {
    children: "Tag Content",
  },
} satisfies Meta<typeof Tag>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    color: "primary",
  },
}

export const WithRemove: Story = {
  args: {
    color: "primary",
  },
}
