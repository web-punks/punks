import { ThemeColor, ThemeColorVariant, classNames } from "@punks/ui-core"
import { Typography } from "../Typography"
import styles from "./Tag.module.css"
import { CloseIcon } from "../../ui/icons"

export type TagSize = "small" | "medium" | "large"

export interface TagProps {
  children?: React.ReactNode
  color?: ThemeColor
  colorVariant?: ThemeColorVariant
  textColor?: string
  backgroundColor?: string
  size?: TagSize
  className?: string
  onRemove?: (e: React.MouseEvent) => void
}

const Tag = ({
  color,
  colorVariant,
  children,
  backgroundColor,
  textColor,
  size = "medium",
  className,
  onRemove,
}: TagProps) => {
  return (
    <Typography
      bgColor={color}
      bgColorVariant={colorVariant}
      color={color}
      colorVariant="contrast"
      style={{
        backgroundColor,
        color: textColor,
      }}
      className={classNames(styles.root, styles[size], className)}
    >
      {children}
      {onRemove && (
        <CloseIcon
          className={styles.close}
          onClick={onRemove}
          width={12}
          height={12}
        />
      )}
    </Typography>
  )
}

export default Tag
