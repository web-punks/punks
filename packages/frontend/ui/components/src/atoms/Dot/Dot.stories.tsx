import type { Meta, StoryObj } from "@storybook/react"
import { Dot } from "."

const meta = {
  title: "UI/Atoms/Dot",
  component: Dot,
  args: {
    children: "x",
  },
} satisfies Meta<typeof Dot>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    color: "yellow",
    size: "28px",
  },
}
