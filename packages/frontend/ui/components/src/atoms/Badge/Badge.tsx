import {
  TextVariant,
  ThemeColor,
  ThemeColorVariant,
  classNames,
} from "@punks/ui-core"
import { Typography } from "../Typography"
import styles from "./Badge.module.css"

export type TagSize = "small" | "medium" | "large"

export interface BadgeProps {
  children?: React.ReactNode
  color?: ThemeColor
  colorVariant?: ThemeColorVariant
  colorValue?: string
  textColor?: ThemeColor
  textColorVariant?: ThemeColorVariant
  textVariant?: TextVariant
  textColorValue?: string
  size?: TagSize
  className?: string
  center?: boolean
  component?: keyof JSX.IntrinsicElements
}

const DEFAULT_VARIANTS = {
  small: "body3" as TextVariant,
  medium: "body2" as TextVariant,
  large: "body1" as TextVariant,
}

const Badge = ({
  color,
  colorVariant,
  colorValue,
  children,
  textColor,
  textColorVariant,
  textColorValue,
  textVariant,
  size = "medium",
  className,
  component = "div",
  center = true,
}: BadgeProps) => {
  return (
    <Typography
      bgColor={color}
      bgColorVariant={colorVariant}
      bgColorValue={colorValue}
      align={center ? "center" : undefined}
      component={component}
      {...(textColorValue
        ? {
            colorValue: textColorValue,
          }
        : textColor
          ? {
              color: textColor,
              colorVariant: textColorVariant,
            }
          : {
              color,
              colorVariant: "contrast",
            })}
      variant={textVariant ?? DEFAULT_VARIANTS[size]}
      className={classNames(styles.root, styles[size], className)}
    >
      {children}
    </Typography>
  )
}

export default Badge
