import type { Meta, StoryObj } from "@storybook/react"

import { Badge } from "."

const meta = {
  title: "Ui/Atoms/Badge",
  component: Badge,
  tags: ["autodocs"],
  args: {
    children: "Badge Content",
  },
} satisfies Meta<typeof Badge>
export default meta

type Story = StoryObj<typeof meta>

export const Small: Story = {
  args: {
    color: "primary",
    size: "small",
  },
}

export const Medium: Story = {
  args: {
    color: "primary",
    size: "medium",
  },
}

export const Large: Story = {
  args: {
    color: "primary",
    size: "large",
  },
}

export const CustomColors: Story = {
  args: {
    color: "success",
    colorVariant: "light",
    textColor: "dark",
    size: "large",
  },
}
