import {
  ThemeColor,
  classNames,
  normalizePxSize,
  setColorVar,
} from "@punks/ui-core"
import styles from "./StatusIndicator.module.css"
import { ApprovedIcon, PendingIcon, DeniedIcon } from "../../ui/icons"

export type Status = "success" | "error" | "warning"

type StatusProps = {
  status: Status
  statusIcon?: React.ReactNode
  statusIconSize?: number
  children: any
  className?: string
}

const getStatusIcon = (status: Status, className: string) => {
  switch (status) {
    case "success":
      return <ApprovedIcon className={className} />
    case "error":
      return <DeniedIcon className={className} />
    case "warning":
      return <PendingIcon className={className} />
  }
}

const getBackgroundColor = (status: Status): ThemeColor => {
  switch (status) {
    case "success":
      return "success"
    case "error":
      return "error"
    case "warning":
      return "warning"
  }
}

const StatusIndicator: React.FC<StatusProps> = ({
  status,
  statusIcon,
  statusIconSize = 22,
  children,
  className,
}) => {
  return (
    <div
      style={{
        ...setColorVar("--bg", getBackgroundColor(status)),
        ...(statusIconSize
          ? { "--icon-size": normalizePxSize(statusIconSize) }
          : {}),
      }}
      className={classNames(styles.root, className)}
    >
      {statusIcon ?? getStatusIcon(status, styles.icon)}
      {children}
    </div>
  )
}

export default StatusIndicator
