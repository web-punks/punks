import type { Meta, StoryObj } from "@storybook/react"
import StatusIndicator from "./StatusIndicator"

const meta = {
  title: "UI/Atoms/StatusIndicator",
  component: StatusIndicator,
  tags: ["autodocs"],
} satisfies Meta<typeof StatusIndicator>
export default meta

type Story = StoryObj<typeof meta>

export const Success: Story = {
  args: {
    status: "success",
    children: "Approvato",
  },
  parameters: {
    backgrounds: {
      default: "white",
      values: [
        {
          name: "white",
          value: "#ffffff",
        },
        {
          name: "gray",
          value: "#f5f5f5",
        },
      ],
    },
  },
}

export const Error: Story = {
  args: {
    status: "error",
    children: "Rifiutato",
  },
}

export const Warning: Story = {
  args: {
    status: "warning",
    children: "In corso",
  },
}
