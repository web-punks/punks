import React from "react"
import { Rectangle, RectangleProps } from "../Rectangle"

export interface SquareProps extends Omit<RectangleProps, "ratio"> {}

const Square = ({ children, ...other }: SquareProps) => {
  return (
    <Rectangle ratio={1} {...other}>
      {children}
    </Rectangle>
  )
}

export default Square
