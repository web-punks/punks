import { classNames, setCssVar } from "@punks/ui-core"
import React from "react"
import styles from "./Rectangle.module.css"

export interface RectangleClasses {
  root?: string
  content?: string
}

export interface RectangleProps {
  ratio: number
  center?: boolean
  children?: any
  className?: string
  classes?: RectangleClasses
  style?: React.CSSProperties
  fill?: boolean
}

const Rectangle = ({
  ratio,
  center,
  children,
  className,
  classes,
  style,
  fill,
}: RectangleProps) => {
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.fill]: fill,
        },
        classes?.root,
        className
      )}
      style={{
        ...style,
        ...setCssVar("--ratio", `${ratio * 100}%`),
      }}
    >
      <div
        className={classNames(styles.content, classes?.content, {
          [styles.center]: center,
        })}
      >
        {children}
      </div>
    </div>
  )
}

export default Rectangle
