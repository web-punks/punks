import React from "react"
import { Square, SquareProps } from "../Square"
import { classNames } from "@punks/ui-core"
import styles from "./Circle.module.css"

export interface CircleProps extends SquareProps {}

const Circle = ({ children, className, ...other }: CircleProps) => {
  return (
    <Square className={classNames(styles.root, className)} {...other}>
      {children}
    </Square>
  )
}

export default Circle
