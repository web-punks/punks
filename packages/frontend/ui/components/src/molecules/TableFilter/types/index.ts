export enum TableColumnFilterType {
  Date = "date",
  Text = "text",
  SingleSelect = "singleSelect",
  MultipleSelect = "multipleSelect",
}

export type TableColumnFilterOption<TValue> = {
  label: string
  value: TValue
}

export type TableFilterLayoutOptions = {
  minWidth?: number
  maxWidth?: number
}

export type TableFilterBase<TValue> = {
  enabled?: boolean
  options?: TableColumnFilterOption<TValue>[]
  layout?: TableFilterLayoutOptions
}

export type TableTextFilterType = "contains" | "isEmpty" | "isNotEmpty"

export type TableTextFilterValue = {
  type: TableTextFilterType
  value?: string
}

export type TableTextFilter<TValue> = TableFilterBase<TValue> & {
  type: TableColumnFilterType.Text
  onSubmit: (value: TableTextFilterValue) => void
  onClear: () => void
  value?: TableTextFilterValue
  types?: TableTextFilterType[]
}

export type TableFilterDateOperator = "=" | ">" | "<" | ">=" | "<="
export type TableFilterDateValue = {
  date: string
  operator: TableFilterDateOperator
}

export type TableDateFilter<TValue> = TableFilterBase<TValue> & {
  type: TableColumnFilterType.Date
  onSubmit: (value: TableFilterDateValue) => void
  onClear: () => void
  value?: TableFilterDateValue
}

export type TableSingleSelectFilter<TValue> = TableFilterBase<TValue> & {
  type: TableColumnFilterType.SingleSelect
  onChange: (value?: TValue) => void
  selectedValue?: TValue
}

export type TableMultiSelectFilter<TValue> = TableFilterBase<TValue> & {
  type: TableColumnFilterType.MultipleSelect
  onChange: (values: TValue[]) => void
  selectedValues?: TValue[]
}

export type TableColumnFilter<TValue> =
  | TableTextFilter<TValue>
  | TableSingleSelectFilter<TValue>
  | TableMultiSelectFilter<TValue>
  | TableDateFilter<TValue>
