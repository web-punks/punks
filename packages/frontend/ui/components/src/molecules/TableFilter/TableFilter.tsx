import React from "react"
import {
  TableFilterText,
  TableFilterTextLabels,
} from "./components/TableFilterText"
import TableFilterMultipleSelect from "./components/TableFilterMultipleSelect"
import TableFilterSingleSelect from "./components/TableFilterSingleSelect"
import {
  TableColumnFilter,
  TableColumnFilterType,
  TableDateFilter,
  TableMultiSelectFilter,
  TableSingleSelectFilter,
  TableTextFilter,
} from "./types"
import {
  TableFilterDate,
  TableFilterDateLabels,
} from "./components/TableFilterDate"

export type TableFilterLabels = {
  textFilter: TableFilterTextLabels
  dateFilter: TableFilterDateLabels
}

export type TableFilterProps = {
  filter: TableColumnFilter<unknown>
  labels: TableFilterLabels
  locale: string
}

const TableFilter: React.FC<TableFilterProps> = ({
  filter,
  labels,
  locale,
}) => {
  switch (filter.type) {
    case TableColumnFilterType.MultipleSelect:
      return (
        <TableFilterMultipleSelect
          filter={filter as TableMultiSelectFilter<unknown>}
        />
      )
    case TableColumnFilterType.SingleSelect:
      return (
        <TableFilterSingleSelect
          filter={filter as TableSingleSelectFilter<unknown>}
        />
      )
    case TableColumnFilterType.Text:
      return (
        <TableFilterText
          filter={filter as TableTextFilter<unknown>}
          labels={labels.textFilter}
        />
      )
    case TableColumnFilterType.Date:
      return (
        <TableFilterDate
          filter={filter as TableDateFilter<unknown>}
          labels={labels.dateFilter}
          locale={locale}
        />
      )
    default:
      console.error("Invalid Table Column Filter Type", filter)
      return <></>
  }
}

export default TableFilter
