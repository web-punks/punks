import React from "react"
import styles from "./TableFilterMultipleSelect.module.css"
import { TableMultiSelectFilter } from "../../types"
import { CheckIcon } from "../../../../ui"
import { classNames } from "@punks/ui-core"
import TableFilterControl from "../TableFilterControl"
import { PopoverMenu, PopoverMenuItem } from "../../../../atoms/PopoverMenu"

interface TableFilterMultipleSelectProps {
  filter: TableMultiSelectFilter<unknown>
}

const TableFilterMultipleSelect: React.FC<TableFilterMultipleSelectProps> = ({
  filter,
}) => {
  const toggleOption = (value: unknown) => {
    const isPresent = filter.selectedValues?.some(
      (selectedValue) => selectedValue == value
    )

    if (isPresent) {
      filter.onChange?.(
        filter.selectedValues?.filter(
          (selectedValue) => selectedValue !== value
        )!
      )
    } else {
      filter.onChange?.([...(filter.selectedValues ?? []), value])
    }
  }

  return (
    <div>
      <PopoverMenu
        autoClose="none"
        shadow="2"
        position="bottom"
        align="end"
        minPanelWidth={filter?.layout?.minWidth}
        control={<TableFilterControl />}
        items={
          filter.options?.map(
            (o): PopoverMenuItem => ({
              content: (
                <div
                  className={classNames(
                    styles.content,
                    filter.selectedValues?.some(
                      (selectedValue) => selectedValue == o.value
                    )
                      ? styles.checked
                      : styles.unchecked
                  )}
                >
                  <CheckIcon width={12} className={classNames(styles.icon)} />
                  <p className={styles.text}>{o.label}</p>
                </div>
              ),
              onClick: () => toggleOption(o.value),
              className: styles.item,
            })
          )!
        }
      />
    </div>
  )
}

export default TableFilterMultipleSelect
