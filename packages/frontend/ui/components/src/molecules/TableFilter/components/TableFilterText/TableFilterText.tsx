import React, { useState } from "react"
import styles from "./TableFilterText.module.css"
import { TableTextFilter, TableTextFilterType } from "../../types"
import { CloseIcon, SearchIcon2 } from "../../../../ui"
import TableFilterControl from "../TableFilterControl"
import { TextInput } from "../../../../atoms/TextInput"
import { Button } from "../../../../atoms/Button"
import { Popover } from "../../../../panels/Popover"
import { Select } from "../../../../atoms/Select"

export type TableFilterTextLabels = {
  search: string
  apply: string
  contains: string
  isEmpty: string
  isNotEmpty: string
}

export interface TableFilterTextProps {
  filter: TableTextFilter<unknown>
  labels: TableFilterTextLabels
}

const TableFilterText: React.FC<TableFilterTextProps> = ({
  filter,
  labels,
}) => {
  const [open, setOpen] = useState(false)
  const [term, setTerm] = useState("")

  const types = filter.types ?? ["contains"]
  const [type, setType] = useState<TableTextFilterType>(types[0])

  const handleContainsSubmit = () => {
    if (term) {
      filter.onSubmit({
        type: "contains",
        value: term,
      })
      setTerm("")
    } else {
      filter.onClear()
    }
    setOpen(false)
  }

  const handleContainsClear = () => {
    setTerm("")
    filter.onClear()
    setOpen(false)
  }

  const handleIsEmptySubmit = () => {
    filter.onSubmit({
      type: "isEmpty",
      value: undefined,
    })
    setOpen(false)
  }

  const handleIsNotEmptySubmit = () => {
    filter.onSubmit({
      type: "isNotEmpty",
      value: undefined,
    })
    setOpen(false)
  }

  return (
    <div>
      <Popover
        open={open}
        onOpen={() => setOpen(true)}
        onClose={() => setOpen(false)}
        shadow="2"
        position="bottom"
        align="end"
        panelStyles={{
          minWidth: filter?.layout?.minWidth ?? 80,
          maxWidth: filter?.layout?.maxWidth ?? 220,
          overflow: "auto",
        }}
        classes={{
          panel: styles.panel,
        }}
        content={
          <>
            {types.length > 1 ? (
              <>
                <Select
                  fullWidth
                  value={type}
                  className={styles.select}
                  onValueChange={(type) => setType(type as TableTextFilterType)}
                  options={[
                    {
                      label: labels.contains,
                      value: "contains",
                    },
                    {
                      label: labels.isEmpty,
                      value: "isEmpty",
                    },
                    {
                      label: labels.isNotEmpty,
                      value: "isNotEmpty",
                    },
                  ].filter((x) =>
                    types.includes(x.value as TableTextFilterType)
                  )}
                />
              </>
            ) : (
              <></>
            )}
            {type === "contains" && (
              <>
                <TextInput
                  placeholder={labels.search}
                  value={term}
                  onChange={(event) => setTerm(event.target.value)}
                  startAdornment={<SearchIcon2 width={16} height={16} />}
                  endAdornment={
                    <CloseIcon
                      width={10}
                      height={10}
                      onClick={handleContainsClear}
                      className={styles.clear}
                    />
                  }
                  fullWidth
                />
                <Button
                  className={styles.searchButton}
                  color="primary"
                  size="sm"
                  variant="filled"
                  fullWidth
                  onClick={handleContainsSubmit}
                >
                  {labels.apply}
                </Button>
              </>
            )}
            {type === "isEmpty" && (
              <>
                <Button
                  className={styles.searchButton}
                  color="primary"
                  size="sm"
                  variant="filled"
                  fullWidth
                  onClick={handleIsEmptySubmit}
                >
                  {labels.apply}
                </Button>
              </>
            )}
            {type === "isNotEmpty" && (
              <>
                <Button
                  className={styles.searchButton}
                  color="primary"
                  size="sm"
                  variant="filled"
                  fullWidth
                  onClick={handleIsNotEmptySubmit}
                >
                  {labels.apply}
                </Button>
              </>
            )}
          </>
        }
      >
        <TableFilterControl />
      </Popover>
    </div>
  )
}

export default TableFilterText
