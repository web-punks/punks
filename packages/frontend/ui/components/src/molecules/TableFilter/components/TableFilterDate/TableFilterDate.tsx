import React, { useState } from "react"
import styles from "./TableFilterDate.module.css"
import { TableDateFilter, TableFilterDateOperator } from "../../types"
import TableFilterControl from "../TableFilterControl"
import { Button } from "../../../../atoms/Button"
import { Popover } from "../../../../panels/Popover"
import Calendar from "react-calendar"
import { Select } from "../../../../atoms"

export type TableFilterDateLabels = {
  apply: string
}

export interface TableFilterDateProps {
  filter: TableDateFilter<unknown>
  labels: TableFilterDateLabels
  locale: string
}

const TableFilterDate: React.FC<TableFilterDateProps> = ({
  filter,
  labels,
  locale = "en-US",
}) => {
  const [open, setOpen] = useState(false)
  const [date, setDate] = useState<string>()
  const [operator, setOperator] = useState<TableFilterDateOperator>("=")

  const handleSubmit = () => {
    if (!date) {
      return
    }
    filter.onSubmit({ date, operator })
    setOpen(false)
  }

  const handleClear = () => {
    filter.onClear()
    setOpen(false)
  }

  const handleDateChange = (date: Date) => {
    const dateValue = {
      day: date.getDate(),
      month: date.getMonth() + 1,
      year: date.getFullYear(),
    }
    setDate(
      `${dateValue.year}-${dateValue.month
        .toString()
        .padStart(2, "0")}-${dateValue.day.toString().padStart(2, "0")}`
    )
  }

  return (
    <div>
      <Popover
        open={open}
        onOpen={() => setOpen(true)}
        onClose={() => setOpen(false)}
        shadow="2"
        position="bottom"
        align="end"
        panelStyles={{
          minWidth: filter?.layout?.minWidth ?? 80,
          maxWidth: filter?.layout?.maxWidth ?? 280,
          overflow: "auto",
        }}
        classes={{
          panel: styles.panel,
        }}
        content={
          <>
            <Select
              fullWidth
              value={operator}
              onValueChange={(operator) =>
                setOperator(operator as TableFilterDateOperator)
              }
              options={[
                {
                  label: "=",
                  value: "=",
                },
                {
                  label: ">",
                  value: ">",
                },
                {
                  label: ">=",
                  value: ">=",
                },
                {
                  label: "<",
                  value: "<",
                },
                {
                  label: "<=",
                  value: "<=",
                },
              ]}
            />

            <Calendar
              className={styles.calendar}
              locale={locale}
              value={date ? new Date(date) : undefined}
              onChange={(date) =>
                date ? handleDateChange(new Date(date as any)) : undefined
              }
            />

            <Button
              className={styles.searchButton}
              color="primary"
              size="sm"
              variant="filled"
              fullWidth
              onClick={handleSubmit}
            >
              {labels.apply}
            </Button>
          </>
        }
      >
        <TableFilterControl />
      </Popover>
    </div>
  )
}

export default TableFilterDate
