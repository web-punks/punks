import { FilterIcon } from "../../../../ui"
import styles from "./TableFilterControl.module.css"

const TableFilterControl = () => {
  return (
    <div className={styles.control}>
      <FilterIcon width={13} height={13} />
    </div>
  )
}

export default TableFilterControl
