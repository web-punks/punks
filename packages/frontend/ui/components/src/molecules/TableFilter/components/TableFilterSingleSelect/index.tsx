import React from "react"
import styles from "./TableFilterSingleSelect.module.css"
import { TableSingleSelectFilter } from "../../types"
import { CheckIcon } from "../../../../ui"
import { classNames } from "@punks/ui-core"
import { PopoverMenu, PopoverMenuItem } from "../../../../atoms/PopoverMenu"
import TableFilterControl from "../TableFilterControl"

interface TableFilterSingleSelectProps {
  filter: TableSingleSelectFilter<unknown>
}

const TableFilterSingleSelect: React.FC<TableFilterSingleSelectProps> = ({
  filter,
}) => {
  const toggleOption = (value: unknown) => {
    filter.onChange?.(value == filter.selectedValue ? undefined : value)
  }

  return (
    <div>
      <PopoverMenu
        autoClose="none"
        shadow="2"
        position="bottom"
        align="end"
        minPanelWidth={filter?.layout?.minWidth}
        control={<TableFilterControl />}
        items={
          filter.options?.map(
            (o): PopoverMenuItem => ({
              content: (
                <div
                  className={classNames(
                    styles.content,
                    filter.selectedValue == o.value
                      ? styles.checked
                      : styles.unchecked
                  )}
                >
                  <CheckIcon width={12} className={classNames(styles.icon)} />
                  <p className={styles.text}>{o.label}</p>
                </div>
              ),
              onClick: () => toggleOption(o.value),
              className: styles.item,
            })
          )!
        }
      />
    </div>
  )
}

export default TableFilterSingleSelect
