import { classNames } from "@punks/ui-core"
import styles from "./DropMenu.module.css"
import { useEffect, useRef, useState } from "react"
import { Button } from "../../atoms/Button"

export type DropMenuSize = "sm" | "md" | "lg" | "auto"
export type DropMenuPanelAlignment = "start" | "end"

export type DropMenuItem = {
  label: React.ReactNode
  onClick: () => void
}

export interface DropMenuProps {
  fullWidth?: boolean
  size?: DropMenuSize
  items: DropMenuItem[]
  children: React.ReactNode
  iconOnly?: boolean
  rounded?: boolean
  className?: string
  alignment?: DropMenuPanelAlignment
}

const DropMenu = ({
  fullWidth,
  size = "md",
  alignment = "start",
  items,
  children,
  iconOnly,
  rounded,
  className,
}: DropMenuProps) => {
  const [isOpen, setIsOpen] = useState(false)
  const panelRef = useRef<HTMLDivElement | null>(null)

  const toggleDropdown = () => {
    setIsOpen(!isOpen)
  }

  const handleItemClick = (item: DropMenuItem) => {
    item.onClick?.()
    setIsOpen(false)
  }

  const handleOutsideClick = (event: MouseEvent) => {
    if (panelRef.current && !panelRef.current.contains(event.target as Node)) {
      setIsOpen(false)
    }
  }

  useEffect(() => {
    window.addEventListener("click", handleOutsideClick)
    return () => {
      window.removeEventListener("click", handleOutsideClick)
    }
  }, [])

  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.fullWidth]: fullWidth,
        },
        className
      )}
      ref={panelRef}
    >
      <Button
        variant="text"
        fullWidth={fullWidth}
        iconOnly={iconOnly}
        rounded={rounded}
        onClick={toggleDropdown}
      >
        {children}
      </Button>
      {isOpen && (
        <div
          className={classNames(
            styles.items,
            styles[`itemsAlign-${alignment}`],
            styles[`itemsSize-${size}`]
          )}
        >
          {items.map((item, index) => (
            <div key={index} className={classNames(styles.item)}>
              <Button
                onClick={() => handleItemClick(item)}
                variant="text"
                fullWidth
              >
                {item.label}
              </Button>
            </div>
          ))}
        </div>
      )}
    </div>
  )
}

export default DropMenu
