import type { Meta, StoryObj } from "@storybook/react"

import { DropMenu } from "."
import { ContextMenuIcon } from "../../ui/icons"

const meta = {
  title: "Ui/Molecules/DropMenu",
  component: DropMenu,
  tags: ["autodocs"],
  args: {
    children: "Click me",
    items: [
      {
        label: "Item 1",
        onClick: () => console.log("click 1"),
      },
      {
        label: "Item 2",
        onClick: () => console.log("click 2"),
      },
      {
        label: "Item 3 with a very long label",
        onClick: () => console.log("click 3"),
      },
    ],
  },
} satisfies Meta<typeof DropMenu>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {}

export const Small: Story = {
  args: {
    size: "sm",
  },
}

export const Large: Story = {
  args: {
    size: "lg",
  },
}

export const Auto: Story = {
  args: {
    size: "auto",
  },
}

export const IconOnly: Story = {
  args: {
    iconOnly: true,
    children: <ContextMenuIcon width={20} />,
  },
}

export const IconOnlyRounded: Story = {
  args: {
    iconOnly: true,
    rounded: true,
    children: <ContextMenuIcon width={20} />,
  },
}

export const IconOnlyRoundedAuto: Story = {
  args: {
    iconOnly: true,
    rounded: true,
    children: <ContextMenuIcon width={20} />,
    size: "auto",
  },
}
