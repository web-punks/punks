import type { Meta, StoryObj } from "@storybook/react"
import { Tabs } from "./Tabs"

const meta = {
  title: "UI/Molecules/Tabs",
  component: Tabs,
  args: {
    tabs: [
      {
        head: "Tab 1",
        content: "Tab 1 content",
      },
      {
        head: "Tab 2",
        content: "Tab 2 content",
      },
      {
        head: "Tab 3",
        content: "Tab 3 content",
      },
    ],
  },
} satisfies Meta<typeof Tabs>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
}

export const FullWidth: Story = {
  args: {
    fullWidth: true,
  },
}

export const WithErrors: Story = {
  args: {
    fullWidth: true,
    tabs: [
      {
        head: "Tab 1",
        content: "Tab 1 content",
        error: true,
      },
      {
        head: "Tab 2",
        content: "Tab 2 content",
        error: true,
      },
      {
        head: "Tab 3",
        content: "Tab 3 content",
        error: true,
      },
    ],
  },
}
