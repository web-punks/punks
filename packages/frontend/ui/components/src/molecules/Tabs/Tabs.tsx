import { classNames } from "@punks/ui-core"
import React from "react"
import styles from "./Tabs.module.css"
import { ErrorIcon, WarningIcon } from "../../ui/icons"

export interface TabItem {
  head: React.ReactNode
  content: React.ReactNode
  hidden?: boolean
  error?: boolean
}

export interface TabItemClasses {
  root?: string
  head?: string
  tab?: string
  content?: string
}

export interface TabsProps {
  className?: string
  tabs: TabItem[]
  defaultActiveTabIndex?: number
  center?: boolean
  classes?: TabItemClasses
  fullWidth?: boolean
  jsHidden?: boolean
  hiddenIfSingle?: boolean
  activeTabIndex?: number
  onTabChange?: (index: number) => void
}

export const Tabs = ({
  className,
  classes,
  center,
  tabs,
  defaultActiveTabIndex = 0,
  fullWidth,
  jsHidden,
  hiddenIfSingle,
  activeTabIndex: controlledActiveTabIndex,
  onTabChange,
}: TabsProps) => {
  const [activeTabIndex, setActiveTabIndex] = React.useState(
    defaultActiveTabIndex
  )

  const visibleTabs = tabs.filter((x) => x.hidden !== true)

  if (hiddenIfSingle && visibleTabs.length === 1) {
    return (
      <div className={classNames(styles.root, classes?.root, className)}>
        {visibleTabs[0].content}
      </div>
    )
  }

  const handleTabChange = (index: number) => {
    setActiveTabIndex(index)
    onTabChange?.(index)
  }

  const isTabActive = (index: number) => {
    return (controlledActiveTabIndex ?? activeTabIndex) === index
  }

  return (
    <div className={classNames(styles.root, classes?.root, className)}>
      <div
        className={classNames(classes?.head, styles.tabs, {
          [styles.center]: center,
        })}
      >
        {visibleTabs.map((x, i) => (
          <div
            className={classNames(
              styles.tab,
              {
                [styles.tabSelected]: isTabActive(i),
                [styles.tabFullWidth]: fullWidth,
                [styles.error]: x.error,
              },
              classes?.tab
            )}
            key={i}
            onClick={() => handleTabChange(i)}
          >
            {x.head}
            {x.error && <WarningIcon className={styles.errorIcon} />}
          </div>
        ))}
      </div>
      <div>
        {tabs
          .filter((x) => x.hidden !== true)
          .map((x, i) => {
            return (
              <div
                key={i}
                className={classNames({
                  [styles.hidden]: !isTabActive(i),
                })}
              >
                {(!jsHidden || isTabActive(i)) && (
                  <div
                    className={classNames(styles.tabContent, classes?.content)}
                  >
                    {x.content}
                  </div>
                )}
              </div>
            )
          })}
      </div>
    </div>
  )
}
