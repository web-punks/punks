import type { Meta, StoryObj } from "@storybook/react"
import * as yup from "yup"

import { DateInput } from "."
import { useForm } from "react-hook-form"
import { yupResolver } from "@hookform/resolvers/yup"
import { Box } from "../../structure"
import { Button } from "../../atoms"

const meta = {
  title: "Ui/Molecules/DateInput",
  component: DateInput,
  argTypes: {},
  tags: ["autodocs"],
} satisfies Meta<typeof DateInput>
export default meta

type Story = StoryObj<typeof meta>

export const Outlined: Story = {
  args: {
    variant: "outlined",
    placeholder: "Placeholder",
  },
}

export const Filled: Story = {
  args: {
    variant: "filled",
    placeholder: "Placeholder",
  },
}

export const WithDefaultValue: Story = {
  args: {
    variant: "outlined",
    placeholder: "Placeholder",
    defaultValue: "2021-01-02",
  },
}

export const WithReactHookFormAndDefaultValue: Story = {
  args: {},
  decorators: [
    (Story, props) => {
      const schema = yup.object().shape({
        value: yup.string().required(),
      })
      const {
        register,
        handleSubmit,
        formState: { errors },
      } = useForm({
        resolver: yupResolver(schema),
        defaultValues: {
          value: "2021-01-02",
        },
      })
      return (
        <div>
          <Story
            args={{
              ...props.args,
              ...register("value"),
            }}
          />
          <Box mt={2}>
            <Button
              variant="filled"
              color="primary"
              onClick={handleSubmit((data) => console.log("submit data", data))}
            >
              submit
            </Button>
          </Box>
        </div>
      )
    },
  ],
}
