import type { Meta, StoryObj } from "@storybook/react"
import MultiDropdowns, { MultiDropdownOption } from "./MultiDropdowns"
import { range } from "@punks/ui-core"

const meta: Meta = {
  title: "Ui/Molecules/MultiDropdowns",
  component: MultiDropdowns,
  tags: ["autodocs"],
  args: {
    optionGroups: [
      {
        id: "group1",
        label: "Group1",
        options: [
          { label: "Value 1", value: "value1" },
          { label: "Value 2", value: "value2" },
        ],
      },
      {
        id: "group2",
        label: "Group2",
        options: [
          { label: "Value 3", value: "value3" },
          { label: "Value 4", value: "value4" },
        ],
      },
      {
        id: "group3",
        label: "Group3",
        options: [
          { label: "Value 5", value: "value5" },
          { label: "Value 6", value: "value6" },
        ],
      },
    ],
    placeholder: "Select some items",
  },
} satisfies Meta<typeof MultiDropdowns>
export default meta

type Story = StoryObj<typeof meta>

export const Outlined: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
  },
}

export const OutlinedWithoutSeparator: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    layout: {
      noGroupSeparator: true,
    },
  },
}

export const OutlinedWithMaxHeight: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    maxPanelHeight: 120,
  },
}

export const OutlinedWithManyGroups: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    optionGroups: range(100).map((x) => ({
      id: `group${x}`,
      label: `Group ${x}`,
      options: [
        { label: `Value ${x}.1`, value: `value${x}.1` },
        { label: `Value ${x}.2`, value: `value${x}.2` },
        { label: `Value ${x}.3`, value: `value${x}.3` },
      ],
    })),
  },
}

export const OutlinedWithDisabledOptions: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    optionGroups: range(5).map((x) => ({
      id: `group${x}`,
      label: `Group ${x}`,
      options: [
        { label: `Value ${x}.1`, value: `value${x}.1` },
        { label: `Value ${x}.2`, value: `value${x}.2`, disabled: true },
        {
          label: `Value ${x}.3`,
          value: `value${x}.3`,
          disabled: true,
          checked: true,
        },
      ],
    })),
  },
}

export const OutlinedSorted: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    tagsSortFn(a: MultiDropdownOption, b: MultiDropdownOption) {
      return a.label.localeCompare(b.label)
    },
  },
}

export const OutlinedEmptyGroups: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    optionGroups: [],
    emptyGroupsContent: "No groups",
    emptyOptionsContent: "No options",
  },
}

export const OutlinedEmptyOptions: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    optionGroups: [
      {
        id: "group1",
        label: "Group 1",
        options: [],
      },
      {
        id: "group2",
        label: "Group 2",
        options: [],
      },
      {
        id: "group3",
        label: "Group 3",
        options: [],
      },
    ],
    emptyGroupsContent: "No groups",
    emptyOptionsContent: "No options",
  },
}

export const OutlinedDefaultExpanded: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    defaultExpanded: true,
  },
}

export const OutlinedWithDefaultValue: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    defaultExpanded: true,
    defaultValue: ["value1", "value2"],
  },
}

export const OutlinedWithValue: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    defaultExpanded: true,
    value: ["value1", "value2"],
  },
}
