import React, { useState, useRef, useMemo, ReactNode } from "react"
import { ChevronDown } from "../../ui/icons"
import styles from "./MultiDropdowns.module.css"
import { classNames, normalizePxSize, setCssVar } from "@punks/ui-core"
import { Checkbox } from "../../atoms/Checkbox"
import { Tag } from "../../atoms/Tag"

export type MultiDropdownSize = "sm" | "md" | "lg"
export type MultiDropdownVariant = "filled" | "outlined" | "underlined"

export type MultiDropdownClasses = {
  root?: string
  input?: string
  placeholder?: string
  filter?: string
  tags?: string
  tag?: string
  options?: string
  option?: string
  emptyOptionsContent?: string
  emptyGroupsContent?: string
}

export type MultiDropdownOption = {
  value: string
  label: string
  disabled?: boolean
  checked?: boolean
}

export type MultiDropdownOptionGroup = {
  id: string
  label: string
  options: MultiDropdownOption[]
}

export type MultiDropdownLayoutOptions = {
  noGroupSeparator?: boolean
}

export interface MultiDropdownsProps {
  optionGroups: MultiDropdownOptionGroup[]
  className?: string
  placeholder?: string
  maxPanelHeight?: number
  size?: MultiDropdownSize
  variant?: MultiDropdownVariant
  rounded?: boolean
  fullWidth?: boolean
  error?: boolean
  disabled?: boolean
  onChange?: (value: string[]) => void
  onValuesChange?: (selected: MultiDropdownOption[]) => void
  tagsSortFn?: (a: MultiDropdownOption, b: MultiDropdownOption) => number
  controlled?: boolean
  value?: string[]
  defaultValue?: string[]
  layout?: MultiDropdownLayoutOptions
  classes?: MultiDropdownClasses
  emptyOptionsContent?: ReactNode
  emptyGroupsContent?: ReactNode
  defaultExpanded?: boolean
  outline?: "none"
}

const getOptions = (
  values: string[],
  optionGroups: MultiDropdownOptionGroup[]
) =>
  optionGroups.flatMap((x) => x.options).filter((x) => values.includes(x.value))

const MultiDropdowns: React.FC<MultiDropdownsProps> = ({
  optionGroups,
  className,
  placeholder,
  maxPanelHeight = 400,
  rounded,
  size = "md",
  variant = "outlined",
  fullWidth,
  error,
  disabled,
  onChange,
  onValuesChange,
  tagsSortFn,
  defaultValue,
  value,
  layout,
  classes,
  controlled,
  emptyGroupsContent,
  emptyOptionsContent,
  defaultExpanded = false,
  outline,
}) => {
  const defaultOptions = useMemo(
    () => (defaultValue ? getOptions(defaultValue, optionGroups) : undefined),
    [optionGroups, defaultValue]
  )

  const [uncontrolledSelectedOptions, setUncontrolledSelectedOptions] =
    useState<MultiDropdownOption[]>(defaultOptions || [])

  const selectedOptions = useMemo(
    () =>
      value || controlled
        ? getOptions(value ?? [], optionGroups) ?? []
        : uncontrolledSelectedOptions,
    [value, controlled, optionGroups, uncontrolledSelectedOptions]
  )

  const [expandedGroups, setExpandedGroups] = useState<{
    [key: string]: boolean
  }>({})

  const isGroupExpanded = (groupId: string) =>
    expandedGroups[groupId] ?? defaultExpanded

  const rootRef = useRef<HTMLDivElement | null>(null)
  const [isOpen, setIsOpen] = useState<boolean>(false)

  const togglePanel = () => {
    setIsOpen(!isOpen)
  }

  const handleOutsideClick = (event: MouseEvent) => {
    if (rootRef.current && !rootRef.current.contains(event.target as Node)) {
      setIsOpen(false)
    }
  }

  React.useEffect(() => {
    window.addEventListener("click", handleOutsideClick)
    return () => {
      window.removeEventListener("click", handleOutsideClick)
    }
  }, [])

  const isSelected = (option: MultiDropdownOption) =>
    selectedOptions?.some((selected) => selected.value === option.value)

  const handleOptionToggle = (option: MultiDropdownOption) => {
    const updatedSelectedOptions = isSelected(option)
      ? selectedOptions.filter((selected) => selected.value !== option.value)
      : [...selectedOptions, option]

    setUncontrolledSelectedOptions(updatedSelectedOptions)
    onChange?.(updatedSelectedOptions.map((x) => x.value))
    onValuesChange?.(updatedSelectedOptions)
  }

  const handleTagRemove = (
    e: React.MouseEvent,
    option: MultiDropdownOption
  ) => {
    e.stopPropagation()
    handleOptionToggle(option)
  }

  const toggleGroup = (groupId: string) => {
    setExpandedGroups((prevExpandedGroups) => ({
      ...prevExpandedGroups,
      [groupId]: !isGroupExpanded(groupId),
    }))
  }

  const renderOptions = (group: MultiDropdownOptionGroup) => {
    const isExpanded = isGroupExpanded(group.id)
    return (
      <div
        key={group.id}
        className={classNames(styles.group, {
          [styles.groupsSeparator]: !layout?.noGroupSeparator,
        })}
      >
        <div
          className={styles.groupLabel}
          onClick={() => toggleGroup(group.id)}
        >
          <ChevronDown
            className={classNames(styles.chevron, {
              [styles.chevronExpanded]: isExpanded,
            })}
          />
          {group.label}
        </div>
        {isExpanded && (
          <div>
            {(group.options?.length ?? 0) === 0 && (
              <div
                className={classNames(
                  styles.emptyOptionsContent,
                  classes?.emptyOptionsContent
                )}
              >
                {emptyOptionsContent}
              </div>
            )}
            {group.options?.map((option) => (
              <div
                key={option.label}
                className={classNames(
                  styles.option,
                  {
                    [styles.optionDisabled]: option.disabled,
                    [styles.optionSelected]: isSelected(option),
                  },
                  classes?.option
                )}
                onClick={() => !option.disabled && handleOptionToggle(option)}
              >
                <Checkbox
                  checked={isSelected(option) || option.checked}
                  disabled={option.disabled}
                  size="large"
                  readOnly
                />
                <span>{option?.label}</span>
              </div>
            ))}
          </div>
        )}
      </div>
    )
  }

  const sortedTags = useMemo(() => {
    return tagsSortFn ? selectedOptions.sort(tagsSortFn) : selectedOptions
  }, [selectedOptions, tagsSortFn])

  return (
    <div
      ref={rootRef}
      className={classNames(
        styles.root,
        styles[variant],
        styles[`${variant}-${size}`],
        {
          [styles.fullWidth]: fullWidth,
          [styles[`${variant}-rounded-${size}`]]: rounded,
          [styles.opened]: isOpen,
          [styles.error]: error,
          [styles.disabled]: disabled,
        },
        classes?.root,
        className
      )}
    >
      <div
        className={classNames(
          styles.input,
          styles[`input-${size}`],
          classes?.input
        )}
        onClick={togglePanel}
      >
        {sortedTags.length === 0 && (
          <span
            className={classNames(styles.placeholder, classes?.placeholder)}
          >
            {placeholder}
          </span>
        )}
        {sortedTags.length > 0 && (
          <div className={styles.tags}>
            {sortedTags.map((option) => (
              <Tag
                className={classNames(styles.tag, classes?.tag)}
                key={option.value}
                color="primary"
                onRemove={(e) => handleTagRemove(e, option)}
              >
                {option?.label}
              </Tag>
            ))}
          </div>
        )}
        <div className={styles.arrowContainer}>
          <ChevronDown
            className={classNames(styles.arrowIcon, {
              [styles.arrowIconExpanded]: isOpen,
            })}
          />
        </div>
      </div>
      {isOpen && (
        <div
          style={{
            ...setCssVar("--max-h", normalizePxSize(maxPanelHeight)),
          }}
          className={classNames(
            styles.options,
            styles[`options-${variant}`],
            {
              [styles.panelMaxHeight]: !!maxPanelHeight,
            },
            classes?.options
          )}
        >
          {optionGroups.length === 0 && (
            <div
              className={classNames(
                styles.emptyGroupsContent,
                classes?.emptyGroupsContent
              )}
            >
              {emptyGroupsContent}
            </div>
          )}
          {optionGroups.map(renderOptions)}
        </div>
      )}
    </div>
  )
}
export default MultiDropdowns
