import type { Meta, StoryObj } from "@storybook/react"

import ActionButton from "./ActionButton"
import { DeleteIcon } from "../../ui/icons"

const meta = {
  title: "Ui/Molecules/ActionButton",
  component: ActionButton,
  tags: ["autodocs"],
  argTypes: {},
} satisfies Meta<typeof ActionButton>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    children: <DeleteIcon width={18} />,
  },
}
