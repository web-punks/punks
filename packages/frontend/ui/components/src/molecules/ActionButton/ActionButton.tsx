import { classNames } from "@punks/ui-core"
import { Button } from "../../atoms/Button"
import styles from "./ActionButton.module.css"

export interface ActionButtonProps {
  children?: React.ReactNode
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
  disabled?: boolean
  className?: string
  loading?: boolean
  preventDefault?: boolean
}

const ActionButton = ({ children, className, ...other }: ActionButtonProps) => {
  return (
    <Button
      className={classNames(styles.root, className)}
      rounded
      iconOnly
      {...other}
    >
      {children}
    </Button>
  )
}

export default ActionButton
