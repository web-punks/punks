import { classNames } from "@punks/ui-core"
import { Spinner } from "../../animations/Spinner"
import styles from "./LoaderBar.module.css"

export interface LoaderBarProps {
  className?: string
}

const LoaderBar = ({ className }: LoaderBarProps) => {
  return (
    <div className={classNames(styles.root, className)}>
      <Spinner size="lg" />
    </div>
  )
}

export default LoaderBar
