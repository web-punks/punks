import { useEffect, useState } from "react"
import styles from "./ProgressBar.module.css"
import { Typography } from "../../atoms/Typography"
import {
  ThemeColor,
  classNames,
  normalizePxSize,
  setColorVar,
  setColorVarContrast,
  setColorVarDark,
  setCssVar,
} from "@punks/ui-core"

export type ProgressBarSize = "small" | "medium" | "large"

type ProgressBarProgress =
  | {
      progress: number
      max: number
      indeterminate?: never
    }
  | {
      progress?: never
      max?: never
      indeterminate: true
    }

export type ProgressBarProps = ProgressBarProgress & {
  size?: ProgressBarSize
  height?: number
  label?: string
  color?: ThemeColor
  animated?: boolean
  animationDelay?: number
  gradient?: boolean
  className?: string
}

const ProgressBar = ({
  size = "medium",
  height,
  max,
  progress,
  indeterminate,
  label,
  color = "primary",
  animated = true,
  animationDelay = 250,
  gradient,
  className,
}: ProgressBarProps) => {
  const [showProgress, setShowProgress] = useState(false || !animated)
  useEffect(() => {
    if (animated) {
      setTimeout(() => {
        setShowProgress(true)
      }, animationDelay)
    }
  }, [])

  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.small]: size === "small",
          [styles.medium]: size === "medium",
          [styles.large]: size === "large",
          [styles.customHeight]: height !== undefined,
        },
        className
      )}
      style={{
        ...setCssVar("--bar-h", normalizePxSize(height)),
      }}
    >
      <div
        style={{
          ...(typeof progress !== "undefined" && typeof max !== "undefined"
            ? {
                ...setCssVar("--progress", `${(progress / max) * 100}%`),
              }
            : {}),
          ...setColorVar("--bar-color", color),
          ...setColorVarContrast("--bar-color-contrast", color),
          ...setColorVarDark("--bar-color-dark", color),
        }}
        className={classNames(styles.bar, {
          [styles.barFilled]: !gradient,
          [styles.barGradient]: gradient,
          [styles.barEmpty]: !showProgress,
          [styles.barProgress]: showProgress,
          [styles.barIndeterminate]: indeterminate,
        })}
      >
        {label && (
          <Typography align="right" mr={2}>
            {label}
          </Typography>
        )}
      </div>
    </div>
  )
}

export default ProgressBar
