import type { Meta, StoryObj } from "@storybook/react"

import { ProgressBar } from "."

const meta = {
  title: "Ui/Molecules/ProgressBar",
  component: ProgressBar,
  tags: ["autodocs"],
  argTypes: {},
} satisfies Meta<typeof ProgressBar>
export default meta

type Story = StoryObj<typeof meta>

export const ProgressBarSmall: Story = {
  args: {
    size: "small",
    progress: 0,
    max: 100,
  },
}

export const ProgressBarMedium: Story = {
  args: {
    size: "medium",
    progress: 0,
    max: 100,
  },
}

export const ProgressBarMediumGradient: Story = {
  args: {
    size: "medium",
    gradient: true,
    progress: 0,
    max: 100,
  },
}

export const ProgressBarMediumAnimated: Story = {
  args: {
    size: "medium",
    gradient: true,
    animated: true,
    progress: 70,
    max: 100,
  },
}

export const ProgressBarMediumAnimatedWithLabel: Story = {
  args: {
    size: "medium",
    gradient: true,
    animated: true,
    progress: 70,
    max: 100,
    label: "70%",
  },
}

export const ProgressBarLarge: Story = {
  args: {
    size: "large",
    progress: 0,
    max: 100,
  },
}

export const ProgressBarCustomH: Story = {
  args: {
    height: 10,
    gradient: true,
    animated: true,
    progress: 70,
    max: 100,
  },
}

export const ProgressBarIndeterminate: Story = {
  args: {
    gradient: true,
    animated: true,
    indeterminate: true,
  },
}
