import React from "react"
import { TextArea } from "../../atoms/TextArea"

const parseValues = (
  lines: string[],
  { trim }: { trim: boolean }
): string[] => {
  if (!trim) {
    return lines
  }

  return lines.map((x) => x.trim()).filter((x) => x)
}

export interface TextAreaMultilineInputProps {
  values: string[]
  onValuesChange: (values: string[]) => void
  minRows?: number
  trim?: boolean
}

const TextAreaMultilineInput = ({
  onValuesChange,
  values,
  minRows,
  trim = false,
}: TextAreaMultilineInputProps) => {
  const [rawValue, setRawValue] = React.useState<string>(
    values?.join("\n") ?? ""
  )
  const handleValueChange = (value: string) => {
    setRawValue(value)
    onValuesChange(
      parseValues(value.split("\n"), {
        trim,
      })
    )
  }

  return (
    <TextArea
      rows={minRows}
      value={rawValue}
      onChange={(e) => handleValueChange(e.target.value ?? "")}
    />
  )
}

export default TextAreaMultilineInput
