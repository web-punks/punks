import {
  ThemeColor,
  classNames,
  normalizePxSize,
  setCssVar,
} from "@punks/ui-core"
import styles from "./DropButton.module.css"
import { Button } from "../../atoms/Button"
import { useEffect, useRef, useState } from "react"

export type DropButtonSize = "sm" | "md" | "lg" | "auto"
export type DropButtonVariant = "filled" | "outlined" | "text"
export type DropButtonPanelAlignment = "start" | "end"

export type DropButtonItem = {
  label: React.ReactNode
  onClick: () => void
  className?: string
}

export type DropButtonClasses = {
  root?: string
  items?: string
  item?: string
}

export interface DropButtonProps {
  fullWidth?: boolean
  color?: ThemeColor
  size?: DropButtonSize
  variant?: DropButtonVariant
  items: DropButtonItem[]
  children: React.ReactNode
  rounded?: boolean
  className?: string
  classes?: DropButtonClasses
  alignment?: DropButtonPanelAlignment
  iconOnly?: boolean
  width?: string | number
  panelZIndex?: number
  loading?: boolean
  disabled?: boolean
}

const DropButton = ({
  color = "primary",
  fullWidth,
  size = "md",
  variant = "filled",
  alignment = "start",
  items,
  iconOnly,
  children,
  rounded,
  className,
  classes,
  width,
  panelZIndex,
  loading,
  disabled,
}: DropButtonProps) => {
  const [isOpen, setIsOpen] = useState(false)
  const panelRef = useRef<HTMLDivElement | null>(null)

  const toggleDropdown = (e: MouseEvent) => {
    e.preventDefault()
    setIsOpen(!isOpen)
  }

  const handleItemClick = (e: MouseEvent, item: DropButtonItem) => {
    e.preventDefault()
    item.onClick?.()
    setIsOpen(false)
  }

  const handleOutsideClick = (event: MouseEvent) => {
    if (panelRef.current && !panelRef.current.contains(event.target as Node)) {
      setIsOpen(false)
    }
  }

  useEffect(() => {
    window.addEventListener("click", handleOutsideClick)
    return () => {
      window.removeEventListener("click", handleOutsideClick)
    }
  }, [])

  return (
    <div
      style={{
        ...(width ? setCssVar("--w", normalizePxSize(width)) : {}),
      }}
      className={classNames(
        styles.root,
        styles[variant],
        styles[`${variant}-${size}`],
        {
          [styles.fullWidth]: fullWidth,
          [styles.fixedWith]: !!width,
          [styles[`${variant}-rounded-${size}`]]: rounded,
        },
        classes?.root,
        className
      )}
      ref={panelRef}
    >
      <Button
        color={color}
        fullWidth={fullWidth || !!width}
        size={size !== "auto" ? size : undefined}
        variant={variant}
        onClick={(e) => toggleDropdown(e as any)}
        iconOnly={iconOnly}
        rounded={rounded}
        loading={loading}
        disabled={disabled}
      >
        {children}
      </Button>
      {isOpen && (
        <div
          style={{ ...setCssVar("--panel-z", panelZIndex ?? 2) }}
          className={classNames(
            styles.items,
            styles[`itemsAlign-${alignment}`],
            {
              [styles[`itemsSize-${size}`]]: !width,
              [styles.itemsFixed]: !!width,
            },
            classes?.items
          )}
        >
          {items.map((item, index) => (
            <div
              key={index}
              className={classNames(styles.item, classes?.item, item.className)}
            >
              <Button
                onClick={(e) => handleItemClick(e as any, item)}
                variant="text"
                fullWidth
              >
                {item.label}
              </Button>
            </div>
          ))}
        </div>
      )}
    </div>
  )
}

export default DropButton
