import type { Meta, StoryObj } from "@storybook/react"

import { DropButton } from "."
import { ContextMenuIcon } from "../../ui/icons"

const meta = {
  title: "Ui/Molecules/DropButton",
  component: DropButton,
  tags: ["autodocs"],
  args: {
    children: "Click me",
    items: [
      {
        label: "Item 1",
        onClick: () => console.log("click 1"),
      },
      {
        label: "Item 2",
        onClick: () => console.log("click 2"),
      },
      {
        label: "Item 3 with a very long label",
        onClick: () => console.log("click 3"),
      },
    ],
  },
} satisfies Meta<typeof DropButton>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {}

export const FixedWith: Story = {
  args: {
    width: 300,
  },
}

export const AlignEnd: Story = {
  args: {
    alignment: "end",
  },
}

export const IconOnly: Story = {
  args: {
    variant: "text",
    children: <ContextMenuIcon width={20} />,
    iconOnly: true,
  },
}

export const IconOnlyRounded: Story = {
  args: {
    variant: "text",
    children: <ContextMenuIcon width={20} />,
    iconOnly: true,
    rounded: true,
  },
}

export const IconOnlyRoundedAuto: Story = {
  args: {
    variant: "text",
    children: <ContextMenuIcon width={20} />,
    iconOnly: true,
    rounded: true,
    size: "auto",
  },
}

export const IconOnlyRoundedAutoEnd: Story = {
  args: {
    variant: "text",
    children: <ContextMenuIcon width={20} />,
    iconOnly: true,
    rounded: true,
    size: "auto",
    alignment: "end",
  },
  decorators: [
    (Story) => (
      <div style={{ marginLeft: 200 }}>
        <Story />{" "}
      </div>
    ),
  ],
}
