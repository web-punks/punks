import React, { useEffect, useState } from "react"
import styles from "./DateSelect.module.css"
import {
  classNames,
  normalizePxSize,
  setCssVar,
  useDetectClickOutside,
} from "@punks/ui-core"
import { CalendarIcon, CloseIcon } from "../../ui/icons"
import { DateValue } from "./types"
import {
  calculateAvailableDays,
  calculateAvailableMonths,
  calculateAvailableYears,
  changeDateDay,
  changeDateMonth,
  changeDateYear,
  formatIsoDate,
  getMonthName,
  parseDate,
} from "./utils"
import { TextInput } from "../../atoms/TextInput"
import { Button } from "../../atoms/Button"

export type DateSelectSize = "sm" | "md" | "lg"
export type DateSelectVariant = "filled" | "outlined"
export type DateSelectPosition = "top" | "bottom"

export interface DateSelectProps
  extends Omit<
    React.InputHTMLAttributes<HTMLInputElement>,
    "size" | "onChange"
  > {
  inputRef?: React.Ref<HTMLInputElement>
  fullWidth?: boolean
  size?: DateSelectSize
  variant?: DateSelectVariant
  startAdornment?: React.ReactNode
  endAdornment?: React.ReactNode
  controlled?: boolean
  error?: boolean
  confirmLabel: string
  onChange?: (value: string | undefined) => void
  onValueChange?: (value: DateValue | undefined) => void
  allowClear?: boolean
  showCalendarIcon?: boolean
  calendarIcon?: React.ReactNode
  closeIcon?: React.ReactNode
  maxPanelHeight?: number
  minDate?: DateValue
  maxDate?: DateValue
  locale?: string
  position?: DateSelectPosition
}

const DateSelect = ({
  className,
  inputRef,
  fullWidth,
  size = "md",
  variant = "outlined",
  startAdornment,
  endAdornment,
  error,
  disabled,
  controlled,
  confirmLabel,
  onChange,
  onValueChange,
  allowClear,
  showCalendarIcon = true,
  calendarIcon,
  closeIcon,
  value,
  defaultValue,
  maxPanelHeight = 300,
  minDate = {
    day: 1,
    month: 1,
    year: 1900,
  },
  maxDate = {
    day: 31,
    month: 12,
    year: 2100,
  },
  locale,
  position = "bottom",
  ...props
}: DateSelectProps) => {
  const [panelOpen, setPanelOpen] = useState(false)

  const togglePanel = () => {
    setPanelOpen(!panelOpen)
  }

  const closePanel = () => {
    setPanelOpen(false)
  }

  const ref = useDetectClickOutside<HTMLDivElement>({
    onTriggered: () => {
      setPanelOpen(false)
    },
  })

  const daysRef = React.useRef<HTMLDivElement>(null)
  const monthsRef = React.useRef<HTMLDivElement>(null)
  const yearsRef = React.useRef<HTMLDivElement>(null)

  const isControlled = controlled || value !== undefined
  const [uncontrolledDate, setUncontrolledDate] = React.useState<
    DateValue | undefined
  >(
    value || defaultValue
      ? parseDate((value ?? defaultValue) as string)
      : undefined
  )

  const selectedDate = isControlled
    ? parseDate(value as string)
    : uncontrolledDate

  const days = calculateAvailableDays({
    selectedDate,
    maxDate,
    minDate,
  })

  const months = calculateAvailableMonths({
    selectedDate,
    maxDate,
    minDate,
  })

  const years = calculateAvailableYears({
    maxDate,
    minDate,
  })

  const ensureDayVisible = (day: number) => {
    if (daysRef.current) {
      const dayCell = daysRef.current.querySelector(
        `[data-day="${day}"]`
      ) as HTMLDivElement
      if (dayCell) {
        dayCell.scrollIntoView({ block: "nearest" })
      }
    }
  }

  const ensureMonthVisible = (month: number) => {
    if (monthsRef.current) {
      const monthCell = monthsRef.current.querySelector(
        `[data-month="${month}"]`
      ) as HTMLDivElement
      if (monthCell) {
        monthCell.scrollIntoView({ block: "nearest" })
      }
    }
  }

  const ensureYearVisible = (year: number) => {
    if (yearsRef.current) {
      const yearCell = yearsRef.current.querySelector(
        `[data-year="${year}"]`
      ) as HTMLDivElement
      if (yearCell) {
        yearCell.scrollIntoView({ block: "nearest" })
      }
    }
  }

  useEffect(() => {
    if (panelOpen) {
      ensureDayVisible(selectedDate?.day ?? 1)
      ensureMonthVisible(selectedDate?.month ?? 1)
      ensureYearVisible(selectedDate?.year ?? new Date().getFullYear())
    }
  }, [panelOpen])

  const handleDateChange = (value?: DateValue) => {
    onChange?.(value ? formatIsoDate(value) : undefined)
    onValueChange?.(value)
  }

  const selectMonth = (month: number) => {
    const newDate = changeDateMonth(month, {
      maxDate,
      minDate,
      selectedDate,
    })
    setUncontrolledDate(newDate)
    handleDateChange(newDate)
    ensureYearVisible(newDate.year)
    ensureDayVisible(newDate.day)
  }

  const selectDay = (day: number) => {
    const newDate = changeDateDay(day, {
      maxDate,
      minDate,
      selectedDate,
    })
    setUncontrolledDate(newDate)
    handleDateChange(newDate)
    ensureYearVisible(newDate.year)
    ensureMonthVisible(newDate.day)
  }

  const selectYear = (year: number) => {
    const newDate = changeDateYear(year, {
      maxDate,
      minDate,
      selectedDate,
    })
    setUncontrolledDate(newDate)
    handleDateChange(newDate)
    ensureMonthVisible(newDate.day)
    ensureDayVisible(newDate.day)
  }

  const handleClear = () => {
    setUncontrolledDate(undefined)
    onChange?.(undefined)
    onValueChange?.(undefined)
    handleDateChange(undefined)
    closePanel()
  }

  const isDaySelected = (day: number) => day === selectedDate?.day
  const isMonthSelected = (month: number) => month === selectedDate?.month
  const isYearSelected = (year: number) => year === selectedDate?.year

  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.fullWidth]: fullWidth,
        },
        className
      )}
      style={{
        ...setCssVar(
          "--wp-theme-panel-max-height",
          normalizePxSize(maxPanelHeight)
        ),
      }}
      ref={ref}
    >
      <TextInput
        error={error}
        value={[
          selectedDate?.day?.toString().padStart(2, "0") ?? "__",
          selectedDate?.month?.toString().padStart(2, "0") ?? " __",
          selectedDate?.year?.toString().padStart(4, "0") ?? " ____",
        ].join("/")}
        readOnly
        width={props.width ?? fullWidth ? undefined : 145}
        inputRef={inputRef}
        onClick={togglePanel}
        fullWidth={fullWidth}
        endAdornment={
          allowClear && selectedDate ? (
            <div className={styles.closeIcon} onClick={handleClear}>
              {closeIcon ?? <CloseIcon />}
            </div>
          ) : (
            showCalendarIcon && (
              <div className={styles.calendarIcon} onClick={togglePanel}>
                {calendarIcon ?? <CalendarIcon width={15} />}
              </div>
            )
          )
        }
        {...props}
      />
      {panelOpen && (
        <div
          className={classNames(styles.panel, {
            [styles.panelTop]: position === "top",
            [styles.panelBottom]: position === "bottom",
          })}
        >
          <div className={styles.panelTracks}>
            <div className={styles.panelTracksRange} ref={daysRef}>
              {days.map((day) => (
                <div key={day} className={styles.panelRangeCell} data-day={day}>
                  <Button
                    size="sm"
                    onClick={() => selectDay(day)}
                    variant="filled"
                    color={isDaySelected(day) ? "primary" : "transparent"}
                  >
                    {day.toString().padStart(2, "0")}
                  </Button>
                </div>
              ))}
            </div>
            <div className={styles.panelTracksRange} ref={monthsRef}>
              {months.map((month) => (
                <div
                  key={month}
                  className={styles.panelRangeCell}
                  data-month={month}
                >
                  <Button
                    size="sm"
                    onClick={() => selectMonth(month)}
                    variant="filled"
                    color={isMonthSelected(month) ? "primary" : "transparent"}
                    fullWidth
                  >
                    {getMonthName(month, locale ?? navigator.language)}
                  </Button>
                </div>
              ))}
            </div>
            <div className={styles.panelTracksRange} ref={yearsRef}>
              {years.map((year) => (
                <div
                  key={year}
                  className={styles.panelRangeCell}
                  data-year={year}
                >
                  <Button
                    size="sm"
                    onClick={() => selectYear(year)}
                    variant="filled"
                    color={isYearSelected(year) ? "primary" : "transparent"}
                  >
                    {year.toString().padStart(4, "0")}
                  </Button>
                </div>
              ))}
            </div>
          </div>
          <div className={styles.panelFooter}>
            <Button
              size="sm"
              fullWidth
              color="primary"
              variant="filled"
              disabled={!selectedDate}
              onClick={closePanel}
            >
              {confirmLabel}
            </Button>
          </div>
        </div>
      )}
    </div>
  )
}

export default DateSelect

export const RefDateSelect = React.forwardRef<
  HTMLInputElement,
  DateSelectProps
>((props, ref) => <DateSelect {...props} inputRef={ref ?? props.inputRef} />)
