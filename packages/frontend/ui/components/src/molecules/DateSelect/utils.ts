import { range, toTitleCase } from "@punks/ui-core"
import { DateValue } from "./types"

export const getMonthName = (month: number, locale: string) => {
  return toTitleCase(
    new Date(2000, month - 1, 1).toLocaleString(locale, {
      month: "long",
    })
  )
}

export const formatIsoDate = (value: DateValue) =>
  [
    value.year.toString(),
    value.month.toString().padStart(2, "0"),
    value.day.toString().padStart(2, "0"),
  ].join("-")

export const parseDate = (value: string): DateValue => {
  const parts = value.split("-")
  return {
    day: parseInt(parts[2]),
    month: parseInt(parts[1]),
    year: parseInt(parts[0]),
  }
}

const getMaxDaysInMonth = (month: any, year: any) => {
  return new Date(year, month, 0).getDate()
}

const calculateDaysInMonth = (month: number, year: number) => {
  return range(getMaxDaysInMonth(month, year)).map((day) => day + 1)
}

const months = range(12).map((month) => month + 1)

export const calculateAvailableDays = ({
  maxDate,
  minDate,
  selectedDate,
}: {
  selectedDate?: DateValue
  minDate?: DateValue
  maxDate?: DateValue
}) => {
  let availableDays = calculateDaysInMonth(
    selectedDate?.month ?? 1,
    selectedDate?.year ?? 1
  )

  if (minDate && minDate.year === selectedDate?.year) {
    availableDays = availableDays.filter((day) => day >= minDate.day)
  }

  if (maxDate && maxDate.year === selectedDate?.year) {
    availableDays = availableDays.filter((day) => day <= maxDate.day)
  }

  return availableDays
}

export const calculateAvailableMonths = ({
  maxDate,
  minDate,
  selectedDate,
}: {
  selectedDate?: DateValue
  minDate?: DateValue
  maxDate?: DateValue
}) => {
  let availableMonths = months

  if (minDate && minDate.year === selectedDate?.year) {
    availableMonths = availableMonths.filter((month) => month >= minDate.month)
  }

  if (maxDate && maxDate.year === selectedDate?.year) {
    availableMonths = availableMonths.filter((month) => month <= maxDate.month)
  }

  return availableMonths
}

export const calculateAvailableYears = ({
  minDate,
  maxDate,
}: {
  minDate: DateValue
  maxDate: DateValue
}) => {
  return range(maxDate.year - minDate.year + 1).map(
    (year) => year + minDate.year
  )
}

export const changeDateDay = (
  day: number,
  {
    maxDate,
    minDate,
    selectedDate,
  }: {
    selectedDate?: DateValue
    minDate?: DateValue
    maxDate?: DateValue
  }
) => {
  const newDate = {
    month: selectedDate?.month ?? 1,
    year: selectedDate?.year ?? new Date().getFullYear(),
    day,
  }

  return newDate
}

export const changeDateMonth = (
  month: number,
  {
    maxDate,
    minDate,
    selectedDate,
  }: {
    selectedDate?: DateValue
    minDate?: DateValue
    maxDate?: DateValue
  }
): DateValue => {
  const newDate = {
    month,
    year: selectedDate?.year ?? new Date().getFullYear(),
    day: selectedDate?.day ?? 1,
  }

  const monthDays = calculateDaysInMonth(newDate.month, newDate.year)
  if (newDate.day > monthDays.length) {
    newDate.day = monthDays.length
  }

  return newDate
}

export const changeDateYear = (
  year: number,
  {
    maxDate,
    minDate,
    selectedDate,
  }: {
    selectedDate?: DateValue
    minDate?: DateValue
    maxDate?: DateValue
  }
) => {
  const newDate = {
    month: selectedDate?.month ?? 1,
    year,
    day: selectedDate?.day ?? 1,
  }

  const monthDays = calculateDaysInMonth(newDate.month, newDate.year)
  if (newDate.day > monthDays.length) {
    newDate.day = monthDays.length
  }

  return newDate
}
