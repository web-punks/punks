import type { Meta, StoryObj } from "@storybook/react"

import DateSelect from "./DateSelect"
import { useState } from "react"

const meta: Meta<typeof DateSelect> = {
  title: "Ui/Molecules/DateSelect",
  component: DateSelect,
  argTypes: {
    onChange: { action: "onChange" },
    inputRef: { table: { disable: true } },
  },
  tags: ["autodocs"],
} satisfies Meta<typeof DateSelect>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
  },
}

export const Error: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    error: true,
  },
}

export const PanelTop: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    position: "top",
  },
  decorators: [
    (Story) => (
      <div style={{ marginTop: 400 }}>
        <Story />
      </div>
    ),
  ],
}

export const PanelBottom: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    position: "bottom",
  },
}

export const FullWidth: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    fullWidth: true,
  },
}

export const Italian: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    locale: "it",
  },
}

export const AllowClear: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
  },
}

export const AllowClearWithoutCalendar: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
    showCalendarIcon: false,
  },
}

export const WithDefaultValue: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
    defaultValue: "2020-01-02",
  },
}

export const CustomHeight: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
    maxPanelHeight: 200,
  },
}

export const Controlled: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
  },
  render: (args) => {
    const [value, setValue] = useState<string | undefined>(undefined)
    return (
      <DateSelect
        {...args}
        value={value}
        onChange={(value) => {
          setValue(value)
        }}
      />
    )
  },
}
