import React from "react"
import styles from "./Card.module.css"
import {
  getPaddingClasses,
  getMarginClasses,
  getMarginStyles,
  getPaddingStyles,
} from "../../helpers/spacings"
import { classNames } from "@punks/ui-core"
import { ComponentMarginProps, ComponentPaddingProps } from "../../types"

export type CardFocus = "auto"
export type CardShadow = "1" | "2" | "3"

export interface CardProps extends ComponentMarginProps, ComponentPaddingProps {
  component?: keyof JSX.IntrinsicElements
  className?: string
  children?: any
  noGutters?: boolean
  noShadow?: boolean
  shadow?: CardShadow
  style?: React.CSSProperties
  focused?: boolean
  focus?: CardFocus
  focusPosition?: "top" | "bottom" | "left" | "right"
}

const Card = ({
  children,
  className,
  component,
  m,
  mb,
  ml,
  mr,
  mt,
  mx,
  my,
  p,
  pb,
  pl,
  pr,
  pt,
  px,
  py,
  noGutters,
  noShadow,
  shadow = "1",
  focused,
  focus,
  focusPosition = "left",
  style,
}: CardProps) => {
  const Tag = component ?? "div"
  const spacings = {
    mb,
    mt,
    ml,
    mr,
    mx,
    my,
    m,
    pb,
    pt,
    pl,
    pr,
    px,
    py,
    p,
  }
  return (
    <Tag
      style={{
        ...getMarginStyles(spacings),
        ...getPaddingStyles(spacings),
        ...style,
      }}
      className={classNames(
        styles.root,
        {
          [styles.gutters]: !noGutters && !p,
          [styles[`shadow-${shadow}`]]: !noShadow,
          [styles.focusX]:
            focusPosition === "left" || focusPosition === "right",
          [styles.focusedLeft]: focused && focusPosition === "left",
          [styles[`focusLeft-${focus}`]]: focus && focusPosition === "left",
          [styles.focusedRight]: focused && focusPosition === "right",
          [styles[`focusRight-${focus}`]]: focus && focusPosition === "right",
          [styles.focusY]:
            focusPosition === "top" || focusPosition === "bottom",
          [styles.focusedTop]: focused && focusPosition === "top",
          [styles[`focusTop-${focus}`]]: focus && focusPosition === "top",
          [styles.focusedBottom]: focused && focusPosition === "bottom",
          [styles[`focusBottom-${focus}`]]: focus && focusPosition === "bottom",
          ...getMarginClasses(styles, spacings),
          ...getPaddingClasses(styles, spacings),
        },
        className
      )}
    >
      {children}
    </Tag>
  )
}

export default Card
