import type { Meta, StoryObj } from "@storybook/react"

import { Card } from "."

const meta = {
  title: "Ui/Molecules/Card",
  component: Card,
  tags: ["autodocs"],
  argTypes: {},
} satisfies Meta<typeof Card>
export default meta

type Story = StoryObj<typeof meta>

export const CardDefault: Story = {
  args: {
    children: <div>content</div>,
  },
}

export const CardShadow2: Story = {
  args: {
    children: <div>content</div>,
    shadow: "2",
  },
}

export const CardShadow3: Story = {
  args: {
    children: <div>content</div>,
    shadow: "3",
  },
}

export const CardFocused: Story = {
  args: {
    focused: true,
    children: <div>content</div>,
  },
}

export const CardFocusAuto: Story = {
  args: {
    focus: "auto",
    children: <div>content</div>,
  },
}

export const CardFocusedRight: Story = {
  args: {
    focused: true,
    focusPosition: "right",
    children: <div>content</div>,
  },
}

export const CardFocusRightAuto: Story = {
  args: {
    focus: "auto",
    focusPosition: "right",
    children: <div>content</div>,
  },
}

export const CardFocusedTop: Story = {
  args: {
    focused: true,
    focusPosition: "top",
    children: <div>content</div>,
  },
}

export const CardFocusTopAuto: Story = {
  args: {
    focus: "auto",
    focusPosition: "top",
    children: <div>content</div>,
  },
}

export const CardFocusedBottom: Story = {
  args: {
    focused: true,
    focusPosition: "bottom",
    children: <div>content</div>,
  },
}

export const CardFocusBottomAuto: Story = {
  args: {
    focus: "auto",
    focusPosition: "bottom",
    children: <div>content</div>,
  },
}
