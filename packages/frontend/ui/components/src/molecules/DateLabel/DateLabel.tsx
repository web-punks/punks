import { Typography, TypographyProps } from "../../atoms/Typography"

export interface DateLabelProps extends Omit<TypographyProps, "children"> {
  value: string
}

const DateLabel = ({ value, ...props }: DateLabelProps) => {
  const p = value?.split("-")
  return (
    <Typography {...props}>
      {value ? `${p[2]}/${p[1]}/${p[0]}` : undefined}
    </Typography>
  )
}

export default DateLabel
