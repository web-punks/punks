import { classNames } from "@punks/ui-core"
import styles from "./DocumentListItem.module.css"
import { Typography } from "../../atoms/Typography"
import React from "react"

export interface DocumentListItemProps {
  icon?: React.ReactNode
  title?: React.ReactNode
  subtitle?: React.ReactNode
  className?: string
  control?: React.ReactNode
}

const DocumentListItem = ({
  icon,
  subtitle,
  title,
  className,
  control,
}: DocumentListItemProps) => {
  return (
    <div className={classNames(styles.root, className)}>
      {icon && <div className={styles.icon}>{icon}</div>}
      <div className={styles.content}>
        <Typography weight="bold">{title}</Typography>
        <Typography variant="caption2">{subtitle}</Typography>
      </div>
      <div className={styles.control}>{control}</div>
    </div>
  )
}

export default DocumentListItem
