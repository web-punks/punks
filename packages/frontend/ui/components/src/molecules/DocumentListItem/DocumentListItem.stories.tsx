import type { Meta, StoryObj } from "@storybook/react"
import { DocumentListItem } from "."

const meta = {
  title: "UI/Molecules/DocumentListItem",
  component: DocumentListItem,
  tags: ["autodocs"],
  args: {},
} satisfies Meta<typeof DocumentListItem>
export default meta

type Story = StoryObj<typeof meta>

const SearchIcon = () => (
  <svg
    width="15"
    height="15"
    viewBox="0 0 15 15"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M5.66667 10.3333C8.244 10.3333 10.3333 8.244 10.3333 5.66667C10.3333 3.08934 8.244 1 5.66667 1C3.08934 1 1 3.08934 1 5.66667C1 8.244 3.08934 10.3333 5.66667 10.3333Z"
      stroke="currentColor"
      strokeWidth="1.6"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.0001 13L9.00012 9"
      stroke="currentColor"
      strokeWidth="1.6"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
  </svg>
)

export const Default: Story = {
  args: {
    title: "Title",
    subtitle: "Subtitle",
    icon: <SearchIcon />,
  },
}
