import type { Meta, StoryObj } from "@storybook/react"

import TimeSelect from "./TimeSelect"
import { useState } from "react"

const meta: Meta<typeof TimeSelect> = {
  title: "Ui/Molecules/TimeSelect",
  component: TimeSelect,
  argTypes: {
    onChange: { action: "onChange" },
    inputRef: { table: { disable: true } },
  },
  tags: ["autodocs"],
} satisfies Meta<typeof TimeSelect>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
  },
}

export const Error: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    error: true,
  },
}

export const PanelTop: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    position: "top",
  },
  decorators: [
    (Story) => (
      <div style={{ marginTop: 400 }}>
        <Story />
      </div>
    ),
  ],
}

export const PanelBottom: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    position: "bottom",
  },
}

export const FullWidth: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    fullWidth: true,
  },
}

export const AllowClear: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
  },
}

export const AllowClearWithoutClock: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
    showTimeIcon: false,
  },
}

export const WithDefaultValue: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
    defaultValue: "12:00",
  },
}

export const CustomHeight: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
    maxPanelHeight: 200,
  },
}

export const Controlled: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
  },
  render: (args) => {
    const [value, setValue] = useState<string | undefined>(undefined)
    return (
      <TimeSelect
        {...args}
        value={value}
        onChange={(value) => {
          setValue(value)
        }}
      />
    )
  },
}
