import React, { useState } from "react"
import styles from "./TimeSelect.module.css"
import {
  classNames,
  normalizePxSize,
  range,
  setCssVar,
  useDetectClickOutside,
} from "@punks/ui-core"
import { Button, TextInput } from "../../atoms"
import { ClockIcon, CloseIcon } from "../../ui/icons"

export type TimeSelectSize = "sm" | "md" | "lg"
export type TimeSelectVariant = "filled" | "outlined"
export type TimeSelectPosition = "top" | "bottom"

export type TimeValue = {
  hours: number
  minutes: number
}

export interface TimeSelectProps
  extends Omit<
    React.InputHTMLAttributes<HTMLInputElement>,
    "size" | "onChange"
  > {
  inputRef?: React.Ref<HTMLInputElement>
  fullWidth?: boolean
  size?: TimeSelectSize
  variant?: TimeSelectVariant
  startAdornment?: React.ReactNode
  endAdornment?: React.ReactNode
  controlled?: boolean
  error?: boolean
  confirmLabel: string
  onChange?: (value: string | undefined) => void
  onValueChange?: (value: TimeValue | undefined) => void
  allowClear?: boolean
  showTimeIcon?: boolean
  timeIcon?: React.ReactNode
  closeIcon?: React.ReactNode
  maxPanelHeight?: number
  position?: TimeSelectPosition
}

const hours = range(24)
const minutes = range(60)

const parseHours = (value?: string) =>
  value ? parseInt(value.split(":")[0]) : undefined

const parseMinutes = (value?: string) =>
  value ? parseInt(value.split(":")[1]) : undefined

const formatTime = (value: TimeValue) => {
  return `${value.hours.toString().padStart(2, "0")}:${value.minutes
    .toString()
    .padStart(2, "0")}`
}

const TimeSelect = ({
  className,
  inputRef,
  fullWidth,
  size = "md",
  variant = "outlined",
  startAdornment,
  endAdornment,
  error,
  disabled,
  controlled,
  confirmLabel,
  onChange,
  onValueChange,
  allowClear,
  showTimeIcon = true,
  timeIcon,
  closeIcon,
  value,
  defaultValue,
  maxPanelHeight = 300,
  position = "bottom",
  ...props
}: TimeSelectProps) => {
  const [panelOpen, setPanelOpen] = useState(false)

  const togglePanel = () => {
    setPanelOpen(!panelOpen)
  }

  const closePanel = () => {
    setPanelOpen(false)
  }

  const ref = useDetectClickOutside<HTMLDivElement>({
    onTriggered: () => {
      setPanelOpen(false)
    },
  })

  const isControlled = controlled || value !== undefined

  const [uncontrolledMins, setUncontrolledMins] = React.useState<
    number | undefined
  >(
    value || defaultValue
      ? parseMinutes((value ?? defaultValue) as string | undefined)
      : undefined
  )

  const [uncontrolledHours, setUncontrolledHours] = React.useState<
    number | undefined
  >(
    value || defaultValue
      ? parseHours((value ?? defaultValue) as string | undefined)
      : undefined
  )

  const selectedHour = isControlled
    ? parseHours(value as string)
    : uncontrolledHours

  const selectedMinute = isControlled
    ? parseMinutes(value as string)
    : uncontrolledMins

  const isValidMinute =
    typeof selectedMinute === "number" &&
    selectedMinute >= 0 &&
    selectedMinute <= 59
  const isValidHour =
    typeof selectedHour === "number" && selectedHour >= 0 && selectedHour <= 23
  const isValidTime = isValidMinute && isValidHour

  const handleTimeChange = (value?: TimeValue) => {
    onChange?.(value ? formatTime(value) : undefined)
    onValueChange?.(value)
  }

  const selectHour = (hour: number) => {
    setUncontrolledHours(hour)
    if (!isValidMinute) {
      setUncontrolledMins(0)
    }

    handleTimeChange({
      hours: hour,
      minutes: isValidMinute ? selectedMinute : 0,
    })
  }

  const selectMinute = (minute: number) => {
    setUncontrolledMins(minute)
    if (!isValidHour) {
      setUncontrolledHours(0)
    }

    handleTimeChange({
      hours: isValidHour ? selectedHour : 0,
      minutes: minute,
    })
  }

  const handleClear = () => {
    setUncontrolledHours(undefined)
    setUncontrolledMins(undefined)
    onChange?.(undefined)
    onValueChange?.(undefined)
    handleTimeChange(undefined)
    closePanel()
  }

  const isHourSelected = (hour: number) => hour === selectedHour
  const isMinuteSelected = (minute: number) => minute === selectedMinute

  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.fullWidth]: fullWidth,
        },
        className
      )}
      style={{
        ...setCssVar(
          "--wp-theme-panel-max-height",
          normalizePxSize(maxPanelHeight)
        ),
      }}
      ref={ref}
    >
      <TextInput
        value={[
          selectedHour?.toString().padStart(2, "0") ?? "-- ",
          selectedMinute?.toString().padStart(2, "0") ?? " --",
        ].join(":")}
        error={error}
        readOnly
        width={props.width ?? fullWidth ? undefined : 105}
        inputRef={inputRef}
        onClick={togglePanel}
        fullWidth={fullWidth}
        endAdornment={
          allowClear && isValidTime ? (
            <div className={styles.closeIcon} onClick={handleClear}>
              {closeIcon ?? <CloseIcon />}
            </div>
          ) : (
            showTimeIcon && (
              <div className={styles.timeIcon} onClick={togglePanel}>
                {timeIcon ?? <ClockIcon />}
              </div>
            )
          )
        }
        {...props}
      />
      {panelOpen && (
        <div
          className={classNames(styles.panel, {
            [styles.panelTop]: position === "top",
            [styles.panelBottom]: position === "bottom",
          })}
        >
          <div className={styles.panelTracks}>
            <div className={styles.panelTracksRange}>
              {hours.map((hour) => (
                <div key={hour} className={styles.panelRangeCell}>
                  <Button
                    size="sm"
                    onClick={() => selectHour(hour)}
                    variant="filled"
                    color={isHourSelected(hour) ? "primary" : "transparent"}
                  >
                    {hour.toString().padStart(2, "0")}
                  </Button>
                </div>
              ))}
            </div>
            <div className={styles.panelTracksRange}>
              {minutes.map((minute) => (
                <div key={minute} className={styles.panelRangeCell}>
                  <Button
                    size="sm"
                    onClick={() => selectMinute(minute)}
                    variant="filled"
                    color={isMinuteSelected(minute) ? "primary" : "transparent"}
                  >
                    {minute.toString().padStart(2, "0")}
                  </Button>
                </div>
              ))}
            </div>
          </div>
          <div className={styles.panelFooter}>
            <Button
              size="sm"
              fullWidth
              color="primary"
              variant="filled"
              disabled={!isValidTime}
              onClick={closePanel}
            >
              {confirmLabel}
            </Button>
          </div>
        </div>
      )}
    </div>
  )
}

export default TimeSelect

export const RefTimeSelect = React.forwardRef<
  HTMLInputElement,
  TimeSelectProps
>((props, ref) => <TimeSelect {...props} inputRef={ref ?? props.inputRef} />)
