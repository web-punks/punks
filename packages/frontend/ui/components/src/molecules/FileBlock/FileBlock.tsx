import { ReactNode } from "react"
import { Typography } from "../../atoms/Typography"
import { TextVariant, classNames } from "@punks/ui-core"
import styles from "./FileBlock.module.css"
import { Button } from "../../atoms/Button"
import { DownloadIcon, TrashIcon } from "../../ui"

export type FileBlockClasses = {
  root?: string
  info?: string
  icon?: string
  block?: string
  actions?: string
}

export interface FileBlockProps {
  icon?: ReactNode
  title: ReactNode
  titleVariant?: TextVariant
  subtitle?: ReactNode
  subtitleVariant?: TextVariant
  classes?: FileBlockClasses
  className?: string
  onDownload?: () => void
  onRemove?: () => void
}

export const FileBlock = ({
  icon,
  subtitle,
  subtitleVariant = "body2",
  title,
  titleVariant = "h5",
  classes,
  className,
  onDownload,
  onRemove,
}: FileBlockProps) => {
  return (
    <div className={classNames(styles.root, classes?.root, className)}>
      <div className={classNames(styles.info, classes?.info)}>
        {icon && (
          <div className={classNames(styles.icon, classes?.icon)}>{icon}</div>
        )}
        <div className={classNames(styles.block, classes?.block)}>
          <Typography variant={titleVariant}>{title}</Typography>
          {subtitle && (
            <Typography className={styles.subtitle} variant={subtitleVariant}>
              {subtitle}
            </Typography>
          )}
        </div>
      </div>
      <div className={classNames(styles.actions, classes?.actions)}>
        {onDownload && (
          <Button rounded iconOnly onClick={onDownload}>
            <DownloadIcon className={styles.action} width={20} />
          </Button>
        )}
        {onRemove && (
          <Button rounded iconOnly onClick={onRemove}>
            <TrashIcon className={styles.action} width={16} />
          </Button>
        )}
      </div>
    </div>
  )
}
