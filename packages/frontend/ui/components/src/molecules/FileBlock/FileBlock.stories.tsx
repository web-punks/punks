import type { Meta, StoryObj } from "@storybook/react"

import { FileBlock } from "."

const meta = {
  title: "Ui/Molecules/FileBlock",
  component: FileBlock,
  tags: ["autodocs"],
  args: {
    title: "File name.txt",
    subtitle: "20 MB",
  },
} satisfies Meta<typeof FileBlock>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
}

export const WithActions: Story = {
  args: {
    onDownload: () => {},
    onRemove: () => {},
  },
}
