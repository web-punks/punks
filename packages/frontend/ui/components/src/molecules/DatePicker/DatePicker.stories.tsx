import type { Meta, StoryObj } from "@storybook/react"
import * as yup from "yup"
import { useForm } from "react-hook-form"

import DatePicker from "./DatePicker"
import { useState } from "react"
import { yupResolver } from "@hookform/resolvers/yup"
import { Box } from "../../structure"
import { Button } from "../../atoms"

const meta: Meta<typeof DatePicker> = {
  title: "Ui/Molecules/DatePicker",
  component: DatePicker,
  argTypes: {
    onChange: { action: "onChange" },
    inputRef: { table: { disable: true } },
  },
  tags: ["autodocs"],
} satisfies Meta<typeof DatePicker>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
  },
}

export const Error: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    error: true,
  },
}

export const PanelTop: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    position: "top",
  },
  decorators: [
    (Story) => (
      <div style={{ marginTop: 400 }}>
        <Story />
      </div>
    ),
  ],
}

export const PanelTopRight: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    position: "top",
    alignment: "right",
  },
  decorators: [
    (Story) => (
      <div style={{ marginTop: 400, marginLeft: 400 }}>
        <Story />
      </div>
    ),
  ],
}

export const PanelBottom: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    position: "bottom",
  },
}

export const PanelBottomRight: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    position: "bottom",
    alignment: "right",
  },
}

export const FullWidth: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    fullWidth: true,
  },
}

export const Italian: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    locale: "it",
  },
}

export const AllowClear: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
  },
}

export const AllowClearWithoutCalendar: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
    showCalendarIcon: false,
  },
}

export const WithDefaultValue: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
    defaultValue: "2020-01-02",
  },
}

export const CustomHeight: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
    allowClear: true,
  },
}

export const Controlled: Story = {
  args: {
    placeholder: "Placeholder",
    confirmLabel: "Ok",
  },
  render: (args) => {
    const [value, setValue] = useState<string | undefined>(undefined)
    return (
      <DatePicker
        {...args}
        value={value}
        onChange={(value) => {
          setValue(value)
        }}
      />
    )
  },
}

// export const WithReactHookFormAndDefaultValue: Story = {
//   args: {},
//   decorators: [
//     (Story, props) => {
//       const schema = yup.object().shape({
//         value: yup.string().required(),
//       })
//       const {
//         register,
//         handleSubmit,
//         formState: { errors },
//       } = useForm({
//         resolver: yupResolver(schema),
//         defaultValues: {
//           value: "2021-01-02",
//         },
//       })
//       return (
//         <div>
//           <Story
//             args={{
//               ...props.args,
//               ...register("value"),
//             }}
//           />
//           <Box mt={2}>
//             <Button
//               variant="filled"
//               color="primary"
//               onClick={handleSubmit((data) => console.log("submit data", data))}
//             >
//               submit
//             </Button>
//           </Box>
//         </div>
//       )
//     },
//   ],
// }
