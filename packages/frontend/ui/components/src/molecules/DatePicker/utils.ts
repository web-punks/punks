import { DateValue } from "./types"

export const formatIsoDate = (value: DateValue) =>
  [
    value.year.toString(),
    value.month.toString().padStart(2, "0"),
    value.day.toString().padStart(2, "0"),
  ].join("-")

export const parseDate = (value: string): DateValue => {
  const parts = value.split("-")
  return {
    day: parseInt(parts[2]),
    month: parseInt(parts[1]),
    year: parseInt(parts[0]),
  }
}
