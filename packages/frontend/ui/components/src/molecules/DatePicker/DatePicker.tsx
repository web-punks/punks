import React, { useState } from "react"
import Calendar from "react-calendar"
import "react-calendar/dist/Calendar.css"
import styles from "./DatePicker.module.css"
import { classNames, setCssVar, useDetectClickOutside } from "@punks/ui-core"
import { CalendarIcon, CloseIcon } from "../../ui/icons"
import { DateValue } from "./types"
import { formatIsoDate, parseDate } from "./utils"
import { TextInput } from "../../atoms/TextInput"
import { Button } from "../../atoms/Button"

export type DatePickerSize = "sm" | "md" | "lg"
export type DatePickerVariant = "filled" | "outlined"
export type DatePickerPosition = "top" | "bottom"
export type DatePickerAlignment = "left" | "right"

export interface DatePickerProps
  extends Omit<
    React.InputHTMLAttributes<HTMLInputElement>,
    "size" | "onChange"
  > {
  inputRef?: React.Ref<HTMLInputElement>
  fullWidth?: boolean
  size?: DatePickerSize
  variant?: DatePickerVariant
  startAdornment?: React.ReactNode
  endAdornment?: React.ReactNode
  controlled?: boolean
  error?: boolean
  onChange?: (value: string | undefined) => void
  onValueChange?: (value: DateValue | undefined) => void
  allowClear?: boolean
  showCalendarIcon?: boolean
  calendarIcon?: React.ReactNode
  closeIcon?: React.ReactNode
  minDate?: DateValue
  maxDate?: DateValue
  locale?: string
  confirmLabel: string
  position?: DatePickerPosition
  alignment?: DatePickerAlignment
  zIndex?: number
}

const DatePicker = ({
  className,
  inputRef,
  fullWidth,
  size = "md",
  variant = "outlined",
  startAdornment,
  endAdornment,
  error,
  disabled,
  controlled,
  onChange,
  onValueChange,
  allowClear,
  showCalendarIcon = true,
  calendarIcon,
  closeIcon,
  value,
  defaultValue,
  minDate = {
    day: 1,
    month: 1,
    year: 1900,
  },
  maxDate = {
    day: 31,
    month: 12,
    year: 2100,
  },
  locale,
  confirmLabel,
  position = "bottom",
  alignment = "left",
  zIndex = 1,
  ...props
}: DatePickerProps) => {
  const [panelOpen, setPanelOpen] = useState(false)

  const togglePanel = () => {
    setPanelOpen(!panelOpen)
  }

  const closePanel = () => {
    setPanelOpen(false)
  }

  const ref = useDetectClickOutside<HTMLDivElement>({
    onTriggered: () => {
      setPanelOpen(false)
    },
  })

  const isControlled = controlled || value !== undefined
  const [uncontrolledDate, setUncontrolledDate] = React.useState<
    DateValue | undefined
  >(
    value || defaultValue
      ? parseDate((value ?? defaultValue) as string)
      : undefined
  )

  const selectedDate = isControlled
    ? value
      ? parseDate(value as string)
      : undefined
    : uncontrolledDate

  const handleDateChange = (value?: DateValue) => {
    onChange?.(value ? formatIsoDate(value) : undefined)
    onValueChange?.(value)
  }

  const handleClear = () => {
    setUncontrolledDate(undefined)
    onChange?.(undefined)
    onValueChange?.(undefined)
    handleDateChange(undefined)
    closePanel()
  }

  const handleDateSelect = (date: Date) => {
    const dateValue = {
      day: date.getDate(),
      month: date.getMonth() + 1,
      year: date.getFullYear(),
    }
    setUncontrolledDate(dateValue)
    handleDateChange(dateValue)
  }

  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.fullWidth]: fullWidth,
        },
        className
      )}
      style={{
        ...setCssVar("--z", zIndex),
      }}
      ref={ref}
    >
      <TextInput
        error={error}
        value={[
          selectedDate?.day?.toString().padStart(2, "0") ?? "__",
          selectedDate?.month?.toString().padStart(2, "0") ?? " __",
          selectedDate?.year?.toString().padStart(4, "0") ?? " ____",
        ].join("/")}
        readOnly
        width={props.width ?? fullWidth ? undefined : 145}
        inputRef={inputRef}
        onClick={togglePanel}
        fullWidth={fullWidth}
        endAdornment={
          allowClear && selectedDate ? (
            <div className={styles.closeIcon} onClick={handleClear}>
              {closeIcon ?? <CloseIcon />}
            </div>
          ) : (
            showCalendarIcon && (
              <div className={styles.calendarIcon} onClick={togglePanel}>
                {calendarIcon ?? <CalendarIcon width={15} />}
              </div>
            )
          )
        }
        {...props}
      />
      {panelOpen && (
        <div
          className={classNames(styles.panel, {
            [styles.panelTopLeft]: position === "top" && alignment === "left",
            [styles.panelTopRight]: position === "top" && alignment === "right",
            [styles.panelBottomLeft]:
              position === "bottom" && alignment === "left",
            [styles.panelBottomRight]:
              position === "bottom" && alignment === "right",
          })}
        >
          <Calendar
            className={styles.calendar}
            locale={locale}
            value={
              selectedDate
                ? new Date(
                    selectedDate.year,
                    selectedDate.month - 1,
                    selectedDate.day
                  )
                : undefined
            }
            onChange={(date) => handleDateSelect(new Date(date as any))}
          />
          <div className={styles.panelFooter}>
            <Button
              size="sm"
              fullWidth
              color="primary"
              variant="filled"
              disabled={!selectedDate}
              onClick={closePanel}
            >
              {confirmLabel}
            </Button>
          </div>
        </div>
      )}
    </div>
  )
}

export default DatePicker

export const RefDatePicker = React.forwardRef<
  HTMLInputElement,
  DatePickerProps
>((props, ref) => <DatePicker {...props} inputRef={ref ?? props.inputRef} />)
