import type { Meta, StoryObj } from "@storybook/react"
import { CancelConfirmCta } from "./CancelConfirmCta"

const meta = {
  title: "UI/Molecules/CancelConfirmCta",
  component: CancelConfirmCta,
  tags: ["autodocs"],
  args: {
    confirm: {
      label: "Confirm",
      onClick: () => {},
    },
    cancel: {
      label: "Cancel",
      onClick: () => {},
    },
  },
} satisfies Meta<typeof CancelConfirmCta>
export default meta

type Story = StoryObj<typeof meta>

export const Small: Story = {
  args: {
    size: "sm",
  },
}

export const Medium: Story = {
  args: {
    size: "md",
  },
}

export const Large: Story = {
  args: {
    size: "lg",
  },
}
