import { ReactNode } from "react"
import { ThemeColor, classNames } from "@punks/ui-core"
import styles from "./CancelConfirmCta.module.css"
import { Button, ButtonSize } from "../../atoms/Button"

export interface CancelConfirmCta {
  label: ReactNode
  onClick: () => void
  loading?: boolean
  disabled?: boolean
  hidden?: boolean
  color?: ThemeColor
}

export type CancelConfirmCtaAlign = "left" | "right" | "center"

export interface CancelConfirmCtaProps {
  cancel?: CancelConfirmCta
  confirm?: CancelConfirmCta
  size?: ButtonSize
  className?: string
  align?: CancelConfirmCtaAlign
}

export const CancelConfirmCta = ({
  confirm,
  cancel,
  size,
  className,
  align,
}: CancelConfirmCtaProps) => {
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.alignLeft]: align === "left",
          [styles.alignRight]: align === "right",
          [styles.alignCenter]: align === "center",
        },
        className
      )}
    >
      {cancel && !cancel.hidden && (
        <Button
          size={size}
          disabled={cancel.disabled}
          loading={cancel.loading}
          onClick={cancel.onClick}
          color={cancel.color ?? "light"}
          variant="text"
        >
          {cancel.label}
        </Button>
      )}
      {confirm && !confirm.hidden && (
        <Button
          size={size}
          disabled={confirm.disabled}
          loading={confirm.loading}
          onClick={confirm.onClick}
          color={confirm.color ?? "primary"}
          variant="filled"
        >
          {confirm.label}
        </Button>
      )}
    </div>
  )
}
