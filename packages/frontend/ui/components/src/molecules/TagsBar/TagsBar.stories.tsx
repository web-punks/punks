import type { Meta, StoryObj } from "@storybook/react"
import TagsBar from "./TagsBar"

const meta = {
  title: "UI/Molecules/TagsBar",
  component: TagsBar,
  tags: ["autodocs"],
  args: {},
} satisfies Meta<typeof TagsBar>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    items: [
      { label: "Tag 1" },
      { label: "Tag 2" },
      { label: "Tag 3", onRemove: () => {} },
    ],
    color: "info",
  },
} satisfies Story
