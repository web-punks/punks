import { ReactNode } from "react"
import styles from "./TagsBar.module.css"
import { Tag } from "../../atoms"
import { ThemeColor, ThemeColorVariant, classNames } from "@punks/ui-core"
import { TagSize } from "../../atoms/Tag/Tag"

export type TagBarItem = {
  label: ReactNode
  onRemove?: () => void
  className?: string
}

export interface TagsBarProps {
  items: TagBarItem[]
  className?: string
  color?: ThemeColor
  colorVariant?: ThemeColorVariant
  textColor?: string
  backgroundColor?: string
  size?: TagSize
  multiline?: boolean
}

const TagsBar = ({
  items,
  className,
  backgroundColor,
  color,
  colorVariant,
  size,
  textColor,
  multiline,
}: TagsBarProps) => {
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.multiline]: multiline,
        },
        className
      )}
    >
      {items.map((item, index) => (
        <Tag
          key={index}
          onRemove={item.onRemove}
          className={item.className}
          backgroundColor={backgroundColor}
          color={color}
          colorVariant={colorVariant}
          size={size}
          textColor={textColor}
        >
          {item.label}
        </Tag>
      ))}
    </div>
  )
}

export default TagsBar
