import React, { ReactNode, useRef, useState } from "react"
import { DropInput } from "../../panels/DropInput"
import { Spinner } from "../../animations"
import { classNames } from "@punks/ui-core"
import styles from "./Autocomplete.module.css"

export type AutocompleteSize = "sm" | "md" | "lg"
export type AutocompleteVariant = "filled" | "outlined" | "underlined"

export type AutocompleteClasses = {
  root?: string
  input?: string
  placeholder?: string
  panel?: string
  option?: string
  emptyOptionsContent?: string
}

export interface AutocompleteOption {
  value: string
  label: ReactNode
  disabled?: boolean
}

export interface AutocompleteProps
  extends Omit<React.InputHTMLAttributes<HTMLInputElement>, "size"> {
  inputRef?: React.Ref<HTMLInputElement>
  options?: AutocompleteOption[]
  onOptionSelect?: (option: AutocompleteOption) => void
  loading?: boolean
  className?: string
  placeholder?: string
  size?: AutocompleteSize
  variant?: AutocompleteVariant
  rounded?: boolean
  fullWidth?: boolean
  error?: boolean
  disabled?: boolean
  maxPanelHeight?: string | number
  controlled?: boolean
  classes?: AutocompleteClasses
  emptyOptionsContent?: React.ReactNode
  readOnly?: boolean
  autocompleteDisabled?: boolean
  open?: boolean
  onOpenChange?: (open: boolean) => void
}

const Autocomplete: React.FC<AutocompleteProps> = ({
  inputRef,
  options,
  className,
  placeholder,
  rounded,
  size = "md",
  variant = "outlined",
  maxPanelHeight = 400,
  fullWidth,
  error,
  disabled,
  controlled,
  value,
  defaultValue,
  classes,
  emptyOptionsContent,
  readOnly,
  loading,
  autocompleteDisabled,
  autoComplete,
  open,
  onOptionSelect,
  onOpenChange,
  ...other
}) => {
  const openControlled = typeof open === "boolean"

  const rootRef = useRef<HTMLDivElement>(null)
  const [isOpen, setIsOpen] = useState(false)

  const openValue = openControlled ? open : isOpen

  const handleOpenChange = (value: boolean) => {
    if (openControlled) {
      if (!onOpenChange) {
        console.error(
          `You are trying to control the 'open' state of Autocomplete without providing 'onOpenChange' prop`
        )
      }
      onOpenChange?.(value)
    }

    setIsOpen(value)
  }

  const togglePanel = () => {
    if (disabled || readOnly) {
      return
    }

    handleOpenChange(!openValue)
  }

  const handleOutsideClick = (event: MouseEvent) => {
    if (rootRef.current && !rootRef.current.contains(event.target as Node)) {
      handleOpenChange(false)
    }
  }

  React.useEffect(() => {
    window.addEventListener("click", handleOutsideClick)
    return () => {
      window.removeEventListener("click", handleOutsideClick)
    }
  }, [])

  const handleOptionToggle = (option: AutocompleteOption) => {
    onOptionSelect?.(option)
    handleOpenChange(false)
  }

  return (
    <DropInput
      onClick={togglePanel}
      rootRef={rootRef}
      className={className}
      classes={{
        root: classNames(styles.root, classes?.root),
        panel: classes?.panel,
      }}
      disabled={disabled}
      fullWidth={fullWidth}
      error={error}
      readOnly={readOnly}
      rounded={rounded}
      size={size}
      open={openValue}
      panelMaxHeight={maxPanelHeight}
      variant={variant}
      panelContent={
        <div>
          {(!options || options?.length === 0) && (
            <div
              className={classNames(
                styles.emptyOptionsContent,
                classes?.emptyOptionsContent
              )}
            >
              {emptyOptionsContent}
            </div>
          )}
          {options?.map((option) => (
            <div
              key={option.value}
              className={classNames(
                styles.option,
                {
                  [styles.optionDisabled]: option.disabled,
                },
                classes?.option
              )}
              onClick={() => !option.disabled && handleOptionToggle(option)}
            >
              <span>{option?.label}</span>
            </div>
          ))}
        </div>
      }
      endAdornment={loading && <Spinner size="md" />}
      chevronHidden
    >
      <input
        ref={inputRef}
        className={classNames(
          styles.input,
          styles[`input-${variant}`],
          {
            [styles.inputDisabled]: disabled,
          },
          classes?.input
        )}
        defaultValue={defaultValue}
        value={value}
        placeholder={placeholder}
        disabled={disabled}
        autoComplete={autocompleteDisabled ? "none" : autoComplete}
        {...other}
      />
    </DropInput>
  )
}

export default Autocomplete

export const RefAutocomplete = React.forwardRef<
  HTMLInputElement,
  AutocompleteProps
>((props, ref) => <Autocomplete {...props} inputRef={ref ?? props.inputRef} />)
