import type { Meta, StoryObj } from "@storybook/react"

import { Autocomplete } from "."
import { range } from "@punks/ui-core"

const meta = {
  title: "Ui/Molecules/Autocomplete",
  component: Autocomplete,
  tags: ["autodocs"],
  args: {
    placeholder: "Type something",
    emptyOptionsContent: "No items",
    options: [
      { label: "Item 1", value: "item1" },
      { label: "Item 2", value: "item2" },
      { label: "Item 3", value: "item3" },
    ],
    onOptionSelect: (option: any) => console.log("option selected", option),
  },
} satisfies Meta<typeof Autocomplete>
export default meta

type Story = StoryObj<typeof meta>

export const Outlined: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
  },
}

export const OutlinedOpen: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    open: true,
  },
}

export const OutlinedLoading: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    loading: true,
  },
}

export const OutlinedReadonly: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    readOnly: true,
  },
}

export const OutlinedMediumWithDisabledOptions: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
  },
}

export const OutlinedMediumWithManyOptions: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    options: range(100).map((i) => ({
      label: `Item ${i}`,
      value: `item${i}`,
    })),
  },
}

export const OutlinedMediumDisabled: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    disabled: true,
  },
}
