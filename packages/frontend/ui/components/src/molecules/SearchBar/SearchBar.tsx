import { Spinner } from "../../animations"
import { TextInput, TextInputProps } from "../../atoms/TextInput"
import { SearchIcon } from "../../ui/icons"

export interface SearchBarProps
  extends Omit<TextInputProps, "startAdornment" | "endAdornment"> {
  loading?: boolean
  icon?: React.ReactNode
  searchIconPosition?: "start" | "end"
}

const SearchBar = ({
  loading,
  icon,
  size = "md",
  searchIconPosition = "start",
  ...textProps
}: SearchBarProps) => {
  return (
    <TextInput
      startAdornment={
        searchIconPosition === "start"
          ? icon ?? <SearchIcon width={16} />
          : undefined
      }
      size={size}
      endAdornment={
        <>
          {loading ? <Spinner size={size} /> : undefined}
          {searchIconPosition === "end"
            ? icon ?? <SearchIcon width={16} />
            : undefined}
        </>
      }
      {...textProps}
    />
  )
}

export default SearchBar
