import type { Meta, StoryObj } from "@storybook/react"

import { SearchBar } from "."

const meta = {
  title: "Ui/Molecules/SearchBar",
  component: SearchBar,
  tags: ["autodocs"],
  argTypes: {},
  args: {
    placeholder: "Search",
  },
} satisfies Meta<typeof SearchBar>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
}

export const FullWidth: Story = {
  args: {
    fullWidth: true,
  },
}

export const FullWidthLoading: Story = {
  args: {
    fullWidth: true,
    loading: true,
  },
}
