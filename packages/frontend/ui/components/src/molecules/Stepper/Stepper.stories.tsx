import type { Meta, StoryObj } from "@storybook/react"
import { Stepper } from "./Stepper"

const meta = {
  title: "UI/Molecules/Stepper",
  component: Stepper,
  args: {
    activeStep: 0,
  },
} satisfies Meta<typeof Stepper>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    steps: [{}, {}, {}],
  },
}

export const FullWidth: Story = {
  args: {
    steps: [{}, {}, {}],
    fullWidth: true,
  },
}

export const FullWidthStep2: Story = {
  args: {
    steps: [{}, {}, {}],
    fullWidth: true,
    activeStep: 1,
  },
}

export const Labeled: Story = {
  args: {
    steps: [
      {
        title: "Step 1",
      },
      {
        title: "Step 2",
      },
      {
        title: "Step 3",
      },
    ],
  },
}

export const LabeledFullWidth: Story = {
  args: {
    steps: [
      {
        title: "Step 1",
      },
      {
        title: "Step 2",
      },
      {
        title: "Step 3",
      },
    ],
    fullWidth: true,
  },
}
