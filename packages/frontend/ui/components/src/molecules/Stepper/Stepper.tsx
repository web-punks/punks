import React from "react"
import styles from "./Stepper.module.css"
import { classNames } from "@punks/ui-core"

export interface StepItem {
  title?: React.ReactNode
}

export interface StepperProps {
  steps: StepItem[]
  activeStep: number
  className?: string
  fullWidth?: boolean
}

export const Stepper = ({
  activeStep,
  steps,
  className,
  fullWidth,
}: StepperProps) => {
  return (
    <ul
      className={classNames(
        styles.root,
        {
          [styles.fullWidth]: fullWidth,
        },
        className
      )}
    >
      {steps.map((step, index) => (
        <li key={index}>
          <div
            className={classNames(styles.step, {
              [styles.stepConnector]: index > 0,
              [styles.stepPrimary]: index <= activeStep,
            })}
          ></div>
          {step.title && (
            <div
              className={classNames(styles.label, {
                [styles.labelNext]: activeStep < index,
              })}
            >
              {step.title}
            </div>
          )}
        </li>
      ))}
    </ul>
  )
}
