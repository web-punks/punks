import type { Meta, StoryObj } from "@storybook/react"
import Toolbar from "./Toolbar"
import { FaBeer, FaCoffee, FaApple } from "react-icons/fa"

const meta: Meta = {
  title: "UI/Molecules/Toolbar",
  component: Toolbar,
}
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    buttons: [
      { Icon: FaBeer, onClick: () => console.log("Beer icon clicked") },
      { Icon: FaCoffee, onClick: () => console.log("Coffee icon clicked") },
      { Icon: FaApple, onClick: () => console.log("Apple icon clicked") },
    ],
  },
}
