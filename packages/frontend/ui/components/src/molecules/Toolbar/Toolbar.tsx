import React from "react"
import styles from "./Toolbar.module.css"
import { classNames } from "@punks/ui-core"

interface ToolbarButtonProps {
  Icon?: React.ElementType
  onClick?: () => void
  className?: string
  enabled?: boolean
  visible?: boolean
}

const ToolbarButton: React.FC<ToolbarButtonProps> = ({
  Icon,
  onClick,
  className,
  ...props
}) => {
  return (
    <button
      onClick={onClick}
      className={classNames(styles.toolbarButton, className)}
      {...props}
    >
      {Icon && <Icon />}
    </button>
  )
}

export type ToolbarButtonType = {
  Icon: React.ElementType
  onClick: () => void
  className?: string
  enabled?: boolean
  visible?: boolean
}

export interface ToolbarProps {
  buttons: ToolbarButtonType[]
  className?: string
}

const Toolbar: React.FC<ToolbarProps> = ({ buttons, className, ...props }) => {
  return (
    <div className={classNames(styles.toolbar, className)} {...props}>
      {buttons
        .filter((x) => x.visible !== false)
        .map((button, index) => (
          <ToolbarButton key={index} {...button} />
        ))}
    </div>
  )
}

export default Toolbar
