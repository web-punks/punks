import type { Meta, StoryObj } from "@storybook/react"

import { Header } from "."
import { FilterIcon, MenuIcon, SearchIcon } from "../../ui/icons"

const meta = {
  title: "Ui/Molecules/Header",
  component: Header,
  tags: ["autodocs"],
  argTypes: {},
  args: {
    leftItems: [
      {
        content: (
          <img
            src="https://a.storyblok.com/f/196735/601x601/29667fe0ad/fav10.png"
            style={{
              maxWidth: "100%",
            }}
            width={50}
          />
        ),
      },
    ],
    rightItems: [
      {
        content: <FilterIcon />,
      },
      {
        content: <SearchIcon />,
      },
      {
        content: <MenuIcon />,
      },
    ],
    barColor: "grey-70",
    textColor: "paper",
  },
} satisfies Meta<typeof Header>
export default meta

type Story = StoryObj<typeof meta>

// const Template: ComponentStory<typeof Header> = (args) => <Header {...args} />

export const HeaderSmall: Story = {
  args: {
    size: "small",
  },
}

export const HeaderMedium: Story = {
  args: {
    size: "medium",
  },
}

export const HeaderLarge: Story = {
  args: {
    size: "large",
  },
}
