import React, { Fragment, ReactNode } from "react"
import styles from "./Header.module.css"
import {
  ThemeColor,
  ThemeColorVariant,
  classNames,
  setThemeColorVar,
} from "@punks/ui-core"

export type HeaderItem = {
  content: ReactNode
}

export type BarSize = "small" | "medium" | "large"

export type HeaderClasses = {
  root?: string
  col?: string
  colLeft?: string
  colRight?: string
}

export interface HeaderProps {
  leftItems?: HeaderItem[]
  rightItems?: HeaderItem[]
  component?: keyof JSX.IntrinsicElements
  barColor?: ThemeColor
  barColorVariant?: ThemeColorVariant
  textColor?: ThemeColor
  textColorVariant?: ThemeColorVariant
  size?: BarSize
  classes?: HeaderClasses
}

export const Header = ({
  leftItems,
  rightItems,
  component,
  barColor,
  barColorVariant,
  textColor,
  textColorVariant,
  size = "medium",
  classes,
}: HeaderProps) => {
  const Tag = component ?? "div"
  return (
    <Tag
      className={classNames(styles.root, styles[size], classes?.root)}
      style={{
        ...setThemeColorVar("--bg-color", barColor, barColorVariant),
        ...setThemeColorVar("--color", textColor, textColorVariant),
      }}
    >
      <div className={classNames(styles.col, classes?.col, classes?.colLeft)}>
        {leftItems?.map((item, index) => (
          <Fragment key={index}>{item.content}</Fragment>
        ))}
      </div>
      <div className={classNames(styles.col, classes?.col, classes?.colRight)}>
        {rightItems?.map((item, index) => (
          <Fragment key={index}>{item.content}</Fragment>
        ))}
      </div>
    </Tag>
  )
}
