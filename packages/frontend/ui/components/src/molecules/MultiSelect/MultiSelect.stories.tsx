import type { Meta, StoryObj } from "@storybook/react"

import { MultiSelect } from "."
import { range } from "@punks/ui-core"

const meta = {
  title: "Ui/Molecules/MultiSelect",
  component: MultiSelect,
  tags: ["autodocs"],
  args: {
    options: [
      {
        label: "Item1",
        value: "item1",
      },
      {
        label: "Item2",
        value: "item2",
      },
      {
        label: "Item3",
        value: "item3",
      },
      {
        label: "Item4",
        value: "item4",
      },
    ],
  },
} satisfies Meta<typeof MultiSelect>
export default meta

type Story = StoryObj<typeof meta>

export const OutlinedMedium: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    placeholder: "Select items",
  },
}

export const OutlinedMediumGrouped: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    placeholder: "Select items",
    options: [
      {
        groupByKey: "Item",
        label: "Item3",
        value: "item3",
      },
      {
        label: "Item4",
        value: "item4",
      },
    ],
  },
}

export const OutlinedReadonly: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    placeholder: "Select items",
    readOnly: true,
    value: ["item1", "item2"],
  },
}

export const OutlinedMediumWithDisabledOptions: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    placeholder: "Select items",
    options: [
      {
        label: "Item1",
        value: "item1",
      },
      {
        label: "Item2",
        value: "item2",
        disabled: true,
      },
      {
        label: "Item3",
        value: "item3",
        disabled: true,
        checked: true,
      },
      {
        label: "Item4",
        value: "item4",
      },
    ],
  },
}

export const OutlinedMediumWithManyOptions: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    placeholder: "Select items",
    options: range(100).map((i) => ({
      label: `Item ${i}`,
      value: `item${i}`,
    })),
  },
}

export const OutlinedMediumDisabled: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    placeholder: "Select items",
    disabled: true,
  },
}

export const OutlinedMediumFilterable: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    placeholder: "Select items",
    filterable: true,
  },
}

export const OutlinedMediumEmpty: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    placeholder: "Select items",
    emptyOptionsContent: "No items",
    options: [],
  },
}

export const OutlinedMediumSorted: Story = {
  args: {
    variant: "outlined",
    size: "md",
    fullWidth: true,
    placeholder: "Select items",
    tagsSortFn(a, b) {
      return a.label.localeCompare(b.label)
    },
  },
}
