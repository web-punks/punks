import React, { useState, useRef, useMemo } from "react"
import { ChevronDown } from "../../ui/icons"
import styles from "./MultiSelect.module.css"
import { classNames, groupBy, normalizePxSize, setCssVar } from "@punks/ui-core"
import { Tag } from "../../atoms/Tag"
import { Checkbox } from "../../atoms/Checkbox"

export type MultiSelectSize = "sm" | "md" | "lg"
export type MultiSelectVariant = "filled" | "outlined" | "underlined"

export type MultiSelectClasses = {
  root?: string
  input?: string
  placeholder?: string
  filter?: string
  tags?: string
  tag?: string
  options?: string
  option?: string
  emptyOptionsContent?: string
}

export interface MultiSelectOption {
  value: string
  label: string
  disabled?: boolean
  checked?: boolean
  key?: string
  groupByKey?: string
}

export interface MultiSelectProps {
  options: MultiSelectOption[]
  className?: string
  placeholder?: string
  size?: MultiSelectSize
  variant?: MultiSelectVariant
  rounded?: boolean
  fullWidth?: boolean
  error?: boolean
  disabled?: boolean
  name?: string
  maxOptionsHeight?: string | number
  onChange?: (value: string[]) => void
  controlled?: boolean
  value?: string[]
  defaultValue?: string[]
  filterable?: boolean
  filter?: string
  onFilterChange?: (filter: string) => void
  filterMatchFn?: (option: MultiSelectOption, filter: string) => boolean
  tagsSortFn?: (a: MultiSelectOption, b: MultiSelectOption) => number
  classes?: MultiSelectClasses
  emptyOptionsContent?: React.ReactNode
  readOnly?: boolean
  inputRef?: React.Ref<HTMLInputElement>
  defaultGroupLabel?: string
}

const InnerMultiSelect: React.FC<MultiSelectProps> = ({
  options,
  className,
  placeholder,
  rounded,
  size = "md",
  variant = "outlined",
  maxOptionsHeight = 400,
  fullWidth,
  error,
  disabled,
  onChange,
  name,
  controlled,
  value,
  defaultValue,
  filterable,
  filter,
  filterMatchFn = (option: MultiSelectOption, filter: string) =>
    option.label.toLowerCase().includes(filter.toLowerCase()),
  onFilterChange,
  tagsSortFn,
  classes,
  emptyOptionsContent,
  readOnly,
  defaultGroupLabel = "Other",
  inputRef,
}) => {
  const [isControlled] = useState(controlled || value !== undefined)
  const [uncontrolledValue, setUncontrolledValue] = useState<string[]>(
    defaultValue ?? []
  )

  const [isOpen, setIsOpen] = useState(false)
  const rootRef = useRef<HTMLDivElement | null>(null)

  const [uncontrolledFilter, setUncontrolledFilter] = useState<string>()

  const selectValue = (isControlled ? value : uncontrolledValue) ?? []
  const isSelected = (value: string) => selectValue?.includes(value)

  const toggleDropdown = () => {
    if (disabled || readOnly) {
      return
    }

    setIsOpen(!isOpen)
  }

  const updateSelectedOptions = (value: string[]) => {
    setUncontrolledValue(value)
    onChange?.(value)
  }

  const handleOptionToggle = (value: string) => {
    const updatedValue: string[] = selectValue.includes(value)
      ? selectValue.filter((option) => option !== value)
      : [...selectValue, value]

    updateSelectedOptions(updatedValue)
    handleFilterUpdate("")
  }

  const handleOutsideClick = (event: MouseEvent) => {
    if (rootRef.current && !rootRef.current.contains(event.target as Node)) {
      setIsOpen(false)
    }
  }

  React.useEffect(() => {
    window.addEventListener("click", handleOutsideClick)
    return () => {
      window.removeEventListener("click", handleOutsideClick)
    }
  }, [])

  const handleFilterUpdate = (value: string) => {
    onFilterChange?.(value) ?? setUncontrolledFilter(value)
  }

  const sortedTags = useMemo(() => {
    const selectOptions = selectValue
      .filter((x) => x)
      .map(
        (value) =>
          options.filter((x) => x).find((option) => option.value === value)!
      )
      .filter((x) => x)
    return tagsSortFn ? selectOptions.sort(tagsSortFn) : selectOptions
  }, [selectValue, tagsSortFn, options])

  return (
    <div
      className={classNames(
        styles.root,
        styles[variant],
        styles[`${variant}-${size}`],
        {
          [styles.fullWidth]: fullWidth,
          [styles[`${variant}-rounded-${size}`]]: rounded,
          [styles.opened]: isOpen,
          [styles.error]: error,
          [styles.disabled]: disabled,
          [styles.clickable]: !disabled && !readOnly,
        },
        classes?.root,
        className
      )}
      ref={rootRef}
    >
      <div
        className={classNames(
          styles.input,
          {
            [styles.clickable]: !disabled && !readOnly,
          },
          styles[`input-${size}`],
          classes?.input
        )}
        onClick={!readOnly ? toggleDropdown : undefined}
      >
        {sortedTags.length === 0 && (
          <span
            className={classNames(styles.placeholder, classes?.placeholder)}
          >
            {placeholder}
          </span>
        )}
        {sortedTags.length > 0 && (
          <div className={classNames(styles.tags, classes?.tags)}>
            {sortedTags
              .filter((x) => x)
              .map((option) => (
                <Tag
                  className={classNames(styles.tag, classes?.tag)}
                  key={option?.key ?? option.value}
                  color="primary"
                  onRemove={
                    !readOnly
                      ? (e) => {
                          e.stopPropagation()
                          handleOptionToggle(option.value)
                        }
                      : undefined
                  }
                >
                  {option.label}
                </Tag>
              ))}
          </div>
        )}
        {!readOnly && (
          <>
            {filterable && (
              <input
                className={classNames(styles.filter, classes?.filter)}
                placeholder={placeholder}
                value={filter ?? uncontrolledFilter}
                onChange={(e) => handleFilterUpdate(e.target.value)}
              />
            )}

            <input
              type="text"
              value={isControlled ? value : uncontrolledValue}
              ref={inputRef}
              hidden
              readOnly
              name={name}
            />

            <div className={styles.arrowContainer}>
              <ChevronDown
                className={classNames(
                  styles.arrowIcon,
                  styles[`arrowIcon-${variant}`],
                  {
                    [styles.rotateIcon]: isOpen,
                  }
                )}
              />
            </div>
          </>
        )}
      </div>
      {isOpen && (
        <div
          style={{
            ...setCssVar("--max-h", normalizePxSize(maxOptionsHeight)),
          }}
          className={classNames(
            styles.options,
            styles[`options-${variant}`],
            {
              [styles[`options-${size}-rounded-${size}`]]: rounded,
              [styles.optionsMaxH]: !!maxOptionsHeight,
              // [styles.textVariant]: variant === "text",
            },
            classes?.options
          )}
        >
          {options.length === 0 && (
            <div
              className={classNames(
                styles.emptyOptionsContent,
                classes?.emptyOptionsContent
              )}
            >
              {emptyOptionsContent}
            </div>
          )}
          {options.some((x) => x?.groupByKey)
            ? Object.entries(
                groupBy(
                  options.filter((x) => x.value),
                  (x) => x?.key ?? ""
                )
              ).map(([key, options], index) => (
                <>
                  <span
                    className={classNames(styles.groupLabel, {
                      [styles.optionDisabled]: options?.every(
                        (option) => option.disabled
                      ),
                    })}
                  >
                    {key !== "undefined" ? key : defaultGroupLabel}
                  </span>

                  {options?.map((option) => (
                    <div
                      key={option?.key ?? option.value}
                      className={classNames(
                        styles.option,
                        {
                          [styles.optionDisabled]: option.disabled,
                          [styles.optionSelected]: isSelected(option.value),
                        },
                        classes?.option
                      )}
                      onClick={() =>
                        !option.disabled && handleOptionToggle(option.value)
                      }
                    >
                      <Checkbox
                        disabled={option.disabled}
                        checked={isSelected(option.value) || option.checked}
                        readOnly
                      />
                      <span>{option?.label}</span>
                    </div>
                  ))}
                </>
              ))
            : options
                .filter((x) => x.value)
                .map((option) => (
                  <div
                    key={option?.key ?? option.value}
                    className={classNames(
                      styles.option,
                      {
                        [styles.optionDisabled]: option.disabled,
                        [styles.optionSelected]: isSelected(option.value),
                      },
                      classes?.option
                    )}
                    onClick={() =>
                      !option.disabled && handleOptionToggle(option.value)
                    }
                  >
                    <Checkbox
                      disabled={option.disabled}
                      checked={isSelected(option.value) || option.checked}
                      readOnly
                    />
                    <span>{option?.label}</span>
                  </div>
                ))}
        </div>
      )}
    </div>
  )
}

const MultiSelect = React.forwardRef<HTMLInputElement, MultiSelectProps>(
  (props, ref) => (
    <InnerMultiSelect {...props} inputRef={ref ?? props.inputRef} />
  )
)
MultiSelect.displayName = "MultiSelect"

export default MultiSelect
