import { Button } from "../../atoms"
import ListEditor from "./ListEditor"
import type { Meta, StoryObj } from "@storybook/react"

interface ListItem {
  id: string
  text: string
}

const TypedListEditor = ListEditor<any>

const meta: Meta = {
  title: "UI/Molecules/ListEditor",
  component: TypedListEditor,
  args: {},
} satisfies Meta<typeof TypedListEditor>
export default meta

type Story = StoryObj<typeof TypedListEditor>

export const TextListEditor: Story = {
  args: {
    id: "text-list-editor",
    items: [
      { id: "1", text: "Elemento 1" },
      { id: "2", text: "Elemento 2" },
      { id: "3", text: "Elemento 3" },
    ],
    renderItem: (item: ListItem) => <div key={item.id}>{item.text}</div>,
    addControl: (
      <Button variant="outlined" color="primary">
        Add
      </Button>
    ),
    removeControl: (
      <Button variant="outlined" color="error">
        Remove
      </Button>
    ),
    onAddItem: () => console.log("onAddItem"),
    onRemoveItem: (id: string) => console.log("onRemoveItem", id),
    onItemsChange: (items: ListItem[]) => console.log("onItemsChange", items),
  },
}
