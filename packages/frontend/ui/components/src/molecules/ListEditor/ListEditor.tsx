import React, { useState, useEffect } from "react"
import DraggableList, {
  DraggableListItem,
} from "../../structure/DraggableList/DraggableList"
import styles from "./ListEditor.module.css"
import { classNames } from "@punks/ui-core"

export type ListItem = {
  id: string
}

export type ListEditorClasses = {
  root?: string
  item?: string
  addControl?: string
  removeControl?: string
}

export interface ListEditorProps<T extends ListItem> {
  id: string
  items?: T[]
  onItemsChange?: (items: T[]) => void
  renderItem: (item: T) => React.ReactNode
  onAddItem?: () => void
  onRemoveItem?: (id: string) => void
  addControl?: React.ReactNode
  removeControl?: React.ReactNode
  className?: string
  classes?: ListEditorClasses
}

const ListEditor = <T extends ListItem>({
  id,
  items = [],
  onItemsChange,
  renderItem,
  onAddItem,
  onRemoveItem,
  addControl,
  removeControl,
  className,
  classes,
}: ListEditorProps<T>) => {
  const [internalItems, setInternalItems] = useState<T[]>(items)

  useEffect(() => {
    setInternalItems(items)
  }, [items])

  const handleItemsChange = (newItems: T[]) => {
    setInternalItems(newItems)
    onItemsChange && onItemsChange(newItems)
  }

  const handleDragEnd = (result: any) => {
    if (!result.destination) return

    const newItems = Array.from(internalItems)
    const [removed] = newItems.splice(result.source.index, 1)
    newItems.splice(result.destination.index, 0, removed)

    handleItemsChange(newItems)
  }

  const handleAddItem = () => {
    onAddItem && onAddItem()
  }

  const handleRemoveItem = (id: string) => {
    onRemoveItem && onRemoveItem(id)
  }

  const draggableItems: DraggableListItem[] = internalItems.map((item) => ({
    id: item.id,
    content: (
      <div
        key={item.id}
        className={classNames(styles.optionItem, classes?.item)}
      >
        {renderItem(item)}
        {removeControl && (
          <div
            className={classNames(styles.removeControl, classes?.removeControl)}
            onClick={() => handleRemoveItem(item.id)}
          >
            {removeControl}
          </div>
        )}
      </div>
    ),
  }))

  return (
    <div
      className={classNames(styles.editorContainer, classes?.root, className)}
    >
      <DraggableList id={id} items={draggableItems} onDragEnd={handleDragEnd} />
      {addControl && onAddItem && (
        <div
          className={classNames(styles.addControl, classes?.addControl)}
          onClick={handleAddItem}
        >
          {addControl}
        </div>
      )}
    </div>
  )
}

export default ListEditor
