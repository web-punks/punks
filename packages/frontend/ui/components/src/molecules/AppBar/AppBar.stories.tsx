import type { Meta, StoryObj } from "@storybook/react"

import { AppBar } from "."

const meta = {
  title: "Ui/Molecules/AppBar",
  component: AppBar,
  tags: ["autodocs"],
  argTypes: {},
} satisfies Meta<typeof AppBar>
export default meta

type Story = StoryObj<typeof meta>

export const AppBarSmall: Story = {
  args: {
    children: <div>content</div>,
    size: "small",
  },
}

export const AppBarMedium: Story = {
  args: {
    children: <div>content</div>,
    size: "medium",
  },
}

export const AppBarLarge: Story = {
  args: {
    children: <div>content</div>,
    size: "large",
  },
}
