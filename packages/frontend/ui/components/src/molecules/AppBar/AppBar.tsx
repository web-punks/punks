import React from "react"
import styles from "./AppBar.module.css"
import { classNames } from "@punks/ui-core"

export type AppBarSize = "small" | "medium" | "large"

export interface AppBarProps {
  children?: any
  size?: AppBarSize
  component?: keyof JSX.IntrinsicElements
  className?: string
}

const AppBar = ({
  children,
  size = "medium",
  component,
  className,
}: AppBarProps) => {
  const Tag = component ?? "div"
  return (
    <Tag className={classNames(styles.root, styles[size], className)}>
      {children}
    </Tag>
  )
}

export default AppBar
