import {
  ThemeColor,
  classNames,
  normalizePxSize,
  setCssVar,
} from "@punks/ui-core"
import styles from "./DropButtonMenu.module.css"
import { Button } from "../../atoms/Button"
import { useEffect, useRef, useState } from "react"
import { Anchor } from "../../structure/Anchor"

export type DropButtonMenuSize = "sm" | "md" | "lg" | "auto"
export type DropButtonMenuVariant = "filled" | "outlined" | "text"
export type DropButtonMenuAlignment = "start" | "end"

export type DropButtonMenuItem = {
  label: React.ReactNode
  onClick: () => void
  className?: string
}

export type DropButtonMenuClasses = {
  root?: string
  items?: string
  item?: string
}

export interface DropButtonMenuProps {
  fullWidth?: boolean
  color?: ThemeColor
  size?: DropButtonMenuSize
  variant?: DropButtonMenuVariant
  items: DropButtonMenuItem[]
  children: React.ReactNode
  rounded?: boolean
  className?: string
  classes?: DropButtonMenuClasses
  alignment?: DropButtonMenuAlignment
  iconOnly?: boolean
  width?: string | number
  panelZIndex?: number
}

const DropButtonMenu = ({
  color = "primary",
  fullWidth,
  size = "md",
  variant = "filled",
  alignment = "start",
  items,
  iconOnly,
  children,
  rounded,
  className,
  classes,
  width,
  panelZIndex,
}: DropButtonMenuProps) => {
  const [isOpen, setIsOpen] = useState(false)
  const panelRef = useRef<HTMLDivElement | null>(null)

  const toggleDropdown = (e: MouseEvent) => {
    e.preventDefault()
    setIsOpen(!isOpen)
  }

  const handleItemClick = (e: MouseEvent, item: DropButtonMenuItem) => {
    e.preventDefault()
    item.onClick?.()
    setIsOpen(false)
  }

  const handleOutsideClick = (event: MouseEvent) => {
    if (panelRef.current && !panelRef.current.contains(event.target as Node)) {
      setIsOpen(false)
    }
  }

  useEffect(() => {
    window.addEventListener("click", handleOutsideClick)
    return () => {
      window.removeEventListener("click", handleOutsideClick)
    }
  }, [])

  return (
    <Anchor
      open={isOpen}
      onOpenChange={(open) => setIsOpen(open)}
      backdrop={false}
      zIndex={panelZIndex ?? 2}
      position={alignment === "start" ? "bottomLeft" : "bottomRight"}
      content={
        <div
          className={classNames(
            styles.items,
            {
              [styles[`itemsSize-${size}`]]: !width,
              [styles.itemsFixed]: !!width,
            },
            classes?.items
          )}
        >
          {items.map((item, index) => (
            <div
              key={index}
              className={classNames(styles.item, classes?.item, item.className)}
            >
              <Button
                onClick={(e) => handleItemClick(e as any, item)}
                variant="text"
                fullWidth
              >
                {item.label}
              </Button>
            </div>
          ))}
        </div>
      }
    >
      <div
        style={{
          ...(width ? setCssVar("--w", normalizePxSize(width)) : {}),
        }}
        className={classNames(
          styles.root,
          styles[variant],
          styles[`${variant}-${size}`],
          {
            [styles.fullWidth]: fullWidth,
            [styles.fixedWith]: !!width,
            [styles[`${variant}-rounded-${size}`]]: rounded,
          },
          classes?.root,
          className
        )}
        ref={panelRef}
      >
        <Button
          color={color}
          fullWidth={fullWidth || !!width}
          size={size !== "auto" ? size : undefined}
          variant={variant}
          onClick={(e) => toggleDropdown(e as any)}
          iconOnly={iconOnly}
          rounded={rounded}
        >
          {children}
        </Button>
      </div>
    </Anchor>
  )
}

export default DropButtonMenu
