import { useDebounce } from "@punks/ui-core"
import { TinyMCE } from "../../integrations"
import { InputHTMLAttributes, useEffect, useMemo, useState } from "react"
import React from "react"
import {
  RichText,
  htmlToRichText,
  richTextToHtml,
} from "../../converters/richText"

export interface RichTextEditorProps
  extends Omit<InputHTMLAttributes<HTMLInputElement>, "size"> {
  debounceMs?: number
  inputRef?: React.Ref<HTMLInputElement>
  className?: string
  initialValue?: string
  value?: string
  onValueChange?: (value: string) => void
  height?: number
}

const normalizeInput = (value?: string) => {
  if (!value?.trim()) {
    return "[]"
  }

  return value
}

const parseInput = (value?: string) => {
  try {
    return JSON.parse(normalizeInput(value))
  } catch (e) {
    console.error(`Failed to parse input: ${value}`, e)
    throw new Error(`Failed to parse input: ${value}`)
  }
}

const RichTextEditor = ({
  initialValue,
  value,
  onValueChange,
  onChange,
  debounceMs = 150,
  className,
  inputRef,
  height,
  ...other
}: RichTextEditorProps) => {
  const initialRichTextValue = useMemo(
    () => parseInput(initialValue ?? value),
    []
  )
  const initialRichText = useMemo(
    () => richTextToHtml(initialRichTextValue),
    []
  )
  const [htmlValue, setHtmlValue] = useState(initialRichText)

  const debouncedHtmlValue = useDebounce(htmlValue, debounceMs)

  const handleHtmlValueChange = (value: string) => {
    setHtmlValue(value)
  }

  const handleOnChange = (richText: RichText) => {
    onChange?.({
      type: "text",
      target: {
        name: other.name,
        value: JSON.stringify(richText),
      },
    } as any)
  }

  const updateRichTextValue = (htmlValue?: string) => {
    const richText = htmlValue ? htmlToRichText(htmlValue) : []
    handleOnChange(richText)
  }

  useEffect(() => {
    updateRichTextValue(debouncedHtmlValue)
  }, [debouncedHtmlValue])

  useEffect(() => {
    handleOnChange(initialRichTextValue)
  }, [])

  return (
    <>
      <input
        type="text"
        onChange={onChange}
        ref={inputRef}
        hidden
        readOnly
        {...other}
      />
      <TinyMCE
        initialValue={initialRichText}
        onChange={handleHtmlValueChange}
        className={className}
        height={height}
      />
    </>
  )
}

export const RefRichTextEditor = React.forwardRef<
  HTMLInputElement,
  RichTextEditorProps
>((props, ref) => (
  <RichTextEditor {...props} inputRef={ref ?? props.inputRef} />
))

export default RefRichTextEditor
