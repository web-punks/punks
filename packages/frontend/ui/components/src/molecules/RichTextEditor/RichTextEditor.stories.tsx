import type { Meta, StoryObj } from "@storybook/react"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"

import { RichTextEditor } from "."
import { useForm } from "react-hook-form"
import { Button } from "../../atoms"
import { Box } from "../../structure"

const meta = {
  title: "Ui/Molecules/RichTextEditor",
  component: RichTextEditor,
  argTypes: {},
  tags: ["autodocs"],
} satisfies Meta<typeof RichTextEditor>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
}

export const WithReactHookForm: Story = {
  args: {},
  decorators: [
    (Story, props) => {
      const schema = yup.object().shape({
        value: yup.string().required(),
      })
      const {
        register,
        handleSubmit,
        formState: { errors },
      } = useForm({
        resolver: yupResolver(schema),
      })
      return (
        <div>
          <Story
            args={{
              ...props.args,
              ...register("value"),
            }}
          />
          <Box mt={2}>
            <Button
              variant="filled"
              color="primary"
              fullWidth
              onClick={handleSubmit((data) => console.log("submit data", data))}
            >
              submit
            </Button>
          </Box>
        </div>
      )
    },
  ],
}

const InitialValue = `[{"_type":"block","_key":"svlnv109","style":"normal","children":[{"_type":"span","_key":"tivnatft","text":"lorem ipsium","marks":[]}]},{"_type":"block","_key":"ar8mi2wp","style":"normal","children":[{"_type":"span","_key":"pinbn8tc","text":"lorem ipsium","marks":["textRight"]}]},{"_type":"block","_key":"09ubzc9q","style":"normal","children":[{"_type":"span","_key":"r017511f","text":"lorem ipsium","marks":["textCenter","strong"]}]}]`

export const WithReactHookFormAndInitialValue: Story = {
  args: {
    initialValue: InitialValue,
  },
  decorators: [
    (Story, props) => {
      const schema = yup.object().shape({
        value: yup.string().required(),
      })
      const {
        register,
        handleSubmit,
        formState: { errors },
      } = useForm({
        resolver: yupResolver(schema),
      })
      return (
        <div>
          <Story
            args={{
              ...props.args,
              ...register("value"),
            }}
          />
          <Box mt={2}>
            <Button
              variant="filled"
              color="primary"
              fullWidth
              onClick={handleSubmit((data) => console.log("submit data", data))}
            >
              submit
            </Button>
          </Box>
        </div>
      )
    },
  ],
}
