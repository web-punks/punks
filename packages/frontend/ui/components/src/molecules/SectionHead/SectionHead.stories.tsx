import type { Meta, StoryObj } from "@storybook/react"

import SectionHead from "./SectionHead"

const meta = {
  title: "Ui/Molecules/SectionHead",
  component: SectionHead,
  tags: ["autodocs"],
  argTypes: {},
  args: {},
} satisfies Meta<typeof SectionHead>
export default meta

type Story = StoryObj<typeof meta>

export const HeadWithTitle: Story = {
  args: {
    title: "Title",
  },
}

export const HeadWithTitleAndToolbar: Story = {
  args: {
    title: "Title",
    toolbar: <div>Toolbar</div>,
  },
}
