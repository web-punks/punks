export {
  default as SectionHead,
  SectionHeadProps,
  SectionHeadClasses,
} from "./SectionHead"
