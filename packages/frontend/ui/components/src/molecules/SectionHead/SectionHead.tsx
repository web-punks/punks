import { classNames } from "@punks/ui-core"
import { Typography } from "../../atoms/Typography"
import styles from "./SectionHead.module.css"
import { Box } from "../../structure"

export type SectionHeadClasses = {
  root?: string
  title?: string
  subtitle?: string
  toolbar?: string
}

export interface SectionHeadProps {
  title?: React.ReactNode
  subtitle?: React.ReactNode
  toolbar?: React.ReactNode
  children?: any
  bordered?: boolean
  className?: string
  classes?: SectionHeadClasses
}

const SectionHead = ({
  title,
  subtitle,
  toolbar,
  bordered,
  className,
  classes,
}: SectionHeadProps) => {
  return (
    <Box
      pt={4}
      pb={2}
      className={classNames(
        styles.root,
        {
          [styles.bordered]: bordered,
        },
        className,
        classes?.root
      )}
    >
      <div>
        {title && (
          <Typography variant="h4" mb={1} className={classes?.title}>
            {title}
          </Typography>
        )}
        {subtitle && (
          <Typography variant="caption1" mt={1} className={classes?.subtitle}>
            {subtitle}
          </Typography>
        )}
      </div>
      {toolbar && <div className={classes?.toolbar}>{toolbar}</div>}
    </Box>
  )
}

export default SectionHead
