import JsonFieldInput from "./JsonFieldInput"
import type { Meta, StoryObj } from "@storybook/react"

interface CustomItem {
  id: string
  value: string
}

const TypedJsonFieldInput = JsonFieldInput<CustomItem>

const meta: Meta = {
  title: "UI/Molecules/JsonFieldInput",
  component: TypedJsonFieldInput,
  args: {},
} satisfies Meta<typeof TypedJsonFieldInput>
export default meta

type Story = StoryObj<typeof TypedJsonFieldInput>

export const Default: Story = {
  args: {
    onChange: (value?: CustomItem) => console.log("onChange", value),
  },
}

export const WithDefaultValue: Story = {
  args: {
    defaultValue: { id: "1", value: "Initial value" },
    onChange: (value?: CustomItem) => console.log("onChange", value),
  },
}
