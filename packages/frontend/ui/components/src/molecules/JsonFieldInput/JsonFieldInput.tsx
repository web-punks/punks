import { useState } from "react"
import { TextArea, TextAreaProps } from "../../atoms"

export interface JsonFieldInputProps<T>
  extends Omit<TextAreaProps, "value" | "defaultValue" | "onChange"> {
  defaultValue?: T
  onChange: (value?: T) => void
}

const isValidJson = (value: string) => {
  try {
    if (value) {
      JSON.parse(value)
    }
    return true
  } catch (e) {
    return false
  }
}

const JsonFieldInput = <T,>({
  defaultValue,
  onChange,
  error,
  ...props
}: JsonFieldInputProps<T>) => {
  const [jsonValue, setJsonValue] = useState(
    JSON.stringify(defaultValue, null, 2)
  )

  const handleChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setJsonValue(event.target.value)
    if (!isValidJson(event.target.value)) {
      return
    }
    onChange(JSON.parse(event.target.value))
  }

  return (
    <TextArea
      value={jsonValue}
      onChange={handleChange}
      error={error || !isValidJson(jsonValue)}
      {...props}
    />
  )
}

export default JsonFieldInput
