import type { Meta, StoryObj } from "@storybook/react"
import { FileDropControl } from "."

const meta = {
  title: "UI/Molecules/FileDropControl",
  component: FileDropControl,
  tags: ["autodocs"],
  args: {
    placeholder: "Drop files here",
    cta: {
      label: "Upload",
    },
    onChange: (files: File[]) => {
      console.log("files changed", files)
    },
    onSubmit: (files: File[]) => {
      console.log("files submitted", files)
    },
  },
} satisfies Meta<typeof FileDropControl>
export default meta

type Story = StoryObj<typeof meta>

export const Single: Story = {}
export const SinglePdf: Story = {
  args: {
    accept: ".pdf",
  },
}
export const SingleFullWidth: Story = {
  args: {
    fullWidth: true,
  },
}

export const Multiple: Story = {
  args: {
    multiple: true,
  },
}

export const MultiplePdf: Story = {
  args: {
    multiple: true,
    accept: ".pdf",
  },
}

export const MultipleFullWidth: Story = {
  args: {
    multiple: true,
    fullWidth: true,
  },
}

export const FilesListHidden: Story = {
  args: {
    fullWidth: true,
    filesListHidden: true,
  },
}

export const AutoSubmit: Story = {
  args: {
    fullWidth: true,
    autoSubmit: true,
  },
}
