import React, { useState } from "react"
import styles from "./FileDropControl.module.css"
import { CloseIcon, FileIcon } from "../../ui/icons"
import { classNames } from "@punks/ui-core"
import { Button } from "../../atoms/Button"

export type FileDropControlClasses = {
  root?: string
  dragging?: string
  placeholder?: string
  filesList?: string
  file?: string
}

export type FileDropControlSubmitCta = {
  label: React.ReactNode
  loading?: boolean
}

export interface FileDropControlProps {
  accept?: string
  multiple?: boolean
  onChange?: (files: File[]) => void
  onSubmit?: (files: File[]) => void
  cta: FileDropControlSubmitCta
  placeholder: any
  placeholderHidden?: boolean
  fileIcon?: React.ReactNode
  fileRemoveIcon?: React.ReactNode
  fullWidth?: boolean
  className?: string
  classes?: FileDropControlClasses
  inputRef?: React.RefObject<HTMLInputElement>
  submitInProgress?: boolean
  filesListHidden?: boolean
  autoSubmit?: boolean
}

const FileDropControl: React.FC<FileDropControlProps> = ({
  accept,
  multiple = false,
  onChange,
  onSubmit,
  placeholder,
  placeholderHidden,
  fileIcon,
  fileRemoveIcon,
  fullWidth,
  className,
  classes,
  cta,
  inputRef,
  submitInProgress,
  filesListHidden,
  autoSubmit,
}) => {
  const hideFilesList = filesListHidden || autoSubmit
  const [dragging, setDragging] = useState(false)
  const [files, setFiles] = useState<File[]>([])
  const ref = inputRef ?? React.useRef<HTMLInputElement>(null)

  const cleanFileValue = () => {
    if (ref.current) {
      ref.current.value = ""
    }
  }

  const handleFilesSelect = (file: File[]) => {
    setFiles(file)
    if (autoSubmit) {
      onSubmit?.(file)
      return
    }
    onChange?.(file)
  }

  const handleDragEnter = (e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault()
    setDragging(true)
  }

  const handleDragOver = (e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault()
    e.dataTransfer.dropEffect = "copy"
  }

  const handleDragLeave = () => {
    setDragging(false)
  }

  const handleDrop = (e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault()
    setDragging(false)

    const newFiles = multiple
      ? Array.from(e.dataTransfer.files)
      : [e.dataTransfer.files[0]]

    handleFilesSelect(newFiles)
  }

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e.target.files) {
      return
    }
    const newFiles = multiple
      ? Array.from(e.target.files || [])
      : [e.target.files[0]]

    handleFilesSelect(newFiles)
  }

  const handleFileRemove = (index: number) => {
    const newFiles = files.filter((_, i) => i !== index)

    handleFilesSelect(newFiles)
  }

  const handleSubmit = () => {
    onSubmit?.(files)
    cleanFileValue()
  }

  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.dragging]: dragging,
          [classes?.dragging ?? ""]: dragging,
          [styles.fullWidth]: fullWidth,
        },
        classes?.root,
        className
      )}
      onDragEnter={handleDragEnter}
      onDragOver={handleDragOver}
      onDragLeave={handleDragLeave}
      onDrop={handleDrop}
    >
      <div
        className={classNames(
          styles.placeholder,
          {
            [styles.placeholderHidden]: placeholderHidden,
          },
          classes?.placeholder
        )}
        onClick={() => ref.current?.click()}
      >
        {placeholder}
      </div>
      <input
        type="file"
        ref={ref}
        accept={accept}
        multiple={multiple ?? false}
        onChange={handleInputChange}
        hidden
      />
      {!hideFilesList && files.length > 0 && (
        <ul className={classNames(styles.filesList, classes?.filesList)}>
          {files.map((file, index) => (
            <li key={index} className={classNames(styles.file, classes?.file)}>
              {fileIcon ?? <FileIcon width={20} height={20} />}
              {file.name}
              {fileRemoveIcon ?? (
                <CloseIcon
                  className={styles.fileRemoveIcon}
                  width={16}
                  height={16}
                  onClick={() => handleFileRemove(index)}
                />
              )}
            </li>
          ))}
        </ul>
      )}
      {!hideFilesList && files.length > 0 ? (
        <Button
          className={styles.cta}
          variant="outlined"
          color="primary"
          onClick={handleSubmit}
          loading={submitInProgress || cta?.loading}
        >
          {cta?.label}
        </Button>
      ) : undefined}
    </div>
  )
}

export default FileDropControl
