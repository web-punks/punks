import type { Meta, StoryObj } from "@storybook/react"

import { AccountInfo } from "."

const meta = {
  title: "Ui/Molecules/AccountInfo",
  component: AccountInfo,
  tags: ["autodocs"],
  args: {
    title: "Title",
    subtitle: "Subtitle",
  },
} satisfies Meta<typeof AccountInfo>
export default meta

type Story = StoryObj<typeof meta>

export const Outlined: Story = {
  args: {
    avatar: {
      initials: "AB",
      variant: "outlined",
    },
  },
}

export const Filled: Story = {
  args: {
    avatar: {
      initials: "AB",
      variant: "filled",
    },
  },
}
