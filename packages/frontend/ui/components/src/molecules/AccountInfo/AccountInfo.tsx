import { ClassValue } from "@punks/ui-core"
import Avatar, { AvatarVariant } from "../../atoms/Avatar/Avatar"
import { InlineBar } from "../../atoms/InlineBar"
import { Typography } from "../../atoms/Typography"

export type AvatarContent = {
  initials?: string
  image?: React.ReactNode
  variant?: AvatarVariant
}

export interface AccountInfoProps {
  title?: React.ReactNode
  subtitle?: React.ReactNode
  avatar: AvatarContent
  avatarClasses?: ClassValue
}

const AccountInfo = ({
  avatar,
  subtitle,
  title,
  avatarClasses,
}: AccountInfoProps) => {
  return (
    <InlineBar gap={4} inline>
      <div>
        <Typography align="right">{title}</Typography>
        <Typography align="right" weight="bold">
          {subtitle}
        </Typography>
      </div>
      {avatar.image ||
        (avatar.initials && (
          <Avatar
            image={avatar.image}
            initials={avatar.initials}
            variant={avatar.variant}
            className={avatarClasses}
          />
        ))}
    </InlineBar>
  )
}

export default AccountInfo
