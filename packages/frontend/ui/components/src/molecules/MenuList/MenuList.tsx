import React from "react"
import { ContextMenuIcon } from "../../ui/icons"
import { DropPanel } from "../../panels/DropPanel"
import { Box } from "../../structure"

export interface MenuListItem {
  content: React.ReactNode
  onClick?: () => void
}

export interface MenuSection {
  items: MenuListItem[]
}

export interface MenuClasses {
  button?: string
}

export interface MenuListProps {
  className?: string
  sections: MenuSection[]
}

export const MenuList = ({ className, sections }: MenuListProps) => {
  return (
    <DropPanel
      dropIconHidden
      control={<ContextMenuIcon width={24} />}
      className={className}
    >
      <Box flex flexDirection="column" gap={2}>
        {sections.map((section, i) => (
          <React.Fragment key={i}>
            {section.items.map((item, y) => (
              <Box
                key={y}
                onClick={() => {
                  close()
                  item.onClick?.()
                }}
                cursorPointer
              >
                {item.content}
              </Box>
            ))}
          </React.Fragment>
        ))}
      </Box>
    </DropPanel>
  )
}
