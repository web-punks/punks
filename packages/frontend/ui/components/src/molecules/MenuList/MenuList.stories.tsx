import type { Meta, StoryObj } from "@storybook/react"
import { MenuList } from "./MenuList"

const meta = {
  title: "UI/Molecules/MenuList",
  component: MenuList,
  tags: ["autodocs"],
  args: {},
  decorators: [
    (Story) => (
      <div style={{ paddingLeft: 80 }}>
        <Story />
      </div>
    ),
  ],
} satisfies Meta<typeof MenuList>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    sections: [
      {
        items: [
          {
            content: "Item 1",
          },
          {
            content: "Item 2",
          },
          {
            content: "Item 3",
          },
          {
            content: "Item 4",
          },
        ],
      },
    ],
  },
}
