import { Alert } from "../../atoms/Alert"
import { AppBar } from "../../molecules/AppBar"
import { Avatar } from "../../atoms/Avatar"
import { Box } from "../../structure/Box"
import { Button } from "../../atoms/Button"
import { CancelConfirmCta } from "../../molecules/CancelConfirmCta"
import { Card } from "../../molecules/Card"
import { Checkbox } from "../../atoms/Checkbox"
import { Collapsible } from "../../panels/Collapsible"
import { Col } from "../../structure/Col"
import { ConfirmationDialog } from "../../organisms/ConfirmationDialog"
import { Container } from "../../structure/Container"
import { Dot } from "../../atoms/Dot"
import { DropPanel } from "../../panels/DropPanel"
import { FormError } from "../../forms/FormError"
import { FormHelp } from "../../forms/FormHelp"
import { FormInput } from "../../forms/FormInput"
import { FormLabel } from "../../forms/FormLabel"
import { FormSection } from "../../forms/FormSection"
import { Frame } from "../../structure/Frame"
import { Header } from "../../molecules/Header"
import { Modal } from "../../panels/Modal"
import { ProgressBar } from "../../molecules/ProgressBar"
import { Ring } from "../../atoms/Ring"
import { Row } from "../../structure/Row"
import { Separator } from "../../structure/Separator"
import { Sidebar } from "../../organisms/Sidebar"
import { Table } from "../../organisms/Table"
import { TextInput } from "../../atoms/TextInput"
import { Typography } from "../../atoms/Typography"
import { UiRegistry } from "../template"
import { showAlert, withNotification } from "../../notifications/toast"
import { SectionHead } from "../../molecules/SectionHead"
import { InlineBar } from "../../atoms/InlineBar"
import { MenuList } from "../../molecules/MenuList"
import { MultiSelect } from "../../molecules/MultiSelect"
import { Select } from "../../atoms/Select"
import { Tag } from "../../atoms/Tag"
import { DropButton } from "../../molecules/DropButton"
import { DocumentItemsList } from "../../organisms/DocumentItemsList"
import { Dialog } from "../../organisms/Dialog"
import { FormContainer } from "../../forms/FormContainer"
import { FormRoot } from "../../organisms/FormRoot"
import { SearchBar } from "../../molecules/SearchBar"
import { FileDropControl } from "../../molecules/FileDropControl"
import { FileUploadPanel } from "../../organisms/FileUploadPanel"
import { DropMenu } from "../../molecules/DropMenu"
import { FormRow } from "../../forms/FormRow"
import {
  ChevronDoubleLeftIcon,
  ChevronDoubleRightIcon,
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  ChevronUpIcon,
  CloseIcon,
  CodeIcon,
  ContextMenuIcon,
  DeleteIcon,
  FilterIcon,
  FolderDetailIcon,
  MenuIcon,
  RefreshIcon,
  SearchIcon,
  DocumentIcon,
  DeniedIcon,
  PendingIcon,
  ApprovedIcon,
  EditIcon,
  EyeIcon,
  LinkIcon,
} from "../../ui/icons"
import { DashboardLayout, SplashScreenLayout } from "../../layouts"
import { Link } from "../../core"
import { FormHead, FormSeparator } from "../../forms"
import { Switch } from "../../atoms/Switch"
import { RichTextEditor } from "../../molecules/RichTextEditor"
import { Tabs } from "../../molecules/Tabs"
import { Stepper } from "../../molecules/Stepper"
import { DropDown } from "../../atoms/DropDown"
import { ScrollView } from "../../structure"
import { TreeList } from "../../organisms/TreeList"
import { TreeTable } from "../../organisms/TreeTable"
import { JsonViewer } from "../../organisms/JsonViewer"
import { PopoverMenu } from "../../atoms/PopoverMenu"
import { TagsBar } from "../../molecules/TagsBar"
import { DropButtonMenu } from "../../molecules/DropButtonMenu"
import { LoaderBar } from "../../molecules/LoaderBar"

export const initializeRegistry = (registry: UiRegistry) => {
  registry.components.registerAlert(Alert)
  registry.components.registerAppBar(AppBar)
  registry.components.registerAvatar(Avatar)
  registry.components.registerBox(Box)
  registry.components.registerButton(Button)
  registry.components.registerCancelConfirmCta(CancelConfirmCta)
  registry.components.registerCard(Card)
  registry.components.registerCheckbox(Checkbox)
  registry.components.registerCollapsible(Collapsible)
  registry.components.registerColumn(Col)
  registry.components.registerConfirmationDialog(ConfirmationDialog)
  registry.components.registerContainer(Container)
  registry.components.registerDot(Dot)
  registry.components.registerDialog(Dialog)
  registry.components.registerDropDown(DropDown)
  registry.components.registerDropPanel(DropPanel)
  registry.components.registerDropButton(DropButton)
  registry.components.registerDropButtonMenu(DropButtonMenu)
  registry.components.registerDropMenu(DropMenu)
  registry.components.registerFormContainer(FormContainer)
  registry.components.registerFormError(FormError)
  registry.components.registerFormHelp(FormHelp)
  registry.components.registerFormHead(FormHead)
  registry.components.registerFormInput(FormInput)
  registry.components.registerFormLabel(FormLabel)
  registry.components.registerFormRow(FormRow)
  registry.components.registerFormRoot(FormRoot)
  registry.components.registerFormSection(FormSection)
  registry.components.registerFormSeparator(FormSeparator)
  registry.components.registerFileDropControl(FileDropControl)
  registry.components.registerFileUploadPanel(FileUploadPanel)
  registry.components.registerFrame(Frame)
  registry.components.registerHeader(Header)
  registry.components.registerInlineBar(InlineBar)
  registry.components.registerJsonViewer(JsonViewer)
  registry.components.registerLink(Link)
  registry.components.registerLoaderBar(LoaderBar)
  registry.components.registerModal(Modal)
  registry.components.registerMenuList(MenuList)
  registry.components.registerMultiSelect(MultiSelect)
  registry.components.registerPopoverMenu(PopoverMenu)
  registry.components.registerProgressBar(ProgressBar)
  registry.components.registerRing(Ring)
  registry.components.registerRichTextEditor(RichTextEditor)
  registry.components.registerRow(Row)
  registry.components.registerSearchBar(SearchBar)
  registry.components.registerSectionHead(SectionHead)
  registry.components.registerSeparator(Separator)
  registry.components.registerSelect(Select)
  registry.components.registerSidebar(Sidebar)
  registry.components.registerStepper(Stepper)
  registry.components.registerSwitch(Switch)
  registry.components.registerTable(Table as any)
  registry.components.registerTabs(Tabs)
  registry.components.registerTag(Tag)
  registry.components.registerTagsBar(TagsBar)
  registry.components.registerTextInput(TextInput)
  registry.components.registerTreeList(TreeList)
  registry.components.registerTreeTable(TreeTable<any>)
  registry.components.registerDocumentItemsList(DocumentItemsList)
  registry.components.registerTypography(Typography)
  registry.components.registerSplashScreenLayout(SplashScreenLayout)
  registry.components.registerScrollView(ScrollView)
  registry.components.registerDashboardLayout(DashboardLayout)

  registry.registerAlertHandler(showAlert)
  registry.registerNotifiedOperationsHandler(withNotification)

  return registry
}

export const registerDefaultIcons = (registry: UiRegistry) => {
  registry.registerIcon("chevronDown", ChevronDownIcon)
  registry.registerIcon("chevronUp", ChevronUpIcon)
  registry.registerIcon("chevronLeft", ChevronLeftIcon)
  registry.registerIcon("chevronRight", ChevronRightIcon)
  registry.registerIcon("chevronDoubleLeft", ChevronDoubleLeftIcon)
  registry.registerIcon("chevronDoubleRight", ChevronDoubleRightIcon)
  registry.registerIcon("filter", FilterIcon)
  registry.registerIcon("search", SearchIcon)
  registry.registerIcon("document", DocumentIcon)
  registry.registerIcon("close", CloseIcon)
  registry.registerIcon("menu", MenuIcon)
  registry.registerIcon("options", ContextMenuIcon)
  registry.registerIcon("delete", DeleteIcon)
  registry.registerIcon("refresh", RefreshIcon)
  registry.registerIcon("code", CodeIcon)
  registry.registerIcon("eye", EyeIcon)
  registry.registerIcon("link", LinkIcon)
  registry.registerIcon("edit", EditIcon)
  registry.registerIcon("approved", ApprovedIcon)
  registry.registerIcon("denied", DeniedIcon)
  registry.registerIcon("pending", PendingIcon)

  return registry
}
