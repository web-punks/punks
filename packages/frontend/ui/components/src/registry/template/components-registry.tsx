import React, { ReactNode } from "react"

interface ComponentMap {
  [name: string]: React.ComponentType<any>
}

export class ComponentsRegistry {
  private components: ComponentMap = {}

  registerComponent(name: string, component: React.ComponentType<any>): void {
    this.components[name] = component
  }

  resolveComponent(name: string): React.ComponentType<any> | undefined {
    if (!this.components[name]) {
      throw new Error(`Component ${name} not found`)
    }
    return this.components[name]
  }

  renderComponent(name: string, props: any): ReactNode | null {
    const Component = this.resolveComponent(name)

    if (Component) {
      return React.createElement(Component, props)
    }

    console.error(`Component ${name} not found in registry`)
    return <></>
  }
}
