import React from "react"
import { ComponentsRegistry } from "./components-registry"
import { AlertProps } from "../../atoms/Alert"
import { AvatarProps } from "../../atoms/Avatar"
import { ButtonProps } from "../../atoms/Button/Button"
import { CheckboxProps } from "../../atoms/Checkbox"
import { DotProps } from "../../atoms/Dot/Dot"
import { RingProps } from "../../atoms/Ring/Ring"
import { SwitchProps } from "../../atoms/Switch/Switch"
import { TextInputProps } from "../../atoms/TextInput"
import { TypographyProps } from "../../atoms/Typography"
import { AppBarProps } from "../../molecules/AppBar"
import { CancelConfirmCtaProps } from "../../molecules/CancelConfirmCta"
import { CardProps } from "../../molecules/Card"
import { HeaderProps } from "../../molecules/Header"
import { ProgressBarProps } from "../../molecules/ProgressBar"
import { TableProps } from "../../organisms/Table"
import { ConfirmationDialogProps } from "../../organisms/ConfirmationDialog"
import { DropPanelProps } from "../../panels/DropPanel"
import { SidebarProps } from "../../organisms/Sidebar"
import { CollapsibleProps } from "../../panels/Collapsible"
import { ModalProps } from "../../panels/Modal"
import { BoxProps } from "../../structure/Box"
import { ColProps } from "../../structure/Col"
import { ContainerProps } from "../../structure/Container"
import { FrameProps } from "../../structure/Frame"
import { RowProps } from "../../structure/Row"
import { SeparatorProps } from "../../structure/Separator"
import { FormErrorProps } from "../../forms/FormError"
import { FormHelpProps } from "../../forms/FormHelp"
import { FormInputProps } from "../../forms/FormInput"
import { FormLabelProps } from "../../forms/FormLabel"
import { FormSectionProps } from "../../forms/FormSection"
import { SectionHeadProps } from "../../molecules/SectionHead"
import { InlineBarProps } from "../../atoms/InlineBar"
import { MenuListProps } from "../../molecules/MenuList"
import { DialogProps } from "../../organisms/Dialog"
import { DocumentItemsListProps } from "../../organisms/DocumentItemsList"
import { FormContainerProps } from "../../forms/FormContainer"
import { FormRootProps } from "../../organisms/FormRoot"
import { FileDropControlProps } from "../../molecules/FileDropControl"
import { FileUploadPanelProps } from "../../organisms/FileUploadPanel"
import { FormRowProps } from "../../forms/FormRow"
import { DashboardLayoutProps, SplashScreenLayoutProps } from "../../layouts"
import { LinkProps } from "../../core/Link"
import { FormHeadProps, FormSeparatorProps } from "../../forms"
import { ScrollViewProps } from "../../structure"
import { JsonViewerProps, TreeListProps, TreeTableProps } from "../../organisms"
import { DropDownProps } from "../../atoms/DropDown"
import { MultiSelectProps } from "../../molecules/MultiSelect"
import { PopoverMenuProps } from "../../atoms/PopoverMenu"
import { SelectProps } from "../../atoms/Select"
import { TagProps } from "../../atoms/Tag"
import { TagsBarProps } from "../../molecules/TagsBar"
import { DropButtonProps } from "../../molecules/DropButton"
import { DropMenuProps } from "../../molecules/DropMenu"
import { RichTextEditorProps } from "../../molecules/RichTextEditor"
import { SearchBarProps } from "../../molecules/SearchBar"
import { StepperProps } from "../../molecules/Stepper"
import { TabsProps } from "../../molecules/Tabs"
import { DropButtonMenuProps } from "../../molecules/DropButtonMenu"
import { LoaderBarProps } from "../../molecules/LoaderBar"

const ComponentNames = {
  Core: {
    Link: "Link",
  },
  Atoms: {
    Alert: "Alert",
    Avatar: "Avatar",
    Button: "Button",
    Checkbox: "Checkbox",
    Dot: "Dot",
    DropDown: "DropDown",
    MultiSelect: "MultiSelect",
    PopoverMenu: "PopoverMenu",
    Radio: "Radio",
    Ring: "Ring",
    Select: "Select",
    Switch: "Switch",
    Tag: "Tag",
    TextInput: "TextInput",
    Typography: "Typography",
  },
  Form: {
    FormContainer: "FormContainer",
    FormError: "FormError",
    FormHead: "FormHead",
    FormHelp: "FormHelp",
    FormInput: "FormInput",
    FormLabel: "FormLabel",
    FormRow: "FormRow",
    FormSection: "FormSection",
    FormSeparator: "FormSeparator",
  },
  Molecules: {
    AppBar: "AppBar",
    CancelConfirmCta: "CancelConfirmCta",
    Card: "Card",
    DropButton: "DropButton",
    DropButtonMenu: "DropButtonMenu",
    DropMenu: "DropMenu",
    FileDropControl: "FileDropControl",
    Header: "Header",
    InlineBar: "InlineBar",
    LoaderBar: "LoaderBar",
    MenuList: "MenuList",
    ProgressBar: "ProgressBar",
    RichTextEditor: "RichTextEditor",
    SearchBar: "SearchBar",
    SectionHead: "SectionHead",
    Stepper: "Stepper",
    Table: "Table",
    Tabs: "Tabs",
    TagsBar: "TagsBar",
  },
  Layouts: {
    DashboardLayout: "DashboardLayout",
    SplashScreenLayout: "SplashScreenLayout",
  },
  Organisms: {
    ConfirmationDialog: "ConfirmationDialog",
    Dialog: "Dialog",
    DocumentItemsList: "DocumentItemsList",
    FormRoot: "FormRoot",
    FileUploadPanel: "FileUploadPanel",
    JsonViewer: "JsonViewer",
    Sidebar: "Sidebar",
    SteppedPanel: "SteppedPanel",
    TreeList: "TreeList",
    TreeTable: "TreeTable",
  },
  Panels: {
    Collapsible: "Collapsible",
    Drawer: "Drawer",
    DropPanel: "DropPanel",
    Modal: "Modal",
  },
  Shapes: {
    Circle: "Circle",
    Rectangle: "Rectangle",
    Square: "Square",
  },
  Structure: {
    Box: "Box",
    Col: "Col",
    Container: "Container",
    Frame: "Frame",
    Row: "Row",
    ScrollView: "ScrollView",
    Separator: "Separator",
  },
}

export class ComponentsManager {
  constructor(private readonly componentsRegistry: ComponentsRegistry) {}

  // (core)

  registerLink(component: React.ComponentType<LinkProps>) {
    this.registerComponent(ComponentNames.Core.Link, component)
  }

  renderLink(props: LinkProps) {
    return this.renderComponent(ComponentNames.Core.Link, props)
  }

  // (atoms)

  registerAlert(component: React.ComponentType<AlertProps>) {
    this.registerComponent(ComponentNames.Atoms.Alert, component)
  }

  renderAlert(props: AlertProps) {
    return this.renderComponent(ComponentNames.Atoms.Alert, props)
  }

  registerAvatar(component: React.ComponentType<AvatarProps>) {
    this.registerComponent(ComponentNames.Atoms.Avatar, component)
  }

  renderAvatar(props: AvatarProps) {
    return this.renderComponent(ComponentNames.Atoms.Avatar, props)
  }

  registerButton(component: React.ComponentType<ButtonProps>) {
    this.registerComponent(ComponentNames.Atoms.Button, component)
  }

  renderButton(props: ButtonProps) {
    return this.renderComponent(ComponentNames.Atoms.Button, props)
  }

  registerCheckbox(component: React.ComponentType<CheckboxProps>) {
    this.registerComponent(ComponentNames.Atoms.Checkbox, component)
  }

  renderCheckbox(props: CheckboxProps) {
    return this.renderComponent(ComponentNames.Atoms.Checkbox, props)
  }

  registerDot(component: React.ComponentType<DotProps>) {
    this.registerComponent(ComponentNames.Atoms.Dot, component)
  }

  renderDot(props: DotProps) {
    return this.renderComponent(ComponentNames.Atoms.Dot, props)
  }

  registerDropDown(component: React.ComponentType<DropDownProps>) {
    this.registerComponent(ComponentNames.Atoms.DropDown, component)
  }

  renderDropDown(props: DropDownProps) {
    return this.renderComponent(ComponentNames.Atoms.DropDown, props)
  }

  registerMultiSelect(component: React.ComponentType<MultiSelectProps>) {
    this.registerComponent(ComponentNames.Atoms.MultiSelect, component)
  }

  renderMultiSelect(props: MultiSelectProps) {
    return this.renderComponent(ComponentNames.Atoms.MultiSelect, props)
  }

  registerPopoverMenu(component: React.ComponentType<PopoverMenuProps>) {
    this.registerComponent(ComponentNames.Atoms.PopoverMenu, component)
  }

  renderPopoverMenu(props: PopoverMenuProps) {
    return this.renderComponent(ComponentNames.Atoms.PopoverMenu, props)
  }

  registerRing(component: React.ComponentType<RingProps>) {
    this.registerComponent(ComponentNames.Atoms.Ring, component)
  }

  renderRing(props: RingProps) {
    return this.renderComponent(ComponentNames.Atoms.Ring, props)
  }

  registerSelect(component: React.ComponentType<SelectProps>) {
    this.registerComponent(ComponentNames.Atoms.Select, component)
  }

  renderSelect(props: SelectProps) {
    return this.renderComponent(ComponentNames.Atoms.Select, props)
  }

  registerSwitch(component: React.ComponentType<SwitchProps>) {
    this.registerComponent(ComponentNames.Atoms.Switch, component)
  }

  renderSwitch(props: SwitchProps) {
    return this.renderComponent(ComponentNames.Atoms.Switch, props)
  }

  registerTag(component: React.ComponentType<TagProps>) {
    this.registerComponent(ComponentNames.Atoms.Tag, component)
  }

  renderTagsBar(props: TagsBarProps) {
    return this.renderComponent(ComponentNames.Molecules.TagsBar, props)
  }

  registerTagsBar(component: React.ComponentType<TagsBarProps>) {
    this.registerComponent(ComponentNames.Molecules.TagsBar, component)
  }

  renderTag(props: TagProps) {
    return this.renderComponent(ComponentNames.Atoms.Tag, props)
  }

  registerTextInput(component: React.ComponentType<TextInputProps>) {
    this.registerComponent(ComponentNames.Atoms.TextInput, component)
  }

  renderTextInput(props: TextInputProps) {
    return this.renderComponent(ComponentNames.Atoms.TextInput, props)
  }

  registerTypography(component: React.ComponentType<TypographyProps>) {
    this.registerComponent(ComponentNames.Atoms.Typography, component)
  }

  renderTypography(props: TypographyProps) {
    return this.renderComponent(ComponentNames.Atoms.Typography, props)
  }

  // (molecules)

  registerAppBar(component: React.ComponentType<AppBarProps>) {
    this.registerComponent(ComponentNames.Molecules.AppBar, component)
  }

  renderAppBar(props: AppBarProps) {
    return this.renderComponent(ComponentNames.Molecules.AppBar, props)
  }

  registerCancelConfirmCta(
    component: React.ComponentType<CancelConfirmCtaProps>
  ) {
    this.registerComponent(ComponentNames.Molecules.CancelConfirmCta, component)
  }

  renderCancelConfirmCta(props: CancelConfirmCtaProps) {
    return this.renderComponent(
      ComponentNames.Molecules.CancelConfirmCta,
      props
    )
  }

  registerCard(component: React.ComponentType<CardProps>) {
    this.registerComponent(ComponentNames.Molecules.Card, component)
  }

  renderCard(props: CardProps) {
    return this.renderComponent(ComponentNames.Molecules.Card, props)
  }

  registerDropButton(component: React.ComponentType<DropButtonProps>) {
    this.registerComponent(ComponentNames.Molecules.DropButton, component)
  }

  renderDropButton(props: DropButtonProps) {
    return this.renderComponent(ComponentNames.Molecules.DropButton, props)
  }

  registerDropButtonMenu(component: React.ComponentType<DropButtonMenuProps>) {
    this.registerComponent(ComponentNames.Molecules.DropButtonMenu, component)
  }

  renderDropButtonMenu(props: DropButtonMenuProps) {
    return this.renderComponent(ComponentNames.Molecules.DropButtonMenu, props)
  }

  registerDropMenu(component: React.ComponentType<DropMenuProps>) {
    this.registerComponent(ComponentNames.Molecules.DropMenu, component)
  }

  renderDropMenu(props: DropMenuProps) {
    return this.renderComponent(ComponentNames.Molecules.DropMenu, props)
  }

  registerFileDropControl(
    component: React.ComponentType<FileDropControlProps>
  ) {
    this.registerComponent(ComponentNames.Molecules.FileDropControl, component)
  }

  renderFileDropControl(props: FileDropControlProps) {
    return this.renderComponent(ComponentNames.Molecules.FileDropControl, props)
  }

  registerHeader(component: React.ComponentType<HeaderProps>) {
    this.registerComponent(ComponentNames.Molecules.Header, component)
  }

  renderHeader(props: HeaderProps) {
    return this.renderComponent(ComponentNames.Molecules.Header, props)
  }

  registerInlineBar(component: React.ComponentType<InlineBarProps>) {
    this.registerComponent(ComponentNames.Molecules.InlineBar, component)
  }

  renderInlineBar(props: InlineBarProps) {
    return this.renderComponent(ComponentNames.Molecules.InlineBar, props)
  }

  registerLoaderBar(component: React.ComponentType<LoaderBarProps>) {
    this.registerComponent(ComponentNames.Molecules.LoaderBar, component)
  }

  renderLoaderBar(props: LoaderBarProps) {
    return this.renderComponent(ComponentNames.Molecules.LoaderBar, props)
  }

  registerMenuList(component: React.ComponentType<MenuListProps>) {
    this.registerComponent(ComponentNames.Molecules.MenuList, component)
  }

  renderMenuList(props: MenuListProps) {
    return this.renderComponent(ComponentNames.Molecules.MenuList, props)
  }

  registerProgressBar(component: React.ComponentType<ProgressBarProps>) {
    this.registerComponent(ComponentNames.Molecules.ProgressBar, component)
  }

  renderProgressBar(props: ProgressBarProps) {
    return this.renderComponent(ComponentNames.Molecules.ProgressBar, props)
  }

  registerRichTextEditor(component: React.ComponentType<RichTextEditorProps>) {
    this.registerComponent(ComponentNames.Molecules.RichTextEditor, component)
  }

  renderRichTextEditor(props: RichTextEditorProps) {
    return this.renderComponent(ComponentNames.Molecules.RichTextEditor, props)
  }

  registerSearchBar(component: React.ComponentType<SearchBarProps>) {
    this.registerComponent(ComponentNames.Molecules.SearchBar, component)
  }

  renderSearchBar(props: SearchBarProps) {
    return this.renderComponent(ComponentNames.Molecules.SearchBar, props)
  }

  registerSectionHead(component: React.ComponentType<SectionHeadProps>) {
    this.registerComponent(ComponentNames.Molecules.SectionHead, component)
  }

  renderSectionHead(props: SectionHeadProps) {
    return this.renderComponent(ComponentNames.Molecules.SectionHead, props)
  }

  registerStepper(component: React.ComponentType<StepperProps>) {
    this.registerComponent(ComponentNames.Molecules.Stepper, component)
  }

  renderStepper(props: StepperProps) {
    return this.renderComponent(ComponentNames.Molecules.Stepper, props)
  }

  registerTable(component: React.ComponentType<TableProps<unknown, unknown>>) {
    this.registerComponent(ComponentNames.Molecules.Table, component)
  }

  renderTable<RecordType, SortingType>(
    props: TableProps<RecordType, SortingType>
  ) {
    return this.renderComponent(ComponentNames.Molecules.Table, props)
  }

  registerTabs(component: React.ComponentType<TabsProps>) {
    this.registerComponent(ComponentNames.Molecules.Tabs, component)
  }

  renderTabs(props: TabsProps) {
    return this.renderComponent(ComponentNames.Molecules.Tabs, props)
  }

  // (layouts)

  registerDashboardLayout(
    component: React.ComponentType<DashboardLayoutProps>
  ): void {
    this.registerComponent(ComponentNames.Layouts.DashboardLayout, component)
  }

  renderDashboardLayout(props: DashboardLayoutProps) {
    return this.renderComponent(ComponentNames.Layouts.DashboardLayout, props)
  }

  registerSplashScreenLayout(
    component: React.ComponentType<SplashScreenLayoutProps>
  ): void {
    this.registerComponent(ComponentNames.Layouts.SplashScreenLayout, component)
  }

  renderSplashScreenLayout(props: SplashScreenLayoutProps) {
    return this.renderComponent(
      ComponentNames.Layouts.SplashScreenLayout,
      props
    )
  }

  // (organisms)

  registerConfirmationDialog(
    component: React.ComponentType<ConfirmationDialogProps>
  ) {
    this.registerComponent(
      ComponentNames.Organisms.ConfirmationDialog,
      component
    )
  }

  renderConfirmationDialog(props: ConfirmationDialogProps) {
    return this.renderComponent(
      ComponentNames.Organisms.ConfirmationDialog,
      props
    )
  }

  registerDialog(component: React.ComponentType<DialogProps>) {
    this.registerComponent(ComponentNames.Organisms.Dialog, component)
  }

  renderDialog(props: DialogProps) {
    return this.renderComponent(ComponentNames.Organisms.Dialog, props)
  }

  registerFormRoot(component: React.ComponentType<FormRootProps>) {
    this.registerComponent(ComponentNames.Organisms.FormRoot, component)
  }

  renderFormRoot(props: FormRootProps) {
    return this.renderComponent(ComponentNames.Organisms.FormRoot, props)
  }

  registerFileUploadPanel(
    component: React.ComponentType<FileUploadPanelProps>
  ) {
    this.registerComponent(ComponentNames.Organisms.FileUploadPanel, component)
  }

  renderFileUploadPanel(props: FileUploadPanelProps) {
    return this.renderComponent(ComponentNames.Organisms.FileUploadPanel, props)
  }

  registerJsonViewer(component: React.ComponentType<JsonViewerProps>) {
    this.registerComponent(ComponentNames.Organisms.JsonViewer, component)
  }

  renderJsonViewer(props: JsonViewerProps) {
    return this.renderComponent(ComponentNames.Organisms.JsonViewer, props)
  }

  registerSidebar(component: React.ComponentType<SidebarProps>) {
    this.registerComponent(ComponentNames.Organisms.Sidebar, component)
  }

  renderSidebar(props: SidebarProps) {
    return this.renderComponent(ComponentNames.Organisms.Sidebar, props)
  }

  registerDocumentItemsList(
    component: React.ComponentType<DocumentItemsListProps>
  ) {
    this.registerComponent(
      ComponentNames.Organisms.DocumentItemsList,
      component
    )
  }

  renderDocumentItemsList(props: DocumentItemsListProps) {
    return this.renderComponent(
      ComponentNames.Organisms.DocumentItemsList,
      props
    )
  }

  registerTreeList(component: React.ComponentType<TreeListProps>) {
    this.registerComponent(ComponentNames.Organisms.TreeList, component)
  }

  renderTreeList(props: TreeListProps) {
    return this.renderComponent(ComponentNames.Organisms.TreeList, props)
  }

  registerTreeTable(component: React.ComponentType<TreeTableProps<unknown>>) {
    this.registerComponent(ComponentNames.Organisms.TreeTable, component)
  }

  renderTreeTable<RecordType>(props: TreeTableProps<RecordType>) {
    return this.renderComponent(ComponentNames.Organisms.TreeTable, props)
  }

  // (panels)

  registerCollapsible(component: React.ComponentType<CollapsibleProps>) {
    this.registerComponent(ComponentNames.Panels.Collapsible, component)
  }

  renderCollapsible(props: CollapsibleProps) {
    return this.renderComponent(ComponentNames.Panels.Collapsible, props)
  }

  registerDropPanel(component: React.ComponentType<DropPanelProps>) {
    this.registerComponent(ComponentNames.Panels.DropPanel, component)
  }

  renderDropPanel(props: DropPanelProps) {
    return this.renderComponent(ComponentNames.Panels.DropPanel, props)
  }

  registerModal(component: React.ComponentType<ModalProps>) {
    this.registerComponent(ComponentNames.Panels.Modal, component)
  }

  renderModal(props: ModalProps) {
    return this.renderComponent(ComponentNames.Panels.Modal, props)
  }

  // (structure)

  registerBox(component: React.ComponentType<BoxProps>) {
    this.registerComponent(ComponentNames.Structure.Box, component)
  }

  renderBox(props: BoxProps) {
    return this.renderComponent(ComponentNames.Structure.Box, props)
  }

  registerColumn(component: React.ComponentType<ColProps>) {
    this.registerComponent(ComponentNames.Structure.Col, component)
  }

  renderColumn(props: ColProps) {
    return this.renderComponent(ComponentNames.Structure.Col, props)
  }

  registerContainer(component: React.ComponentType<ContainerProps>) {
    this.registerComponent(ComponentNames.Structure.Container, component)
  }

  renderContainer(props: ContainerProps) {
    return this.renderComponent(ComponentNames.Structure.Container, props)
  }

  registerFrame(component: React.ComponentType<FrameProps>) {
    this.registerComponent(ComponentNames.Structure.Frame, component)
  }

  renderFrame(props: FrameProps) {
    return this.renderComponent(ComponentNames.Structure.Frame, props)
  }

  registerRow(component: React.ComponentType<RowProps>) {
    this.registerComponent(ComponentNames.Structure.Row, component)
  }

  renderRow(props: RowProps) {
    return this.renderComponent(ComponentNames.Structure.Row, props)
  }

  registerScrollView(component: React.ComponentType<ScrollViewProps>) {
    this.registerComponent(ComponentNames.Structure.ScrollView, component)
  }

  renderScrollView(props: ScrollViewProps) {
    return this.renderComponent(ComponentNames.Structure.ScrollView, props)
  }

  registerSeparator(component: React.ComponentType<SeparatorProps>) {
    this.registerComponent(ComponentNames.Structure.Separator, component)
  }

  renderSeparator(props: SeparatorProps) {
    return this.renderComponent(ComponentNames.Structure.Separator, props)
  }

  // forms

  registerFormContainer(component: React.ComponentType<FormContainerProps>) {
    this.registerComponent(ComponentNames.Form.FormContainer, component)
  }

  renderFormContainer(props: FormContainerProps) {
    return this.renderComponent(ComponentNames.Form.FormContainer, props)
  }

  registerFormError(component: React.ComponentType<FormErrorProps>) {
    this.registerComponent(ComponentNames.Form.FormError, component)
  }

  renderFormError(props: FormErrorProps) {
    return this.renderComponent(ComponentNames.Form.FormError, props)
  }

  registerFormHead(component: React.ComponentType<FormHeadProps>) {
    this.registerComponent(ComponentNames.Form.FormHead, component)
  }

  renderFormHead(props: FormHeadProps) {
    return this.renderComponent(ComponentNames.Form.FormHead, props)
  }

  registerFormHelp(component: React.ComponentType<FormHelpProps>) {
    this.registerComponent(ComponentNames.Form.FormHelp, component)
  }

  renderFormHelp(props: FormHelpProps) {
    return this.renderComponent(ComponentNames.Form.FormHelp, props)
  }

  registerFormInput(component: React.ComponentType<FormInputProps>) {
    this.registerComponent(ComponentNames.Form.FormInput, component)
  }

  renderFormInput(props: FormInputProps) {
    return this.renderComponent(ComponentNames.Form.FormInput, props)
  }

  registerFormLabel(component: React.ComponentType<FormLabelProps>) {
    this.registerComponent(ComponentNames.Form.FormLabel, component)
  }

  renderFormLabel(props: FormLabelProps) {
    return this.renderComponent(ComponentNames.Form.FormLabel, props)
  }

  registerFormRow(component: React.ComponentType<FormRowProps>) {
    this.registerComponent(ComponentNames.Form.FormRow, component)
  }

  renderFormRow(props: FormRowProps) {
    return this.renderComponent(ComponentNames.Form.FormRow, props)
  }

  registerFormSection(component: React.ComponentType<FormSectionProps>) {
    this.registerComponent(ComponentNames.Form.FormSection, component)
  }

  renderFormSection(props: FormSectionProps) {
    return this.renderComponent(ComponentNames.Form.FormSection, props)
  }

  registerFormSeparator(component: React.ComponentType<FormSeparatorProps>) {
    this.registerComponent(ComponentNames.Form.FormSeparator, component)
  }

  renderFormSeparator(props: FormSeparatorProps) {
    return this.renderComponent(ComponentNames.Form.FormSeparator, props)
  }

  // base

  private registerComponent(
    name: string,
    component: React.ComponentType<any>
  ): void {
    this.componentsRegistry.registerComponent(name, component)
  }

  private renderComponent<TProps>(
    name: string,
    props: TProps
  ): React.ReactNode | null {
    const Component = this.componentsRegistry.resolveComponent(name)
    if (Component) {
      return React.createElement(Component, props)
    }

    console.error(`Component ${name} not found in registry`)
    return <></>
  }
}
