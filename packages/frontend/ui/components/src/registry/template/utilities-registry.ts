interface UtilitiesMap {
  [name: string]: any
}

export class UtilitiesRegistry {
  private utilities: UtilitiesMap = {}

  registerUtility<T>(name: string, instance: T): void {
    this.utilities[name] = instance
  }

  resolveUtility<T>(name: string): T {
    if (!this.utilities[name]) {
      throw new Error(`Utility ${name} not found`)
    }
    return this.utilities[name]
  }
}
