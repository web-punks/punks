import React, { ReactNode } from "react"
import { IconProps } from "../../ui/icons"

interface IconsMap {
  [name: string]: React.ComponentType<any>
}

export class IconsRegistry {
  private icons: IconsMap = {}

  registerIcon(name: string, component: React.ComponentType<IconProps>): void {
    this.icons[name] = component
  }

  resolveIcon(name: string): React.ComponentType<IconProps> | undefined {
    if (!this.icons[name]) {
      throw new Error(`Icon ${name} not found`)
    }
    return this.icons[name]
  }

  renderIcon(name: string, props: IconProps): ReactNode | null {
    const Icon = this.resolveIcon(name)

    if (Icon) {
      return React.createElement(Icon, props)
    }

    console.error(`Icon ${name} not found in registry`)
    return <></>
  }
}
