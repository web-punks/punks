import React, { ReactNode } from "react"
import { ComponentsRegistry } from "./components-registry"
import { UtilitiesRegistry } from "./utilities-registry"

import { IconsRegistry } from "./icons-registry"
import { ComponentsManager } from "./components-manager"
import { IconProps } from "../../ui/icons"
import { IAlertHandler, INotifiedOperationHandler } from "../../notifications"

const UtilityNames = {
  NotifiedOperationHandler: "NotifiedOperationHandler",
  AlertHandler: "AlertHandler",
}

export class UiRegistry {
  private readonly componentsManager = new ComponentsManager(
    new ComponentsRegistry()
  )
  private readonly utilitiesRegistry = new UtilitiesRegistry()
  private readonly iconsRegistry = new IconsRegistry()

  // icons

  registerIcon(name: string, component: React.ComponentType<IconProps>): void {
    this.iconsRegistry.registerIcon(name, component)
  }

  renderIcon(name: string, props: IconProps): ReactNode | null {
    const Icon = this.iconsRegistry.resolveIcon(name)
    if (Icon) {
      return React.createElement(Icon, props)
    }

    console.error(`Icon ${name} not found in registry`)
    return <></>
  }

  // components

  get components() {
    return this.componentsManager
  }

  // notifications

  registerNotifiedOperationsHandler(instance: INotifiedOperationHandler) {
    this.utilitiesRegistry.registerUtility(
      UtilityNames.NotifiedOperationHandler,
      instance
    )
  }

  get withNotification() {
    return this.utilitiesRegistry.resolveUtility<INotifiedOperationHandler>(
      UtilityNames.NotifiedOperationHandler
    )
  }

  registerAlertHandler(instance: IAlertHandler) {
    this.utilitiesRegistry.registerUtility(UtilityNames.AlertHandler, instance)
  }

  get showAlert() {
    return this.utilitiesRegistry.resolveUtility<IAlertHandler>(
      UtilityNames.AlertHandler
    )
  }
}

const instance = new UiRegistry()

export const uiRegistry = () => instance
