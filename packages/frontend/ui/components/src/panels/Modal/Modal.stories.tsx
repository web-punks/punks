import type { Meta, StoryObj } from "@storybook/react"
import Modal from "./Modal"
import { useState } from "react"
import { Button, Typography } from "../../atoms"
import { range } from "@punks/ui-core"

const meta = {
  title: "Ui/Panels/Modal",
  component: Modal,
  tags: ["autodocs"],
  render: (args) => {
    const [isOpen, setIsOpen] = useState(false)
    const { children, ...props } = args

    const openModal = () => {
      setIsOpen(true)
    }

    const closeModal = () => {
      setIsOpen(false)
    }

    return (
      <>
        <Button variant="filled" color="primary" onClick={openModal}>
          Open Modal
        </Button>
        <Modal {...props} open={isOpen} onClose={closeModal}>
          {children}
        </Modal>
      </>
    )
  },
  args: {
    children: (
      <>
        <Typography variant="h2">Modal Content</Typography>
        <Typography>This is the content of the modal.</Typography>
      </>
    ),
  },
} satisfies Meta<typeof Modal>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {} as any,
}

export const Auto: Story = {
  args: {
    size: "auto",
  } as any,
}

export const AutoWithHead: Story = {
  args: {
    size: "auto",
    head: {
      title: "Title",
    },
    children: "Content",
  } as any,
}

export const FullScreen: Story = {
  args: {
    fullScreen: true,
  } as any,
}

export const Large: Story = {
  args: {
    size: "lg",
  } as any,
}

export const Medium: Story = {
  args: {
    size: "md",
  } as any,
}

export const MediumWithHead: Story = {
  args: {
    size: "md",
    head: {
      title: "Modal Title",
    },
    children: <Typography>This is the content of the modal.</Typography>,
  } as any,
}

export const Small: Story = {
  args: {
    size: "sm",
  } as any,
}

export const Xl: Story = {
  args: {
    size: "xl",
  } as any,
}

export const Xs: Story = {
  args: {
    size: "xs",
  } as any,
}

export const LongContent: Story = {
  args: {
    children: (
      <div>
        {range(1000).map((i) => (
          <Typography key={i}>{`Line ${i + 1}`}</Typography>
        ))}
      </div>
    ),
  } as any,
}

export const LongContentWithHeader: Story = {
  args: {
    head: {
      title: "Modal Title",
    },
    children: (
      <div>
        {range(1000).map((i) => (
          <Typography key={i}>{`Line ${i + 1}`}</Typography>
        ))}
      </div>
    ),
  } as any,
}

export const LargeContent: Story = {
  args: {
    children: (
      <div
        style={{
          display: "flex",
          gap: "1rem",
        }}
      >
        {range(1000).map((i) => (
          <Typography key={i}>{`Line ${i + 1}`}</Typography>
        ))}
      </div>
    ),
  } as any,
}

export const LargeContentWithHeader: Story = {
  args: {
    head: {
      title: "Modal Title",
    },
    children: (
      <div
        style={{
          display: "flex",
          gap: "1rem",
        }}
      >
        {range(1000).map((i) => (
          <Typography key={i}>{`Line ${i + 1}`}</Typography>
        ))}
      </div>
    ),
  } as any,
}
