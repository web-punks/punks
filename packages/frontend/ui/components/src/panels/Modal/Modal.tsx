import React from "react"
import styles from "./Modal.module.css"
import {
  classNames,
  setCssVar,
  useEscKeyListener,
  useOnClickOutside,
} from "@punks/ui-core"
import { Typography } from "../../atoms"
import { CloseIcon } from "../../ui/icons"
import { Portal } from "../../structure"

export type ModalDimension = "sm" | "md" | "lg" | "xl" | "full" | "auto"

export type ModalClasses = {
  root?: string
  overlay?: string
  head?: string
  body?: string
  title?: string
  content?: string
}

export type ModalHead = {
  title?: React.ReactNode
  closeIcon?: React.ReactNode
  closeHidden?: boolean
  noMargin?: boolean
}

export type ModalProps = {
  open: boolean
  onClose?: () => void
  children: React.ReactNode
  fullScreen?: boolean
  noGutters?: boolean
  className?: string
  classes?: ModalClasses
  size?: ModalDimension
  head?: ModalHead
  disablePortal?: boolean
}

const Modal: React.FC<ModalProps> = ({
  open,
  onClose,
  children,
  fullScreen,
  noGutters,
  className,
  classes,
  head,
  size = "md",
}) => {
  React.useEffect(() => {
    if (open) {
      document.querySelector("body")?.classList.add(styles.overflowHidden)
    } else {
      document.querySelector("body")?.classList.remove(styles.overflowHidden)
    }

    return () => {
      document.querySelector("body")?.classList.remove(styles.overflowHidden)
    }
  }, [open])

  useEscKeyListener(() => {
    onClose?.()
  })

  const { onClick, ref } = useOnClickOutside({
    onTriggered: () => onClose?.(),
  })

  return (
    <div
      onClick={(e) => onClick(e)}
      className={classNames(styles.overlay, classes?.overlay, {
        [styles.overlayOpen]: open,
      })}
    >
      <div
        ref={ref}
        style={{
          ...setCssVar("--p", noGutters ? 0 : "var(--wp-spacing-8)"),
        }}
        className={classNames(
          styles.modal,
          styles[`size-${size}`],
          {
            [styles.modalOpen]: open,
            [styles.fullScreen]: fullScreen,
            [styles.gutters]: !noGutters,
          },
          classes?.root,
          className
        )}
      >
        {head && (
          <div className={classNames(styles.head, classes?.head)}>
            <div className={styles.headStart}></div>
            <div className={classNames(styles.headTitle, classes?.title)}>
              {head.title && <Typography variant="h3">{head.title}</Typography>}
            </div>
            <div className={styles.headEnd}>
              {!head.closeHidden && (
                <div className={styles.close} onClick={onClose}>
                  {head.closeIcon ?? <CloseIcon width={16} height={16} />}
                </div>
              )}
            </div>
          </div>
        )}
        <div
          className={classNames(
            styles.content,
            {
              [styles.contentFull]: !head,
              [styles.contentWithHeader]: !!head,
              [styles.contentFramed]: !fullScreen,
            },
            classes?.content
          )}
        >
          {children}
        </div>
      </div>
    </div>
  )
}

const WrappedModal: React.FC<ModalProps> = (props) => {
  return props.disablePortal ? (
    <Modal {...props} />
  ) : (
    <Portal>
      <Modal {...props} />
    </Portal>
  )
}

export default WrappedModal
