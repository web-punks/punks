import type { Meta, StoryObj } from "@storybook/react"
import { Collapsible } from "./Collapsible"
import { Typography } from "../../atoms"
import { useState } from "react"

const meta = {
  title: "Ui/Panels/Collapsible",
  component: Collapsible,
  tags: ["autodocs"],
  args: {
    header: "Panel Title",
    children: (
      <>
        <Typography variant="h2">Panel Content</Typography>
        <Typography>This is the content of the modal.</Typography>
      </>
    ),
  },
} satisfies Meta<typeof Collapsible>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
}

export const DefaultOpen: Story = {
  args: {
    defaultOpen: true,
  },
}

export const CustomDuration: Story = {
  args: {
    animationMs: 1500,
  },
}

export const DefaultPanelWithPreview: Story = {
  args: {
    previewSize: 18,
  },
}

export const Bordered: Story = {
  args: {},
  decorators: [
    (Story) => (
      <div style={{ border: "1px solid red" }}>
        <Story />
      </div>
    ),
  ],
}

export const Controlled: Story = {
  args: {},
  render: (args) => {
    const [open, setOpen] = useState(false)
    return <Collapsible {...args} open={open} onToggle={setOpen} />
  },
}

export const ControlledDefaultOpen: Story = {
  args: {},
  render: (args) => {
    const [open, setOpen] = useState(true)
    return <Collapsible {...args} open={open} onToggle={setOpen} />
  },
}

export const Nested: Story = {
  args: {
    children: (
      <div>
        <Collapsible header="Subpanel">
          <Typography>Subpanel Content</Typography>
        </Collapsible>
      </div>
    ),
  },
  decorators: [
    (Story) => (
      <div style={{ border: "1px solid red" }}>
        <Story />
      </div>
    ),
  ],
}
