import React, { ReactNode, useEffect, useMemo, useState } from "react"
import styles from "./Collapsible.module.css"
import { ChevronDown } from "../../ui/icons"
import {
  classNames,
  normalizePxSize,
  setCssVar,
  useElementTransition,
} from "@punks/ui-core"

export type CollapsibleClasses = {
  root?: string
  header?: string
  icon?: string
  content?: string
}

export interface CollapsibleProps {
  header?: ReactNode
  defaultOpen?: boolean
  children?: any
  disabled?: boolean
  className?: string
  classes?: CollapsibleClasses
  previewSize?: string | number
  icon?: ReactNode
  iconHidden?: boolean
  animationMs?: number
  open?: boolean
  onToggle?: (open: boolean) => void
}

export const Collapsible = ({
  header,
  defaultOpen,
  children,
  disabled,
  className,
  classes,
  previewSize = 0,
  iconHidden,
  icon = <ChevronDown />,
  animationMs = 300,
  open,
  onToggle,
}: CollapsibleProps) => {
  const contentRef = React.useRef<HTMLDivElement>(null)

  const [uncontrolledOpen, setUncontrolledOpen] = useState(defaultOpen ?? false)
  const openState = open ?? uncontrolledOpen

  const initialClosed = useMemo(() => !openState, [])

  // beforeChange state is used for reapplying max-height property when closing
  // completed state is used for removing max-height property when opening completed (to allow inner content to grow)
  const [expandState, setExpandState] = useState<
    "idle" | "beforeChange" | "inProgress" | "completed"
  >("idle")

  const toggleCollapsible = () => {
    setExpandState("inProgress")
    setUncontrolledOpen(!openState)
    onToggle?.(!openState)
  }

  const triggerToggle = () => {
    setExpandState("beforeChange")
  }

  useEffect(() => {
    if (expandState === "beforeChange") {
      toggleCollapsible()
    }
  }, [expandState])

  useElementTransition({
    element: contentRef.current,
    trigger: openState,
    onTransitionEnd: (event) => {
      if (event.propertyName === "max-height") {
        setExpandState("completed")
      }
    },
  })

  const openCompleted = openState && expandState === "completed"
  const expandInProgress =
    expandState === "inProgress" || expandState === "beforeChange"

  return (
    <div
      style={{
        ...setCssVar("--s", animationMs > 0 ? `${animationMs / 1000}s` : 0),
      }}
      className={classNames(styles.root, classes?.root, className)}
    >
      <div
        className={classNames(
          styles.header,
          {
            [styles.headerDisabled]: disabled,
          },
          classes?.header
        )}
        onClick={!disabled ? triggerToggle : undefined}
      >
        {header}
        {!iconHidden && (
          <div
            className={classNames(
              styles.icon,
              {
                [styles.iconExpanded]: openState,
              },
              classes?.icon
            )}
          >
            {icon}
          </div>
        )}
      </div>
      <div
        ref={contentRef}
        style={{
          ...setCssVar(
            "--h",
            normalizePxSize(
              openState ? contentRef.current?.scrollHeight : previewSize
            )
          ),
          ...setCssVar("--ps", normalizePxSize(previewSize)),
        }}
        className={classNames(
          styles.content,
          {
            [styles.controlClosed]: initialClosed && expandState === "idle",
            [styles.controlHeight]: !openCompleted || expandInProgress,
          },
          classes?.content
        )}
      >
        {children}
      </div>
    </div>
  )
}
