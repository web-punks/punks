import React from "react"
import styles from "./DropPanel.module.css"
import { ChevronDown } from "../../ui/icons"
import {
  classNames,
  normalizePxSize,
  setCssVar,
  useDetectClickOutside,
} from "@punks/ui-core"
import { Card } from "../../molecules/Card"

export type DropPanelPosition = "bottomLeft" | "bottomRight"

export type DropPanelClasses = {
  root?: string
  control?: string
  panel?: string
  card?: string
}

export interface DropPanelProps {
  control: React.ReactNode
  children: React.ReactNode
  defaultOpen?: boolean
  startIcon?: React.ReactNode
  dropIcon?: React.ReactNode
  dropIconHidden?: boolean
  position?: DropPanelPosition
  maxPanelHeight?: number | string
  className?: string
  classes?: DropPanelClasses
}

const DropPanel = ({
  children,
  control,
  dropIcon,
  dropIconHidden,
  startIcon,
  defaultOpen,
  position = "bottomRight",
  maxPanelHeight,
  className,
  classes,
}: DropPanelProps) => {
  const [open, setOpen] = React.useState(defaultOpen ?? false)
  const ref = useDetectClickOutside<HTMLDivElement>({
    onTriggered: () => setOpen(false),
  })
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.open]: open,
        },
        classes?.root,
        className
      )}
      ref={ref}
    >
      <div
        className={classNames(styles.control, classes?.control)}
        onClick={() => setOpen(!open)}
      >
        {startIcon && <div className={styles.startIcon}>{startIcon}</div>}
        {control}
        {dropIconHidden !== true && (
          <div className={styles.dropIcon}>
            {dropIcon ? dropIcon : <ChevronDown />}
          </div>
        )}
      </div>
      <div
        className={classNames(
          styles.panel,
          {
            [styles.panelBottomLeft]: position === "bottomLeft",
            [styles.panelBottomRight]: position === "bottomRight",
          },
          classes?.panel
        )}
      >
        <Card className={classes?.card}>
          <div
            style={{
              ...(maxPanelHeight
                ? setCssVar(
                    "--max-height",
                    normalizePxSize(maxPanelHeight) ?? 0
                  )
                : {}),
            }}
            className={classNames({
              [styles.panelMaxH]: !!maxPanelHeight,
            })}
          >
            {children}
          </div>
        </Card>
      </div>
    </div>
  )
}

export default DropPanel
