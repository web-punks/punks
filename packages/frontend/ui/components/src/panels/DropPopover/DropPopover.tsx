import React from "react"
import styles from "./DropPopover.module.css"
import { ChevronDown } from "../../ui/icons"
import { classNames, normalizePxSize, setCssVar } from "@punks/ui-core"
import { Card } from "../../molecules/Card"
import { Anchor } from "../../structure/Anchor"

export type DropPopoverPosition = "bottomLeft" | "bottomRight"

export type DropPopoverClasses = {
  root?: string
  control?: string
  card?: string
}

export interface DropPopoverProps {
  control: React.ReactNode
  children: React.ReactNode
  defaultOpen?: boolean
  startIcon?: React.ReactNode
  dropIcon?: React.ReactNode
  dropIconHidden?: boolean
  position?: DropPopoverPosition
  maxPanelHeight?: number | string
  className?: string
  classes?: DropPopoverClasses
}

const DropPopover = ({
  children,
  control,
  dropIcon,
  dropIconHidden,
  startIcon,
  defaultOpen,
  position = "bottomRight",
  maxPanelHeight,
  className,
  classes,
}: DropPopoverProps) => {
  const [open, setOpen] = React.useState(defaultOpen ?? false)

  return (
    <Anchor
      position={position}
      className={classNames(
        styles.root,
        {
          [styles.open]: open,
        },
        classes?.root,
        className
      )}
      content={
        <Card className={classes?.card}>
          <div
            style={{
              ...(maxPanelHeight
                ? setCssVar(
                    "--max-height",
                    normalizePxSize(maxPanelHeight) ?? 0
                  )
                : {}),
            }}
            className={classNames({
              [styles.panelMaxH]: !!maxPanelHeight,
            })}
          >
            {children}
          </div>
        </Card>
      }
    >
      <div
        className={classNames(styles.control, classes?.control)}
        onClick={() => setOpen(!open)}
      >
        {startIcon && <div className={styles.startIcon}>{startIcon}</div>}
        {control}
        {dropIconHidden !== true && (
          <div className={styles.dropIcon}>
            {dropIcon ? dropIcon : <ChevronDown />}
          </div>
        )}
      </div>
    </Anchor>
  )
}

export default DropPopover
