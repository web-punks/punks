import type { Meta, StoryObj } from "@storybook/react"

import { DropPopover } from "."
import { range } from "@punks/ui-core"
import { Typography } from "../../atoms"

const meta = {
  title: "Ui/Panels/DropPopover",
  component: DropPopover,
  tags: ["autodocs"],
} satisfies Meta<typeof DropPopover>
export default meta

type Story = StoryObj<typeof meta>

const Icon1 = () => (
  <svg width="30" height="30" viewBox="0 0 30 30" fill="none">
    <path
      d="M16.3425 10.5H13.6575C12.7421 10.5 12 11.2421 12 12.1575V26.8425C12 27.7579 12.7421 28.5 13.6575 28.5H16.3425C17.2579 28.5 18 27.7579 18 26.8425V12.1575C18 11.2421 17.2579 10.5 16.3425 10.5Z"
      stroke="currentColor"
      strokeWidth="2.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M26.8425 1.5H24.1575C23.2421 1.5 22.5 2.24209 22.5 3.1575V26.8425C22.5 27.7579 23.2421 28.5 24.1575 28.5H26.8425C27.7579 28.5 28.5 27.7579 28.5 26.8425V3.1575C28.5 2.24209 27.7579 1.5 26.8425 1.5Z"
      stroke="currentColor"
      strokeWidth="2.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M4.5 28.5C6.15685 28.5 7.5 27.1569 7.5 25.5C7.5 23.8431 6.15685 22.5 4.5 22.5C2.84315 22.5 1.5 23.8431 1.5 25.5C1.5 27.1569 2.84315 28.5 4.5 28.5Z"
      stroke="currentColor"
      strokeWidth="2.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const DropPanelDefault: Story = {
  args: {
    startIcon: <Icon1 />,
    children: <div>CONTENT</div>,
    control: "Menu",
  },
}

export const DropPanelMaxHeight: Story = {
  args: {
    startIcon: <Icon1 />,
    children: (
      <div>
        {range(40).map((x) => (
          <Typography my={4}>{x}</Typography>
        ))}
      </div>
    ),
    control: "Menu",
    maxPanelHeight: 200,
  },
}

export const DropPanelWithLongContent: Story = {
  args: {
    startIcon: <Icon1 />,
    children: <div>Thisisaveryveryveryverylongpanelcontent</div>,
    control: "Menu",
  },
  decorators: [
    (Story) => (
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Story />
      </div>
    ),
  ],
}
