import { classNames } from "@punks/ui-core"
import React, { useState } from "react"
import { Popover as PopoverInner } from "react-tiny-popover"
import styles from "./Popover.module.css"
import { Card } from "../../molecules/Card"
import { CardShadow } from "../../molecules/Card/Card"

export type PopoverPosition = "left" | "right" | "top" | "bottom"
export type PopoverAlign = "start" | "center" | "end"

export type PopoverClasses = {
  root?: string
  panel?: string
}

export interface PopoverProps {
  position?: PopoverPosition | PopoverPosition[]
  align?: PopoverAlign
  shadow?: CardShadow
  children: any
  content: any
  noGutters?: boolean
  panelStyles?: React.CSSProperties
  open?: boolean
  onOpen?: () => void
  onClose?: () => void
  className?: string
  classes?: PopoverClasses
}

const Popover: React.FC<PopoverProps> = ({
  position = ["bottom", "top", "left", "right"],
  align = "start",
  shadow,
  children,
  content,
  noGutters,
  panelStyles,
  open,
  onClose,
  onOpen,
  className,
  classes,
}) => {
  const [isPopoverOpen, setIsPopoverOpen] = useState(false)

  const controlled = open !== undefined
  const opened = controlled ? open : isPopoverOpen

  const handleToggle = () => {
    setIsPopoverOpen(!opened)
    if (onOpen && !opened) {
      onOpen()
    }
    if (onClose && opened) {
      onClose()
    }
  }

  const handleClose = () => {
    setIsPopoverOpen(false)
    onClose && onClose()
  }

  return (
    <PopoverInner
      isOpen={opened}
      positions={position}
      align={align}
      padding={8}
      clickOutsideCapture
      onClickOutside={() => handleClose()}
      content={({ position, childRect, popoverRect }) => (
        <Card
          shadow={shadow}
          noGutters={noGutters}
          style={panelStyles}
          className={classes?.panel}
        >
          {content}
        </Card>
      )}
    >
      <div
        className={classNames(styles.control, classes?.root, className)}
        onClick={() => handleToggle()}
      >
        {children}
      </div>
    </PopoverInner>
  )
}

export default Popover
