import type { Meta, StoryObj } from "@storybook/react"
import Popover from "./Popover"

const meta = {
  title: "Ui/Panels/Popover",
  component: Popover,
  tags: ["autodocs"],
  args: {},
} satisfies Meta<typeof Popover>
export default meta

type Story = StoryObj<typeof meta>

export const BottomLeft: Story = {
  args: {
    position: ["bottom", "top", "left", "right"],
    align: "start",
    children: "TOGGLE",
    content: "CONTENT",
  },
}
