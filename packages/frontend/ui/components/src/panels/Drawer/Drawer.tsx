import React from "react"
import { SwipeEventData, useSwipeable } from "react-swipeable"
import styles from "./Drawer.module.css"
import {
  classNames,
  setCssVar,
  useScrollDisabler,
  normalizePxSize,
  useDetectClickOutside,
} from "@punks/ui-core"
import { DrawerContent } from "./DrawerContent"
import { DrawerHead } from "./DrawerHead"
import { Portal } from "../../structure"

export type DrawerPosition = "top" | "bottom" | "left" | "right"

export interface DrawerClasses {
  body?: string
  content?: string
}

export interface DrawerProps {
  open: boolean
  onClose?: () => void
  children: any
  position?: DrawerPosition
  minWidth?: string | number
  maxWidth?: string | number
  minHeight?: string | number
  maxHeight?: string | number
  padding?: string | number
  fullWidth?: boolean
  className?: string
  zIndex?: number
  classes?: DrawerClasses
  swipable?: boolean
  disablePortal?: boolean
}

const Drawer = ({
  children,
  open,
  onClose,
  minWidth,
  maxWidth,
  minHeight,
  maxHeight,
  padding,
  fullWidth,
  className,
  position = "left",
  zIndex,
  classes,
  swipable,
  disablePortal,
}: DrawerProps) => {
  const ref = React.useRef<HTMLDivElement>(null)
  const handleClicks = (event: any) => {
    if (ref.current?.contains(event.target)) {
      onClose?.()
      return
    }
  }

  useScrollDisabler(open)
  useDetectClickOutside<HTMLDivElement>({
    onTriggered: handleClicks,
  })

  const handleSwipe = (eventData: SwipeEventData) => {
    if (eventData.dir === "Down" && position === "bottom") {
      onClose?.()
    }
    if (eventData.dir === "Up" && position === "top") {
      onClose?.()
    }
    if (eventData.dir === "Right" && position === "left") {
      onClose?.()
    }
    if (eventData.dir === "Left" && position === "right") {
      onClose?.()
    }
  }

  const handlers = useSwipeable({
    onSwiped: swipable ? handleSwipe : undefined,
  })

  return (
    <div
      className={classNames(styles.drawer, {})}
      style={{
        visibility: !open ? "hidden" : undefined,
        ...setCssVar("--min-w", normalizePxSize(minWidth) ?? ""),
        ...setCssVar(
          "--max-w",
          fullWidth ? "100vw" : normalizePxSize(maxWidth) ?? ""
        ),
        ...setCssVar("--min-h", normalizePxSize(minHeight) ?? ""),
        ...setCssVar("--max-h", normalizePxSize(maxHeight) ?? ""),
      }}
    >
      <div
        ref={ref}
        style={{
          zIndex,
          visibility: !open ? "hidden" : undefined,
        }}
        className={classNames(styles.drawerShadow, {
          [styles.drawerShadowOpen]: open,
        })}
      />
      <main
        {...handlers}
        style={{
          zIndex,
          visibility: !open ? "hidden" : undefined,
        }}
        className={classNames(
          styles.drawerBody,
          {
            [styles.drawerLeftBody]: position === "left",
            [styles.drawerLeftBodyOpen]: position === "left" && open,
            [styles.drawerRightBody]: position === "right",
            [styles.drawerRightBodyOpen]: position === "right" && open,
            [styles.drawerTopBody]: position === "top",
            [styles.drawerTopBodyOpen]: position === "top" && open,
            [styles.drawerBottomBody]: position === "bottom",
            [styles.drawerBottomBodyOpen]: position === "bottom" && open,
          },
          className,
          classes?.body
        )}
      >
        <section
          style={{
            minWidth,
            minHeight,
            maxHeight,
            padding,
          }}
          className={classNames(styles.drawerContent, classes?.content)}
        >
          <article className={styles.drawerInner}>{children}</article>
        </section>
      </main>
    </div>
  )
}

const WrappedDrawer = (props: DrawerProps) => {
  return props.disablePortal ? (
    <Drawer {...props} />
  ) : (
    <Portal>
      <Drawer {...props} />
    </Portal>
  )
}

Drawer.Content = DrawerContent
Drawer.Head = DrawerHead

WrappedDrawer.Content = DrawerContent
WrappedDrawer.Head = DrawerHead

export default WrappedDrawer
