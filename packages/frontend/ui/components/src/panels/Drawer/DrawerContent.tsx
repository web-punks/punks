import { classNames } from "@punks/ui-core"
import React from "react"
import styles from "./DrawerContent.module.css"

export interface DrawerContentProps {
  children: any
  className?: string
}

export const DrawerContent = ({ children, className }: DrawerContentProps) => {
  return <div className={classNames(styles.root, className)}>{children}</div>
}
