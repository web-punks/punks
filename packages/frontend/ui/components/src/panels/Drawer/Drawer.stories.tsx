import type { Meta, StoryObj } from "@storybook/react"
import { useState } from "react"
import { Button, Typography } from "../../atoms"
import { Drawer } from "."

const meta = {
  title: "Ui/Panels/Drawer",
  component: Drawer,
  tags: ["autodocs"],
  render: (args) => {
    const [isOpen, setIsOpen] = useState(false)

    const openDrawer = () => {
      setIsOpen(true)
    }

    const closeDrawer = () => {
      setIsOpen(false)
    }

    return (
      <>
        <Button variant="filled" color="primary" onClick={openDrawer}>
          Open
        </Button>
        <Drawer {...args} open={isOpen} onClose={closeDrawer}>
          <Drawer.Head onClose={closeDrawer}>Title</Drawer.Head>
          <Drawer.Content>{args.children}</Drawer.Content>
        </Drawer>
      </>
    )
  },
} satisfies Meta<typeof Drawer>
export default meta

type Story = StoryObj<typeof meta>

export const DrawerLeft: Story = {
  args: {
    children: (
      <>
        <Typography variant="h2">Content</Typography>
        <Typography>This is the content of the drawer.</Typography>
      </>
    ),
    position: "left",
  } as any,
}

export const DrawerLeftMaxW: Story = {
  args: {
    children: (
      <>
        <Typography variant="h2">Content</Typography>
        <Typography>This is the content of the drawer.</Typography>
      </>
    ),
    position: "left",
    maxWidth: 300,
  } as any,
}

export const DrawerLeftFullWidth: Story = {
  args: {
    children: (
      <>
        <Typography variant="h2">Content</Typography>
        <Typography>This is the content of the drawer.</Typography>
      </>
    ),
    position: "left",
    fullWidth: true,
  } as any,
}

export const DrawerRight: Story = {
  args: {
    children: (
      <>
        <Typography variant="h2">Content</Typography>
        <Typography>This is the content of the drawer.</Typography>
      </>
    ),
    position: "right",
  } as any,
}

// export const DrawerRightSwipable = Template.bind({})
// DrawerRightSwipable.args = {
//   children: (
//     <>
//       <Typography variant="h2">Content</Typography>
//       <Typography>This is the content of the drawer.</Typography>
//     </>
//   ),
//   position: "right",
//   swipable: true,
// }

export const DrawerRightMaxW: Story = {
  args: {
    children: (
      <>
        <Typography variant="h2">Content</Typography>
        <Typography>This is the content of the drawer.</Typography>
      </>
    ),
    position: "right",
    maxWidth: 300,
  } as any,
}

export const DrawerRightFullWidth: Story = {
  args: {
    children: (
      <>
        <Typography variant="h2">Content</Typography>
        <Typography>This is the content of the drawer.</Typography>
      </>
    ),
    position: "right",
    fullWidth: true,
  } as any,
}
