import React, { ReactNode } from "react"
import { Typography } from "../../atoms"
import { CloseIcon } from "../../ui/icons"
import styles from "./DrawerHead.module.css"
import { TextVariant, classNames } from "@punks/ui-core"

export interface DrawerClasses {
  root?: string
  title?: string
  close?: string
}

export interface DrawerHeadProps {
  children?: ReactNode
  onClose?: () => void
  closeIcon?: ReactNode
  titleVariant?: TextVariant
  classes?: DrawerClasses
}

export const DrawerHead = ({
  titleVariant = "h3",
  closeIcon = <CloseIcon />,
  onClose,
  children,
  classes,
}: DrawerHeadProps) => {
  return (
    <div className={classNames(styles.root, classes?.root)}>
      <Typography variant={titleVariant} className={classes?.title}>
        {children}
      </Typography>
      {onClose && (
        <button
          className={classNames(styles.close, classes?.close)}
          onClick={onClose}
        >
          {closeIcon}
        </button>
      )}
    </div>
  )
}
