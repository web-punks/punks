import React, { useState, useRef } from "react"
import { ChevronDown } from "../../ui/icons"
import styles from "./DropInput.module.css"
import { classNames, normalizePxSize, setCssVar } from "@punks/ui-core"

export type DropInputSize = "sm" | "md" | "lg"
export type DropInputVariant = "filled" | "outlined" | "underlined"

export type DropInputClasses = {
  root?: string
  input?: string
  panel?: string
  startAdornment?: string
  endAdornment?: string
}

export interface DropInputProps {
  className?: string
  size?: DropInputSize
  variant?: DropInputVariant
  rounded?: boolean
  fullWidth?: boolean
  error?: boolean
  disabled?: boolean
  readOnly?: boolean
  classes?: DropInputClasses
  children: any
  panelContent?: any
  panelMaxHeight?: string | number
  startAdornment?: React.ReactNode
  endAdornment?: React.ReactNode
  chevronHidden?: boolean
  rootRef?: React.Ref<HTMLDivElement>
  open?: boolean
  clickable?: boolean
  onClick?: (e: React.MouseEvent<HTMLDivElement>) => void
}

const DropInput: React.FC<DropInputProps> = ({
  className,
  rounded,
  size = "md",
  variant = "outlined",
  fullWidth,
  error,
  disabled,
  readOnly,
  classes,
  children,
  panelContent,
  panelMaxHeight,
  open,
  startAdornment,
  endAdornment,
  chevronHidden,
  rootRef,
  clickable,
  onClick,
}) => {
  return (
    <div
      className={classNames(
        styles.root,
        styles[variant],
        styles[`${variant}-${size}`],
        {
          [styles.fullWidth]: fullWidth,
          [styles[`${variant}-rounded-${size}`]]: rounded,
          [styles.opened]: open,
          [styles.error]: error,
          [styles.disabled]: disabled,
          [styles.clickable]: !disabled && !readOnly && clickable,
        },
        classes?.root,
        className
      )}
      ref={rootRef}
    >
      <div
        className={classNames(
          styles.input,
          {
            [styles.clickable]: !disabled && !readOnly && clickable,
          },
          styles[`input-${size}`],
          classes?.input
        )}
        onClick={onClick}
      >
        {startAdornment && (
          <span
            className={classNames(
              styles.startAdornment,
              classes?.startAdornment
            )}
          >
            {startAdornment}
          </span>
        )}
        {children}
        {endAdornment && (
          <span
            className={classNames(styles.endAdornment, classes?.endAdornment)}
          >
            {endAdornment}
          </span>
        )}
        {!endAdornment && !chevronHidden && !readOnly && clickable && (
          <div className={styles.arrowContainer}>
            <ChevronDown
              className={classNames(
                styles.arrowIcon,
                styles[`arrowIcon-${variant}`],
                {
                  [styles.rotateIcon]: open,
                }
              )}
            />
          </div>
        )}
      </div>
      {open && (
        <div
          style={{
            ...setCssVar("--max-h", normalizePxSize(panelMaxHeight)),
          }}
          className={classNames(
            styles.panel,
            styles[`panel-${variant}`],
            {
              [styles[`panel-${size}-rounded-${size}`]]: rounded,
              [styles.panelMaxH]: !!panelMaxHeight,
            },
            classes?.panel
          )}
        >
          {panelContent}
        </div>
      )}
    </div>
  )
}

export default DropInput
