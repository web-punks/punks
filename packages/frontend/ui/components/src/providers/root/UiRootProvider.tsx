import { ToastRoot } from "../../notifications"

export interface UiRootProviderProps {
  children: any
}

const UiRootProvider = ({ children }: UiRootProviderProps) => {
  return (
    <>
      {children}
      <ToastRoot />
    </>
  )
}

export default UiRootProvider
