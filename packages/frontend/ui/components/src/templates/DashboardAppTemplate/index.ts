export {
  default as DashboardAppTemplate,
  DashboardAppTemplateProps,
  DashboardAppTemplateClasses,
} from "./DashboardAppTemplate"
