import { classNames } from "@punks/ui-core"
import styles from "./DashboardAppTemplate.module.css"

export type DashboardAppTemplateClasses = {
  root?: string
  content?: string
  body?: string
  header?: string
  sidebar?: string
  main?: string
  footer?: string
}

export interface DashboardAppTemplateProps {
  header: React.ReactNode
  sidebar: React.ReactNode
  classes?: DashboardAppTemplateClasses
  children?: React.ReactNode
  footer?: React.ReactNode
}

export const DashboardAppTemplate = ({
  header,
  classes,
  children,
  footer,
  sidebar,
}: DashboardAppTemplateProps) => {
  return (
    <div className={classNames(styles.root, classes?.root)}>
      <div className={classNames(styles.header, classes?.header)}>{header}</div>
      <div className={classNames(styles.content, classes?.content)}>
        <div className={classNames(styles.sidebar, classes?.sidebar)}>
          {sidebar}
        </div>
        <div className={classNames(styles.body, classes?.body)}>
          <main className={classNames(styles.main, classes?.main)}>
            {children}
          </main>
          <footer className={classNames(styles.footer, classes?.footer)}>
            {footer}
          </footer>
        </div>
      </div>
    </div>
  )
}

export default DashboardAppTemplate
