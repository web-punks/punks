import { Meta, StoryObj } from "@storybook/react"
import {
  AddIcon,
  AddOutlinedIcon,
  ApprovedIcon,
  ArrowDownIcon,
  ArrowLeftIcon,
  ArrowRightIcon,
  ArrowUpIcon,
  AudioIcon,
  CalendarIcon,
  ChevronDoubleLeftIcon,
  ChevronDoubleRightIcon,
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  ChevronUpDownIcon,
  ChevronUpIcon,
  CirclesIcon,
  CloseIcon,
  CodeIcon,
  ContextMenuIcon,
  CopyIcon,
  DashboardIcon,
  DeleteIcon,
  DeniedIcon,
  DirectoryIcon,
  DocumentIcon,
  DownloadIcon,
  DragIcon,
  DrawerIcon,
  DuplicateIcon,
  EditIcon,
  EnterIcon,
  ErrorIcon,
  EyeIcon,
  FileIcon,
  FilterIcon,
  FiltersIcon,
  FolderDetailIcon,
  HorizontalLinesIcon,
  InfoIcon,
  LeftArrow,
  MenuIcon,
  PendingIcon,
  PictureIcon,
  RefreshIcon,
  RightArrow,
  SearchIcon,
  SettingsIcon,
  ShareIcon,
  SuccessIcon,
  TextIcon,
  UploadIcon,
  VideoIcon,
  WarningIcon,
  LinkIcon,
} from "./icons"

const meta = {
  title: "Ui/Icons",
  component: FilterIcon,
  args: {
    width: 24,
  },
} satisfies Meta<typeof FilterIcon>
export default meta

export const Add: StoryObj<typeof meta> = {
  render: (args) => <AddIcon {...args} />,
}

export const AddOutlined: StoryObj<typeof meta> = {
  render: (args) => <AddOutlinedIcon {...args} />,
}

export const Approved: StoryObj<typeof meta> = {
  render: (args) => <ApprovedIcon {...args} />,
}

export const ArrowDown: StoryObj<typeof meta> = {
  render: (args) => <ArrowDownIcon {...args} />,
}

export const ArrowLeft: StoryObj<typeof meta> = {
  render: (args) => <ArrowLeftIcon {...args} />,
}

export const ArrowRight: StoryObj<typeof meta> = {
  render: (args) => <ArrowRightIcon {...args} />,
}

export const ArrowUp: StoryObj<typeof meta> = {
  render: (args) => <ArrowUpIcon {...args} />,
}

export const Audio: StoryObj<typeof meta> = {
  render: (args) => <AudioIcon {...args} />,
}

export const Calendar: StoryObj<typeof meta> = {
  render: (args) => <CalendarIcon {...args} />,
}

export const ChevronDown: StoryObj<typeof meta> = {
  render: (args) => <ChevronDownIcon {...args} />,
}

export const ChevronUp: StoryObj<typeof meta> = {
  render: (args) => <ChevronUpIcon {...args} />,
}

export const ChevronRight: StoryObj<typeof meta> = {
  render: (args) => <ChevronRightIcon {...args} />,
}

export const ChevronDoubleRight: StoryObj<typeof meta> = {
  render: (args) => <ChevronDoubleRightIcon {...args} />,
}

export const ChevronLeft: StoryObj<typeof meta> = {
  render: (args) => <ChevronLeftIcon {...args} />,
}

export const ChevronDoubleLeft: StoryObj<typeof meta> = {
  render: (args) => <ChevronDoubleLeftIcon {...args} />,
}

export const ChevronUpDown: StoryObj<typeof meta> = {
  render: (args) => <ChevronUpDownIcon {...args} />,
}

export const Circles: StoryObj<typeof meta> = {
  render: (args) => <CirclesIcon {...args} />,
}

export const Close: StoryObj<typeof meta> = {
  render: (args) => <CloseIcon {...args} />,
}

export const Code: StoryObj<typeof meta> = {
  render: (args) => <CodeIcon {...args} />,
}

export const ContextMenu: StoryObj<typeof meta> = {
  render: (args) => <ContextMenuIcon {...args} />,
}

export const Copy: StoryObj<typeof meta> = {
  render: (args) => <CopyIcon {...args} />,
}

export const Dashboard: StoryObj<typeof meta> = {
  render: (args) => <DashboardIcon {...args} />,
}

export const Delete: StoryObj<typeof meta> = {
  render: (args) => <DeleteIcon {...args} />,
}

export const Denied: StoryObj<typeof meta> = {
  render: (args) => <DeniedIcon {...args} />,
}

export const Directory: StoryObj<typeof meta> = {
  render: (args) => <DirectoryIcon {...args} />,
}

export const Document: StoryObj<typeof meta> = {
  render: (args) => <DocumentIcon {...args} />,
}

export const Download: StoryObj<typeof meta> = {
  render: (args) => <DownloadIcon {...args} />,
}

export const Drag: StoryObj<typeof meta> = {
  render: (args) => <DragIcon {...args} />,
}

export const Drawer: StoryObj<typeof meta> = {
  render: (args) => <DrawerIcon {...args} />,
}

export const Duplicate: StoryObj<typeof meta> = {
  render: (args) => <DuplicateIcon {...args} />,
}

export const Edit: StoryObj<typeof meta> = {
  render: (args) => <EditIcon {...args} />,
}

export const Enter: StoryObj<typeof meta> = {
  render: (args) => <EnterIcon {...args} />,
}

export const Error: StoryObj<typeof meta> = {
  render: (args) => <ErrorIcon {...args} />,
}

export const Eye: StoryObj<typeof meta> = {
  render: (args) => <EyeIcon {...args} />,
}

export const File: StoryObj<typeof meta> = {
  render: (args) => <FileIcon {...args} />,
}

export const Filter: StoryObj<typeof meta> = {
  render: (args) => <FilterIcon {...args} />,
}

export const Filters: StoryObj<typeof meta> = {
  render: (args) => <FiltersIcon {...args} />,
}

export const FolderDetail: StoryObj<typeof meta> = {
  render: (args) => <FolderDetailIcon {...args} />,
}

export const HorizontalLines: StoryObj<typeof meta> = {
  render: (args) => <HorizontalLinesIcon {...args} />,
}

export const Info: StoryObj<typeof meta> = {
  render: (args) => <InfoIcon {...args} />,
}

export const Left: StoryObj<typeof meta> = {
  render: (args) => <LeftArrow {...args} />,
}

export const Menu: StoryObj<typeof meta> = {
  render: (args) => <MenuIcon {...args} />,
}

export const Pending: StoryObj<typeof meta> = {
  render: (args) => <PendingIcon {...args} />,
}

export const Picture: StoryObj<typeof meta> = {
  render: (args) => <PictureIcon {...args} />,
}

export const Refresh: StoryObj<typeof meta> = {
  render: (args) => <RefreshIcon {...args} />,
}

export const Right: StoryObj<typeof meta> = {
  render: (args) => <RightArrow {...args} />,
}

export const Search: StoryObj<typeof meta> = {
  render: (args) => <SearchIcon {...args} />,
}

export const Settings: StoryObj<typeof meta> = {
  render: (args) => <SettingsIcon {...args} />,
}

export const Share: StoryObj<typeof meta> = {
  render: (args) => <ShareIcon {...args} />,
}

export const Success: StoryObj<typeof meta> = {
  render: (args) => <SuccessIcon {...args} />,
}

export const Text: StoryObj<typeof meta> = {
  render: (args) => <TextIcon {...args} />,
}

export const Upload: StoryObj<typeof meta> = {
  render: (args) => <UploadIcon {...args} />,
}

export const Video: StoryObj<typeof meta> = {
  render: (args) => <VideoIcon {...args} />,
}

export const Warning: StoryObj<typeof meta> = {
  render: (args) => <WarningIcon {...args} />,
}

export const Link: StoryObj<typeof meta> = {
  render: (args) => <LinkIcon {...args} />,
}
