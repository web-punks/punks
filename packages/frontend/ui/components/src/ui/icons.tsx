export type IconProps = {
  className?: string
  width?: number | string
  height?: number | string
  onClick?: (e: React.MouseEvent) => void
  children?: React.ReactNode
}

export const SearchIcon2 = (props: IconProps) => (
  <svg
    width="24"
    height="25"
    viewBox="0 0 24 25"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M11 19.2784C15.4183 19.2784 19 15.6967 19 11.2784C19 6.86016 15.4183 3.27844 11 3.27844C6.58172 3.27844 3 6.86016 3 11.2784C3 15.6967 6.58172 19.2784 11 19.2784Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M20.9999 21.2785L16.6499 16.9285"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const CheckIcon = (props: IconProps) => (
  <svg
    width="18"
    height="13"
    viewBox="0 0 18 13"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M17 1L6 12L1 7"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const ChevronDown = (props: IconProps) => (
  <svg
    width="14"
    height="9"
    viewBox="0 0 14 9"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M12 2L7 7"
      stroke="currentColor"
      strokeWidth="1.8"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
    <path
      d="M2 2L7 7"
      stroke="currentColor"
      strokeWidth="1.8"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
  </svg>
)

export const FilterIcon = (props: IconProps) => (
  <svg
    width="16"
    height="16"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M1.65328 1H14.3467C14.4873 1.04932 14.6148 1.13017 14.7194 1.23636C14.8239 1.34254 14.9028 1.47125 14.9499 1.61263C14.997 1.754 15.0112 1.90428 14.9912 2.05196C14.9713 2.19964 14.9178 2.3408 14.8349 2.46463L9.95284 7.83493V14.6699L6.04716 11.7406V7.83493L1.16507 2.46463C1.08216 2.3408 1.0287 2.19964 1.00877 2.05196C0.988843 1.90428 1.00298 1.754 1.05011 1.61263C1.09723 1.47125 1.17609 1.34254 1.28064 1.23636C1.38519 1.13017 1.51265 1.04932 1.65328 1Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const SearchIcon = (props: IconProps) => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M11 19C15.4183 19 19 15.4183 19 11C19 6.58172 15.4183 3 11 3C6.58172 3 3 6.58172 3 11C3 15.4183 6.58172 19 11 19Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M21 20.9999L16.65 16.6499"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const MenuIcon = (props: IconProps) => (
  <svg
    width="16"
    height="13"
    viewBox="0 0 16 13"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path d="M0 1H16" stroke="currentColor" strokeWidth="1.5" />
    <path d="M0 6.3335H16" stroke="currentColor" strokeWidth="1.5" />
    <path d="M0 11.6665H16" stroke="currentColor" strokeWidth="1.5" />
  </svg>
)

export const AddIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="15"
    height="16"
    viewBox="0 0 15 16"
    fill="none"
    {...props}
  >
    <path
      d="M13.9694 8.00063H0.999374M7.48438 1.51562V14.4856V1.51562Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const AddOutlinedIcon = (props: IconProps) => (
  <svg
    width="100"
    height="100"
    viewBox="0 0 100 100"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M53.5 59C59.299 59 64 54.299 64 48.5C64 42.701 59.299 38 53.5 38C47.701 38 43 42.701 43 48.5C43 54.299 47.701 59 53.5 59Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M53.5 44.2998V52.6998"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M49.2998 48.5H57.6998"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const ClockIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="21"
    height="21"
    viewBox="0 0 21 21"
    fill="none"
    {...props}
  >
    <path
      d="M0.85 10.4986C0.85 15.8224 5.17971 20.15 10.5 20.15C15.8203 20.15 20.15 15.8196 20.15 10.4986C20.15 5.17755 15.8203 0.85 10.5 0.85C5.17976 0.85 0.85 5.17482 0.85 10.4986ZM2.10054 10.5013C2.10054 5.8687 5.86807 2.10067 10.5 2.10067C15.1319 2.10067 18.8995 5.8687 18.8995 10.5013C18.8995 15.134 15.1319 18.902 10.5 18.902C5.86807 18.902 2.10054 15.134 2.10054 10.5013Z"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="0.3"
    />
    <path
      d="M9.87612 3.37543C9.87612 3.71771 10.1539 3.99941 10.5 3.99941C10.8423 3.99941 11.124 3.72153 11.124 3.37543V1.47679C11.124 1.13395 10.8429 0.852808 10.5 0.852808C10.1578 0.852808 9.87612 1.13069 9.87612 1.47679V3.37543Z"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="0.3"
    />
    <path
      d="M17.6224 11.1227H19.5234C19.8663 11.1227 20.1474 10.8415 20.1474 10.4987C20.1474 10.1564 19.8695 9.87472 19.5234 9.87472H17.6224C17.2795 9.87472 16.9984 10.1559 16.9984 10.4987C16.9984 10.841 17.2762 11.1227 17.6224 11.1227Z"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="0.3"
    />
    <path
      d="M9.87612 19.526C9.87612 19.8683 10.1539 20.15 10.5 20.15C10.8423 20.15 11.124 19.8721 11.124 19.526V17.6247C11.124 17.2818 10.8429 17.0007 10.5 17.0007C10.1578 17.0007 9.87612 17.2786 9.87612 17.6247V19.526Z"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="0.3"
    />
    <path
      d="M1.47392 11.237H3.375C3.71784 11.237 3.99892 10.9559 3.99892 10.613C3.99892 10.2707 3.72111 9.98904 3.375 9.98904H1.47392C1.13108 9.98904 0.85 10.2702 0.85 10.613C0.85 10.9553 1.12781 11.237 1.47392 11.237Z"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="0.3"
    />
    <path
      d="M8.92544 10.613C8.92544 11.4812 9.63169 12.1876 10.4999 12.1876C11.3681 12.1876 12.0744 11.4812 12.0744 10.613C12.0744 9.74467 11.3681 9.03829 10.4999 9.03829C9.63169 9.03829 8.92544 9.74467 8.92544 10.613ZM10.176 10.613C10.176 10.4352 10.3206 10.289 10.4999 10.289C10.677 10.289 10.8238 10.4358 10.8238 10.613C10.8238 10.7901 10.677 10.9369 10.4999 10.9369C10.3228 10.9369 10.176 10.7901 10.176 10.613Z"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="0.3"
    />
    <path
      d="M10.999 9.25124C10.757 9.49328 10.7543 9.88895 10.999 10.1337C11.241 10.3757 11.6366 10.3784 11.8813 10.1337L14.9031 7.11154C15.1455 6.8691 15.1455 6.47152 14.9031 6.22908C14.6611 5.98704 14.2655 5.98433 14.0207 6.22908L10.999 9.25124Z"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="0.3"
    />
    <path
      d="M6.70048 11.237H9.5494C9.8955 11.237 10.1733 10.9553 10.1733 10.613C10.1733 10.2707 9.8955 9.98904 9.5494 9.98904H6.70048C6.35764 9.98904 6.07656 10.2702 6.07656 10.613C6.07656 10.9553 6.35437 11.237 6.70048 11.237Z"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="0.3"
    />
  </svg>
)

export const CloseIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="12"
    height="12"
    viewBox="0 0 12 12"
    fill="none"
    {...props}
  >
    <path
      d="M11 1L1 11M1 1L11 11L1 1Z"
      stroke="currentColor"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const ChevronDownIcon = (props: IconProps) => {
  return (
    <svg
      width="18"
      height="10"
      viewBox="0 0 18 10"
      fill="currentColor"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.53033 9.28033C9.23744 9.57322 8.76256 9.57322 8.46967 9.28033L0.96967 1.78033C0.676777 1.48744 0.676777 1.01256 0.96967 0.719671C1.26256 0.426777 1.73744 0.426777 2.03033 0.719671L9 7.68934L15.9697 0.71967C16.2626 0.426777 16.7374 0.426777 17.0303 0.71967C17.3232 1.01256 17.3232 1.48744 17.0303 1.78033L9.53033 9.28033Z"
        fill="currentColor"
      />
    </svg>
  )
}

export const ChevronUpIcon = (props: IconProps) => {
  return (
    <svg
      width="18"
      height="10"
      viewBox="0 0 18 10"
      fill="currentColor"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.46967 0.71967C8.76256 0.426777 9.23744 0.426777 9.53033 0.71967L17.0303 8.21967C17.3232 8.51256 17.3232 8.98744 17.0303 9.28033C16.7374 9.57322 16.2626 9.57322 15.9697 9.28033L9 2.31066L2.03033 9.28033C1.73744 9.57322 1.26256 9.57322 0.96967 9.28033C0.676777 8.98744 0.676777 8.51256 0.96967 8.21967L8.46967 0.71967Z"
        fill="currentColor"
      />
    </svg>
  )
}

export const ChevronRightIcon = (props: IconProps) => {
  return (
    <svg
      width="10"
      height="18"
      viewBox="0 0 10 18"
      fill="currentColor"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.28033 8.46967C9.57322 8.76256 9.57322 9.23744 9.28033 9.53033L1.78033 17.0303C1.48744 17.3232 1.01256 17.3232 0.71967 17.0303C0.426777 16.7374 0.426777 16.2626 0.71967 15.9697L7.68934 9L0.719671 2.03033C0.426777 1.73744 0.426777 1.26256 0.719671 0.96967C1.01256 0.676777 1.48744 0.676777 1.78033 0.96967L9.28033 8.46967Z"
        fill="currentColor"
      />
    </svg>
  )
}

export const ChevronDoubleRightIcon = (props: IconProps) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      strokeWidth={1.5}
      stroke="currentColor"
      {...props}
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M11.25 4.5l7.5 7.5-7.5 7.5m-6-15l7.5 7.5-7.5 7.5"
      />
    </svg>
  )
}

export const ChevronLeftIcon = (props: IconProps) => {
  return (
    <svg
      width="10"
      height="18"
      viewBox="0 0 10 18"
      fill="currentColor"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0.71967 9.53033C0.426777 9.23744 0.426777 8.76256 0.71967 8.46967L8.21967 0.96967C8.51256 0.676777 8.98744 0.676777 9.28033 0.96967C9.57322 1.26256 9.57322 1.73744 9.28033 2.03033L2.31066 9L9.28033 15.9697C9.57322 16.2626 9.57322 16.7374 9.28033 17.0303C8.98744 17.3232 8.51256 17.3232 8.21967 17.0303L0.71967 9.53033Z"
        fill="currentColor"
      />
    </svg>
  )
}
export const ChevronDoubleLeftIcon = (props: IconProps) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      strokeWidth={1.5}
      stroke="currentColor"
      {...props}
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M18.75 19.5l-7.5-7.5 7.5-7.5m-6 15L5.25 12l7.5-7.5"
      />
    </svg>
  )
}

export const ChevronUpDownIcon = (props: IconProps) => {
  return (
    <svg
      width="10"
      height="16"
      viewBox="0 0 10 16"
      fill="currentColor"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.46967 0.71967C4.76256 0.426777 5.23744 0.426777 5.53033 0.71967L9.28033 4.46967C9.57322 4.76256 9.57322 5.23744 9.28033 5.53033C8.98744 5.82322 8.51256 5.82322 8.21967 5.53033L5 2.31066L1.78033 5.53033C1.48744 5.82322 1.01256 5.82322 0.71967 5.53033C0.426777 5.23744 0.426777 4.76256 0.71967 4.46967L4.46967 0.71967ZM0.71967 10.4697C1.01256 10.1768 1.48744 10.1768 1.78033 10.4697L5 13.6893L8.21967 10.4697C8.51256 10.1768 8.98744 10.1768 9.28033 10.4697C9.57322 10.7626 9.57322 11.2374 9.28033 11.5303L5.53033 15.2803C5.23744 15.5732 4.76256 15.5732 4.46967 15.2803L0.71967 11.5303C0.426777 11.2374 0.426777 10.7626 0.71967 10.4697Z"
        fill="currentColor"
      />
    </svg>
  )
}

export const FileIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    {...props}
  >
    <path
      d="M15.0549 1.66676H8.18987C7.86144 1.66582 7.5361 1.73009 7.23269 1.85583C6.92928 1.98157 6.65386 2.16628 6.42237 2.39926L4.0657 4.75592C3.83262 4.98751 3.64785 5.26307 3.52211 5.56663C3.39637 5.87019 3.33216 6.19569 3.3332 6.52426V16.6668C3.32651 17.1013 3.49241 17.5208 3.79453 17.8332C4.09665 18.1457 4.51033 18.3255 4.94487 18.3334H15.0549C15.4894 18.3255 15.9031 18.1457 16.2052 17.8332C16.5073 17.5208 16.6732 17.1013 16.6665 16.6668V3.33342C16.6732 2.89886 16.5073 2.47938 16.2052 2.16696C15.9031 1.85453 15.4894 1.67465 15.0549 1.66676ZM7.49987 3.67842V5.83342H5.34487L7.49987 3.67842ZM4.99987 16.6668V7.50009H7.49987C7.9419 7.50009 8.36582 7.3245 8.67838 7.01194C8.99094 6.69937 9.16654 6.27545 9.16654 5.83342V3.33342L14.9949 3.31509C14.9983 3.32059 15 3.32696 14.9999 3.33342L15.0549 16.6668H4.99987Z"
      fill="currentColor"
    />
    <path
      d="M12.5549 9.16676H7.35654C7.13552 9.16676 6.92356 9.25456 6.76728 9.41084C6.611 9.56712 6.5232 9.77908 6.5232 10.0001C6.5232 10.2211 6.611 10.4331 6.76728 10.5893C6.92356 10.7456 7.13552 10.8334 7.35654 10.8334H12.5549C12.7759 10.8334 12.9878 10.7456 13.1441 10.5893C13.3004 10.4331 13.3882 10.2211 13.3882 10.0001C13.3882 9.77908 13.3004 9.56712 13.1441 9.41084C12.9878 9.25456 12.7759 9.16676 12.5549 9.16676Z"
      fill="currentColor"
    />
    <path
      d="M12.5549 12.5001H7.35654C7.13552 12.5001 6.92356 12.5879 6.76728 12.7442C6.611 12.9004 6.5232 13.1124 6.5232 13.3334C6.5232 13.5544 6.611 13.7664 6.76728 13.9227C6.92356 14.079 7.13552 14.1668 7.35654 14.1668H12.5549C12.7759 14.1668 12.9878 14.079 13.1441 13.9227C13.3004 13.7664 13.3882 13.5544 13.3882 13.3334C13.3882 13.1124 13.3004 12.9004 13.1441 12.7442C12.9878 12.5879 12.7759 12.5001 12.5549 12.5001Z"
      fill="currentColor"
    />
  </svg>
)

export const File2Icon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="18"
    height="22"
    viewBox="0 0 18 22"
    fill="none"
    {...props}
  >
    <path
      d="M11 1H3C2.46957 1 1.96086 1.21071 1.58579 1.58579C1.21071 1.96086 1 2.46957 1 3V19C1 19.5304 1.21071 20.0391 1.58579 20.4142C1.96086 20.7893 2.46957 21 3 21H15C15.5304 21 16.0391 20.7893 16.4142 20.4142C16.7893 20.0391 17 19.5304 17 19V7L11 1Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M11 1V7H17"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const FiltersIcon = (props: IconProps) => (
  <svg width="16" height="16" viewBox="0 0 16 16" fill="none" {...props}>
    <path
      d="M4.66683 4.6665H1.3335V7.99984H4.66683V4.6665Z"
      stroke="currentColor"
      strokeWidth="1.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M3 1.3335V4.66683"
      stroke="currentColor"
      strokeWidth="1.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M3 8V14.6667"
      stroke="currentColor"
      strokeWidth="1.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M9.66683 9.6665H6.3335V12.9998H9.66683V9.6665Z"
      stroke="currentColor"
      strokeWidth="1.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M8 1.3335V9.66683"
      stroke="currentColor"
      strokeWidth="1.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M8 13V14.6667"
      stroke="currentColor"
      strokeWidth="1.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M14.6668 2.1665H11.3335V5.49984H14.6668V2.1665Z"
      stroke="currentColor"
      strokeWidth="1.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13 1.3335V2.16683"
      stroke="currentColor"
      strokeWidth="1.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13 5.5V14.6667"
      stroke="currentColor"
      strokeWidth="1.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const ContextMenuIcon = (props: IconProps) => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M10 3.97461L10 3.98294"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M10 9.9834L10 9.99173"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M10 15.9912L10 15.9995"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const DeleteIcon = (props: IconProps) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="22"
      viewBox="0 0 20 22"
      fill="none"
      {...props}
    >
      <path
        d="M1 5H3H19"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17 5V19C17 19.5304 16.7893 20.0391 16.4142 20.4142C16.0391 20.7893 15.5304 21 15 21H5C4.46957 21 3.96086 20.7893 3.58579 20.4142C3.21071 20.0391 3 19.5304 3 19V5M6 5V3C6 2.46957 6.21071 1.96086 6.58579 1.58579C6.96086 1.21071 7.46957 1 8 1H12C12.5304 1 13.0391 1.21071 13.4142 1.58579C13.7893 1.96086 14 2.46957 14 3V5"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8 10V16"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12 10V16"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export const RefreshIcon = (props: IconProps) => {
  return (
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" {...props}>
      <path
        d="M14.023 7.34841H19.0156V7.34663M0.984375 17.6444V12.6517M0.984375 12.6517L5.97702 12.6517M0.984375 12.6517L4.16527 15.8347C5.15579 16.8271 6.41285 17.58 7.8646 17.969C12.2657 19.1483 16.7895 16.5364 17.9687 12.1353M2.03097 7.86484C3.21024 3.46374 7.73402 0.851937 12.1351 2.03121C13.5869 2.4202 14.8439 3.17312 15.8345 4.1655L19.0156 7.34663M19.0156 2.3558V7.34663"
        stroke="currentColor"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export const CodeIcon = (props: IconProps) => {
  return (
    <svg width="20" height="16" viewBox="0 0 20 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6.28033 3.21967C6.57322 3.51256 6.57322 3.98744 6.28033 4.28033L2.56066 8L6.28033 11.7197C6.57322 12.0126 6.57322 12.4874 6.28033 12.7803C5.98744 13.0732 5.51256 13.0732 5.21967 12.7803L0.96967 8.53033C0.676777 8.23744 0.676777 7.76256 0.96967 7.46967L5.21967 3.21967C5.51256 2.92678 5.98744 2.92678 6.28033 3.21967ZM13.7197 3.21967C14.0126 2.92678 14.4874 2.92678 14.7803 3.21967L19.0303 7.46967C19.3232 7.76256 19.3232 8.23744 19.0303 8.53033L14.7803 12.7803C14.4874 13.0732 14.0126 13.0732 13.7197 12.7803C13.4268 12.4874 13.4268 12.0126 13.7197 11.7197L17.4393 8L13.7197 4.28033C13.4268 3.98744 13.4268 3.51256 13.7197 3.21967Z"
        fill="currentColor"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M11.3774 0.0109049C11.7856 0.0812827 12.0595 0.469239 11.9891 0.87743L9.48909 15.3774C9.41872 15.7856 9.03076 16.0595 8.62257 15.9891C8.21438 15.9187 7.94053 15.5308 8.0109 15.1226L10.5109 0.62257C10.5813 0.214379 10.9692 -0.0594729 11.3774 0.0109049Z"
        fill="currentColor"
      />
    </svg>
  )
}

export const FolderDetailIcon = (props: IconProps) => {
  return (
    <svg width="22" height="18" viewBox="0 0 22 18" fill="none" {...props}>
      <path
        d="M12.0606 3.31066L12.5909 2.78033L12.0606 3.31066ZM9.93924 1.18934L9.40891 1.71967V1.71967L9.93924 1.18934ZM0.866786 9.3182L0.124324 9.42426L0.866786 9.3182ZM1.72393 15.3182L0.981467 15.4243L1.72393 15.3182ZM20.2759 15.3182L21.0183 15.4243V15.4243L20.2759 15.3182ZM21.133 9.3182L20.3906 9.21213V9.21213L21.133 9.3182ZM4.9999 1.5H8.87858V0H4.9999V1.5ZM9.40891 1.71967L11.5302 3.84099L12.5909 2.78033L10.4696 0.65901L9.40891 1.71967ZM13.1212 4.5H16.9999V3H13.1212V4.5ZM11.5302 3.84099C11.9522 4.26295 12.5245 4.5 13.1212 4.5V3C12.9223 3 12.7315 2.92098 12.5909 2.78033L11.5302 3.84099ZM8.87858 1.5C9.07749 1.5 9.26826 1.57902 9.40891 1.71967L10.4696 0.65901C10.0476 0.237053 9.47532 0 8.87858 0V1.5ZM19.9999 6C19.9999 4.34315 18.6568 3 16.9999 3V4.5C17.8283 4.5 18.4999 5.17157 18.4999 6H19.9999ZM3.4999 3C3.4999 2.17157 4.17147 1.5 4.9999 1.5V0C3.34305 0 1.9999 1.34315 1.9999 3H3.4999ZM0.124324 9.42426L0.981467 15.4243L2.46639 15.2121L1.60925 9.21213L0.124324 9.42426ZM0.981467 15.4243C1.1926 16.9022 2.45837 18 3.95132 18V16.5C3.20484 16.5 2.57196 15.9511 2.46639 15.2121L0.981467 15.4243ZM3.95132 18H18.0485V16.5H3.95132V18ZM18.0485 18C19.5414 18 20.8072 16.9022 21.0183 15.4243L19.5334 15.2121C19.4278 15.9511 18.795 16.5 18.0485 16.5V18ZM21.0183 15.4243L21.8755 9.42426L20.3906 9.21213L19.5334 15.2121L21.0183 15.4243ZM18.9056 6H3.09417V7.5H18.9056V6ZM3.09417 6C2.9389 6 2.78607 6.01178 2.6366 6.03463L2.8632 7.51741C2.93781 7.50601 3.01489 7.5 3.09417 7.5V6ZM2.6366 6.03463C1.04091 6.27849 -0.111894 7.77074 0.124324 9.42426L1.60925 9.21213C1.49136 8.38691 2.06688 7.63911 2.8632 7.51741L2.6366 6.03463ZM3.4999 6.77602V3H1.9999V6.77602H3.4999ZM21.8755 9.42426C22.1117 7.77074 20.9589 6.27849 19.3632 6.03463L19.1366 7.51741C19.9329 7.63911 20.5084 8.38691 20.3906 9.21213L21.8755 9.42426ZM19.3632 6.03463C19.2137 6.01178 19.0609 6 18.9056 6V7.5C18.9849 7.5 19.062 7.50601 19.1366 7.51741L19.3632 6.03463ZM18.4999 6V6.77602H19.9999V6H18.4999Z"
        fill="currentColor"
      />
    </svg>
  )
}

export const InfoIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
    {...props}
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
    ></path>
  </svg>
)

export const SuccessIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
    {...props}
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
    />
  </svg>
)

export const WarningIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
    {...props}
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"
    />
  </svg>
)

export const ErrorIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
    {...props}
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      stroke="currentColor"
      d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"
    />
  </svg>
)

export const EditIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="23"
    height="23"
    viewBox="0 0 23 23"
    fill="none"
    {...props}
  >
    <path
      d="M10 3.12109H3C2.46957 3.12109 1.96086 3.33181 1.58579 3.70688C1.21071 4.08195 1 4.59066 1 5.12109V19.1211C1 19.6515 1.21071 20.1602 1.58579 20.5353C1.96086 20.9104 2.46957 21.1211 3 21.1211H17C17.5304 21.1211 18.0391 20.9104 18.4142 20.5353C18.7893 20.1602 19 19.6515 19 19.1211V12.1211"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M17.5 1.62132C17.8978 1.2235 18.4374 1 19 1C19.5626 1 20.1022 1.2235 20.5 1.62132C20.8978 2.01915 21.1213 2.55871 21.1213 3.12132C21.1213 3.68393 20.8978 4.2235 20.5 4.62132L11 14.1213L7 15.1213L8 11.1213L17.5 1.62132Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const DownloadIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="25"
    height="21"
    viewBox="0 0 25 21"
    fill="none"
    {...props}
  >
    <path
      d="M8.00879 15.0117L12.0088 19.0117L16.0088 15.0117"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M12.0088 10.0117V19.0117"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M20.889 16.1017C21.7584 15.4903 22.4103 14.6178 22.7503 13.6108C23.0902 12.6038 23.1004 11.5147 22.7794 10.5015C22.4584 9.48828 21.8228 8.60374 20.965 7.97621C20.1072 7.34868 19.0718 7.01082 18.009 7.01165H16.749C16.4482 5.83952 15.8855 4.7509 15.1031 3.82773C14.3207 2.90456 13.3391 2.17091 12.2322 1.68201C11.1252 1.1931 9.92181 0.961679 8.71249 1.00517C7.50317 1.04866 6.31948 1.36592 5.25052 1.93308C4.18156 2.50023 3.25519 3.3025 2.54115 4.27948C1.82712 5.25646 1.34402 6.3827 1.12823 7.5734C0.912436 8.7641 0.969576 9.98825 1.29535 11.1537C1.62112 12.3191 2.20703 13.3954 3.00897 14.3017"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const UploadIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="18"
    height="22"
    viewBox="0 0 18 22"
    fill="none"
    {...props}
  >
    <path
      d="M1 11V19C1 19.5304 1.21071 20.0391 1.58579 20.4142C1.96086 20.7893 2.46957 21 3 21H15C15.5304 21 16.0391 20.7893 16.4142 20.4142C16.7893 20.0391 17 19.5304 17 19V11"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13 5L9 1L5 5"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M9 1V14"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const DrawerIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="18"
    height="20"
    viewBox="0 0 18 20"
    fill="none"
    {...props}
  >
    <path
      d="M0.999999 2.63636L1 17.3636C1 18.2674 1.89543 19 3 19L15 19C16.1046 19 17 18.2674 17 17.3636L17 2.63636C17 1.73263 16.1046 0.999999 15 0.999999L3 1C1.89543 1 0.999999 1.73263 0.999999 2.63636Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M10 19L10 1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const ShareIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="22"
    viewBox="0 0 20 22"
    fill="none"
    {...props}
  >
    <path
      d="M16 7C17.6569 7 19 5.65685 19 4C19 2.34315 17.6569 1 16 1C14.3431 1 13 2.34315 13 4C13 5.65685 14.3431 7 16 7Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M4 14C5.65685 14 7 12.6569 7 11C7 9.34315 5.65685 8 4 8C2.34315 8 1 9.34315 1 11C1 12.6569 2.34315 14 4 14Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M16 21C17.6569 21 19 19.6569 19 18C19 16.3431 17.6569 15 16 15C14.3431 15 13 16.3431 13 18C13 19.6569 14.3431 21 16 21Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M6.58984 12.5098L13.4198 16.4898"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.4098 5.50977L6.58984 9.48977"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const EyeIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="18"
    viewBox="0 0 24 18"
    fill="none"
    {...props}
  >
    <path
      d="M1 9C1 9 5 1 12 1C19 1 23 9 23 9C23 9 19 17 12 17C5 17 1 9 1 9Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M12 12C13.6569 12 15 10.6569 15 9C15 7.34315 13.6569 6 12 6C10.3431 6 9 7.34315 9 9C9 10.6569 10.3431 12 12 12Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const EnterIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="18"
    height="18"
    viewBox="0 0 18 18"
    fill="none"
    {...props}
  >
    <path
      d="M6 7L1 12L6 17"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M17 1V8C17 9.06087 16.5786 10.0783 15.8284 10.8284C15.0783 11.5786 14.0609 12 13 12H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const CopyIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="22"
    height="22"
    viewBox="0 0 22 22"
    fill="none"
    {...props}
  >
    <path
      d="M19 8H10C8.89543 8 8 8.89543 8 10V19C8 20.1046 8.89543 21 10 21H19C20.1046 21 21 20.1046 21 19V10C21 8.89543 20.1046 8 19 8Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M4 14H3C2.46957 14 1.96086 13.7893 1.58579 13.4142C1.21071 13.0391 1 12.5304 1 12V3C1 2.46957 1.21071 1.96086 1.58579 1.58579C1.96086 1.21071 2.46957 1 3 1H12C12.5304 1 13.0391 1.21071 13.4142 1.58579C13.7893 1.96086 14 2.46957 14 3V4"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const DuplicateIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="23"
    height="23"
    viewBox="0 0 23 23"
    fill="none"
    {...props}
  >
    <path
      d="M19.9 1H3.1C1.9402 1 1 1.9402 1 3.1V7.3C1 8.4598 1.9402 9.4 3.1 9.4H19.9C21.0598 9.4 22 8.4598 22 7.3V3.1C22 1.9402 21.0598 1 19.9 1Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M19.9 13.6001H3.1C1.9402 13.6001 1 14.5403 1 15.7001V19.9001C1 21.0599 1.9402 22.0001 3.1 22.0001H19.9C21.0598 22.0001 22 21.0599 22 19.9001V15.7001C22 14.5403 21.0598 13.6001 19.9 13.6001Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const DocumentIcon = (props: IconProps) => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M15.055 1.6667H8.18999C7.86156 1.66576 7.53622 1.73003 7.23281 1.85577C6.92941 1.98151 6.65398 2.16622 6.42249 2.3992L4.06582 4.75586C3.83275 4.98745 3.64798 5.26301 3.52223 5.56657C3.39649 5.87013 3.33228 6.19563 3.33332 6.5242V16.6667C3.32663 17.1013 3.49254 17.5207 3.79465 17.8332C4.09677 18.1456 4.51045 18.3255 4.94499 18.3334H15.055C15.4895 18.3255 15.9032 18.1456 16.2053 17.8332C16.5074 17.5207 16.6733 17.1013 16.6667 16.6667V3.33336C16.6733 2.8988 16.5074 2.47932 16.2053 2.1669C15.9032 1.85447 15.4895 1.67459 15.055 1.6667ZM7.49999 3.67836V5.83336H5.34499L7.49999 3.67836ZM4.99999 16.6667V7.50003H7.49999C7.94202 7.50003 8.36594 7.32444 8.6785 7.01187C8.99106 6.69931 9.16666 6.27539 9.16666 5.83336V3.33336L14.995 3.31503C14.9984 3.32053 15.0001 3.3269 15 3.33336L15.055 16.6667H4.99999Z"
      fill="currentColor"
    />
    <path
      d="M12.555 9.1667H7.35666C7.13564 9.1667 6.92368 9.25449 6.7674 9.41078C6.61112 9.56706 6.52332 9.77902 6.52332 10C6.52332 10.221 6.61112 10.433 6.7674 10.5893C6.92368 10.7456 7.13564 10.8334 7.35666 10.8334H12.555C12.776 10.8334 12.988 10.7456 13.1442 10.5893C13.3005 10.433 13.3883 10.221 13.3883 10C13.3883 9.77902 13.3005 9.56706 13.1442 9.41078C12.988 9.25449 12.776 9.1667 12.555 9.1667Z"
      fill="currentColor"
    />
    <path
      d="M12.555 12.5H7.35666C7.13564 12.5 6.92368 12.5878 6.7674 12.7441C6.61112 12.9004 6.52332 13.1124 6.52332 13.3334C6.52332 13.5544 6.61112 13.7663 6.7674 13.9226C6.92368 14.0789 7.13564 14.1667 7.35666 14.1667H12.555C12.776 14.1667 12.988 14.0789 13.1442 13.9226C13.3005 13.7663 13.3883 13.5544 13.3883 13.3334C13.3883 13.1124 13.3005 12.9004 13.1442 12.7441C12.988 12.5878 12.776 12.5 12.555 12.5Z"
      fill="currentColor"
    />
  </svg>
)

export const ApprovedIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="27"
    height="27"
    viewBox="0 0 27 27"
    fill="none"
    {...props}
  >
    <circle cx="13.5" cy="13.5" r="13.5" fill="currentColor" />
    <path
      d="M8 13.2353L11.84 17L20 9"
      stroke="white"
      strokeWidth="2"
      strokeLinecap="round"
    />
  </svg>
)

export const DeniedIcon = (props: IconProps) => (
  <svg
    width="27"
    height="27"
    viewBox="0 0 27 27"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <circle cx="13.5" cy="13.5" r="13.5" fill="currentColor" />
    <path d="M9 9L18 18" stroke="white" strokeWidth="2" strokeLinecap="round" />
    <path d="M18 9L9 18" stroke="white" strokeWidth="2" strokeLinecap="round" />
  </svg>
)

export const PendingIcon = (props: IconProps) => (
  <svg
    width="27"
    height="27"
    viewBox="0 0 27 27"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <circle cx="13.5" cy="13.5" r="13.5" fill="currentColor" />
    <path d="M8 13H19" stroke="white" strokeWidth="2" strokeLinecap="round" />
  </svg>
)

export const ArrowUpIcon = (props: IconProps) => (
  <svg
    stroke="currentColor"
    fill="currentColor"
    strokeWidth="0"
    viewBox="0 0 512 512"
    height="1em"
    width="1em"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fill="none"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="48"
      d="M112 244l144-144 144 144M256 120v292"
    ></path>
  </svg>
)

export const ArrowDownIcon = (props: IconProps) => (
  <svg
    stroke="currentColor"
    fill="currentColor"
    strokeWidth="0"
    viewBox="0 0 512 512"
    height="1em"
    width="1em"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fill="none"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="48"
      d="M112 268l144 144 144-144M256 392V100"
    ></path>
  </svg>
)

export const ArrowLeftIcon = (props: IconProps) => (
  <svg
    stroke="currentColor"
    fill="currentColor"
    strokeWidth="0"
    viewBox="0 0 512 512"
    height="1em"
    width="1em"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fill="none"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="48"
      d="M244 400L100 256l144-144M120 256h292"
    ></path>
  </svg>
)

export const ArrowRightIcon = (props: IconProps) => (
  <svg
    stroke="currentColor"
    fill="currentColor"
    strokeWidth="0"
    viewBox="0 0 512 512"
    height="1em"
    width="1em"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fill="none"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="48"
      d="M268 112l144 144-144 144m124-144H100"
    ></path>
  </svg>
)

export const LeftArrow = (props: IconProps) => (
  <svg
    width="20"
    height="21"
    viewBox="0 0 20 21"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M12 15.5L7 10.5"
      stroke="currentColor"
      strokeWidth="1.8"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
    <path
      d="M12 5.5L7 10.5"
      stroke="currentColor"
      strokeWidth="1.8"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
  </svg>
)

export const RightArrow = (props: IconProps) => (
  <svg
    width="20"
    height="21"
    viewBox="0 0 20 21"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M8 5.5L13 10.5"
      stroke="currentColor"
      strokeWidth="1.8"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
    <path
      d="M8 15.5L13 10.5"
      stroke="currentColor"
      strokeWidth="1.8"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
  </svg>
)

export const SettingsIcon = (props: IconProps) => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g clipPath="url(#clip0_795_2863)">
      <path
        d="M12 15C13.6569 15 15 13.6569 15 12C15 10.3431 13.6569 9 12 9C10.3431 9 9 10.3431 9 12C9 13.6569 10.3431 15 12 15Z"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M19.4 15C19.2669 15.3016 19.2272 15.6362 19.286 15.9606C19.3448 16.285 19.4995 16.5843 19.73 16.82L19.79 16.88C19.976 17.0657 20.1235 17.2863 20.2241 17.5291C20.3248 17.7719 20.3766 18.0322 20.3766 18.295C20.3766 18.5578 20.3248 18.8181 20.2241 19.0609C20.1235 19.3037 19.976 19.5243 19.79 19.71C19.6043 19.896 19.3837 20.0435 19.1409 20.1441C18.8981 20.2448 18.6378 20.2966 18.375 20.2966C18.1122 20.2966 17.8519 20.2448 17.6091 20.1441C17.3663 20.0435 17.1457 19.896 16.96 19.71L16.9 19.65C16.6643 19.4195 16.365 19.2648 16.0406 19.206C15.7162 19.1472 15.3816 19.1869 15.08 19.32C14.7842 19.4468 14.532 19.6572 14.3543 19.9255C14.1766 20.1938 14.0813 20.5082 14.08 20.83V21C14.08 21.5304 13.8693 22.0391 13.4942 22.4142C13.1191 22.7893 12.6104 23 12.08 23C11.5496 23 11.0409 22.7893 10.6658 22.4142C10.2907 22.0391 10.08 21.5304 10.08 21V20.91C10.0723 20.579 9.96512 20.258 9.77251 19.9887C9.5799 19.7194 9.31074 19.5143 9 19.4C8.69838 19.2669 8.36381 19.2272 8.03941 19.286C7.71502 19.3448 7.41568 19.4995 7.18 19.73L7.12 19.79C6.93425 19.976 6.71368 20.1235 6.47088 20.2241C6.22808 20.3248 5.96783 20.3766 5.705 20.3766C5.44217 20.3766 5.18192 20.3248 4.93912 20.2241C4.69632 20.1235 4.47575 19.976 4.29 19.79C4.10405 19.6043 3.95653 19.3837 3.85588 19.1409C3.75523 18.8981 3.70343 18.6378 3.70343 18.375C3.70343 18.1122 3.75523 17.8519 3.85588 17.6091C3.95653 17.3663 4.10405 17.1457 4.29 16.96L4.35 16.9C4.58054 16.6643 4.73519 16.365 4.794 16.0406C4.85282 15.7162 4.81312 15.3816 4.68 15.08C4.55324 14.7842 4.34276 14.532 4.07447 14.3543C3.80618 14.1766 3.49179 14.0813 3.17 14.08H3C2.46957 14.08 1.96086 13.8693 1.58579 13.4942C1.21071 13.1191 1 12.6104 1 12.08C1 11.5496 1.21071 11.0409 1.58579 10.6658C1.96086 10.2907 2.46957 10.08 3 10.08H3.09C3.42099 10.0723 3.742 9.96512 4.0113 9.77251C4.28059 9.5799 4.48572 9.31074 4.6 9C4.73312 8.69838 4.77282 8.36381 4.714 8.03941C4.65519 7.71502 4.50054 7.41568 4.27 7.18L4.21 7.12C4.02405 6.93425 3.87653 6.71368 3.77588 6.47088C3.67523 6.22808 3.62343 5.96783 3.62343 5.705C3.62343 5.44217 3.67523 5.18192 3.77588 4.93912C3.87653 4.69632 4.02405 4.47575 4.21 4.29C4.39575 4.10405 4.61632 3.95653 4.85912 3.85588C5.10192 3.75523 5.36217 3.70343 5.625 3.70343C5.88783 3.70343 6.14808 3.75523 6.39088 3.85588C6.63368 3.95653 6.85425 4.10405 7.04 4.29L7.1 4.35C7.33568 4.58054 7.63502 4.73519 7.95941 4.794C8.28381 4.85282 8.61838 4.81312 8.92 4.68H9C9.29577 4.55324 9.54802 4.34276 9.72569 4.07447C9.90337 3.80618 9.99872 3.49179 10 3.17V3C10 2.46957 10.2107 1.96086 10.5858 1.58579C10.9609 1.21071 11.4696 1 12 1C12.5304 1 13.0391 1.21071 13.4142 1.58579C13.7893 1.96086 14 2.46957 14 3V3.09C14.0013 3.41179 14.0966 3.72618 14.2743 3.99447C14.452 4.26276 14.7042 4.47324 15 4.6C15.3016 4.73312 15.6362 4.77282 15.9606 4.714C16.285 4.65519 16.5843 4.50054 16.82 4.27L16.88 4.21C17.0657 4.02405 17.2863 3.87653 17.5291 3.77588C17.7719 3.67523 18.0322 3.62343 18.295 3.62343C18.5578 3.62343 18.8181 3.67523 19.0609 3.77588C19.3037 3.87653 19.5243 4.02405 19.71 4.21C19.896 4.39575 20.0435 4.61632 20.1441 4.85912C20.2448 5.10192 20.2966 5.36217 20.2966 5.625C20.2966 5.88783 20.2448 6.14808 20.1441 6.39088C20.0435 6.63368 19.896 6.85425 19.71 7.04L19.65 7.1C19.4195 7.33568 19.2648 7.63502 19.206 7.95941C19.1472 8.28381 19.1869 8.61838 19.32 8.92V9C19.4468 9.29577 19.6572 9.54802 19.9255 9.72569C20.1938 9.90337 20.5082 9.99872 20.83 10H21C21.5304 10 22.0391 10.2107 22.4142 10.5858C22.7893 10.9609 23 11.4696 23 12C23 12.5304 22.7893 13.0391 22.4142 13.4142C22.0391 13.7893 21.5304 14 21 14H20.91C20.5882 14.0013 20.2738 14.0966 20.0055 14.2743C19.7372 14.452 19.5268 14.7042 19.4 15Z"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </g>
    <defs>
      <clipPath id="clip0_795_2863">
        <rect width="24" height="24" fill="white" />
      </clipPath>
    </defs>
  </svg>
)

export const CalendarIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="21"
    height="23"
    viewBox="0 0 21 23"
    fill="none"
    {...props}
  >
    <path
      d="M17.8 3.1001H3.1C1.9402 3.1001 1 4.0403 1 5.2001V19.9001C1 21.0599 1.9402 22.0001 3.1 22.0001H17.8C18.9598 22.0001 19.9 21.0599 19.9 19.9001V5.2001C19.9 4.0403 18.9598 3.1001 17.8 3.1001Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M14.6499 1V5.2"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M6.25 1V5.2"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M1 9.3999H19.9"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const PictureIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    {...props}
  >
    <path
      d="M17 1H3C1.89543 1 1 1.89543 1 3V17C1 18.1046 1.89543 19 3 19H17C18.1046 19 19 18.1046 19 17V3C19 1.89543 18.1046 1 17 1Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M6.5 8C7.32843 8 8 7.32843 8 6.5C8 5.67157 7.32843 5 6.5 5C5.67157 5 5 5.67157 5 6.5C5 7.32843 5.67157 8 6.5 8Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M19 13L14 8L3 19"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const AudioIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    {...props}
  >
    <path
      d="M1 16V10C1 7.61305 1.94821 5.32387 3.63604 3.63604C5.32387 1.94821 7.61305 1 10 1C12.3869 1 14.6761 1.94821 16.364 3.63604C18.0518 5.32387 19 7.61305 19 10V16"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M19 17C19 17.5304 18.7893 18.0391 18.4142 18.4142C18.0391 18.7893 17.5304 19 17 19H16C15.4696 19 14.9609 18.7893 14.5858 18.4142C14.2107 18.0391 14 17.5304 14 17V14C14 13.4696 14.2107 12.9609 14.5858 12.5858C14.9609 12.2107 15.4696 12 16 12H19V17ZM1 17C1 17.5304 1.21071 18.0391 1.58579 18.4142C1.96086 18.7893 2.46957 19 3 19H4C4.53043 19 5.03914 18.7893 5.41421 18.4142C5.78929 18.0391 6 17.5304 6 17V14C6 13.4696 5.78929 12.9609 5.41421 12.5858C5.03914 12.2107 4.53043 12 4 12H1V17Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const VideoIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="25"
    height="19"
    viewBox="0 0 25 19"
    fill="none"
    {...props}
  >
    <path
      d="M23.293 4.00453C23.17 3.51336 22.9196 3.06333 22.5671 2.6999C22.2146 2.33647 21.7724 2.07251 21.2852 1.93467C19.5051 1.5 12.3848 1.5 12.3848 1.5C12.3848 1.5 5.2645 1.5 3.48442 1.97607C2.99723 2.1139 2.55503 2.37787 2.2025 2.7413C1.84997 3.10473 1.5996 3.55476 1.47666 4.04593C1.15088 5.85245 0.991521 7.68506 1.00059 9.5207C0.988979 11.3702 1.14835 13.2167 1.47666 15.0369C1.61219 15.5128 1.86818 15.9457 2.21989 16.2938C2.57159 16.6419 3.00713 16.8933 3.48442 17.0239C5.2645 17.5 12.3848 17.5 12.3848 17.5C12.3848 17.5 19.5051 17.5 21.2852 17.0239C21.7724 16.8861 22.2146 16.6221 22.5671 16.2587C22.9196 15.8953 23.17 15.4452 23.293 14.9541C23.6162 13.1612 23.7756 11.3425 23.769 9.5207C23.7806 7.67123 23.6213 5.82466 23.293 4.00453Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M10.0566 12.9052L16.0075 9.52094L10.0566 6.13672V12.9052Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const HorizontalLinesIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="22"
    height="7"
    viewBox="0 0 22 7"
    fill="none"
    {...props}
  >
    <path
      d="M1 1H13"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
    />
    <path
      d="M1 6H21"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
    />
  </svg>
)

export const CirclesIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="23"
    height="23"
    viewBox="0 0 23 23"
    fill="none"
    {...props}
  >
    <path
      d="M11.5 22C17.299 22 22 17.299 22 11.5C22 5.70101 17.299 1 11.5 1C5.70101 1 1 5.70101 1 11.5C1 17.299 5.70101 22 11.5 22Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M11.4996 14.6496C13.2393 14.6496 14.6496 13.2393 14.6496 11.4996C14.6496 9.75991 13.2393 8.34961 11.4996 8.34961C9.75991 8.34961 8.34961 9.75991 8.34961 11.4996C8.34961 13.2393 9.75991 14.6496 11.4996 14.6496Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const GridIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="14"
    height="16"
    viewBox="0 0 14 16"
    fill="none"
    {...props}
  >
    <path
      d="M12 14.0254L12 14.0171"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M12 8.0166L12 8.00827"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M12 2.00879L12 2.00046"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 14.0254L2 14.0171"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 8.0166L2 8.00827"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 2.00879L2 2.00046"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 14.0254L7 14.0171"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 8.0166L7 8.00827"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 2.00879L7 2.00046"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const DragIcon = (props: IconProps) => (
  <svg
    width="9"
    height="16"
    viewBox="0 0 9 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M2 14.0254L2 14.0171"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 8.0166L2 8.00827"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 2.00879L2 2.00046"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 14.0254L7 14.0171"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 8.0166L7 8.00827"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 2.00879L7 2.00046"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const DashboardIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    {...props}
  >
    <path
      d="M17 1H3C1.89543 1 1 1.89543 1 3V17C1 18.1046 1.89543 19 3 19H17C18.1046 19 19 18.1046 19 17V3C19 1.89543 18.1046 1 17 1Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M1 7H19"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 19V7"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const DirectoryIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="18"
    viewBox="0 0 20 18"
    fill="none"
    {...props}
  >
    <g clipPath="url(#clip0_1193_7997)">
      <path
        d="M20 16C20 16.5304 19.7893 17.0391 19.4142 17.4142C19.0391 17.7893 18.5304 18 18 18H2C1.46957 18 0.960859 17.7893 0.585786 17.4142C0.210714 17.0391 0 16.5304 0 16V2C0 1.46957 0.210714 0.960859 0.585786 0.585786C0.960859 0.210714 1.46957 0 2 0H7L9 3H18C18.5304 3 19.0391 3.21071 19.4142 3.58579C19.7893 3.96086 20 4.46957 20 5V16Z"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </g>
    <defs>
      <clipPath id="clip0_1193_7997">
        <rect width="20" height="18" fill="transparent" />
      </clipPath>
    </defs>
  </svg>
)

export const TextIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    {...props}
  >
    <path
      d="M1 4.375V1H19V4.375"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M6.625 19H13.375"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M10 1V19"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const TrashIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="22"
    viewBox="0 0 20 22"
    fill="none"
    {...props}
  >
    <path
      d="M1 5H3H19"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M17 5V19C17 19.5304 16.7893 20.0391 16.4142 20.4142C16.0391 20.7893 15.5304 21 15 21H5C4.46957 21 3.96086 20.7893 3.58579 20.4142C3.21071 20.0391 3 19.5304 3 19V5M6 5V3C6 2.46957 6.21071 1.96086 6.58579 1.58579C6.96086 1.21071 7.46957 1 8 1H12C12.5304 1 13.0391 1.21071 13.4142 1.58579C13.7893 1.96086 14 2.46957 14 3V5"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M8 10V16"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M12 10V16"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const HorizontalHandleIcon = (props: IconProps) => (
  <svg
    width="16"
    height="9"
    viewBox="0 0 16 9"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M1.97461 2L1.98294 2"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7.9834 2L7.99173 2"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.9912 2L13.9995 2"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M1.97461 7L1.98294 7"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7.9834 7L7.99173 7"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.9912 7L13.9995 7"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const VerticalHandleIcon = (props: IconProps) => (
  <svg
    width="9"
    height="16"
    viewBox="0 0 9 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M2 14.0254L2 14.0171"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 8.01758L2 8.00924"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 2.00781L2 1.99948"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 14.0254L7 14.0171"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 8.01758L7 8.00924"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 2.00781L7 1.99948"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const SaveIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    {...props}
  >
    <path
      d="M17 19H3C2.46957 19 1.96086 18.7893 1.58579 18.4142C1.21071 18.0391 1 17.5304 1 17V3C1 2.46957 1.21071 1.96086 1.58579 1.58579C1.96086 1.21071 2.46957 1 3 1H14L19 6V17C19 17.5304 18.7893 18.0391 18.4142 18.4142C18.0391 18.7893 17.5304 19 17 19Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M15 19V11H5V19"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M5 1V6H13"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const NotificationIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="22"
    viewBox="0 0 20 22"
    fill="none"
    {...props}
  >
    <path
      d="M16 7C16 5.4087 15.3679 3.88258 14.2426 2.75736C13.1174 1.63214 11.5913 1 10 1C8.4087 1 6.88258 1.63214 5.75736 2.75736C4.63214 3.88258 4 5.4087 4 7C4 14 1 16 1 16H19C19 16 16 14 16 7Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M11.73 20C11.5542 20.3031 11.3019 20.5547 10.9982 20.7295C10.6946 20.9044 10.3504 20.9965 10 20.9965C9.64964 20.9965 9.30541 20.9044 9.00179 20.7295C8.69818 20.5547 8.44583 20.3031 8.27002 20"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const ShortAnswerIcon = (props: IconProps) => (
  <svg
    width="22"
    height="7"
    viewBox="0 0 22 7"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M1 1H13"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
    />
    <path
      d="M1 6H21"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
    />
  </svg>
)

export const LongAnswerIcon = (props: IconProps) => (
  <svg
    width="20"
    height="14"
    viewBox="0 0 20 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M15 5H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M19 1H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M19 9H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M15 13H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const GridChoiceAnswerIcon = (props: IconProps) => (
  <svg
    width="14"
    height="16"
    viewBox="0 0 14 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M12 14.025L12 14.0167"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M12 8.01666L12 8.00833"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M12 2.00836L12 2.00003"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 14.025L2 14.0167"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 8.01666L2 8.00833"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 2.00836L2 2.00003"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 14.025L7 14.0167"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 8.01666L7 8.00833"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 2.00836L7 2.00003"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const MultipleChoiceAnswerIcon = (props: IconProps) => (
  <svg
    width="23"
    height="23"
    viewBox="0 0 23 23"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M11.5 22C17.299 22 22 17.299 22 11.5C22 5.70101 17.299 1 11.5 1C5.70101 1 1 5.70101 1 11.5C1 17.299 5.70101 22 11.5 22Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M11.5001 14.65C13.2398 14.65 14.6501 13.2397 14.6501 11.5C14.6501 9.76028 13.2398 8.34998 11.5001 8.34998C9.7604 8.34998 8.3501 9.76028 8.3501 11.5C8.3501 13.2397 9.7604 14.65 11.5001 14.65Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const SectionIcon = (props: IconProps) => (
  <svg
    width="23"
    height="23"
    viewBox="0 0 23 23"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M19.9 1H3.1C1.9402 1 1 1.9402 1 3.1V7.3C1 8.4598 1.9402 9.4 3.1 9.4H19.9C21.0598 9.4 22 8.4598 22 7.3V3.1C22 1.9402 21.0598 1 19.9 1Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M19.9 13.6H3.1C1.9402 13.6 1 14.5402 1 15.7V19.9C1 21.0598 1.9402 22 3.1 22H19.9C21.0598 22 22 21.0598 22 19.9V15.7C22 14.5402 21.0598 13.6 19.9 13.6Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const NumberIncrementIcon = (props: IconProps) => (
  <svg
    width="13"
    height="13"
    viewBox="0 0 13 13"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M3.25 7.7998L6.5 4.5498"
      stroke="currentColor"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
    <path
      d="M9.75 7.7998L6.5 4.5498"
      stroke="currentColor"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
  </svg>
)

export const NumberDecrementIcon = (props: IconProps) => (
  <svg
    width="13"
    height="13"
    viewBox="0 0 13 13"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M9.75 5.2002L6.5 8.4502"
      stroke="currentColor"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
    <path
      d="M3.25 5.2002L6.5 8.4502"
      stroke="currentColor"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
  </svg>
)

export const AttachmentIcon = (props: IconProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="22"
    height="23"
    viewBox="0 0 22 23"
    fill="none"
    {...props}
  >
    <path
      d="M20.4383 10.6622L11.2483 19.8522C10.1225 20.9781 8.59552 21.6106 7.00334 21.6106C5.41115 21.6106 3.88418 20.9781 2.75834 19.8522C1.63249 18.7264 1 17.1994 1 15.6072C1 14.015 1.63249 12.4881 2.75834 11.3622L11.9483 2.17222C12.6989 1.42166 13.7169 1 14.7783 1C15.8398 1 16.8578 1.42166 17.6083 2.17222C18.3589 2.92279 18.7806 3.94077 18.7806 5.00222C18.7806 6.06368 18.3589 7.08166 17.6083 7.83222L8.40834 17.0222C8.03306 17.3975 7.52406 17.6083 6.99334 17.6083C6.46261 17.6083 5.95362 17.3975 5.57834 17.0222C5.20306 16.6469 4.99222 16.138 4.99222 15.6072C4.99222 15.0765 5.20306 14.5675 5.57834 14.1922L14.0683 5.71222"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const Paragraph = (props: IconProps) => (
  <svg
    width="20"
    height="14"
    viewBox="0 0 20 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M15 5H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M19 1H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M19 9H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M15 13H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const MultipleChoiceRadioIcon = (props: IconProps) => (
  <svg
    width="20"
    height="14"
    viewBox="0 0 20 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M15 5H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M19 1H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M19 9H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M15 13H1"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const GridChoiceRadioIcon = (props: IconProps) => (
  <svg
    width="14"
    height="16"
    viewBox="0 0 14 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M12 14.025L12 14.0167"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M12 8.01666L12 8.00833"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M12 2.00836L12 2.00003"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 14.025L2 14.0167"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 8.01666L2 8.00833"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2 2.00836L2 2.00003"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 14.025L7 14.0167"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 8.01666L7 8.00833"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7 2.00836L7 2.00003"
      stroke="currentColor"
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const MultipleChoiceCheckboxesIcon = (props: IconProps) => (
  <svg
    width="23"
    height="23"
    viewBox="0 0 23 23"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M18.7692 1H4.23077C2.44646 1 1 2.44646 1 4.23077V18.7692C1 20.5535 2.44646 22 4.23077 22H18.7692C20.5535 22 22 20.5535 22 18.7692V4.23077C22 2.44646 20.5535 1 18.7692 1Z"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M18 7L9.5 16L6 11.3636"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export const GridChoiceCheckboxesIcon = (props: IconProps) => (
  <svg
    width="13"
    height="15"
    viewBox="0 0 13 15"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <rect width="3" height="3" fill="currentColor" />
    <rect y="6" width="3" height="3" fill="currentColor" />
    <rect y="12" width="3" height="3" fill="currentColor" />
    <rect x="5" width="3" height="3" fill="currentColor" />
    <rect x="5" y="6" width="3" height="3" fill="currentColor" />
    <rect x="5" y="12" width="3" height="3" fill="currentColor" />
    <rect x="10" width="3" height="3" fill="currentColor" />
    <rect x="10" y="6" width="3" height="3" fill="currentColor" />
    <rect x="10" y="12" width="3" height="3" fill="currentColor" />
  </svg>
)

export const LinkIcon = (props: IconProps) => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M10 6H6C4.89543 6 4 6.89543 4 8V18C4 19.1046 4.89543 20 6 20H16C17.1046 20 18 19.1046 18 18V14M14 4H20M20 4V10M20 4L10 14"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)
