export interface LinkProps {
  href: string
  target?: string
  children: React.ReactNode
  rel?: string
}

const Link = ({ children, href, target, rel }: LinkProps) => {
  return (
    <a href={href} target={target} rel={rel}>
      {children}
    </a>
  )
}

export default Link
