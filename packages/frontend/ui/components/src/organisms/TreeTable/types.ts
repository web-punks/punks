import { TreeTable } from "."
import { TableColumnSorting, TableColumnStyles } from "../Table"
import { TableColumnClasses } from "../Table/types"
import { TableColumnFilter } from "../../molecules/TableFilter"

export type TreeTableColumnRenderContext = {
  level: number
}

export type TreeTableColumnType<RecordType, FilterType, SortingType> = {
  key: string
  title: React.ReactNode
  render: (
    record: RecordType,
    context: TreeTableColumnRenderContext,
    rowNumber: number
  ) => React.ReactNode
  colSpan?: number
  minWidth?: number | string
  hidden?: boolean
  styles?: TableColumnStyles
  filter?: TableColumnFilter<FilterType>
  sorting?: TableColumnSorting<RecordType, SortingType>
  classes?: TableColumnClasses
  widthFactor?: number
}

export type TreeTableRowControlType<RecordType> = {
  key: string
  hidden?: boolean
  render: (record: RecordType) => React.ReactNode
  styles?: TableColumnStyles
}

export type TreeTableMainColumnType<RecordType, FilterType, SortingType> = Omit<
  TreeTableColumnType<RecordType, FilterType, SortingType>,
  "hidden"
>

export declare type TreeTableColumnsType<
  RecordType = unknown,
  FilterType = unknown,
  SortingType = unknown,
> = TreeTableColumnType<RecordType, FilterType, SortingType>[]

export declare type TreeTableRowControlsType<RecordType = unknown> =
  TreeTableRowControlType<RecordType>[]

export type TreeTableCellAlignment = "left" | "center" | "right"

export type TreeTableOptions = {
  columnsMinWidth?: number
  noRowsSeparator?: boolean
  evenOddRows?: boolean
  defaultAlignment?: TreeTableCellAlignment
}

export type TreeNode<RecordType> = {
  id: string
  record: RecordType
  childrenCount: number
  children?: TreeNode<RecordType>[]
  childrenLoading?: boolean
  expanded: boolean
}

export type TreeNodeEvents<RecordType> = {
  onExpand: (node: TreeNode<RecordType>) => void
  onCollapse: (node: TreeNode<RecordType>) => void
}

export type ExpandedTreeNode<RecordType> = {
  node: TreeNode<RecordType>
  context: TreeTableColumnRenderContext
}
