import styles from "./TreeTable.module.css"
import { TableEmptyContentComponentProps } from "../Table/components/TableEmptyContent"
import { TableGridType, TablePaging, TableSelection } from "../Table/types"
import {
  ExpandedTreeNode,
  TreeNode,
  TreeNodeEvents,
  TreeTableColumnsType,
  TreeTableMainColumnType,
  TreeTableOptions,
  TreeTableRowControlsType,
} from "./types"
import { Table } from "../Table"
import { useMemo } from "react"
import { expandRows } from "./utils"
import { classNames, setCssVar } from "@punks/ui-core"
import { Spinner } from "../../animations"
import { ChevronRightIcon } from "../../ui"
import { TableLabels, TableRowActions } from "../Table/Table"

export type TreeTableEmptyDataContent = {
  content: React.ReactNode
  component?: React.ComponentType<TableEmptyContentComponentProps>
}

export interface TreeTableProps<RecordType> {
  className?: string
  dataSource?: TreeNode<RecordType>[]
  loading?: boolean
  emptyData?: TreeTableEmptyDataContent
  paging?: TablePaging
  mainColumn: TreeTableMainColumnType<RecordType, unknown, unknown>
  columns: TreeTableColumnsType<RecordType>
  rowControls?: TreeTableRowControlsType<RecordType>
  options?: TreeTableOptions
  grid?: TableGridType
  chevronIcon?: React.ReactNode
  loadingIcon?: React.ReactNode
  events: TreeNodeEvents<RecordType>
  rowActions?: TableRowActions<ExpandedTreeNode<RecordType>>
  selection?: TableSelection<ExpandedTreeNode<RecordType>>
  labels: TableLabels
}

export const TreeTable = <RecordType extends {}>({
  className,
  dataSource,
  loading,
  paging,
  mainColumn,
  columns,
  rowControls,
  emptyData,
  options,
  grid,
  chevronIcon = <ChevronRightIcon width={12} height={12} />,
  loadingIcon = <Spinner sizeValue={12} />,
  events,
  rowActions,
  selection,
  labels,
}: TreeTableProps<RecordType>) => {
  const rows = useMemo(() => expandRows(dataSource ?? []), [dataSource])

  return (
    <Table
      labels={labels}
      className={className}
      loading={loading}
      paging={paging}
      options={options}
      grid={grid}
      emptyData={emptyData}
      dataSource={rows}
      rowActions={rowActions}
      selection={selection}
      rowControls={rowControls?.map((x) => ({
        ...x,
        render: (record: ExpandedTreeNode<RecordType>) =>
          x.render(record.node.record),
      }))}
      columns={[
        {
          key: mainColumn.key,
          render: (x, rowNumber) => (
            <div
              className={styles.mainColumn}
              style={{
                ...setCssVar("--l", x.context.level),
              }}
              onClick={() =>
                x.node.expanded
                  ? events.onCollapse(x.node)
                  : events.onExpand(x.node)
              }
            >
              <div className={styles.iconContainer}>
                {!x.node.childrenLoading && x.node.childrenCount > 0 && (
                  <div
                    className={classNames(styles.icon, {
                      [styles.iconExpanded]: x.node.expanded,
                    })}
                  >
                    {chevronIcon}
                  </div>
                )}
                {x.node.childrenLoading && <div>{loadingIcon}</div>}
              </div>
              <div className={classNames(styles.mainColumnContent)}>
                {mainColumn.render(x.node.record, x.context, rowNumber)}
              </div>
            </div>
          ),
          title: mainColumn.title,
          colSpan: mainColumn.colSpan,
          minWidth: mainColumn.minWidth,
          styles: mainColumn.styles,
          classes: mainColumn.classes,
          widthFactor: mainColumn.widthFactor,
        },
        ...columns.map((x, rowNumber) => ({
          key: x.key,
          render: (record: ExpandedTreeNode<RecordType>) =>
            x.render(record.node.record, record.context, rowNumber),
          title: x.title,
          colSpan: x.colSpan,
          minWidth: x.minWidth,
          hidden: x.hidden,
          styles: x.styles,
          classes: x.classes,
          widthFactor: x.widthFactor,
        })),
      ]}
    />
  )
}

export default TreeTable
