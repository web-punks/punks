import { ExpandedTreeNode, TreeNode } from "./types"

export const expandRows = <RecordType>(
  dataSource: TreeNode<RecordType>[]
): ExpandedTreeNode<RecordType>[] => {
  const expandedRows: ExpandedTreeNode<RecordType>[] = []

  const expand = (node: TreeNode<RecordType>, level: number) => {
    expandedRows.push({
      node,
      context: {
        level,
      },
    })
    if (node.children && node.expanded) {
      node.children.forEach((child) => expand(child, level + 1))
    }
  }
  dataSource.forEach((node) => expand(node, 0))

  return expandedRows
}
