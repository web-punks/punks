import type { Meta, StoryObj } from "@storybook/react"

import { TreeTable } from "."
import { range } from "@punks/ui-core"
import {
  TreeNode,
  TreeTableColumnType,
  TreeTableRowControlsType,
} from "./types"
import { Button } from "../../atoms"
import { EditIcon } from "../../ui"

type TableRow = {
  id: string
  title: string
  description: string
}

const items: TreeNode<TableRow>[] = range(200).map((x, i) => ({
  id: x.toString(),
  record: {
    id: x.toString(),
    title: `title ${x}`,
    description: `description ${x}`,
  },
  childrenCount: i % 2 === 0 ? 0 : 2,
  expanded: false,
  children:
    i % 2 === 0
      ? undefined
      : range(2).map((y) => ({
          id: `${x}-${y}`,
          record: {
            id: `${x}-${y}`,
            title: `title ${x}-${y}`,
            description: `description ${x}-${y}`,
          },
          childrenCount: 0,
          expanded: false,
          children: undefined,
        })),
}))

const columns: TreeTableColumnType<TableRow, unknown, unknown>[] = [
  {
    key: "id",
    title: "Id",
    render: (x) => x.id,
    widthFactor: undefined,
  },
  {
    key: "title",
    title: "Title",
    render: (x) => x.title,
    widthFactor: undefined,
  },
]

const rowControls: TreeTableRowControlsType<TableRow> = [
  {
    key: "actions",
    render: (x) => (
      <Button iconOnly rounded>
        <EditIcon width={18} />
      </Button>
    ),
  },
]

const meta = {
  title: "Ui/Organisms/TreeTable",
  component: TreeTable,
  tags: ["autodocs"],
  argTypes: {},
  args: {
    labels: {
      filters: {
        textFilter: {
          apply: "Apply",
          search: "Search",
          contains: "Contains",
          isEmpty: "Is empty",
          isNotEmpty: "Is not empty",
        },
        dateFilter: {
          apply: "Apply",
        },
      },
    },
    columns: columns.slice(1),
    mainColumn: columns[0],
    rowControls,
    events: {
      onExpand: () => {},
      onCollapse: () => {},
    },
  },
} satisfies Meta<typeof TreeTable<TableRow>>
export default meta

type Story = StoryObj<typeof meta>

export const TreeTableCollapsed: Story = {
  args: {
    dataSource: items,
  },
}

export const TreeTableExpanded: Story = {
  args: {
    dataSource: items.map((x) => ({
      ...x,
      expanded: true,
    })),
  },
}

export const TreeTableLoading: Story = {
  args: {
    loading: true,
  },
}
