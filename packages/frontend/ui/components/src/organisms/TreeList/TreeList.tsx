import { useState } from "react"
import TreeListElement from "./TreeListElement"
import { TreeListItem } from "./types"
import { SearchBar } from "../../molecules"
import { classNames } from "@punks/ui-core"
import styles from "./TreeList.module.css"

export type TreeListClasses = {
  root?: string
  searchBar?: string
  content?: string
  element?: string
}

export type TreeListFilter = {
  placeholder: string
  fullWidth?: boolean
}

export interface TreeListProps {
  items: TreeListItem[]
  defaultExpended?: boolean
  defaultExpendedIds?: string[]
  showNumbers?: boolean
  filter?: TreeListFilter
  className?: string
  classes?: TreeListClasses
}

const TreeList = ({
  items,
  defaultExpended,
  defaultExpendedIds,
  showNumbers,
  filter,
  className,
  classes,
}: TreeListProps) => {
  const [expandedIds, setExpandedIds] = useState<string[]>(
    defaultExpendedIds ?? defaultExpended ? items.map((x) => x.id) : []
  )
  const [query, setQuery] = useState<string>("")

  const handleToggle = (id: string) => {
    if (expandedIds.includes(id)) {
      setExpandedIds(expandedIds.filter((x) => x !== id))
    } else {
      setExpandedIds([...expandedIds, id])
    }
  }

  const filteredItems = query?.trim()
    ? items.filter((x) => x.name.toLowerCase().includes(query.toLowerCase()))
    : items

  return (
    <div className={classNames(classes?.root, className)}>
      {filter && (
        <SearchBar
          className={classNames(styles.searchBar, classes?.searchBar)}
          placeholder={filter.placeholder}
          value={query}
          fullWidth={filter.fullWidth}
          onChange={(e) => setQuery(e.target.value)}
          searchIconPosition="end"
        />
      )}
      <div className={classNames(classes?.content)}>
        {filteredItems.map((item, index) => (
          <TreeListElement
            className={classes?.element}
            key={index}
            item={item}
            showNumbers={showNumbers}
            expanded={expandedIds.includes(item.id)}
            onToggle={() => handleToggle(item.id)}
          />
        ))}
      </div>
    </div>
  )
}

export default TreeList
