import { classNames } from "@punks/ui-core"
import { Typography } from "../../atoms"
import { TreeListItem } from "./types"
import styles from "./TreeListElement.module.css"
import { ChevronRightIcon } from "../../ui/icons"
import { defaultRenderer } from "../../utils/render"
import { Fragment } from "react"

export interface TreeListElementProps {
  item: TreeListItem
  expanded?: boolean
  showNumbers?: boolean
  onToggle?: () => void
  className?: string
}

const TreeListElement = ({
  item,
  expanded,
  showNumbers,
  onToggle,
  className,
}: TreeListElementProps) => {
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.expanded]: expanded,
        },
        className
      )}
    >
      <div
        className={classNames(styles.item, item.className)}
        onClick={onToggle}
      >
        <div className={styles.arrow}>
          <ChevronRightIcon width={7} />
        </div>
        <Typography weight="bold">{item.name}</Typography>
        {showNumbers &&
          item.children &&
          (item.render ?? defaultRenderer)(
            <div className={styles.number}>{item.children.length}</div>
          )}
      </div>

      {item.children && (
        <div className={styles.subitems}>
          {item.children.map((child, index) => (
            <Fragment key={index}>
              {(child.render ?? defaultRenderer)(
                <div
                  className={classNames(styles.subitem, child.className)}
                  onClick={child.onClick}
                >
                  {child.name}
                </div>
              )}
            </Fragment>
          ))}
        </div>
      )}
    </div>
  )
}

export default TreeListElement
