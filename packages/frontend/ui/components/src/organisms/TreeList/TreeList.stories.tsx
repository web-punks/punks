import type { Meta, StoryObj } from "@storybook/react"

import { TreeList } from "."

const meta = {
  title: "Ui/Organisms/TreeList",
  component: TreeList,
  tags: ["autodocs"],
  argTypes: {},
  args: {
    items: [
      {
        id: "1",
        name: "Item 1",
        children: [
          {
            name: "Item 1.1",
          },
          {
            name: "Item 1.2",
          },
          {
            name: "Item 1.3",
          },
        ],
      },
      {
        id: "2",
        name: "Item 2",
        children: [
          {
            name: "Item 2.1",
          },
          {
            name: "Item 2.2",
          },
          {
            name: "Item 2.3",
          },
        ],
      },
      {
        id: "3",
        name: "Item 3",
        children: [
          {
            name: "Item 3.1",
          },
          {
            name: "Item 3.2",
          },
          {
            name: "Item 3.3",
          },
        ],
      },
    ],
  },
} satisfies Meta<typeof TreeList>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
}

export const DefaultExpanded: Story = {
  args: {
    defaultExpended: true,
  },
}

export const WithNumbers: Story = {
  args: {
    showNumbers: true,
  },
}

export const Filterable: Story = {
  args: {
    filter: {
      placeholder: "Filter",
      fullWidth: true,
    },
  },
}
