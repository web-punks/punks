export type TreeListItemChild = {
  name: string
  onClick?: () => void
  className?: string
  render?: (item: React.ReactNode) => React.ReactNode
}

export type TreeListItem = {
  id: string
  name: string
  onClick?: () => void
  children?: TreeListItemChild[]
  className?: string
  render?: (item: React.ReactNode) => React.ReactNode
}
