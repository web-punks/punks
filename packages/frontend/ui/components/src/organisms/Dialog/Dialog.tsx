import { classNames } from "@punks/ui-core"
import { Modal } from "../../panels"
import styles from "./Dialog.module.css"
import { CloseIcon } from "../../ui/icons"
import { Button, Typography } from "../../atoms"

export type DialogAction = {
  label?: React.ReactNode
  loading?: boolean
  disabled?: boolean
  hidden?: boolean
  onClick?: () => void
  primary?: boolean
}

export type ActionsAlignment = "left" | "center" | "right"

export type DialogActions = {
  items: DialogAction[]
  alignment?: ActionsAlignment
}

export interface DialogProps {
  title?: React.ReactNode
  actions: DialogActions
  open: boolean
  onClose?: () => void
  closeHidden?: boolean
  children: any
  fullScreen?: boolean
}

const Dialog = ({
  actions,
  children,
  closeHidden,
  onClose,
  open,
  title,
  fullScreen,
}: DialogProps) => {
  return (
    <Modal
      open={open}
      onClose={onClose}
      fullScreen={fullScreen}
      className={styles.root}
    >
      <div className={styles.head}>
        <div className={styles.headStart}></div>
        <div className={styles.headTitle}>
          <Typography variant="h3">{title}</Typography>
        </div>
        <div className={styles.headClose}>
          {!closeHidden && (
            <button onClick={onClose} className={styles.close}>
              <CloseIcon />
            </button>
          )}
        </div>
      </div>
      <div className={styles.body}>{children}</div>
      <div
        className={classNames(
          styles.actions,
          styles[`actions-${actions.alignment ?? "right"}`]
        )}
      >
        {actions.items.map((action, index) => (
          <Button
            key={index}
            onClick={action.onClick}
            variant={action.primary ? "filled" : "text"}
            color={action.primary ? "primary" : undefined}
          >
            {action.label}
          </Button>
        ))}
      </div>
    </Modal>
  )
}

export default Dialog
