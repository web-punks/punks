import type { Meta, StoryObj } from "@storybook/react"
import Dialog from "./Dialog"
import { useState } from "react"
import { Button, Typography } from "../../atoms"

const actions = {
  items: [
    {
      label: "Cancel",
      onClick: () => console.log("cancel"),
    },
    {
      label: "OK",
      onClick: () => console.log("OK"),
      primary: true,
    },
  ],
}

const meta = {
  title: "Ui/Organisms/Dialog",
  component: Dialog,
  tags: ["autodocs"],
  render: (args) => {
    const [isOpen, setIsOpen] = useState(false)

    const openModal = () => {
      setIsOpen(true)
    }

    const closeModal = () => {
      setIsOpen(false)
    }

    return (
      <>
        <Button variant="filled" color="primary" onClick={openModal}>
          Open Dialog
        </Button>
        <Dialog
          open={isOpen}
          onClose={closeModal}
          fullScreen={args.fullScreen}
          {...(args as any)}
        >
          {args.children}
        </Dialog>
      </>
    )
  },
  args: {
    title: "Title",
    children: (
      <>
        <Typography>Dialog Content</Typography>
        <Typography>This is the content of the dialog.</Typography>
      </>
    ),
    actions,
  },
} satisfies Meta<typeof Dialog>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {} as any,
}

export const FullScreen: Story = {
  args: {
    fullScreen: true,
  } as any,
}

export const ActionsCentered: Story = {
  args: {
    actions: {
      ...actions,
      alignment: "center",
    },
  } as any,
}
