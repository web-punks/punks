import React from "react"
import { Button, ButtonVariant, Typography } from "../../atoms"
import { FileDropControl } from "../../molecules/FileDropControl"
import styles from "./FileUploadPanel.module.css"
import { ThemeColor, classNames } from "@punks/ui-core"

export type FileUploadPanelCta = {
  label: React.ReactNode
  color?: ThemeColor
  variant?: ButtonVariant
  loading?: boolean
}

export interface FileUploadPanelProps {
  accept?: string
  multiple?: boolean
  subtitle?: React.ReactNode
  middleLabel?: React.ReactNode
  placeholder?: any
  selectCta?: FileUploadPanelCta
  submitCta?: FileUploadPanelCta
  onChange?: (files: File[]) => void
  onSubmit?: (files: File[]) => void
  submitInProgress?: boolean
  filesListHidden?: boolean
  noGutters?: boolean
  autoSubmit?: boolean
}

const FileUploadPanel: React.FC<FileUploadPanelProps> = ({
  accept,
  multiple = false,
  subtitle,
  middleLabel,
  placeholder,
  selectCta,
  submitCta,
  onChange,
  onSubmit,
  submitInProgress,
  filesListHidden,
  noGutters,
  autoSubmit,
}) => {
  const inputRef = React.useRef<HTMLInputElement>(null)

  return (
    <div
      className={classNames(styles.root, {
        [styles.rootGutters]: !noGutters,
      })}
    >
      {selectCta && (
        <Button
          className={styles.selectCta}
          variant={selectCta.variant ?? "filled"}
          color={selectCta.color ?? "primary"}
          onClick={() => !submitInProgress && inputRef.current?.click()}
          loading={selectCta.loading}
        >
          {selectCta.label}
        </Button>
      )}
      {middleLabel && (
        <Typography className={styles.subtitle} variant="body3" my={4}>
          {middleLabel}
        </Typography>
      )}
      <FileDropControl
        placeholder={placeholder}
        fullWidth
        accept={accept}
        multiple={multiple}
        onChange={onChange}
        onSubmit={onSubmit}
        submitInProgress={submitInProgress}
        cta={{
          label: submitCta?.label ?? "Upload",
          loading: submitCta?.loading,
        }}
        classes={{
          root: styles.dropControl,
        }}
        filesListHidden={filesListHidden}
        autoSubmit={autoSubmit}
        inputRef={inputRef}
      />
      {subtitle && (
        <Typography className={styles.subtitle} variant="body3">
          {subtitle}
        </Typography>
      )}
    </div>
  )
}

export default FileUploadPanel
