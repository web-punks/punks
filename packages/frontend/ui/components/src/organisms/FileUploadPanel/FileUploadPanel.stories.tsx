import type { Meta, StoryObj } from "@storybook/react"
import { FileUploadPanel } from "."
import { Typography } from "../../atoms"

const meta = {
  title: "UI/Organisms/FileUploadPanel",
  component: FileUploadPanel,
  tags: ["autodocs"],
  args: {
    subtitle: "Subtitle",
    placeholder: (
      <div>
        <Typography>or</Typography>
        <Typography>
          <b>drag here</b>
        </Typography>
      </div>
    ),
    selectCta: {
      label: "Click here",
    },
    submitCta: {
      label: "Upload",
    },
  },
} satisfies Meta<typeof FileUploadPanel>
export default meta

type Story = StoryObj<typeof meta>

export const Single: Story = {}
export const SinglePdf: Story = {
  args: {
    accept: ".pdf",
  },
}

export const Multiple: Story = {
  args: {
    multiple: true,
  },
}

export const MultiplePdf: Story = {
  args: {
    multiple: true,
    accept: ".pdf",
  },
}
