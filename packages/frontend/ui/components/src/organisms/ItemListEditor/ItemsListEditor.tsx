import { classNames } from "@punks/ui-core"
import {
  DragDropContext,
  Droppable,
  Draggable,
  DropResult,
} from "@hello-pangea/dnd"
import { ActionButton } from "../../molecules"
import { AddIcon, HorizontalHandleIcon, VerticalHandleIcon } from "../../ui"
import styles from "./ItemsListEditor.module.css"

export type ItemsListEditorClasses = {
  root?: string
  item?: string
  content?: string
  handle?: string
  addControl?: string
}

export type ItemsListItemMovedResult = {
  sourceIndex: number
  destinationIndex: number
}

export type ItemsListEditorHandlePosition = "left" | "right" | "top" | "bottom"

export type AddButtonPosition = "left" | "center" | "right"

export interface ItemsListEditorProps<
  T extends {
    id: string
  },
> {
  id: string
  items: T[]
  onItemMoved?: (result: ItemsListItemMovedResult) => void
  onAddItem?: () => void
  className?: string
  classes?: ItemsListEditorClasses
  renderItemContent: (item: T, index: number) => React.ReactNode
  addIcon?: React.ReactNode
  addButtonPosition?: AddButtonPosition
  handlePosition?: ItemsListEditorHandlePosition
}

const ItemsListEditor = <
  T extends {
    id: string
  },
>({
  id,
  renderItemContent,
  className,
  classes,
  items,
  onAddItem,
  onItemMoved,
  addIcon,
  addButtonPosition = "center",
  handlePosition = "left",
}: ItemsListEditorProps<T>) => {
  const handleDragEnd = (result: DropResult) => {
    onItemMoved?.({
      sourceIndex: result.source.index,
      destinationIndex: result.destination?.index ?? -1,
    })
  }

  return (
    <div>
      <DragDropContext onDragEnd={handleDragEnd}>
        <Droppable droppableId={id}>
          {(provided) => (
            <div
              className={classNames(styles.root, classes?.root, className)}
              ref={provided.innerRef}
              {...provided.droppableProps}
            >
              {items.map((item, index) => (
                <Draggable key={item.id} draggableId={item.id} index={index}>
                  {(provided) => {
                    return (
                      <div
                        ref={provided.innerRef}
                        className={styles.item}
                        {...provided.draggableProps}
                      >
                        <div
                          className={classNames(
                            {
                              [styles.handleContainerHorizontal]:
                                handlePosition === "left" ||
                                handlePosition === "right",
                            },
                            classes?.item
                          )}
                        >
                          {handlePosition === "top" && (
                            <div
                              className={classNames(
                                styles.handleTop,
                                classes?.handle
                              )}
                              {...provided.dragHandleProps}
                            >
                              <HorizontalHandleIcon />
                            </div>
                          )}

                          {handlePosition === "left" && (
                            <div
                              className={classNames(
                                styles.handleLeft,
                                classes?.handle
                              )}
                              {...provided.dragHandleProps}
                            >
                              <VerticalHandleIcon />
                            </div>
                          )}

                          <div
                            className={classNames(
                              styles.content,
                              {
                                [styles.contentHorizontal]:
                                  handlePosition === "left" ||
                                  handlePosition === "right",
                              },
                              classes?.content
                            )}
                          >
                            {renderItemContent(item, index)}
                          </div>

                          {handlePosition === "bottom" && (
                            <div
                              className={classNames(
                                styles.handleBottom,
                                classes?.handle
                              )}
                              {...provided.dragHandleProps}
                            >
                              <HorizontalHandleIcon />
                            </div>
                          )}

                          {handlePosition === "right" && (
                            <div
                              className={classNames(
                                styles.handleRight,
                                classes?.handle
                              )}
                              {...provided.dragHandleProps}
                            >
                              <VerticalHandleIcon />
                            </div>
                          )}
                        </div>
                      </div>
                    )
                  }}
                </Draggable>
              ))}
              {provided.placeholder}
              {onAddItem && (
                <div
                  className={classNames(
                    styles.addActionRow,
                    styles[`addActionRow-${addButtonPosition}`],
                    classes?.addControl
                  )}
                >
                  <ActionButton onClick={onAddItem}>
                    {addIcon ?? <AddIcon />}
                  </ActionButton>
                </div>
              )}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </div>
  )
}

export default ItemsListEditor
