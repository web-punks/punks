import { classNames } from "@punks/ui-core"
import { FormContainer } from "../../forms"
import { CancelConfirmCta, CancelConfirmCtaAlign } from "../../molecules"
import { Separator } from "../../structure"

export type FormCta = {
  cancel?: CancelConfirmCta
  confirm?: CancelConfirmCta
  align?: CancelConfirmCtaAlign
}

export type FormClasses = {
  root?: string
  body?: string
  separator?: string
  cta?: string
}

export interface FormRootProps {
  children: any
  cta: FormCta
  classes?: FormClasses
  className?: string
}

const FormRoot = ({ children, cta, className, classes }: FormRootProps) => {
  return (
    <FormContainer className={classNames(classes?.root, className)}>
      <div className={classes?.body}>{children}</div>
      <Separator className={classes?.separator} />
      <CancelConfirmCta
        className={classes?.cta}
        align="right"
        cancel={cta.cancel}
        confirm={cta.confirm}
      />
    </FormContainer>
  )
}

export default FormRoot
