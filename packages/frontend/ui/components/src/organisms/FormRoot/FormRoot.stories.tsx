import type { Meta, StoryObj } from "@storybook/react"
import { FormRoot } from "."
import { Typography } from "../../atoms"

const meta = {
  title: "Ui/Organisms/FormRoot",
  component: FormRoot,
  tags: ["autodocs"],
} satisfies Meta<typeof FormRoot>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    children: (
      <>
        <Typography>Form content</Typography>
      </>
    ),
    cta: {
      align: "right",
      cancel: {
        label: "Cancel",
        onClick: () => {},
      },
      confirm: {
        label: "Confirm",
        onClick: () => {},
      },
    },
  },
}
