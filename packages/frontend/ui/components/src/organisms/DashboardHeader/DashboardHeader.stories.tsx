import type { Meta, StoryObj } from "@storybook/react"

import { DashboardHeader } from "."
import { DropPanel } from "../../panels"

const meta = {
  title: "Ui/Organisms/DashboardHeader",
  component: DashboardHeader,
  tags: ["autodocs"],
  args: {
    logo: (
      <img
        src="https://a.storyblok.com/f/196735/601x601/29667fe0ad/fav10.png"
        style={{
          maxWidth: "100%",
        }}
        width={50}
      />
    ),
    userInfo: {
      name: "John Doe",
      title: "Admin",
      initials: "JD",
    },
  },
} satisfies Meta<typeof DashboardHeader>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {}

export const LogoLeft: Story = {
  args: {
    logoPosition: "left",
  },
}

export const WithMenuPanel: Story = {
  args: {
    logoPosition: "left",
    userMenu: <>MENU</>,
  },
}

export const WithMenuAdditionalPanel: Story = {
  args: {
    logoPosition: "left",
    userMenu: <>MENU</>,
    endControls: (
      <DropPanel control="IT">
        <div>IT</div>
        <div>EN</div>
      </DropPanel>
    ),
  },
}
