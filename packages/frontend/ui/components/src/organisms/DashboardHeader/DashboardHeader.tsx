import { classNames, ClassValue } from "@punks/ui-core"
import styles from "./DashboardHeader.module.css"
import { AccountInfo } from "../../molecules"
import { DropPanel } from "../../panels"
import SidebarTrigger from "../Sidebar/SidebarTrigger"

export type DashboardHeaderClasses = {
  root?: string
  central?: string
  left?: string
  right?: string
  userMenu?: string
}

export type DashboardUserInfo = {
  name: string
  title: string
  initials?: string
  avatarClasses?: ClassValue
}

export type DashboardHeaderLogoPosition = "left" | "center"

export type SidebarTrigger = {
  children?: React.ReactNode
  className?: ClassValue
}

export interface DashboardHeaderProps {
  logo?: React.ReactNode
  logoPosition?: DashboardHeaderLogoPosition
  className?: ClassValue
  classes?: DashboardHeaderClasses
  userInfo: DashboardUserInfo
  userMenu?: React.ReactNode
  startControls?: React.ReactNode
  middleControls?: React.ReactNode
  endControls?: React.ReactNode
  sidebarTrigger?: SidebarTrigger
}

const DashboardHeader = ({
  logo,
  logoPosition = "center",
  className,
  classes,
  userInfo,
  userMenu,
  startControls,
  middleControls,
  endControls,
  sidebarTrigger,
}: DashboardHeaderProps) => {
  return (
    <header className={classNames(styles.root, classes?.root, className)}>
      <div className={classNames(styles.left, classes?.left)}>
        {logoPosition === "left" && logo}
        {startControls}
      </div>
      <div className={classNames(styles.central, classes?.central)}>
        {logoPosition === "center" && logo}
        {middleControls}
      </div>
      <div className={classNames(styles.right, classes?.right)}>
        {userMenu ? (
          <DropPanel
            dropIconHidden
            control={
              <AccountInfo
                title={userInfo.name}
                subtitle={userInfo.title}
                avatar={{
                  initials: userInfo.initials,
                  variant: "outlined",
                }}
                avatarClasses={userInfo.avatarClasses}
              />
            }
            classes={{
              panel: classNames(styles.menu, classes?.userMenu),
            }}
          >
            {userMenu}
          </DropPanel>
        ) : (
          <AccountInfo
            title={userInfo.name}
            subtitle={userInfo.title}
            avatar={{
              initials: userInfo.initials,
              variant: "outlined",
            }}
          />
        )}
        {endControls}
        <SidebarTrigger className={sidebarTrigger?.className}>
          {sidebarTrigger?.children}
        </SidebarTrigger>
      </div>
    </header>
  )
}

export default DashboardHeader
