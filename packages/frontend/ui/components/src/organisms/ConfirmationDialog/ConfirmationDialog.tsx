import {
  CancelConfirmCta,
  CancelConfirmCtaAlign,
} from "../../molecules/CancelConfirmCta"
import { Typography } from "../../atoms/Typography"
import { Modal } from "../../panels"
import { Box } from "../../structure"

export interface ConfirmationDialogProps {
  open: boolean
  onClose?: () => void
  title?: React.ReactNode
  message?: React.ReactNode
  detail?: React.ReactNode
  cancel?: CancelConfirmCta
  confirm: CancelConfirmCta
  align?: CancelConfirmCtaAlign
}

export const ConfirmationDialog = ({
  confirm,
  message,
  detail,
  cancel,
  open,
  onClose,
  title,
  align = "right",
}: ConfirmationDialogProps) => {
  return (
    <Modal
      head={{
        title,
      }}
      open={open}
      onClose={() => onClose?.()}
    >
      <Typography my={4}>{message}</Typography>
      {detail}
      <Box mt={4}>
        <CancelConfirmCta align={align} confirm={confirm} cancel={cancel} />
      </Box>
    </Modal>
  )
}
