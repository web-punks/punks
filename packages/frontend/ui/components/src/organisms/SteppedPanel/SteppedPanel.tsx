import { Stepper } from "../../molecules"
import { classNames } from "@punks/ui-core"
import styles from "./SteppedPanel.module.css"

export interface StepPanelClasses {
  root?: string
  head?: string
  tab?: string
  content?: string
}

export interface StepPanelItem {
  title?: React.ReactNode
  content: React.ReactNode
}

export interface SteppedPanelProps {
  activeStep: number
  steps: StepPanelItem[]
  className?: string
  classes?: StepPanelClasses
}

const SteppedPanel = ({
  steps,
  activeStep,
  className,
  classes,
}: SteppedPanelProps) => {
  return (
    <div className={classNames(classes?.root, className)}>
      <Stepper
        activeStep={activeStep}
        steps={steps.map((step) => ({ title: step.title }))}
        fullWidth
      />
      <div>
        {steps.map((x, i) => {
          return (
            <div
              key={i}
              className={classNames(
                styles.stepContent,
                {
                  [styles.hidden]: i !== activeStep,
                },
                classes?.content
              )}
            >
              {x.content}
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default SteppedPanel
