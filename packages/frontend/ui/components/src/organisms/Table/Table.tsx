import React, { useState } from "react"
import styles from "./Table.module.css"
import { Typography } from "../../atoms/Typography"
import { classNames, isLast, normalizePxSize, setCssVar } from "@punks/ui-core"
import {
  TableColumnType,
  TableColumnSortingDirection,
  TableColumnsType,
  TableRowControlsType,
  TablePaging,
  TableSorting,
  TableOptions,
  TableSelection,
  TableSortingState,
  TableGridType,
} from "./types"
import { useTableItemsFilter } from "./hooks/useTableItemsFilter"
import { TablePagination } from "./components/TablePagination"
import { useTableSorter } from "./hooks/useTableSorter"
import TableEmptyContent, {
  TableEmptyContentComponentProps,
} from "./components/TableEmptyContent"
import TableLoader from "./components/TableLoader"
import { useTableRowActions } from "./hooks/useTableRowActions"
import TableFilter, {
  TableFilterLabels,
} from "../../molecules/TableFilter/TableFilter"
import { SortingControl } from "../../atoms/SortingControl"

export type TableLabels = {
  filters: TableFilterLabels
}

export type TableClasses = {
  root?: string
  tableContainer?: string
  table?: string
  header?: string
  body?: string
  headerCell?: string
  bodyCell?: string
}

export type TableEmptyDataContent = {
  content: React.ReactNode
  component?: React.ComponentType<TableEmptyContentComponentProps>
}

export type TableRowActions<RecordType> = {
  onSingleClick?: (item: RecordType) => void
  onDoubleClick?: (item: RecordType) => void
}

export interface TableProps<RecordType, SortingType> {
  classes?: TableClasses
  className?: string
  dataSource?: RecordType[]
  loading?: boolean
  columns: TableColumnsType<RecordType>
  rowControls?: TableRowControlsType<RecordType>
  rowActions?: TableRowActions<RecordType>
  emptyData?: TableEmptyDataContent
  options?: TableOptions
  paging?: TablePaging
  selection?: TableSelection<RecordType>
  sorting?: TableSorting<SortingType>
  grid?: TableGridType
  style?: React.CSSProperties
  labels?: TableLabels
  locale?: string
}

const Table = <RecordType extends {}, SortingType>({
  classes,
  className,
  dataSource,
  columns,
  rowControls,
  rowActions,
  emptyData,
  loading,
  options,
  paging,
  selection,
  sorting,
  style,
  grid = "minimal",
  labels = {
    filters: {
      textFilter: {
        apply: "Apply",
        search: "Search",
        contains: "Contains",
        isEmpty: "Is empty",
        isNotEmpty: "Is not empty",
      },
      dateFilter: {
        apply: "Apply",
      },
    },
  },
  locale = "en",
}: TableProps<RecordType, SortingType>) => {
  const [uncontrolledPaging, setUncontrolledPaging] = useState<{
    index: number
  }>({ index: 0 })
  const [uncontrolledSelection, setUncontrolledSelection] = useState<
    RecordType[]
  >([])
  const [tableSorting, setTableSorting] =
    useState<TableSortingState<RecordType, SortingType>>()
  const { sortedData } = useTableSorter({
    data: dataSource ?? [],
    sorting: tableSorting,
    paginationMode: paging?.mode,
  })

  const { items: filteredItems } = useTableItemsFilter({
    items: sortedData,
    pagination: paging
      ? {
          pageIndex: paging.index ?? uncontrolledPaging.index,
          pageSize: paging.size,
        }
      : undefined,
    paginationMode: paging?.mode,
  })

  const getSortingValue = (
    column: TableColumnType<RecordType, unknown, SortingType>
  ): TableColumnSortingDirection => {
    if (!sorting || tableSorting?.column !== column) {
      return column.sorting?.defaultDirection ?? "asc"
    }

    return tableSorting.direction === "asc" ? "desc" : "asc"
  }

  const toggleSorting = (
    column: TableColumnType<RecordType, unknown, SortingType>
  ) => {
    setTableSorting({
      column,
      direction: getSortingValue(column),
    })
  }

  const defaultAlignment = options?.defaultAlignment ?? "left"

  const { isRowClickable, isSelected, handleRowClick, handleRowDoubleClick } =
    useTableRowActions({
      selection: selection
        ? {
            selectedItems: selection?.selected ?? uncontrolledSelection,
            onSelectionChange: (items) => {
              if (!selection?.selected) {
                setUncontrolledSelection(items)
              }
              selection?.onSelectionChange?.(items)
            },
            mode: selection?.mode ?? "single",
          }
        : undefined,
      rowActions,
    })

  return (
    <div
      className={classNames(styles.root, classes?.root, className)}
      style={style}
    >
      <div
        className={classNames(styles.tableContainer, classes?.tableContainer)}
      >
        <table className={classNames(styles.table, classes?.table)}>
          <thead className={classes?.header}>
            <tr>
              {columns.map((column, colIndex) => (
                <th
                  className={classNames(
                    styles.th,
                    {
                      [styles.colLeft]:
                        (column.styles?.align ?? defaultAlignment) === "left",
                      [styles.colCenter]:
                        (column.styles?.align ?? defaultAlignment) === "center",
                      [styles.colRight]:
                        (column.styles?.align ?? defaultAlignment) === "right",
                    },
                    classes?.headerCell,
                    column.classes?.header
                  )}
                  key={column.key}
                  colSpan={column.colSpan}
                >
                  <div
                    className={classNames(styles.headerContainer, {
                      [styles.gridVertical]:
                        (grid === "full" || grid === "vertical") &&
                        colIndex > 0,
                    })}
                  >
                    <div
                      className={classNames(
                        styles.header,
                        {
                          [styles.notFirstTitle]: colIndex > 0,
                          [styles.notLastTitle]: colIndex < columns.length - 1,
                        },
                        {
                          // todo decide if filter's controls should also have possibility to be right next to the text (not only in the left side)
                          [styles.headerColLeft]:
                            (column.styles?.align ?? defaultAlignment) ===
                            "left",
                          [styles.headerColCenter]:
                            (column.styles?.align ?? defaultAlignment) ===
                            "center",
                          [styles.headerColRight]:
                            (column.styles?.align ?? defaultAlignment) ===
                            "right",
                        }
                      )}
                    >
                      <Typography
                        variant="body2"
                        // todo ask if the clicking for the sorting should span across the whole th right to the filter's controls
                        className={styles.text}
                        weight="bold"
                        color="grey-60"
                        // className={classNames(styles.header, {
                        //   [styles.notFirstTitle]: colIndex > 0,
                        //   [styles.notLastTitle]: colIndex < columns.length - 1,
                        // })}
                        component="div"
                        onClick={
                          column.sorting
                            ? () =>
                                toggleSorting(
                                  column as TableColumnType<
                                    RecordType,
                                    unknown,
                                    SortingType
                                  >
                                )
                            : undefined
                        }
                      >
                        {column.title}
                      </Typography>
                      {column.filter && (
                        <div className={styles.filterContainer}>
                          <TableFilter
                            labels={labels.filters}
                            filter={column.filter}
                            locale={locale}
                          />
                        </div>
                      )}
                      {column.sorting && column.sorting.enabled !== false && (
                        <div className={classNames(styles.headerSorting)}>
                          <SortingControl
                            value={
                              sorting?.value?.column &&
                              sorting.value.column === column.sorting?.value
                                ? sorting?.value?.direction
                                : undefined
                            }
                            onChange={(direction) =>
                              sorting?.onChange({
                                column: column.sorting?.value as SortingType,
                                direction,
                              })
                            }
                          />
                        </div>
                      )}
                    </div>
                  </div>
                </th>
              ))}
            </tr>
          </thead>

          <tbody className={styles.body}>
            {!loading
              ? filteredItems.map((row, index) => (
                  <tr
                    key={index}
                    className={classNames(styles.row, {
                      [styles.evenRow]: options?.evenOddRows && index % 2 === 0,
                      [styles.oddRow]: options?.evenOddRows && index % 2 === 1,
                      [styles.rowClickable]: isRowClickable(row),
                      [styles.rowSelected]: isSelected(row),
                      [styles.rowGridHorizontal]:
                        (grid === "full" || grid === "horizontal") &&
                        !isLast(index, filteredItems),
                    })}
                    onClick={() => handleRowClick(row)}
                    onDoubleClick={() => handleRowDoubleClick(row)}
                  >
                    {columns.map(
                      (column, colIndex) =>
                        !column.hidden && (
                          <td
                            style={{
                              ...(column.widthFactor
                                ? setCssVar("--w-factor", column.widthFactor)
                                : {}),
                              ...(column.minWidth
                                ? setCssVar(
                                    "--custom-w",
                                    normalizePxSize(column.minWidth)
                                  )
                                : {}),
                            }}
                            className={classNames(
                              styles.td,
                              {
                                [styles.colGridVertical]:
                                  (grid === "full" || grid === "vertical") &&
                                  colIndex > 0,
                                [styles.colLeft]:
                                  (column.styles?.align ?? defaultAlignment) ===
                                  "left",
                                [styles.colCenter]:
                                  (column.styles?.align ?? defaultAlignment) ===
                                  "center",
                                [styles.colRight]:
                                  (column.styles?.align ?? defaultAlignment) ===
                                  "right",
                                [styles.cellBorderLeft]:
                                  column.styles?.borders?.includes("left"),
                                [styles.cellBorderRight]:
                                  column.styles?.borders?.includes("right"),
                                [styles.colWidth]: !!column.widthFactor,
                                [styles.colCustomWidth]: !!column.minWidth,
                              },
                              classes?.bodyCell,
                              column.classes?.body
                            )}
                            colSpan={column.colSpan}
                            key={column.key}
                          >
                            <Typography
                              weight={column.styles?.fontWeight}
                              variant="body1"
                              component="div"
                            >
                              {column.render(row, index)}
                            </Typography>
                          </td>
                        )
                    )}
                    {!!rowControls?.length &&
                      rowControls.map(
                        (control, controlIndex) =>
                          !control.hidden && (
                            <td
                              className={classNames(
                                styles.td,
                                {
                                  [styles.colGridVertical]:
                                    (grid === "full" || grid === "vertical") &&
                                    controlIndex + columns.length > 0,
                                  [styles.colLeft]:
                                    (control.styles?.align ??
                                      defaultAlignment) === "left",
                                  [styles.colCenter]:
                                    (control.styles?.align ??
                                      defaultAlignment) === "center",
                                  [styles.colRight]:
                                    (control.styles?.align ??
                                      defaultAlignment) === "right",
                                  [styles.cellBorderLeft]:
                                    control.styles?.borders?.includes("left"),
                                  [styles.cellBorderRight]:
                                    control.styles?.borders?.includes("right"),
                                },
                                classes?.bodyCell,
                                styles.actionsCell
                              )}
                              key={control.key}
                            >
                              <Typography
                                weight={control.styles?.fontWeight}
                                variant="body1"
                                component="div"
                              >
                                {control.render(row)}
                              </Typography>
                            </td>
                          )
                      )}
                  </tr>
                ))
              : undefined}
          </tbody>
        </table>
      </div>

      {loading && (
        <div className={styles.loader}>
          <TableLoader />
        </div>
      )}
      {!loading && filteredItems.length === 0 && emptyData ? (
        <TableEmptyContent
          content={emptyData.content}
          component={emptyData.component}
        />
      ) : undefined}
      {paging && dataSource && (
        <TablePagination
          pagination={{
            pageIndex: paging.index ?? uncontrolledPaging.index,
            pageSize: paging.size,
          }}
          onPageChange={(value, pageSize) =>
            paging.onPagingChange?.(value, pageSize) ??
            setUncontrolledPaging({
              index: value,
            })
          }
          parameters={
            paging.mode === "clientSide"
              ? {
                  pageItems: filteredItems.length,
                  totItems: dataSource.length ?? 0,
                  totPages: paging?.size
                    ? Math.ceil(dataSource.length / paging?.size)
                    : 0,
                }
              : {
                  pageItems: paging.pageItems,
                  totItems: paging.totItems ?? 0,
                  totPages: paging.totPages ?? 0,
                }
          }
        />
      )}
    </div>
  )
}

export default Table
