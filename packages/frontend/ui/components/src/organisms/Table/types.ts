import { Filter } from "./../../ui/icons.stories"
import { FontWeight } from "@punks/ui-core"
import { TableColumnFilter } from "../../molecules/TableFilter/types"

export type TablePaginationState = {
  pageIndex: number
  pageSize: number
}

export type TablePageParameters = {
  totPages: number
  totItems: number
  pageItems: number
}

export type PaginationMode = "clientSide" | "serverSide"

export type TableCellAlignment = "left" | "center" | "right"

export type TableCellBorder = "left" | "right"

export type TableColumnStyles = {
  align?: TableCellAlignment
  fontWeight?: FontWeight
  borders?: TableCellBorder[]
}

export type TableColumnSortingDirection = "asc" | "desc"

export type TableColumnClasses = {
  header?: string
  body?: string
}

export type TableColumnSorting<RecordType, SortingType> = {
  enabled?: boolean
  defaultDirection?: TableColumnSortingDirection
  sort?: (a: RecordType, b: RecordType) => number
  value?: SortingType
  icon?: React.ReactNode
}

export type TableColumnType<RecordType, FilterType, SortingType> = {
  key: string
  title: React.ReactNode
  render: (record: RecordType, rowNumber: number) => React.ReactNode
  colSpan?: number
  minWidth?: number | string
  hidden?: boolean
  styles?: TableColumnStyles
  filter?: TableColumnFilter<FilterType>
  sorting?: TableColumnSorting<RecordType, SortingType>
  classes?: TableColumnClasses
  widthFactor?: number
}

export type TableRowControlType<RecordType> = {
  key: string
  hidden?: boolean
  render: (record: RecordType) => React.ReactNode
  styles?: TableColumnStyles
}

export declare type TableRowControlsType<RecordType = unknown> =
  TableRowControlType<RecordType>[]

export declare type TableColumnsType<
  RecordType = unknown,
  FilterType = unknown,
  SortingType = unknown,
> = TableColumnType<RecordType, FilterType, SortingType>[]

export type TablePaging =
  | {
      mode: "clientSide"
      index?: number
      size: number
      totPages?: number
      totItems?: number
      onPagingChange?: (index: number, size: number) => void
    }
  | {
      mode: "serverSide"
      index?: number
      size: number
      totPages?: number
      totItems?: number
      pageItems: number
      onPagingChange?: (index: number, size: number) => void
    }

export type TableSortingValue<SortingType> = {
  column: SortingType
  direction: TableColumnSortingDirection
}

export type TableSorting<SortingType> = {
  value?: TableSortingValue<SortingType>
  onChange: (value?: TableSortingValue<SortingType>) => void
}

export type TableGridType = "minimal" | "horizontal" | "vertical" | "full"

export type TableOptions = {
  columnsMinWidth?: number
  noRowsSeparator?: boolean
  evenOddRows?: boolean
  defaultAlignment?: TableCellAlignment
}

export type TableSelectionMode = "single" | "multiple"

export type TableSelection<RecordType> = {
  selected?: RecordType[]
  onSelectionChange: (selected: RecordType[]) => void
  mode: TableSelectionMode
}

export type TableSortingState<RecordType, SortingType> = {
  column: TableColumnType<RecordType, unknown, SortingType>
  direction: TableColumnSortingDirection
}
