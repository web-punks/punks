import { Button, Typography } from "../../../../atoms"
import { classNames, range } from "@punks/ui-core"
import styles from "./TablePagination.module.css"
import {
  ChevronDoubleLeftIcon,
  ChevronDoubleRightIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from "../../../../ui/icons"
import { TablePageParameters, TablePaginationState } from "../../types"

type PaginationButtonsProps = {
  onClick: (index: number) => void
  pageCount: number
  currentPage: number
}

const PaginationButtons = ({
  onClick,
  pageCount,
  currentPage,
}: PaginationButtonsProps) => {
  if (pageCount === 1) {
    return null
  }
  return (
    <div className={styles.paginationButtons}>
      {range(pageCount)
        .slice(0, 5)
        .map((_p, i) => {
          return (
            <PaginationButton
              key={i}
              onClick={onClick}
              currentPage={currentPage}
              index={i}
              pageCount={pageCount}
            />
          )
        })}
    </div>
  )
}

type ButtonProps = {
  onClick: (pageIndex: number) => void
  index: number
  currentPage: number
  pageCount: number
}

const PaginationButton = ({
  onClick,
  index,
  currentPage,
  pageCount,
}: ButtonProps) => {
  if (pageCount <= 5) {
    return (
      <Button
        onClick={() => onClick(index)}
        size="sm"
        color={index === currentPage ? "primary" : "light"}
      >
        {index + 1}
      </Button>
    )
  }
  const CurrentIconMap: Record<number, JSX.Element | null> = {
    0: <ChevronDoubleLeftIcon width={14} height={14} />,
    1: <ChevronLeftIcon width={14} height={10} />,
    2: <div>{currentPage + 1}</div>,
    3: <ChevronRightIcon width={14} height={10} />,
    4: <ChevronDoubleRightIcon width={14} height={14} />,
  }
  const indexNavigationMap: Record<number, number> = {
    0: 0,
    1: Math.max(0, currentPage - 1),
    2: currentPage,
    3: Math.min(currentPage + 1, pageCount - 1),
    4: pageCount - 1,
  }
  const CurrentIcon = CurrentIconMap[index]
  if (CurrentIcon == null) {
    return null
  }

  const nextIndex = indexNavigationMap[index]
  return (
    <Button
      size="sm"
      variant="text"
      className={classNames(
        index === 2 && styles.buttonDisabled,
        styles.button
      )}
      disabled={nextIndex === currentPage}
      onClick={() => {
        if (nextIndex !== currentPage) {
          onClick(nextIndex)
        }
      }}
    >
      <div className={styles.buttonInner}>{CurrentIcon}</div>
    </Button>
  )
}

export type TablePaginationProps = {
  pagination?: TablePaginationState
  parameters?: TablePageParameters
  onPageChange: (pageIndex: number, pageSize: number) => void
}

export const TablePagination = ({
  pagination,
  parameters,
  onPageChange,
}: TablePaginationProps) => {
  const pageIndex = pagination?.pageIndex ?? 0
  const pageSize = pagination?.pageSize ?? 10

  const pageItemsFrom = pageIndex * pageSize + 1
  const pageItemsTo = pageItemsFrom + (parameters?.pageItems ?? 0) - 1

  return (
    <div className={styles.paginationRoot}>
      <div>
        <Typography>{`${pageItemsFrom}-${pageItemsTo} / ${
          parameters?.totItems ?? 0
        }`}</Typography>
      </div>
      <PaginationButtons
        onClick={(page) => onPageChange(page, pageSize)}
        pageCount={parameters?.totPages ?? 0}
        currentPage={pageIndex}
      />
    </div>
  )
}
