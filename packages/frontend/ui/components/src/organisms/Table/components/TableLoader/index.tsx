import { Spinner } from "../../../../animations"

const TableLoader = () => {
  return <Spinner size="lg" />
}

export default TableLoader
