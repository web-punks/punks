import { Alert } from "../../../../atoms"

export interface TableEmptyContentComponentProps {
  className?: string
  children: any
}

export interface TableEmptyContentProps {
  content: React.ReactNode
  component?: React.ComponentType<TableEmptyContentComponentProps>
  className?: string
}

const TableEmptyContent = ({
  content,
  component,
  className,
}: TableEmptyContentProps) => {
  const Component = component
  return (
    <>
      {Component ? (
        <Component className={className}>{content}</Component>
      ) : (
        <Alert className={className} type="info">
          {content}
        </Alert>
      )}
    </>
  )
}

export default TableEmptyContent
