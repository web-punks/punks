import { TableSelectionMode } from "../types"

type TableSelectionParams<RecordType> = {
  selectedItems: RecordType[]
  onSelectionChange: (selectedItems: RecordType[]) => void
  mode: TableSelectionMode
}

type TableRowClickAction<RecordType> = {
  onSingleClick?: (item: RecordType) => void
  onDoubleClick?: (item: RecordType) => void
}

export const useTableRowActions = <RecordType>({
  selection,
  rowActions,
}: {
  selection?: TableSelectionParams<RecordType>
  rowActions?: TableRowClickAction<RecordType>
}) => {
  const isSelected = (item: RecordType) => {
    if (!selection) {
      return false
    }

    return selection.selectedItems.includes(item)
  }

  const handleRowSelection = (item: RecordType) => {
    if (!selection) {
      return
    }

    if (isSelected(item)) {
      selection.onSelectionChange(
        selection.selectedItems.filter((i) => i !== item)
      )
      return
    }

    const newSelection =
      selection.mode === "single" ? [item] : [...selection.selectedItems, item]
    selection.onSelectionChange(newSelection)
  }

  const isRowSelectable = (item: RecordType) => {
    return !!selection
  }

  const isRowClickable = (item: RecordType) => {
    return (
      isRowSelectable(item) ||
      !!rowActions?.onSingleClick ||
      !!rowActions?.onDoubleClick
    )
  }

  const handleRowClick = (item: RecordType) => {
    if (!isRowClickable(item)) {
      return
    }

    rowActions?.onSingleClick?.(item)
    if (isRowSelectable(item)) {
      handleRowSelection(item)
    }
  }

  const handleRowDoubleClick = (item: RecordType) => {
    rowActions?.onDoubleClick?.(item)
  }

  return {
    isSelected,
    isRowClickable,
    handleRowClick,
    handleRowDoubleClick,
  }
}
