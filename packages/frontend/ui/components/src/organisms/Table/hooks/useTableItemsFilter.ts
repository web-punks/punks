import { PaginationMode, TablePaginationState } from "../types"

export interface TableServerSideSize {
  totItems: number
  totPages: number
}

export interface TableItemsFiltering<T> {
  items: T[]
  paginationMode?: PaginationMode
  pagination?: TablePaginationState
  serverSizeSize?: TableServerSideSize
}

export const useTableItemsFilter = <T>({
  items,
  paginationMode,
  pagination,
  serverSizeSize,
}: TableItemsFiltering<T>) => {
  if (!paginationMode || paginationMode === "serverSide") {
    return {
      items,
      totItems: serverSizeSize?.totItems,
      totPages: serverSizeSize?.totPages,
    }
  }

  const { pageIndex, pageSize } = pagination || {}
  const pageItems = items.slice(
    (pageIndex ?? 0) * (pageSize ?? 0),
    ((pageIndex ?? 0) + 1) * (pageSize ?? 0)
  )

  return {
    items: pageItems,
    totItems: items.length,
    totPages: Math.ceil(items.length / (pageSize ?? 0)),
  }
}
