import { useMemo } from "react"
import { PaginationMode, TableSortingState } from "../types"

export const useTableSorter = <RecordType, SortingType>(input: {
  data: RecordType[]
  sorting?: TableSortingState<RecordType, SortingType>
  paginationMode?: PaginationMode
}) => {
  const sortedData = useMemo(() => {
    if (input.paginationMode !== "clientSide") {
      return input.data
    }
    if (!input.sorting?.column.sorting?.sort) {
      return input.data
    }
    const sortFn = input.sorting?.column.sorting.sort
    return input.sorting?.direction === "asc"
      ? input.data.sort(sortFn)
      : input.data.sort((a, b) => sortFn(b, a))
  }, [
    input.data,
    input.sorting,
    input.sorting?.column.sorting?.sort,
    input.paginationMode,
  ])

  return {
    sortedData,
  }
}
