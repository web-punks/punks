import React, { useEffect } from "react"
import type { Meta, StoryObj } from "@storybook/react"

import { Table } from "."
import { range } from "@punks/ui-core"
import { TableColumnType, TableColumnsType } from "./types"
import { Button } from "../../atoms"
import { ContextMenuIcon, EditIcon } from "../../ui"
import { sortBy, uniq } from "lodash"
import { ActionButton } from "../../molecules/ActionButton"
import { PopoverMenu } from "../../atoms/PopoverMenu"
import {
  TableColumnFilterType,
  TableTextFilterType,
} from "../../molecules/TableFilter/types"
import { DropButtonMenu } from "../../molecules"

type TableRow = {
  id: string
  title: string
  description: string
  age: number
  createdOn: string
}

type TableSorting = any

const items: TableRow[] = range(200).map((x) => ({
  id: x.toString(),
  title: `title ${x}`,
  description:
    "Lorem ipsium Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
  age: x % 10,
  createdOn: new Date(2025, 0, x % 30).toISOString(),
}))

type TableSortingValue = "title" | "age"

const columns: TableColumnType<TableRow, unknown, TableSortingValue>[] = [
  {
    key: "title",
    title: "Title",
    render: (x) => x.title,
    widthFactor: undefined,
    filter: {
      enabled: true,
      type: TableColumnFilterType.Text,
      onSubmit: (value) => console.log("text term submitted", value),
      onClear: () => console.log("filter clear"),
    },
    sorting: {
      enabled: true,
      value: "title",
    },
  },
  {
    key: "description",
    title: "Description",
    render: (x) => x.description,
    styles: {
      align: "center",
    },
    widthFactor: 4,
    filter: {
      enabled: true,
      type: TableColumnFilterType.SingleSelect,
      options: [
        { value: "option1", label: "Option 1" },
        { value: "option2", label: "Option 2" },
        { value: "option3", label: "Option 3" },
        { value: "option4", label: "Option 4" },
        { value: "option5", label: "Option 5" },
        { value: "option6", label: "Option 6" },
      ],
      selectedValue: "option1",
      onChange: (value) => console.log("single value changed", value),
    },
  },
  {
    key: "description2",
    title: "Description2",
    render: (x) => x.description,
    widthFactor: 8,
    sorting: { enabled: true },
    filter: {
      enabled: true,
      type: TableColumnFilterType.MultipleSelect,
      options: [
        { value: "option1", label: "Option 1" },
        { value: "option2", label: "Option 2" },
        { value: "option3", label: "Option 3" },
        { value: "option4", label: "Option 4" },
        { value: "option5", label: "Option 5" },
        { value: "option6", label: "Option 6" },
      ],
      selectedValues: ["option1"],
      onChange: (value) => console.log("multiple value changed", value),
    },
  },
  {
    key: "id",
    title: "Id",
    render: (x) => x.id,
    widthFactor: undefined,
  },
  {
    key: "actions",
    title: "",
    render: (x) => <div></div>,
    widthFactor: undefined,
  },
]

const meta = {
  title: "Ui/Organisms/Table",
  component: Table,
  tags: ["autodocs"],
  argTypes: {},
  args: {
    dataSource: items,
    columns,
    labels: {
      filters: {
        textFilter: {
          apply: "Apply",
          search: "Search",
          contains: "Contains",
          isEmpty: "Is empty",
          isNotEmpty: "Is not empty",
        },
        dateFilter: {
          apply: "Apply",
        },
      },
    },
  },
} satisfies Meta<typeof Table<TableRow, TableSorting>>
export default meta

type Story = StoryObj<typeof meta>

export const TableDefault: Story = {}

export const TableWithAllTextFilters: Story = {
  args: {
    columns: [
      {
        key: "title",
        title: "Title",
        render: (x: any) => x.title,
        widthFactor: undefined,
        filter: {
          enabled: true,
          type: TableColumnFilterType.Text,
          onSubmit: (value) => console.log("text term submitted", value),
          onClear: () => console.log("filter clear"),
          types: [
            "contains" as TableTextFilterType,
            "isEmpty" as TableTextFilterType,
            "isNotEmpty" as TableTextFilterType,
          ],
        },
        sorting: {
          enabled: true,
          value: "title",
        },
      },
      columns[1] as any,
      columns[2] as any,
    ],
  },
}

export const TableWithManyColumns: Story = {
  args: {
    columns: [
      ...columns,
      ...columns,
      ...columns,
      ...columns,
      ...columns,
      ...columns,
      {
        key: "actions",
        render(record, rowNumber) {
          return (
            <DropButtonMenu
              items={[
                {
                  label: "Item1",
                  onClick: () => console.log("item1 clicked"),
                },
                {
                  label: "Item2",
                  onClick: () => console.log("item2 clicked"),
                },
              ]}
            >
              x
            </DropButtonMenu>
          )
        },
        title: "",
      } satisfies TableColumnsType<TableRow>[0],
    ] as any,
    style: {
      width: "800px",
      maxHeight: "400px",
    },
    dataSource: items.slice(0, 50),
  },
  decorators: [
    (Story) => (
      <div
        style={{
          margin: "20px auto",
        }}
      >
        <Story />
      </div>
    ),
  ],
}

export const TableAlignLeft: Story = {
  args: {
    options: {
      defaultAlignment: "left",
    },
  },
}

export const TableAlignCenter: Story = {
  args: {
    options: {
      defaultAlignment: "center",
    },
  },
}

export const TableAlignRight: Story = {
  args: {
    options: {
      defaultAlignment: "right",
    },
  },
}

export const TableLoading: Story = {
  args: {
    loading: true,
  },
}

export const TableEmpty: Story = {
  args: {
    dataSource: [],
    emptyData: {
      content: "No data",
    },
  },
}

export const TableWithActionCols: Story = {
  args: {
    dataSource: items.slice(0, 5),
    columns: [
      ...columns,
      {
        key: "actions",
        title: "",
        styles: {
          borders: ["left"],
        },
        render: () => (
          <div
            style={{
              display: "flex",
              width: "100px",
              justifyContent: "flex-end",
            }}
          >
            <Button iconOnly rounded>
              <EditIcon width={18} />
            </Button>
            <PopoverMenu
              control={
                <ActionButton>
                  <ContextMenuIcon width={18} />
                </ActionButton>
              }
              items={[
                {
                  content: "Item1",
                },
                {
                  content: "Item2",
                },
                {
                  content: "Item3",
                },
                {
                  content: "Item4",
                },
              ]}
            />
          </div>
        ),
        widthFactor: undefined,
      },
    ] as any,
  },
}

export const TableWithStickyActionCols: Story = {
  args: {
    dataSource: items.slice(0, 5),
    columns: [...columns, ...columns] as any,
    rowControls: [
      {
        key: "actions",
        styles: {
          borders: ["left"],
        },
        render: () => (
          <div
            style={{
              display: "flex",
              width: "100px",
              justifyContent: "flex-end",
            }}
          >
            <Button iconOnly rounded>
              <EditIcon width={18} />
            </Button>
            <PopoverMenu
              control={
                <ActionButton>
                  <ContextMenuIcon width={18} />
                </ActionButton>
              }
              items={[
                {
                  content: "Item1",
                },
                {
                  content: "Item2",
                },
                {
                  content: "Item3",
                },
                {
                  content: "Item4",
                },
              ]}
            />
          </div>
        ),
      },
    ],
  },
}

export const TableGridMinimal: Story = {
  args: {
    grid: "minimal",
  },
}

export const TableGridHorizontal: Story = {
  args: {
    grid: "horizontal",
  },
}

export const TableGridVertical: Story = {
  args: {
    grid: "vertical",
  },
}

export const TableGridFull: Story = {
  args: {
    grid: "full",
  },
}

export const TableEvenOdd: Story = {
  args: {
    options: {
      evenOddRows: true,
    },
  },
}

export const TableEvenOddGrid: Story = {
  args: {
    grid: "full",
    options: {
      evenOddRows: true,
    },
  },
}

export const TableRowClickable: Story = {
  args: {
    rowActions: {
      onSingleClick: (x) => console.log("item clicked", x),
      onDoubleClick: (x) => console.log("item double clicked", x),
    },
  },
}

export const TableRowSelectableSingle: Story = {
  args: {
    selection: {
      mode: "single",
      onSelectionChange: (selection) =>
        console.log("selection change", selection),
    },
  },
}

export const TableRowSelectableMulti: Story = {
  args: {
    selection: {
      mode: "multiple",
      onSelectionChange: (selection) =>
        console.log("selection change", selection),
    },
  },
}

export const TablePagedClientSide: Story = {
  render: (args) => {
    const [pageIndex, setPageIndex] = React.useState(0)
    return (
      <Table
        {...args}
        paging={{
          index: pageIndex,
          size: 10,
          mode: "clientSide",
          onPagingChange(index) {
            setPageIndex(index)
          },
        }}
      />
    )
  },
}

export const TablePagedClientSideUncontrolled: Story = {
  render: (args) => {
    return (
      <Table
        {...args}
        paging={{
          size: 10,
          mode: "clientSide",
        }}
      />
    )
  },
}

export const TablePagedServerSide: Story = {
  render: (args) => {
    const pageSize = 10
    const [pageIndex, setPageIndex] = React.useState(0)
    const [pageItems, setPageItems] = React.useState(
      args.dataSource?.slice(0, pageSize)
    )

    useEffect(() => {
      setPageItems(
        args.dataSource?.slice(
          pageIndex * pageSize,
          pageIndex * pageSize + pageSize
        )
      )
    }, [pageIndex])

    return (
      <Table
        {...args}
        dataSource={pageItems}
        paging={{
          index: pageIndex,
          size: pageSize,
          mode: "serverSide",
          pageItems: pageItems?.length ?? 0,
          totPages: Math.ceil((args.dataSource?.length ?? 0) / pageSize),
          totItems: args.dataSource?.length ?? 0,
          onPagingChange(index) {
            setPageIndex(index)
          },
        }}
      />
    )
  },
}

export const TableWithFilters: Story = {
  render: (args) => {
    // const pageSize = 10
    // const [pageIndex, setPageIndex] = React.useState(0)
    // const [pageItems, setPageItems] = React.useState(
    //   args.dataSource?.slice(0, pageSize)
    // )

    // useEffect(() => {
    //   setPageItems(
    //     args.dataSource?.slice(
    //       pageIndex * pageSize,
    //       pageIndex * pageSize + pageSize
    //     )
    //   )
    // }, [pageIndex])

    const data = args.dataSource as TableRow[]

    const [filteredItems, setFilteredItems] = React.useState<TableRow[]>(data)
    const [filters, setFilters] = React.useState<Record<string, any>>({})

    return (
      <Table
        {...args}
        dataSource={filteredItems}
        sorting={{
          value: {
            column: "title",
            direction: "asc",
          },
          onChange: (value) => console.log("sorting change", value),
        }}
        columns={[
          ...(args.columns as any),
          {
            title: "Age",
            key: "age",
            render(record: TableRow, rowNumber) {
              return record.age.toString()
            },
            filter: {
              type: TableColumnFilterType.MultipleSelect,
              options: sortBy(uniq(data.map((x) => x.age)), (x) => x).map(
                (x) => ({
                  label: x,
                  value: x,
                })
              ),
              onChange(values) {
                setFilters((x) => ({ ...x, age: values }))
              },
              selectedValues: filters.age,
            },
            sorting: {
              enabled: true,
              value: "age",
            },
          },
          {
            title: "Created On",
            key: "createdOn",
            render(record: TableRow, rowNumber) {
              return record.createdOn.toString()
            },
            filter: {
              type: TableColumnFilterType.Date,
              onSubmit(values) {
                setFilters((x) => ({ ...x, createdOn: values }))
              },
              onClear() {
                setFilters((x) => ({ ...x, createdOn: undefined }))
              },
              selectedValues: filters.createdOn,
            },
            sorting: {
              enabled: true,
              value: "createdOn",
            },
          },
        ]}
      />
    )
  },
}

export const TableWithAFewRows: Story = {
  args: {
    dataSource: items.slice(0, 15),
  },
}
