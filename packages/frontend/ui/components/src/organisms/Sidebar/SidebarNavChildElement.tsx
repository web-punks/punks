import { classNames } from "@punks/ui-core"
import { Box } from "../../structure"
import styles from "./SidebarNavChildElement.module.css"
import { sidebarOpenAtom } from "./atoms"
import { useSetAtom } from "jotai"

export interface SidebarNavChildElementProps {
  label: string
  onClick?: () => void
  selected?: boolean
  clickable?: boolean
  divider?: boolean
}

export const SidebarNavChildElement = ({
  label,
  clickable,
  onClick,
  selected,
  divider,
}: SidebarNavChildElementProps) => {
  const toggleSidebar = useSetAtom(sidebarOpenAtom)
  return (
    <Box
      onClick={() => {
        toggleSidebar((prev) => !prev)
        onClick?.()
      }}
      cursorPointer={clickable}
      className={classNames(styles.label, {
        [styles.selected]: selected,
        [styles.divider]: divider,
      })}
    >
      {label}
    </Box>
  )
}

export default SidebarNavChildElement
