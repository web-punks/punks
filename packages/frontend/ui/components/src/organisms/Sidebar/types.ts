export type SidebarColor = "dark" | "light"

export type SidebarNavItemChild = {
  label: string
  onClick?: () => void
  render?: (item: React.ReactNode) => React.ReactNode
  selected?: boolean
  clickable?: boolean
}

export interface SidebarNavItem {
  icon: React.ReactNode
  label: string
  onClick?: () => void
  render?: (item: React.ReactNode, onClick?: () => void) => React.ReactNode
  selected?: boolean
  clickable?: boolean
  children?: SidebarNavItemChild[]
}
