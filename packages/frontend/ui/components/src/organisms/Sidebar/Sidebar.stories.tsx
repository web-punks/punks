import type { Meta, StoryObj } from "@storybook/react"

import { Sidebar } from "."

const Icon1 = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="22"
    viewBox="0 0 20 22"
    fill="none"
  >
    <path
      d="M19.5 11.0938C19.5 16.5277 15.2323 20.9062 10 20.9062C4.76769 20.9062 0.5 16.5277 0.5 11.0938C0.5 5.65984 4.76769 1.28125 10 1.28125C15.2323 1.28125 19.5 5.65984 19.5 11.0938Z"
      stroke="#608BC3"
    />
  </svg>
)

const meta = {
  title: "Ui/Organisms/Sidebar",
  component: Sidebar,
  tags: ["autodocs"],
  args: {
    navItems: [
      {
        icon: <Icon1 />,
        label: "Item 1",
        selected: true,
        children: [
          {
            label: "Subitem 1",
            selected: true,
          },
          {
            label: "Subitem 2",
          },
          {
            label: "Subitem 3",
          },
          {
            label: "Subitem 4",
          },
        ],
      },
      {
        icon: <Icon1 />,
        label: "Item 2",
        children: [
          {
            label: "Subitem 1",
            selected: true,
          },
          {
            label: "Subitem 2",
          },
          {
            label: "Subitem 3",
          },
          {
            label: "Subitem 4",
          },
        ],
      },
      {
        icon: <Icon1 />,
        label: "Item 3",
        children: [
          {
            label: "Subitem 1",
            selected: true,
          },
          {
            label: "Subitem 2",
          },
          {
            label: "Subitem 3",
          },
          {
            label: "Subitem 4",
          },
        ],
      },
      {
        icon: <Icon1 />,
        label: "Item 4",
      },
      {
        icon: <Icon1 />,
        label: "Item 5",
      },
      {
        icon: <Icon1 />,
        label: "Item 6",
      },
    ],
  },
} satisfies Meta<typeof Sidebar>
export default meta

type Story = StoryObj<typeof meta>

export const SidebarLight: Story = {
  args: {
    logo: (
      <img
        src="https://a.storyblok.com/f/196735/601x601/29667fe0ad/fav10.png"
        style={{
          maxWidth: 60,
        }}
      />
    ),
    color: "light",
    hPage: true,
  },
}

export const SidebarLightNoLogo: Story = {
  args: {
    color: "light",
    hPage: true,
  },
}

export const SidebarDark: Story = {
  args: {
    logo: (
      <img
        src="https://a.storyblok.com/f/196735/601x601/29667fe0ad/fav10.png"
        style={{
          maxWidth: "100%",
        }}
      />
    ),
    color: "dark",
    hPage: true,
  },
}

export const SidebarDarkNoLogo: Story = {
  args: {
    color: "dark",
    hPage: true,
  },
}
