import React, { Fragment } from "react"
import styles from "./SidebarNavElement.module.css"
import { Box } from "../../structure"
import { classNames, isLast } from "@punks/ui-core"
import { SidebarColor, SidebarNavItemChild } from "./types"
import SidebarNavChildElement from "./SidebarNavChildElement"
import { useSetAtom } from "jotai"
import { sidebarOpenAtom } from "./atoms"

export interface SidebarNavItemProps {
  icon: React.ReactNode
  label: string
  onClick?: () => void
  className?: string
  selected?: boolean
  clickable?: boolean
  color: SidebarColor
  subitems?: SidebarNavItemChild[]
  selectionMode?: "hover" | "click"
}

const SidebarNavElement = ({
  icon,
  label,
  selected,
  onClick,
  className,
  clickable,
  color,
  subitems,
  selectionMode = "click",
}: SidebarNavItemProps) => {
  const [isSectionSelected, setIsSectionSelected] = React.useState(false)
  const expanded = selected || isSectionSelected
  const toggleSidebar = useSetAtom(sidebarOpenAtom)
  return (
    <div
      onMouseEnter={() =>
        selectionMode === "hover" && setIsSectionSelected(true)
      }
      onMouseLeave={() =>
        selectionMode === "hover" && setIsSectionSelected(false)
      }
    >
      <Box
        className={classNames(
          styles.root,
          styles[color],
          {
            [styles.selected]: selected,
            [styles.clickable]: clickable || selectionMode === "click",
          },
          className
        )}
        onClick={() => {
          if (selectionMode === "click") {
            setIsSectionSelected(!isSectionSelected)
          }
          toggleSidebar((prev) => !prev)
          onClick?.()
        }}
        py={5}
      >
        <div>{icon}</div>
        <Box
          mt={2}
          className={classNames(styles.label, {
            [styles.selected]: selected,
          })}
        >
          {label}
        </Box>
      </Box>
      {expanded && subitems && subitems.length > 0 && (
        <div className={styles.subitems}>
          {subitems.map((subitem, index) => (
            <Fragment key={index}>
              {subitem.render ? (
                subitem.render(
                  <SidebarNavChildElement
                    label={subitem.label}
                    clickable={subitem.clickable}
                    onClick={subitem.onClick}
                    selected={subitem.selected}
                    divider={!isLast(index, subitems)}
                  />
                )
              ) : (
                <SidebarNavChildElement
                  label={subitem.label}
                  clickable={subitem.clickable}
                  onClick={subitem.onClick}
                  selected={subitem.selected}
                  divider={!isLast(index, subitems)}
                />
              )}
            </Fragment>
          ))}
        </div>
      )}
    </div>
  )
}

export default SidebarNavElement
