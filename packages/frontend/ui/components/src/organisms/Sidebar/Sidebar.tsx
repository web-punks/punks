import React, { Fragment } from "react"
import styles from "./Sidebar.module.css"
import SidebarNavElement from "./SidebarNavElement"
import { classNames, ClassValue } from "@punks/ui-core"
import { SidebarColor, SidebarNavItem } from "./types"
import { sidebarOpenAtom } from "./atoms"
import { useAtom } from "jotai"

export type SidebarClasses = {
  root?: string
  logo?: string
  navItems?: string
  navItem?: string
}

export interface SidebarProps {
  logo?: React.ReactNode
  navItems?: SidebarNavItem[]
  hPage?: boolean
  color?: SidebarColor
  classes?: SidebarClasses
  className?: ClassValue
  selectionMode?: "hover" | "click"
}

const Sidebar = ({
  logo,
  navItems,
  hPage,
  className,
  classes,
  color = "light",
  selectionMode = "click",
}: SidebarProps) => {
  const [isOpen, toggleSidebar] = useAtom(sidebarOpenAtom)

  return (
    <div
      className={classNames(
        styles.root,
        styles[color],
        {
          [styles.hPage]: hPage,
        },
        classes?.root,
        className
      )}
      data-open={isOpen ? "" : undefined}
    >
      {logo && (
        <div className={classNames(styles.logo, classes?.logo)}>{logo}</div>
      )}
      <div className={classNames(styles.navItems, classes?.navItems)}>
        {navItems?.map((item, index) =>
          item.render ? (
            <Fragment key={index}>
              {item.render(
                <SidebarNavElement
                  color={color}
                  icon={item.icon}
                  label={item.label}
                  selected={item.selected}
                  clickable={item.clickable}
                  onClick={item.onClick}
                  subitems={item.children}
                  selectionMode={selectionMode}
                  className={classNames(styles.navItem, classes?.navItem)}
                />,
                () => toggleSidebar((prev) => !prev)
              )}
            </Fragment>
          ) : (
            <SidebarNavElement
              color={color}
              key={index}
              icon={item.icon}
              label={item.label}
              selected={item.selected}
              clickable={item.clickable}
              onClick={item.onClick}
              subitems={item.children}
              selectionMode={selectionMode}
              className={classNames(styles.navItem, classes?.navItem)}
            />
          )
        )}
      </div>
      <div></div>
    </div>
  )
}

export default Sidebar
