import { classNames, ClassValue } from "@punks/ui-core"
import styles from "./SidebarTrigger.module.css"
import { SidebarColor } from "./types"
import { CloseIcon, MenuIcon } from "../../ui"
import { useAtom, useSetAtom } from "jotai"
import { sidebarOpenAtom } from "./atoms"
import { Button } from "../../atoms"

export default function SidebarTrigger({
  className,
  children,
  color = "light",
}: {
  color?: SidebarColor
  className?: ClassValue
  children?: React.ReactNode
}) {
  const [isOpen, toggleSidebar] = useAtom(sidebarOpenAtom)
  return (
    <Button
      id="sidebar-trigger"
      variant="filled"
      iconOnly={!children}
      onClick={() => toggleSidebar((prev) => !prev)}
      className={classNames(styles.root, styles[color], className)}
    >
      {children ? (
        children
      ) : (
        <>
          {isOpen ? (
            <CloseIcon className={styles.icon} />
          ) : (
            <MenuIcon className={styles.icon} />
          )}
          <span className={classNames(styles.srOnly)}>Open sidebar</span>
        </>
      )}
    </Button>
  )
}
