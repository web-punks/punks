import { Provider } from "jotai"
import Sidebar, { SidebarProps } from "./Sidebar"

export { SidebarProps }
export default function SidebarWithProvider({ ...props }: SidebarProps) {
  return (
    <Provider>
      <Sidebar {...props} />
    </Provider>
  )
}
