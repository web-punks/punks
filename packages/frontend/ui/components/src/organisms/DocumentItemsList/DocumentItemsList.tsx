import { classNames, isLast } from "@punks/ui-core"
import styles from "./DocumentItemsList.module.css"
import { Fragment, ReactNode } from "react"
import { Typography } from "../../atoms"
import { Separator } from "../../structure"
import { DocumentListItem } from "../../molecules"

export interface DocumentListItemData {
  icon?: React.ReactNode
  title?: React.ReactNode
  subtitle?: React.ReactNode
  control?: React.ReactNode
  className?: string
}

export interface DocumentItemsListClasses {
  root?: string
  item?: string
}

export interface DocumentItemsListProps {
  items: DocumentListItemData[]
  className?: string
  classes?: DocumentItemsListClasses
  addSeparator?: boolean
  title?: ReactNode
}

const DocumentItemsList = ({
  items,
  className,
  classes,
  addSeparator,
  title,
}: DocumentItemsListProps) => {
  return (
    <div className={classNames(styles?.root, classes?.root, className)}>
      {title && (
        <Typography variant="h4" weight="bold" mb={4}>
          {title}
        </Typography>
      )}
      {items.map((item, index: number) => (
        <Fragment key={index}>
          <DocumentListItem
            icon={item.icon}
            title={item.title}
            subtitle={item.subtitle}
            control={item.control}
            className={classNames(styles?.item, classes?.item, item.className)}
          />
          {addSeparator && !isLast(index, items) && <Separator margin="xs" />}
        </Fragment>
      ))}
    </div>
  )
}

export default DocumentItemsList
