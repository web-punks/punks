import type { Meta, StoryObj } from "@storybook/react"
import DocumentItemsList from "./DocumentItemsList"
import { DocumentIcon } from "../../ui/icons"
import { Switch } from "../../atoms"

const items = [
  {
    icon: <DocumentIcon />,
    title: "Item 1",
    subtitle: "Subtitle 1",
  },
  {
    icon: <DocumentIcon />,
    title: "Item 2",
    subtitle: "Subtitle 2",
  },
  {
    icon: <DocumentIcon />,
    title: "Item 3",
    subtitle: "Subtitle 3",
  },
]

const meta = {
  title: "UI/Organisms/DocumentItemsList",
  component: DocumentItemsList,
  tags: ["autodocs"],
  args: {
    title: "Title",
    items,
  },
} satisfies Meta<typeof DocumentItemsList>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {}

export const WithSeparator: Story = {
  args: {
    addSeparator: true,
  },
}

export const WithToggleControl: Story = {
  args: {
    addSeparator: true,
    items: items.map((x) => ({
      ...x,
      control: <Switch size="medium" />,
    })),
  },
}
