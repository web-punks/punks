import CardsListEditor from "./CardsListEditor"
import type { Meta, StoryObj } from "@storybook/react"

interface ListItem {
  id: string
  text: string
}

const TypedListEditor = CardsListEditor<any>

const meta: Meta = {
  title: "UI/Organisms/CardListEditor",
  component: TypedListEditor,
  args: {
    id: "text-list-editor",
    items: [
      { id: "1", text: "Elemento 1" },
      { id: "2", text: "Elemento 2" },
      { id: "3", text: "Elemento 3" },
    ],
    renderCardContent: (item: ListItem) => <div key={item.id}>{item.text}</div>,
    onAddItem: () => console.log("onAddItem"),
    onItemMoved: (result) => console.log("onItemMoved", result),
  },
  decorators: [
    (Story) => (
      <div
        style={{
          width: 500,
          padding: 8,
          background: "var(--wp-theme-color-grey-10)",
        }}
      >
        <Story />
      </div>
    ),
  ],
} satisfies Meta<typeof TypedListEditor>
export default meta

type Story = StoryObj<typeof TypedListEditor>

export const Default: Story = {
  args: {},
}

export const HandleRight: Story = {
  args: {
    handlePosition: "right",
  },
}

export const HandleBottom: Story = {
  args: {
    handlePosition: "bottom",
  },
}

export const HandleLeft: Story = {
  args: {
    handlePosition: "left",
  },
}
