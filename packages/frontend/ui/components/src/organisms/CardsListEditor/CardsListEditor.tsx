import { classNames } from "@punks/ui-core"
import {
  DragDropContext,
  Droppable,
  Draggable,
  DropResult,
} from "@hello-pangea/dnd"
import { ActionButton, Card } from "../../molecules"
import { AddIcon, HorizontalHandleIcon, VerticalHandleIcon } from "../../ui"
import styles from "./CardsListEditor.module.css"
import { CardFocus } from "../../molecules/Card/Card"

export type CardsListEditorClasses = {
  root?: string
  card?: string
  cardContent?: string
  cardHandle?: string
  addControl?: string
}

export type CardListOptions = {
  focus?: CardFocus
}

export type CardsListItemMovedResult = {
  sourceIndex: number
  destinationIndex: number
}

export type CardsListEditorHandlePosition = "left" | "right" | "top" | "bottom"

export type CardListItemUiState = {
  focused?: boolean
}

export interface CardsListEditorProps<
  T extends {
    id: string
  },
> {
  id: string
  items: T[]
  onItemMoved?: (result: CardsListItemMovedResult) => void
  onAddItem?: () => void
  className?: string
  classes?: CardsListEditorClasses
  renderCardContent: (item: T, index: number) => React.ReactNode
  cardOptions?: CardListOptions
  addIcon?: React.ReactNode
  handlePosition?: CardsListEditorHandlePosition
  getUiState?: (item: T) => CardListItemUiState
}

const CardsListEditor = <
  T extends {
    id: string
  },
>({
  id,
  renderCardContent,
  className,
  classes,
  items,
  onAddItem,
  onItemMoved,
  cardOptions,
  addIcon,
  handlePosition = "top",
  getUiState,
}: CardsListEditorProps<T>) => {
  const handleDragEnd = (result: DropResult) => {
    onItemMoved?.({
      sourceIndex: result.source.index,
      destinationIndex: result.destination?.index ?? -1,
    })
  }

  return (
    <div>
      <DragDropContext onDragEnd={handleDragEnd}>
        <Droppable droppableId={id}>
          {(provided) => (
            <div
              className={classNames(styles.root, classes?.root, className)}
              ref={provided.innerRef}
              {...provided.droppableProps}
            >
              {items.map((item, index) => (
                <Draggable key={item.id} draggableId={item.id} index={index}>
                  {(provided) => {
                    return (
                      <div
                        ref={provided.innerRef}
                        className={styles.item}
                        {...provided.draggableProps}
                      >
                        <Card
                          className={classNames(
                            {
                              [styles.handleContainerHorizontal]:
                                handlePosition === "left" ||
                                handlePosition === "right",
                            },
                            classes?.card
                          )}
                          focus={cardOptions?.focus}
                          focused={getUiState?.(item)?.focused}
                          noGutters
                        >
                          {handlePosition === "top" && (
                            <div
                              className={classNames(
                                styles.handleTop,
                                classes?.cardHandle
                              )}
                              {...provided.dragHandleProps}
                            >
                              <HorizontalHandleIcon />
                            </div>
                          )}

                          {handlePosition === "left" && (
                            <div
                              className={classNames(
                                styles.handleLeft,
                                classes?.cardHandle
                              )}
                              {...provided.dragHandleProps}
                            >
                              <VerticalHandleIcon />
                            </div>
                          )}

                          <div
                            className={classNames(
                              styles.content,
                              {
                                [styles.contentHorizontal]:
                                  handlePosition === "left" ||
                                  handlePosition === "right",
                              },
                              classes?.cardContent
                            )}
                          >
                            {renderCardContent(item, index)}
                          </div>

                          {handlePosition === "bottom" && (
                            <div
                              className={classNames(
                                styles.handleBottom,
                                classes?.cardHandle
                              )}
                              {...provided.dragHandleProps}
                            >
                              <HorizontalHandleIcon />
                            </div>
                          )}

                          {handlePosition === "right" && (
                            <div
                              className={classNames(
                                styles.handleRight,
                                classes?.cardHandle
                              )}
                              {...provided.dragHandleProps}
                            >
                              <VerticalHandleIcon />
                            </div>
                          )}
                        </Card>
                      </div>
                    )
                  }}
                </Draggable>
              ))}
              {provided.placeholder}
              {onAddItem && (
                <div
                  className={classNames(
                    classes?.addControl,
                    styles.addActionRow
                  )}
                >
                  <ActionButton onClick={onAddItem}>
                    {addIcon ?? <AddIcon />}
                  </ActionButton>
                </div>
              )}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </div>
  )
}

export default CardsListEditor
