import { ReactJsonView } from "../../integrations"
import { Tabs } from "../../molecules"

export type JsonViewerLabels = {
  explorerTabTitle: string
  rawTabTitle: string
}

export interface JsonViewerProps {
  src: any
  fullWidth?: boolean
  labels: JsonViewerLabels
}

const JsonViewer = ({ src, fullWidth, labels }: JsonViewerProps) => {
  return (
    <Tabs
      fullWidth={fullWidth}
      tabs={[
        {
          head: labels.explorerTabTitle,
          content: <ReactJsonView src={src} />,
        },
        {
          head: labels.rawTabTitle,
          content: <pre>{JSON.stringify(src, null, 4)}</pre>,
        },
      ]}
    />
  )
}

export default JsonViewer
