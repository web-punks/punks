import type { Meta, StoryObj } from "@storybook/react"

import { JsonViewer } from "."

const meta = {
  title: "Ui/Organisms/JsonViewer",
  component: JsonViewer,
  tags: ["autodocs"],
  args: {
    src: {
      foo: "bar",
      baz: "qux",
      parent: {
        child: "value",
        array: [1, 2, 3],
      },
    },
    labels: {
      explorerTabTitle: "Explorer",
      rawTabTitle: "Raw",
    },
  },
} satisfies Meta<typeof JsonViewer>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {}
