import { Radio } from "../../atoms"
import OptionsListEditor from "./OptionsListEditor"
import type { Meta, StoryObj } from "@storybook/react"

interface ListItem {
  id: string
  text: string
}

const TypedListEditor = OptionsListEditor<any>

const meta: Meta = {
  title: "UI/Organisms/OptionsListEditor",
  component: TypedListEditor,
  args: {
    id: "text-list-editor",
    items: [
      { id: "1", text: "Elemento 1" },
      { id: "2", text: "Elemento 2" },
      { id: "3", text: "Elemento 3" },
    ],
    renderOptionContent: (item: ListItem) => (
      <div key={item.id}>{item.text}</div>
    ),
    onItemMoved(result) {
      console.log("onItemMoved", result)
    },
  },
} satisfies Meta<typeof TypedListEditor>
export default meta

type Story = StoryObj<typeof TypedListEditor>

export const Default: Story = {
  args: {},
  decorators: [
    (Story) => (
      <div
        style={{
          width: 500,
          padding: 8,
          background: "var(--wp-theme-color-grey-10)",
        }}
      >
        <Story />
      </div>
    ),
  ],
}

export const WithRadioButtons: Story = {
  args: {
    ...meta.args,
    renderOptionContent: (item: ListItem) => (
      <div
        key={item.id}
        style={{ display: "flex", alignItems: "center", gap: 8 }}
      >
        <Radio onChange={() => {}} name="radio-buttons"></Radio>
        {item.text}
      </div>
    ),
  },
}
