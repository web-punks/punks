import React from "react"
import {
  DragDropContext,
  Droppable,
  Draggable,
  DropResult,
} from "@hello-pangea/dnd"
import { classNames } from "@punks/ui-core"
import styles from "./OptionsListEditor.module.css"
import { VerticalHandleIcon } from "../../ui"

export type OptionListItemMovedResult = {
  sourceIndex: number
  destinationIndex: number
}

export type OptionsListEditorClasses = {
  root?: string
  item?: string
  handle?: string
  addControl?: string
}

export interface OptionsListEditorProps<T extends { id: string }> {
  id: string
  items: T[]
  className?: string
  classes?: OptionsListEditorClasses
  onItemMoved?: (result: OptionListItemMovedResult) => void
  renderOptionContent: (item: T, index: number) => React.ReactNode
  optionAddControl?: React.ReactNode
}

const OptionsListEditor = <T extends { id: string }>({
  id,
  items,
  className,
  classes,
  onItemMoved,
  renderOptionContent,
  optionAddControl,
}: OptionsListEditorProps<T>) => {
  const handleDragEnd = (result: DropResult) => {
    onItemMoved?.({
      sourceIndex: result.source.index,
      destinationIndex: result.destination?.index ?? -1,
    })
  }

  return (
    <div>
      <DragDropContext onDragEnd={handleDragEnd}>
        <Droppable droppableId={id}>
          {(provided) => (
            <div
              ref={provided.innerRef}
              {...provided.droppableProps}
              className={classNames(styles.root, classes?.root, className)}
            >
              {items.map((item, index) => (
                <Draggable key={item.id} draggableId={item.id} index={index}>
                  {(provided) => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      className={classNames(styles.item, classes?.item)}
                    >
                      <div
                        {...provided.dragHandleProps}
                        className={classNames(styles.handle, classes?.handle)}
                      >
                        <VerticalHandleIcon />
                      </div>
                      {renderOptionContent(item, index)}
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>

      {optionAddControl && (
        <div className={classNames(classes?.addControl, styles.addActionRow)}>
          {optionAddControl}
        </div>
      )}
    </div>
  )
}

export default OptionsListEditor
