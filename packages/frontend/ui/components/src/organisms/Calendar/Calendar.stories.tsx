import { Meta, StoryObj } from "@storybook/react"
import Calendar from "./Calendar"
import { Box } from "../../structure"
import { Button, DropDown } from "../../atoms"
import { SearchIcon, SettingsIcon } from "../../ui"

type DataPayload = {
  projectName: string
}

export default {
  title: "Ui/Organisms/Calendar",
  component: Calendar<DataPayload>,
  args: {
    events: [
      {
        id: "aedfas",
        title: "Event 2",
        description: "test",
        location: "torino",
        start: new Date("2023-11-17T10:00:00"),
        data: {
          projectName: "Project 1",
        },
      },
    ],
    onEventClick: (event) => {
      console.log("Event clicked ", event)
    },
  },
} as Meta<typeof Calendar<DataPayload>>

export const Default: StoryObj<typeof Calendar<DataPayload>> = {}

export const WithPreSelectedDate: StoryObj<typeof Calendar<DataPayload>> = {
  args: {
    initialMonth: new Date("2023-11-17T10:00:00"),
  },
}

export const WithCustomRendering: StoryObj<typeof Calendar<DataPayload>> = {
  args: {
    initialMonth: new Date("2023-11-17T10:00:00"),
    renderEvent: (event) => <div>{event.data?.projectName}</div>,
  },
}

export const WithCustomHeight: StoryObj<typeof Calendar<DataPayload>> = {
  args: {
    layoutOptions: {
      cellMinHeight: 180,
    },
  },
}

export const Italian: StoryObj<typeof Calendar<DataPayload>> = {
  args: {
    locale: "it",
  },
}

export const WithToolbar: StoryObj<typeof Calendar<DataPayload>> = {
  args: {
    headerToolbar: (
      <>
        <Box flex alignItems="center">
          <Box flex justifyContent="between" mr={4}>
            <DropDown
              control="Calendari"
              items={[
                {
                  content: "Item1",
                },
                {
                  content: "Item2",
                },
                {
                  content: "Item3",
                },
                {
                  content: "Item4",
                },
              ]}
            />
          </Box>
          <Box flex justifyContent="between" mr={4}>
            <DropDown
              control="Vista: Mese"
              items={[
                {
                  content: "Item1",
                },
                {
                  content: "Item2",
                },
                {
                  content: "Item3",
                },
                {
                  content: "Item4",
                },
              ]}
            />
          </Box>
          <Button>
            <SearchIcon />
          </Button>
          <Button>
            <SettingsIcon />
          </Button>
        </Box>
      </>
    ),
  },
}
