import React, { ReactNode, useMemo, useRef, useState } from "react"
import { DatesSetArg, EventClickArg } from "@fullcalendar/core"
import FullCalendar from "@fullcalendar/react"
import dayGridPlugin from "@fullcalendar/daygrid"
import timeGridPlugin from "@fullcalendar/timegrid"
import interactionPlugin from "@fullcalendar/interaction"
import { Button } from "../../atoms"
import { Box } from "../../structure"
import { SectionHead } from "../../molecules"
import { LeftArrow, RightArrow } from "../../ui/icons"
import styles from "./Calendar.module.css"
import { normalizePxSize, setCssVar, toTitleCase } from "@punks/ui-core"
import { toDict } from "../../utils/objects"

export type CalendarEvent<T> = {
  id: string
  title: string
  description?: string
  location?: string
  start: Date
  end?: Date
  allDay?: boolean
  data?: T
}

export type CalendarLabels = {
  today: string
}

export type CalendarLayoutOptions = {
  cellMinHeight?: number
}

export interface CalendarProps<T> {
  initialMonth?: Date
  initialView?: string
  headerToolbar?: ReactNode
  locale?: string
  labels: CalendarLabels
  events: CalendarEvent<T>[]
  layoutOptions?: CalendarLayoutOptions
  onEventClick?: (event: CalendarEvent<T>) => void
  onMonthChange?: (monthStart: Date) => void
  renderEvent?: (event: CalendarEvent<T>) => ReactNode
}

const formatMonthTitle = (date: Date, locale: string) => {
  return `${toTitleCase(
    new Intl.DateTimeFormat(locale, { month: "long" }).format(date)
  )} ${date.getFullYear()}`
}

const formatDayTitle = (date: Date, locale: string) => {
  return `${toTitleCase(
    new Intl.DateTimeFormat(locale, { weekday: "short" }).format(date)
  )}`
}

const Calendar = <T,>({
  initialView = "dayGridMonth",
  labels = {
    today: "Oggi",
  },
  locale = "default",
  events,
  headerToolbar,
  layoutOptions,
  initialMonth,
  onEventClick,
  onMonthChange,
  renderEvent,
}: CalendarProps<T>) => {
  const calendarRef = useRef<FullCalendar>(null)
  const [currentMonth, setCurrentMonth] = useState<Date>()

  const handleEventClick = (clickInfo: EventClickArg) => {
    const event = events.find((e) => e.id === clickInfo.event.id)
    if (!event) {
      throw new Error(`Event not found ${clickInfo.event.id}`)
    }
    onEventClick?.(event)
  }

  const handleNextMonth = () => {
    if (calendarRef.current) {
      calendarRef.current.getApi().next()
    }
  }

  const handlePrevMonth = () => {
    if (calendarRef.current) {
      calendarRef.current.getApi().prev()
    }
  }

  const handleCurrentMonth = () => {
    if (calendarRef.current) {
      calendarRef.current.getApi().today()
    }
  }

  const handleDatesSet = (info: DatesSetArg) => {
    onMonthChange?.(info.view.calendar.getDate())
    setCurrentMonth(info.view.calendar.getDate())
  }

  const eventsDict = useMemo(() => toDict(events ?? [], (e) => e.id), [events])
  const getEvent = (id: string) => eventsDict[id]

  return (
    <>
      <Box flex alignItems="center" justifyContent="between">
        <Box flex alignItems="center" gap={2}>
          <Button
            variant="outlined"
            color="primary"
            onClick={handleCurrentMonth}
          >
            {labels.today}
          </Button>
          <Button onClick={handlePrevMonth}>
            <LeftArrow />
          </Button>
          <Button onClick={handleNextMonth}>
            <RightArrow />
          </Button>
          {currentMonth && (
            <SectionHead title={formatMonthTitle(currentMonth, locale)} />
          )}
        </Box>
        <div>{headerToolbar}</div>
      </Box>

      <div
        className={styles.calendarContainer}
        style={{
          ...(layoutOptions?.cellMinHeight
            ? setCssVar(
                "--cell-min-h",
                normalizePxSize(layoutOptions.cellMinHeight)
              )
            : {}),
        }}
      >
        <FullCalendar
          plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
          initialView={initialView}
          initialDate={initialMonth}
          locale={locale}
          firstDay={1}
          events={events.map((event) => ({
            ...event,
            allDay: event.allDay ?? false,
            start: event.start?.toISOString(),
            end: event.end?.toISOString(),
          }))}
          eventContent={
            renderEvent
              ? (x) =>
                  getEvent(x.event.id)
                    ? renderEvent(getEvent(x.event.id))
                    : undefined
              : undefined
          }
          headerToolbar={{
            left: "",
            right: "",
          }}
          themeSystem="tailwind"
          height="auto"
          ref={calendarRef}
          buttonText={labels}
          dayHeaderContent={(arg) => {
            return <>{formatDayTitle(arg.date, locale)}</>
          }}
          datesSet={handleDatesSet}
          eventClick={handleEventClick}
        />
      </div>
    </>
  )
}

export default Calendar
