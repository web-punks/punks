import {
  cssVar,
  getColorVar,
  getColorVarContrast,
  getColorVarDark,
  getColorVarLight,
  getGradientColorVar,
} from "../../../theme"
import { ThemeColor, ThemeColorGradient } from "../../../types"

const extendBaseColor = (color: ThemeColor) => ({
  [color]: {
    DEFAULT: cssVar(getColorVar(color)),
    contrast: cssVar(getColorVarContrast(color)),
    dark: cssVar(getColorVarDark(color)),
    light: cssVar(getColorVarLight(color)),
  },
})

const gradients: ThemeColorGradient[] = [10, 20, 30, 40, 50, 60, 70, 80, 90]
const extendGradientColor = (color: string) => ({
  [color]: gradients.reduce((x, v) => {
    x[v] = cssVar(getGradientColorVar(color as ThemeColor, v))
    return x
  }, {} as any),
})

export const extendThemeColors = () => ({
  colors: {
    ...extendBaseColor("primary"),
    ...extendBaseColor("secondary"),
    ...extendBaseColor("info"),
    ...extendBaseColor("success"),
    ...extendBaseColor("warning"),
    ...extendBaseColor("error"),
    ...extendBaseColor("white"),
    ...extendBaseColor("black"),
    ...extendBaseColor("paper"),
    ...extendBaseColor("paperNegative"),
    ...extendGradientColor("grey"),
  },
})
