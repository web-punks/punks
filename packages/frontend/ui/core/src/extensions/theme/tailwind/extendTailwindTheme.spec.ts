import { writeFileSync } from "fs"
import { extendTailwindTheme } from "."

test("extendTailwindTheme", () => {
  const item = extendTailwindTheme()
  writeFileSync("extendTailwindTheme.json", JSON.stringify(item, null, 4))
  expect(item).toMatchSnapshot()
})
