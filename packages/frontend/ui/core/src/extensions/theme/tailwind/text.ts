import {
  cssVar,
  getTextFontVar,
  getTextLetterSpacingVar,
  getTextLineHeightVar,
  getTextSizeVar,
  getTextWeightVar,
} from "../../../theme"
import { TextVariant } from "../../../types"

const sizes = [
  "body1",
  "body2",
  "body3",
  "caption1",
  "caption2",
  "caption3",
  "h1",
  "h2",
  "h3",
  "h4",
  "h5",
  "h6",
  "label1",
  "label2",
  "label3",
] as TextVariant[]

const getFontSizes = () =>
  sizes.reduce((x, v) => {
    x[v] = cssVar(getTextSizeVar(v))
    return x
  }, {} as any)

const getLineHeights = () =>
  sizes.reduce((x, v) => {
    x[v] = cssVar(getTextLineHeightVar(v))
    return x
  }, {} as any)

const getLetterSpacings = () =>
  sizes.reduce((x, v) => {
    x[v] = cssVar(getTextLetterSpacingVar(v))
    return x
  }, {} as any)

const getWeights = () =>
  sizes.reduce((x, v) => {
    x[v] = cssVar(getTextFontVar(v))
    return x
  }, {} as any)

const getFonts = () =>
  sizes.reduce((x, v) => {
    x[v] = cssVar(getTextWeightVar(v))
    return x
  }, {} as any)

export const extendFonts = () => ({
  fontSize: getFontSizes(),
  fontWeight: getWeights(),
  lineHeight: getLineHeights(),
  letterSpacing: getLetterSpacings(),
  fontFamily: getFonts(),
})
