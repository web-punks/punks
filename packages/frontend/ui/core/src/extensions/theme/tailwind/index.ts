import { extendThemeColors } from "./colors"
import { extendFonts } from "./text"

export const extendTailwindTheme = () => ({
  ...extendThemeColors(),
  ...extendFonts(),
})
