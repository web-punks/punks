export type ResponsiveValues<T> = {
  xs?: T
  sm?: T
  md?: T
  lg?: T
  xl?: T
}

export type ResponsiveStringValue = ResponsiveValues<string>

export type ResponsiveBreakpoints = keyof ResponsiveValues<any>
