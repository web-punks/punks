import {
  ILogger,
  ILoggerProvider,
  LogLevel,
  LogSettings,
} from "../abstractions"
import { DefaultLogger } from "./concrete/defaultLogger"

class InternalLogger implements ILogger {
  constructor(
    private readonly settings: LogSettings,
    private readonly provider: ILoggerProvider
  ) {}

  debug(message: string, meta?: any): void {
    if (this.settings.enabled && this.settings.level === LogLevel.Debug) {
      this.provider.debug(message, meta)
    }
  }

  info(message: string, meta?: any): void {
    if (this.settings.enabled && this.settings.level <= LogLevel.Info) {
      this.provider.info(message, meta)
    }
  }

  warn(message: string, meta?: any): void {
    if (this.settings.enabled && this.settings.level <= LogLevel.Warn) {
      this.provider.warn(message, meta)
    }
  }

  error(message: string, meta?: any): void {
    if (this.settings.enabled && this.settings.level <= LogLevel.Error) {
      this.provider.error(message, meta)
    }
  }

  fatal(message: string, meta?: any): void {
    if (this.settings.enabled && this.settings.level <= LogLevel.Fatal) {
      this.provider.fatal(message, meta)
    }
  }

  exception(message: string, error: Error, meta?: any): void {
    if (this.settings.enabled && this.settings.level <= LogLevel.Error) {
      this.provider.exception(message, error, meta)
    }
  }
}

const getLogger = ({
  settings,
  loggerName,
}: {
  settings: LogSettings
  loggerName?: string
}): ILogger => {
  return new InternalLogger(settings, new DefaultLogger(loggerName))
}

export class Log {
  private static settings: LogSettings = {
    enabled: true,
    level: LogLevel.Debug,
  }

  public static readonly root = getLogger({
    settings: this.settings,
  })

  static setLevel(level: LogLevel) {
    this.settings.level = level
  }

  static enable() {
    this.settings.enabled = true
  }

  static disable() {
    this.settings.enabled = false
  }

  static getLogger(loggerName: string): ILogger {
    return getLogger({
      settings: this.settings,
      loggerName,
    })
  }
}
