import { ILoggerProvider } from "../../abstractions"

export class DefaultLogger implements ILoggerProvider {
  constructor(private readonly loggerName?: string) {}

  debug(message: string, meta?: any): void {
    if (meta) {
      console.log(
        `${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`,
        meta
      )
    } else {
      console.log(`${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`)
    }
  }

  info(message: string, meta?: any): void {
    if (meta) {
      console.info(
        `${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`,
        meta
      )
    } else {
      console.info(
        `${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`
      )
    }
  }

  warn(message: string, meta?: any): void {
    if (meta) {
      console.warn(
        `${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`,
        meta
      )
    } else {
      console.warn(
        `${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`
      )
    }
  }

  error(message: string, meta?: any): void {
    if (meta) {
      console.error(
        `${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`,
        meta
      )
    } else {
      console.error(
        `${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`
      )
    }
  }

  fatal(message: string, meta?: any): void {
    if (meta) {
      console.error(
        `${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`,
        meta
      )
    } else {
      console.error(
        `${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`
      )
    }
  }

  exception(message: string, error: Error, meta?: any): void {
    if (meta) {
      console.error(
        `${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`,
        error,
        meta
      )
    } else {
      console.error(
        `${this.loggerName ? `[${this.loggerName}] ` : ""}${message}`,
        error
      )
    }
  }
}
