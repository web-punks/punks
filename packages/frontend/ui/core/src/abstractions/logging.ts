export enum LogLevel {
  Debug = 0,
  Info = 1,
  Warn = 2,
  Error = 3,
  Fatal = 4,
}

export interface ILoggerProvider {
  debug(message: string, meta?: any): void
  info(message: string, meta?: any): void
  warn(message: string, meta?: any): void
  error(message: string, meta?: any): void
  fatal(message: string, meta?: any): void
  exception(message: string, error: Error, meta?: any): void
}

export interface ILogger {
  debug(message: string, meta?: any): void
  info(message: string, meta?: any): void
  warn(message: string, meta?: any): void
  error(message: string, meta?: any): void
  fatal(message: string, meta?: any): void
  exception(message: string, error: Error, meta?: any): void
}

export type LogSettings = {
  enabled: boolean
  level: LogLevel
}
