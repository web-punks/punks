import { trimEnd, trimStart } from "./strings"

export const makeAbsoluteUrl = (path: string) =>
  `${trimEnd(window.location.origin, "/")}/${trimStart(path, "/")}`

export function serializeQueryString(obj: { [key: string]: any }): string {
  let queryString = ""
  for (let key in obj) {
    if (obj.hasOwnProperty(key) && obj[key]) {
      if (queryString.length > 0) {
        queryString += "&"
      }
      queryString += key + "=" + encodeURIComponent(obj[key])
    }
  }
  return queryString
}

export const buildUrl = (path: string, query: { [key: string]: any }) => {
  const queryString = serializeQueryString(query)
  return queryString ? `${path}?${queryString}` : path
}

export function deserializeQueryString(queryString: string): {
  [key: string]: any
} {
  const obj: { [key: string]: any } = {}
  const pairs = queryString.substring(1).split("&")
  for (let i = 0; i < pairs.length; i++) {
    const pair = pairs[i].split("=")
    const key = decodeURIComponent(pair[0])
    const value = decodeURIComponent(pair[1] || "")
    if (obj[key]) {
      if (Array.isArray(obj[key])) {
        obj[key].push(value)
      } else {
        obj[key] = [obj[key], value]
      }
    } else {
      obj[key] = value
    }
  }
  return obj
}

export const getQueryParameter = (
  name: string,
  location: Location = window.location
) => {
  const urlParams = deserializeQueryString(location.search)
  return urlParams[name]
}

export const updateQueryParameters = (
  queryString: string,
  params: { [key: string]: any }
) => {
  const queryParams = deserializeQueryString(queryString)
  for (const [key, value] of Object.entries(params)) {
    queryParams[key] = value
  }
  return serializeQueryString(queryParams)
}
