import { ResponsiveBreakpoints, ResponsiveValues } from "../types/responsive"

export const getResponsiveValue = <T>(
  breakpoint: ResponsiveBreakpoints,
  value?: T | ResponsiveValues<T>
) => {
  if (!value) {
    return undefined
  }
  if (typeof value === "object") {
    return (value as ResponsiveValues<T>)[breakpoint]
  }
  return value
}
