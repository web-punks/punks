export const encodeUtf8 = (value: Uint8Array) =>
  Buffer.from(value).toString("utf-8")
export const decodeUtf8 = (value: string) => Buffer.from(value, "utf-8")

export const encodeBase64 = (value: Uint8Array) =>
  Buffer.from(value).toString("base64")
export const decodeBase64 = (value: string) => Buffer.from(value, "base64")
