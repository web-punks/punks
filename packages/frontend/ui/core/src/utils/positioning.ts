import { ResponsiveStringValue, ResponsiveValues } from "../types/responsive"
type PositionObject = {
  top: string
  right: string
  bottom: string
  left: string
}

export function parseObjectPosition(position: string): PositionObject {
  if (!position) {
    return {
      top: "",
      right: "",
      bottom: "",
      left: "",
    }
  }

  const defaults: PositionObject = {
    top: "auto",
    right: "auto",
    bottom: "auto",
    left: "auto",
  }
  if (position.trim() === "center" || position.trim() === "center center") {
    return defaults
  }

  const positionParts = position.split(" ")
  const result = { ...defaults }

  let cursor = 0
  while (cursor < positionParts.length) {
    const key = positionParts[cursor] // 'right', 'left', 'top', 'bottom', 'center'
    if (key === "center") {
      if (cursor === 0 && positionParts.length === 1) {
        result["top"] = defaults.top
        result["right"] = defaults.right
        result["bottom"] = defaults.bottom
        result["left"] = defaults.left
        return result
      }

      if (positionParts.includes("right") || positionParts.includes("left")) {
        result["top"] = defaults.top
        result["bottom"] = defaults.bottom
      } else {
        result["right"] = defaults.right
        result["left"] = defaults.left
      }

      cursor++
      continue
    }

    const value = positionParts[cursor + 1]
    if (key && value) {
      switch (key) {
        case "top":
        case "right":
        case "bottom":
        case "left":
          result[key as keyof PositionObject] = value
          break
      }
    }
    cursor += 2
  }

  return result
}

export function parseObjectPositionResponsive(
  position: ResponsiveStringValue
): ResponsiveValues<PositionObject> {
  return {
    xs: parseObjectPosition(position.xs ?? ""),
    sm: parseObjectPosition(position.sm ?? ""),
    md: parseObjectPosition(position.md ?? ""),
    lg: parseObjectPosition(position.lg ?? ""),
    xl: parseObjectPosition(position.xl ?? ""),
  }
}
