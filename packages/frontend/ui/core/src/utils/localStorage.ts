export class LocalStorageService {
  writeString = (key: string, value: string) =>
    window.localStorage.setItem(key, value ?? "")

  readString = (key: string): string | undefined =>
    window.localStorage.getItem(key) ?? undefined

  writeObject = <T>(key: string, value: T) =>
    window.localStorage.setItem(key, JSON.stringify(value))

  readObject = <T>(key: string): T | undefined => {
    const serializedValue = window.localStorage.getItem(key)
    if (serializedValue === "undefined") {
      return undefined
    }
    return serializedValue ? (JSON.parse(serializedValue) as T) : undefined
  }

  removeObject = (key: string) => {
    window.localStorage.removeItem(key)
  }

  clearObjects = () => window.localStorage.clear()
}
