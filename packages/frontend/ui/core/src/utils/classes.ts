export type ClassValue =
  | string
  | number
  | boolean
  | undefined
  | null
  | ClassArray
  | ClassObject
type ClassArray = Array<ClassValue>
type ClassObject = { [key: string]: boolean | undefined | null }

export function classNames(...args: ClassValue[]): string {
  const classes: string[] = []

  for (const arg of args) {
    if (!arg) {
      continue
    }

    if (typeof arg === "string" || typeof arg === "number") {
      classes.push(String(arg))
    } else if (Array.isArray(arg)) {
      classes.push(classNames(...arg))
    } else if (typeof arg === "object") {
      for (const key in arg) {
        if (Object.prototype.hasOwnProperty.call(arg, key) && arg[key]) {
          classes.push(key)
        }
      }
    }
  }

  return classes.join(" ")
}
