export const isFirst = (index: number) => index === 0
export const isLast = (index: number, array: any[]) =>
  index === array.length - 1

export const groupBy = <T>(
  array: T[],
  key: (item: T) => PropertyKey
): Record<ReturnType<typeof key>, T[]> => {
  return array.reduce((acc, curr) => {
    const groupKey = key(curr)
    acc[groupKey] = [...(acc[groupKey] || []), curr]
    return acc
  }, {} as Record<ReturnType<typeof key>, T[]>)
}
