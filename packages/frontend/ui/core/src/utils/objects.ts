export const isNullOrUndefined = (value: any) => {
  return value === null || value === undefined
}

export const isObject = (obj: any) => obj && typeof obj === "object"

export const deepMerge = <T>(obj1: any, obj2: any): T => {
  if (!isObject(obj1)) {
    return obj2
  }

  if (!isObject(obj2)) {
    return obj1
  }

  const keys = new Set([...Object.keys(obj1), ...Object.keys(obj2)])
  const result = {} as any

  keys.forEach((key) => {
    const val1 = obj1[key]
    const val2 = obj2[key]

    if (Array.isArray(val1) && Array.isArray(val2)) {
      result[key] = val2 === undefined ? val1 : val2
    } else if (isObject(val1) && isObject(val2)) {
      result[key] = deepMerge(val1, val2)
    } else {
      result[key] = val2 === undefined ? val1 : val2
    }
  })

  return result as T
}

export const jsonClone = <T>(obj: T): T => {
  return !!obj ? JSON.parse(JSON.stringify(obj)) : undefined
}

export function jsonEquals(obj1: any, obj2: any) {
  return JSON.stringify(obj1) === JSON.stringify(obj2)
}
