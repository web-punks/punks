import { parseObjectPosition } from "./positioning"

describe("positioning", () => {
  it("should parse right -10% left 8px top -10vh bottom 9px position", () => {
    const parsed = parseObjectPosition(
      "right -10% left 8px top -10vh bottom 9px"
    )
    expect(parsed).toMatchObject({
      top: "-10vh",
      right: "-10%",
      bottom: "9px",
      left: "8px",
    })
  })

  it("should parse right -10% top -10vh position", () => {
    const parsed = parseObjectPosition("right -10% top -10vh")
    expect(parsed).toMatchObject({
      top: "-10vh",
      right: "-10%",
      bottom: "auto",
      left: "auto",
    })
  })

  it("should parse center position", () => {
    const parsed = parseObjectPosition("center")
    expect(parsed).toMatchObject({
      top: "auto",
      right: "auto",
      bottom: "auto",
      left: "auto",
    })
  })

  it("should parse center center position", () => {
    const parsed = parseObjectPosition("center center")
    expect(parsed).toMatchObject({
      top: "auto",
      right: "auto",
      bottom: "auto",
      left: "auto",
    })
  })

  it("should parse center center position", () => {
    const parsed = parseObjectPosition("center bottom 10%")
    expect(parsed).toMatchObject({
      top: "auto",
      right: "auto",
      bottom: "10%",
      left: "auto",
    })
  })

  it("should parse right -10% center position", () => {
    const parsed = parseObjectPosition("right -10% center")
    expect(parsed).toMatchObject({
      top: "auto",
      right: "-10%",
      bottom: "auto",
      left: "auto",
    })
  })

  it("should parse center center position", () => {
    const parsed = parseObjectPosition("")
    expect(parsed).toMatchObject({
      top: "",
      right: "",
      bottom: "",
      left: "",
    })
  })
})
