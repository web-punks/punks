export const setCssVar = (
  varName: string,
  value: string | number | undefined
) => {
  return {
    [varName]: value ?? "",
  } as React.CSSProperties
}

export const normalizePxSize = (
  size: number | string | undefined
): string | undefined => {
  function isNumber(value: string): boolean {
    const regex = /^-?\d+(\.\d+)?$/
    return regex.test(value)
  }

  if (typeof size === "number") {
    return `${size}px`
  }

  if (typeof size === "string" && isNumber(size)) {
    return `${size}px`
  }

  return size
}
