export const openUrlInNewWindow = (url: string) => window.open(url, "_blank")
