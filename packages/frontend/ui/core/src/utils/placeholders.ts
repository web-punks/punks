export type LabelsPlaceholders = {
  [key: string]: string | number | boolean | null | undefined
}

const toString = (value: string | number | boolean | null | undefined) => {
  return value ? value.toString() : ""
}

const replaceAll = (input: string, value: string, replaceWith: string) => {
  return input.split(value).join(replaceWith)
}

export const replacePlaceholders = (
  value: string,
  placeholders: LabelsPlaceholders
) => {
  return Object.entries(placeholders).reduce((acc, [key, value]) => {
    return replaceAll(acc, `{{${key}}}`, toString(value))
  }, value)
}
