import { useEffect } from "react"

export const useOnUnmount = (handler: () => void) => {
  useEffect(() => {
    return () => {
      handler()
    }
  }, [])
}
