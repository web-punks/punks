import { useEffect, useState } from "react"
import { useDebouncedValue } from "./useDebouncedValue"

export const useDebouncedCallback = <T>({
  value,
  callback,
  delay = 1000,
}: {
  value: T
  callback: (value: T) => void
  delay?: number
}) => {
  const debouncedValue = useDebouncedValue(value, delay)
  const [initialized, setInitialized] = useState(false)

  useEffect(() => {
    if (!initialized) {
      setInitialized(true)
      return
    }

    callback(debouncedValue)
  }, [debouncedValue])
}
