import React, { useState, useEffect } from "react"

export const useIntersectionObserver = <T extends Element>(
  ref: React.RefObject<T>,
  rootMargin?: string
) => {
  const [isVisible, setState] = useState(false)

  useEffect(() => {
    const observer = new IntersectionObserver(
      ([entry]) => {
        setState(entry.isIntersecting)
      },
      { rootMargin }
    )

    ref.current && observer.observe(ref.current)

    return () => {
      if (ref?.current) {
        observer.unobserve(ref.current)
      }
    }
  }, [])

  return { isVisible }
}
