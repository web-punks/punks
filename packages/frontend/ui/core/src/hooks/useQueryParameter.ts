import { deserializeQueryString } from "../utils"
import { useLocation } from "./useLocation"

export const useQueryParameter = (name: string) => {
  const location = useLocation()
  const urlParams = deserializeQueryString(location?.search ?? "")
  return urlParams?.[name]
}
