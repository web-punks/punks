import { useEffect, useState } from "react"
/**
 * Parameters for the useIsDesktop hook.
 */
type UseIsDesktopParams = {
  /** Initial value to use before the effect runs. Defaults to true. */
  fallback?: boolean
  /** Custom media query string. If not provided, defaults to "(min-width: 1024px)". */
  query?: string
}
/**
 * A custom React hook that determines if the current viewport matches a desktop-sized screen.
 *
 * @param {boolean} [options.fallback=true] - The initial value to use before the effect runs.
 * @param {string} [options.query="(min-width: 1024px)"] - A custom media query string to use instead of the default.
 * @returns {boolean} Whether the current viewport matches the desktop criteria.
 */
export const useIsDesktop = ({
  fallback = true,
  query = "(min-width: 1024px)",
}: UseIsDesktopParams = {}): boolean => {
  const [isDesktop, setIsDesktop] = useState(fallback)

  useEffect(() => {
    setIsDesktop(window.matchMedia(query).matches)

    const windowResizeController = new AbortController()
    window.addEventListener(
      "resize",
      () => {
        setIsDesktop(window.matchMedia(query).matches)
      },
      { signal: windowResizeController.signal }
    )

    return () => windowResizeController.abort()
  }, [])

  return isDesktop
}
