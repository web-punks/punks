import { useRef } from "react"

export interface OnClickOutsideInput {
  onTriggered: () => void
  allowAnyKey?: boolean
  triggerKeys?: string[]
  excludedElements?: HTMLElement[]
}

/**
 * Hook used to detect clicks outside a component (or an escape key press). onTriggered function is triggered on `click` or escape `keyup` event.
 *
 */
export function useOnClickOutside({
  onTriggered,
  allowAnyKey,
  triggerKeys,
  excludedElements,
}: OnClickOutsideInput) {
  const ref = useRef(null)

  const onKeyboardEvent = (e: React.KeyboardEvent) => {
    if (allowAnyKey) {
      onTriggered()
    } else if (triggerKeys) {
      if (triggerKeys.includes(e.key)) {
        onTriggered()
      }
    } else {
      if (e.key === "Escape") {
        onTriggered()
      }
    }
  }

  const onClick = (e: React.MouseEvent) => {
    if (ref && ref.current) {
      if (
        !(ref.current! as any).contains(e.target) &&
        !excludedElements?.some((x) => x.contains(e.target as any))
      ) {
        onTriggered()
      }
    }
  }

  return {
    ref,
    onKeyboardEvent,
    onClick,
  }
}
