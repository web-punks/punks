import { useEffect, useState } from "react"

function deepCompare(obj1: any, obj2: any) {
  return JSON.stringify(obj1) === JSON.stringify(obj2)
}

/**
 * @deprecated Use useDebouncedValue instead
 */
export function useDebounce<T>(
  value: T,
  delay?: number,
  options?: {
    deepCompare?: boolean
  }
): T {
  const [debouncedValue, setDebouncedValue] = useState<T>(value)

  useEffect(() => {
    const timer = setTimeout(() => {
      const isChanged = options?.deepCompare
        ? !deepCompare(value, debouncedValue)
        : value !== debouncedValue
      if (isChanged) {
        setDebouncedValue(value)
      }
    }, delay || 500)

    return () => {
      clearTimeout(timer)
    }
  }, [value, delay])

  return debouncedValue
}
