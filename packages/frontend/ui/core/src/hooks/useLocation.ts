import React, { useEffect } from "react"

export const useLocation = () => {
  const [location, setLocation] = React.useState<Location>()

  useEffect(() => {
    setLocation(window.location)
  }, [])

  return location
}
