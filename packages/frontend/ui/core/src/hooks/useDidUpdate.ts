import { EffectCallback, useEffect, useRef } from "react"

export const useDidUpdate = (fn: EffectCallback, dependencies?: any[]) => {
  const mounted = useRef(false)

  useEffect(() => {
    if (mounted.current) {
      fn()
    } else {
      mounted.current = true
    }
  }, dependencies)
}
