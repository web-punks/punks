import { useEffect, useState } from "react"
import { jsonClone, jsonEquals } from "../utils/objects"

export function useDebouncedValue<T>(value: T, delay: number = 500): T {
  const [debouncedValue, setDebouncedValue] = useState<T>(value)

  useEffect(() => {
    const timer = setTimeout(() => {
      const isChanged = !jsonEquals(value, debouncedValue)
      if (isChanged) {
        setDebouncedValue(jsonClone(value))
      }
    }, delay)

    return () => {
      clearTimeout(timer)
    }
  }, [JSON.stringify(value), delay])

  return debouncedValue
}
