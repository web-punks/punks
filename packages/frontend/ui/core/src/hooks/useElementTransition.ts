import { useEffect } from "react"

export const useElementTransition = ({
  element,
  trigger,
  onTransitionEnd,
}: {
  element: HTMLElement | null
  trigger: boolean
  onTransitionEnd: (event: TransitionEvent) => void
}) => {
  useEffect(() => {
    if (!element) return

    const handleTransitionEnd = (event: TransitionEvent) => {
      onTransitionEnd(event)
    }

    element.addEventListener("transitionend", handleTransitionEnd)

    return () => {
      element.removeEventListener("transitionend", handleTransitionEnd)
    }
  }, [trigger])
}
