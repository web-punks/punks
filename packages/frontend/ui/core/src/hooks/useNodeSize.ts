import { useCallback, useEffect, useRef, useState } from "react"

const getNodeSize = (node: HTMLElement) => {
  return node.getBoundingClientRect()
}
export const useNodeSize = () => {
  const [intObserver, setIntObserver] = useState<IntersectionObserver>()
  const [resObserver, setResObserver] = useState<ResizeObserver>()
  const [mutObserver, setMutObserver] = useState<MutationObserver>()
  const [size, setSize] = useState<DOMRect>()
  const ref = useRef<any>()

  const updateSize = () => {
    setSize(getNodeSize(ref.current))
  }

  const setRef = useCallback(<T extends Element | null>(node: T) => {
    if (node !== null) {
      ref.current = node
      const intObs = new IntersectionObserver((mutationsList, o) => {
        console.log()
        for (const mutation of mutationsList) {
          if (mutation.isIntersecting) {
            updateSize()
            // o.disconnect()
          }
        }
      })
      intObs.observe(node)
      setIntObserver(intObs)

      const resObs = new ResizeObserver((entries) => {
        for (const entry of entries) {
          updateSize()
        }
      })
      resObs.observe(node)
      setResObserver(resObs)

      const mutObs = new MutationObserver((mutationList, observer) => {
        // Use traditional 'for loops' for IE 11
        for (const mutation of mutationList) {
          if (mutation.type === "attributes") {
            updateSize()
          }
        }
      })
      mutObs.observe(node, { attributes: true })
      setMutObserver(mutObs)
    } else {
      setIntObserver(undefined)
      setResObserver(undefined)
      setMutObserver(undefined)
    }
  }, [])

  useEffect(() => {
    return () => {
      if (intObserver) {
        intObserver.disconnect()
      }
    }
  }, [intObserver])
  useEffect(() => {
    return () => {
      if (resObserver) {
        resObserver.disconnect()
      }
    }
  }, [resObserver])
  useEffect(() => {
    return () => {
      if (mutObserver) {
        mutObserver.disconnect()
      }
    }
  }, [mutObserver])

  return [size, setRef, ref] as const
}
