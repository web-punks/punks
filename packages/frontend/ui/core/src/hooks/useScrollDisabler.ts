import React from "react"
import { useIsClient } from "./useIsClient"

export const useScrollDisabler = (disableScroll: boolean) => {
  const isClient = useIsClient()
  const [activated, setActivated] = React.useState(false)

  const handleDisableScroll = () => {
    document.body.style.overflowY = "hidden"
  }
  const handleEnableScroll = () => {
    document.body.style.overflowY = ""
  }

  React.useEffect(() => {
    if (!isClient) {
      return
    }

    if (disableScroll) {
      handleDisableScroll()
      setActivated(true)
    } else {
      if (activated) {
        handleEnableScroll()
      }
    }
  }, [disableScroll])
}
