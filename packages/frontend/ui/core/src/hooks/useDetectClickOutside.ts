import { useEffect, useRef } from "react"

export interface DetectClickOutsideInput {
  onTriggered: (e: Event) => void
  disableClick?: boolean
  disableKeys?: boolean
  allowAnyKey?: boolean
  triggerKeys?: string[]
  excludedElements?: HTMLElement[]
}

/**
 * Hook used to detect clicks outside a component (or an escape key press). onTriggered function is triggered on `click` or escape `keyup` event.
 *
 */
export function useDetectClickOutside<RefType extends HTMLElement>({
  onTriggered,
  disableClick = false,
  disableKeys = false,
  allowAnyKey = false,
  triggerKeys = [],
  excludedElements = [],
}: DetectClickOutsideInput) {
  const ref = useRef<RefType>(null)

  useEffect(() => {
    function handleClickOutside(event: Event) {
      const target = event.target as HTMLElement

      if (
        !(ref.current as any)?.contains(target) &&
        !excludedElements.includes(target) &&
        !disableClick
      ) {
        onTriggered(event)
      }
    }

    function handleKeyDown(event: KeyboardEvent) {
      const { key } = event

      if (
        (!disableKeys && triggerKeys.length === 0 && !allowAnyKey) ||
        (disableKeys && triggerKeys.length === 0) ||
        (triggerKeys.includes(key) && !disableKeys) ||
        (allowAnyKey && !disableKeys)
      ) {
        onTriggered(event)
      }
    }

    document.addEventListener("mousedown", handleClickOutside)
    document.addEventListener("keydown", handleKeyDown)

    return () => {
      document.removeEventListener("mousedown", handleClickOutside)
      document.removeEventListener("keydown", handleKeyDown)
    }
  }, [
    onTriggered,
    disableClick,
    disableKeys,
    allowAnyKey,
    triggerKeys,
    excludedElements,
  ])

  return ref
}
