import { useEffect } from "react"

export const useKeyListener = ({
  key,
  handler,
}: {
  key: string
  handler: () => void
}) => {
  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      if (event.key === key) {
        handler()
      }
    }

    document.addEventListener("keydown", handleKeyDown)
    return () => {
      document.removeEventListener("keydown", handleKeyDown)
    }
  }, [])
}

export const useEscKeyListener = (handler: () => void) => {
  useKeyListener({
    key: "Escape",
    handler,
  })
}
