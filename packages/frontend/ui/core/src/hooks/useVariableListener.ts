import { useEffect, useRef } from "react"

export const useVariableListener = <T>(
  value: T,
  {
    onChange,
    skipFirst = false,
  }: {
    onChange: (value: T) => void
    skipFirst?: boolean
  }
) => {
  const firstRender = useRef(true)

  useEffect(() => {
    if (skipFirst && firstRender.current) {
      firstRender.current = false
      return
    }

    onChange(value)
  }, [value])
}
