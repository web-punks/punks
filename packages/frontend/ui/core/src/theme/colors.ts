import { ThemeColorGradient, ThemeColor, ThemeColorVariant } from "../types"
import { setCssVar } from "../utils"
import { cssVar } from "./common"

export const getGradientColorVar = (
  color: ThemeColor,
  gradient: ThemeColorGradient
) => `--wp-theme-color-${color}-${gradient}`

export const getColorVar = (color: ThemeColor) => `--wp-theme-color-${color}`
export const getColorVarContrast = (color: ThemeColor) =>
  `--wp-theme-color-${color}-contrast`
export const getColorVarLight = (color: ThemeColor) =>
  `--wp-theme-color-${color}-light`
export const getColorVarDark = (color: ThemeColor) =>
  `--wp-theme-color-${color}-dark`

export const getThemeColorVar = (
  color: ThemeColor,
  variant?: ThemeColorVariant
) => {
  switch (variant) {
    case "contrast":
      return getColorVarContrast(color)
    case "light":
      return getColorVarLight(color)
    case "dark":
      return getColorVarDark(color)
    default:
      return getColorVar(color)
  }
}

export const setColorVar = (varName: string, color?: ThemeColor) =>
  color ? setCssVar(varName, cssVar(getColorVar(color))) : undefined

export const setColorVarContrast = (varName: string, color?: ThemeColor) =>
  color ? setCssVar(varName, cssVar(getColorVarContrast(color))) : undefined

export const setColorVarLight = (varName: string, color?: ThemeColor) =>
  color ? setCssVar(varName, cssVar(getColorVarLight(color))) : undefined

export const setColorVarDark = (varName: string, color?: ThemeColor) =>
  color ? setCssVar(varName, cssVar(getColorVarDark(color))) : undefined

export const setThemeColorVar = (
  varName: string,
  color?: ThemeColor,
  variant?: ThemeColorVariant
) =>
  color
    ? setCssVar(varName, cssVar(getThemeColorVar(color, variant)))
    : undefined
