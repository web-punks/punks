import { ThemeVariables } from "./types"
import {
  ComplexColor,
  GradientColor,
  SidebarColors,
  TextColors,
  ThemeColors,
  ThemePalette,
} from "../palette"
import { ThemeVariablesPrefixes } from "../consts"

const themeColorVariables = (
  palette: string,
  name: string,
  color: ComplexColor
): { [key: string]: string } => ({
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}`]: color.default,
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-contrast`]:
    color.contrast ?? "",
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-focus`]:
    color.focus ?? "",
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-light`]:
    color.light ?? "",
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-dark`]:
    color.dark ?? "",
})

const themeScaledColorVariables = (
  palette: string,
  name: string,
  color: GradientColor
): { [key: string]: string } => ({
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-10`]: color[10],
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-20`]: color[20],
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-30`]: color[30],
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-40`]: color[40],
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-50`]: color[50],
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-60`]: color[60],
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-70`]: color[70],
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-80`]: color[80],
  [`--${ThemeVariablesPrefixes.Color}-${palette}-${name}-90`]: color[90],
})

const themeTextColorVariables = (palette: string, colors: TextColors) => ({
  [`--${ThemeVariablesPrefixes.Color}-${palette}-text-default`]: colors.default,
  [`--${ThemeVariablesPrefixes.Color}-${palette}-text-caption`]: colors.caption,
})

const themeSidebarColorValues = (palette: string, colors: SidebarColors) => ({
  [`--${ThemeVariablesPrefixes.Color}-${palette}-sidebar-background`]:
    colors.background,
  [`--${ThemeVariablesPrefixes.Color}-${palette}-sidebar-content`]:
    colors.content,
})

const buildBaseColors = (colors: ThemeColors, palette: string) => {
  return {
    ...themeColorVariables(palette, "primary", colors.primary),
    ...themeColorVariables(palette, "secondary", colors.secondary),
    ...themeColorVariables(palette, "info", colors.info),
    ...themeColorVariables(palette, "success", colors.success),
    ...themeColorVariables(palette, "error", colors.error),
    ...themeColorVariables(palette, "warning", colors.warning),
    ...themeColorVariables(palette, "background", colors.background),
    ...themeColorVariables(palette, "paper", colors.paper),
    ...themeColorVariables(palette, "paperNegative", colors.paperNegative),
    ...themeColorVariables(palette, "white", colors.paper),
    ...themeColorVariables(palette, "black", colors.paper),
    ...themeColorVariables(palette, "border", colors.border),
    ...themeScaledColorVariables(palette, "grey", colors.grey),
    ...themeSidebarColorValues(palette, colors.sidebar),
    ...themeTextColorVariables(palette, colors.text),
  }
}

export const buildThemePaletteVariables = (
  palette: ThemePalette
): ThemeVariables => {
  return {
    ...buildBaseColors(palette.light, "light"),
    ...(palette.dark ? buildBaseColors(palette.dark, "dark") : {}),
  }
}
