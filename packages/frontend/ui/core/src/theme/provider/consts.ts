export const ThemeVariablesPrefixes = {
  Color: "wp-theme-color",
  Text: "wp-theme-text",
  Font: "wp-theme-font",
}
