import { render } from "@testing-library/react"
import { mockedTheme } from "./mocks"
import React from "react"
import { ThemeStyles } from "../styles"

describe("styles theme", () => {
  it("theme", () => {
    const { container: component } = render(<ThemeStyles theme={mockedTheme} />)
    expect(component).toMatchSnapshot()
  })
})
