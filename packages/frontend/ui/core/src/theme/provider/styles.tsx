import React, { useMemo } from "react"
import { AppTheme } from "./types"
import { buildThemeVariables } from "./builder"
import { ThemeVariablesPrefixes } from "./consts"
import { ThemeColor, ThemeColorGradient } from "../../types"
import {
  getColorVar,
  getColorVarContrast,
  getColorVarDark,
  getColorVarLight,
  getGradientColorVar,
} from "../colors"

const buildThemeColor = (color: ThemeColor, mode: string) => `
${getColorVar(color)}: var(--${ThemeVariablesPrefixes.Color}-${mode}-${color});
${getColorVarContrast(color)}: var(--${
  ThemeVariablesPrefixes.Color
}-${mode}-${color}-contrast);
--${ThemeVariablesPrefixes.Color}-${color}-focus: var(--${
  ThemeVariablesPrefixes.Color
}-${mode}-${color}-focus);
${getColorVarLight(color)}: var(--${
  ThemeVariablesPrefixes.Color
}-${mode}-${color}-light);
${getColorVarDark(color)}: var(--${
  ThemeVariablesPrefixes.Color
}-${mode}-${color}-dark);
--${ThemeVariablesPrefixes.Color}-text-${color}: var(--${
  ThemeVariablesPrefixes.Color
}-${color});
`

const buildScaledThemeColor = (color: ThemeColor, mode: string) =>
  ([10, 20, 30, 40, 50, 60, 70, 80, 90] as ThemeColorGradient[])
    .map(
      (x) =>
        `${getGradientColorVar(color, x)}: var(--${
          ThemeVariablesPrefixes.Color
        }-${mode}-${color}-${x});`
    )
    .join("\n")

const mainColors: ThemeColor[] = [
  "primary",
  "secondary",
  "info",
  "success",
  "error",
  "warning",
  // "background",
  "paper",
  "paperNegative",
  "white",
  "black",
  // "border",
]

const gradientColors: ThemeColor[] = ["grey"] as any

const buildThemeColors = (mode: string) => `
${mainColors.map((x) => buildThemeColor(x, mode)).join("\n")}
${gradientColors.map((x) => buildScaledThemeColor(x, mode)).join("\n")}
`

export interface ThemeStylesProps {
  theme: AppTheme
  excludeBaseStyles?: boolean
}

export const buildStylesheet = (theme: AppTheme) => {
  const variables = buildThemeVariables(theme)
  return `
:root {
  ${Object.keys(variables)
    .map((key) => `${key}: ${variables[key]};`)
    .join("\n")}
}

:root,
:root.light,
:root[data-theme="light"] {
  ${buildThemeColors("light")}
}

:root.dark,
:root[data-theme="dark"] {
  ${buildThemeColors("dark")}
}

.root {
}
`
}

// export const useStylesheet = (theme: AppTheme) => {
//   const variables = useMemo(() => buildThemeVariables(theme), [theme])
//   return buildStylesheet(variables)
// }

export const ThemeStyles = ({ theme, excludeBaseStyles }: ThemeStylesProps) => {
  const variables = buildThemeVariables(theme) // useMemo(() => buildThemeVariables(theme), [theme])
  return (
    // @ts-ignore
    <style jsx global>
      {`
        :root {
          ${Object.keys(variables)
            .map((key) => `${key}: ${variables[key]};`)
            .join("\n")}
        }

        :root,
        :root.light,
        :root[data-theme="light"] {
          ${buildThemeColors("light")}
        }

        :root.dark,
        :root[data-theme="dark"] {
          ${buildThemeColors("dark")}
        }

        .root {
        }
      `}
    </style>
  )
}
