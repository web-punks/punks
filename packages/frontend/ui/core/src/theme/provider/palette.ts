export type ComplexColor = {
  default: string
  focus?: string
  contrast?: string
  light?: string
  dark?: string
}

export type GradientColor = {
  10: string
  20: string
  30: string
  40: string
  50: string
  60: string
  70: string
  80: string
  90: string
}

export type SingleColor = {
  default: string
}

export type TextColors = {
  default: string
  caption: string
}

export type SidebarColors = {
  background: string
  content: string
}

export type ThemeColors = {
  primary: ComplexColor
  secondary: ComplexColor
  info: ComplexColor
  success: ComplexColor
  error: ComplexColor
  warning: ComplexColor
  background: ComplexColor
  paper: ComplexColor
  paperNegative: ComplexColor
  border: ComplexColor
  white: ComplexColor
  black: ComplexColor
  grey: GradientColor
  sidebar: SidebarColors
  text: TextColors
}

export type ThemePalette = {
  light: ThemeColors
  dark?: ThemeColors
}
