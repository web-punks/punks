import { TextVariant } from "../types"

export const getTextSizeVar = (variant: TextVariant) =>
  `--wp-text-${variant}-size`

export const getTextLineHeightVar = (variant: TextVariant) =>
  `--wp-text-${variant}-line-height`

export const getTextLetterSpacingVar = (variant: TextVariant) =>
  `--wp-text-${variant}-spacing`

export const getTextWeightVar = (variant: TextVariant) =>
  `--wp-text-${variant}-weight`

export const getTextFontVar = (variant: TextVariant) =>
  `--wp-text-${variant}-font`
