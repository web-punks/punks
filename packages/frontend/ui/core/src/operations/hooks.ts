import React, { useEffect } from "react"
import { newUuid } from "../utils/uid"

export type OperationCallbacks<TResult = unknown, TInput = unknown> = {
  onStarted?: (variables: TInput) => void
  onSuccess?: (result: TResult) => void
  onError?: (error: Error) => void
  onFinalized?: (variables: TInput) => void
}

export type UseOperationInput<TResult, TInput> = OperationCallbacks<
  TResult,
  TInput
> & {
  operation: (variables: TInput) => Promise<TResult>
  autoTrigger?: boolean
  key?: string[]
}

export type OperationState<TResult> = {
  operationId?: string
  loading: boolean
  completed: boolean
  called: boolean
  success?: boolean
  data?: TResult
  error?: any
  elapsedTime?: number
}

const invokeOperation = async <TResult, TInput>(
  input: UseOperationInput<TResult, TInput>,
  variables?: TInput
): Promise<TResult> => {
  if (variables) {
    return await (input.operation as (variables: TInput) => Promise<TResult>)(
      variables
    )
  }
  return await (input.operation as () => Promise<TResult>)()
}

export const useOperation = <TResult, TInput>(
  input: UseOperationInput<TResult, TInput>
) => {
  const isAutoTriggerInvoked = React.useRef(false)
  const startTime = React.useRef<number>(0)
  const [state, setState] = React.useState<OperationState<TResult>>({
    called: false,
    loading: false,
    completed: false,
  })

  const invoke = async (variables?: TInput): Promise<TResult | undefined> => {
    isAutoTriggerInvoked.current = input?.key ? false : true
    const operationId = newUuid()
    const start = Date.now()
    setState({
      called: true,
      loading: true,
      completed: false,
      data: undefined,
      error: undefined,
      operationId,
      elapsedTime: undefined,
    })
    try {
      input.onStarted?.(variables as TInput)
      const result = await invokeOperation(input, variables)
      setState({
        called: true,
        loading: false,
        data: result,
        error: undefined,
        operationId,
        completed: true,
        success: true,
        elapsedTime: Date.now() - start,
      })
      input.onSuccess?.(result)
      return result
    } catch (e: any) {
      console.error("Error invoking operation", e)
      setState({
        called: true,
        loading: false,
        data: undefined,
        error: e,
        operationId,
        completed: true,
        success: false,
        elapsedTime: Date.now() - start,
      })
      input.onError?.(e)
      throw e
    } finally {
      input.onFinalized?.(variables as TInput)
    }
  }

  useEffect(
    () => {
      if (!input.autoTrigger || isAutoTriggerInvoked.current) {
        return
      }
      invoke().catch(() => null)
    },
    input?.key ? [JSON.stringify(input.key)] : []
  )
  return {
    invoke,
    state,
  }
}
