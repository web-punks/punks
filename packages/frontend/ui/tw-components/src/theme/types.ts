import { ThemePalette } from "./palette"

export type HeadWrapper = React.FC<{ children: any }>

export type TypographyFont = {
  family: string
  url?: string
}

export type TypographyFonts = {
  default: TypographyFont
  secondary?: TypographyFont
}

export type ThemeFontWeights = {
  extraLight: number
  light: number
  regular: number
  medium: number
  semiBold: number
  bold: number
  extraBold: number
}

export type ThemeTextVariant = {
  size: string
  height: string
  weight: number
  style?: string
  font?: string
}

export type ThemeTextVariants = {
  // extra: ThemeTextVariant
  h1: ThemeTextVariant
  h2: ThemeTextVariant
  h3: ThemeTextVariant
  h4: ThemeTextVariant
  h5: ThemeTextVariant
  h6: ThemeTextVariant
  subtitle1: ThemeTextVariant
  subtitle2: ThemeTextVariant
  subtitle3: ThemeTextVariant
  body1: ThemeTextVariant
  body2: ThemeTextVariant
  body3: ThemeTextVariant
  label1: ThemeTextVariant
  label2: ThemeTextVariant
  label3: ThemeTextVariant
  caption1: ThemeTextVariant
  caption2: ThemeTextVariant
  caption3: ThemeTextVariant
}

export type ThemeTypography = {
  fonts: TypographyFonts
  weights: ThemeFontWeights
  variants: ThemeTextVariants
}

export type AppTheme = {
  palette: ThemePalette
  typography: ThemeTypography
  // brand: ThemeBrand
}
