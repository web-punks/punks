import { render } from "@testing-library/react"
import { ThemeStyles } from "../styles"
import { mockedTheme } from "./mocks"
import React from "react"

describe("styles theme", () => {
  it("theme", () => {
    const { container: component } = render(<ThemeStyles theme={mockedTheme} />)
    expect(component).toMatchSnapshot()
  })
})
