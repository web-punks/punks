import { ThemeStyles } from "./styles"
import React from "react"
import { AppTheme, HeadWrapper } from "./types"
import { ThemeMode } from "@punks/ui-core"
import { AppThemeContext } from "./context"
import { ThemeFontsImport } from "./fonts"

export interface ThemeProviderProps {
  appTheme?: AppTheme
  fallbackTheme: AppTheme
  children: any
  Head: HeadWrapper
  importFonts?: boolean
}

export const ThemeProvider = ({
  children,
  fallbackTheme,
  appTheme,
  Head,
  importFonts,
}: ThemeProviderProps) => {
  const theme = appTheme ?? fallbackTheme
  const [mode, setMode] = React.useState<ThemeMode>("light")
  return (
    <AppThemeContext.Provider
      value={{
        fallbackTheme,
        theme: appTheme,
        mode,
        setMode,
      }}
    >
      {theme && (
        <>
          {/* <ThemeMetaImport theme={theme} Head={Head} /> */}
          {importFonts && <ThemeFontsImport theme={theme} Head={Head} />}
          <ThemeStyles theme={theme} />
        </>
      )}
      <>{children}</>
    </AppThemeContext.Provider>
  )
}
