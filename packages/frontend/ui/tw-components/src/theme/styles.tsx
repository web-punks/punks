import React, { useMemo } from "react"
import { AppTheme } from "./types"
import { buildThemeVariables } from "./builder"
import { ThemeVariablesPrefixes } from "./consts"
import {
  ThemeColor,
  ThemeColorGradient,
  getColorVar,
  getColorVarContrast,
  getColorVarDark,
  getColorVarLight,
  getGradientColorVar,
} from "@punks/ui-core"

const buildThemeColor = (color: ThemeColor, mode: string) => `
${getColorVar(color)}: var(--${ThemeVariablesPrefixes.Color}-${mode}-${color});
${getColorVarContrast(color)}: var(--${
  ThemeVariablesPrefixes.Color
}-${mode}-${color}-contrast);
--${ThemeVariablesPrefixes.Color}-${color}-focus: var(--${
  ThemeVariablesPrefixes.Color
}-${mode}-${color}-focus);
${getColorVarLight(color)}: var(--${
  ThemeVariablesPrefixes.Color
}-${mode}-${color}-light);
${getColorVarDark(color)}: var(--${
  ThemeVariablesPrefixes.Color
}-${mode}-${color}-dark);
--${ThemeVariablesPrefixes.Color}-text-${color}: var(--${
  ThemeVariablesPrefixes.Color
}-${color});
`

const buildScaledThemeColor = (color: ThemeColor, mode: string) =>
  ([10, 20, 30, 40, 50, 60, 70, 80, 90] as ThemeColorGradient[])
    .map(
      (x) =>
        `${getGradientColorVar(color, x)}: var(--${
          ThemeVariablesPrefixes.Color
        }-${mode}-${color}-${x});`
    )
    .join("\n")

const mainColors: ThemeColor[] = [
  "primary",
  "secondary",
  "info",
  "success",
  "error",
  "warning",
  // "background",
  "paper",
  "paperNegative",
  "white",
  "black",
  // "border",
]

const gradientColors: ThemeColor[] = ["grey"] as any

const buildThemeColors = (mode: string) => `
${mainColors.map((x) => buildThemeColor(x, mode)).join("\n")}
${gradientColors.map((x) => buildScaledThemeColor(x, mode)).join("\n")}
`

const DAISY_UI_VARS_LIGHT = `
  --n: var(--du-light-n);
  --nc: var(--du-light-nc);
  --nf: var(--du-light-nf);

  --a: var(--du-light-a);
  --ac: var(--du-light-ac);
  --af: var(--du-light-af);

  --p: var(--du-light-p);
  --pc: var(--du-light-pc);
  --pf: var(--du-light-pf);

  --s: var(--du-light-s);
  --sc: var(--du-light-sc);
  --sf: var(--du-light-sf);

  --in: var(--du-light-in);
  --inc: var(--du-light-inc);

  --su: var(--du-light-su);
  --suc: var(--du-light-suc);

  --wa: var(--du-light-wa);
  --wac: var(--du-light-wac);

  --er: var(--du-light-er);
  --erc: var(--du-light-erc);

  --b1: var(--du-light-b1);
  --b2: var(--du-light-b2);
  --b3: var(--du-light-b3);
  --bc: var(--du-light-bc);

  --b1: 0 0% 100%;
  --b2: 0 0% 95%;
  --b3: 180 2% 90%;
  --bc: 215 28% 17%;
`

const DAISY_UI_VARS_DARK = `
  --n: var(--du-dark-n);
  --nc: var(--du-dark-nc);
  --nf: var(--du-dark-nf);

  --a: var(--du-dark-a);
  --ac: var(--du-dark-ac);
  --af: var(--du-dark-af);

  --p: var(--du-dark-p);
  --pc: var(--du-dark-pc);
  --pf: var(--du-dark-pf);

  --s: var(--du-dark-s);
  --sc: var(--du-dark-sc);
  --sf: var(--du-dark-sf);

  --in: var(--du-dark-in);
  --inc: var(--du-dark-inc);

  --su: var(--du-dark-su);
  --suc: var(--du-dark-suc);

  --wa: var(--du-dark-wa);
  --wac: var(--du-dark-wac);

  --er: var(--du-dark-er);
  --erc: var(--du-dark-erc);

  --b1: var(--du-dark-b1);
  --b2: var(--du-dark-b2);
  --b3: var(--du-dark-b3);
  --bc: var(--du-dark-bc);

  --b1: 220 18% 20%;
  --b2: 220 17% 17%;
  --b3: 219 18% 15%;
  --bc: 220 13% 69%;
`

export interface ThemeStylesProps {
  theme: AppTheme
  excludeBaseStyles?: boolean
}

export const buildStylesheet = (variables: any) => `
:root {
  ${Object.keys(variables)
    .map((key) => `${key}: ${variables[key]};`)
    .join("\n")}
}

:root,
:root.light,
:root[data-theme="light"] {
  ${buildThemeColors("light")}
  ${DAISY_UI_VARS_LIGHT}
}

:root.dark,
:root[data-theme="dark"] {
  ${buildThemeColors("dark")}
  ${DAISY_UI_VARS_DARK}
}

.root {
}
`

export const useStylesheet = (theme: AppTheme) => {
  const variables = useMemo(() => buildThemeVariables(theme), [theme])
  return buildStylesheet(variables)
}

export const ThemeStyles = ({ theme, excludeBaseStyles }: ThemeStylesProps) => {
  const variables = useMemo(() => buildThemeVariables(theme), [theme])
  return (
    // @ts-ignore
    <style jsx global>
      {`
        :root {
          ${Object.keys(variables)
            .map((key) => `${key}: ${variables[key]};`)
            .join("\n")}
        }

        :root,
        :root.light,
        :root[data-theme="light"] {
          ${buildThemeColors("light")}
          ${DAISY_UI_VARS_LIGHT}
        }

        :root.dark,
        :root[data-theme="dark"] {
          ${buildThemeColors("dark")}
          ${DAISY_UI_VARS_DARK}
        }

        .root {
        }
      `}
    </style>
  )
}
