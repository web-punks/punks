import React from "react"
import { AppTheme } from "./types"
import { ThemeMode } from "@punks/ui-core"

export interface AppThemeContextData {
  theme?: AppTheme
  fallbackTheme?: AppTheme
  mode: ThemeMode
  setMode: (mode: ThemeMode) => void
}

export const AppThemeContext = React.createContext<AppThemeContextData>({
  mode: "light",
  setMode: () => {},
})
