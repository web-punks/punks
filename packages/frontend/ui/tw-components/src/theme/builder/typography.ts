import { ThemeVariablesPrefixes } from "../consts"
import {
  ThemeFontWeights,
  ThemeTextVariant,
  ThemeTextVariants,
  TypographyFont,
  TypographyFonts,
  ThemeTypography,
} from "../types"
import { ThemeVariables } from "./types"

const textVar = (suffix: string) => `--${ThemeVariablesPrefixes.Text}-${suffix}`
const fontVar = (suffix: string) => `--${ThemeVariablesPrefixes.Font}-${suffix}`

const fontWeightVariables = (weights: ThemeFontWeights) => ({
  [textVar("extraLight")]: weights.extraLight,
  [textVar("light")]: weights.light,
  [textVar("regular-w")]: weights.regular,
  [textVar("medium-w")]: weights.medium,
  [textVar("semiBold-w")]: weights.semiBold,
  [textVar("bold-w")]: weights.bold,
  [textVar("extraBold-w")]: weights.extraBold,
})

const themeTextVariant = (
  name: string,
  variant: ThemeTextVariant
): ThemeVariables => ({
  [textVar(`${name}-s`)]: variant.size,
  [textVar(`${name}-h`)]: variant.height,
  [textVar(`${name}-w`)]: variant.weight,
  [textVar(`${name}-st`)]: variant.style ?? "normal",
  [textVar(`${name}-f`)]: variant.font ?? "inherit", // family
})

const typographyVariantVariables = (
  variants: ThemeTextVariants
): ThemeVariables => ({
  ...Object.entries(variants).reduce(
    (obj, [key, value]) => ({
      ...obj,
      ...themeTextVariant(key, value),
    }),
    {}
  ),
})

const fontFamilyVariables = (
  name: string,
  font: TypographyFont
): ThemeVariables => ({
  [fontVar(name)]: font.family,
})

const fontFamiliesVariables = (fonts: TypographyFonts): ThemeVariables => ({
  ...fontFamilyVariables("default", fonts.default),
  ...fontFamilyVariables("secondary", fonts.secondary ?? fonts.default),
})

export const buildThemeTypographyVariables = (
  typography: ThemeTypography
): ThemeVariables => ({
  ...fontWeightVariables(typography.weights),
  ...typographyVariantVariables(typography.variants),
  ...fontFamiliesVariables(typography.fonts),
})
