import { AppTheme } from "../types"
import { buildThemePaletteVariables } from "./palette"
import { buildThemeTypographyVariables } from "./typography"

export const buildThemeVariables = (theme: AppTheme) => {
  return {
    ...buildThemePaletteVariables(theme.palette),
    ...buildThemeTypographyVariables(theme.typography),
  }
}
