export interface ThemeVariables {
  [key: string]: string | number
}
