import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { Modal } from "./Modal"

export default {
  title: "UI/Structure/Modal",
  component: Modal,
  args: {
    title: "Modal Title",
    children: "Modal Content",
    open: true,
  },
} as ComponentMeta<typeof Modal>

const Template: ComponentStory<typeof Modal> = (args) => <Modal {...args} />

export const Default = Template.bind({})
Default.args = {}

export const Small = Template.bind({})
Small.args = {
  width: "sm",
}

export const Medium = Template.bind({})
Medium.args = {
  width: "md",
}

export const Large = Template.bind({})
Large.args = {
  width: "lg",
}

export const ExtraLarge = Template.bind({})
ExtraLarge.args = {
  width: "xl",
}

export const FullScreen = Template.bind({})
FullScreen.args = {
  width: "fullScreen",
}
