import React from "react"
import { FormHelp } from "../../atoms/FormHelp/FormHelp"
import { FormLabel } from "../../atoms/FormLabel/FormLabel"
import styles from "./FormInput.module.css"
import { classNames } from "@punks/ui-core"

export interface FormInputProps {
  className?: string
  children: any
  label?: React.ReactNode
  helpText?: React.ReactNode
  fill?: boolean | number
  inline?: boolean
  bottom?: boolean
  my?: boolean | number
  fillLabel?: boolean
}

const normalizeFill = (value?: boolean | number) => {
  if (value === true) {
    return 1
  }
  return value
}

export const FormInput = ({
  className,
  children,
  label,
  helpText,
  fill,
  inline,
  bottom,
  my,
  fillLabel,
}: FormInputProps) => {
  const normalizedFill = normalizeFill(fill)
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.bottom]: bottom,
          ["my-6"]: my === true,
        },
        className
      )}
      style={{
        flex: normalizedFill ? `${normalizedFill} 0 0%` : undefined,
      }}
    >
      {(label || fillLabel) && (
        <FormLabel className="min-h-[24px]">{label ?? ""}</FormLabel>
      )}
      {inline && <div className="flex items-center">{children}</div>}
      {!inline && children}
      {helpText && <FormHelp>{helpText}</FormHelp>}
    </div>
  )
}

FormInput.defaultProps = {
  my: true,
}
