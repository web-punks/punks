import React from "react"
import { AddButton, RemoveButton } from "../../molecules/Buttons"

export interface FormArrayAddRemoveControlProps {
  onClick: () => void
  disabled?: boolean
  hidden?: boolean
}

export interface FormArrayAddRemoveControlsProps {
  add: FormArrayAddRemoveControlProps
  remove: FormArrayAddRemoveControlProps
}

export const FormArrayAddRemoveControls = ({
  add,
  remove,
}: FormArrayAddRemoveControlsProps) => {
  return (
    <div className="flex flex-col gap-2">
      {!add.hidden && (
        <AddButton
          onClick={() => add.onClick()}
          disabled={add.disabled}
          color="light"
        />
      )}
      {!remove.hidden && (
        <RemoveButton
          onClick={() => remove.onClick()}
          color="light"
          disabled={remove.disabled}
        />
      )}
    </div>
  )
}
