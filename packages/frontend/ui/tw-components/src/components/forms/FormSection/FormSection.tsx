import React from "react"
import { Separator } from "../../atoms/Separator"
import { Typography } from "../../atoms/Typography"
import { classNames } from "@punks/ui-core"

export interface FormSectionProps {
  title: React.ReactNode
  children: any
  className?: string
}

export const FormSection = ({
  children,
  title,
  className,
}: FormSectionProps) => {
  return (
    <div className={classNames("mb-8", className)}>
      <Typography variant="h4">{title}</Typography>
      <div>{children}</div>
      <Separator />
    </div>
  )
}
