import React from "react"
import { Typography } from "../../atoms/Typography"
import { classNames } from "@punks/ui-core"

export interface FormGroupProps {
  title: React.ReactNode
  children: any
  className?: string
}

export const FormGroup = ({ children, title, className }: FormGroupProps) => {
  return (
    <div className={classNames("mb-8 mt-4", className)}>
      <Typography variant="h5" weight={"bold"}>
        {title}
      </Typography>
      <div>{children}</div>
    </div>
  )
}
