import React from "react"
import { Typography } from "../../atoms/Typography"
import { classNames } from "@punks/ui-core"

export interface FormErrorProps {
  show?: boolean
  children: any
  className?: string
}

export const FormError = ({ children, show, className }: FormErrorProps) => {
  if (show === false) {
    return <></>
  }
  return (
    <Typography
      variant="body1"
      color="error"
      className={classNames("my-2", className)}
    >
      {children}
    </Typography>
  )
}
