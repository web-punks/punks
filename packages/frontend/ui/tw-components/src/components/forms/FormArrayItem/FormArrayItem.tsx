import { ReactNode } from "react"
import { Typography } from "../../atoms/Typography"
import React from "react"

export interface FormArrayItemProps {
  title?: ReactNode
  children: any
  extraControls?: ReactNode
}

export const FormArrayItem = ({
  title,
  children,
  extraControls,
}: FormArrayItemProps) => {
  return (
    <div className="my-8">
      {title && (
        <Typography variant={"subtitle2"} className="mb-2">
          {title}
        </Typography>
      )}
      <div className="flex items-center gap-2">
        <div className="flex-1 p-4 border-solid border-[1px] border-border-input rounded-md">
          {children}
        </div>
        {extraControls && <div>{extraControls}</div>}
      </div>
    </div>
  )
}
