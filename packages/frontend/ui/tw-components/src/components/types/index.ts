// import { themeKeys } from "../utils/theme"

export type ResponsiveValue<T> = T | Array<T | null> // | ResponsiveObject<T>>

// export type ResponsiveObject<T> = {
//   [P in Theme]?: T | null
// }

// export type ResposiveStyleOptions<T, A extends string> = {
//   // componentName: string
//   propertyNames: Readonly<A[]>
//   mapFunction: (
//     prop: A,
//     vals: {
//       [P in keyof T]: T[P] extends ResponsiveValue<infer R> ? R : never
//     }
//   ) => string | null | undefined
//   values: T
// }
// export type ResposiveClassesOptions<T> = {
//   mapFunction: (
//     vals: {
//       [P in keyof T]: T[P] extends ResponsiveValue<infer R> ? R : never
//     }
//   ) => string | null | undefined
//   values: T
// }

// export type Theme = typeof themeKeys[number]

// export type ThemeColor =
//   | "primary"
//   | "primary-light"
//   | "primary-contrast"
//   | "primary-focus"
//   | "secondary"
//   | "secondary-light"
//   | "secondary-contrast"
//   | "secondary-focus"
//   | "info"
//   | "info-light"
//   | "info-contrast"
//   | "info-focus"
//   | "success"
//   | "success-light"
//   | "success-contrast"
//   | "success-focus"
//   | "warning"
//   | "warning-light"
//   | "warning-contrast"
//   | "warning-focus"
//   | "error"
//   | "error-light"
//   | "error-contrast"
//   | "error-focus"
//   | "background"
//   | "background-light"
//   | "background-contrast"
//   | "background-focus"
//   | "paper"
//   | "paper-light"
//   | "paper-contrast"
//   | "paper-focus"
//   | "negative"
//   | "negative-light"
//   | "negative-contrast"
//   | "negative-focus"
//   | "border"
//   | "border-light"
//   | "border-contrast"
//   | "border-focus"
//   | "grey"
//   | "grey-medium"
//   | "sidebar-background"
//   | "sidebar-content"
//   | "white"
//   | "black"

// export type PaletteColor =
//   | "primary"
//   | "secondary"
//   | "info"
//   | "success"
//   | "warning"
//   | "error"

// export type ThemeMode = "light" | "dark"
