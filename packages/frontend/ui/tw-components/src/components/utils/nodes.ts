import React, { ReactNode } from "react"

export const filterNodes = <T>(
  children: ReactNode,
  type: React.ComponentType<T>
): ReactNode[] => {
  const nodes: ReactNode[] = []
  React.Children.forEach(children, (child) => {
    if (!React.isValidElement(child)) {
      return null
    }
    console.log(child.type, child.type.toString(), type)
    if (child.type === type) {
      nodes.push(child)
    }
  })
  return nodes
}

export const findNode = <T>(
  children: ReactNode,
  type: React.ComponentType<T>
) => {
  return filterNodes(children, type)[0]
}
