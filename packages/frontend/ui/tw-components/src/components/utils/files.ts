export const pathToBlob = (path: string): Promise<Blob> => {
  return new Promise<Blob>((resolve, reject) => {
    try {
      const reader = new FileReader()
      reader.onload = function (e) {
        resolve(e.target?.result as any)
      }
      reader.readAsDataURL(path as any)
    } catch (e) {
      return reject(e)
    }
  })
}

export const bytesToMB = (size: number) => size / 1024 / 1024
export const getFileMB = (file: File) => bytesToMB(file.size)

export const getFileSizeLabel = (sizeMB: number) => {
  if (sizeMB < 1) {
    return `${(sizeMB * 1024).toFixed(2)} KB`
  }
  return `${sizeMB.toFixed(2)} MB`
}
