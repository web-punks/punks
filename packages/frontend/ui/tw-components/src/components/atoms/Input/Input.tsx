import { classNames } from "@punks/ui-core"
import React, { InputHTMLAttributes } from "react"

function isFunction(functionToCheck: any) {
  return (
    functionToCheck && {}.toString.call(functionToCheck) === "[object Function]"
  )
}

export type InputSize = "sm" | "md" | "lg"

export interface SelectionRange {
  start: number
  end: number
}

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  error?: boolean
  inputSize?: InputSize
  inputRef?: React.Ref<HTMLInputElement>
  onEnterKeyPressed?: () => void
  selectionRange?: SelectionRange
}

const InnerInput = ({
  className,
  error,
  inputSize,
  inputRef,
  onEnterKeyPressed,
  onKeyDown,
  selectionRange,
  ...other
}: InputProps) => {
  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (onEnterKeyPressed && e.keyCode === 13) {
      e.preventDefault()
      onEnterKeyPressed()
    }

    onKeyDown?.(e)
  }

  const ref = React.useRef<HTMLInputElement>(null)

  React.useEffect(() => {
    if (selectionRange) {
      if (!ref.current) {
        return
      }
      ref.current.setSelectionRange(selectionRange.start, selectionRange.end)
    }
  }, [selectionRange?.start, selectionRange?.end])

  const registerRef = (element: HTMLInputElement | null) => {
    if (inputRef) {
      // eslint-disable-next-line
      if ((inputRef as any).hasOwnProperty("current")) {
        ;(inputRef as any).current = element
      } else if (isFunction(inputRef)) {
        ;(inputRef as any)(element)
      } else {
        console.error("inputRef invalid", inputRef)
      }
    }
    ;(ref as any).current = element
  }

  return (
    <input
      ref={(element) => {
        registerRef(element)
      }}
      className={classNames(
        "input input-bordered w-full",
        {
          "input-lg": inputSize === "lg",
          "input-sm": inputSize === "sm",
          "input-error": error,
        },
        className
      )}
      onKeyDown={handleKeyDown}
      {...other}
    />
  )
}

export const Input = React.forwardRef<HTMLInputElement, InputProps>(
  (props, ref) => <InnerInput {...props} inputRef={ref ?? props.inputRef} />
)
