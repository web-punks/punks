import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { Input } from "./Input"

export default {
  title: "UI/Atoms/Input",
  component: Input,
  args: {
    placeholder: "Fill me...",
  },
} as ComponentMeta<typeof Input>

const Template: ComponentStory<typeof Input> = (args) => <Input {...args} />

export const Default = Template.bind({})
Default.args = {}
