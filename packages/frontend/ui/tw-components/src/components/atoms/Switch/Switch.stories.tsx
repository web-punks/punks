import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { Switch } from "./Switch"

export default {
  title: "UI/Atoms/Switch",
  component: Switch,
  args: {},
} as ComponentMeta<typeof Switch>

const Template: ComponentStory<typeof Switch> = (args) => <Switch {...args} />

export const Default = Template.bind({})
Default.args = {
  switchSize: "default",
}

export const Primary = Template.bind({})
Primary.args = {
  switchSize: "default",
  switchColor: "primary",
}

export const Small = Template.bind({})
Small.args = {
  switchSize: "small",
}

export const Large = Template.bind({})
Large.args = {
  switchSize: "large",
}
