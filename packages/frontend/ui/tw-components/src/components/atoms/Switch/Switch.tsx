import { classNames } from "@punks/ui-core"
import React, { InputHTMLAttributes } from "react"

export type SwitchType = "small" | "default" | "large"
export type SwitchColor = "default" | "primary" | "secondary" | "accent"

export interface SwitchProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: React.ReactNode
  loading?: boolean
  switchSize?: SwitchType
  switchColor?: SwitchColor
  inputRef?: React.Ref<HTMLInputElement>
}

const SwitchInner = ({
  switchSize,
  switchColor,
  inputRef,
  ...other
}: SwitchProps) => {
  const { className, ...rest } = other
  return (
    <input
      className={classNames(
        "toggle",
        {
          "toggle-sm": switchSize === "small",
          "toggle-lg": switchSize === "large",
          "toggle-primary": switchColor === "primary",
          "toggle-secondary": switchColor === "secondary",
          "toggle-accent": switchColor === "accent",
        },
        className
      )}
      type="checkbox"
      ref={inputRef}
      {...rest}
    />
  )
}

SwitchInner.defaultProps = {
  switchSize: "default",
  switchColor: "primary",
}

export const Switch = React.forwardRef<HTMLInputElement, SwitchProps>(
  (props, ref) => <SwitchInner {...props} inputRef={ref ?? props.inputRef} />
)
