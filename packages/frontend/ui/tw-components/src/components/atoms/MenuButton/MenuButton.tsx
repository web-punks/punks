import { classNames } from "@punks/ui-core"
import React from "react"

export interface MenuButtonProps {
  children: React.ReactNode
  onClick?: () => void
}

export const MenuButton = ({ children, onClick }: MenuButtonProps) => {
  return (
    <div
      className={classNames(
        "flex items-center justify-center min-w-[1.75rem] aspect-square text-text border-text rounded-[0.4rem] border",
        {
          "cursor-pointer": !!onClick,
        }
      )}
      onClick={onClick}
    >
      {children}
    </div>
  )
}
