import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { Alert } from "./Alert"

export default {
  title: "UI/Atoms/Alert",
  component: Alert,
  args: {
    children: "Lorem ipsium",
  },
} as ComponentMeta<typeof Alert>

const Template: ComponentStory<typeof Alert> = (args) => <Alert {...args} />

export const Success = Template.bind({})
Success.args = {
  type: "success",
}

export const Info = Template.bind({})
Info.args = {
  type: "info",
}

export const Warning = Template.bind({})
Warning.args = {
  type: "warning",
}

export const Error = Template.bind({})
Error.args = {
  type: "error",
}
