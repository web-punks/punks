import React from "react"
import { classNames } from "@punks/ui-core"

export type AlertSeverity = "success" | "error" | "warning" | "info"

export interface AlertProps {
  type?: AlertSeverity
  children: any
  show?: boolean
  className?: string
}

const InfoIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
    className="stroke-current flex-shrink-0 w-6 h-6"
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
    ></path>
  </svg>
)

const SuccessIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    className="stroke-current flex-shrink-0 h-6 w-6"
    fill="none"
    viewBox="0 0 24 24"
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
    />
  </svg>
)

const WarningIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    className="stroke-current flex-shrink-0 h-6 w-6"
    fill="none"
    viewBox="0 0 24 24"
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"
    />
  </svg>
)

const ErrorIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    className="stroke-current flex-shrink-0 h-6 w-6"
    fill="none"
    viewBox="0 0 24 24"
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      stroke="currentColor"
      d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"
    />
  </svg>
)

export const Alert = ({
  type = "info",
  children,
  show,
  className,
}: AlertProps) => {
  if (show === false) {
    return <></>
  }
  return (
    <div
      className={classNames(
        "alert my-4",
        {
          "alert-info": type === "info",
          "alert-success": type === "success",
          "alert-warning": type === "warning",
          "alert-error": type === "error",
        },
        className
      )}
      role="alert"
    >
      <div>
        {type === "info" && <InfoIcon />}
        {type === "success" && <SuccessIcon />}
        {type === "warning" && <WarningIcon />}
        {type === "error" && <ErrorIcon />}
        <span>{children}</span>
      </div>
    </div>
  )
}
