import React from "react"
import { getTextVariable } from "./functions"
import { TypographyProps } from "./types"
import { classNames } from "@punks/ui-core"
import styles from "./Typography.module.css"

export const Typography = ({
  component,
  children,
  className,
  font,
  textTransform,
  textAlign,
  underline,
  truncate,
  color,
  colorValue,
  variant,
  lineHeight,
  lineHeightValue,
  fontSize,
  fontSizeValue,
  weight,
  weightValue,
  style,
  innerRef,
  ...props
}: TypographyProps) => {
  const Tag = component ?? "p"
  const weights = getTextVariable(
    weight ?? variant,
    weightValue,
    "font-weight",
    {
      valSuffix: "weight",
      valPrefix: "wp-text",
    }
  )
  const fontSizes = getTextVariable(
    fontSize ?? variant,
    fontSizeValue,
    "font-size",
    {
      valSuffix: "size",
      valPrefix: "wp-text",
    }
  )
  const heights = getTextVariable(
    lineHeight ?? variant,
    lineHeightValue,
    "line-height",
    {
      valSuffix: "line-height",
      valPrefix: "wp-text",
    }
  )
  const aligns = getTextVariable(null, textAlign, "text-align")
  const transforms = getTextVariable(null, textTransform, "text-transform")
  const colors = getTextVariable(color ?? variant, colorValue, "text-color", {
    valPrefix: "color-text",
  })
  const families = getTextVariable(font, null, "font-family", {
    valPrefix: "font-family",
  })
  return (
    //@ts-ignore
    <Tag
      //@ts-ignore
      className={classNames(
        styles.root,
        truncate && styles.truncate,
        className
      )}
      style={{
        ...weights,
        ...fontSizes,
        ...heights,
        ...aligns,
        ...transforms,
        ...families,
        ...colors,
        ...style,
      }}
      {...props}
      ref={innerRef}
    >
      {children as any}
    </Tag>
  )
}

Typography.defaultProps = {
  variant: "body1",
}
