import { getTextVariable } from "../functions"

describe("getFontWeightVariable", () => {
  it("gets default value", () => {
    const weights = getTextVariable(["body1", null], null, "font-weight", {
      valSuffix: "w",
      valPrefix: "text",
    })
    expect(weights).toStrictEqual({
      "--font-weight": "var(--text-body1-w)",
    })
  })

  it("gets default value", () => {
    const weights = getTextVariable("body1", null, "font-weight", {
      valSuffix: "w",
      valPrefix: "text",
    })
    expect(weights).toStrictEqual({
      "--font-weight": "var(--text-body1-w)",
    })
  })

  it("gets font weight value override", () => {
    const weights = getTextVariable("extraBold", null, "font-weight", {
      valSuffix: "w",
      valPrefix: "text",
    })
    expect(weights).toStrictEqual({
      "--font-weight": "var(--text-extraBold-w)",
    })
  })

  it("gets custom font weight value", () => {
    const weights = getTextVariable(["body1", null], "bold", "font-weight", {
      valSuffix: "w",
      valPrefix: "text",
    })
    expect(weights).toStrictEqual({
      "--font-weight": "bold",
    })
  })

  it("gets font weight value responsive", () => {
    const weights = getTextVariable(
      ["bold", "extraBold"],
      null,
      "font-weight",
      {
        valSuffix: "w",
        valPrefix: "text",
      }
    )
    expect(weights).toStrictEqual({
      "--font-weight": "var(--text-bold-w)",
      "--font-weight_desk": "var(--text-extraBold-w)",
    })
  })
})
