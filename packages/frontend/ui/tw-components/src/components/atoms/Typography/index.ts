export { Typography } from "./Typography"
export {
  TypographyProps,
  FontFamily,
  TextAlign,
  TextProps,
  TextTransform,
} from "./types"
