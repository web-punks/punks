import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"

import { Typography } from "."

export default {
  title: "Ui/Atoms/Typography",
  component: Typography,
  argTypes: {},
} as ComponentMeta<typeof Typography>

const Template: ComponentStory<typeof Typography> = (args) => (
  <Typography {...args}>Lorem ipsum dolor sit amet</Typography>
)

export const H1 = Template.bind({})
H1.args = {
  variant: "h1",
}

export const H2 = Template.bind({})
H2.args = {
  variant: "h2",
}

export const H3 = Template.bind({})
H3.args = {
  variant: "h3",
}

export const H4 = Template.bind({})
H4.args = {
  variant: "h4",
}

export const H5 = Template.bind({})
H5.args = {
  variant: "h5",
}

export const H6 = Template.bind({})
H6.args = {
  variant: "h6",
}

export const Body1 = Template.bind({})
Body1.args = {
  variant: "body1",
}

export const Body2 = Template.bind({})
Body2.args = {
  variant: "body2",
}

export const Body3 = Template.bind({})
Body2.args = {
  variant: "body3",
}

export const Caption1 = Template.bind({})
Caption1.args = {
  variant: "caption1",
}

export const Caption2 = Template.bind({})
Caption2.args = {
  variant: "caption2",
}
