import { getTextVariable } from "../functions"

describe("getColorVariable", () => {
  it("gets default value", () => {
    const colors = getTextVariable("caption1", null, "text-color", {
      valPrefix: "color-text",
    })
    expect(colors).toStrictEqual({
      "--text-color": "var(--color-text-caption1)",
    })
  })
})
