import { FontWeight, TextVariant, ThemeColor } from "@punks/ui-core"
import { ResponsiveValue } from "../../types"

export type TextAlign = "left" | "center" | "right" | "justify"
export type TextTransform = "capitalize" | "lowercase" | "uppercase"
export type FontFamily = "default" | "secondary"

export interface TextProps {
  color?: ResponsiveValue<ThemeColor>
  colorValue?: ResponsiveValue<string>
  font?: ResponsiveValue<FontFamily>
  textTransform?: ResponsiveValue<TextTransform>
  textAlign?: ResponsiveValue<TextAlign>
  variant?: ResponsiveValue<TextVariant>
  lineHeight?: ResponsiveValue<TextVariant>
  lineHeightValue?: ResponsiveValue<string | number>
  fontSize?: ResponsiveValue<TextVariant>
  fontSizeValue?: ResponsiveValue<string>
  weight?: ResponsiveValue<FontWeight>
  weightValue?: ResponsiveValue<number | FontWeight>
  innerRef?: React.LegacyRef<any>
  underline?: ResponsiveValue<boolean>
  truncate?: ResponsiveValue<boolean>
}

export type TypographyProps = Omit<
  React.HTMLAttributes<HTMLOrSVGElement>,
  "color"
> &
  TextProps & {
    component?: keyof JSX.IntrinsicElements
  }
