import { getTextVariable } from "../functions"

describe("getFontFamilyVariable", () => {
  it("gets default value", () => {
    const families = getTextVariable("secondary", null, "font-family", {
      valPrefix: "font-family",
    })
    expect(families).toStrictEqual({
      "--font-family": "var(--font-family-secondary)",
    })
  })
})
