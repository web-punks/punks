import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { EditIcon } from "../../icons"
import { Button } from "./Button"

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI/Atoms/Button",
  component: Button,
  args: {
    children: "Button",
  },
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  // argTypes: {
  //   backgroundColor: { control: 'color' },
  // },
} as ComponentMeta<typeof Button>

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />

export const Default = Template.bind({})
Default.args = {
  color: "default",
}

export const DefaultDisabled = Template.bind({})
DefaultDisabled.args = {
  color: "default",
  disabled: true,
}

export const DefaultLoading = Template.bind({})
DefaultLoading.args = {
  color: "default",
  loading: true,
}

export const DefaultIconOnly = Template.bind({})
DefaultIconOnly.args = {
  color: "default",
  iconOnly: true,
  children: <EditIcon />,
}

export const DefaultIconOnlyLoading = Template.bind({})
DefaultIconOnlyLoading.args = {
  color: "default",
  iconOnly: true,
  loading: true,
  children: <EditIcon />,
}

export const Primary = Template.bind({})
Primary.args = {
  color: "primary",
}

export const PrimaryDisabled = Template.bind({})
PrimaryDisabled.args = {
  color: "primary",
  disabled: true,
}

export const PrimaryLoading = Template.bind({})
PrimaryLoading.args = {
  loading: true,
  color: "primary",
}

export const Secondary = Template.bind({})
Secondary.args = {
  color: "secondary",
}

export const SecondaryDisabled = Template.bind({})
SecondaryDisabled.args = {
  color: "secondary",
  disabled: true,
}

export const SecondaryLoading = Template.bind({})
SecondaryLoading.args = {
  loading: true,
  color: "secondary",
}

export const Success = Template.bind({})
Success.args = {
  color: "success",
}

export const SuccessDisabled = Template.bind({})
SuccessDisabled.args = {
  color: "success",
  disabled: true,
}

export const SuccessLoading = Template.bind({})
SuccessLoading.args = {
  loading: true,
  color: "success",
}

export const Error = Template.bind({})
Error.args = {
  color: "error",
}

export const ErrorDisabled = Template.bind({})
ErrorDisabled.args = {
  color: "error",
  disabled: true,
}

export const ErrorLoading = Template.bind({})
ErrorLoading.args = {
  loading: true,
  color: "error",
}

export const Info = Template.bind({})
Info.args = {
  color: "info",
}

export const InfoDisabled = Template.bind({})
InfoDisabled.args = {
  color: "info",
  disabled: true,
}

export const InfoLoading = Template.bind({})
InfoLoading.args = {
  loading: true,
  color: "info",
}

export const Warning = Template.bind({})
Warning.args = {
  color: "warning",
}

export const WarningDisabled = Template.bind({})
WarningDisabled.args = {
  color: "warning",
  disabled: true,
}

export const WarningLoading = Template.bind({})
WarningLoading.args = {
  loading: true,
  color: "warning",
}

// export const Accent = Template.bind({})
// Accent.args = {
//   color: "accent",
// }

// export const AccentDisabled = Template.bind({})
// AccentDisabled.args = {
//   color: "accent",
//   disabled: true,
// }

// export const AccentLoading = Template.bind({})
// AccentLoading.args = {
//   loading: true,
//   color: "accent",
// }
