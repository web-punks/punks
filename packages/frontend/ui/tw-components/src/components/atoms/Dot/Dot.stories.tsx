import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { Dot } from "./Dot"

export default {
  title: "UI/Atoms/Dot",
  component: Dot,
  args: {
    children: "x",
  },
} as ComponentMeta<typeof Dot>

const Template: ComponentStory<typeof Dot> = (args) => <Dot {...args} />

export const Default = Template.bind({})
Default.args = {
  color: "yellow",
  size: "28px",
}
