import React, { CSSProperties } from "react"
import classes from "./Dot.module.css"
import { classNames } from "@punks/ui-core"

export interface DotProps {
  color?: string
  backgroundImage?: string
  size: string
  onClick?: () => void
  className?: string
  style?: CSSProperties
  children?: React.ReactNode
}

export const Dot = ({
  color,
  size,
  onClick,
  className,
  style,
  backgroundImage,
  children,
}: DotProps) => {
  return (
    <div
      className={classes.container}
      style={{
        ["--size" as any]: size,
        ["--color" as any]: color,
        ["--background-img" as any]: backgroundImage,
      }}
    >
      <div
        className={classNames(
          classes.dot,
          { [classes.clickable]: !!onClick },
          className
        )}
        onClick={onClick}
        style={style}
      >
        {children}
      </div>
    </div>
  )
}

Dot.defaultProps = {
  size: "0.875rem",
}
