import React from "react"
import styles from "./Separator.module.css"
import { classNames } from "@punks/ui-core"

export interface SeparatorProps {
  children?: any
  className?: string
  margin?: "none" | "sm" | "md" | "lg"
}

export const Separator = ({ children, className, margin }: SeparatorProps) => {
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.marginSm]: margin === "sm",
          [styles.marginMd]: margin === "md",
          [styles.marginLg]: margin === "lg",
        },
        className
      )}
    >
      {children}
    </div>
  )
}

Separator.defaultProps = {
  margin: "sm",
}
