import React from "react"
import { Spinner } from "../Spinner"

export type SpinnerSize = "small" | "default" | "large"
export type SpinnerColor = "primary" | "white"

export interface LoaderProps {
  size?: number
  show?: boolean
  color?: SpinnerColor
}

export const Loader = ({ show, color, size }: LoaderProps) => {
  return show !== false ? <Spinner color={color} size={size} /> : <></>
}

Loader.defaultProps = {
  color: "primary",
}
