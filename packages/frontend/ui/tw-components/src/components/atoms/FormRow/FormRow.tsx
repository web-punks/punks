import { classNames } from "@punks/ui-core"
import React from "react"

export type FlexRowInlineBreakpoint = "sm" | "md" | "lg" | "xl" | "desk"

export interface FormRowProps {
  className?: string
  children: any
  inline?: boolean | FlexRowInlineBreakpoint
  noMargin?: boolean
  fullWidth?: boolean
  justify?: "start" | "center" | "end" | "between"
  items?: "start" | "center" | "end"
}

export const FormRow = ({
  className,
  children,
  inline,
  justify,
  fullWidth,
  items,
}: FormRowProps) => {
  return (
    <div
      className={classNames(
        {
          // "mb-10": noMargin !== true,
          "flex gap-4": inline === true,
          "sm:flex sm:gap-4": inline === "sm",
          "md:flex md:gap-4": inline === "md",
          "lg:flex lg:gap-4": inline === "lg",
          "xl:flex xl:gap-4": inline === "xl",
          "desk:flex desk:gap-4": inline === "desk",
          "w-full": fullWidth,
          "justify-between": justify === "between",
          "justify-center": justify === "center",
          "justify-end": justify === "end",
          "justify-start": justify === "start",
          "items-center": items === "center",
          "items-end": items === "end",
        },
        className
      )}
    >
      {children}
    </div>
  )
}
