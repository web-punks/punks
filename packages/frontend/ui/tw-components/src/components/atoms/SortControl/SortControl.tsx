import React from "react"
import { ChevronDownIcon, ChevronUpIcon } from "../../icons"
import { classNames } from "@punks/ui-core"

export interface SortControlProps {
  direction?: "asc" | "desc"
  onClick?: () => void
}

export const SortControl = ({ direction, onClick }: SortControlProps) => {
  return (
    <div
      className={classNames("flex-1 flex justify-end", {
        "cursor-pointer": !!onClick,
      })}
      onClick={onClick}
    >
      <div
        className={classNames(
          "inline-flex flex-col items-center justify-center text-slate-500"
        )}
      >
        <div className={classNames(direction === "asc" && "text-amber-500")}>
          <ChevronUpIcon width={12} height={10} />
        </div>
        <div className={classNames(direction === "desc" && "text-amber-500")}>
          <ChevronDownIcon width={12} height={10} />
        </div>
      </div>
    </div>
  )
}
