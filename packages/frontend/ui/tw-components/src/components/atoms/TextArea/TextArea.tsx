import { classNames } from "@punks/ui-core"
import React, { TextareaHTMLAttributes } from "react"

export interface TextAreaProps
  extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  touched?: boolean
  error?: boolean
}

export const TextArea = ({
  className,
  touched,
  error,
  ...other
}: TextAreaProps) => {
  return (
    <textarea
      className={classNames(
        "block w-full textarea textarea-bordered",
        {
          // "form-control-lg": lg,
          // "form-control-sm": sm,
          "invalid-input": touched && error,
        },
        className
      )}
      {...other}
    />
  )
}
