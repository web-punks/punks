import { classNames } from "@punks/ui-core"
import { ReactNode } from "react"
import React from "react"

export interface TooltipProps {
  children: ReactNode
  content?: string
  className?: string
}

export const Tooltip = ({ children, content, className }: TooltipProps) => {
  return (
    <div
      className={classNames(
        {
          tooltip: !!content,
        },
        className
      )}
      data-tip={content}
    >
      {children}
    </div>
  )
}
