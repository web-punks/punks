import React, { SelectHTMLAttributes } from "react"
import styles from "./Select.module.css"
import { classNames } from "@punks/ui-core"

export interface SelectOption {
  value: string
  label: React.ReactNode
}

export interface SelectProps extends SelectHTMLAttributes<HTMLSelectElement> {
  options: SelectOption[]
  blankOption?: boolean
  touched?: boolean
  error?: boolean
  sm?: boolean
  lg?: boolean
  inputRef?: React.Ref<HTMLSelectElement>
}

const SelectInner = ({
  className,
  options,
  blankOption,
  touched,
  error,
  sm,
  lg,
  inputRef,
  ...other
}: SelectProps) => {
  return (
    <select
      className={classNames(
        "select select-bordered w-full custom-select",
        styles.root,
        {
          "select-lg": lg,
          "select-sm": sm,
          "invalid-input border-danger": touched && error,
        },
        className
      )}
      {...other}
      ref={inputRef}
    >
      {[
        ...(blankOption
          ? [
              {
                value: "",
                label: "",
              },
            ]
          : []),
        ...(options ?? []),
      ].map((x) => (
        <option key={x.value} value={x.value}>
          {x.label}
        </option>
      ))}
    </select>
  )
}

export const Select = React.forwardRef<HTMLSelectElement, SelectProps>(
  (props, ref) => <SelectInner {...props} inputRef={ref ?? props.inputRef} />
)
