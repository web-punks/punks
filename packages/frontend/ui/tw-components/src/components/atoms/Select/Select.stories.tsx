import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { Select } from "./Select"

export default {
  title: "UI/Atoms/Select",
  component: Select,
  args: {
    options: [
      {
        value: "1",
        label: "Option 1",
      },
      {
        value: "2",
        label: "Option 2",
      },
      {
        value: "3",
        label: "Option 3",
      },
    ],
  },
} as ComponentMeta<typeof Select>

const Template: ComponentStory<typeof Select> = (args) => <Select {...args} />

export const Default = Template.bind({})
Default.args = {}
