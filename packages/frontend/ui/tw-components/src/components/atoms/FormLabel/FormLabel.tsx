import React from "react"
import { Typography } from "../Typography"
import { classNames } from "@punks/ui-core"

export interface FormLabelProps {
  children: any
  className?: string
}

export const FormLabel = ({ children, className }: FormLabelProps) => {
  return (
    <Typography variant="body1" className={classNames("pb-2", className)}>
      {children}
    </Typography>
  )
}
