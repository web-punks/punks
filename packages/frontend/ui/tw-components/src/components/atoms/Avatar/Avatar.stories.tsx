import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { Avatar } from "./Avatar"

export default {
  title: "UI/Atoms/Avatar",
  component: Avatar,
  args: {
    initials: "AB",
  },
} as ComponentMeta<typeof Avatar>

const Template: ComponentStory<typeof Avatar> = (args) => <Avatar {...args} />

export const Primary = Template.bind({})
Primary.args = {
  variant: "primary",
}

export const Secondary = Template.bind({})
Secondary.args = {
  variant: "secondary",
}

export const Info = Template.bind({})
Info.args = {
  variant: "info",
}

export const Success = Template.bind({})
Success.args = {
  variant: "success",
}

export const Warning = Template.bind({})
Warning.args = {
  variant: "warning",
}

export const Error = Template.bind({})
Error.args = {
  variant: "error",
}
