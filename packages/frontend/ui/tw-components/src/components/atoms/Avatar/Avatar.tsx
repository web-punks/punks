import React from "react"
import styles from "./Avatar.module.css"
import { ThemeColor, classNames } from "@punks/ui-core"

export interface AvatarProps {
  variant: ThemeColor
  initials?: string
  url?: string
  className?: string
}

export const Avatar = ({ variant, initials, className }: AvatarProps) => {
  return (
    <div className={classNames(styles.root, className)}>
      <span
        className={classNames(styles.label, {
          "bg-primary-light text-primary": variant === "primary",
          "bg-secondary-light text-secondary": variant === "secondary",
          "bg-info-light text-info": variant === "info",
          "bg-success-light text-success": variant === "success",
          "bg-warning-light text-warning": variant === "warning",
          "bg-error-light text-error": variant === "error",
        })}
      >
        {initials}
      </span>
    </div>
  )
}
