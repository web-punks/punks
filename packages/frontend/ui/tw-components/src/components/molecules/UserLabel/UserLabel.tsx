import React from "react"
import { Typography } from "../../atoms/Typography"

export interface UserLabelProps {
  name: React.ReactNode
  type: React.ReactNode
}

export const UserLabel = ({ name, type }: UserLabelProps) => {
  return (
    <div className="text-right">
      <Typography variant="caption1" className="mt-1">
        {type}
      </Typography>
      <Typography className="mt-1">{name}</Typography>
    </div>
  )
}
