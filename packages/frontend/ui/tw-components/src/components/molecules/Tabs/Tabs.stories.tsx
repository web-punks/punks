import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { Tabs } from "./Tabs"

export default {
  title: "UI/Molecules/Tabs",
  component: Tabs,
  args: {},
} as ComponentMeta<typeof Tabs>

const Template: ComponentStory<typeof Tabs> = (args) => (
  <div>
    <Tabs {...args} />
  </div>
)

export const Default = Template.bind({})
Default.args = {
  tabs: [
    {
      head: "Tab 1",
      content: "Tab 1 content",
    },
    {
      head: "Tab 2",
      content: "Tab 2 content",
    },
    {
      head: "Tab 3",
      content: "Tab 3 content",
    },
  ],
}
