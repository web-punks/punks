export {
  Tabs,
  TabItem as Tab,
  TabItemClasses as TabClasses,
  TabsProps,
} from "./Tabs"
