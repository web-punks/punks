import { Transition } from "@headlessui/react"
import { classNames } from "@punks/ui-core"
import React, { Fragment } from "react"

export interface TabItem {
  head: React.ReactNode
  content: React.ReactNode
  hidden?: boolean
}

export interface TabItemClasses {
  root?: string
  head?: string
  tab?: string
  content?: string
}

export interface TabsProps {
  className?: string
  tabs: TabItem[]
  defaultActiveTabIndex: number
  classes?: TabItemClasses
  fullWidth?: boolean
}

export const Tabs = ({
  className,
  classes,
  tabs,
  defaultActiveTabIndex,
  fullWidth,
}: TabsProps) => {
  const [activeTabIndex, setActiveTabIndex] = React.useState(
    defaultActiveTabIndex
  )

  return (
    <div className={classNames(classes?.root, className)}>
      <div className={classNames(classes?.head, "tabs")}>
        {tabs
          .filter((x) => x.hidden !== true)
          .map((x, i) => (
            <div
              className={classNames(
                "tab tab-bordered px-12",
                {
                  "tab-active !border-primary": i === activeTabIndex,
                  "flex-1": fullWidth,
                },
                classes?.tab
              )}
              key={i}
              onClick={() => setActiveTabIndex(i)}
            >
              {x.head}
            </div>
          ))}
      </div>
      <div>
        {tabs.map((x, i) => {
          return (
            <Transition appear show={i === activeTabIndex} key={i}>
              <Transition.Child
                as={"div"}
                className={classNames({
                  hidden: i !== activeTabIndex,
                })}
                enter="ease-out duration-[1s]"
                enterFrom="opacity-0"
                enterTo="opacity-100"
              >
                <div className={classNames("py-4", classes?.content)}>
                  {x.content}
                </div>
              </Transition.Child>
            </Transition>
          )
        })}
      </div>
    </div>
  )
}

Tabs.defaultProps = {
  defaultActiveTabIndex: 0,
}
