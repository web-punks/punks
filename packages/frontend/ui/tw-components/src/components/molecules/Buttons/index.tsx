import React from "react"
import { Button, ButtonProps } from "../../atoms/Button/Button"
import { AddCircledIcon, RemoveCircledIcon } from "../../icons"

export interface IconButtonProps
  extends Omit<ButtonProps, "children" | "size"> {}

export const AddButton = (props: IconButtonProps) => {
  return (
    <Button {...props}>
      <AddCircledIcon size={20} />
    </Button>
  )
}

export const RemoveButton = (props: IconButtonProps) => {
  return (
    <Button {...props}>
      <RemoveCircledIcon size={20} />
    </Button>
  )
}
