import { useState } from "react"
import React from "react"
import { ChevronDownIcon } from "../../icons"
import { classNames } from "@punks/ui-core"

export type CollapseClasses = {
  root?: string
  head?: string
  title?: string
  content?: string
  arrow?: string
}

export interface CollapseProps {
  head?: React.ReactNode
  children: React.ReactNode
  defaultExpanded?: boolean
  className?: string
  classes?: CollapseClasses
  open?: boolean
  onToggle?: (open: boolean) => void
}

export const Collapse = ({
  children,
  head,
  defaultExpanded,
  className,
  classes,
  open,
  onToggle,
}: CollapseProps) => {
  const [uncontrolledOpen, setOpen] = useState(defaultExpanded ?? false)
  const openState = open ?? uncontrolledOpen

  const handleToggle = (value: boolean) => {
    onToggle?.(value)
    setOpen(value)
  }

  return (
    <div
      tabIndex={0}
      className={classNames(
        "collapse",
        {
          "collapse-close": !openState,
          "collapse-open": openState,
        },
        className,
        classes?.root
      )}
    >
      <div
        className={classNames("p-0 cursor-pointer relative", classes?.head)}
        onClick={() => handleToggle(!openState)}
      >
        {head && <div>{head}</div>}
        <div className="absolute right-0 top-0">
          <div className={classes?.arrow}>
            <ChevronDownIcon
              className={classNames({
                "rotate-180": openState,
              })}
            />
          </div>
        </div>
      </div>
      <div className="collapse-content">{children}</div>
    </div>
  )
}
