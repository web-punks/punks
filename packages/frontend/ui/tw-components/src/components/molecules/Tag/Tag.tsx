import React from "react"
import { Typography } from "../../atoms/Typography"

export interface TagProps {
  children: any
  backgroundColor?: string
}

export const Tag = ({ children, backgroundColor }: TagProps) => {
  return (
    <div
      style={{
        backgroundColor,
      }}
      className="px-2 py-2 text-center rounded"
    >
      <Typography
        color="secondary"
        component="span"
        variant="body2"
        weight="bolder"
      >
        {children}
      </Typography>
    </div>
  )
}
