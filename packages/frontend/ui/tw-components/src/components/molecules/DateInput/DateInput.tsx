import React from "react"
import { Input, InputProps, SelectionRange } from "../../atoms/Input/Input"

export const toISODate = (date: Date) => {
  return `${date.getFullYear()}-${(date.getMonth() + 1)
    .toString()
    .padStart(2, "0")}-${date.getDate().toString().padStart(2, "0")}`
}

const parseIsoDate = (value: string, separator = "-") => {
  const parts = value.split("T")[0].split(separator)
  return new Date(
    Date.UTC(parseInt(parts[0]), parseInt(parts[1]) - 1, parseInt(parts[2]))
  )
}

export interface DateInputProps extends InputProps {}

const formatRawDate = (date: Date) =>
  `${date.getDate().toString().padStart(2, "0")}/${(date.getMonth() + 1)
    .toString()
    .padStart(2, "0")}/${date.getFullYear()}`

const isValidDate = (date: Date) => !isNaN(date.getTime())
const parseRawDate = (value: string) => {
  if (!value || value.length !== 10 || value.includes("_")) {
    return null
  }
  const parts = value.split("/")
  const date = new Date(
    [
      parseInt(parts[2]).toString().padStart(4, "0"),
      parseInt(parts[1]).toString().padStart(2, "0"),
      parseInt(parts[0]).toString().padStart(2, "0"),
    ].join("-")
  )
  return isValidDate(date) ? date : null
}

const isSlashPosition = (position: number) => position === 2 || position === 5
const normalizeCursorPosition = (
  position: number,
  direction: "left" | "right"
) => {
  if (!isSlashPosition(position)) {
    return position
  }

  return direction === "left" ? position - 1 : position + 1
}

const escapeDigit = (digit: string) => digit ?? "_"
const addDigit = (current: string, position: number, digit: string) => {
  return [
    [
      escapeDigit(position === 0 ? digit : current[0]),
      escapeDigit(position === 1 ? digit : current[1]),
    ].join(""),
    [
      escapeDigit(position === 2 || position === 3 ? digit : current[3]),
      escapeDigit(position === 4 ? digit : current[4]),
    ].join(""),
    [
      escapeDigit(position === 5 || position === 6 ? digit : current[6]),
      escapeDigit(position === 7 ? digit : current[7]),
      escapeDigit(position === 8 ? digit : current[8]),
      escapeDigit(position === 9 ? digit : current[9]),
    ].join(""),
  ].join("/")
}
const deleteDigit = (current: string, position: number) =>
  addDigit(current, position, "_")

const isDigit = (value: string) => {
  const reg = /[\d]/g
  return reg.test(value)
}

// borrowed from https://github.com/elter1109/react-input-date-mask/blob/master/src/ReactInputDateMask.js

const DateInputInner = ({
  id,
  value,
  defaultValue,
  onChange,
  ...props
}: DateInputProps) => {
  const [selectionRange, setSelectionRange] = React.useState<SelectionRange>()

  const getDefaultValue = () => {
    if (defaultValue) {
      return defaultValue
    }
    if (value) {
      return value
    }
    const elementValue = id
      ? (document.getElementById(id) as any)?.value
      : undefined
    if (elementValue) {
      return elementValue
    }

    return ""
  }

  const [rawValue, setRawValue] = React.useState(getDefaultValue())

  React.useEffect(() => {
    const defaultValue = getDefaultValue()
    if (defaultValue !== rawValue) {
      setRawValue(defaultValue)
    }
  }, [])

  React.useEffect(() => {
    if (value) {
      setRawValue(formatRawDate(parseIsoDate(value?.toString() ?? "")))
    }
  }, [value])

  React.useEffect(() => {
    if (rawValue === "__/__/____") {
      setSelectionRange({
        start: 0,
        end: 0,
      })
    }
  }, [rawValue])

  const handleChange = (value: string) => {
    try {
      // const date = parseRawDate(value)
      //   if (onChange && date) {
      //     onChange(toISODate(date))
      //   }
    } catch (e) {
      console.error("Error updating date")
      console.error(e)
    }
  }

  const updateRawValue = (value: string) => {
    setRawValue(value)
    handleChange(value)
  }

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Tab") {
      return
    }

    event.preventDefault()
    const selectionStart = (event.target as any).selectionStart
    if (
      (event.key === "Backspace" || event.key === "Delete") &&
      selectionStart > 0
    ) {
      const delPosition = normalizeCursorPosition(selectionStart - 1, "left")
      updateRawValue(deleteDigit(rawValue, delPosition))
      setSelectionRange({
        start: delPosition,
        end: delPosition,
      })
      return
    }
    // if (event.key === "Delete" && selectionStart < 10) {
    //   updateRawValue(deleteDigit(rawValue, selectionStart))
    //   setSelectionRange({
    //     start: selectionStart,
    //     end: selectionStart,
    //   })
    // }
    if (isDigit(event.key)) {
      updateRawValue(addDigit(rawValue, selectionStart, event.key))
      setSelectionRange({
        start: normalizeCursorPosition(selectionStart + 1, "right"),
        end: normalizeCursorPosition(selectionStart + 1, "right"),
      })
      return
    }
    if (event.key === "ArrowLeft" && selectionStart > 0) {
      setSelectionRange({
        start: normalizeCursorPosition(selectionStart - 1, "left"),
        end: normalizeCursorPosition(selectionStart - 1, "left"),
      })
      return
    }
    if (event.key === "ArrowRight") {
      setSelectionRange({
        start: normalizeCursorPosition(selectionStart + 1, "right"),
        end: normalizeCursorPosition(selectionStart + 1, "right"),
      })
      return
    }

    setSelectionRange({
      start: selectionStart,
      end: selectionStart,
    })
  }

  const handleFocus = () => {
    console.log("focus", rawValue)
    if (!rawValue || rawValue === "__/__/____") {
      updateRawValue("__/__/____")
      setTimeout(() => {
        setSelectionRange({
          start: 0,
          end: 0,
        })
      }, 100)
    }
  }

  return (
    <Input
      id={id}
      value={rawValue}
      selectionRange={selectionRange}
      onKeyDown={handleKeyDown}
      onChange={(e) => handleChange(e.target.value)}
      onFocus={() => handleFocus()}
      onTouchEnd={() => handleFocus()}
      type="tel"
      {...props}
    />
  )
}

export const DateInput = React.forwardRef<HTMLInputElement, DateInputProps>(
  (props, ref) => <DateInputInner {...props} inputRef={ref ?? props.inputRef} />
)
