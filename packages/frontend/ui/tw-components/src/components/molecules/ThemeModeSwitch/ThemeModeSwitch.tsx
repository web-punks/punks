import React from "react"
import { MenuButton } from "../../atoms/MenuButton"
import { MoonIcon, SunIcon } from "../../icons"
import { ThemeMode } from "@punks/ui-core"

export interface ThemeModeSwitchProps {
  mode: ThemeMode
  onModeToggle: () => void
}

export const ThemeModeSwitch = ({
  mode,
  onModeToggle,
}: ThemeModeSwitchProps) => {
  return (
    <MenuButton onClick={() => onModeToggle()}>
      {mode === "light" ? <MoonIcon width={14} /> : <SunIcon width={14} />}
    </MenuButton>
  )
}
