import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { ThemeModeSwitch } from "./ThemeModeSwitch"

export default {
  title: "UI/Molecules/ThemeModeSwitch",
  component: ThemeModeSwitch,
  args: {},
} as ComponentMeta<typeof ThemeModeSwitch>

const Template: ComponentStory<typeof ThemeModeSwitch> = (args) => (
  <div>
    <ThemeModeSwitch {...args} />
  </div>
)

export const Light = Template.bind({})
Light.args = {
  mode: "light",
}

export const Dark = Template.bind({})
Dark.args = {
  mode: "dark",
}
