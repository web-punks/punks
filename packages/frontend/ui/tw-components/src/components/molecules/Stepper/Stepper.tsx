import React from "react"
import styles from "./Stepper.module.css"
import { classNames } from "@punks/ui-core"

export interface StepItem {
  title?: React.ReactNode
}

export interface StepperProps {
  steps: StepItem[]
  activeStep: number
  className?: string
}

export const Stepper = ({ activeStep, steps, className }: StepperProps) => {
  return (
    <ul className={classNames("steps", styles.root, className)}>
      {steps.map((step, index) => (
        <li
          key={index}
          className={classNames("step", styles.step, {
            "step-primary": index <= activeStep,
            [styles.stepPrimary]: index <= activeStep,
          })}
        >
          {step.title && (
            <div
              className={classNames(styles.label, {
                [styles.labelNext]: activeStep < index,
              })}
            >
              {step.title}
            </div>
          )}
        </li>
      ))}
    </ul>
  )
}
