import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { CancelConfirmCta } from "./CancelConfirmCta"

export default {
  title: "UI/Molecules/CancelConfirmCta",
  component: CancelConfirmCta,
  args: {
    confirm: {
      label: "Confirm",
    },
    cancel: {
      label: "Cancel",
    },
  },
} as ComponentMeta<typeof CancelConfirmCta>

const Template: ComponentStory<typeof CancelConfirmCta> = (args) => (
  <CancelConfirmCta {...args} />
)

export const Default = Template.bind({})
Default.args = {
  size: "default",
}

export const Small = Template.bind({})
Small.args = {
  size: "small",
}

export const Large = Template.bind({})
Large.args = {
  size: "large",
}
