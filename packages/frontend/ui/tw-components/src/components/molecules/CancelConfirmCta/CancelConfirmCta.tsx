import React, { ReactNode } from "react"
import { Button, ButtonSize } from "../../atoms/Button/Button"
import { classNames } from "@punks/ui-core"

export interface CancelConfirmCta {
  label: ReactNode
  onClick: () => void
  loading?: boolean
  disabled?: boolean
  hidden?: boolean
}

export type CancelConfirmCtaAlign = "left" | "right"

export interface CancelConfirmCtaProps {
  cancel?: CancelConfirmCta
  confirm?: CancelConfirmCta
  size?: ButtonSize
  className?: string
  align?: CancelConfirmCtaAlign
}

export const CancelConfirmCta = ({
  confirm,
  cancel,
  size,
  className,
  align,
}: CancelConfirmCtaProps) => {
  return (
    <div
      className={classNames(
        "flex gap-2",
        {
          "justify-start": align === "left",
          "justify-end": align === "right",
        },
        className
      )}
    >
      {cancel && !cancel.hidden && (
        <Button
          size={size}
          disabled={cancel.disabled}
          loading={cancel.loading}
          onClick={cancel.onClick}
          color="light"
        >
          {cancel.label}
        </Button>
      )}
      {confirm && !confirm.hidden && (
        <Button
          size={size}
          disabled={confirm.disabled}
          loading={confirm.loading}
          onClick={confirm.onClick}
          color="primary"
        >
          {confirm.label}
        </Button>
      )}
    </div>
  )
}
