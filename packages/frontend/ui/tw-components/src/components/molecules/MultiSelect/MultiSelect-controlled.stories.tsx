import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { action } from "@storybook/addon-actions"
import { MultiSelect, MultiSelectData } from "./MultiSelect"
import { FunnelIcon } from "../../icons"

type ControlledMultiSelectProps<T> = {
  inputData: MultiSelectData<T>[]
  defaultSelected?: T[]
}

const ControlledMultiSelect = <T,>(props: ControlledMultiSelectProps<T>) => {
  const [selectedData, setSelectedData] = React.useState(
    props.defaultSelected ?? []
  )

  return (
    <MultiSelect
      options={props.inputData}
      value={selectedData}
      onChange={(e) => {
        action("onSelectedData")(e)
        setSelectedData(e)
      }}
      control={<FunnelIcon />}
    />
  )
}

export default {
  title: "UI/Molecules/Multiselect-Controlled",
  component: MultiSelect,
  args: {},
} as ComponentMeta<typeof MultiSelect>

const Template: ComponentStory<typeof ControlledMultiSelect> = (args) => (
  <ControlledMultiSelect {...args} />
)

export const Default = Template.bind({})
Default.args = {
  defaultSelected: [],
  inputData: [
    {
      label: "aaaaaa",
      value: "val1",
    },
    {
      label: "bbbbbb",
      value: "val2",
    },
    {
      label: "cccccc",
      value: "val3",
    },
    {
      label: "dddddd",
      value: "val4",
    },
    {
      label: "eeeeee",
      value: "val5",
    },
  ],
}
