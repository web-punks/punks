import { Listbox, Transition } from "@headlessui/react"
import { classNames } from "@punks/ui-core"
import React, { Fragment, ReactNode } from "react"
import { CheckIcon } from "../../icons"

export type MultiSelectData<T> = {
  label: string
  value: T
}
export type MultiSelectProps<T> = {
  options: MultiSelectData<T>[]
  value?: T[]
  onChange: (values: T[]) => void
  control: ReactNode
}

export function MultiSelect<T>({
  options,
  value,
  onChange,
  control,
}: MultiSelectProps<T>) {
  return (
    <div className="text-caption1">
      <Listbox value={value} onChange={onChange} multiple>
        <div className="relative">
          <Listbox.Button className="flex items-center justify-end w-full rounded-lg cursor-pointer focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300">
            {control}
          </Listbox.Button>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Listbox.Options className="absolute w-full min-w-[160px] py-1 mt-3 overflow-hidden text-base rounded-md shadow-lg bg-paper max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
              {options.map((data, index) => (
                <Listbox.Option
                  key={index}
                  className={({ active }) =>
                    classNames(
                      "relative cursor-default select-none py-2",
                      active && "bg-orange-300 font-bold"
                    )
                  }
                  value={data.value}
                >
                  {({ selected }) => (
                    <div className={classNames("px-3 flex gap-3 items-center")}>
                      <div className={classNames(!selected && "invisible")}>
                        <CheckIcon width={16} />
                      </div>
                      <span
                        className={classNames(
                          "block truncate",
                          selected ? "font-bold" : "font-normal"
                        )}
                      >
                        {data.label}
                      </span>
                    </div>
                  )}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </Listbox>
    </div>
  )
}
