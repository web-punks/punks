import React, { ReactNode } from "react"
import { MenuList } from "../../molecules/MenuList"
import { Typography } from "../../atoms/Typography"

export interface UserMenuItem {
  content: ReactNode
}

export interface UserMenuProps {
  initial: string
  menuItems: UserMenuItem[]
}

export const UserMenu = ({ initial, menuItems }: UserMenuProps) => {
  return (
    <div>
      <MenuList
        classes={{
          button: "py-2 px-[10px]",
        }}
        align="end"
        sections={[
          {
            items: menuItems.map((x) => ({ content: x.content })),
          },
        ]}
      >
        <Typography
          variant="subtitle1"
          component="span"
          textTransform="uppercase"
        >
          {initial}
        </Typography>
      </MenuList>
    </div>
  )
}
