import { Combobox, Transition } from "@headlessui/react"
import React, { Fragment, ReactNode, useRef, useState } from "react"
import { InputSize } from "../../atoms/Input/Input"
import { ChevronUpDownIcon, TickIcon } from "../../icons"
import { classNames } from "@punks/ui-core"

export interface AutocompleteItem<T> {
  id: string
  value: T
  label: ReactNode
}

export interface AutocompleteProps<T> {
  inputId?: string
  placeholder?: string
  value?: T | null
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void
  onOptionChange?: (value: T) => void
  query?: string
  onQueryChange?: (query: string) => void
  options?: AutocompleteItem<T>[]
  disabled?: boolean
  inputRef?: React.Ref<HTMLInputElement>
  inputSize?: InputSize
  className?: string
  error?: boolean
  empty?: boolean
  emptyMessage?: ReactNode
  loading?: boolean
  getId?: (item: T) => string
  getDisplayValue: (value: T) => string
  absolutePositioned?: boolean
}

export const Autocomplete = <T,>({
  inputId,
  placeholder,
  value,
  onOptionChange,
  onQueryChange,
  onChange,
  query,
  options,
  disabled,
  inputRef,
  inputSize,
  error,
  className,
  empty,
  emptyMessage,
  getId,
  getDisplayValue,
  absolutePositioned = true,
}: AutocompleteProps<T>) => {
  const [uncontrolledValue, setUncontrolledValue] = useState<T>()
  const isControlled = value !== undefined
  const [initialControlled] = useState(isControlled)

  if (initialControlled && !isControlled) {
    console.warn(
      "Autocomplete: value prop is changing from controlled to uncontrolled"
    )
  }
  if (!initialControlled && isControlled) {
    console.warn(
      "Autocomplete: value prop is changing from uncontrolled to controlled"
    )
  }

  const getValue = () => {
    if (isControlled) {
      return value
    }

    return uncontrolledValue
  }

  const getValueId = () => {
    const value = getValue()
    if (value) {
      return getId?.(value) ?? ""
    }
    return ""
  }

  const evaluateDisplayValue = () => (value ? getDisplayValue(value) : "")

  const handleChange = (value: T) => {
    if (!isControlled) {
      setUncontrolledValue(value)
    }
    onOptionChange?.(value)

    if (inputId) {
      // const rawValue = value ? getId?.(value) : ""
      // document.getElementById(inputId)?.setAttribute("value", rawValue ?? "")
      document.dispatchEvent(new Event("change"))
    }
  }

  return (
    <div>
      <input
        type="hidden"
        ref={inputRef}
        value={getValueId()}
        onChange={onChange}
        id={inputId}
      />
      <Combobox value={value} onChange={handleChange} disabled={disabled}>
        <div className="relative">
          <Combobox.Input
            placeholder={placeholder}
            onChange={(event) => onQueryChange?.(event.target.value)}
            displayValue={() => evaluateDisplayValue()}
            className={classNames(
              "input input-bordered w-full",
              {
                "input-lg": inputSize === "lg",
                "input-sm": inputSize === "sm",
                "input-error": error,
              },
              className
            )}
          />
          <Combobox.Button className="absolute inset-y-0 right-0 flex items-center pr-2">
            <ChevronUpDownIcon className="w-5 h-5" aria-hidden="true" />
          </Combobox.Button>
        </div>
        <div className="relative z-[1]">
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Combobox.Options
              className={classNames(
                "mt-1 max-h-60 w-full overflow-auto rounded-md bg-paper text-base shadow-md ring-1 ring-paper ring-opacity-5 focus:outline-none sm:text-sm",
                absolutePositioned ? "absolute" : "relative"
              )}
            >
              {empty && emptyMessage && (
                <div className="relative px-4 py-2 cursor-default select-none">
                  {emptyMessage}
                </div>
              )}
              {!empty &&
                (options ?? []).map((option) => (
                  <Combobox.Option
                    key={option.id}
                    value={option.value}
                    className="py-[0.5rem] px-4 cursor-pointer hover:bg-grey-10"
                  >
                    {option.label}
                    <TickIcon className="hidden ui-selected:block" />
                  </Combobox.Option>
                ))}
            </Combobox.Options>
          </Transition>
        </div>
      </Combobox>
    </div>
  )
}
