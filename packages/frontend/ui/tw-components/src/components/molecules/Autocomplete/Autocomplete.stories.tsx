import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { Autocomplete, AutocompleteItem } from "./Autocomplete"

interface Item {
  id: string
  displayName: string
}

const createOption = (item: Item): AutocompleteItem<Item> => ({
  id: item.id,
  value: item,
  label: item.displayName,
})

const options = [
  createOption({ id: "1", displayName: "Item 1" }),
  createOption({ id: "2", displayName: "Item 2" }),
  createOption({ id: "3", displayName: "Item 3" }),
  createOption({ id: "4", displayName: "Item 4" }),
]

export default {
  title: "UI/Molecules/Autocomplete",
  component: Autocomplete,
  args: {
    placeholder: "Search something",
    getDisplayValue: (item: Item) => item.displayName,
    options,
  },
} as ComponentMeta<typeof Autocomplete>

const Template: ComponentStory<typeof Autocomplete<Item>> = (args) => (
  <Autocomplete {...args} />
)

export const Default = Template.bind({})
Default.args = {}

export const Disabled = Template.bind({})
Disabled.args = {
  disabled: true,
}

export const Empty = Template.bind({})
Empty.args = {
  empty: true,
  emptyMessage: "No results",
}
