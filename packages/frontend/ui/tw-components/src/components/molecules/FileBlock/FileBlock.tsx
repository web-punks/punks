import { ReactNode } from "react"
import React from "react"
import { Typography } from "../../atoms/Typography"
import { Separator } from "../../atoms/Separator"
import { Tooltip } from "../../atoms/Tooltip"
import { TextVariant, classNames } from "@punks/ui-core"

export interface FileBlockClasses {
  root?: string
  icon?: string
  block?: string
}

export interface FileBlockProps {
  icon: ReactNode
  title: ReactNode
  subtitle?: ReactNode
  filePath?: string
  titleVariant?: TextVariant
  subtitleVariant?: TextVariant
  classes?: FileBlockClasses
}

export const FileBlock = ({
  icon,
  subtitle,
  filePath,
  title,
  titleVariant,
  subtitleVariant,
  classes,
}: FileBlockProps) => {
  return (
    <div className={classNames("flex items-center", classes?.root)}>
      <div className={classNames("w-[60px]", classes?.icon)}>{icon}</div>
      <div className={classNames("mx-4", classes?.block)}>
        <div>
          <Typography variant={titleVariant}>{title}</Typography>
          {subtitle && <Separator />}
        </div>
        {subtitle && (
          <div>
            <Typography variant={subtitleVariant}>{subtitle}</Typography>
          </div>
        )}
        {filePath && (
          <Tooltip content={filePath}>
            <Typography className="mt-2 max-w-[280px]" truncate>
              {filePath}
            </Typography>
          </Tooltip>
        )}
      </div>
    </div>
  )
}

FileBlock.defaultProps = {
  titleVariant: "h4",
  subtitleVariant: "body1",
}
