import React from "react"
import { ContextMenuIcon } from "../../icons"
import { classNames } from "@punks/ui-core"

export interface MenuListItem {
  content: React.ReactNode
  onClick?: () => void
}

export interface MenuSection {
  items: MenuListItem[]
}

export interface MenuClasses {
  button?: string
}

export type MenuAlign = "start" | "end"

export interface MenuListProps {
  id?: string
  className?: string
  children?: any
  sections: MenuSection[]
  classes?: MenuClasses
  align?: MenuAlign
}

export const MenuList = ({
  id,
  className,
  children,
  sections,
  classes,
  align,
}: MenuListProps) => {
  const ref = React.useRef<HTMLLabelElement>(null)
  const close = () => ref.current?.blur()

  // todo: fix status toggle on click

  return (
    <div
      className={classNames(
        "dropdown",
        {
          "dropdown-end": align === "end",
          "dropdown-start": align === "start",
        },
        className
      )}
    >
      <label
        ref={ref}
        id={id}
        tabIndex={0}
        className={classNames("btn btn-square", classes?.button)}
      >
        {children ?? <ContextMenuIcon width={24} />}
      </label>
      <ul
        className="dropdown-content menu p-2 shadow bg-base-100 rounded-box min-w-[180px] mt-1"
        aria-labelledby={id}
        tabIndex={0}
      >
        {sections.map((section, i) => (
          <React.Fragment key={i}>
            {section.items.map((item, y) => (
              <li
                key={y}
                onClick={() => {
                  close()
                  item.onClick?.()
                }}
              >
                <a>{item.content}</a>
              </li>
            ))}
          </React.Fragment>
        ))}
      </ul>
    </div>
  )
}
