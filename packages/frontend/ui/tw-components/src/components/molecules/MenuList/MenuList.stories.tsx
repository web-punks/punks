import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { MenuList } from "./MenuList"

export default {
  title: "UI/Molecules/MenuList",
  component: MenuList,
  args: {},
} as ComponentMeta<typeof MenuList>

const Template: ComponentStory<typeof MenuList> = (args) => (
  <div>
    <MenuList {...args} />
  </div>
)

export const Default = Template.bind({})
Default.args = {
  sections: [
    {
      items: [
        {
          content: "Item 1",
        },
        {
          content: "Item 2",
        },
        {
          content: "Item 3",
        },
        {
          content: "Item 4",
        },
      ],
    },
  ],
}
