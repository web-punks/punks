import { classNames } from "@punks/ui-core"
import React from "react"

export interface CardBodyProps {
  children: any
  className?: string
}

const CardBody = ({ children, className }: CardBodyProps) => {
  return (
    <div className={classNames("pt-4 pb-8 card-body", className)}>
      {children}
    </div>
  )
}

export default CardBody
