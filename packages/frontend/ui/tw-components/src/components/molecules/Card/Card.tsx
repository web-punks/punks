import React from "react"
import CardBody from "./CardBody"
import CardFooter from "./CardFooter"
import CardHead from "./CardHead"
import { CardMenu } from "./CardMenu"
import { classNames } from "@punks/ui-core"

export interface CardProps {
  children: any
  noGutters?: boolean
  className?: string
}

export const Card = ({ children, noGutters, className }: CardProps) => {
  return (
    <div
      className={classNames(
        "card bg-paper relative",
        {
          "px-2": noGutters !== true,
        },
        className
      )}
    >
      {children}
    </div>
  )
}

Card.Head = CardHead
Card.Body = CardBody
Card.Footer = CardFooter
Card.Menu = CardMenu
