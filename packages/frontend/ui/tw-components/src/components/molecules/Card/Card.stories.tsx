import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { Card } from "./Card"

export default {
  title: "UI/Molecules/Card",
  component: Card,
  args: {},
} as ComponentMeta<typeof Card>

const Template: ComponentStory<typeof Card> = (args) => (
  <div className="bg-background p-8">
    <div className="max-w-[400px]">
      <Card {...args} />
    </div>
  </div>
)

export const Default = Template.bind({})
Default.args = {
  children: (
    <>
      <Card.Head title="Hello folks!" />
      <Card.Body>
        <p>Lorem ipsium</p>
        <p>Lorem ipsium</p>
        <p>Lorem ipsium</p>
      </Card.Body>
    </>
  ),
}
