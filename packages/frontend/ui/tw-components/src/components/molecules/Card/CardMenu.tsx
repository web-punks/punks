import React from "react"
import { Card } from "./Card"
import { classNames } from "@punks/ui-core"

export type CardMenuDirection = "horizontal" | "vertical"

export interface CardMenuProps {
  controls: React.ReactNode[]
  direction: CardMenuDirection
}

export const CardMenu = ({ controls, direction }: CardMenuProps) => {
  return (
    <Card
      className={classNames("flex", {
        "flex-col px-2 py-3": direction === "vertical",
        "flex px-3 py-2": direction === "horizontal",
      })}
    >
      {controls?.map((x, i) => (
        <div className="my-2" key={i}>
          {x}
        </div>
      ))}
    </Card>
  )
}

CardMenu.defaultProps = {
  direction: "vertical",
}
