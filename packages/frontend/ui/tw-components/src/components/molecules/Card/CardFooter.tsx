import { classNames } from "@punks/ui-core"
import React from "react"

export interface CardFooterProps {
  children: any
  className?: string
}

const CardFooter = ({ children, className }: CardFooterProps) => {
  return (
    <div
      className={classNames(
        "border-border border-t-[1px] border-solid",
        className
      )}
    >
      {children}
    </div>
  )
}

export default CardFooter
