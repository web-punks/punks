import React from "react"
import { Typography } from "../../atoms/Typography"
import { classNames } from "@punks/ui-core"

export interface CardHeadProps {
  title?: React.ReactNode
  subtitle?: React.ReactNode
  toolbar?: React.ReactNode
  children?: any
  bordered: boolean
}

const CardHead = ({ title, subtitle, toolbar, bordered }: CardHeadProps) => {
  return (
    <div
      className={classNames("py-8 px-[30px] flex justify-between", {
        "border-border border-b-[1px] border-solid": bordered,
      })}
    >
      <div className="card-title items-start flex-col">
        {title && (
          <Typography variant="h6" className="mb-1 ">
            {title}
          </Typography>
        )}
        {subtitle && (
          <Typography variant="caption1" className="mt-1">
            {subtitle}
          </Typography>
        )}
      </div>
      {toolbar && <div>{toolbar}</div>}
    </div>
  )
}

CardHead.defaultProps = {
  bordered: true,
}

export default CardHead
