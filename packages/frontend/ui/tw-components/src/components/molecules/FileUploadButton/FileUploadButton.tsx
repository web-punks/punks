import React, { useRef } from "react"
import { Button, ButtonColor } from "../../atoms/Button/Button"

export interface FileUploadButtonProps {
  children: any
  accept?: string
  onFileSelected: (file: File) => void
  uploading?: boolean
  color?: ButtonColor
  iconOnly?: boolean
}

export const FileUploadButton = ({
  children,
  accept,
  onFileSelected,
  uploading,
  color,
  iconOnly,
}: FileUploadButtonProps) => {
  const ref = useRef<HTMLInputElement>(null)
  const handleClick = () => {
    ref.current?.click()
  }

  const handleFileSelected = (file?: File) => {
    if (file) {
      onFileSelected(file)
    }
  }

  return (
    <>
      <input
        type="file"
        style={{
          display: "none",
        }}
        accept={accept}
        ref={ref}
        onChange={(e) => handleFileSelected(e.target.files?.[0])}
      />
      <Button
        onClick={(e) => {
          e.preventDefault()
          handleClick()
        }}
        loading={uploading}
        color={color}
        iconOnly={iconOnly}
      >
        {children}
      </Button>
    </>
  )
}
