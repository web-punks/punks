import React from "react"
import { classNames } from "@punks/ui-core"

export interface CustomContainerProps {
  children: any
  fluid?: boolean
  className?: string
}

const getContainerClass = (props: CustomContainerProps) => {
  if (props.fluid) {
    return ""
  }

  return "container mx-auto"
}

export const Container = (props: CustomContainerProps) => {
  return (
    <div className={classNames(getContainerClass(props), props.className)}>
      {props.children}
    </div>
  )
}
