import { CustomCol } from "./Col"
import { CustomRow } from "./Row"

export const FlexRow = {
  Container: CustomRow,
  Col: CustomCol,
}
