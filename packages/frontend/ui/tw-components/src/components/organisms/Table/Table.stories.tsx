import React, { useState } from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { Table, TableProps } from "./Table"
import fakeData from "./MOCK_DATA.json"
import _ from "lodash"
import {
  TableColumnFilterType,
  TableSortingValue,
  TableColumnsType,
} from "./types"

type TRecord = typeof fakeData[0]
const columns: TableColumnsType<TRecord> = [
  {
    key: "name",
    title: "Nome",
    render: (record) => record.name,
  },
  {
    key: "role",
    title: "Ruolo",
    render: (record) => record.role,
  },
  {
    key: "age",
    title: "Anni",
    render: (record) => record.age,
  },
]

export default {
  title: "UI/Organisms/Table",
  component: Table,
  args: {
    columns,
  },
} as ComponentMeta<typeof Table>

const Template: ComponentStory<typeof Table> = (args) => <Table {...args} />

export const Default = Template.bind({})
Default.args = {
  dataSource: fakeData,
}

export const Loading = Template.bind({})
Loading.args = {
  loading: true,
}

export const Empty = Template.bind({})
Empty.args = {
  loading: false,
  dataSource: [],
  emptyContent: <div>Nessun dato trovato</div>,
}

type SortingType = "name" | "role" | "age"

const filterableColumns: TableColumnsType<TRecord, unknown, SortingType> = [
  {
    ...columns[0],
    filter: {
      type: TableColumnFilterType.Text,
    },
    sorting: {
      defaultDirection: "asc",
      value: "name",
    },
  },
  {
    ...columns[1],
    filter: {
      type: TableColumnFilterType.MultipleSelect,
      options: [
        {
          label: "Developer",
          value: "Developer",
        },
        {
          label: "Admin",
          value: "Admin",
        },
      ],
    },
    sorting: {
      defaultDirection: "asc",
      value: "role",
    },
  },
  {
    ...columns[2],
    sorting: {
      enabled: false,
      defaultDirection: "asc",
      value: "age",
    },
    filter: {
      type: TableColumnFilterType.SingleSelect,
      options: [
        {
          label: "Developer",
          value: "Developer",
        },
        {
          label: "Admin",
          value: "Admin",
        },
      ],
    },
  },
]

type CustomTableFilters = {
  name?: string[]
  age?: string[]
  role?: string[]
}

enum TableSortingType {
  Name = "name",
  Role = "role",
  Age = "age",
}

const ControlledTable = (
  args: TableProps<typeof fakeData[0], TableSortingType>
) => {
  const [filters, setFilters] = useState<CustomTableFilters>({})
  const [sorting, setSorting] = useState<TableSortingValue<TableSortingType>>()
  const [pageIndex, setPageIndex] = useState(args?.paging?.index ?? 0)

  let data = args.dataSource

  // if (filters) {
  //   data = data?.filter((item) => {
  //     return (
  //       (filters?.age?.some((selected) =>
  //         new RegExp(selected, "i").test(item.age.toString())
  //       ) ??
  //         true) &&
  //       (filters?.name?.some((selected) =>
  //         new RegExp(selected, "i").test(item.name)
  //       ) ??
  //         true) &&
  //       (filters?.role?.some((selected) =>
  //         new RegExp(selected, "i").test(item.role)
  //       ) ??
  //         true)
  //     )
  //   })
  // }
  if (sorting) {
    data = _.orderBy(data, sorting.column, sorting.direction)
  }

  if (args?.paging) {
    data = data?.slice(
      pageIndex * args.paging.size,
      (pageIndex + 1) * args.paging.size
    )
  }
  const pageCount = Math.ceil(
    (args.dataSource?.length ?? 0) / (args.paging?.size ?? 10)
  )

  return (
    <Table
      {...args}
      paging={
        args.paging
          ? {
              index: pageIndex,
              size: args.paging?.size,
              totPages: pageCount,
              pageItems: data?.length ?? 0,
              totItems: args.dataSource?.length ?? 0,
              onPagingChange: (i, size) => {
                setPageIndex(i)
              },
            }
          : undefined
      }
      sorting={{
        value: sorting,
        onChange: (value) => setSorting(value),
      }}
      dataSource={data}
      // filtering={{
      //   state: args.filtering?.state ?? {},
      //   onChange: (e) => {
      //     setState({
      //       filters: e.filters,
      //       paging: {
      //         index: e.paging?.index ?? 0,
      //         size: e.paging?.size ?? 1,
      //       },
      //       sorting:
      //         e.sorting?.sortColumn != null
      //           ? {
      //               direction: e.sorting?.direction,
      //               sortColumn: e.sorting?.sortColumn,
      //             }
      //           : undefined,
      //     })
      //   },
      // }}
    />
  )
}
const ControlledTableTemplate: ComponentStory<typeof ControlledTable> = (
  args
) => <ControlledTable {...args} />
export const Filterable = ControlledTableTemplate.bind({})

Filterable.args = {
  dataSource: fakeData,
  columns: filterableColumns as any,
  paging: {
    index: 0,
    size: 250,
    totPages: 2,
    pageItems: 50,
  },
}

export const Selectable = Template.bind({})
Selectable.args = {
  dataSource: fakeData,
  selection: {
    selected: [fakeData[0], fakeData[2]],
    onSelectionChange: (selected) => console.log(selected),
  },
}
