import React from "react"
import {
  TableClasses,
  TableColumnsType,
  TableOptions,
  TablePaging,
  TableSelection,
  TableSorting,
} from "./types"
import ReactTable from "../../../integrations/tables/ReactTable"
import { classNames } from "@punks/ui-core"
import { Loader } from "../../atoms/Loader"

export interface TableProps<RecordType, SortingType> {
  dataSource?: RecordType[]
  loading?: boolean
  columns: TableColumnsType<RecordType>
  emptyContent?: React.ReactNode
  classes?: TableClasses
  options?: TableOptions
  paging?: TablePaging
  selection?: TableSelection<RecordType>
  sorting?: TableSorting<SortingType>
}

export const Table = <T, TSorting>({
  columns,
  classes,
  dataSource,
  emptyContent,
  loading,
  options,
  paging,
  sorting,
}: TableProps<T, TSorting>) => {
  return (
    <div className="overflow-x-auto">
      <ReactTable
        pagination={
          !!paging
            ? {
                totPages: paging.totPages,
                totItems: paging.totItems,
                pageItems: paging.pageItems,
                initialState: {
                  pageIndex: paging.index,
                  pageSize: paging.size,
                },
                state: {
                  pageIndex: paging.index,
                  pageSize: paging.size,
                },
                onPageChange: (state) =>
                  paging.onPagingChange?.(state.pageIndex, state.pageSize),
              }
            : undefined
        }
        sorting={sorting as TableSorting<unknown>}
        classes={{
          head: "border-b-[1px] border-border",
          header: " text-left",
          row: classNames(
            {
              "border-b-[1px] border-border": !options?.noRowsSeparator,
            },
            classes?.row
          ),
          cell: classNames("", classes?.cell),
          cellContent: classNames("py-6", classes?.cellContent),
        }}
        emptyContent={emptyContent}
        data={dataSource ?? []}
        loading={loading}
        loader={
          <div className="py-4">
            <Loader />
          </div>
        }
        columns={columns}
        options={options}
      />
    </div>
  )
}
