export enum TableColumnFilterType {
  Text = "text",
  SingleSelect = "singleSelect",
  MultipleSelect = "multipleSelect",
}

export type TableColumnFilterOption<TValue> = {
  label: string
  value: TValue
}

export type TableFilterBase<TValue> = {
  enabled?: boolean
  options?: TableColumnFilterOption<TValue>[]
}

export interface TableTextFilter<TValue> extends TableFilterBase<TValue> {
  type: TableColumnFilterType.Text
  onChange?: (value: string) => void
}

export interface TableSingleSelectFilter<TValue>
  extends TableFilterBase<TValue> {
  type: TableColumnFilterType.SingleSelect
  onChange?: (value: TValue) => void
  selectedValue?: TValue
}

export interface TableMultiSelectFilter<TValue>
  extends TableFilterBase<TValue> {
  type: TableColumnFilterType.MultipleSelect
  onChange?: (values: TValue[]) => void
  selectedValues?: TValue[]
}

export type TableColumnFilter<TValue> =
  | TableTextFilter<TValue>
  | TableSingleSelectFilter<TValue>
  | TableMultiSelectFilter<TValue>

export type TableColumnSortingDirection = "asc" | "desc"

export type TableColumnSorting<T> = {
  enabled?: boolean
  defaultDirection: TableColumnSortingDirection
  value: T
}

export interface TableColumnType<RecordType, FilterType, SortingType> {
  title: React.ReactNode
  key: string
  span?: number
  minWidth?: number | string
  render?: (record: RecordType, index: number) => React.ReactNode
  hidden?: boolean
  filter?: TableColumnFilter<FilterType>
  sorting?: TableColumnSorting<SortingType>
}

export declare type TableColumnsType<
  RecordType = unknown,
  FilterType = unknown,
  SortingType = unknown
> = TableColumnType<RecordType, FilterType, SortingType>[]

export type TablePaging = {
  index: number
  size: number
  pageItems: number
  totPages?: number
  totItems?: number
  onPagingChange?: (index: number, size: number) => void
}

export type TableSortingValue<SortingType> = {
  column: SortingType
  direction: TableColumnSortingDirection
}

export type TableSorting<SortingType> = {
  value?: TableSortingValue<SortingType>
  onChange: (value?: TableSortingValue<SortingType>) => void
}

export type TableSelection<RecordType> = {
  selected: RecordType[]
  onSelectionChange: (selected: RecordType[]) => void
}

export interface TableOptions {
  columnsMinWidth?: number
  noRowsSeparator?: boolean
}

export interface TableClasses {
  row?: string
  cell?: string
  cellContent?: string
}
