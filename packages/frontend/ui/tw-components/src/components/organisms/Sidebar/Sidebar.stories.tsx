import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"

import { Sidebar } from "."

export default {
  title: "Ui/Organisms/Sidebar",
  component: Sidebar,
  argTypes: {},
} as ComponentMeta<typeof Sidebar>

const Template: ComponentStory<typeof Sidebar> = (args) => <Sidebar {...args} />

export const SidebarDefault = Template.bind({})

const Icon1 = () => (
  <svg width="30" height="30" viewBox="0 0 30 30" fill="none">
    <path
      d="M16.3425 10.5H13.6575C12.7421 10.5 12 11.2421 12 12.1575V26.8425C12 27.7579 12.7421 28.5 13.6575 28.5H16.3425C17.2579 28.5 18 27.7579 18 26.8425V12.1575C18 11.2421 17.2579 10.5 16.3425 10.5Z"
      stroke="currentColor"
      strokeWidth="2.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M26.8425 1.5H24.1575C23.2421 1.5 22.5 2.24209 22.5 3.1575V26.8425C22.5 27.7579 23.2421 28.5 24.1575 28.5H26.8425C27.7579 28.5 28.5 27.7579 28.5 26.8425V3.1575C28.5 2.24209 27.7579 1.5 26.8425 1.5Z"
      stroke="currentColor"
      strokeWidth="2.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M4.5 28.5C6.15685 28.5 7.5 27.1569 7.5 25.5C7.5 23.8431 6.15685 22.5 4.5 22.5C2.84315 22.5 1.5 23.8431 1.5 25.5C1.5 27.1569 2.84315 28.5 4.5 28.5Z"
      stroke="currentColor"
      strokeWidth="2.25"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

SidebarDefault.args = {
  logo: (
    <img
      src="https://a.storyblok.com/f/196735/601x601/29667fe0ad/fav10.png"
      style={{
        maxWidth: "100%",
      }}
    />
  ),
  navItems: [
    {
      icon: <Icon1 />,
      label: "Item 1",
    },
    {
      icon: <Icon1 />,
      label: "Item 2",
    },
    {
      icon: <Icon1 />,
      label: "Item 3",
    },
    {
      icon: <Icon1 />,
      label: "Item 4",
    },
    {
      icon: <Icon1 />,
      label: "Item 5",
    },
    {
      icon: <Icon1 />,
      label: "Item 6",
    },
  ],
  hPage: true,
}

export const SidebarCustomRender = Template.bind({})
SidebarCustomRender.args = {
  logo: (
    <img
      src="https://a.storyblok.com/f/196735/601x601/29667fe0ad/fav10.png"
      style={{
        maxWidth: "100%",
      }}
    />
  ),
  navItems: [
    {
      icon: <Icon1 />,
      label: "Item 1",
      render: (item) => <div style={{ color: "blue" }}>{item}</div>,
    },
    {
      icon: <Icon1 />,
      label: "Item 2",
      render: (item) => <div style={{ color: "blue" }}>{item}</div>,
    },
    {
      icon: <Icon1 />,
      label: "Item 3",
      render: (item) => <div style={{ color: "blue" }}>{item}</div>,
    },
    {
      icon: <Icon1 />,
      label: "Item 4",
      render: (item) => <div style={{ color: "blue" }}>{item}</div>,
    },
  ],
  hPage: true,
}
