import React from "react"
import styles from "./SidebarNavItem.module.css"
import { classNames } from "@punks/ui-core"

export interface SidebarNavItemProps {
  icon: React.ReactNode
  label: string
  onClick?: () => void
  className?: string
  selected?: boolean
  clickable?: boolean
}

export const SidebarNavItem = ({
  icon,
  label,
  selected,
  onClick,
  className,
  clickable,
}: SidebarNavItemProps) => {
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.selected]: selected,
          [styles.clickable]: clickable,
        },
        className
      )}
      onClick={onClick}
    >
      <div>{icon}</div>
      <div className="mt-2">{label}</div>
    </div>
  )
}
