import React, { Fragment } from "react"
import styles from "./Sidebar.module.css"
import { SidebarNavItem } from "./SidebarNavItem"
import { classNames } from "@punks/ui-core"

export interface SidebarNavItem {
  icon: React.ReactNode
  label: string
  onClick?: () => void
  render?: (item: React.ReactNode) => React.ReactNode
  selected?: boolean
  clickable?: boolean
}

export interface SidebarProps {
  logo?: React.ReactNode
  navItems?: SidebarNavItem[]
  className?: string
  hPage?: boolean
}

export const Sidebar = ({ logo, navItems, hPage, className }: SidebarProps) => {
  return (
    <div
      className={classNames(
        styles.root,
        {
          [styles.hPage]: hPage,
        },
        className
      )}
    >
      <div className={styles.logo}>{logo}</div>
      <div className={styles.navItems}>
        {navItems?.map((item, index) =>
          item.render ? (
            <Fragment key={index}>
              {item.render(
                <SidebarNavItem
                  icon={item.icon}
                  label={item.label}
                  selected={item.selected}
                  clickable={item.clickable}
                  onClick={item.onClick}
                  className={styles.navItem}
                />
              )}
            </Fragment>
          ) : (
            <SidebarNavItem
              key={index}
              icon={item.icon}
              label={item.label}
              selected={item.selected}
              clickable={item.clickable}
              onClick={item.onClick}
              className={styles.navItem}
            />
          )
        )}
      </div>
      <div></div>
    </div>
  )
}

export default Sidebar
