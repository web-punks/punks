import React from "react"
import { Table } from "../Table"
import {
  TableClasses,
  TableColumnsType,
  TablePaging,
  TableSorting,
} from "../Table/types"
import { Card } from "../../molecules/Card"

export interface CardTableProps<RecordType, SortingType> {
  className?: string
  title: React.ReactNode
  subtitle?: React.ReactNode
  dataSource?: RecordType[]
  loading?: boolean
  columns: TableColumnsType<RecordType>
  sorting?: TableSorting<SortingType>
  paging?: TablePaging
  emptyContent?: React.ReactNode
  classes?: TableClasses
  toolbar?: React.ReactNode
  controls?: React.ReactNode
  bottomContent?: React.ReactNode
}

export function CardTable<T, SortingType>({
  className,
  title,
  subtitle,
  columns,
  dataSource,
  toolbar,
  controls,
  loading,
  emptyContent,
  classes,
  bottomContent,
  sorting,
  paging,
}: CardTableProps<T, SortingType>) {
  return (
    <Card className={className}>
      <Card.Head
        bordered={false}
        title={title}
        subtitle={subtitle}
        toolbar={toolbar}
      />
      {controls && <div className="pt-4 px-8">{controls}</div>}
      <Card.Body>
        <Table
          dataSource={dataSource}
          loading={loading}
          columns={columns}
          emptyContent={emptyContent}
          classes={classes}
          options={{
            columnsMinWidth: 90,
          }}
          sorting={sorting}
          paging={paging}
        />
        {bottomContent}
      </Card.Body>
    </Card>
  )
}
