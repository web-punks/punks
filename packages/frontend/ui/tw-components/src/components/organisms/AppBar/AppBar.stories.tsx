import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"

import { AppBar } from "."

export default {
  title: "Ui/Organisms/AppBar",
  component: AppBar,
  argTypes: {},
} as ComponentMeta<typeof AppBar>

const Template: ComponentStory<typeof AppBar> = (args) => <AppBar {...args} />

export const AppBarSmall = Template.bind({})
AppBarSmall.args = {
  children: <div>content</div>,
  size: "small",
}

export const AppBarMedium = Template.bind({})
AppBarMedium.args = {
  children: <div>content</div>,
  size: "medium",
}

export const AppBarLarge = Template.bind({})
AppBarLarge.args = {
  children: <div>content</div>,
  size: "large",
}
