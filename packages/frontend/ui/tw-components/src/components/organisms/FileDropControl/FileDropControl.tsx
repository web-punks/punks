import React from "react"
import { Accept, useDropzone } from "react-dropzone"
import styles from "./FileDropControl.module.css"
import { Loader } from "../../atoms/Loader"
import { Typography } from "../../atoms/Typography"
import { DeleteIcon } from "../../icons"
import { getFileMB } from "../../utils/files"
import { classNames } from "@punks/ui-core"

export interface FileDropControlClasses {
  root?: string
  icon?: string
  content?: string
}

export interface FileDropControlProps {
  uploading?: boolean
  accept: Accept
  onFileSelected?: (file: File) => void
  showRemoveControl?: boolean
  onFileRemoved?: () => void
  uploadAction?: (file: File) => Promise<void>
  icon?: React.ReactNode
  title: React.ReactNode
  subtitle?: React.ReactNode
  maxFileMB?: number
  error?: boolean
  classes?: FileDropControlClasses
}

type UploadActionState = "idle" | "uploading" | "success" | "error"

export const FileDropControl = ({
  uploading,
  icon,
  title,
  subtitle,
  accept,
  onFileSelected,
  showRemoveControl,
  onFileRemoved,
  uploadAction,
  maxFileMB,
  error,
  classes,
}: FileDropControlProps) => {
  const [invalidFileSize, setInvalidFileSize] = React.useState(false)
  const [uploadActionState, setUploadActionState] =
    React.useState<UploadActionState>("idle")

  const handleUpload = async (
    file: File,
    action: (file: File) => Promise<void>
  ) => {
    try {
      setUploadActionState("uploading")
      await action(file)
      setUploadActionState("success")
    } catch (e) {
      setUploadActionState("error")
    }
  }

  const handleAddFile = (file: File) => {
    if (maxFileMB) {
      const mb = getFileMB(file)
      console.log("mb", mb)
      if (mb > maxFileMB) {
        setInvalidFileSize(true)
        return
      }
    }
    setInvalidFileSize(false)
    onFileSelected?.(file)

    if (uploadAction) {
      handleUpload(file, uploadAction)
    }
  }

  const handleRemoveFile = () => {
    setUploadActionState("idle")
    onFileRemoved?.()
  }

  const { getRootProps, getInputProps } = useDropzone({
    accept,
    onDropAccepted: (files) => handleAddFile(files[0]),
    disabled: uploading,
  })

  return (
    <section>
      <div {...getRootProps({ className: "dropzone" })}>
        <input {...getInputProps()} />
        <div
          className={classNames(
            styles.control,
            {
              [styles.error]: error,
            },
            classes?.root
          )}
        >
          {icon && (
            <div
              className={classNames(
                "mb-6 flex items-center justify-center",
                classes?.icon
              )}
            >
              {icon}
            </div>
          )}
          {uploading || uploadActionState === "uploading" ? (
            <div className="flex justify-center">
              <Loader color="primary" />
            </div>
          ) : (
            <div className={classes?.content}>
              {title && (
                <Typography
                  variant="subtitle2"
                  component="div"
                  className="flex items-center justify-center"
                >
                  {title}
                  {showRemoveControl && (
                    <div
                      className="clickable ml-4"
                      onClick={(e) => {
                        e.preventDefault()
                        handleRemoveFile()
                      }}
                    >
                      {<DeleteIcon />}
                    </div>
                  )}
                </Typography>
              )}
              {subtitle && (
                <Typography variant="caption1" className="mt-4" component="div">
                  {subtitle}
                </Typography>
              )}
              {invalidFileSize && (
                <Typography color="error" className="mt-4">
                  Il file selezionato supera le dimensioni consentite
                </Typography>
              )}
            </div>
          )}
        </div>
      </div>
    </section>
  )
}
