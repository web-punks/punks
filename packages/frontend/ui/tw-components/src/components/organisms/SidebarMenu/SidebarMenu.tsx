import React from "react"
import SidebarLinkWithLogo from "./SidebarLinkWithLogo"
import SidebarMenuItem from "./SidebarMenuItem"
import SidebarMenuSection from "./SidebarMenuSection"

export interface SidebarMenuProps {
  children: any
}

export const SidebarMenu = ({ children }: SidebarMenuProps) => {
  return <div>{children}</div>
}

SidebarMenu.Item = SidebarMenuItem
SidebarMenu.Section = SidebarMenuSection
SidebarMenu.LinkWithLogo = SidebarLinkWithLogo
