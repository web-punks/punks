import { classNames } from "@punks/ui-core"
import React from "react"
import { Dot } from "../../atoms/Dot"
import { Typography } from "../../atoms/Typography"

type SidebarMenuItemProps = {
  children?: React.ReactNode
  to: string
  title: string
  icon?: string
  fontIcon?: string
  hasBullet?: boolean
  arrow?: boolean
  indent?: boolean
  className?: string
  classes?: {
    link?: string
    icon?: string
    title?: string
  }
}

const SidebarMenuItem: React.FC<SidebarMenuItemProps> = ({
  children,
  to,
  title,
  icon,
  fontIcon,
  className,
  classes,
  hasBullet = false,
  arrow = false,
}) => {
  return (
    <div className={classNames("py-3 px-6", className)}>
      <div
        className={classNames("menu-link without-sub", classes?.link, {
          active: false,
        })}
      >
        {hasBullet && (
          <span className="menu-bullet">
            <Dot />
          </span>
        )}
        {icon && (
          <span className={classNames("menu-icon", classes?.icon)}>
            {/* <KTSVG path={icon} className="svg-icon-2" /> */}
          </span>
        )}
        {fontIcon && <i className={classNames("bi fs-3", fontIcon)}></i>}
        <Typography
          variant="body1"
          className={classNames("!text-[#9899ac]", classes?.title)}
        >
          {title}
        </Typography>
        {arrow && <span className="menu-arrow"></span>}
      </div>
      {children}
    </div>
  )
}

export default SidebarMenuItem
