import React from "react"
import { Typography } from "../../atoms/Typography"

export interface SidebarMenuSectionProps {
  children: any
}

const SidebarMenuSection = ({ children }: SidebarMenuSectionProps) => {
  return (
    <div className="pt-8 pb-2 px-6">
      <Typography
        textTransform="uppercase"
        variant="caption2"
        className="tracking-wider menu-section text-muted text-uppercase fs-8 ls-1 !text-[#4c4e6f]"
      >
        {children}
      </Typography>
    </div>
  )
}

export default SidebarMenuSection
