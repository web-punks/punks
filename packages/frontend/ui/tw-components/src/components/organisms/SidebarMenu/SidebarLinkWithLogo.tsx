import { classNames } from "@punks/ui-core"
import React from "react"
import { Typography } from "../../atoms/Typography"

export type SidebarLinkWithLogoProps = {
  icon: React.ReactNode
  text: string
  url: string
  isCollapsed: boolean
  isSelected?: boolean
}

const SidebarLinkWithLogo = ({
  url,
  icon,
  text,
  isCollapsed,
  isSelected,
}: SidebarLinkWithLogoProps) => {
  return (
    <div
      className={classNames(
        "[--h-icon-collapsed:1.75rem] [--h-icon-open:24px]",
        "block items-center w-full overflow-hidden py-3 [transition:padding_var(--sidebar-duration)_var(--sidebar-function)] relative",
        !isSelected && "opacity-50 hover:opacity-80"
      )}
    >
      {isSelected && (
        <div
          className={classNames(
            "w-1 h-[2.25rem] rounded-l-md bg-paper absolute top-0 bottom-0 my-auto right-0",
            ""
          )}
        ></div>
      )}
      <Typography
        component="div"
        fontSizeValue={isCollapsed ? "1.75rem" : "1.5rem"}
        className={classNames(
          isCollapsed
            ? "h-[var(--h-icon-collapsed)] w-7 translate-x-[calc(var(--sidebar-close-width)/2-50%)]"
            : "h-[var(--h-icon-open)] w-6 translate-x-[3.5rem]",
          "[transition:font-size_var(--sidebar-duration)_var(--sidebar-function),width_var(--sidebar-duration)_var(--sidebar-function),height_var(--sidebar-duration)_var(--sidebar-function),transform_var(--sidebar-duration)_var(--sidebar-function)]",
          isSelected ? "!text-negative-contrast" : "!text-negative-contrast"
        )}
      >
        {icon}
      </Typography>
      <Typography
        variant={isSelected ? "body1" : "body1"}
        className={classNames(
          isCollapsed
            ? "translate-x-[calc(var(--sidebar-close-width)/2-50%)] -scale-0"
            : "-translate-y-[calc(50%+var(--h-icon-open)/2)] translate-x-[calc(3.5rem+var(--h-icon-open)+1.75rem)]",
          "text-center [transition:font-size_var(--sidebar-duration)_var(--sidebar-function),transform_var(--sidebar-duration)_var(--sidebar-function)] absolute",
          isSelected ? "!text-negative-contrast" : "!text-negative-contrast"
        )}
      >
        {text}
      </Typography>
    </div>
  )
}

export default SidebarLinkWithLogo
