import React, { useState } from "react"
import classes from "./SplashScreen.module.css"
import { FlexRow } from "../../spacings"
import { setCssVar } from "@punks/ui-core"

export interface SplashScreenProps {
  image?: string
  logoCompany?: string
  logoApp?: string
  children: any
}

export const SplashScreen = ({
  image,
  logoApp,
  logoCompany,
  children,
}: SplashScreenProps) => {
  return (
    <FlexRow.Container
      noGutters
      h100
      style={{
        ...setCssVar("--bg-image", image ?? ""),
      }}
      className="bg-paper"
    >
      <FlexRow.Col xs={0} md={5} className={classes.bg}></FlexRow.Col>
      <FlexRow.Col xs={12} md={7}>
        <div
          className={`relative h-100 flex justify-center items-center ${classes.formCol}`}
        >
          <div className={`block md:hidden ${classes.formBgTop}`}></div>
          <div className={classes.form}>
            {(logoCompany || logoApp) && (
              <div className="app flex justify-center mb-[1rem] gap-4">
                {logoCompany && (
                  <div>
                    <img src={logoCompany} width={180} />
                  </div>
                )}
                {logoApp && (
                  <div className="flex justify-center">
                    <img src={logoApp} width={480} />
                  </div>
                )}
              </div>
            )}
            {children}
          </div>
          <div className={`block md:hidden ${classes.formBgBottom}`}></div>
        </div>
      </FlexRow.Col>
    </FlexRow.Container>
  )
}
