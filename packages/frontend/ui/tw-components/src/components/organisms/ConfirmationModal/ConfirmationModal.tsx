import React, { ReactNode } from "react"
import {
  CancelConfirmCta,
  CancelConfirmCtaAlign,
} from "../../molecules/CancelConfirmCta"
import { Modal } from "../../structure/Modal"
import { Typography } from "../../atoms/Typography"

export interface ConfirmationModalProps {
  open: boolean
  onClose?: () => void
  title?: ReactNode
  message?: ReactNode
  detail?: ReactNode
  cancel?: CancelConfirmCta
  confirm: CancelConfirmCta
  align?: CancelConfirmCtaAlign
}

export const ConfirmationModal = ({
  confirm,
  message,
  detail,
  cancel,
  open,
  title,
  onClose,
  align,
}: ConfirmationModalProps) => {
  return (
    <Modal title={title} open={open} onClose={onClose}>
      <Typography className="my-8">{message}</Typography>
      {detail}
      <CancelConfirmCta
        className="mt-8 mb-2"
        align={align}
        confirm={confirm}
        cancel={cancel}
      />
    </Modal>
  )
}

ConfirmationModal.defaultProps = {
  align: "right",
}
