import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { NotificationType, showAlert, ToastRoot } from "./Toast"
import { Button } from "../../atoms/Button"

interface ToastWrapperProps {
  type: NotificationType
  content: React.ReactNode
}

const ToastWrapper = ({ content, type }: ToastWrapperProps) => {
  return (
    <div>
      <Button
        onClick={() =>
          showAlert({
            type,
            content,
          })
        }
      >
        Click me
      </Button>
      <ToastRoot />
    </div>
  )
}

export default {
  title: "UI/Notifications/Toast",
  component: ToastWrapper,
  args: {},
} as ComponentMeta<typeof ToastWrapper>

const Template: ComponentStory<typeof ToastWrapper> = (args) => (
  <div>
    <ToastWrapper {...args} />
  </div>
)

export const Info = Template.bind({})
Info.args = {
  type: "info",
  content: "Info message",
}

export const Success = Template.bind({})
Success.args = {
  type: "success",
  content: "Success message",
}

export const Error = Template.bind({})
Error.args = {
  type: "error",
  content: "Error message",
}

export const Warning = Template.bind({})
Warning.args = {
  type: "warning",
  content: "Warning message",
}
