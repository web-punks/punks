import { createContext } from "react"
import { UserConfirmationDefinition } from "./types"

export interface ConfirmationData {
  openModal: (modal: UserConfirmationDefinition) => void
  closeModal: (id: string) => void
}

export const ConfirmationContext = createContext<ConfirmationData | null>(null)
