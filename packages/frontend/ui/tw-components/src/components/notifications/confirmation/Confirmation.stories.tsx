import React from "react"
import { ComponentStory, ComponentMeta } from "@storybook/react"
import { useConfirmation, ConformationProvider } from "."
import { UserConfirmationDefinition } from "./types"
import { Button } from "../../atoms/Button"

const ConformationControl = (definition: UserConfirmationDefinition) => {
  const { withConfirmation } = useConfirmation()
  return <Button onClick={() => withConfirmation(definition)}>Click me</Button>
}

const ConfirmationWrapper = (definition: UserConfirmationDefinition) => {
  return (
    <ConformationProvider>
      <ConformationControl {...definition} />
    </ConformationProvider>
  )
}

export default {
  title: "UI/Notifications/Confirmation",
  component: ConfirmationWrapper,
  args: {},
} as ComponentMeta<typeof ConfirmationWrapper>

const Template: ComponentStory<typeof ConfirmationWrapper> = (args) => (
  <div>
    <ConfirmationWrapper {...args} />
  </div>
)

export const Default = Template.bind({})
Default.args = {
  id: "user-deletion",
  title: "User Deletion",
  message: "Are you sure?",
  confirm: {
    label: "Yes",
    onClick: () => console.log("confirmed"),
  },
  cancel: {
    label: "No",
    onClick: () => console.log("cancelled"),
  },
}
