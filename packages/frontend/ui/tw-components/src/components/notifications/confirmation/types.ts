export interface UserConfirmationCta {
  label: string
  onClick?: () => void
}

export interface UserConfirmationDefinition {
  id: string
  title?: React.ReactNode
  message?: React.ReactNode
  cancel: UserConfirmationCta
  confirm: UserConfirmationCta
}
