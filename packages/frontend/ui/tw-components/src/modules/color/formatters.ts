import { HexColor, HslColor, RgbaColor, RgbColor } from "./types"

export const formatRgbColor = (color: RgbColor) =>
  `rgb(${color.r}, ${color.g}, ${color.b})`

export const formatRgbaColor = (color: RgbaColor) =>
  `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`

export const formatHexColor = (color: HexColor) =>
  `#${color.rHex}${color.gHex}${color.bHex}`

export const formatHslColor = (color: HslColor) =>
  `hsl(${color.h}, ${color.s}%, ${color.l}%)`
