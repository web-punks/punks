import { ThemeMode } from "@punks/ui-core"
import { useEffect } from "react"

export const useThemeHandler = (mode: ThemeMode) => {
  useEffect(() => {
    document.documentElement.setAttribute("data-theme", mode)
  }, [mode])
}
