import { ReactNode } from "react"
import { Typography } from "../../../../components/atoms/Typography"
import React from "react"

export interface DefaultHeaderProps {
  minWidth?: number
  title: ReactNode
}

const DefaultHeader = ({ minWidth, title }: DefaultHeaderProps) => {
  return (
    <div
      style={{
        minWidth,
      }}
    >
      <Typography variant="caption1" weight="bold">
        {title}
      </Typography>
    </div>
  )
}

export default DefaultHeader
