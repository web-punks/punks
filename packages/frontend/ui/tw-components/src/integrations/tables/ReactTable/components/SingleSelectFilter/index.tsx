import React from "react"
import { FunnelIcon } from "../../../../../components/icons"
import { MultiSelect } from "../../../../../components/molecules/MultiSelect"
import {
  TableColumnType,
  TableSingleSelectFilter,
} from "../../../../../components/organisms/Table/types"

export interface SingleSelectFilterProps<T> {
  definition: TableColumnType<T, unknown, unknown>
}

const SingleSelectFilter = <T,>({ definition }: SingleSelectFilterProps<T>) => {
  if (!definition.filter) {
    return <></>
  }

  const filter = definition.filter as TableSingleSelectFilter<T>
  return (
    <MultiSelect
      control={<FunnelIcon width={20} height={20} />}
      options={filter.options ?? []}
      onChange={(value) => filter?.onChange?.(value[0])}
      value={filter.selectedValue ? [filter.selectedValue] : []}
    />
  )
}

export default SingleSelectFilter
