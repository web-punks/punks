import React, { ReactNode, useMemo } from "react"
import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  getPaginationRowModel,
  PaginationState,
  useReactTable,
  getGroupedRowModel,
  getExpandedRowModel,
  TableState,
  getSortedRowModel,
  getFilteredRowModel,
  ColumnFiltersState,
  SortDirection,
  CellContext,
} from "@tanstack/react-table"
import DefaultCell from "./components/DefaultCell"
import Pagination from "./components/Pagination"
import DefaultHeader from "./components/DefaultHeader"
import MultiSelectFilter from "./components/MultiSelectFilter"
import SingleSelectFilter from "./components/SingleSelectFilter"
import {
  TableColumnFilterType,
  TableColumnFilter,
  TableColumnSorting,
  TableColumnSortingDirection,
  TableColumnType,
  TableColumnsType,
  TableOptions,
  TableSorting,
} from "../../../components/organisms/Table/types"
import { classNames, useDidUpdate } from "@punks/ui-core"
import { SortControl } from "../../../components/atoms/SortControl"

type ReactTableClasses = {
  root?: string
  table?: string
  head?: string
  row?: string
  cell?: string
  cellContent?: string
  header?: string
  loader?: string
  emptyContent?: string
}

type Props<T> = {
  data: T[]
  hideHeader?: boolean
  columnVisibility?: Partial<Record<keyof T, boolean>>
  loading?: boolean
  grouping?: Array<keyof T>
  onRowClick?: (row: T) => void
  emptyContent?: React.ReactNode
  loader?: React.ReactNode
  classes?: ReactTableClasses
  pagination?: PaginationType
  sorting?: TableSorting<unknown>
  filters?: ColumnFilter
  columns: TableColumnsType<T>
  onChange?: (state: TableState) => void
  options?: TableOptions
}

type PaginationType = {
  initialState?: Partial<PaginationState>
  state?: PaginationState
  totPages?: number
  totItems?: number
  pageItems?: number
  onPageChange?: (state: PaginationState) => void
}

type ColumnFilter = {
  initialState?: Partial<ColumnFiltersState>
  state?: ColumnFiltersState
}

const isSortingEnabled = <T,>(sorting?: TableColumnSorting<T>) =>
  !!sorting && (sorting?.enabled ?? true)

const isFilterEnabled = (filter?: TableColumnFilter<unknown>) =>
  !!filter && (filter?.enabled ?? true)

const invertSortDirection = (direction: TableColumnSortingDirection) =>
  direction === "asc" ? "desc" : "asc"

const toggleSorting = <T,>(
  sorting: TableSorting<unknown>,
  column: TableColumnType<T, unknown, unknown>
) => {
  if (!column.sorting?.value) {
    throw new Error("Sorting value is not defined")
  }

  const wasSorted = sorting?.value?.column === column.sorting.value
  sorting.onChange({
    column: column.sorting.value,
    direction:
      wasSorted && !!sorting?.value?.direction
        ? invertSortDirection(sorting?.value?.direction)
        : column.sorting.defaultDirection,
  })
}

const ReactTable = <T,>(props: Props<T>) => {
  const data = useMemo(() => {
    return props.data
  }, [props.data])

  const columns = useMemo(() => {
    return props.columns.map((column) => {
      return {
        header: (
          <DefaultHeader
            title={column.title}
            minWidth={column.minWidth ?? props.options?.columnsMinWidth}
          />
        ) as ReactNode,
        accessorKey: column.key,
        cell: (record: CellContext<T, unknown>, index: number) => (
          <>
            {column.render?.(record.row.original, index) ?? (
              <DefaultCell record={record} index={index} />
            )}
          </>
        ),
        enableSorting: isSortingEnabled(column.sorting),
        enableColumnFilter: isFilterEnabled(column.filter),
      } as ColumnDef<T, any>
    })
  }, [props.columns])

  const table = useReactTable({
    data,
    columns,
    manualPagination: true,
    manualSorting: true,
    manualFiltering: true,
    initialState: {
      expanded: true,
      grouping: (props.grouping ?? []) as string[],
      pagination: props.pagination?.initialState,
      columnVisibility: props.columnVisibility as any,
    },
    state: {
      pagination: props.pagination?.state,
      // sorting: [
      //   {
      //     desc: props.sorting?.value?.direction === "desc" ? true : false,
      //     id: props.sorting?.value?.column as any,
      //   },
      // ],
      columnFilters: props.filters?.state,
    },
    pageCount: props.pagination?.totPages,
    getExpandedRowModel: getExpandedRowModel(),
    getGroupedRowModel: getGroupedRowModel(),
    getCoreRowModel: getCoreRowModel(),
    ...(props.pagination
      ? { getPaginationRowModel: getPaginationRowModel() }
      : null),
    ...(props.sorting ? { getSortedRowModel: getSortedRowModel() } : null),
    ...(props.filters ? { getFilteredRowModel: getFilteredRowModel() } : null),
  })
  // const [state, setState] = React.useState(table.initialState)

  const [paginationState, setPaginationState] = React.useState(
    table.initialState.pagination
  )

  // const [sortingState, setSortingState] = React.useState(
  //   table.initialState.sorting
  // )
  // const [columnFiltersState, setColumnFiltersState] = React.useState(
  //   table.initialState.columnFilters
  // )

  table.setOptions((prev) => {
    return {
      ...prev,
      // state: {
      //   ...table.initialState,
      //   sorting: sortingState,
      //   ...paginationState,
      //   // ...sortingState,
      //   ...columnFiltersState,
      // },
      onPaginationChange: setPaginationState,
      // onSortingChange: setSortingState,
      // onColumnFiltersChange: setColumnFiltersState,
    }
  })
  useDidUpdate(() => {
    props.pagination?.onPageChange?.({
      pageIndex: paginationState.pageIndex,
      pageSize: paginationState.pageSize,
    })
  }, [JSON.stringify(paginationState)])

  // useDidUpdate(() => {
  //   if (sortingState[0]?.id == null) {
  //     props.sorting?.onChange()
  //   } else {
  //     props.sorting?.onChange({
  //       column: sortingState[0].id,
  //       direction: sortingState[0].desc ? "desc" : "asc",
  //     })
  //   }
  // }, [JSON.stringify(sortingState)])
  // useDidUpdate(() => {
  //   debugger
  // }, [JSON.stringify(columnFiltersState)])

  return (
    <div className={props.classes?.root}>
      <table className={classNames("w-full", props.classes?.table)}>
        {!props.hideHeader && (
          <thead className={classNames(props.classes?.head)}>
            {table.getHeaderGroups().map((headerGroup) => (
              <tr key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  const definition = props.columns[header.index]
                  const filterType = definition.filter?.type
                  const hasOptions =
                    (definition.filter?.options?.length ?? 0) > 0
                  const isSorted =
                    definition.sorting?.value === props.sorting?.value?.column

                  const handleSort = () => {
                    if (props.sorting && definition.sorting) {
                      toggleSorting(
                        props.sorting as TableSorting<unknown>,
                        definition
                      )
                    }
                  }

                  return (
                    <th
                      key={header.id}
                      colSpan={header.colSpan}
                      className="align-top"
                    >
                      <div className="flex flex-col gap-1 p-4">
                        <div
                          className={classNames(
                            "flex items-center justify-between gap-2",
                            props.classes?.header
                          )}
                        >
                          <div
                            className={classNames(
                              "min-h-[22px]",
                              props.sorting &&
                                definition.sorting &&
                                "cursor-pointer"
                            )}
                            onClick={handleSort}
                          >
                            {header.isPlaceholder
                              ? null
                              : flexRender(
                                  header.column.columnDef.header,
                                  header.getContext()
                                )}
                          </div>
                          <div className="flex gap-2">
                            {hasOptions &&
                              filterType ===
                                TableColumnFilterType.MultipleSelect && (
                                <MultiSelectFilter definition={definition} />
                              )}
                            {hasOptions &&
                              filterType ===
                                TableColumnFilterType.SingleSelect && (
                                <SingleSelectFilter definition={definition} />
                              )}
                            {props.sorting && definition.sorting && (
                              <SortControl
                                direction={
                                  isSorted
                                    ? (props.sorting.value
                                        ?.direction as SortDirection)
                                    : undefined
                                }
                                onClick={handleSort}
                              />
                            )}
                          </div>
                        </div>

                        {/* {canFilter && filterType === ColumnFilterType.Text ? (
                          <div>
                            <Input
                              onChange={(x) =>
                                header.column.setFilterValue(x.target.value)
                              }
                            />
                          </div>
                        ) : null} */}
                      </div>
                    </th>
                  )
                })}
              </tr>
            ))}
          </thead>
        )}
        <tbody>
          {props.loading ? (
            <tr>
              <td colSpan={table.getFlatHeaders().length}>
                <div
                  className={classNames(
                    "flex items-center justify-center",
                    props.classes?.loader
                  )}
                >
                  {props.loader}
                </div>
              </td>
            </tr>
          ) : (
            <>
              {table.getRowModel().rows.map((row, i) => {
                const previousRow = table.getRowModel().rows[i - 1]
                if (row.getIsGrouped()) {
                  return null
                }
                return (
                  <tr
                    onClick={() => props.onRowClick?.(row.original)}
                    key={row.id}
                    className={classNames(
                      !!props.onRowClick && "cursor-pointer",
                      props.classes?.row
                    )}
                  >
                    {row.getVisibleCells().map((cell) => {
                      if (
                        cell.getIsPlaceholder() &&
                        !previousRow?.getIsGrouped()
                      ) {
                        return null
                      }
                      return (
                        <td
                          rowSpan={
                            cell.getIsPlaceholder() &&
                            previousRow?.getIsGrouped()
                              ? previousRow.subRows.length
                              : 1
                          }
                          key={cell.id}
                          className={props.classes?.cell}
                        >
                          <div
                            className={classNames(
                              "p-4",
                              props.classes?.cellContent
                            )}
                          >
                            {flexRender(
                              cell.column.columnDef.cell,
                              cell.getContext()
                            )}
                          </div>
                        </td>
                      )
                    })}
                  </tr>
                )
              })}
              {props.emptyContent && table.getRowModel().rows.length === 0 && (
                <div className={props.classes?.emptyContent}>
                  {props.emptyContent}
                </div>
              )}
            </>
          )}
        </tbody>
        <tfoot></tfoot>
      </table>
      {props.pagination && (props.pagination.pageItems ?? 0) > 0 && (
        <Pagination
          table={table}
          pagination={{
            pageIndex: props.pagination.state?.pageIndex,
            pageSize: props.pagination.state?.pageSize,
          }}
          totPages={props.pagination.totPages}
          totItems={props.pagination.totItems}
          pageItems={props.pagination.pageItems}
        />
      )}
    </div>
  )
}

export default ReactTable
