import React from "react"
import { PaginationState, Table } from "@tanstack/react-table"
import { Typography } from "../../../../components/atoms/Typography"
import {
  ChevronDoubleLeftIcon,
  ChevronDoubleRightIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from "../../../../components/icons"
import { classNames, range } from "@punks/ui-core"
import { Button } from "../../../../components/atoms/Button"

type Props<T> = {
  table: Table<T>
  pagination?: Partial<PaginationState>
  totPages?: number
  totItems?: number
  pageItems?: number
}

const Pagination = <T,>({
  table,
  pagination,
  totPages = 0,
  totItems = 0,
  pageItems = 0,
}: Props<T>) => {
  const pageIndex = pagination?.pageIndex ?? 0
  const pageSize = pagination?.pageSize ?? 10

  const pageItemsFrom = pageIndex * pageSize + 1
  const pageItemsTo = pageItemsFrom + pageItems - 1

  return (
    <div className="flex items-center justify-between my-2">
      <div>
        <Typography>{`${pageItemsFrom}-${pageItemsTo} / ${totItems}`}</Typography>
      </div>
      <PaginationButtons
        onClick={(index) => table.setPageIndex(index)}
        pageCount={totPages}
        currentPage={pageIndex}
      />
    </div>
  )
}
type PaginationButtonsProps = {
  onClick: (index: number) => void
  pageCount: number
  currentPage: number
}
const PaginationButtons = ({
  onClick,
  pageCount,
  currentPage,
}: PaginationButtonsProps) => {
  if (pageCount === 1) {
    return null
  }
  return (
    <div className="flex flex-wrap items-center gap-2 py-2">
      {range(pageCount)
        .slice(0, 5)
        .map((_p, i) => {
          return (
            <PaginationButton
              key={i}
              onClick={onClick}
              currentPage={currentPage}
              index={i}
              pageCount={pageCount}
            />
          )
        })}
    </div>
  )
}
type ButtonProps = {
  onClick: (index: number) => void
  index: number
  currentPage: number
  pageCount: number
}
const PaginationButton = ({
  onClick,
  index,
  currentPage,
  pageCount,
}: ButtonProps) => {
  if (pageCount <= 5) {
    return (
      <Button
        onClick={() => onClick(index)}
        size="small"
        color={index === currentPage ? "primary" : "light"}
      >
        {index + 1}
      </Button>
    )
  }
  const CurrentIconMap: Record<number, JSX.Element | null> = {
    0: <ChevronDoubleLeftIcon width={14} height={14} />,
    1: <ChevronLeftIcon width={14} height={10} />,
    2: <div>{currentPage + 1}</div>,
    3: <ChevronRightIcon width={14} height={10} />,
    4: <ChevronDoubleRightIcon width={14} height={14} />,
  }
  const indexNavigationMap: Record<number, number> = {
    0: 0,
    1: currentPage - 1,
    3: currentPage + 1,
    4: pageCount - 1,
  }
  const CurrentIcon = CurrentIconMap[index]
  if (CurrentIcon == null) {
    return null
  }

  return (
    <Button
      size="small"
      className={classNames(index === 2 && "cursor-default", "!p-0")}
      onClick={() => {
        if (index !== 2) {
          onClick(indexNavigationMap[index])
        }
      }}
    >
      <div className="flex items-center justify-center w-8 h-8">
        {CurrentIcon}
      </div>
    </Button>
  )
}

export default Pagination
