import React from "react"
import { Typography } from "../../../../components/atoms/Typography"

type Props = {
  // color?: ThemeColorFull
  text: string
  otherText?: string
  // otherTextColor?: ThemeColorFull
}

const TextCell = (props: Props) => {
  return (
    <div>
      <Typography
        component="div"
        variant={"body1"}
        // {...(props.color && { colorValue: props.color })}
      >
        {props.text}
      </Typography>
      {!!props.otherText && (
        <Typography
          component="div"
          variant={"caption2"}
          // {...(props.otherTextColor && { colorValue: props.otherTextColor })}
        >
          {props.otherText}
        </Typography>
      )}
    </div>
  )
}

export default TextCell
