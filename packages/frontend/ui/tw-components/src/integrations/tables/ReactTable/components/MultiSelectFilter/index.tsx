import React from "react"
import { FunnelIcon } from "../../../../../components/icons"
import { MultiSelect } from "../../../../../components/molecules/MultiSelect"
import {
  TableColumnType,
  TableMultiSelectFilter,
} from "../../../../../components/organisms/Table/types"

export interface MultiSelectFilterProps<T> {
  definition: TableColumnType<T, unknown, unknown>
}

const MultiSelectFilter = <T,>({ definition }: MultiSelectFilterProps<T>) => {
  if (!definition.filter) {
    return <></>
  }

  const filter = definition.filter as TableMultiSelectFilter<T>
  return (
    <MultiSelect
      control={<FunnelIcon width={20} height={20} />}
      options={
        filter.options?.map((x) => ({
          label: x.label ?? x.value,
          value: x.value,
        })) ?? []
      }
      value={filter.selectedValues ?? []}
      onChange={(value) =>
        (filter as TableMultiSelectFilter<unknown>)?.onChange?.(value)
      }
    />
  )
}

export default MultiSelectFilter
