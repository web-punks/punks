import { CellContext } from "@tanstack/react-table"
import React from "react"
import TextCell from "./TextCell"

export interface DefaultCellProps<T> {
  record: CellContext<T, unknown>
  index: number
}

const DefaultCell = <T,>(props: DefaultCellProps<T>) => {
  return <TextCell text={props.record.getValue() as string} />
}

export default DefaultCell
