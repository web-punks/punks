import React from "react"
import { useDarkMode } from "storybook-dark-mode"
import { useThemeHandler } from "../../hooks/useThemeHandler"
import { extendTailwindTheme } from "../../utils"
import { ThemeProvider } from "../../theme/provider"
import { ThemeVariables } from "./variables"

export interface StorybookWrapperProps {
  children: React.ReactNode
}

const Head = ({ children }: { children: any }) => <>{children}</>

export const StorybookWrapper = ({ children }: StorybookWrapperProps) => {
  const mode = useDarkMode()
  useThemeHandler(mode ? "dark" : "light")

  console.log("theme", extendTailwindTheme())
  return (
    <>
      <ThemeProvider fallbackTheme={ThemeVariables} Head={Head}>
        {children}
      </ThemeProvider>
    </>
  )
}
