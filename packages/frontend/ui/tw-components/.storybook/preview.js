import "@fontsource/roboto/300.css"
import "@fontsource/roboto/400.css"
import "@fontsource/roboto/500.css"
import "@fontsource/roboto/700.css"
import "@fontsource/material-icons"
import { CssBaseline, ThemeProvider } from "@mui/material"
import { withThemeFromJSXProvider } from "@storybook/addon-themes"
import { lightTheme, darkTheme } from "../src/themes.js"
import "./preview.css"
import { StorybookWrapper } from "../src/integrations/storybook"

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  previewTabs: {
    "storybook/docs/panel": { index: -1 },
  },
  // darkMode: {
  //   // Override the default dark theme
  //   // dark: { ...themes.dark, appBg: "black" },
  //   // Override the default light theme
  //   // light: { ...themes.normal, appBg: "white" },
  // },
}

export const decorators = [
  (Story) => (
    <StorybookWrapper>
      <withThemeFromJSXProvider
        themes={{
          light: lightTheme,
          dark: darkTheme,
        }}
        defaultTheme="light"
        Provider={ThemeProvider}
        GlobalStyles={CssBaseline}
      >
        <Story />
      </withThemeFromJSXProvider>
    </StorybookWrapper>
  ),
]
