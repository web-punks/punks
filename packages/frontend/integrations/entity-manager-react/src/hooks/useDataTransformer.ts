export interface DataFilterInput<T> {
  data?: T[]
  filter?: (item: T) => boolean
}

const filterData = <T>(data: T[], filter: (item: T) => boolean) =>
  data.filter(filter)

export function useDataFilter<T>({ data, filter }: DataFilterInput<T>) {
  return {
    original: data,
    filtered: filter && data ? filterData(data, filter) : data,
  }
}
