export type AuthenticationTokens = {
  accessToken: string
  refreshToken?: string
}

export enum AuthenticationLoginError {
  InvalidCredentials = "InvalidCredentials",
  ServerError = "ServerError",
}

export type AuthenticationLoginResult<TProfile> = {
  success: boolean
  error?: AuthenticationLoginError
  tokens?: AuthenticationTokens
  profile?: TProfile
}

export type AuthenticationLoginInput<TContext> = {
  username: string
  password: string
  context: TContext
}

export type AuthenticationForgotPasswordInput<TContext> = {
  email: string
  context: TContext
}

export enum AuthenticationForgotPasswordError {
  UserNotFound = "UserNotFound",
  ServerError = "ServerError",
}

export type AuthenticationForgotPasswordResult = {
  success: boolean
  error?: AuthenticationForgotPasswordError
}

export type AuthenticationOperationEvents<TInput, TResult> = {
  onTriggered?: (input: TInput) => void
  onCompleted?: (result: TResult) => void
  onError?: (error: any) => void
}

export type AuthenticationPasswordResetInput<TContext> = {
  password: string
  token: string
  context: TContext
  extraFields?: Record<string, any>
}

export enum AuthenticationPasswordResetError {
  InvalidPassword = "InvalidPassword",
  ServerError = "ServerError",
}

export type AuthenticationPasswordResetResult = {
  success: boolean
  error?: AuthenticationPasswordResetError
}

export type AuthenticationEvents<TProfile> = {
  login?: AuthenticationOperationEvents<
    AuthenticationLoginInput<unknown>,
    AuthenticationLoginResult<TProfile>
  >
  forgotPassword?: AuthenticationOperationEvents<
    AuthenticationForgotPasswordInput<unknown>,
    AuthenticationForgotPasswordResult
  >
  passwordReset?: AuthenticationOperationEvents<
    AuthenticationPasswordResetInput<unknown>,
    AuthenticationPasswordResetResult
  >
  logout?: AuthenticationOperationEvents<void, void>
}

export type AuthenticationState<TProfile> = {
  isAuthenticated: boolean
  authenticationTokens?: AuthenticationTokens
  profile?: TProfile
}

export type AuthenticationAction<TInput, TResult> = (
  input: TInput
) => Promise<TResult>

export type AuthenticationActions<TProfile, TContext> = {
  login: AuthenticationAction<
    AuthenticationLoginInput<TContext>,
    AuthenticationLoginResult<TProfile>
  >
  forgotPassword: AuthenticationAction<
    AuthenticationForgotPasswordInput<TContext>,
    AuthenticationForgotPasswordResult
  >
  passwordReset: AuthenticationAction<
    AuthenticationPasswordResetInput<TContext>,
    AuthenticationPasswordResetResult
  >
  logout: AuthenticationAction<void, void>
}

export type AuthenticationContextType<TProfile, TContext> = {
  context: TContext
  state: AuthenticationState<TProfile>
  events: AuthenticationEvents<TProfile>
  actions: AuthenticationActions<TProfile, TContext>
  stateUpdate: (state: AuthenticationState<TProfile>) => void
}
