import { createContext, useContext } from "react"
import { AuthenticationContextType } from "./types"

export const AuthenticationContext = createContext<
  AuthenticationContextType<unknown, unknown>
>({
  context: {},
  state: {
    isAuthenticated: false,
  },
  events: {} as any,
  actions: {} as any,
  stateUpdate: () => {},
})

export const useAuthenticationContext = <
  TAuthenticationContext extends AuthenticationContextType<any, any>
>() => useContext(AuthenticationContext) as TAuthenticationContext
