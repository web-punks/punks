import { Context, useState } from "react"
import { AuthenticationContext } from "./context"
import {
  AuthenticationActions,
  AuthenticationContextType,
  AuthenticationEvents,
  AuthenticationState,
} from "./types"

export interface AuthenticationProviderProps<TProfile, TContext> {
  children: any
  events?: AuthenticationEvents<TProfile>
  actions: AuthenticationActions<TProfile, TContext>
  context: TContext
}

export const AuthenticationProvider = <TProfile, TContext>({
  children,
  actions,
  events,
  context,
}: AuthenticationProviderProps<TProfile, TContext>) => {
  const ContextComponent = AuthenticationContext as Context<
    AuthenticationContextType<TProfile, TContext>
  >
  const [state, setState] = useState<AuthenticationState<TProfile>>({
    isAuthenticated: false,
  })
  return (
    <ContextComponent.Provider
      value={{
        state,
        actions,
        events: events ?? {},
        stateUpdate: setState,
        context,
      }}
    >
      {children}
    </ContextComponent.Provider>
  )
}
