import { Log, useOperation } from "@punks/ui-core"
import { useAuthenticationContext } from "../context"

export type PasswordResetInput = {
  password: string
  token: string
  extraFields?: Record<string, any>
}

export type PasswordResetActionInput = {
  onSuccess?: () => void
  onError?: () => void
}

export const usePasswordResetAction = ({
  onError,
  onSuccess,
}: PasswordResetActionInput) => {
  const { actions, context } = useAuthenticationContext()

  const { invoke, state } = useOperation({
    operation: actions.passwordReset,
    onSuccess: (result) => {
      if (result?.success) {
        Log.root.info("Password reset succeed", { result })
        onSuccess?.()
      } else {
        Log.root.info("Password reset error result", { result })
        onError?.()
      }
    },
    onError,
  })

  const handleResetPassword = async (input: PasswordResetInput) => {
    invoke({
      password: input.password,
      token: input.token,
      ...(input?.extraFields ? { extraFields: input.extraFields } : {}),
      context,
    })
  }

  return {
    handleResetPassword,
    state,
  }
}
