import { Log, useOperation } from "@punks/ui-core"
import { useAuthenticationContext } from "../context"

export type UserLoginInput = {
  username: string
  password: string
}

export type LoginActionInput = {
  onSuccess?: () => void
  onError?: () => void
}

export const useLoginAction = ({ onError, onSuccess }: LoginActionInput) => {
  const { actions, context } = useAuthenticationContext()

  const { invoke, state } = useOperation({
    operation: actions.login,
    onSuccess: (result) => {
      if (result?.success) {
        Log.root.info("Login succeed", { result })
        onSuccess?.()
      } else {
        Log.root.info("Login failed", { result })
        onError?.()
      }
    },
    onError,
  })

  const handleLogin = async (input: UserLoginInput) => {
    invoke({
      username: input.username,
      password: input.password,
      context,
    })
  }

  return {
    handleLogin,
    state,
  }
}
