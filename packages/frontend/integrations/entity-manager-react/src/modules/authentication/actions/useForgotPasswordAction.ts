import { Log, useOperation } from "@punks/ui-core"
import { useAuthenticationContext } from "../context"

export type ForgotPasswordInput = {
  email: string
}

export type ForgotPasswordActionInput = {
  onSuccess?: () => void
  onError?: () => void
}

export const useForgotPasswordAction = ({
  onError,
  onSuccess,
}: ForgotPasswordActionInput) => {
  const { actions, context } = useAuthenticationContext()

  const { invoke, state } = useOperation({
    operation: actions.forgotPassword,
    onSuccess: (result) => {
      if (result?.success) {
        Log.root.info("Forgot password email sent", { result })
        onSuccess?.()
      } else {
        Log.root.info("Forgot password email error result", { result })
        onError?.()
      }
    },
    onError,
  })

  const handleForgotPassword = async (input: ForgotPasswordInput) => {
    invoke({
      email: input.email,
      context,
    })
  }

  return {
    handleForgotPassword,
    state,
  }
}
