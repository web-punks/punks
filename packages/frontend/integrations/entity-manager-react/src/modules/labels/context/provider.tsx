import { createContext, useContext } from "react"

export type EntityManagerLabelsMap = {
  [key: string]: string
}

export type EntityManagerLabelsType = {
  map: EntityManagerLabelsMap
}

const EntityManagerLabelsContext = createContext<EntityManagerLabelsType>({
  map: {},
})

export interface EntityManagerLabelsProviderProps {
  children: any
  labels: EntityManagerLabelsMap
}

export const EntityManagerLabelsProvider = ({
  children,
  labels,
}: EntityManagerLabelsProviderProps) => {
  return (
    <EntityManagerLabelsContext.Provider
      value={{
        map: labels,
      }}
    >
      {children}
    </EntityManagerLabelsContext.Provider>
  )
}

export const useEntityManagerLabels = () =>
  useContext(EntityManagerLabelsContext)
