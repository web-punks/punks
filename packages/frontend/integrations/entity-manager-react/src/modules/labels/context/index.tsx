export {
  EntityManagerLabelsMap,
  EntityManagerLabelsProvider,
  EntityManagerLabelsType,
  EntityManagerLabelsProviderProps,
  useEntityManagerLabels,
} from "./provider"
