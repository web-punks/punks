import { useCallback } from "react"
import { useEntityManagerLabels } from "../context"
import { LabelsPlaceholders } from "../types"
import { replacePlaceholders } from "../utils/placeholders"

export const useEntityManagerLabel = () => {
  const { map } = useEntityManagerLabels()

  const getLabel = useCallback(
    (
      key: string,
      options?: {
        placeholders?: LabelsPlaceholders
        defaultValue?: string
      }
    ) => {
      if (!map[key]) {
        return options?.defaultValue ?? map[key]
      }
      return options?.placeholders
        ? replacePlaceholders(map[key], options.placeholders)
        : map[key]
    },
    [map]
  )

  return {
    getLabel,
  }
}
