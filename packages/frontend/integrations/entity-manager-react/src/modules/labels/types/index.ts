export type LabelsPlaceholders = {
  [key: string]: string | number | boolean | null | undefined
}
