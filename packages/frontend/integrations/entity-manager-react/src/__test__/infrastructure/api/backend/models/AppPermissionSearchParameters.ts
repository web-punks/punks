/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppPermissionFullTextQuery } from './AppPermissionFullTextQuery';
import type { AppPermissionQueryPaging } from './AppPermissionQueryPaging';
import type { AppPermissionQuerySorting } from './AppPermissionQuerySorting';
import type { AppPermissionSearchFilters } from './AppPermissionSearchFilters';
import type { AppPermissionSearchOptions } from './AppPermissionSearchOptions';

export type AppPermissionSearchParameters = {
    query?: AppPermissionFullTextQuery;
    filters?: AppPermissionSearchFilters;
    sorting?: AppPermissionQuerySorting;
    paging?: AppPermissionQueryPaging;
    options?: AppPermissionSearchOptions;
};

