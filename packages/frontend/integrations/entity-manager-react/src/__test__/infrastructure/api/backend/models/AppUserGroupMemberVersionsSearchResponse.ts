/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupMemberListItemDto } from './AppUserGroupMemberListItemDto';
import type { AppUserGroupMemberVersionsResultsPaging } from './AppUserGroupMemberVersionsResultsPaging';

export type AppUserGroupMemberVersionsSearchResponse = {
    paging: AppUserGroupMemberVersionsResultsPaging;
    items: Array<AppUserGroupMemberListItemDto>;
};

