/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type NumericFacetValue = {
    value: number;
    label: string;
    count: number;
};

