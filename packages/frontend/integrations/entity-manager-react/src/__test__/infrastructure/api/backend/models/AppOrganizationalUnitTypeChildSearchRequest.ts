/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeChildSearchParameters } from './AppOrganizationalUnitTypeChildSearchParameters';

export type AppOrganizationalUnitTypeChildSearchRequest = {
    params: AppOrganizationalUnitTypeChildSearchParameters;
};

