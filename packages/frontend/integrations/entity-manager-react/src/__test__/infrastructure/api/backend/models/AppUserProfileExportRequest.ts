/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserProfileEntitiesExportOptions } from './AppUserProfileEntitiesExportOptions';
import type { AppUserProfileSearchParameters } from './AppUserProfileSearchParameters';

export type AppUserProfileExportRequest = {
    options: AppUserProfileEntitiesExportOptions;
    filter?: AppUserProfileSearchParameters;
};

