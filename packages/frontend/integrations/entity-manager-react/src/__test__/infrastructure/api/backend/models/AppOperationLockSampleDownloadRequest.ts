/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOperationLockSampleDownloadRequest = {
    format: AppOperationLockSampleDownloadRequest.format;
};

export namespace AppOperationLockSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

