/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserRoleSearchSortingField } from './AppUserRoleSearchSortingField';

export type AppUserRoleQuerySorting = {
    fields?: Array<AppUserRoleSearchSortingField>;
};

