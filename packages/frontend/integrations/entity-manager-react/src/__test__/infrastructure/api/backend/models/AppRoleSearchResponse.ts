/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppRoleFacets } from './AppRoleFacets';
import type { AppRoleListItemDto } from './AppRoleListItemDto';
import type { AppRoleSearchParameters } from './AppRoleSearchParameters';
import type { AppRoleSearchResultsPaging } from './AppRoleSearchResultsPaging';

export type AppRoleSearchResponse = {
    request: AppRoleSearchParameters;
    facets: AppRoleFacets;
    paging: AppRoleSearchResultsPaging;
    items: Array<AppRoleListItemDto>;
};

