/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserGroupMemberSampleDownloadRequest = {
    format: AppUserGroupMemberSampleDownloadRequest.format;
};

export namespace AppUserGroupMemberSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

