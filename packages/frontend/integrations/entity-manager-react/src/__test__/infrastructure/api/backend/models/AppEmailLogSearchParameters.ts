/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEmailLogFullTextQuery } from './AppEmailLogFullTextQuery';
import type { AppEmailLogQueryPaging } from './AppEmailLogQueryPaging';
import type { AppEmailLogQuerySorting } from './AppEmailLogQuerySorting';
import type { AppEmailLogSearchFilters } from './AppEmailLogSearchFilters';
import type { AppEmailLogSearchOptions } from './AppEmailLogSearchOptions';

export type AppEmailLogSearchParameters = {
    query?: AppEmailLogFullTextQuery;
    filters?: AppEmailLogSearchFilters;
    sorting?: AppEmailLogQuerySorting;
    paging?: AppEmailLogQueryPaging;
    options?: AppEmailLogSearchOptions;
};

