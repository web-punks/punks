/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppCompanyFullTextQuery } from './AppCompanyFullTextQuery';
import type { AppCompanyQueryPaging } from './AppCompanyQueryPaging';
import type { AppCompanyQuerySorting } from './AppCompanyQuerySorting';
import type { AppCompanySearchFilters } from './AppCompanySearchFilters';
import type { AppCompanySearchOptions } from './AppCompanySearchOptions';

export type AppCompanySearchParameters = {
    query?: AppCompanyFullTextQuery;
    filters?: AppCompanySearchFilters;
    sorting?: AppCompanyQuerySorting;
    paging?: AppCompanyQueryPaging;
    options?: AppCompanySearchOptions;
};

