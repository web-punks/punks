/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppTenantEntitiesExportOptions } from './AppTenantEntitiesExportOptions';
import type { AppTenantSearchParameters } from './AppTenantSearchParameters';

export type AppTenantExportRequest = {
    options: AppTenantEntitiesExportOptions;
    filter?: AppTenantSearchParameters;
};

