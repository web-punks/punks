/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOperationLockVersionsSearchParameters } from './AppOperationLockVersionsSearchParameters';

export type AppOperationLockVersionsSearchRequest = {
    params: AppOperationLockVersionsSearchParameters;
};

