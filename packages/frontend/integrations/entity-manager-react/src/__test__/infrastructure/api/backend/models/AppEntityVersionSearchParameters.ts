/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEntityVersionFullTextQuery } from './AppEntityVersionFullTextQuery';
import type { AppEntityVersionQueryPaging } from './AppEntityVersionQueryPaging';
import type { AppEntityVersionQuerySorting } from './AppEntityVersionQuerySorting';
import type { AppEntityVersionSearchFilters } from './AppEntityVersionSearchFilters';
import type { AppEntityVersionSearchOptions } from './AppEntityVersionSearchOptions';

export type AppEntityVersionSearchParameters = {
    query?: AppEntityVersionFullTextQuery;
    filters?: AppEntityVersionSearchFilters;
    sorting?: AppEntityVersionQuerySorting;
    paging?: AppEntityVersionQueryPaging;
    options?: AppEntityVersionSearchOptions;
};

