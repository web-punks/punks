/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationalUnitTypeSearchSortingField = {
    field: AppOrganizationalUnitTypeSearchSortingField.field;
    direction: AppOrganizationalUnitTypeSearchSortingField.direction;
};

export namespace AppOrganizationalUnitTypeSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

