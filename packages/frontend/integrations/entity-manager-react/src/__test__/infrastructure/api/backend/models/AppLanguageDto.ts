/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationReferenceDto } from './AppOrganizationReferenceDto';

export type AppLanguageDto = {
    id: string;
    name: string;
    uid: string;
    default: boolean;
    organization: AppOrganizationReferenceDto;
    createdOn: string;
    updatedOn: string;
};

