/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitSearchParameters } from './AppOrganizationalUnitSearchParameters';

export type AppOrganizationalUnitSearchRequest = {
    params: AppOrganizationalUnitSearchParameters;
};

