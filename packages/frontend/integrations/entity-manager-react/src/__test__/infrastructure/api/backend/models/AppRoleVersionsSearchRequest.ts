/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppRoleVersionsSearchParameters } from './AppRoleVersionsSearchParameters';

export type AppRoleVersionsSearchRequest = {
    params: AppRoleVersionsSearchParameters;
};

