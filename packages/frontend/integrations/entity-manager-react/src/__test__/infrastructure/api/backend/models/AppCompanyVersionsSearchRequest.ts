/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppCompanyVersionsSearchParameters } from './AppCompanyVersionsSearchParameters';

export type AppCompanyVersionsSearchRequest = {
    params: AppCompanyVersionsSearchParameters;
};

