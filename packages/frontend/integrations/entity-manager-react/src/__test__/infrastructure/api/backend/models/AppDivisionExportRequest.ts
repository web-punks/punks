/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDivisionEntitiesExportOptions } from './AppDivisionEntitiesExportOptions';
import type { AppDivisionSearchParameters } from './AppDivisionSearchParameters';

export type AppDivisionExportRequest = {
    options: AppDivisionEntitiesExportOptions;
    filter?: AppDivisionSearchParameters;
};

