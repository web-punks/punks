/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppCompanySearchParameters } from './AppCompanySearchParameters';

export type AppCompanySearchRequest = {
    params: AppCompanySearchParameters;
};

