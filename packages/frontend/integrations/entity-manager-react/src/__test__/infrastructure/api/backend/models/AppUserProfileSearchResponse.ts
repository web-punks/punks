/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserProfileFacets } from './AppUserProfileFacets';
import type { AppUserProfileListItemDto } from './AppUserProfileListItemDto';
import type { AppUserProfileSearchParameters } from './AppUserProfileSearchParameters';
import type { AppUserProfileSearchResultsPaging } from './AppUserProfileSearchResultsPaging';

export type AppUserProfileSearchResponse = {
    request: AppUserProfileSearchParameters;
    facets: AppUserProfileFacets;
    paging: AppUserProfileSearchResultsPaging;
    items: Array<AppUserProfileListItemDto>;
};

