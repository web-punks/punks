/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppFileReferenceSearchSortingField } from './AppFileReferenceSearchSortingField';

export type AppFileReferenceQuerySorting = {
    fields?: Array<AppFileReferenceSearchSortingField>;
};

