/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AuthUserContext } from './AuthUserContext';
import type { AuthUserRegistrationData } from './AuthUserRegistrationData';
import type { OperationCallbackTemplate } from './OperationCallbackTemplate';

export type UserRegisterRequest = {
    email: string;
    userName: string;
    password: string;
    data: AuthUserRegistrationData;
    userContext: AuthUserContext;
    callback: OperationCallbackTemplate;
    languageCode: string;
};

