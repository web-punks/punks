/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FooEntitiesExportOptions } from './FooEntitiesExportOptions';
import type { FooSearchParameters } from './FooSearchParameters';

export type FooExportRequest = {
    options: FooEntitiesExportOptions;
    filter?: FooSearchParameters;
};

