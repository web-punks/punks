/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDirectoryEntitiesExportOptions } from './AppDirectoryEntitiesExportOptions';
import type { AppDirectorySearchParameters } from './AppDirectorySearchParameters';

export type AppDirectoryExportRequest = {
    options: AppDirectoryEntitiesExportOptions;
    filter?: AppDirectorySearchParameters;
};

