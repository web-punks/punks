/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppPermissionSearchSortingField } from './AppPermissionSearchSortingField';

export type AppPermissionQuerySorting = {
    fields?: Array<AppPermissionSearchSortingField>;
};

