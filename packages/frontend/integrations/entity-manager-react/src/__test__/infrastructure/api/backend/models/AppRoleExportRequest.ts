/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppRoleEntitiesExportOptions } from './AppRoleEntitiesExportOptions';
import type { AppRoleSearchParameters } from './AppRoleSearchParameters';

export type AppRoleExportRequest = {
    options: AppRoleEntitiesExportOptions;
    filter?: AppRoleSearchParameters;
};

