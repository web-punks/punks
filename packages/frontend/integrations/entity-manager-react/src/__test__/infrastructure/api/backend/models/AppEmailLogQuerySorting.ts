/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEmailLogSearchSortingField } from './AppEmailLogSearchSortingField';

export type AppEmailLogQuerySorting = {
    fields?: Array<AppEmailLogSearchSortingField>;
};

