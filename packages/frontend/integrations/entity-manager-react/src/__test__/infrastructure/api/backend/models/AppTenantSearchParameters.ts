/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppTenantFullTextQuery } from './AppTenantFullTextQuery';
import type { AppTenantQueryPaging } from './AppTenantQueryPaging';
import type { AppTenantQuerySorting } from './AppTenantQuerySorting';
import type { AppTenantSearchFilters } from './AppTenantSearchFilters';
import type { AppTenantSearchOptions } from './AppTenantSearchOptions';

export type AppTenantSearchParameters = {
    query?: AppTenantFullTextQuery;
    filters?: AppTenantSearchFilters;
    sorting?: AppTenantQuerySorting;
    paging?: AppTenantQueryPaging;
    options?: AppTenantSearchOptions;
};

