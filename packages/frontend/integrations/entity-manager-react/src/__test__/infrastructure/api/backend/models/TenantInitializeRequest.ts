/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DirectoryInitializeInfo } from './DirectoryInitializeInfo';
import type { TenantAdmin } from './TenantAdmin';
import type { TenantInitializeInfo } from './TenantInitializeInfo';

export type TenantInitializeRequest = {
    tenant: TenantInitializeInfo;
    directory: DirectoryInitializeInfo;
    defaultAdmin: TenantAdmin;
};

