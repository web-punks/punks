/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IdFilter = {
    in?: Array<string>;
    eq?: string;
    ne?: string;
    notIn?: Array<string>;
    isNull?: boolean;
};

