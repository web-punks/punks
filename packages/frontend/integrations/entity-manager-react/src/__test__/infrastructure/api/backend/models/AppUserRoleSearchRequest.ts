/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserRoleSearchParameters } from './AppUserRoleSearchParameters';

export type AppUserRoleSearchRequest = {
    params: AppUserRoleSearchParameters;
};

