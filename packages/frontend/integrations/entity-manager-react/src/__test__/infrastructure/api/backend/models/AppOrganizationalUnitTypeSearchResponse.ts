/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeFacets } from './AppOrganizationalUnitTypeFacets';
import type { AppOrganizationalUnitTypeListItemDto } from './AppOrganizationalUnitTypeListItemDto';
import type { AppOrganizationalUnitTypeSearchParameters } from './AppOrganizationalUnitTypeSearchParameters';
import type { AppOrganizationalUnitTypeSearchResultsPaging } from './AppOrganizationalUnitTypeSearchResultsPaging';

export type AppOrganizationalUnitTypeSearchResponse = {
    request: AppOrganizationalUnitTypeSearchParameters;
    facets: AppOrganizationalUnitTypeFacets;
    paging: AppOrganizationalUnitTypeSearchResultsPaging;
    items: Array<AppOrganizationalUnitTypeListItemDto>;
};

