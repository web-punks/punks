/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppRoleAllowedOrganizationalUnitTypeInput } from './AppRoleAllowedOrganizationalUnitTypeInput';
import type { AppRoleAssignableRoleInput } from './AppRoleAssignableRoleInput';
import type { AppRolePermissionInput } from './AppRolePermissionInput';

export type AppRoleUpdateDto = {
    name: string;
    enabled: boolean;
    allowAsRoot: boolean;
    organizationalUnityTypes?: Array<AppRoleAllowedOrganizationalUnitTypeInput>;
    assignableRoles?: Array<AppRoleAssignableRoleInput>;
    permissions?: Array<AppRolePermissionInput>;
};

