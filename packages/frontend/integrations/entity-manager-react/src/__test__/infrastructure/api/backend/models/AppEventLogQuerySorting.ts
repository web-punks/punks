/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEventLogSearchSortingField } from './AppEventLogSearchSortingField';

export type AppEventLogQuerySorting = {
    fields?: Array<AppEventLogSearchSortingField>;
};

