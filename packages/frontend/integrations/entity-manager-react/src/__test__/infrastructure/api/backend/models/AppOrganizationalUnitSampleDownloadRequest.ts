/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationalUnitSampleDownloadRequest = {
    format: AppOrganizationalUnitSampleDownloadRequest.format;
};

export namespace AppOrganizationalUnitSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

