/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppLanguageSampleDownloadRequest = {
    format: AppLanguageSampleDownloadRequest.format;
};

export namespace AppLanguageSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

