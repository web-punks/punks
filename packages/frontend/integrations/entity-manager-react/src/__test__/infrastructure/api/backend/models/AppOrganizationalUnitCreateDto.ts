/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationalUnitCreateDto = {
    name: string;
    uid: string;
    typeId: string;
    parentId?: string;
    organizationId: string;
};

