/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppCompanyOrganizationDto } from './AppCompanyOrganizationDto';

export type AppCompanyListItemDto = {
    id: string;
    uid: string;
    name: string;
    organization: AppCompanyOrganizationDto;
    createdOn: string;
    updatedOn: string;
};

