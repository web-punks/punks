/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEmailLogListItemDto } from './AppEmailLogListItemDto';
import type { AppEmailLogVersionsResultsPaging } from './AppEmailLogVersionsResultsPaging';

export type AppEmailLogVersionsSearchResponse = {
    paging: AppEmailLogVersionsResultsPaging;
    items: Array<AppEmailLogListItemDto>;
};

