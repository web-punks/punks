/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppFileReferenceFacets } from './AppFileReferenceFacets';
import type { AppFileReferenceListItemDto } from './AppFileReferenceListItemDto';
import type { AppFileReferenceSearchParameters } from './AppFileReferenceSearchParameters';
import type { AppFileReferenceSearchResultsPaging } from './AppFileReferenceSearchResultsPaging';

export type AppFileReferenceSearchResponse = {
    request: AppFileReferenceSearchParameters;
    facets: AppFileReferenceFacets;
    paging: AppFileReferenceSearchResultsPaging;
    items: Array<AppFileReferenceListItemDto>;
};

