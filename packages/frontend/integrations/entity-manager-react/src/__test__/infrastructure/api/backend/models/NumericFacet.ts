/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { NumericFacetValue } from './NumericFacetValue';

export type NumericFacet = {
    values: Array<NumericFacetValue>;
};

