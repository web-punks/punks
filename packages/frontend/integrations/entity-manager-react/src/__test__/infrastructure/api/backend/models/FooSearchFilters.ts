/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { BooleanFilter } from './BooleanFilter';
import type { IdFilter } from './IdFilter';
import type { NumericFilter } from './NumericFilter';
import type { StringFilter } from './StringFilter';

export type FooSearchFilters = {
    age?: NumericFilter;
    name?: StringFilter;
    uid?: StringFilter;
    type?: IdFilter;
    other?: StringFilter;
    enabled?: BooleanFilter;
};

