/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EntityParseResult } from './EntityParseResult';

export type FooParseResponse = {
    entries: Array<EntityParseResult>;
};

