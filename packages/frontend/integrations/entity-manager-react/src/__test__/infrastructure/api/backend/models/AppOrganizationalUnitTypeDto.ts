/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeAllowedChildType } from './AppOrganizationalUnitTypeAllowedChildType';
import type { AppOrganizationReferenceDto } from './AppOrganizationReferenceDto';

export type AppOrganizationalUnitTypeDto = {
    id: string;
    uid: string;
    name: string;
    allowAsRoot: boolean;
    allowedChildrenTypes: Array<AppOrganizationalUnitTypeAllowedChildType>;
    organization: AppOrganizationReferenceDto;
    createdOn: string;
    updatedOn: string;
};

