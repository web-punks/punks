/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationFullTextQuery = {
    term: string;
    fields: Array<string>;
};

