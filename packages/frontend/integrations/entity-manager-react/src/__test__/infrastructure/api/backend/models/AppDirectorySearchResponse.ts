/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDirectoryFacets } from './AppDirectoryFacets';
import type { AppDirectoryListItemDto } from './AppDirectoryListItemDto';
import type { AppDirectorySearchParameters } from './AppDirectorySearchParameters';
import type { AppDirectorySearchResultsPaging } from './AppDirectorySearchResultsPaging';

export type AppDirectorySearchResponse = {
    request: AppDirectorySearchParameters;
    facets: AppDirectoryFacets;
    paging: AppDirectorySearchResultsPaging;
    items: Array<AppDirectoryListItemDto>;
};

