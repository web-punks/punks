/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { BooleanFacetValue } from './BooleanFacetValue';

export type BooleanFacet = {
    values: Array<BooleanFacetValue>;
};

