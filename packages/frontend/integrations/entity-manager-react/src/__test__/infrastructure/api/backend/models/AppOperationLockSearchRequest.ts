/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOperationLockSearchParameters } from './AppOperationLockSearchParameters';

export type AppOperationLockSearchRequest = {
    params: AppOperationLockSearchParameters;
};

