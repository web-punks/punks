/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppDivisionDto = {
    id: string;
    uid: string;
    name: string;
    createdOn: string;
    updatedOn: string;
};

