/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TenantAdmin = {
    email: string;
    firstName: string;
    lastName: string;
    password: string;
};

