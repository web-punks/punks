/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppEmailLogEntitiesExportOptions = {
    format: AppEmailLogEntitiesExportOptions.format;
};

export namespace AppEmailLogEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

