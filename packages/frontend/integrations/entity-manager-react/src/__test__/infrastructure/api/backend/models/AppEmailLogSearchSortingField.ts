/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppEmailLogSearchSortingField = {
    field: AppEmailLogSearchSortingField.field;
    direction: AppEmailLogSearchSortingField.direction;
};

export namespace AppEmailLogSearchSortingField {

    export enum field {
        EMAIL_TYPE = 'EmailType',
        TIMESTAMP = 'Timestamp',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

