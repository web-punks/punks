import { trimEnd } from "@punks/ui-core"
import { DefaultClient, OpenAPI } from "./backend"

export { OpenAPI as BackendOpenAPI }
export { DefaultClient as BackendClient }

interface Props {
  children: any
}

export const ApiProvider = ({ children }: Props) => {
  OpenAPI.BASE = trimEnd(
    process.env.BACKEND_ENDPOINT ?? "http://localhost:3599",
    "/"
  )

  // const { token } = useAppSelector((state) => state.auth)

  // React.useEffect(() => {
  //   OpenAPI.TOKEN = token ?? undefined
  // }, [token])

  return <>{children}</>
}
