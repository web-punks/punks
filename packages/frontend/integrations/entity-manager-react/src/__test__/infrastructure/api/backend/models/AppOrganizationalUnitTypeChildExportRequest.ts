/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeChildEntitiesExportOptions } from './AppOrganizationalUnitTypeChildEntitiesExportOptions';
import type { AppOrganizationalUnitTypeChildSearchParameters } from './AppOrganizationalUnitTypeChildSearchParameters';

export type AppOrganizationalUnitTypeChildExportRequest = {
    options: AppOrganizationalUnitTypeChildEntitiesExportOptions;
    filter?: AppOrganizationalUnitTypeChildSearchParameters;
};

