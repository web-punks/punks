/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOperationLockSearchSortingField = {
    field: AppOperationLockSearchSortingField.field;
    direction: AppOperationLockSearchSortingField.direction;
};

export namespace AppOperationLockSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

