/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppLanguageListItemDto } from './AppLanguageListItemDto';
import type { AppLanguageVersionsResultsPaging } from './AppLanguageVersionsResultsPaging';

export type AppLanguageVersionsSearchResponse = {
    paging: AppLanguageVersionsResultsPaging;
    items: Array<AppLanguageListItemDto>;
};

