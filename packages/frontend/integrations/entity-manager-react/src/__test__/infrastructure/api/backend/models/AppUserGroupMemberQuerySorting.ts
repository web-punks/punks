/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupMemberSearchSortingField } from './AppUserGroupMemberSearchSortingField';

export type AppUserGroupMemberQuerySorting = {
    fields?: Array<AppUserGroupMemberSearchSortingField>;
};

