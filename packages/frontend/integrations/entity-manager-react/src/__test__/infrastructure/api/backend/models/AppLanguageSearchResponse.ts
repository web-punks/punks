/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppLanguageFacets } from './AppLanguageFacets';
import type { AppLanguageListItemDto } from './AppLanguageListItemDto';
import type { AppLanguageSearchParameters } from './AppLanguageSearchParameters';
import type { AppLanguageSearchResultsPaging } from './AppLanguageSearchResultsPaging';

export type AppLanguageSearchResponse = {
    request: AppLanguageSearchParameters;
    facets: AppLanguageFacets;
    paging: AppLanguageSearchResultsPaging;
    items: Array<AppLanguageListItemDto>;
};

