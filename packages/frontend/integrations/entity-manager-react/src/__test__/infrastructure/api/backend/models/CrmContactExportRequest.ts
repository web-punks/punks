/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CrmContactEntitiesExportOptions } from './CrmContactEntitiesExportOptions';
import type { CrmContactSearchParameters } from './CrmContactSearchParameters';

export type CrmContactExportRequest = {
    options: CrmContactEntitiesExportOptions;
    filter?: CrmContactSearchParameters;
};

