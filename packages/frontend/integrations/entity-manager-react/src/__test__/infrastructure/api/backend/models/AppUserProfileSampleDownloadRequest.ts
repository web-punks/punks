/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserProfileSampleDownloadRequest = {
    format: AppUserProfileSampleDownloadRequest.format;
};

export namespace AppUserProfileSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

