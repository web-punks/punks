/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppRoleSearchSortingField = {
    field: AppRoleSearchSortingField.field;
    direction: AppRoleSearchSortingField.direction;
};

export namespace AppRoleSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

