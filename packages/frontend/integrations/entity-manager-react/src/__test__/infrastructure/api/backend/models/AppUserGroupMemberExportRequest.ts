/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupMemberEntitiesExportOptions } from './AppUserGroupMemberEntitiesExportOptions';
import type { AppUserGroupMemberSearchParameters } from './AppUserGroupMemberSearchParameters';

export type AppUserGroupMemberExportRequest = {
    options: AppUserGroupMemberEntitiesExportOptions;
    filter?: AppUserGroupMemberSearchParameters;
};

