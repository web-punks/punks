/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppDivisionEntitiesExportOptions = {
    format: AppDivisionEntitiesExportOptions.format;
};

export namespace AppDivisionEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

