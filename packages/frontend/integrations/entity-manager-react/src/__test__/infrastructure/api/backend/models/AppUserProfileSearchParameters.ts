/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserProfileFullTextQuery } from './AppUserProfileFullTextQuery';
import type { AppUserProfileQueryPaging } from './AppUserProfileQueryPaging';
import type { AppUserProfileQuerySorting } from './AppUserProfileQuerySorting';
import type { AppUserProfileSearchFilters } from './AppUserProfileSearchFilters';
import type { AppUserProfileSearchOptions } from './AppUserProfileSearchOptions';

export type AppUserProfileSearchParameters = {
    query?: AppUserProfileFullTextQuery;
    filters?: AppUserProfileSearchFilters;
    sorting?: AppUserProfileQuerySorting;
    paging?: AppUserProfileQueryPaging;
    options?: AppUserProfileSearchOptions;
};

