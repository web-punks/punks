/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserSampleDownloadRequest = {
    format: AppUserSampleDownloadRequest.format;
};

export namespace AppUserSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

