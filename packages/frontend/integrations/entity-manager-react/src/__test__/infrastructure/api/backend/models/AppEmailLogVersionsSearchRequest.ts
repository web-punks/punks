/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEmailLogVersionsSearchParameters } from './AppEmailLogVersionsSearchParameters';

export type AppEmailLogVersionsSearchRequest = {
    params: AppEmailLogVersionsSearchParameters;
};

