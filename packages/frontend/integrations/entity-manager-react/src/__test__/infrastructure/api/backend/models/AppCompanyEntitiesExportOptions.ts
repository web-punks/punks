/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppCompanyEntitiesExportOptions = {
    format: AppCompanyEntitiesExportOptions.format;
};

export namespace AppCompanyEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

