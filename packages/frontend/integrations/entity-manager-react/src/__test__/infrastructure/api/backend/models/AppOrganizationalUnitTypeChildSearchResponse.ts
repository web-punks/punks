/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeChildFacets } from './AppOrganizationalUnitTypeChildFacets';
import type { AppOrganizationalUnitTypeChildListItemDto } from './AppOrganizationalUnitTypeChildListItemDto';
import type { AppOrganizationalUnitTypeChildSearchParameters } from './AppOrganizationalUnitTypeChildSearchParameters';
import type { AppOrganizationalUnitTypeChildSearchResultsPaging } from './AppOrganizationalUnitTypeChildSearchResultsPaging';

export type AppOrganizationalUnitTypeChildSearchResponse = {
    request: AppOrganizationalUnitTypeChildSearchParameters;
    facets: AppOrganizationalUnitTypeChildFacets;
    paging: AppOrganizationalUnitTypeChildSearchResultsPaging;
    items: Array<AppOrganizationalUnitTypeChildListItemDto>;
};

