/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CrmContactVersionsSearchParameters } from './CrmContactVersionsSearchParameters';

export type CrmContactVersionsSearchRequest = {
    params: CrmContactVersionsSearchParameters;
};

