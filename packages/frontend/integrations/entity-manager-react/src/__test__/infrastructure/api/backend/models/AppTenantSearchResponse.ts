/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppTenantFacets } from './AppTenantFacets';
import type { AppTenantListItemDto } from './AppTenantListItemDto';
import type { AppTenantSearchParameters } from './AppTenantSearchParameters';
import type { AppTenantSearchResultsPaging } from './AppTenantSearchResultsPaging';

export type AppTenantSearchResponse = {
    request: AppTenantSearchParameters;
    facets: AppTenantFacets;
    paging: AppTenantSearchResultsPaging;
    items: Array<AppTenantListItemDto>;
};

