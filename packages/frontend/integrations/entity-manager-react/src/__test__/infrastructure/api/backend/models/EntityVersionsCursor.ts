/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type EntityVersionsCursor = {
    cursor?: Record<string, any>;
    pageSize: number;
};

