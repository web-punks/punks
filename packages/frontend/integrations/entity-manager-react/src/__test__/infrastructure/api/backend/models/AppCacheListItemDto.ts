/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppCacheListItemDto = {
    instance: string;
    key: string;
    expiration: string;
    createdOn: string;
    updatedOn: string;
};

