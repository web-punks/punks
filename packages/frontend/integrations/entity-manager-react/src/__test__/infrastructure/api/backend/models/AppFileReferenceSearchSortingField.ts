/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppFileReferenceSearchSortingField = {
    field: AppFileReferenceSearchSortingField.field;
    direction: AppFileReferenceSearchSortingField.direction;
};

export namespace AppFileReferenceSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

