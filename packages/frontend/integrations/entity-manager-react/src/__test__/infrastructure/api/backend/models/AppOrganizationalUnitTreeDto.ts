/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTreeNodeDto } from './AppOrganizationalUnitTreeNodeDto';

export type AppOrganizationalUnitTreeDto = {
    root: Array<AppOrganizationalUnitTreeNodeDto>;
};

