/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserTenant = {
    id: string;
    uid: string;
    name: string;
};

