/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppLanguageEntitiesExportOptions = {
    format: AppLanguageEntitiesExportOptions.format;
};

export namespace AppLanguageEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

