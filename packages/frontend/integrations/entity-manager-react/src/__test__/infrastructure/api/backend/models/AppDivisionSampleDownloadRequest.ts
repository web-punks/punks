/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppDivisionSampleDownloadRequest = {
    format: AppDivisionSampleDownloadRequest.format;
};

export namespace AppDivisionSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

