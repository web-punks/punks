/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IdFilter } from './IdFilter';

export type AppOperationLockSearchFilters = {
    id?: IdFilter;
};

