/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationalUnitTypeSampleDownloadRequest = {
    format: AppOrganizationalUnitTypeSampleDownloadRequest.format;
};

export namespace AppOrganizationalUnitTypeSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

