/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppLanguageEntitiesExportOptions } from './AppLanguageEntitiesExportOptions';
import type { AppLanguageSearchParameters } from './AppLanguageSearchParameters';

export type AppLanguageExportRequest = {
    options: AppLanguageEntitiesExportOptions;
    filter?: AppLanguageSearchParameters;
};

