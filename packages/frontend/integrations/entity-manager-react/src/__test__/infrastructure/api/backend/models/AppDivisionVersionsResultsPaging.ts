/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppDivisionVersionsResultsPaging = {
    pageIndex: number;
    pageSize: number;
    totPageItems: number;
    totPages: number;
    totItems: number;
    nextPageCursor?: number;
    currentPageCursor?: number;
    prevPageCursor?: number;
};

