/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { StringFacetValue } from './StringFacetValue';

export type StringFacet = {
    values: Array<StringFacetValue>;
};

