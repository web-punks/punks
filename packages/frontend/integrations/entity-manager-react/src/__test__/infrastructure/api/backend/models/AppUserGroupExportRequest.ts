/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupEntitiesExportOptions } from './AppUserGroupEntitiesExportOptions';
import type { AppUserGroupSearchParameters } from './AppUserGroupSearchParameters';

export type AppUserGroupExportRequest = {
    options: AppUserGroupEntitiesExportOptions;
    filter?: AppUserGroupSearchParameters;
};

