/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppRoleListItemDto } from './AppRoleListItemDto';
import type { AppRoleVersionsResultsPaging } from './AppRoleVersionsResultsPaging';

export type AppRoleVersionsSearchResponse = {
    paging: AppRoleVersionsResultsPaging;
    items: Array<AppRoleListItemDto>;
};

