/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEventLogListItemDto } from './AppEventLogListItemDto';
import type { AppEventLogVersionsResultsPaging } from './AppEventLogVersionsResultsPaging';

export type AppEventLogVersionsSearchResponse = {
    paging: AppEventLogVersionsResultsPaging;
    items: Array<AppEventLogListItemDto>;
};

