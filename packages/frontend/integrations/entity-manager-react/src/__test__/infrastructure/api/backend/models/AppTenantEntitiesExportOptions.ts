/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppTenantEntitiesExportOptions = {
    format: AppTenantEntitiesExportOptions.format;
};

export namespace AppTenantEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

