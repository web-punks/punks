/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppRoleAssignableRole } from './AppRoleAssignableRole';
import type { AppRoleOrganizationalUnitType } from './AppRoleOrganizationalUnitType';
import type { AppRolePermission } from './AppRolePermission';

export type AppRoleDto = {
    id: string;
    uid: string;
    name: string;
    enabled: boolean;
    allowAsRoot: boolean;
    organizationalUnitTypes: Array<AppRoleOrganizationalUnitType>;
    assignableRoles: Array<AppRoleAssignableRole>;
    permissions: Array<AppRolePermission>;
    createdOn: string;
    updatedOn: string;
};

