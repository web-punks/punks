/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEntityVersionFacets } from './AppEntityVersionFacets';
import type { AppEntityVersionListItemDto } from './AppEntityVersionListItemDto';
import type { AppEntityVersionSearchParameters } from './AppEntityVersionSearchParameters';
import type { AppEntityVersionSearchResultsPaging } from './AppEntityVersionSearchResultsPaging';

export type AppEntityVersionSearchResponse = {
    request: AppEntityVersionSearchParameters;
    facets: AppEntityVersionFacets;
    paging: AppEntityVersionSearchResultsPaging;
    items: Array<AppEntityVersionListItemDto>;
};

