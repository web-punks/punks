/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppTenantVersionsSearchParameters } from './AppTenantVersionsSearchParameters';

export type AppTenantVersionsSearchRequest = {
    params: AppTenantVersionsSearchParameters;
};

