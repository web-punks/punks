/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationalUnitSearchSortingField = {
    field: AppOrganizationalUnitSearchSortingField.field;
    direction: AppOrganizationalUnitSearchSortingField.direction;
};

export namespace AppOrganizationalUnitSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

