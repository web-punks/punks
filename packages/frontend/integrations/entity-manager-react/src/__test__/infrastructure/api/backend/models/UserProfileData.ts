/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserOrganization } from './UserOrganization';
import type { UserProfilePersonalData } from './UserProfilePersonalData';
import type { UserTenant } from './UserTenant';

export type UserProfileData = {
    id: string;
    userName: string;
    email: string;
    verified: boolean;
    disabled: boolean;
    profile: UserProfilePersonalData;
    organization?: UserOrganization;
    tenant: UserTenant;
};

