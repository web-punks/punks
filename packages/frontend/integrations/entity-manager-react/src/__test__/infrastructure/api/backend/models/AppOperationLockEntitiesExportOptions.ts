/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOperationLockEntitiesExportOptions = {
    format: AppOperationLockEntitiesExportOptions.format;
};

export namespace AppOperationLockEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

