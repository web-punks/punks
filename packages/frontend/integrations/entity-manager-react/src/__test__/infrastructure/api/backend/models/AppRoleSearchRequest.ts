/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppRoleSearchParameters } from './AppRoleSearchParameters';

export type AppRoleSearchRequest = {
    params: AppRoleSearchParameters;
};

