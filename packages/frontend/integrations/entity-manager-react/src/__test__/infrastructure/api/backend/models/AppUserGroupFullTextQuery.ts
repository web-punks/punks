/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserGroupFullTextQuery = {
    term: string;
    fields: Array<string>;
};

