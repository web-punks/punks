/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserFullTextQuery } from './AppUserFullTextQuery';
import type { AppUserQueryPaging } from './AppUserQueryPaging';
import type { AppUserQuerySorting } from './AppUserQuerySorting';
import type { AppUserSearchFilters } from './AppUserSearchFilters';
import type { AppUserSearchOptions } from './AppUserSearchOptions';

export type AppUserSearchParameters = {
    query?: AppUserFullTextQuery;
    filters?: AppUserSearchFilters;
    sorting?: AppUserQuerySorting;
    paging?: AppUserQueryPaging;
    options?: AppUserSearchOptions;
};

