/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppTenantSearchParameters } from './AppTenantSearchParameters';

export type AppTenantSearchRequest = {
    params: AppTenantSearchParameters;
};

