/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationalUnitTypeChildSampleDownloadRequest = {
    format: AppOrganizationalUnitTypeChildSampleDownloadRequest.format;
};

export namespace AppOrganizationalUnitTypeChildSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

