/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserGroupDto = {
    id: string;
    uid: string;
    name: string;
    createdOn: string;
    updatedOn: string;
};

