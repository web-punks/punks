/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type FooUpdateDto = {
    name: string;
    uid: string;
    type: string;
    other?: string;
    age: number;
};

