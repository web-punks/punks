/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppPermissionSampleDownloadRequest = {
    format: AppPermissionSampleDownloadRequest.format;
};

export namespace AppPermissionSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

