/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserOrganizationDto = {
    id: string;
    uid: string;
    name: string;
};

