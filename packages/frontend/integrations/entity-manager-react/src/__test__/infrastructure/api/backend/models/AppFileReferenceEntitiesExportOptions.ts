/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppFileReferenceEntitiesExportOptions = {
    format: AppFileReferenceEntitiesExportOptions.format;
};

export namespace AppFileReferenceEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

