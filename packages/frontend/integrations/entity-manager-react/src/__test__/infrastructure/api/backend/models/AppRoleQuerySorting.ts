/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppRoleSearchSortingField } from './AppRoleSearchSortingField';

export type AppRoleQuerySorting = {
    fields?: Array<AppRoleSearchSortingField>;
};

