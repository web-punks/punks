/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeVersionsSearchParameters } from './AppOrganizationalUnitTypeVersionsSearchParameters';

export type AppOrganizationalUnitTypeVersionsSearchRequest = {
    params: AppOrganizationalUnitTypeVersionsSearchParameters;
};

