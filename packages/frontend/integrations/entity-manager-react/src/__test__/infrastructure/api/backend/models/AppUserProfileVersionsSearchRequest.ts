/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserProfileVersionsSearchParameters } from './AppUserProfileVersionsSearchParameters';

export type AppUserProfileVersionsSearchRequest = {
    params: AppUserProfileVersionsSearchParameters;
};

