/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDivisionListItemDto } from './AppDivisionListItemDto';
import type { AppDivisionVersionsResultsPaging } from './AppDivisionVersionsResultsPaging';

export type AppDivisionVersionsSearchResponse = {
    paging: AppDivisionVersionsResultsPaging;
    items: Array<AppDivisionListItemDto>;
};

