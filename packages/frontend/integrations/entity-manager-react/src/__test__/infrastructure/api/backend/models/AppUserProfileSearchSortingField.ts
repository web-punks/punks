/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserProfileSearchSortingField = {
    field: AppUserProfileSearchSortingField.field;
    direction: AppUserProfileSearchSortingField.direction;
};

export namespace AppUserProfileSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

