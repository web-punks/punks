/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type NumericFilter = {
    in?: Array<number>;
    eq?: number;
    gt?: number;
    gte?: number;
    lt?: number;
    lte?: number;
    isNull?: boolean;
};

