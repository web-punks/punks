/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CrmContactListItemDto } from './CrmContactListItemDto';
import type { CrmContactVersionsResultsPaging } from './CrmContactVersionsResultsPaging';

export type CrmContactVersionsSearchResponse = {
    paging: CrmContactVersionsResultsPaging;
    items: Array<CrmContactListItemDto>;
};

