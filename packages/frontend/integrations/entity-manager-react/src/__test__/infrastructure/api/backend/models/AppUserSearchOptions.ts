/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserSearchFilters } from './AppUserSearchFilters';

export type AppUserSearchOptions = {
    includeFacets?: boolean;
    facetsFilters?: AppUserSearchFilters;
};

