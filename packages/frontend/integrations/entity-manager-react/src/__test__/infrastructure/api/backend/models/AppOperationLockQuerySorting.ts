/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOperationLockSearchSortingField } from './AppOperationLockSearchSortingField';

export type AppOperationLockQuerySorting = {
    fields?: Array<AppOperationLockSearchSortingField>;
};

