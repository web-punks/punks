/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupFullTextQuery } from './AppUserGroupFullTextQuery';
import type { AppUserGroupQueryPaging } from './AppUserGroupQueryPaging';
import type { AppUserGroupQuerySorting } from './AppUserGroupQuerySorting';
import type { AppUserGroupSearchFilters } from './AppUserGroupSearchFilters';
import type { AppUserGroupSearchOptions } from './AppUserGroupSearchOptions';

export type AppUserGroupSearchParameters = {
    query?: AppUserGroupFullTextQuery;
    filters?: AppUserGroupSearchFilters;
    sorting?: AppUserGroupQuerySorting;
    paging?: AppUserGroupQueryPaging;
    options?: AppUserGroupSearchOptions;
};

