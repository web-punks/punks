/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDirectoryListItemDto } from './AppDirectoryListItemDto';
import type { AppDirectoryVersionsResultsPaging } from './AppDirectoryVersionsResultsPaging';

export type AppDirectoryVersionsSearchResponse = {
    paging: AppDirectoryVersionsResultsPaging;
    items: Array<AppDirectoryListItemDto>;
};

