/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEventLogVersionsSearchParameters } from './AppEventLogVersionsSearchParameters';

export type AppEventLogVersionsSearchRequest = {
    params: AppEventLogVersionsSearchParameters;
};

