/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDirectoryFullTextQuery } from './AppDirectoryFullTextQuery';
import type { AppDirectoryQueryPaging } from './AppDirectoryQueryPaging';
import type { AppDirectoryQuerySorting } from './AppDirectoryQuerySorting';
import type { AppDirectorySearchFilters } from './AppDirectorySearchFilters';
import type { AppDirectorySearchOptions } from './AppDirectorySearchOptions';

export type AppDirectorySearchParameters = {
    query?: AppDirectoryFullTextQuery;
    filters?: AppDirectorySearchFilters;
    sorting?: AppDirectoryQuerySorting;
    paging?: AppDirectoryQueryPaging;
    options?: AppDirectorySearchOptions;
};

