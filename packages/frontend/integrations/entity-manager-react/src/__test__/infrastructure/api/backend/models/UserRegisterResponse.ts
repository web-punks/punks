/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserRegisterResponse = {
    success: boolean;
    error?: UserRegisterResponse.error;
};

export namespace UserRegisterResponse {

    export enum error {
        USER_ALREADY_EXISTS = 'userAlreadyExists',
    }


}

