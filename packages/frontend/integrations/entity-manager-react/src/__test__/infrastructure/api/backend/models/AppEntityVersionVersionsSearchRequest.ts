/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEntityVersionVersionsSearchParameters } from './AppEntityVersionVersionsSearchParameters';

export type AppEntityVersionVersionsSearchRequest = {
    params: AppEntityVersionVersionsSearchParameters;
};

