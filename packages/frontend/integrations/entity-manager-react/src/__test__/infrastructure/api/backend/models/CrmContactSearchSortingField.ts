/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CrmContactSearchSortingField = {
    field: CrmContactSearchSortingField.field;
    direction: CrmContactSearchSortingField.direction;
};

export namespace CrmContactSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

