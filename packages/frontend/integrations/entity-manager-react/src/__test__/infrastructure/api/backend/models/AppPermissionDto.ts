/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppPermissionDto = {
    id: string;
    name: string;
    uid: string;
    createdOn: string;
    updatedOn: string;
};

