/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserListItemDto } from './AppUserListItemDto';
import type { AppUserVersionsResultsPaging } from './AppUserVersionsResultsPaging';

export type AppUserVersionsSearchResponse = {
    paging: AppUserVersionsResultsPaging;
    items: Array<AppUserListItemDto>;
};

