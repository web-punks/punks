/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupMemberFacets } from './AppUserGroupMemberFacets';
import type { AppUserGroupMemberListItemDto } from './AppUserGroupMemberListItemDto';
import type { AppUserGroupMemberSearchParameters } from './AppUserGroupMemberSearchParameters';
import type { AppUserGroupMemberSearchResultsPaging } from './AppUserGroupMemberSearchResultsPaging';

export type AppUserGroupMemberSearchResponse = {
    request: AppUserGroupMemberSearchParameters;
    facets: AppUserGroupMemberFacets;
    paging: AppUserGroupMemberSearchResultsPaging;
    items: Array<AppUserGroupMemberListItemDto>;
};

