/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppTenantListItemDto } from './AppTenantListItemDto';
import type { AppTenantVersionsResultsPaging } from './AppTenantVersionsResultsPaging';

export type AppTenantVersionsSearchResponse = {
    paging: AppTenantVersionsResultsPaging;
    items: Array<AppTenantListItemDto>;
};

