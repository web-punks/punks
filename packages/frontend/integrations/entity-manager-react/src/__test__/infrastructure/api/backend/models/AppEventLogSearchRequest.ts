/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEventLogSearchParameters } from './AppEventLogSearchParameters';

export type AppEventLogSearchRequest = {
    params: AppEventLogSearchParameters;
};

