/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppRoleFullTextQuery } from './AppRoleFullTextQuery';
import type { AppRoleQueryPaging } from './AppRoleQueryPaging';
import type { AppRoleQuerySorting } from './AppRoleQuerySorting';
import type { AppRoleSearchFilters } from './AppRoleSearchFilters';
import type { AppRoleSearchOptions } from './AppRoleSearchOptions';

export type AppRoleSearchParameters = {
    query?: AppRoleFullTextQuery;
    filters?: AppRoleSearchFilters;
    sorting?: AppRoleQuerySorting;
    paging?: AppRoleQueryPaging;
    options?: AppRoleSearchOptions;
};

