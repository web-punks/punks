/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserRoleEntitiesExportOptions } from './AppUserRoleEntitiesExportOptions';
import type { AppUserRoleSearchParameters } from './AppUserRoleSearchParameters';

export type AppUserRoleExportRequest = {
    options: AppUserRoleEntitiesExportOptions;
    filter?: AppUserRoleSearchParameters;
};

