/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppFileReferenceSampleDownloadRequest = {
    format: AppFileReferenceSampleDownloadRequest.format;
};

export namespace AppFileReferenceSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

