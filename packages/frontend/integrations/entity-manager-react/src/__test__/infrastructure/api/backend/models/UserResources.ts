/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserOrganization } from './UserOrganization';
import type { UserRole } from './UserRole';
import type { UserTenant } from './UserTenant';

export type UserResources = {
    organizations: Array<UserOrganization>;
    tenants: Array<UserTenant>;
    roles: Array<UserRole>;
};

