/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationVersionsSearchParameters } from './AppOrganizationVersionsSearchParameters';

export type AppOrganizationVersionsSearchRequest = {
    params: AppOrganizationVersionsSearchParameters;
};

