/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupVersionsSearchParameters } from './AppUserGroupVersionsSearchParameters';

export type AppUserGroupVersionsSearchRequest = {
    params: AppUserGroupVersionsSearchParameters;
};

