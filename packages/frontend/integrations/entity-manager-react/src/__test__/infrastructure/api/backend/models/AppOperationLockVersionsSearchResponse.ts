/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOperationLockListItemDto } from './AppOperationLockListItemDto';
import type { AppOperationLockVersionsResultsPaging } from './AppOperationLockVersionsResultsPaging';

export type AppOperationLockVersionsSearchResponse = {
    paging: AppOperationLockVersionsResultsPaging;
    items: Array<AppOperationLockListItemDto>;
};

