/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEventLogFacets } from './AppEventLogFacets';
import type { AppEventLogListItemDto } from './AppEventLogListItemDto';
import type { AppEventLogSearchParameters } from './AppEventLogSearchParameters';
import type { AppEventLogSearchResultsPaging } from './AppEventLogSearchResultsPaging';

export type AppEventLogSearchResponse = {
    request: AppEventLogSearchParameters;
    facets: AppEventLogFacets;
    paging: AppEventLogSearchResultsPaging;
    items: Array<AppEventLogListItemDto>;
};

