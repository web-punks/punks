/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FooSearchSortingField } from './FooSearchSortingField';

export type FooQuerySorting = {
    fields?: Array<FooSearchSortingField>;
};

