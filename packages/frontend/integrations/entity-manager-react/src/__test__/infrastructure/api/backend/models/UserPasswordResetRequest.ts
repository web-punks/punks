/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AuthUserContext } from './AuthUserContext';
import type { OperationCallbackTemplate } from './OperationCallbackTemplate';

export type UserPasswordResetRequest = {
    userName: string;
    userContext?: AuthUserContext;
    callback: OperationCallbackTemplate;
    languageCode: string;
};

