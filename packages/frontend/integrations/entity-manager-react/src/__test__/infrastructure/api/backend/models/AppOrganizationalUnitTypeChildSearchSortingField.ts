/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationalUnitTypeChildSearchSortingField = {
    field: AppOrganizationalUnitTypeChildSearchSortingField.field;
    direction: AppOrganizationalUnitTypeChildSearchSortingField.direction;
};

export namespace AppOrganizationalUnitTypeChildSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

