/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CrmContactFacets } from './CrmContactFacets';
import type { CrmContactListItemDto } from './CrmContactListItemDto';
import type { CrmContactSearchParameters } from './CrmContactSearchParameters';
import type { CrmContactSearchResultsPaging } from './CrmContactSearchResultsPaging';

export type CrmContactSearchResponse = {
    request: CrmContactSearchParameters;
    facets: CrmContactFacets;
    paging: CrmContactSearchResultsPaging;
    items: Array<CrmContactListItemDto>;
};

