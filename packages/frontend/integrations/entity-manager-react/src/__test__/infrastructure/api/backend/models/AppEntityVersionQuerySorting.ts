/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEntityVersionSearchSortingField } from './AppEntityVersionSearchSortingField';

export type AppEntityVersionQuerySorting = {
    fields?: Array<AppEntityVersionSearchSortingField>;
};

