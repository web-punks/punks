/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppLanguageSearchParameters } from './AppLanguageSearchParameters';

export type AppLanguageSearchRequest = {
    params: AppLanguageSearchParameters;
};

