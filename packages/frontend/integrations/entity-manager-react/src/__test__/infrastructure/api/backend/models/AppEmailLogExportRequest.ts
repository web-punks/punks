/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEmailLogEntitiesExportOptions } from './AppEmailLogEntitiesExportOptions';
import type { AppEmailLogSearchParameters } from './AppEmailLogSearchParameters';

export type AppEmailLogExportRequest = {
    options: AppEmailLogEntitiesExportOptions;
    filter?: AppEmailLogSearchParameters;
};

