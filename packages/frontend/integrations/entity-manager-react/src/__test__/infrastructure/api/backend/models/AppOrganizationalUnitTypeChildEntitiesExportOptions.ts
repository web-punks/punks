/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationalUnitTypeChildEntitiesExportOptions = {
    format: AppOrganizationalUnitTypeChildEntitiesExportOptions.format;
};

export namespace AppOrganizationalUnitTypeChildEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

