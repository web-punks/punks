/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppDirectorySearchSortingField = {
    field: AppDirectorySearchSortingField.field;
    direction: AppDirectorySearchSortingField.direction;
};

export namespace AppDirectorySearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

