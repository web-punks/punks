/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitSearchSortingField } from './AppOrganizationalUnitSearchSortingField';

export type AppOrganizationalUnitQuerySorting = {
    fields?: Array<AppOrganizationalUnitSearchSortingField>;
};

