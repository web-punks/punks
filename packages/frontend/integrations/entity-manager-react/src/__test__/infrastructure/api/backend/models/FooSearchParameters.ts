/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EntitiesTraverseFilters } from './EntitiesTraverseFilters';
import type { FooFullTextQuery } from './FooFullTextQuery';
import type { FooQueryPaging } from './FooQueryPaging';
import type { FooQuerySorting } from './FooQuerySorting';
import type { FooSearchFilters } from './FooSearchFilters';
import type { FooSearchOptions } from './FooSearchOptions';

export type FooSearchParameters = {
    query?: FooFullTextQuery;
    filters?: FooSearchFilters;
    traverse?: EntitiesTraverseFilters;
    sorting?: FooQuerySorting;
    paging?: FooQueryPaging;
    options?: FooSearchOptions;
};

