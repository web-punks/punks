/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppRolePermission = {
    id: string;
    name: string;
    uid: string;
};

