/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppEmailLogSampleDownloadRequest = {
    format: AppEmailLogSampleDownloadRequest.format;
};

export namespace AppEmailLogSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

