/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppPermissionEntitiesExportOptions } from './AppPermissionEntitiesExportOptions';
import type { AppPermissionSearchParameters } from './AppPermissionSearchParameters';

export type AppPermissionExportRequest = {
    options: AppPermissionEntitiesExportOptions;
    filter?: AppPermissionSearchParameters;
};

