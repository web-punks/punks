/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDirectorySearchParameters } from './AppDirectorySearchParameters';

export type AppDirectorySearchRequest = {
    params: AppDirectorySearchParameters;
};

