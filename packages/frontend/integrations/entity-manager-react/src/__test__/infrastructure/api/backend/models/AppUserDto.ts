/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserOrganizationalUnit } from './AppUserOrganizationalUnit';
import type { AppUserOrganizationDto } from './AppUserOrganizationDto';
import type { AppUserProfileEntityDto } from './AppUserProfileEntityDto';

export type AppUserDto = {
    id: string;
    uid?: string;
    userName: string;
    email: string;
    verified: boolean;
    verifiedTimestamp?: string;
    passwordUpdateTimestamp?: string;
    disabled: boolean;
    profile: AppUserProfileEntityDto;
    organization?: AppUserOrganizationDto;
    organizationalUnit?: AppUserOrganizationalUnit;
    createdOn: string;
    updatedOn: string;
};

