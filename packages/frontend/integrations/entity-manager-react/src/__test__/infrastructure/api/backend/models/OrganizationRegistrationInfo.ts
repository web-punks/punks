/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type OrganizationRegistrationInfo = {
    tenantId: string;
    name: string;
    uid: string;
};

