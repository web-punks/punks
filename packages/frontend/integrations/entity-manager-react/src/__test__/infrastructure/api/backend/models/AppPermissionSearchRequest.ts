/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppPermissionSearchParameters } from './AppPermissionSearchParameters';

export type AppPermissionSearchRequest = {
    params: AppPermissionSearchParameters;
};

