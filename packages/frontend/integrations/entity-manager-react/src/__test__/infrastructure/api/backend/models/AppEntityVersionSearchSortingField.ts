/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppEntityVersionSearchSortingField = {
    field: AppEntityVersionSearchSortingField.field;
    direction: AppEntityVersionSearchSortingField.direction;
};

export namespace AppEntityVersionSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

