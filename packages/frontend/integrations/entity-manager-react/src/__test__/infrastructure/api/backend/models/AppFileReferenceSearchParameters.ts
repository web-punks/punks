/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppFileReferenceFullTextQuery } from './AppFileReferenceFullTextQuery';
import type { AppFileReferenceQueryPaging } from './AppFileReferenceQueryPaging';
import type { AppFileReferenceQuerySorting } from './AppFileReferenceQuerySorting';
import type { AppFileReferenceSearchFilters } from './AppFileReferenceSearchFilters';
import type { AppFileReferenceSearchOptions } from './AppFileReferenceSearchOptions';

export type AppFileReferenceSearchParameters = {
    query?: AppFileReferenceFullTextQuery;
    filters?: AppFileReferenceSearchFilters;
    sorting?: AppFileReferenceQuerySorting;
    paging?: AppFileReferenceQueryPaging;
    options?: AppFileReferenceSearchOptions;
};

