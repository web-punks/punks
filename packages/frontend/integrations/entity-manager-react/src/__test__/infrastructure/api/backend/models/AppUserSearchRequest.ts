/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserSearchParameters } from './AppUserSearchParameters';

export type AppUserSearchRequest = {
    params: AppUserSearchParameters;
};

