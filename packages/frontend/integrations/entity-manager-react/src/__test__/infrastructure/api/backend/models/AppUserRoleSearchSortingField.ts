/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserRoleSearchSortingField = {
    field: AppUserRoleSearchSortingField.field;
    direction: AppUserRoleSearchSortingField.direction;
};

export namespace AppUserRoleSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

