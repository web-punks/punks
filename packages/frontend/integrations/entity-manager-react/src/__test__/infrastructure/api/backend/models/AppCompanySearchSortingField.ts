/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppCompanySearchSortingField = {
    field: AppCompanySearchSortingField.field;
    direction: AppCompanySearchSortingField.direction;
};

export namespace AppCompanySearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

