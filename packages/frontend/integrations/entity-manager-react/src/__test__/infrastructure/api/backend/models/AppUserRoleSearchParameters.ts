/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserRoleFullTextQuery } from './AppUserRoleFullTextQuery';
import type { AppUserRoleQueryPaging } from './AppUserRoleQueryPaging';
import type { AppUserRoleQuerySorting } from './AppUserRoleQuerySorting';
import type { AppUserRoleSearchFilters } from './AppUserRoleSearchFilters';
import type { AppUserRoleSearchOptions } from './AppUserRoleSearchOptions';

export type AppUserRoleSearchParameters = {
    query?: AppUserRoleFullTextQuery;
    filters?: AppUserRoleSearchFilters;
    sorting?: AppUserRoleQuerySorting;
    paging?: AppUserRoleQueryPaging;
    options?: AppUserRoleSearchOptions;
};

