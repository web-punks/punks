/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppCompanyListItemDto } from './AppCompanyListItemDto';
import type { AppCompanyVersionsResultsPaging } from './AppCompanyVersionsResultsPaging';

export type AppCompanyVersionsSearchResponse = {
    paging: AppCompanyVersionsResultsPaging;
    items: Array<AppCompanyListItemDto>;
};

