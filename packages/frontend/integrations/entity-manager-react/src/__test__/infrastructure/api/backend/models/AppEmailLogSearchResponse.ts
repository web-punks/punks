/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEmailLogFacets } from './AppEmailLogFacets';
import type { AppEmailLogListItemDto } from './AppEmailLogListItemDto';
import type { AppEmailLogSearchParameters } from './AppEmailLogSearchParameters';
import type { AppEmailLogSearchResultsPaging } from './AppEmailLogSearchResultsPaging';

export type AppEmailLogSearchResponse = {
    request: AppEmailLogSearchParameters;
    facets: AppEmailLogFacets;
    paging: AppEmailLogSearchResultsPaging;
    items: Array<AppEmailLogListItemDto>;
};

