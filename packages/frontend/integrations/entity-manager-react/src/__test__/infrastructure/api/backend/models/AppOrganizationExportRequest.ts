/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationEntitiesExportOptions } from './AppOrganizationEntitiesExportOptions';
import type { AppOrganizationSearchParameters } from './AppOrganizationSearchParameters';

export type AppOrganizationExportRequest = {
    options: AppOrganizationEntitiesExportOptions;
    filter?: AppOrganizationSearchParameters;
};

