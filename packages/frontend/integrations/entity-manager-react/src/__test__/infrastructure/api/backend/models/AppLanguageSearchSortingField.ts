/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppLanguageSearchSortingField = {
    field: AppLanguageSearchSortingField.field;
    direction: AppLanguageSearchSortingField.direction;
};

export namespace AppLanguageSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

