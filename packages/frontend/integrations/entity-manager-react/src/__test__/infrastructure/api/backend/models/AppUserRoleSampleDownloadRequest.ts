/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserRoleSampleDownloadRequest = {
    format: AppUserRoleSampleDownloadRequest.format;
};

export namespace AppUserRoleSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

