/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserPermission = {
    id: string;
    uid: UserPermission.uid;
    name: string;
};

export namespace UserPermission {

    export enum uid {
        TENANT_FULL_ACCESS = 'tenant-full-access',
        ORGANIZATION_FULL_ACCESS = 'organization-full-access',
        CALENDARS_FULL_ACCESS = 'calendars-full-access',
        CALENDARS_SCOPED_ACCESS = 'calendars-scoped-access',
        CRM_FULL_ACCESS = 'crm-full-access',
        CRM_SCOPED_ACCESS = 'crm-scoped-access',
        PROJECTS_FULL_ACCESS = 'projects-full-access',
        AUDIT_FULL_ACCESS = 'audit-full-access',
        JSON_INSPECT = 'json-inspect',
        USER_IMPERSONATE = 'user-impersonate',
    }


}

