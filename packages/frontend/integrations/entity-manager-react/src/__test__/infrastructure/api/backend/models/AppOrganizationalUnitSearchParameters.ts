/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitFullTextQuery } from './AppOrganizationalUnitFullTextQuery';
import type { AppOrganizationalUnitQueryPaging } from './AppOrganizationalUnitQueryPaging';
import type { AppOrganizationalUnitQuerySorting } from './AppOrganizationalUnitQuerySorting';
import type { AppOrganizationalUnitSearchFilters } from './AppOrganizationalUnitSearchFilters';
import type { AppOrganizationalUnitSearchOptions } from './AppOrganizationalUnitSearchOptions';
import type { EntitiesTraverseFilters } from './EntitiesTraverseFilters';

export type AppOrganizationalUnitSearchParameters = {
    query?: AppOrganizationalUnitFullTextQuery;
    filters?: AppOrganizationalUnitSearchFilters;
    traverse?: EntitiesTraverseFilters;
    sorting?: AppOrganizationalUnitQuerySorting;
    paging?: AppOrganizationalUnitQueryPaging;
    options?: AppOrganizationalUnitSearchOptions;
};

