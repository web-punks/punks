/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppFileReferenceListItemDto } from './AppFileReferenceListItemDto';
import type { AppFileReferenceVersionsResultsPaging } from './AppFileReferenceVersionsResultsPaging';

export type AppFileReferenceVersionsSearchResponse = {
    paging: AppFileReferenceVersionsResultsPaging;
    items: Array<AppFileReferenceListItemDto>;
};

