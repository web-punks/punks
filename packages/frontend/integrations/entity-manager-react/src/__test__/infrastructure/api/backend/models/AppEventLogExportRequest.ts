/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEventLogEntitiesExportOptions } from './AppEventLogEntitiesExportOptions';
import type { AppEventLogSearchParameters } from './AppEventLogSearchParameters';

export type AppEventLogExportRequest = {
    options: AppEventLogEntitiesExportOptions;
    filter?: AppEventLogSearchParameters;
};

