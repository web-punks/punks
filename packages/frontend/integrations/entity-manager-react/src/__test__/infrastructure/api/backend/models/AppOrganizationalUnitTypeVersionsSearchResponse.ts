/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeListItemDto } from './AppOrganizationalUnitTypeListItemDto';
import type { AppOrganizationalUnitTypeVersionsResultsPaging } from './AppOrganizationalUnitTypeVersionsResultsPaging';

export type AppOrganizationalUnitTypeVersionsSearchResponse = {
    paging: AppOrganizationalUnitTypeVersionsResultsPaging;
    items: Array<AppOrganizationalUnitTypeListItemDto>;
};

