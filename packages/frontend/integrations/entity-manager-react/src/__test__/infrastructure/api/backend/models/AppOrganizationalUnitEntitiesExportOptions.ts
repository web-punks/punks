/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationalUnitEntitiesExportOptions = {
    format: AppOrganizationalUnitEntitiesExportOptions.format;
};

export namespace AppOrganizationalUnitEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

