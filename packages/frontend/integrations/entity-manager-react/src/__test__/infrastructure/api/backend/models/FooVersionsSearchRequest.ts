/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FooVersionsSearchParameters } from './FooVersionsSearchParameters';

export type FooVersionsSearchRequest = {
    params: FooVersionsSearchParameters;
};

