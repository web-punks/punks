/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppLanguageSearchSortingField } from './AppLanguageSearchSortingField';

export type AppLanguageQuerySorting = {
    fields?: Array<AppLanguageSearchSortingField>;
};

