/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserSearchSortingField = {
    field: AppUserSearchSortingField.field;
    direction: AppUserSearchSortingField.direction;
};

export namespace AppUserSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

