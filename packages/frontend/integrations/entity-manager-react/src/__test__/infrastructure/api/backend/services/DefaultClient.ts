/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AppCacheInstanceDto } from '../models/AppCacheInstanceDto';
import type { AppCacheItemDto } from '../models/AppCacheItemDto';
import type { AppCacheListItemDto } from '../models/AppCacheListItemDto';
import type { AppCompanyCreateDto } from '../models/AppCompanyCreateDto';
import type { AppCompanyDto } from '../models/AppCompanyDto';
import type { AppCompanyExportRequest } from '../models/AppCompanyExportRequest';
import type { AppCompanyExportResponse } from '../models/AppCompanyExportResponse';
import type { AppCompanySampleDownloadRequest } from '../models/AppCompanySampleDownloadRequest';
import type { AppCompanySampleDownloadResponse } from '../models/AppCompanySampleDownloadResponse';
import type { AppCompanySearchRequest } from '../models/AppCompanySearchRequest';
import type { AppCompanySearchResponse } from '../models/AppCompanySearchResponse';
import type { AppCompanyUpdateDto } from '../models/AppCompanyUpdateDto';
import type { AppCompanyVersionsSearchRequest } from '../models/AppCompanyVersionsSearchRequest';
import type { AppCompanyVersionsSearchResponse } from '../models/AppCompanyVersionsSearchResponse';
import type { AppDirectoryCreateDto } from '../models/AppDirectoryCreateDto';
import type { AppDirectoryDto } from '../models/AppDirectoryDto';
import type { AppDirectoryExportRequest } from '../models/AppDirectoryExportRequest';
import type { AppDirectoryExportResponse } from '../models/AppDirectoryExportResponse';
import type { AppDirectorySampleDownloadRequest } from '../models/AppDirectorySampleDownloadRequest';
import type { AppDirectorySampleDownloadResponse } from '../models/AppDirectorySampleDownloadResponse';
import type { AppDirectorySearchRequest } from '../models/AppDirectorySearchRequest';
import type { AppDirectorySearchResponse } from '../models/AppDirectorySearchResponse';
import type { AppDirectoryUpdateDto } from '../models/AppDirectoryUpdateDto';
import type { AppDirectoryVersionsSearchRequest } from '../models/AppDirectoryVersionsSearchRequest';
import type { AppDirectoryVersionsSearchResponse } from '../models/AppDirectoryVersionsSearchResponse';
import type { AppDivisionCreateDto } from '../models/AppDivisionCreateDto';
import type { AppDivisionDto } from '../models/AppDivisionDto';
import type { AppDivisionExportRequest } from '../models/AppDivisionExportRequest';
import type { AppDivisionExportResponse } from '../models/AppDivisionExportResponse';
import type { AppDivisionSampleDownloadRequest } from '../models/AppDivisionSampleDownloadRequest';
import type { AppDivisionSampleDownloadResponse } from '../models/AppDivisionSampleDownloadResponse';
import type { AppDivisionSearchRequest } from '../models/AppDivisionSearchRequest';
import type { AppDivisionSearchResponse } from '../models/AppDivisionSearchResponse';
import type { AppDivisionUpdateDto } from '../models/AppDivisionUpdateDto';
import type { AppDivisionVersionsSearchRequest } from '../models/AppDivisionVersionsSearchRequest';
import type { AppDivisionVersionsSearchResponse } from '../models/AppDivisionVersionsSearchResponse';
import type { AppEmailLogCreateDto } from '../models/AppEmailLogCreateDto';
import type { AppEmailLogDto } from '../models/AppEmailLogDto';
import type { AppEmailLogExportRequest } from '../models/AppEmailLogExportRequest';
import type { AppEmailLogExportResponse } from '../models/AppEmailLogExportResponse';
import type { AppEmailLogSampleDownloadRequest } from '../models/AppEmailLogSampleDownloadRequest';
import type { AppEmailLogSampleDownloadResponse } from '../models/AppEmailLogSampleDownloadResponse';
import type { AppEmailLogSearchRequest } from '../models/AppEmailLogSearchRequest';
import type { AppEmailLogSearchResponse } from '../models/AppEmailLogSearchResponse';
import type { AppEmailLogUpdateDto } from '../models/AppEmailLogUpdateDto';
import type { AppEmailLogVersionsSearchRequest } from '../models/AppEmailLogVersionsSearchRequest';
import type { AppEmailLogVersionsSearchResponse } from '../models/AppEmailLogVersionsSearchResponse';
import type { AppEntityVersionCreateDto } from '../models/AppEntityVersionCreateDto';
import type { AppEntityVersionDto } from '../models/AppEntityVersionDto';
import type { AppEntityVersionExportRequest } from '../models/AppEntityVersionExportRequest';
import type { AppEntityVersionExportResponse } from '../models/AppEntityVersionExportResponse';
import type { AppEntityVersionSampleDownloadRequest } from '../models/AppEntityVersionSampleDownloadRequest';
import type { AppEntityVersionSampleDownloadResponse } from '../models/AppEntityVersionSampleDownloadResponse';
import type { AppEntityVersionSearchRequest } from '../models/AppEntityVersionSearchRequest';
import type { AppEntityVersionSearchResponse } from '../models/AppEntityVersionSearchResponse';
import type { AppEntityVersionUpdateDto } from '../models/AppEntityVersionUpdateDto';
import type { AppEntityVersionVersionsSearchRequest } from '../models/AppEntityVersionVersionsSearchRequest';
import type { AppEntityVersionVersionsSearchResponse } from '../models/AppEntityVersionVersionsSearchResponse';
import type { AppEventLogCreateDto } from '../models/AppEventLogCreateDto';
import type { AppEventLogDto } from '../models/AppEventLogDto';
import type { AppEventLogExportRequest } from '../models/AppEventLogExportRequest';
import type { AppEventLogExportResponse } from '../models/AppEventLogExportResponse';
import type { AppEventLogSampleDownloadRequest } from '../models/AppEventLogSampleDownloadRequest';
import type { AppEventLogSampleDownloadResponse } from '../models/AppEventLogSampleDownloadResponse';
import type { AppEventLogSearchRequest } from '../models/AppEventLogSearchRequest';
import type { AppEventLogSearchResponse } from '../models/AppEventLogSearchResponse';
import type { AppEventLogUpdateDto } from '../models/AppEventLogUpdateDto';
import type { AppEventLogVersionsSearchRequest } from '../models/AppEventLogVersionsSearchRequest';
import type { AppEventLogVersionsSearchResponse } from '../models/AppEventLogVersionsSearchResponse';
import type { AppFileReferenceCreateDto } from '../models/AppFileReferenceCreateDto';
import type { AppFileReferenceDto } from '../models/AppFileReferenceDto';
import type { AppFileReferenceExportRequest } from '../models/AppFileReferenceExportRequest';
import type { AppFileReferenceExportResponse } from '../models/AppFileReferenceExportResponse';
import type { AppFileReferenceSampleDownloadRequest } from '../models/AppFileReferenceSampleDownloadRequest';
import type { AppFileReferenceSampleDownloadResponse } from '../models/AppFileReferenceSampleDownloadResponse';
import type { AppFileReferenceSearchRequest } from '../models/AppFileReferenceSearchRequest';
import type { AppFileReferenceSearchResponse } from '../models/AppFileReferenceSearchResponse';
import type { AppFileReferenceUpdateDto } from '../models/AppFileReferenceUpdateDto';
import type { AppFileReferenceVersionsSearchRequest } from '../models/AppFileReferenceVersionsSearchRequest';
import type { AppFileReferenceVersionsSearchResponse } from '../models/AppFileReferenceVersionsSearchResponse';
import type { AppLanguageCreateDto } from '../models/AppLanguageCreateDto';
import type { AppLanguageDto } from '../models/AppLanguageDto';
import type { AppLanguageExportRequest } from '../models/AppLanguageExportRequest';
import type { AppLanguageExportResponse } from '../models/AppLanguageExportResponse';
import type { AppLanguageSampleDownloadRequest } from '../models/AppLanguageSampleDownloadRequest';
import type { AppLanguageSampleDownloadResponse } from '../models/AppLanguageSampleDownloadResponse';
import type { AppLanguageSearchRequest } from '../models/AppLanguageSearchRequest';
import type { AppLanguageSearchResponse } from '../models/AppLanguageSearchResponse';
import type { AppLanguageUpdateDto } from '../models/AppLanguageUpdateDto';
import type { AppLanguageVersionsSearchRequest } from '../models/AppLanguageVersionsSearchRequest';
import type { AppLanguageVersionsSearchResponse } from '../models/AppLanguageVersionsSearchResponse';
import type { AppOperationLockCreateDto } from '../models/AppOperationLockCreateDto';
import type { AppOperationLockDto } from '../models/AppOperationLockDto';
import type { AppOperationLockExportRequest } from '../models/AppOperationLockExportRequest';
import type { AppOperationLockExportResponse } from '../models/AppOperationLockExportResponse';
import type { AppOperationLockSampleDownloadRequest } from '../models/AppOperationLockSampleDownloadRequest';
import type { AppOperationLockSampleDownloadResponse } from '../models/AppOperationLockSampleDownloadResponse';
import type { AppOperationLockSearchRequest } from '../models/AppOperationLockSearchRequest';
import type { AppOperationLockSearchResponse } from '../models/AppOperationLockSearchResponse';
import type { AppOperationLockUpdateDto } from '../models/AppOperationLockUpdateDto';
import type { AppOperationLockVersionsSearchRequest } from '../models/AppOperationLockVersionsSearchRequest';
import type { AppOperationLockVersionsSearchResponse } from '../models/AppOperationLockVersionsSearchResponse';
import type { AppOrganizationalUnitCreateDto } from '../models/AppOrganizationalUnitCreateDto';
import type { AppOrganizationalUnitDto } from '../models/AppOrganizationalUnitDto';
import type { AppOrganizationalUnitExportRequest } from '../models/AppOrganizationalUnitExportRequest';
import type { AppOrganizationalUnitExportResponse } from '../models/AppOrganizationalUnitExportResponse';
import type { AppOrganizationalUnitSampleDownloadRequest } from '../models/AppOrganizationalUnitSampleDownloadRequest';
import type { AppOrganizationalUnitSampleDownloadResponse } from '../models/AppOrganizationalUnitSampleDownloadResponse';
import type { AppOrganizationalUnitSearchRequest } from '../models/AppOrganizationalUnitSearchRequest';
import type { AppOrganizationalUnitSearchResponse } from '../models/AppOrganizationalUnitSearchResponse';
import type { AppOrganizationalUnitTreeResponse } from '../models/AppOrganizationalUnitTreeResponse';
import type { AppOrganizationalUnitTypeChildCreateDto } from '../models/AppOrganizationalUnitTypeChildCreateDto';
import type { AppOrganizationalUnitTypeChildDto } from '../models/AppOrganizationalUnitTypeChildDto';
import type { AppOrganizationalUnitTypeChildExportRequest } from '../models/AppOrganizationalUnitTypeChildExportRequest';
import type { AppOrganizationalUnitTypeChildExportResponse } from '../models/AppOrganizationalUnitTypeChildExportResponse';
import type { AppOrganizationalUnitTypeChildSampleDownloadRequest } from '../models/AppOrganizationalUnitTypeChildSampleDownloadRequest';
import type { AppOrganizationalUnitTypeChildSampleDownloadResponse } from '../models/AppOrganizationalUnitTypeChildSampleDownloadResponse';
import type { AppOrganizationalUnitTypeChildSearchRequest } from '../models/AppOrganizationalUnitTypeChildSearchRequest';
import type { AppOrganizationalUnitTypeChildSearchResponse } from '../models/AppOrganizationalUnitTypeChildSearchResponse';
import type { AppOrganizationalUnitTypeChildUpdateDto } from '../models/AppOrganizationalUnitTypeChildUpdateDto';
import type { AppOrganizationalUnitTypeChildVersionsSearchRequest } from '../models/AppOrganizationalUnitTypeChildVersionsSearchRequest';
import type { AppOrganizationalUnitTypeChildVersionsSearchResponse } from '../models/AppOrganizationalUnitTypeChildVersionsSearchResponse';
import type { AppOrganizationalUnitTypeCreateDto } from '../models/AppOrganizationalUnitTypeCreateDto';
import type { AppOrganizationalUnitTypeDto } from '../models/AppOrganizationalUnitTypeDto';
import type { AppOrganizationalUnitTypeExportRequest } from '../models/AppOrganizationalUnitTypeExportRequest';
import type { AppOrganizationalUnitTypeExportResponse } from '../models/AppOrganizationalUnitTypeExportResponse';
import type { AppOrganizationalUnitTypeSampleDownloadRequest } from '../models/AppOrganizationalUnitTypeSampleDownloadRequest';
import type { AppOrganizationalUnitTypeSampleDownloadResponse } from '../models/AppOrganizationalUnitTypeSampleDownloadResponse';
import type { AppOrganizationalUnitTypeSearchRequest } from '../models/AppOrganizationalUnitTypeSearchRequest';
import type { AppOrganizationalUnitTypeSearchResponse } from '../models/AppOrganizationalUnitTypeSearchResponse';
import type { AppOrganizationalUnitTypeUpdateDto } from '../models/AppOrganizationalUnitTypeUpdateDto';
import type { AppOrganizationalUnitTypeVersionsSearchRequest } from '../models/AppOrganizationalUnitTypeVersionsSearchRequest';
import type { AppOrganizationalUnitTypeVersionsSearchResponse } from '../models/AppOrganizationalUnitTypeVersionsSearchResponse';
import type { AppOrganizationalUnitUpdateDto } from '../models/AppOrganizationalUnitUpdateDto';
import type { AppOrganizationalUnitVersionsSearchRequest } from '../models/AppOrganizationalUnitVersionsSearchRequest';
import type { AppOrganizationalUnitVersionsSearchResponse } from '../models/AppOrganizationalUnitVersionsSearchResponse';
import type { AppOrganizationCreateDto } from '../models/AppOrganizationCreateDto';
import type { AppOrganizationDto } from '../models/AppOrganizationDto';
import type { AppOrganizationExportRequest } from '../models/AppOrganizationExportRequest';
import type { AppOrganizationExportResponse } from '../models/AppOrganizationExportResponse';
import type { AppOrganizationSampleDownloadRequest } from '../models/AppOrganizationSampleDownloadRequest';
import type { AppOrganizationSampleDownloadResponse } from '../models/AppOrganizationSampleDownloadResponse';
import type { AppOrganizationSearchRequest } from '../models/AppOrganizationSearchRequest';
import type { AppOrganizationSearchResponse } from '../models/AppOrganizationSearchResponse';
import type { AppOrganizationUpdateDto } from '../models/AppOrganizationUpdateDto';
import type { AppOrganizationVersionsSearchRequest } from '../models/AppOrganizationVersionsSearchRequest';
import type { AppOrganizationVersionsSearchResponse } from '../models/AppOrganizationVersionsSearchResponse';
import type { AppPermissionCreateDto } from '../models/AppPermissionCreateDto';
import type { AppPermissionDto } from '../models/AppPermissionDto';
import type { AppPermissionExportRequest } from '../models/AppPermissionExportRequest';
import type { AppPermissionExportResponse } from '../models/AppPermissionExportResponse';
import type { AppPermissionSampleDownloadRequest } from '../models/AppPermissionSampleDownloadRequest';
import type { AppPermissionSampleDownloadResponse } from '../models/AppPermissionSampleDownloadResponse';
import type { AppPermissionSearchRequest } from '../models/AppPermissionSearchRequest';
import type { AppPermissionSearchResponse } from '../models/AppPermissionSearchResponse';
import type { AppPermissionUpdateDto } from '../models/AppPermissionUpdateDto';
import type { AppPermissionVersionsSearchRequest } from '../models/AppPermissionVersionsSearchRequest';
import type { AppPermissionVersionsSearchResponse } from '../models/AppPermissionVersionsSearchResponse';
import type { AppRoleCreateDto } from '../models/AppRoleCreateDto';
import type { AppRoleDto } from '../models/AppRoleDto';
import type { AppRoleExportRequest } from '../models/AppRoleExportRequest';
import type { AppRoleExportResponse } from '../models/AppRoleExportResponse';
import type { AppRoleSampleDownloadRequest } from '../models/AppRoleSampleDownloadRequest';
import type { AppRoleSampleDownloadResponse } from '../models/AppRoleSampleDownloadResponse';
import type { AppRoleSearchRequest } from '../models/AppRoleSearchRequest';
import type { AppRoleSearchResponse } from '../models/AppRoleSearchResponse';
import type { AppRoleUpdateDto } from '../models/AppRoleUpdateDto';
import type { AppRoleVersionsSearchRequest } from '../models/AppRoleVersionsSearchRequest';
import type { AppRoleVersionsSearchResponse } from '../models/AppRoleVersionsSearchResponse';
import type { AppTenantCreateDto } from '../models/AppTenantCreateDto';
import type { AppTenantDto } from '../models/AppTenantDto';
import type { AppTenantExportRequest } from '../models/AppTenantExportRequest';
import type { AppTenantExportResponse } from '../models/AppTenantExportResponse';
import type { AppTenantSampleDownloadRequest } from '../models/AppTenantSampleDownloadRequest';
import type { AppTenantSampleDownloadResponse } from '../models/AppTenantSampleDownloadResponse';
import type { AppTenantSearchRequest } from '../models/AppTenantSearchRequest';
import type { AppTenantSearchResponse } from '../models/AppTenantSearchResponse';
import type { AppTenantUpdateDto } from '../models/AppTenantUpdateDto';
import type { AppTenantVersionsSearchRequest } from '../models/AppTenantVersionsSearchRequest';
import type { AppTenantVersionsSearchResponse } from '../models/AppTenantVersionsSearchResponse';
import type { AppUserCreateDto } from '../models/AppUserCreateDto';
import type { AppUserDto } from '../models/AppUserDto';
import type { AppUserExportRequest } from '../models/AppUserExportRequest';
import type { AppUserExportResponse } from '../models/AppUserExportResponse';
import type { AppUserGroupCreateDto } from '../models/AppUserGroupCreateDto';
import type { AppUserGroupDto } from '../models/AppUserGroupDto';
import type { AppUserGroupExportRequest } from '../models/AppUserGroupExportRequest';
import type { AppUserGroupExportResponse } from '../models/AppUserGroupExportResponse';
import type { AppUserGroupMemberCreateDto } from '../models/AppUserGroupMemberCreateDto';
import type { AppUserGroupMemberDto } from '../models/AppUserGroupMemberDto';
import type { AppUserGroupMemberExportRequest } from '../models/AppUserGroupMemberExportRequest';
import type { AppUserGroupMemberExportResponse } from '../models/AppUserGroupMemberExportResponse';
import type { AppUserGroupMemberSampleDownloadRequest } from '../models/AppUserGroupMemberSampleDownloadRequest';
import type { AppUserGroupMemberSampleDownloadResponse } from '../models/AppUserGroupMemberSampleDownloadResponse';
import type { AppUserGroupMemberSearchRequest } from '../models/AppUserGroupMemberSearchRequest';
import type { AppUserGroupMemberSearchResponse } from '../models/AppUserGroupMemberSearchResponse';
import type { AppUserGroupMemberUpdateDto } from '../models/AppUserGroupMemberUpdateDto';
import type { AppUserGroupMemberVersionsSearchRequest } from '../models/AppUserGroupMemberVersionsSearchRequest';
import type { AppUserGroupMemberVersionsSearchResponse } from '../models/AppUserGroupMemberVersionsSearchResponse';
import type { AppUserGroupSampleDownloadRequest } from '../models/AppUserGroupSampleDownloadRequest';
import type { AppUserGroupSampleDownloadResponse } from '../models/AppUserGroupSampleDownloadResponse';
import type { AppUserGroupSearchRequest } from '../models/AppUserGroupSearchRequest';
import type { AppUserGroupSearchResponse } from '../models/AppUserGroupSearchResponse';
import type { AppUserGroupUpdateDto } from '../models/AppUserGroupUpdateDto';
import type { AppUserGroupVersionsSearchRequest } from '../models/AppUserGroupVersionsSearchRequest';
import type { AppUserGroupVersionsSearchResponse } from '../models/AppUserGroupVersionsSearchResponse';
import type { AppUserProfileCreateDto } from '../models/AppUserProfileCreateDto';
import type { AppUserProfileDto } from '../models/AppUserProfileDto';
import type { AppUserProfileExportRequest } from '../models/AppUserProfileExportRequest';
import type { AppUserProfileExportResponse } from '../models/AppUserProfileExportResponse';
import type { AppUserProfileSampleDownloadRequest } from '../models/AppUserProfileSampleDownloadRequest';
import type { AppUserProfileSampleDownloadResponse } from '../models/AppUserProfileSampleDownloadResponse';
import type { AppUserProfileSearchRequest } from '../models/AppUserProfileSearchRequest';
import type { AppUserProfileSearchResponse } from '../models/AppUserProfileSearchResponse';
import type { AppUserProfileUpdateDto } from '../models/AppUserProfileUpdateDto';
import type { AppUserProfileVersionsSearchRequest } from '../models/AppUserProfileVersionsSearchRequest';
import type { AppUserProfileVersionsSearchResponse } from '../models/AppUserProfileVersionsSearchResponse';
import type { AppUserRoleCreateDto } from '../models/AppUserRoleCreateDto';
import type { AppUserRoleDto } from '../models/AppUserRoleDto';
import type { AppUserRoleExportRequest } from '../models/AppUserRoleExportRequest';
import type { AppUserRoleExportResponse } from '../models/AppUserRoleExportResponse';
import type { AppUserRoleSampleDownloadRequest } from '../models/AppUserRoleSampleDownloadRequest';
import type { AppUserRoleSampleDownloadResponse } from '../models/AppUserRoleSampleDownloadResponse';
import type { AppUserRoleSearchRequest } from '../models/AppUserRoleSearchRequest';
import type { AppUserRoleSearchResponse } from '../models/AppUserRoleSearchResponse';
import type { AppUserRoleUpdateDto } from '../models/AppUserRoleUpdateDto';
import type { AppUserRoleVersionsSearchRequest } from '../models/AppUserRoleVersionsSearchRequest';
import type { AppUserRoleVersionsSearchResponse } from '../models/AppUserRoleVersionsSearchResponse';
import type { AppUserSampleDownloadRequest } from '../models/AppUserSampleDownloadRequest';
import type { AppUserSampleDownloadResponse } from '../models/AppUserSampleDownloadResponse';
import type { AppUserSearchRequest } from '../models/AppUserSearchRequest';
import type { AppUserSearchResponse } from '../models/AppUserSearchResponse';
import type { AppUserUpdateDto } from '../models/AppUserUpdateDto';
import type { AppUserVersionsSearchRequest } from '../models/AppUserVersionsSearchRequest';
import type { AppUserVersionsSearchResponse } from '../models/AppUserVersionsSearchResponse';
import type { CrmContactCreateDto } from '../models/CrmContactCreateDto';
import type { CrmContactDto } from '../models/CrmContactDto';
import type { CrmContactExportRequest } from '../models/CrmContactExportRequest';
import type { CrmContactExportResponse } from '../models/CrmContactExportResponse';
import type { CrmContactSampleDownloadRequest } from '../models/CrmContactSampleDownloadRequest';
import type { CrmContactSampleDownloadResponse } from '../models/CrmContactSampleDownloadResponse';
import type { CrmContactSearchRequest } from '../models/CrmContactSearchRequest';
import type { CrmContactSearchResponse } from '../models/CrmContactSearchResponse';
import type { CrmContactUpdateDto } from '../models/CrmContactUpdateDto';
import type { CrmContactVersionsSearchRequest } from '../models/CrmContactVersionsSearchRequest';
import type { CrmContactVersionsSearchResponse } from '../models/CrmContactVersionsSearchResponse';
import type { FileGetDownloadUrlResult } from '../models/FileGetDownloadUrlResult';
import type { FooCreateDto } from '../models/FooCreateDto';
import type { FooDto } from '../models/FooDto';
import type { FooExportRequest } from '../models/FooExportRequest';
import type { FooExportResponse } from '../models/FooExportResponse';
import type { FooParseResponse } from '../models/FooParseResponse';
import type { FooSampleDownloadRequest } from '../models/FooSampleDownloadRequest';
import type { FooSampleDownloadResponse } from '../models/FooSampleDownloadResponse';
import type { FooSearchRequest } from '../models/FooSearchRequest';
import type { FooSearchResponse } from '../models/FooSearchResponse';
import type { FooUpdateDto } from '../models/FooUpdateDto';
import type { FooVersionsSearchRequest } from '../models/FooVersionsSearchRequest';
import type { FooVersionsSearchResponse } from '../models/FooVersionsSearchResponse';
import type { OrganizationInitializeRequest } from '../models/OrganizationInitializeRequest';
import type { OrganizationInitializeResponse } from '../models/OrganizationInitializeResponse';
import type { TenantInitializeRequest } from '../models/TenantInitializeRequest';
import type { TenantInitializeResponse } from '../models/TenantInitializeResponse';
import type { UserCreateRequest } from '../models/UserCreateRequest';
import type { UserEmailVerifyCompleteRequest } from '../models/UserEmailVerifyCompleteRequest';
import type { UserEmailVerifyRequest } from '../models/UserEmailVerifyRequest';
import type { UserImpersonateResponse } from '../models/UserImpersonateResponse';
import type { UserLoginRequest } from '../models/UserLoginRequest';
import type { UserLoginResponse } from '../models/UserLoginResponse';
import type { UserPasswordResetCompleteRequest } from '../models/UserPasswordResetCompleteRequest';
import type { UserPasswordResetRequest } from '../models/UserPasswordResetRequest';
import type { UserPasswordResetResponse } from '../models/UserPasswordResetResponse';
import type { UserProfileResponse } from '../models/UserProfileResponse';
import type { UserRegisterRequest } from '../models/UserRegisterRequest';
import type { UserRegisterResponse } from '../models/UserRegisterResponse';
import type { UserUpdateRequest } from '../models/UserUpdateRequest';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class DefaultClient {

    /**
     * @returns any
     * @throws ApiError
     */
    public static appControllerIndex(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/',
        });
    }

    /**
     * @returns any
     * @throws ApiError
     */
    public static healthCheck(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/health',
        });
    }

    /**
     * @returns any
     * @throws ApiError
     */
    public static appControllerMigrate(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/migrate',
        });
    }

    /**
     * @returns UserProfileResponse
     * @throws ApiError
     */
    public static userProfile(): CancelablePromise<UserProfileResponse> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appAuth/profile',
        });
    }

    /**
     * @param requestBody
     * @returns UserLoginResponse
     * @throws ApiError
     */
    public static userLogin(
        requestBody: UserLoginRequest,
    ): CancelablePromise<UserLoginResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAuth/login',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns UserRegisterResponse
     * @throws ApiError
     */
    public static userRegister(
        requestBody: UserRegisterRequest,
    ): CancelablePromise<UserRegisterResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAuth/register',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns any
     * @throws ApiError
     */
    public static userVerify(
        requestBody: UserEmailVerifyRequest,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAuth/verify',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns any
     * @throws ApiError
     */
    public static userVerifyComplete(
        requestBody: UserEmailVerifyCompleteRequest,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAuth/verifyComplete',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns UserPasswordResetResponse
     * @throws ApiError
     */
    public static userPasswordReset(
        requestBody: UserPasswordResetRequest,
    ): CancelablePromise<UserPasswordResetResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAuth/passwordReset',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns any
     * @throws ApiError
     */
    public static userPasswordResetComplete(
        requestBody: UserPasswordResetCompleteRequest,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAuth/passwordResetComplete',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param userId
     * @returns UserImpersonateResponse
     * @throws ApiError
     */
    public static impersonate(
        userId: string,
    ): CancelablePromise<UserImpersonateResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAuth/impersonate/{userId}',
            path: {
                'userId': userId,
            },
        });
    }

    /**
     * @returns any
     * @throws ApiError
     */
    public static instanceInitialize(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAdmin/instanceInitialize',
        });
    }

    /**
     * @param requestBody
     * @returns TenantInitializeResponse
     * @throws ApiError
     */
    public static tenantInitialize(
        requestBody: TenantInitializeRequest,
    ): CancelablePromise<TenantInitializeResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAdmin/tenantInitialize',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns OrganizationInitializeResponse
     * @throws ApiError
     */
    public static organizationInitialize(
        requestBody: OrganizationInitializeRequest,
    ): CancelablePromise<OrganizationInitializeResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAdmin/organizationInitialize',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns OrganizationInitializeResponse
     * @throws ApiError
     */
    public static userCreate(
        requestBody: UserCreateRequest,
    ): CancelablePromise<OrganizationInitializeResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAdmin/userCreate',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns any
     * @throws ApiError
     */
    public static userUpdate(
        id: string,
        requestBody: UserUpdateRequest,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appAdmin/userUpdate/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static userDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appAdmin/userDelete/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @returns AppCacheInstanceDto
     * @throws ApiError
     */
    public static cacheInstances(): CancelablePromise<Array<AppCacheInstanceDto>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appCache/instances',
        });
    }

    /**
     * @param instanceName
     * @returns AppCacheListItemDto
     * @throws ApiError
     */
    public static cacheInstanceItems(
        instanceName: string,
    ): CancelablePromise<Array<AppCacheListItemDto>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appCache/getItems',
            query: {
                'instanceName': instanceName,
            },
        });
    }

    /**
     * @param instanceName
     * @param key
     * @returns AppCacheItemDto
     * @throws ApiError
     */
    public static cacheInstanceItem(
        instanceName: string,
        key: string,
    ): CancelablePromise<AppCacheItemDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appCache/getItem',
            query: {
                'instanceName': instanceName,
                'key': key,
            },
        });
    }

    /**
     * @param instanceName
     * @param key
     * @returns any
     * @throws ApiError
     */
    public static cacheInstanceItemDelete(
        instanceName: string,
        key: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appCache/deleteItem',
            query: {
                'instanceName': instanceName,
                'key': key,
            },
        });
    }

    /**
     * @param instanceName
     * @returns any
     * @throws ApiError
     */
    public static cacheInstanceClear(
        instanceName: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appCache/clearInstance',
            query: {
                'instanceName': instanceName,
            },
        });
    }

    /**
     * @param id
     * @returns AppCompanyDto
     * @throws ApiError
     */
    public static appCompanyGet(
        id: string,
    ): CancelablePromise<AppCompanyDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appCompany/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppCompanyDto
     * @throws ApiError
     */
    public static appCompanyCreate(
        requestBody: AppCompanyCreateDto,
    ): CancelablePromise<AppCompanyDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appCompany/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppCompanyDto
     * @throws ApiError
     */
    public static appCompanyUpdate(
        id: string,
        requestBody: AppCompanyUpdateDto,
    ): CancelablePromise<AppCompanyDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appCompany/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appCompanyDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appCompany/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppCompanySearchResponse
     * @throws ApiError
     */
    public static appCompanySearch(
        requestBody: AppCompanySearchRequest,
    ): CancelablePromise<AppCompanySearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appCompany/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appCompanyImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appCompany/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppCompanyExportResponse
     * @throws ApiError
     */
    public static appCompanyExport(
        requestBody: AppCompanyExportRequest,
    ): CancelablePromise<AppCompanyExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appCompany/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppCompanySampleDownloadResponse
     * @throws ApiError
     */
    public static appCompanySampleDownload(
        requestBody: AppCompanySampleDownloadRequest,
    ): CancelablePromise<AppCompanySampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appCompany/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppCompanyVersionsSearchResponse
     * @throws ApiError
     */
    public static appCompanyVersions(
        requestBody: AppCompanyVersionsSearchRequest,
    ): CancelablePromise<AppCompanyVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appCompany/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppDirectoryDto
     * @throws ApiError
     */
    public static appDirectoryGet(
        id: string,
    ): CancelablePromise<AppDirectoryDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appDirectory/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppDirectoryDto
     * @throws ApiError
     */
    public static appDirectoryCreate(
        requestBody: AppDirectoryCreateDto,
    ): CancelablePromise<AppDirectoryDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appDirectory/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppDirectoryDto
     * @throws ApiError
     */
    public static appDirectoryUpdate(
        id: string,
        requestBody: AppDirectoryUpdateDto,
    ): CancelablePromise<AppDirectoryDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDirectory/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appDirectoryDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appDirectory/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppDirectorySearchResponse
     * @throws ApiError
     */
    public static appDirectorySearch(
        requestBody: AppDirectorySearchRequest,
    ): CancelablePromise<AppDirectorySearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDirectory/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appDirectoryImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDirectory/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppDirectoryExportResponse
     * @throws ApiError
     */
    public static appDirectoryExport(
        requestBody: AppDirectoryExportRequest,
    ): CancelablePromise<AppDirectoryExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDirectory/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppDirectorySampleDownloadResponse
     * @throws ApiError
     */
    public static appDirectorySampleDownload(
        requestBody: AppDirectorySampleDownloadRequest,
    ): CancelablePromise<AppDirectorySampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDirectory/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppDirectoryVersionsSearchResponse
     * @throws ApiError
     */
    public static appDirectoryVersions(
        requestBody: AppDirectoryVersionsSearchRequest,
    ): CancelablePromise<AppDirectoryVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDirectory/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppDivisionDto
     * @throws ApiError
     */
    public static appDivisionGet(
        id: string,
    ): CancelablePromise<AppDivisionDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appDivision/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppDivisionDto
     * @throws ApiError
     */
    public static appDivisionCreate(
        requestBody: AppDivisionCreateDto,
    ): CancelablePromise<AppDivisionDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appDivision/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppDivisionDto
     * @throws ApiError
     */
    public static appDivisionUpdate(
        id: string,
        requestBody: AppDivisionUpdateDto,
    ): CancelablePromise<AppDivisionDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDivision/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appDivisionDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appDivision/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppDivisionSearchResponse
     * @throws ApiError
     */
    public static appDivisionSearch(
        requestBody: AppDivisionSearchRequest,
    ): CancelablePromise<AppDivisionSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDivision/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appDivisionImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDivision/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppDivisionExportResponse
     * @throws ApiError
     */
    public static appDivisionExport(
        requestBody: AppDivisionExportRequest,
    ): CancelablePromise<AppDivisionExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDivision/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppDivisionSampleDownloadResponse
     * @throws ApiError
     */
    public static appDivisionSampleDownload(
        requestBody: AppDivisionSampleDownloadRequest,
    ): CancelablePromise<AppDivisionSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDivision/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppDivisionVersionsSearchResponse
     * @throws ApiError
     */
    public static appDivisionVersions(
        requestBody: AppDivisionVersionsSearchRequest,
    ): CancelablePromise<AppDivisionVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appDivision/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppEmailLogDto
     * @throws ApiError
     */
    public static appEmailLogGet(
        id: string,
    ): CancelablePromise<AppEmailLogDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appEmailLog/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppEmailLogDto
     * @throws ApiError
     */
    public static appEmailLogCreate(
        requestBody: AppEmailLogCreateDto,
    ): CancelablePromise<AppEmailLogDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appEmailLog/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppEmailLogDto
     * @throws ApiError
     */
    public static appEmailLogUpdate(
        id: string,
        requestBody: AppEmailLogUpdateDto,
    ): CancelablePromise<AppEmailLogDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEmailLog/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appEmailLogDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appEmailLog/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppEmailLogSearchResponse
     * @throws ApiError
     */
    public static appEmailLogSearch(
        requestBody: AppEmailLogSearchRequest,
    ): CancelablePromise<AppEmailLogSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEmailLog/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appEmailLogImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEmailLog/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppEmailLogExportResponse
     * @throws ApiError
     */
    public static appEmailLogExport(
        requestBody: AppEmailLogExportRequest,
    ): CancelablePromise<AppEmailLogExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEmailLog/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppEmailLogSampleDownloadResponse
     * @throws ApiError
     */
    public static appEmailLogSampleDownload(
        requestBody: AppEmailLogSampleDownloadRequest,
    ): CancelablePromise<AppEmailLogSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEmailLog/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppEmailLogVersionsSearchResponse
     * @throws ApiError
     */
    public static appEmailLogVersions(
        requestBody: AppEmailLogVersionsSearchRequest,
    ): CancelablePromise<AppEmailLogVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEmailLog/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppEventLogDto
     * @throws ApiError
     */
    public static appEventLogGet(
        id: string,
    ): CancelablePromise<AppEventLogDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appEventLog/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppEventLogDto
     * @throws ApiError
     */
    public static appEventLogCreate(
        requestBody: AppEventLogCreateDto,
    ): CancelablePromise<AppEventLogDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appEventLog/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppEventLogDto
     * @throws ApiError
     */
    public static appEventLogUpdate(
        id: string,
        requestBody: AppEventLogUpdateDto,
    ): CancelablePromise<AppEventLogDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEventLog/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appEventLogDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appEventLog/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppEventLogSearchResponse
     * @throws ApiError
     */
    public static appEventLogSearch(
        requestBody: AppEventLogSearchRequest,
    ): CancelablePromise<AppEventLogSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEventLog/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appEventLogImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEventLog/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppEventLogExportResponse
     * @throws ApiError
     */
    public static appEventLogExport(
        requestBody: AppEventLogExportRequest,
    ): CancelablePromise<AppEventLogExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEventLog/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppEventLogSampleDownloadResponse
     * @throws ApiError
     */
    public static appEventLogSampleDownload(
        requestBody: AppEventLogSampleDownloadRequest,
    ): CancelablePromise<AppEventLogSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEventLog/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppEventLogVersionsSearchResponse
     * @throws ApiError
     */
    public static appEventLogVersions(
        requestBody: AppEventLogVersionsSearchRequest,
    ): CancelablePromise<AppEventLogVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEventLog/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppEntityVersionDto
     * @throws ApiError
     */
    public static appEntityVersionGet(
        id: string,
    ): CancelablePromise<AppEntityVersionDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appEntityVersion/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppEntityVersionDto
     * @throws ApiError
     */
    public static appEntityVersionCreate(
        requestBody: AppEntityVersionCreateDto,
    ): CancelablePromise<AppEntityVersionDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appEntityVersion/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppEntityVersionDto
     * @throws ApiError
     */
    public static appEntityVersionUpdate(
        id: string,
        requestBody: AppEntityVersionUpdateDto,
    ): CancelablePromise<AppEntityVersionDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEntityVersion/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appEntityVersionDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appEntityVersion/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppEntityVersionSearchResponse
     * @throws ApiError
     */
    public static appEntityVersionSearch(
        requestBody: AppEntityVersionSearchRequest,
    ): CancelablePromise<AppEntityVersionSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEntityVersion/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appEntityVersionImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEntityVersion/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppEntityVersionExportResponse
     * @throws ApiError
     */
    public static appEntityVersionExport(
        requestBody: AppEntityVersionExportRequest,
    ): CancelablePromise<AppEntityVersionExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEntityVersion/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppEntityVersionSampleDownloadResponse
     * @throws ApiError
     */
    public static appEntityVersionSampleDownload(
        requestBody: AppEntityVersionSampleDownloadRequest,
    ): CancelablePromise<AppEntityVersionSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEntityVersion/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppEntityVersionVersionsSearchResponse
     * @throws ApiError
     */
    public static appEntityVersionVersions(
        requestBody: AppEntityVersionVersionsSearchRequest,
    ): CancelablePromise<AppEntityVersionVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appEntityVersion/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static fileUpload(
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appFile/upload',
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @returns FileGetDownloadUrlResult
     * @throws ApiError
     */
    public static getFileDownloadUrl(): CancelablePromise<FileGetDownloadUrlResult> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appFile/getDownloadUrl',
        });
    }

    /**
     * @param id
     * @returns AppFileReferenceDto
     * @throws ApiError
     */
    public static appFileReferenceGet(
        id: string,
    ): CancelablePromise<AppFileReferenceDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appFileReference/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppFileReferenceDto
     * @throws ApiError
     */
    public static appFileReferenceCreate(
        requestBody: AppFileReferenceCreateDto,
    ): CancelablePromise<AppFileReferenceDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appFileReference/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppFileReferenceDto
     * @throws ApiError
     */
    public static appFileReferenceUpdate(
        id: string,
        requestBody: AppFileReferenceUpdateDto,
    ): CancelablePromise<AppFileReferenceDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appFileReference/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appFileReferenceDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appFileReference/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppFileReferenceSearchResponse
     * @throws ApiError
     */
    public static appFileReferenceSearch(
        requestBody: AppFileReferenceSearchRequest,
    ): CancelablePromise<AppFileReferenceSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appFileReference/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appFileReferenceImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appFileReference/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppFileReferenceExportResponse
     * @throws ApiError
     */
    public static appFileReferenceExport(
        requestBody: AppFileReferenceExportRequest,
    ): CancelablePromise<AppFileReferenceExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appFileReference/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppFileReferenceSampleDownloadResponse
     * @throws ApiError
     */
    public static appFileReferenceSampleDownload(
        requestBody: AppFileReferenceSampleDownloadRequest,
    ): CancelablePromise<AppFileReferenceSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appFileReference/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppFileReferenceVersionsSearchResponse
     * @throws ApiError
     */
    public static appFileReferenceVersions(
        requestBody: AppFileReferenceVersionsSearchRequest,
    ): CancelablePromise<AppFileReferenceVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appFileReference/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppLanguageDto
     * @throws ApiError
     */
    public static appLanguageGet(
        id: string,
    ): CancelablePromise<AppLanguageDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appLanguage/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppLanguageDto
     * @throws ApiError
     */
    public static appLanguageCreate(
        requestBody: AppLanguageCreateDto,
    ): CancelablePromise<AppLanguageDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appLanguage/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppLanguageDto
     * @throws ApiError
     */
    public static appLanguageUpdate(
        id: string,
        requestBody: AppLanguageUpdateDto,
    ): CancelablePromise<AppLanguageDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appLanguage/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appLanguageDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appLanguage/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppLanguageSearchResponse
     * @throws ApiError
     */
    public static appLanguageSearch(
        requestBody: AppLanguageSearchRequest,
    ): CancelablePromise<AppLanguageSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appLanguage/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appLanguageImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appLanguage/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppLanguageExportResponse
     * @throws ApiError
     */
    public static appLanguageExport(
        requestBody: AppLanguageExportRequest,
    ): CancelablePromise<AppLanguageExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appLanguage/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppLanguageSampleDownloadResponse
     * @throws ApiError
     */
    public static appLanguageSampleDownload(
        requestBody: AppLanguageSampleDownloadRequest,
    ): CancelablePromise<AppLanguageSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appLanguage/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppLanguageVersionsSearchResponse
     * @throws ApiError
     */
    public static appLanguageVersions(
        requestBody: AppLanguageVersionsSearchRequest,
    ): CancelablePromise<AppLanguageVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appLanguage/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppOperationLockDto
     * @throws ApiError
     */
    public static appOperationLockGet(
        id: string,
    ): CancelablePromise<AppOperationLockDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appOperationLock/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppOperationLockDto
     * @throws ApiError
     */
    public static appOperationLockCreate(
        requestBody: AppOperationLockCreateDto,
    ): CancelablePromise<AppOperationLockDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appOperationLock/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppOperationLockDto
     * @throws ApiError
     */
    public static appOperationLockUpdate(
        id: string,
        requestBody: AppOperationLockUpdateDto,
    ): CancelablePromise<AppOperationLockDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOperationLock/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appOperationLockDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appOperationLock/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppOperationLockSearchResponse
     * @throws ApiError
     */
    public static appOperationLockSearch(
        requestBody: AppOperationLockSearchRequest,
    ): CancelablePromise<AppOperationLockSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOperationLock/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appOperationLockImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOperationLock/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppOperationLockExportResponse
     * @throws ApiError
     */
    public static appOperationLockExport(
        requestBody: AppOperationLockExportRequest,
    ): CancelablePromise<AppOperationLockExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOperationLock/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppOperationLockSampleDownloadResponse
     * @throws ApiError
     */
    public static appOperationLockSampleDownload(
        requestBody: AppOperationLockSampleDownloadRequest,
    ): CancelablePromise<AppOperationLockSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOperationLock/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppOperationLockVersionsSearchResponse
     * @throws ApiError
     */
    public static appOperationLockVersions(
        requestBody: AppOperationLockVersionsSearchRequest,
    ): CancelablePromise<AppOperationLockVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOperationLock/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppOrganizationDto
     * @throws ApiError
     */
    public static appOrganizationGet(
        id: string,
    ): CancelablePromise<AppOrganizationDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appOrganization/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationDto
     * @throws ApiError
     */
    public static appOrganizationCreate(
        requestBody: AppOrganizationCreateDto,
    ): CancelablePromise<AppOrganizationDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appOrganization/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppOrganizationDto
     * @throws ApiError
     */
    public static appOrganizationUpdate(
        id: string,
        requestBody: AppOrganizationUpdateDto,
    ): CancelablePromise<AppOrganizationDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganization/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appOrganizationDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appOrganization/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationSearchResponse
     * @throws ApiError
     */
    public static appOrganizationSearch(
        requestBody: AppOrganizationSearchRequest,
    ): CancelablePromise<AppOrganizationSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganization/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appOrganizationImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganization/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationExportResponse
     * @throws ApiError
     */
    public static appOrganizationExport(
        requestBody: AppOrganizationExportRequest,
    ): CancelablePromise<AppOrganizationExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganization/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationSampleDownloadResponse
     * @throws ApiError
     */
    public static appOrganizationSampleDownload(
        requestBody: AppOrganizationSampleDownloadRequest,
    ): CancelablePromise<AppOrganizationSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganization/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationVersionsSearchResponse
     * @throws ApiError
     */
    public static appOrganizationVersions(
        requestBody: AppOrganizationVersionsSearchRequest,
    ): CancelablePromise<AppOrganizationVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganization/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppOrganizationalUnitDto
     * @throws ApiError
     */
    public static appOrganizationalUnitGet(
        id: string,
    ): CancelablePromise<AppOrganizationalUnitDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appOrganizationalUnit/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitDto
     * @throws ApiError
     */
    public static appOrganizationalUnitCreate(
        requestBody: AppOrganizationalUnitCreateDto,
    ): CancelablePromise<AppOrganizationalUnitDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appOrganizationalUnit/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppOrganizationalUnitDto
     * @throws ApiError
     */
    public static appOrganizationalUnitUpdate(
        id: string,
        requestBody: AppOrganizationalUnitUpdateDto,
    ): CancelablePromise<AppOrganizationalUnitDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnit/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appOrganizationalUnitDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appOrganizationalUnit/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitSearchResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitSearch(
        requestBody: AppOrganizationalUnitSearchRequest,
    ): CancelablePromise<AppOrganizationalUnitSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnit/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitTreeResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitTree(
        requestBody: AppOrganizationalUnitSearchRequest,
    ): CancelablePromise<AppOrganizationalUnitTreeResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnit/tree',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appOrganizationalUnitImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnit/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitExportResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitExport(
        requestBody: AppOrganizationalUnitExportRequest,
    ): CancelablePromise<AppOrganizationalUnitExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnit/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitSampleDownloadResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitSampleDownload(
        requestBody: AppOrganizationalUnitSampleDownloadRequest,
    ): CancelablePromise<AppOrganizationalUnitSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnit/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitVersionsSearchResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitVersions(
        requestBody: AppOrganizationalUnitVersionsSearchRequest,
    ): CancelablePromise<AppOrganizationalUnitVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnit/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppOrganizationalUnitTypeDto
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeGet(
        id: string,
    ): CancelablePromise<AppOrganizationalUnitTypeDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appOrganizationalUnitType/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitTypeDto
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeCreate(
        requestBody: AppOrganizationalUnitTypeCreateDto,
    ): CancelablePromise<AppOrganizationalUnitTypeDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appOrganizationalUnitType/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppOrganizationalUnitTypeDto
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeUpdate(
        id: string,
        requestBody: AppOrganizationalUnitTypeUpdateDto,
    ): CancelablePromise<AppOrganizationalUnitTypeDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitType/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appOrganizationalUnitType/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitTypeSearchResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeSearch(
        requestBody: AppOrganizationalUnitTypeSearchRequest,
    ): CancelablePromise<AppOrganizationalUnitTypeSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitType/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitType/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitTypeExportResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeExport(
        requestBody: AppOrganizationalUnitTypeExportRequest,
    ): CancelablePromise<AppOrganizationalUnitTypeExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitType/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitTypeSampleDownloadResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeSampleDownload(
        requestBody: AppOrganizationalUnitTypeSampleDownloadRequest,
    ): CancelablePromise<AppOrganizationalUnitTypeSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitType/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitTypeVersionsSearchResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeVersions(
        requestBody: AppOrganizationalUnitTypeVersionsSearchRequest,
    ): CancelablePromise<AppOrganizationalUnitTypeVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitType/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppOrganizationalUnitTypeChildDto
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeChildGet(
        id: string,
    ): CancelablePromise<AppOrganizationalUnitTypeChildDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appOrganizationalUnitTypeChild/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitTypeChildDto
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeChildCreate(
        requestBody: AppOrganizationalUnitTypeChildCreateDto,
    ): CancelablePromise<AppOrganizationalUnitTypeChildDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appOrganizationalUnitTypeChild/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppOrganizationalUnitTypeChildDto
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeChildUpdate(
        id: string,
        requestBody: AppOrganizationalUnitTypeChildUpdateDto,
    ): CancelablePromise<AppOrganizationalUnitTypeChildDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitTypeChild/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeChildDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appOrganizationalUnitTypeChild/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitTypeChildSearchResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeChildSearch(
        requestBody: AppOrganizationalUnitTypeChildSearchRequest,
    ): CancelablePromise<AppOrganizationalUnitTypeChildSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitTypeChild/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeChildImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitTypeChild/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitTypeChildExportResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeChildExport(
        requestBody: AppOrganizationalUnitTypeChildExportRequest,
    ): CancelablePromise<AppOrganizationalUnitTypeChildExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitTypeChild/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitTypeChildSampleDownloadResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeChildSampleDownload(
        requestBody: AppOrganizationalUnitTypeChildSampleDownloadRequest,
    ): CancelablePromise<AppOrganizationalUnitTypeChildSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitTypeChild/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppOrganizationalUnitTypeChildVersionsSearchResponse
     * @throws ApiError
     */
    public static appOrganizationalUnitTypeChildVersions(
        requestBody: AppOrganizationalUnitTypeChildVersionsSearchRequest,
    ): CancelablePromise<AppOrganizationalUnitTypeChildVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appOrganizationalUnitTypeChild/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppPermissionDto
     * @throws ApiError
     */
    public static appPermissionGet(
        id: string,
    ): CancelablePromise<AppPermissionDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appPermission/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppPermissionDto
     * @throws ApiError
     */
    public static appPermissionCreate(
        requestBody: AppPermissionCreateDto,
    ): CancelablePromise<AppPermissionDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appPermission/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppPermissionDto
     * @throws ApiError
     */
    public static appPermissionUpdate(
        id: string,
        requestBody: AppPermissionUpdateDto,
    ): CancelablePromise<AppPermissionDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appPermission/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appPermissionDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appPermission/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppPermissionSearchResponse
     * @throws ApiError
     */
    public static appPermissionSearch(
        requestBody: AppPermissionSearchRequest,
    ): CancelablePromise<AppPermissionSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appPermission/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appPermissionImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appPermission/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppPermissionExportResponse
     * @throws ApiError
     */
    public static appPermissionExport(
        requestBody: AppPermissionExportRequest,
    ): CancelablePromise<AppPermissionExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appPermission/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppPermissionSampleDownloadResponse
     * @throws ApiError
     */
    public static appPermissionSampleDownload(
        requestBody: AppPermissionSampleDownloadRequest,
    ): CancelablePromise<AppPermissionSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appPermission/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppPermissionVersionsSearchResponse
     * @throws ApiError
     */
    public static appPermissionVersions(
        requestBody: AppPermissionVersionsSearchRequest,
    ): CancelablePromise<AppPermissionVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appPermission/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppRoleDto
     * @throws ApiError
     */
    public static appRoleGet(
        id: string,
    ): CancelablePromise<AppRoleDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appRole/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppRoleDto
     * @throws ApiError
     */
    public static appRoleCreate(
        requestBody: AppRoleCreateDto,
    ): CancelablePromise<AppRoleDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appRole/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppRoleDto
     * @throws ApiError
     */
    public static appRoleUpdate(
        id: string,
        requestBody: AppRoleUpdateDto,
    ): CancelablePromise<AppRoleDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appRole/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appRoleDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appRole/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppRoleSearchResponse
     * @throws ApiError
     */
    public static appRoleSearch(
        requestBody: AppRoleSearchRequest,
    ): CancelablePromise<AppRoleSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appRole/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appRoleImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appRole/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppRoleExportResponse
     * @throws ApiError
     */
    public static appRoleExport(
        requestBody: AppRoleExportRequest,
    ): CancelablePromise<AppRoleExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appRole/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppRoleSampleDownloadResponse
     * @throws ApiError
     */
    public static appRoleSampleDownload(
        requestBody: AppRoleSampleDownloadRequest,
    ): CancelablePromise<AppRoleSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appRole/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppRoleVersionsSearchResponse
     * @throws ApiError
     */
    public static appRoleVersions(
        requestBody: AppRoleVersionsSearchRequest,
    ): CancelablePromise<AppRoleVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appRole/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppTenantDto
     * @throws ApiError
     */
    public static appTenantGet(
        id: string,
    ): CancelablePromise<AppTenantDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appTenant/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppTenantDto
     * @throws ApiError
     */
    public static appTenantCreate(
        requestBody: AppTenantCreateDto,
    ): CancelablePromise<AppTenantDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appTenant/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppTenantDto
     * @throws ApiError
     */
    public static appTenantUpdate(
        id: string,
        requestBody: AppTenantUpdateDto,
    ): CancelablePromise<AppTenantDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appTenant/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appTenantDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appTenant/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppTenantSearchResponse
     * @throws ApiError
     */
    public static appTenantSearch(
        requestBody: AppTenantSearchRequest,
    ): CancelablePromise<AppTenantSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appTenant/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appTenantImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appTenant/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppTenantExportResponse
     * @throws ApiError
     */
    public static appTenantExport(
        requestBody: AppTenantExportRequest,
    ): CancelablePromise<AppTenantExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appTenant/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppTenantSampleDownloadResponse
     * @throws ApiError
     */
    public static appTenantSampleDownload(
        requestBody: AppTenantSampleDownloadRequest,
    ): CancelablePromise<AppTenantSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appTenant/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppTenantVersionsSearchResponse
     * @throws ApiError
     */
    public static appTenantVersions(
        requestBody: AppTenantVersionsSearchRequest,
    ): CancelablePromise<AppTenantVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appTenant/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppUserGroupMemberDto
     * @throws ApiError
     */
    public static appUserGroupMemberGet(
        id: string,
    ): CancelablePromise<AppUserGroupMemberDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appUserGroupMember/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppUserGroupMemberDto
     * @throws ApiError
     */
    public static appUserGroupMemberCreate(
        requestBody: AppUserGroupMemberCreateDto,
    ): CancelablePromise<AppUserGroupMemberDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appUserGroupMember/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppUserGroupMemberDto
     * @throws ApiError
     */
    public static appUserGroupMemberUpdate(
        id: string,
        requestBody: AppUserGroupMemberUpdateDto,
    ): CancelablePromise<AppUserGroupMemberDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroupMember/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appUserGroupMemberDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appUserGroupMember/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppUserGroupMemberSearchResponse
     * @throws ApiError
     */
    public static appUserGroupMemberSearch(
        requestBody: AppUserGroupMemberSearchRequest,
    ): CancelablePromise<AppUserGroupMemberSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroupMember/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appUserGroupMemberImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroupMember/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserGroupMemberExportResponse
     * @throws ApiError
     */
    public static appUserGroupMemberExport(
        requestBody: AppUserGroupMemberExportRequest,
    ): CancelablePromise<AppUserGroupMemberExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroupMember/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserGroupMemberSampleDownloadResponse
     * @throws ApiError
     */
    public static appUserGroupMemberSampleDownload(
        requestBody: AppUserGroupMemberSampleDownloadRequest,
    ): CancelablePromise<AppUserGroupMemberSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroupMember/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserGroupMemberVersionsSearchResponse
     * @throws ApiError
     */
    public static appUserGroupMemberVersions(
        requestBody: AppUserGroupMemberVersionsSearchRequest,
    ): CancelablePromise<AppUserGroupMemberVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroupMember/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppUserGroupDto
     * @throws ApiError
     */
    public static appUserGroupGet(
        id: string,
    ): CancelablePromise<AppUserGroupDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appUserGroup/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppUserGroupDto
     * @throws ApiError
     */
    public static appUserGroupCreate(
        requestBody: AppUserGroupCreateDto,
    ): CancelablePromise<AppUserGroupDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appUserGroup/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppUserGroupDto
     * @throws ApiError
     */
    public static appUserGroupUpdate(
        id: string,
        requestBody: AppUserGroupUpdateDto,
    ): CancelablePromise<AppUserGroupDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroup/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appUserGroupDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appUserGroup/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppUserGroupSearchResponse
     * @throws ApiError
     */
    public static appUserGroupSearch(
        requestBody: AppUserGroupSearchRequest,
    ): CancelablePromise<AppUserGroupSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroup/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appUserGroupImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroup/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserGroupExportResponse
     * @throws ApiError
     */
    public static appUserGroupExport(
        requestBody: AppUserGroupExportRequest,
    ): CancelablePromise<AppUserGroupExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroup/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserGroupSampleDownloadResponse
     * @throws ApiError
     */
    public static appUserGroupSampleDownload(
        requestBody: AppUserGroupSampleDownloadRequest,
    ): CancelablePromise<AppUserGroupSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroup/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserGroupVersionsSearchResponse
     * @throws ApiError
     */
    public static appUserGroupVersions(
        requestBody: AppUserGroupVersionsSearchRequest,
    ): CancelablePromise<AppUserGroupVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserGroup/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppUserProfileDto
     * @throws ApiError
     */
    public static appUserProfileGet(
        id: string,
    ): CancelablePromise<AppUserProfileDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appUserProfile/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppUserProfileDto
     * @throws ApiError
     */
    public static appUserProfileCreate(
        requestBody: AppUserProfileCreateDto,
    ): CancelablePromise<AppUserProfileDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appUserProfile/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppUserProfileDto
     * @throws ApiError
     */
    public static appUserProfileUpdate(
        id: string,
        requestBody: AppUserProfileUpdateDto,
    ): CancelablePromise<AppUserProfileDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserProfile/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appUserProfileDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appUserProfile/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppUserProfileSearchResponse
     * @throws ApiError
     */
    public static appUserProfileSearch(
        requestBody: AppUserProfileSearchRequest,
    ): CancelablePromise<AppUserProfileSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserProfile/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appUserProfileImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserProfile/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserProfileExportResponse
     * @throws ApiError
     */
    public static appUserProfileExport(
        requestBody: AppUserProfileExportRequest,
    ): CancelablePromise<AppUserProfileExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserProfile/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserProfileSampleDownloadResponse
     * @throws ApiError
     */
    public static appUserProfileSampleDownload(
        requestBody: AppUserProfileSampleDownloadRequest,
    ): CancelablePromise<AppUserProfileSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserProfile/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserProfileVersionsSearchResponse
     * @throws ApiError
     */
    public static appUserProfileVersions(
        requestBody: AppUserProfileVersionsSearchRequest,
    ): CancelablePromise<AppUserProfileVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserProfile/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppUserRoleDto
     * @throws ApiError
     */
    public static appUserRoleGet(
        id: string,
    ): CancelablePromise<AppUserRoleDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appUserRole/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppUserRoleDto
     * @throws ApiError
     */
    public static appUserRoleCreate(
        requestBody: AppUserRoleCreateDto,
    ): CancelablePromise<AppUserRoleDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appUserRole/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppUserRoleDto
     * @throws ApiError
     */
    public static appUserRoleUpdate(
        id: string,
        requestBody: AppUserRoleUpdateDto,
    ): CancelablePromise<AppUserRoleDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserRole/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appUserRoleDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appUserRole/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppUserRoleSearchResponse
     * @throws ApiError
     */
    public static appUserRoleSearch(
        requestBody: AppUserRoleSearchRequest,
    ): CancelablePromise<AppUserRoleSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserRole/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appUserRoleImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserRole/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserRoleExportResponse
     * @throws ApiError
     */
    public static appUserRoleExport(
        requestBody: AppUserRoleExportRequest,
    ): CancelablePromise<AppUserRoleExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserRole/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserRoleSampleDownloadResponse
     * @throws ApiError
     */
    public static appUserRoleSampleDownload(
        requestBody: AppUserRoleSampleDownloadRequest,
    ): CancelablePromise<AppUserRoleSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserRole/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserRoleVersionsSearchResponse
     * @throws ApiError
     */
    public static appUserRoleVersions(
        requestBody: AppUserRoleVersionsSearchRequest,
    ): CancelablePromise<AppUserRoleVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUserRole/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns AppUserDto
     * @throws ApiError
     */
    public static appUserGet(
        id: string,
    ): CancelablePromise<AppUserDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/appUser/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppUserDto
     * @throws ApiError
     */
    public static appUserCreate(
        requestBody: AppUserCreateDto,
    ): CancelablePromise<AppUserDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/appUser/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns AppUserDto
     * @throws ApiError
     */
    public static appUserUpdate(
        id: string,
        requestBody: AppUserUpdateDto,
    ): CancelablePromise<AppUserDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUser/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static appUserDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/appUser/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns AppUserSearchResponse
     * @throws ApiError
     */
    public static appUserSearch(
        requestBody: AppUserSearchRequest,
    ): CancelablePromise<AppUserSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUser/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static appUserImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUser/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserExportResponse
     * @throws ApiError
     */
    public static appUserExport(
        requestBody: AppUserExportRequest,
    ): CancelablePromise<AppUserExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUser/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserSampleDownloadResponse
     * @throws ApiError
     */
    public static appUserSampleDownload(
        requestBody: AppUserSampleDownloadRequest,
    ): CancelablePromise<AppUserSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUser/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns AppUserVersionsSearchResponse
     * @throws ApiError
     */
    public static appUserVersions(
        requestBody: AppUserVersionsSearchRequest,
    ): CancelablePromise<AppUserVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/appUser/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns CrmContactDto
     * @throws ApiError
     */
    public static crmContactGet(
        id: string,
    ): CancelablePromise<CrmContactDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/crmContact/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns CrmContactDto
     * @throws ApiError
     */
    public static crmContactCreate(
        requestBody: CrmContactCreateDto,
    ): CancelablePromise<CrmContactDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/crmContact/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns CrmContactDto
     * @throws ApiError
     */
    public static crmContactUpdate(
        id: string,
        requestBody: CrmContactUpdateDto,
    ): CancelablePromise<CrmContactDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/crmContact/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static crmContactDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/crmContact/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns CrmContactSearchResponse
     * @throws ApiError
     */
    public static crmContactSearch(
        requestBody: CrmContactSearchRequest,
    ): CancelablePromise<CrmContactSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/crmContact/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static crmContactImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/crmContact/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns CrmContactExportResponse
     * @throws ApiError
     */
    public static crmContactExport(
        requestBody: CrmContactExportRequest,
    ): CancelablePromise<CrmContactExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/crmContact/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns CrmContactSampleDownloadResponse
     * @throws ApiError
     */
    public static crmContactSampleDownload(
        requestBody: CrmContactSampleDownloadRequest,
    ): CancelablePromise<CrmContactSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/crmContact/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns CrmContactVersionsSearchResponse
     * @throws ApiError
     */
    public static crmContactVersions(
        requestBody: CrmContactVersionsSearchRequest,
    ): CancelablePromise<CrmContactVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/crmContact/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns FooDto
     * @throws ApiError
     */
    public static fooGet(
        id: string,
    ): CancelablePromise<FooDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/v1/foo/item/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns FooDto
     * @throws ApiError
     */
    public static fooCreate(
        requestBody: FooCreateDto,
    ): CancelablePromise<FooDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/v1/foo/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns FooDto
     * @throws ApiError
     */
    public static fooUpdate(
        id: string,
        requestBody: FooUpdateDto,
    ): CancelablePromise<FooDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/foo/update/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static fooDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/v1/foo/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns FooSearchResponse
     * @throws ApiError
     */
    public static fooSearch(
        requestBody: FooSearchRequest,
    ): CancelablePromise<FooSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/foo/search',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns FooParseResponse
     * @throws ApiError
     */
    public static fooParse(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<FooParseResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/foo/parse',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param format
     * @param formData
     * @returns any
     * @throws ApiError
     */
    public static fooImport(
        format: string,
        formData: {
            file?: Blob;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/foo/import',
            query: {
                'format': format,
            },
            formData: formData,
            mediaType: 'multipart/form-data',
        });
    }

    /**
     * @param requestBody
     * @returns FooExportResponse
     * @throws ApiError
     */
    public static fooExport(
        requestBody: FooExportRequest,
    ): CancelablePromise<FooExportResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/foo/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns FooSampleDownloadResponse
     * @throws ApiError
     */
    public static fooSampleDownload(
        requestBody: FooSampleDownloadRequest,
    ): CancelablePromise<FooSampleDownloadResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/foo/sampleDownload',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns FooVersionsSearchResponse
     * @throws ApiError
     */
    public static fooVersions(
        requestBody: FooVersionsSearchRequest,
    ): CancelablePromise<FooVersionsSearchResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/v1/foo/versions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
