/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeFullTextQuery } from './AppOrganizationalUnitTypeFullTextQuery';
import type { AppOrganizationalUnitTypeQueryPaging } from './AppOrganizationalUnitTypeQueryPaging';
import type { AppOrganizationalUnitTypeQuerySorting } from './AppOrganizationalUnitTypeQuerySorting';
import type { AppOrganizationalUnitTypeSearchFilters } from './AppOrganizationalUnitTypeSearchFilters';
import type { AppOrganizationalUnitTypeSearchOptions } from './AppOrganizationalUnitTypeSearchOptions';

export type AppOrganizationalUnitTypeSearchParameters = {
    query?: AppOrganizationalUnitTypeFullTextQuery;
    filters?: AppOrganizationalUnitTypeSearchFilters;
    sorting?: AppOrganizationalUnitTypeQuerySorting;
    paging?: AppOrganizationalUnitTypeQueryPaging;
    options?: AppOrganizationalUnitTypeSearchOptions;
};

