/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppEventLogSearchSortingField = {
    field: AppEventLogSearchSortingField.field;
    direction: AppEventLogSearchSortingField.direction;
};

export namespace AppEventLogSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

