/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CrmContactSearchParameters } from './CrmContactSearchParameters';

export type CrmContactSearchRequest = {
    params: CrmContactSearchParameters;
};

