/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserFacets } from './AppUserFacets';
import type { AppUserListItemDto } from './AppUserListItemDto';
import type { AppUserSearchParameters } from './AppUserSearchParameters';
import type { AppUserSearchResultsPaging } from './AppUserSearchResultsPaging';

export type AppUserSearchResponse = {
    request: AppUserSearchParameters;
    facets: AppUserFacets;
    paging: AppUserSearchResultsPaging;
    items: Array<AppUserListItemDto>;
};

