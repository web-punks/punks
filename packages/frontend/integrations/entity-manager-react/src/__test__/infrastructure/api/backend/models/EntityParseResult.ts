/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EntityParseStatus } from './EntityParseStatus';

export type EntityParseResult = {
    rowIndex: number;
    item: Record<string, any>;
    status: EntityParseStatus;
};

