/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserCreateRequest = {
    email: string;
    userName?: string;
    fistName: string;
    lastName: string;
    roleUid: string;
    tenantUid: string;
    organizationUid?: string;
    organizationalUnitId?: string;
    birthDate?: string;
};

