/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FooSearchFilters } from './FooSearchFilters';

export type FooSearchOptions = {
    includeFacets?: boolean;
    includeChildrenMap?: boolean;
    facetsFilters?: FooSearchFilters;
};

