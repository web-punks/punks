/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserRoleListItemDto } from './AppUserRoleListItemDto';
import type { AppUserRoleVersionsResultsPaging } from './AppUserRoleVersionsResultsPaging';

export type AppUserRoleVersionsSearchResponse = {
    paging: AppUserRoleVersionsResultsPaging;
    items: Array<AppUserRoleListItemDto>;
};

