/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupMemberFullTextQuery } from './AppUserGroupMemberFullTextQuery';
import type { AppUserGroupMemberQueryPaging } from './AppUserGroupMemberQueryPaging';
import type { AppUserGroupMemberQuerySorting } from './AppUserGroupMemberQuerySorting';
import type { AppUserGroupMemberSearchFilters } from './AppUserGroupMemberSearchFilters';
import type { AppUserGroupMemberSearchOptions } from './AppUserGroupMemberSearchOptions';

export type AppUserGroupMemberSearchParameters = {
    query?: AppUserGroupMemberFullTextQuery;
    filters?: AppUserGroupMemberSearchFilters;
    sorting?: AppUserGroupMemberQuerySorting;
    paging?: AppUserGroupMemberQueryPaging;
    options?: AppUserGroupMemberSearchOptions;
};

