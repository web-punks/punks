/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppFileReferenceSearchParameters } from './AppFileReferenceSearchParameters';

export type AppFileReferenceSearchRequest = {
    params: AppFileReferenceSearchParameters;
};

