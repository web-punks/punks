/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppCompanySearchSortingField } from './AppCompanySearchSortingField';

export type AppCompanyQuerySorting = {
    fields?: Array<AppCompanySearchSortingField>;
};

