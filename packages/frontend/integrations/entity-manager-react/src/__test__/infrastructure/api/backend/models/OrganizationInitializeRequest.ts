/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CompanyRegistrationInfo } from './CompanyRegistrationInfo';
import type { OrganizationRegistrationInfo } from './OrganizationRegistrationInfo';

export type OrganizationInitializeRequest = {
    organization: OrganizationRegistrationInfo;
    company: CompanyRegistrationInfo;
};

