/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeChildListItemDto } from './AppOrganizationalUnitTypeChildListItemDto';
import type { AppOrganizationalUnitTypeChildVersionsResultsPaging } from './AppOrganizationalUnitTypeChildVersionsResultsPaging';

export type AppOrganizationalUnitTypeChildVersionsSearchResponse = {
    paging: AppOrganizationalUnitTypeChildVersionsResultsPaging;
    items: Array<AppOrganizationalUnitTypeChildListItemDto>;
};

