/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppCompanyFacets } from './AppCompanyFacets';
import type { AppCompanyListItemDto } from './AppCompanyListItemDto';
import type { AppCompanySearchParameters } from './AppCompanySearchParameters';
import type { AppCompanySearchResultsPaging } from './AppCompanySearchResultsPaging';

export type AppCompanySearchResponse = {
    request: AppCompanySearchParameters;
    facets: AppCompanyFacets;
    paging: AppCompanySearchResultsPaging;
    items: Array<AppCompanyListItemDto>;
};

