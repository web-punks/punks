/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppCacheItemDto = {
    instance: string;
    key: string;
    expiration: string;
    data: Record<string, any>;
    createdOn: string;
    updatedOn: string;
};

