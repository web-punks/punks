/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserGroupSampleDownloadRequest = {
    format: AppUserGroupSampleDownloadRequest.format;
};

export namespace AppUserGroupSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

