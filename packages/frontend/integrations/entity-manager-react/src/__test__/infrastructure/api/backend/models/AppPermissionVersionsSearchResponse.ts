/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppPermissionListItemDto } from './AppPermissionListItemDto';
import type { AppPermissionVersionsResultsPaging } from './AppPermissionVersionsResultsPaging';

export type AppPermissionVersionsSearchResponse = {
    paging: AppPermissionVersionsResultsPaging;
    items: Array<AppPermissionListItemDto>;
};

