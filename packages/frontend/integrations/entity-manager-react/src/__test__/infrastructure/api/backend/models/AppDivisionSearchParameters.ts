/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDivisionFullTextQuery } from './AppDivisionFullTextQuery';
import type { AppDivisionQueryPaging } from './AppDivisionQueryPaging';
import type { AppDivisionQuerySorting } from './AppDivisionQuerySorting';
import type { AppDivisionSearchFilters } from './AppDivisionSearchFilters';
import type { AppDivisionSearchOptions } from './AppDivisionSearchOptions';

export type AppDivisionSearchParameters = {
    query?: AppDivisionFullTextQuery;
    filters?: AppDivisionSearchFilters;
    sorting?: AppDivisionQuerySorting;
    paging?: AppDivisionQueryPaging;
    options?: AppDivisionSearchOptions;
};

