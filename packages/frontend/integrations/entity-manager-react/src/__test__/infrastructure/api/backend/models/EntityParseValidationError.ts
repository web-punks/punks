/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EntityParseValidationColumn } from './EntityParseValidationColumn';

export type EntityParseValidationError = {
    errorCode: string;
    column: EntityParseValidationColumn;
};

