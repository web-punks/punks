/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeChildVersionsSearchParameters } from './AppOrganizationalUnitTypeChildVersionsSearchParameters';

export type AppOrganizationalUnitTypeChildVersionsSearchRequest = {
    params: AppOrganizationalUnitTypeChildVersionsSearchParameters;
};

