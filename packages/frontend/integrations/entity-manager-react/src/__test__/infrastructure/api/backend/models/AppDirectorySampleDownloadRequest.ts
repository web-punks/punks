/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppDirectorySampleDownloadRequest = {
    format: AppDirectorySampleDownloadRequest.format;
};

export namespace AppDirectorySampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

