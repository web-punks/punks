/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppPermissionSearchSortingField = {
    field: AppPermissionSearchSortingField.field;
    direction: AppPermissionSearchSortingField.direction;
};

export namespace AppPermissionSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

