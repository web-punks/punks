/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationSearchSortingField = {
    field: AppOrganizationSearchSortingField.field;
    direction: AppOrganizationSearchSortingField.direction;
};

export namespace AppOrganizationSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

