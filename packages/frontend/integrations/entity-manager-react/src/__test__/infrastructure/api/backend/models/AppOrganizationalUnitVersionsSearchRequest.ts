/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitVersionsSearchParameters } from './AppOrganizationalUnitVersionsSearchParameters';

export type AppOrganizationalUnitVersionsSearchRequest = {
    params: AppOrganizationalUnitVersionsSearchParameters;
};

