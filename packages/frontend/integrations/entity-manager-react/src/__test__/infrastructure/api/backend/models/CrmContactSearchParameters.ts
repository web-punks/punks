/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CrmContactFullTextQuery } from './CrmContactFullTextQuery';
import type { CrmContactQueryPaging } from './CrmContactQueryPaging';
import type { CrmContactQuerySorting } from './CrmContactQuerySorting';
import type { CrmContactSearchFilters } from './CrmContactSearchFilters';
import type { CrmContactSearchOptions } from './CrmContactSearchOptions';

export type CrmContactSearchParameters = {
    query?: CrmContactFullTextQuery;
    filters?: CrmContactSearchFilters;
    sorting?: CrmContactQuerySorting;
    paging?: CrmContactQueryPaging;
    options?: CrmContactSearchOptions;
};

