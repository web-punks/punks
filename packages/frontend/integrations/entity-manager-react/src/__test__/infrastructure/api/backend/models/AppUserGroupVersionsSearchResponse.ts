/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupListItemDto } from './AppUserGroupListItemDto';
import type { AppUserGroupVersionsResultsPaging } from './AppUserGroupVersionsResultsPaging';

export type AppUserGroupVersionsSearchResponse = {
    paging: AppUserGroupVersionsResultsPaging;
    items: Array<AppUserGroupListItemDto>;
};

