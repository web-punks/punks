/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEntityVersionEntitiesExportOptions } from './AppEntityVersionEntitiesExportOptions';
import type { AppEntityVersionSearchParameters } from './AppEntityVersionSearchParameters';

export type AppEntityVersionExportRequest = {
    options: AppEntityVersionEntitiesExportOptions;
    filter?: AppEntityVersionSearchParameters;
};

