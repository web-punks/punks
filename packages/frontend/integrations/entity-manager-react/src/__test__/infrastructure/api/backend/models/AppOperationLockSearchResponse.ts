/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOperationLockFacets } from './AppOperationLockFacets';
import type { AppOperationLockListItemDto } from './AppOperationLockListItemDto';
import type { AppOperationLockSearchParameters } from './AppOperationLockSearchParameters';
import type { AppOperationLockSearchResultsPaging } from './AppOperationLockSearchResultsPaging';

export type AppOperationLockSearchResponse = {
    request: AppOperationLockSearchParameters;
    facets: AppOperationLockFacets;
    paging: AppOperationLockSearchResultsPaging;
    items: Array<AppOperationLockListItemDto>;
};

