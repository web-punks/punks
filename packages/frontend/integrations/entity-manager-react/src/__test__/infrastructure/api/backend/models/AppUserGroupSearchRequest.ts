/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupSearchParameters } from './AppUserGroupSearchParameters';

export type AppUserGroupSearchRequest = {
    params: AppUserGroupSearchParameters;
};

