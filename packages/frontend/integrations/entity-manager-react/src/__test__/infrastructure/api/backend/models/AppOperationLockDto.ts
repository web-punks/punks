/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOperationLockDto = {
    id: string;
    uid: string;
    lockedBy?: string;
    createdOn: string;
    updatedOn: string;
};

