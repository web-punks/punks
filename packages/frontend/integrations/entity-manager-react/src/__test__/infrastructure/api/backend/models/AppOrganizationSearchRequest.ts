/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationSearchParameters } from './AppOrganizationSearchParameters';

export type AppOrganizationSearchRequest = {
    params: AppOrganizationSearchParameters;
};

