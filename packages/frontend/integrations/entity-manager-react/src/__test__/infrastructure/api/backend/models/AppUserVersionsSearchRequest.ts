/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserVersionsSearchParameters } from './AppUserVersionsSearchParameters';

export type AppUserVersionsSearchRequest = {
    params: AppUserVersionsSearchParameters;
};

