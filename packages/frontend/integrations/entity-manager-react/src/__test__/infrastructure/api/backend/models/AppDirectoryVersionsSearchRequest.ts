/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDirectoryVersionsSearchParameters } from './AppDirectoryVersionsSearchParameters';

export type AppDirectoryVersionsSearchRequest = {
    params: AppDirectoryVersionsSearchParameters;
};

