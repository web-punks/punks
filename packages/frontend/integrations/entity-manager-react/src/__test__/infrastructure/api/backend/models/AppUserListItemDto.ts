/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserProfileEntityDto } from './AppUserProfileEntityDto';

export type AppUserListItemDto = {
    id: string;
    uid?: string;
    userName: string;
    email: string;
    verified: boolean;
    verifiedTimestamp?: string;
    passwordUpdateTimestamp?: string;
    disabled: boolean;
    profile: AppUserProfileEntityDto;
    createdOn: string;
    updatedOn: string;
};

