/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeChildSearchSortingField } from './AppOrganizationalUnitTypeChildSearchSortingField';

export type AppOrganizationalUnitTypeChildQuerySorting = {
    fields?: Array<AppOrganizationalUnitTypeChildSearchSortingField>;
};

