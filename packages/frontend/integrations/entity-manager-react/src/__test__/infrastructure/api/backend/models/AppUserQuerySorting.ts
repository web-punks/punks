/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserSearchSortingField } from './AppUserSearchSortingField';

export type AppUserQuerySorting = {
    fields?: Array<AppUserSearchSortingField>;
};

