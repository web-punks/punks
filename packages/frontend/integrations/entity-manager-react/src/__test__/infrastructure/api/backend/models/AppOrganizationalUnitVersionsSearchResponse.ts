/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitListItemDto } from './AppOrganizationalUnitListItemDto';
import type { AppOrganizationalUnitVersionsResultsPaging } from './AppOrganizationalUnitVersionsResultsPaging';

export type AppOrganizationalUnitVersionsSearchResponse = {
    paging: AppOrganizationalUnitVersionsResultsPaging;
    items: Array<AppOrganizationalUnitListItemDto>;
};

