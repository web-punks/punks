/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserRole = {
    id: string;
    uid: UserRole.uid;
    enabled: boolean;
    name: string;
};

export namespace UserRole {

    export enum uid {
        TENANT_ADMIN = 'tenant-admin',
        ORGANIZATION_ADMIN = 'organization-admin',
        ORGANIZATION_MANAGER = 'organization-manager',
        TEACHER = 'teacher',
        PRINCIPAL = 'principal',
        PARTNER = 'partner',
    }


}

