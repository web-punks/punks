/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserGroupMemberEntitiesExportOptions = {
    format: AppUserGroupMemberEntitiesExportOptions.format;
};

export namespace AppUserGroupMemberEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

