/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationFacets } from './AppOrganizationFacets';
import type { AppOrganizationListItemDto } from './AppOrganizationListItemDto';
import type { AppOrganizationSearchParameters } from './AppOrganizationSearchParameters';
import type { AppOrganizationSearchResultsPaging } from './AppOrganizationSearchResultsPaging';

export type AppOrganizationSearchResponse = {
    request: AppOrganizationSearchParameters;
    facets: AppOrganizationFacets;
    paging: AppOrganizationSearchResultsPaging;
    items: Array<AppOrganizationListItemDto>;
};

