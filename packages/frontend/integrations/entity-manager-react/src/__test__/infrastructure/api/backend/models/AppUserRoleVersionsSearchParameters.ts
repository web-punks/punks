/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EntityVersionsCursor } from './EntityVersionsCursor';
import type { EntityVersionsFilters } from './EntityVersionsFilters';
import type { EntityVersionsReference } from './EntityVersionsReference';
import type { EntityVersionsSorting } from './EntityVersionsSorting';

export type AppUserRoleVersionsSearchParameters = {
    entity: EntityVersionsReference;
    filters?: EntityVersionsFilters;
    sorting?: EntityVersionsSorting;
    paging?: EntityVersionsCursor;
};

