/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppTenantSearchSortingField = {
    field: AppTenantSearchSortingField.field;
    direction: AppTenantSearchSortingField.direction;
};

export namespace AppTenantSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

