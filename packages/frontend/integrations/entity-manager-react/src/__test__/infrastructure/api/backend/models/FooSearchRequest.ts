/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FooSearchParameters } from './FooSearchParameters';

export type FooSearchRequest = {
    params: FooSearchParameters;
};

