/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppTenantSearchSortingField } from './AppTenantSearchSortingField';

export type AppTenantQuerySorting = {
    fields?: Array<AppTenantSearchSortingField>;
};

