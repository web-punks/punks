/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitFacets } from './AppOrganizationalUnitFacets';
import type { AppOrganizationalUnitListItemDto } from './AppOrganizationalUnitListItemDto';
import type { AppOrganizationalUnitSearchParameters } from './AppOrganizationalUnitSearchParameters';
import type { AppOrganizationalUnitSearchResultsPaging } from './AppOrganizationalUnitSearchResultsPaging';

export type AppOrganizationalUnitSearchResponse = {
    request: AppOrganizationalUnitSearchParameters;
    facets: AppOrganizationalUnitFacets;
    paging: AppOrganizationalUnitSearchResultsPaging;
    items: Array<AppOrganizationalUnitListItemDto>;
    childrenMap?: Record<string, any>;
};

