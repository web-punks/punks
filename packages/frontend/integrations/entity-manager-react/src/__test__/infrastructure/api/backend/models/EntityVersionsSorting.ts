/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type EntityVersionsSorting = {
    direction: EntityVersionsSorting.direction;
};

export namespace EntityVersionsSorting {

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

