/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDivisionFacets } from './AppDivisionFacets';
import type { AppDivisionListItemDto } from './AppDivisionListItemDto';
import type { AppDivisionSearchParameters } from './AppDivisionSearchParameters';
import type { AppDivisionSearchResultsPaging } from './AppDivisionSearchResultsPaging';

export type AppDivisionSearchResponse = {
    request: AppDivisionSearchParameters;
    facets: AppDivisionFacets;
    paging: AppDivisionSearchResultsPaging;
    items: Array<AppDivisionListItemDto>;
};

