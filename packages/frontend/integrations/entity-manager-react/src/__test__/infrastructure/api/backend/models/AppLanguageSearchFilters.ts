/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { StringFilter } from './StringFilter';

export type AppLanguageSearchFilters = {
    uid?: StringFilter;
};

