/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupSearchSortingField } from './AppUserGroupSearchSortingField';

export type AppUserGroupQuerySorting = {
    fields?: Array<AppUserGroupSearchSortingField>;
};

