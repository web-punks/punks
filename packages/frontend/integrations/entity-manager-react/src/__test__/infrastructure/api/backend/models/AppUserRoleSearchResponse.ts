/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserRoleFacets } from './AppUserRoleFacets';
import type { AppUserRoleListItemDto } from './AppUserRoleListItemDto';
import type { AppUserRoleSearchParameters } from './AppUserRoleSearchParameters';
import type { AppUserRoleSearchResultsPaging } from './AppUserRoleSearchResultsPaging';

export type AppUserRoleSearchResponse = {
    request: AppUserRoleSearchParameters;
    facets: AppUserRoleFacets;
    paging: AppUserRoleSearchResultsPaging;
    items: Array<AppUserRoleListItemDto>;
};

