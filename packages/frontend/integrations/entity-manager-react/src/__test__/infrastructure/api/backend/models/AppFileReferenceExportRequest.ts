/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppFileReferenceEntitiesExportOptions } from './AppFileReferenceEntitiesExportOptions';
import type { AppFileReferenceSearchParameters } from './AppFileReferenceSearchParameters';

export type AppFileReferenceExportRequest = {
    options: AppFileReferenceEntitiesExportOptions;
    filter?: AppFileReferenceSearchParameters;
};

