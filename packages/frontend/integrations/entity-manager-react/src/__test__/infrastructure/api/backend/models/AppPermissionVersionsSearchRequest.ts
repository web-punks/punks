/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppPermissionVersionsSearchParameters } from './AppPermissionVersionsSearchParameters';

export type AppPermissionVersionsSearchRequest = {
    params: AppPermissionVersionsSearchParameters;
};

