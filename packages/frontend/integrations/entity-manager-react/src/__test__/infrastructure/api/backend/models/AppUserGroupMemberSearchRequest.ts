/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupMemberSearchParameters } from './AppUserGroupMemberSearchParameters';

export type AppUserGroupMemberSearchRequest = {
    params: AppUserGroupMemberSearchParameters;
};

