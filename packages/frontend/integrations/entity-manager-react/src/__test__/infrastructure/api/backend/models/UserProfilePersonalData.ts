/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserProfilePersonalData = {
    firstName: string;
    lastName: string;
};

