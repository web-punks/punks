/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitListItemDto } from './AppOrganizationalUnitListItemDto';
import type { EntityTreeNodePaths } from './EntityTreeNodePaths';

export type AppOrganizationalUnitTreeNodeDto = {
    id: string;
    value: AppOrganizationalUnitListItemDto;
    paths: EntityTreeNodePaths;
    children: Array<AppOrganizationalUnitTreeNodeDto>;
};

