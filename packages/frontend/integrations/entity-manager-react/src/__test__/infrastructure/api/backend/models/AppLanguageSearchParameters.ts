/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppLanguageFullTextQuery } from './AppLanguageFullTextQuery';
import type { AppLanguageQueryPaging } from './AppLanguageQueryPaging';
import type { AppLanguageQuerySorting } from './AppLanguageQuerySorting';
import type { AppLanguageSearchFilters } from './AppLanguageSearchFilters';
import type { AppLanguageSearchOptions } from './AppLanguageSearchOptions';

export type AppLanguageSearchParameters = {
    query?: AppLanguageFullTextQuery;
    filters?: AppLanguageSearchFilters;
    sorting?: AppLanguageQuerySorting;
    paging?: AppLanguageQueryPaging;
    options?: AppLanguageSearchOptions;
};

