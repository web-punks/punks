/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type StringFilter = {
    gt?: string;
    gte?: string;
    lt?: string;
    lte?: string;
    in?: Array<string>;
    eq?: string;
    ieq?: string;
    like?: string;
    ne?: string;
    ine?: string;
    notIn?: Array<string>;
    notLike?: string;
    isNull?: boolean;
};

