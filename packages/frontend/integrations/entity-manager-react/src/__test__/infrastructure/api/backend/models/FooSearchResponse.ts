/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FooFacets } from './FooFacets';
import type { FooListItemDto } from './FooListItemDto';
import type { FooSearchParameters } from './FooSearchParameters';
import type { FooSearchResultsPaging } from './FooSearchResultsPaging';

export type FooSearchResponse = {
    request: FooSearchParameters;
    facets: FooFacets;
    paging: FooSearchResultsPaging;
    items: Array<FooListItemDto>;
    childrenMap?: Record<string, any>;
};

