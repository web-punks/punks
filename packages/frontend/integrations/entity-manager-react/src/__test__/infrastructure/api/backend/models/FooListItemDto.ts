/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type FooListItemDto = {
    id: string;
    name: string;
    uid: string;
    other?: string;
    type: string;
    age: number;
    parentId?: string;
    createdOn: string;
    updatedOn: string;
};

