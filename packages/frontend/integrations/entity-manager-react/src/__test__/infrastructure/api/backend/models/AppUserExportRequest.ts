/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserEntitiesExportOptions } from './AppUserEntitiesExportOptions';
import type { AppUserSearchParameters } from './AppUserSearchParameters';

export type AppUserExportRequest = {
    options: AppUserEntitiesExportOptions;
    filter?: AppUserSearchParameters;
};

