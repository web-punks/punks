/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationListItemDto } from './AppOrganizationListItemDto';
import type { AppOrganizationVersionsResultsPaging } from './AppOrganizationVersionsResultsPaging';

export type AppOrganizationVersionsSearchResponse = {
    paging: AppOrganizationVersionsResultsPaging;
    items: Array<AppOrganizationListItemDto>;
};

