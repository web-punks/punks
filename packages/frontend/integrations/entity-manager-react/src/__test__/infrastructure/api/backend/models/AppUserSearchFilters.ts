/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserSearchFilters = {
    id?: string;
    email?: string;
    userName?: string;
    organizationId?: string;
    organizationUid?: string;
    tenantId?: string;
    tenantUid?: string;
    directoryId?: string;
    directoryUid?: string;
};

