/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserGroupEntitiesExportOptions = {
    format: AppUserGroupEntitiesExportOptions.format;
};

export namespace AppUserGroupEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

