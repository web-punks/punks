/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppDivisionListItemDto = {
    id: string;
    uid: string;
    name: string;
    createdOn: string;
    updatedOn: string;
};

