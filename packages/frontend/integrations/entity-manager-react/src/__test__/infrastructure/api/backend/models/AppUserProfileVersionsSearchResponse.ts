/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserProfileListItemDto } from './AppUserProfileListItemDto';
import type { AppUserProfileVersionsResultsPaging } from './AppUserProfileVersionsResultsPaging';

export type AppUserProfileVersionsSearchResponse = {
    paging: AppUserProfileVersionsResultsPaging;
    items: Array<AppUserProfileListItemDto>;
};

