/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeChildFullTextQuery } from './AppOrganizationalUnitTypeChildFullTextQuery';
import type { AppOrganizationalUnitTypeChildQueryPaging } from './AppOrganizationalUnitTypeChildQueryPaging';
import type { AppOrganizationalUnitTypeChildQuerySorting } from './AppOrganizationalUnitTypeChildQuerySorting';
import type { AppOrganizationalUnitTypeChildSearchFilters } from './AppOrganizationalUnitTypeChildSearchFilters';
import type { AppOrganizationalUnitTypeChildSearchOptions } from './AppOrganizationalUnitTypeChildSearchOptions';

export type AppOrganizationalUnitTypeChildSearchParameters = {
    query?: AppOrganizationalUnitTypeChildFullTextQuery;
    filters?: AppOrganizationalUnitTypeChildSearchFilters;
    sorting?: AppOrganizationalUnitTypeChildQuerySorting;
    paging?: AppOrganizationalUnitTypeChildQueryPaging;
    options?: AppOrganizationalUnitTypeChildSearchOptions;
};

