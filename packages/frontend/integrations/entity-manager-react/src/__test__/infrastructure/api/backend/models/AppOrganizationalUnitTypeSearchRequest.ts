/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeSearchParameters } from './AppOrganizationalUnitTypeSearchParameters';

export type AppOrganizationalUnitTypeSearchRequest = {
    params: AppOrganizationalUnitTypeSearchParameters;
};

