/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserGroupSearchSortingField = {
    field: AppUserGroupSearchSortingField.field;
    direction: AppUserGroupSearchSortingField.direction;
};

export namespace AppUserGroupSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

