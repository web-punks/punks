/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserUpdateRequest = {
    email: string;
    userName?: string;
    fistName: string;
    lastName: string;
    roleUid: string;
    organizationalUnitId?: string;
    birthDate?: string;
};

