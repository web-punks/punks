/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEntityVersionSearchParameters } from './AppEntityVersionSearchParameters';

export type AppEntityVersionSearchRequest = {
    params: AppEntityVersionSearchParameters;
};

