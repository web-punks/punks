/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationSearchSortingField } from './AppOrganizationSearchSortingField';

export type AppOrganizationQuerySorting = {
    fields?: Array<AppOrganizationSearchSortingField>;
};

