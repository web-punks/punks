/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppEmailLogListItemDto = {
    id: string;
    emailType: string;
    to?: Array<string>;
    cc?: Array<string>;
    bcc?: Array<string>;
    payload?: Record<string, any>;
    createdOn: string;
    updatedOn: string;
};

