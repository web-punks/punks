/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AuthUserRegistrationData = {
    firstName: string;
    lastName: string;
    birthDate: string;
};

