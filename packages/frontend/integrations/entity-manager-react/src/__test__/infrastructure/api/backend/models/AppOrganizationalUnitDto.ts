/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitType } from './AppOrganizationalUnitType';
import type { AppOrganizationReferenceDto } from './AppOrganizationReferenceDto';

export type AppOrganizationalUnitDto = {
    id: string;
    name: string;
    uid: string;
    type: AppOrganizationalUnitType;
    parentId?: string;
    organization: AppOrganizationReferenceDto;
    createdOn: string;
    updatedOn: string;
};

