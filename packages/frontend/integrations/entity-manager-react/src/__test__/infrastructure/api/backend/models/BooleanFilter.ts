/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type BooleanFilter = {
    eq?: boolean;
    ne?: boolean;
    isNull?: boolean;
};

