/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppDivisionSearchSortingField = {
    field: AppDivisionSearchSortingField.field;
    direction: AppDivisionSearchSortingField.direction;
};

export namespace AppDivisionSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

