/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppFileReferenceVersionsSearchParameters } from './AppFileReferenceVersionsSearchParameters';

export type AppFileReferenceVersionsSearchRequest = {
    params: AppFileReferenceVersionsSearchParameters;
};

