/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationFullTextQuery } from './AppOrganizationFullTextQuery';
import type { AppOrganizationQueryPaging } from './AppOrganizationQueryPaging';
import type { AppOrganizationQuerySorting } from './AppOrganizationQuerySorting';
import type { AppOrganizationSearchFilters } from './AppOrganizationSearchFilters';
import type { AppOrganizationSearchOptions } from './AppOrganizationSearchOptions';

export type AppOrganizationSearchParameters = {
    query?: AppOrganizationFullTextQuery;
    filters?: AppOrganizationSearchFilters;
    sorting?: AppOrganizationQuerySorting;
    paging?: AppOrganizationQueryPaging;
    options?: AppOrganizationSearchOptions;
};

