/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CrmContactSearchSortingField } from './CrmContactSearchSortingField';

export type CrmContactQuerySorting = {
    fields?: Array<CrmContactSearchSortingField>;
};

