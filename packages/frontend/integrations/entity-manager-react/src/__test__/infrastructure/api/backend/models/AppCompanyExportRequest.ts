/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppCompanyEntitiesExportOptions } from './AppCompanyEntitiesExportOptions';
import type { AppCompanySearchParameters } from './AppCompanySearchParameters';

export type AppCompanyExportRequest = {
    options: AppCompanyEntitiesExportOptions;
    filter?: AppCompanySearchParameters;
};

