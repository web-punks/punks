/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDivisionSearchSortingField } from './AppDivisionSearchSortingField';

export type AppDivisionQuerySorting = {
    fields?: Array<AppDivisionSearchSortingField>;
};

