/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppDirectorySearchSortingField } from './AppDirectorySearchSortingField';

export type AppDirectoryQuerySorting = {
    fields?: Array<AppDirectorySearchSortingField>;
};

