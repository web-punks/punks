/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEntityVersionListItemDto } from './AppEntityVersionListItemDto';
import type { AppEntityVersionVersionsResultsPaging } from './AppEntityVersionVersionsResultsPaging';

export type AppEntityVersionVersionsSearchResponse = {
    paging: AppEntityVersionVersionsResultsPaging;
    items: Array<AppEntityVersionListItemDto>;
};

