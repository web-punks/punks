/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppUserGroupMemberSearchSortingField = {
    field: AppUserGroupMemberSearchSortingField.field;
    direction: AppUserGroupMemberSearchSortingField.direction;
};

export namespace AppUserGroupMemberSearchSortingField {

    export enum field {
        NAME = 'Name',
    }

    export enum direction {
        ASC = 'asc',
        DESC = 'desc',
    }


}

