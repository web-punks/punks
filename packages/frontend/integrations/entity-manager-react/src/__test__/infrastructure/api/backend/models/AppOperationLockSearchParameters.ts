/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOperationLockFullTextQuery } from './AppOperationLockFullTextQuery';
import type { AppOperationLockQueryPaging } from './AppOperationLockQueryPaging';
import type { AppOperationLockQuerySorting } from './AppOperationLockQuerySorting';
import type { AppOperationLockSearchFilters } from './AppOperationLockSearchFilters';
import type { AppOperationLockSearchOptions } from './AppOperationLockSearchOptions';

export type AppOperationLockSearchParameters = {
    query?: AppOperationLockFullTextQuery;
    filters?: AppOperationLockSearchFilters;
    sorting?: AppOperationLockQuerySorting;
    paging?: AppOperationLockQueryPaging;
    options?: AppOperationLockSearchOptions;
};

