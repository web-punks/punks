/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOperationLockEntitiesExportOptions } from './AppOperationLockEntitiesExportOptions';
import type { AppOperationLockSearchParameters } from './AppOperationLockSearchParameters';

export type AppOperationLockExportRequest = {
    options: AppOperationLockEntitiesExportOptions;
    filter?: AppOperationLockSearchParameters;
};

