/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeSearchSortingField } from './AppOrganizationalUnitTypeSearchSortingField';

export type AppOrganizationalUnitTypeQuerySorting = {
    fields?: Array<AppOrganizationalUnitTypeSearchSortingField>;
};

