/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { BooleanFacet } from './BooleanFacet';
import type { NumericFacet } from './NumericFacet';
import type { StringFacet } from './StringFacet';

export type FooFacets = {
    age: NumericFacet;
    type: StringFacet;
    enabled: BooleanFacet;
};

