/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeAllowedChildTypeInput } from './AppOrganizationalUnitTypeAllowedChildTypeInput';

export type AppOrganizationalUnitTypeCreateDto = {
    organizationId: string;
    uid: string;
    name: string;
    allowAsRoot: boolean;
    allowedChildrenTypes?: Array<AppOrganizationalUnitTypeAllowedChildTypeInput>;
};

