/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppEntityVersionSampleDownloadRequest = {
    format: AppEntityVersionSampleDownloadRequest.format;
};

export namespace AppEntityVersionSampleDownloadRequest {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

