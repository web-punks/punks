/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserPermission } from './UserPermission';
import type { UserProfileData } from './UserProfileData';
import type { UserResources } from './UserResources';
import type { UserRole } from './UserRole';

export type UserProfileResponse = {
    user: UserProfileData;
    roles: Array<UserRole>;
    permissions: Array<UserPermission>;
    resources: UserResources;
};

