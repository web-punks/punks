/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EntityParseValidationError } from './EntityParseValidationError';

export type EntityParseStatus = {
    isValid: boolean;
    validationErrors: Array<EntityParseValidationError>;
};

