/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FooListItemDto } from './FooListItemDto';
import type { FooVersionsResultsPaging } from './FooVersionsResultsPaging';

export type FooVersionsSearchResponse = {
    paging: FooVersionsResultsPaging;
    items: Array<FooListItemDto>;
};

