/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserGroupFacets } from './AppUserGroupFacets';
import type { AppUserGroupListItemDto } from './AppUserGroupListItemDto';
import type { AppUserGroupSearchParameters } from './AppUserGroupSearchParameters';
import type { AppUserGroupSearchResultsPaging } from './AppUserGroupSearchResultsPaging';

export type AppUserGroupSearchResponse = {
    request: AppUserGroupSearchParameters;
    facets: AppUserGroupFacets;
    paging: AppUserGroupSearchResultsPaging;
    items: Array<AppUserGroupListItemDto>;
};

