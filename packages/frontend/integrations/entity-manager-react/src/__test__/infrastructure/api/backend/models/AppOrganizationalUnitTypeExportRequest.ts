/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitTypeEntitiesExportOptions } from './AppOrganizationalUnitTypeEntitiesExportOptions';
import type { AppOrganizationalUnitTypeSearchParameters } from './AppOrganizationalUnitTypeSearchParameters';

export type AppOrganizationalUnitTypeExportRequest = {
    options: AppOrganizationalUnitTypeEntitiesExportOptions;
    filter?: AppOrganizationalUnitTypeSearchParameters;
};

