/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppOrganizationalUnitTypeEntitiesExportOptions = {
    format: AppOrganizationalUnitTypeEntitiesExportOptions.format;
};

export namespace AppOrganizationalUnitTypeEntitiesExportOptions {

    export enum format {
        CSV = 'csv',
        JSON = 'json',
        XLSX = 'xlsx',
    }


}

