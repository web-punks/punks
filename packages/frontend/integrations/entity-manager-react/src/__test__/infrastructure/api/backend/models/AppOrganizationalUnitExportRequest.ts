/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppOrganizationalUnitEntitiesExportOptions } from './AppOrganizationalUnitEntitiesExportOptions';
import type { AppOrganizationalUnitSearchParameters } from './AppOrganizationalUnitSearchParameters';

export type AppOrganizationalUnitExportRequest = {
    options: AppOrganizationalUnitEntitiesExportOptions;
    filter?: AppOrganizationalUnitSearchParameters;
};

