/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserProfileSearchSortingField } from './AppUserProfileSearchSortingField';

export type AppUserProfileQuerySorting = {
    fields?: Array<AppUserProfileSearchSortingField>;
};

