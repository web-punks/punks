/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AuthUserContext } from './AuthUserContext';

export type UserLoginRequest = {
    userName: string;
    userContext: AuthUserContext;
    password: string;
};

