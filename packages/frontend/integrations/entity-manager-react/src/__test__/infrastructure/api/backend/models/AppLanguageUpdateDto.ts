/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppLanguageUpdateDto = {
    organizationId: string;
    name: string;
    uid: string;
};

