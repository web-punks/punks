/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppPermissionFacets } from './AppPermissionFacets';
import type { AppPermissionListItemDto } from './AppPermissionListItemDto';
import type { AppPermissionSearchParameters } from './AppPermissionSearchParameters';
import type { AppPermissionSearchResultsPaging } from './AppPermissionSearchResultsPaging';

export type AppPermissionSearchResponse = {
    request: AppPermissionSearchParameters;
    facets: AppPermissionFacets;
    paging: AppPermissionSearchResultsPaging;
    items: Array<AppPermissionListItemDto>;
};

