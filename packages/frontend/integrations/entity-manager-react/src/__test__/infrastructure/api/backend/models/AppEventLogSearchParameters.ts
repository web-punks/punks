/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppEventLogFullTextQuery } from './AppEventLogFullTextQuery';
import type { AppEventLogQueryPaging } from './AppEventLogQueryPaging';
import type { AppEventLogQuerySorting } from './AppEventLogQuerySorting';
import type { AppEventLogSearchFilters } from './AppEventLogSearchFilters';
import type { AppEventLogSearchOptions } from './AppEventLogSearchOptions';

export type AppEventLogSearchParameters = {
    query?: AppEventLogFullTextQuery;
    filters?: AppEventLogSearchFilters;
    sorting?: AppEventLogQuerySorting;
    paging?: AppEventLogQueryPaging;
    options?: AppEventLogSearchOptions;
};

