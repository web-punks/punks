/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppUserRoleVersionsSearchParameters } from './AppUserRoleVersionsSearchParameters';

export type AppUserRoleVersionsSearchRequest = {
    params: AppUserRoleVersionsSearchParameters;
};

