export const downloadBinaryFile = (input: {
  fileName: string
  content: string | Blob
}) => {
  const fileContent =
    typeof input.content === "string"
      ? new Blob([input.content])
      : input.content

  const link = document.createElement("a")
  link.style.display = "none"
  link.href = window.URL.createObjectURL(fileContent)
  link.download = input.fileName

  document.body.appendChild(link)
  link.click()
  document.body.removeChild(link)
}
