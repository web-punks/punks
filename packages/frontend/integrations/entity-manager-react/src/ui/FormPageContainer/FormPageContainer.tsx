import { uiRegistry } from "@punks/ui-components"

export type FormPageLayout = {
  backgroundImageUrl: string
  logo?: React.ReactNode
}

export interface FormPageContainerProps {
  children: React.ReactNode
  layout: FormPageLayout
}

const FormPageContainer = ({ children, layout }: FormPageContainerProps) => {
  return (
    <>
      {uiRegistry().components.renderSplashScreenLayout({
        children: (
          <>
            {layout.logo &&
              uiRegistry().components.renderBox({
                children: layout.logo,
                mb: 4,
                flex: true,
                justifyContent: "center",
                alignItems: "center",
              })}
            {children}
          </>
        ),
        imageUrl: layout.backgroundImageUrl,
      })}
    </>
  )
}

export default FormPageContainer
