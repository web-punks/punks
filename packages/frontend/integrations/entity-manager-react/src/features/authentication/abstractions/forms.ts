export type EntityCancelOptions = {
  refetch?: boolean
}

export type EntityCloseOptions = {
  refetch?: boolean
}

export type EntityCreateFormProps<TItem, TEntity, TAdditionalProps> = {
  item?: TEntity
  parentItem?: TEntity
  onSave: (item: TItem) => Promise<void>
  onCancel: (options?: EntityCancelOptions) => void
  saving: boolean
  additionalProps?: TAdditionalProps
}

export type EntityUpdateFormProps<TItem, TEntity, TAdditionalProps> = {
  item: TEntity
  onSave: (item: TItem) => Promise<void>
  onCancel: (options?: EntityCancelOptions) => void
  saving: boolean
  additionalProps?: TAdditionalProps
}

export type EntityDetailsFormProps<TEntity, TAdditionalProps> = {
  item: TEntity
  onClose: (options?: EntityCloseOptions) => void
  additionalProps?: TAdditionalProps
}

export type EntityUploadFormProps<TAdditionalProps> = {
  onSubmit: (file: File) => Promise<void>
  onCancel: (options?: EntityCloseOptions) => void
  additionalProps?: TAdditionalProps
}

export type EntityCreateFormComponent<
  TCreate,
  TEntity,
  TAdditionalProps = any
> = React.FC<EntityCreateFormProps<TCreate, TEntity, TAdditionalProps>>

export type EntityUpdateFormComponent<
  TUpdate,
  TEntity,
  TAdditionalProps = any
> = React.FC<EntityUpdateFormProps<TUpdate, TEntity, TAdditionalProps>>

export type EntityDetailsFormComponent<
  TEntity,
  TAdditionalProps = any
> = React.FC<EntityDetailsFormProps<TEntity, TAdditionalProps>>
