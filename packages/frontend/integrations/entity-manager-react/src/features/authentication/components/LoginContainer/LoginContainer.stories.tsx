import type { Meta, StoryObj } from "@storybook/react"

import { LoginContainer } from "."
import {
  AuthenticationLoginError,
  AuthenticationProvider,
} from "../../../../modules"

const meta = {
  title: "Components/Authentication/LoginContainer",
  component: LoginContainer,
  tags: ["autodocs"],
  parameters: {
    layout: "fullscreen",
  },
  args: {
    layout: {
      logo: (
        <img
          src="https://a.storyblok.com/f/196735/601x601/29667fe0ad/fav10.png"
          style={{
            width: 100,
          }}
        />
      ),
      backgroundImageUrl:
        "https://source.unsplash.com/random/900%C3%97700/?nature",
    },
    form: {
      events: {
        onSuccess: () => {
          alert("Login success")
        },
      },
      labels: {
        username: {
          label: "Username",
          placeholder: "Username",
        },
        password: {
          label: "Password",
          placeholder: "Password",
        },
        submit: {
          label: "Login",
        },
      },
      messages: {
        invalidCredentials: "Invalid credentials",
        serverError: "Server error",
      },
    },
  },
} satisfies Meta<typeof LoginContainer>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{}}
        actions={{
          login: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: true,
            }
          },
          forgotPassword: async () => {
            throw new Error("Not implemented")
          },
          passwordReset: async () => {
            throw new Error("Not implemented")
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const InvalidCredentials: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{
          directoryId: "test",
        }}
        actions={{
          login: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: false,
              error: AuthenticationLoginError.InvalidCredentials,
            }
          },
          forgotPassword: async () => {
            throw new Error("Not implemented")
          },
          passwordReset: async () => {
            throw new Error("Not implemented")
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const ServerError: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{
          directoryId: "test",
        }}
        actions={{
          login: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: false,
              error: AuthenticationLoginError.ServerError,
            }
          },
          forgotPassword: async () => {
            throw new Error("Not implemented")
          },
          passwordReset: async () => {
            throw new Error("Not implemented")
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}
