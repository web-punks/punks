import type { Meta, StoryObj } from "@storybook/react"

import { PasswordResetForm } from "."
import {
  AuthenticationPasswordResetError,
  AuthenticationProvider,
} from "../../../../modules"
import { PasswordResetExtrafieldsType } from "./PasswordResetForm"

const meta = {
  title: "Components/Authentication/PasswordResetForm",
  component: PasswordResetForm,
  tags: ["autodocs"],
  args: {
    events: {
      onSuccess: () => {
        alert("Login success")
      },
    },
    labels: {
      email: {
        label: "Email",
      },
      password: {
        label: "Password",
        placeholder: "Password",
      },
      submit: {
        label: "Login",
      },
    },
    messages: {
      invalidPassword: "Invalid password",
      serverError: "Server error",
    },
    resetToken: "test",
  },
  decorators: [
    (Story: any) => (
      <div
        style={{
          maxWidth: 400,
          margin: "auto",
        }}
      >
        <Story />
      </div>
    ),
  ],
} satisfies Meta<typeof PasswordResetForm>
export default meta

type Story = StoryObj<typeof meta>

const DefaultActions = {
  login: async () => {
    throw new Error("Not implemented")
  },
  forgotPassword: async () => {
    throw new Error("Not implemented")
  },
  passwordReset: async () => {
    await new Promise((resolve) => setTimeout(resolve, 300))
    return {
      success: true,
    }
  },
  logout: async () => {
    throw new Error("Not implemented")
  },
}

export const Default: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider context={{}} actions={DefaultActions}>
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const WithEmail: Story = {
  args: {
    email: "test@test.it",
  },
  decorators: [
    (Story: any) => (
      <AuthenticationProvider context={{}} actions={DefaultActions}>
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const WithPolicy: Story = {
  args: {
    extraFields: [
      {
        required: true,
        type: PasswordResetExtrafieldsType.CHECKBOX,
        label: "I accept the terms and conditions",
        name: "policy",
      },
    ],
  },
  decorators: [
    (Story: any) => (
      <AuthenticationProvider context={{}} actions={DefaultActions}>
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const InvalidPassword: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{
          directoryId: "test",
        }}
        actions={{
          ...DefaultActions,
          passwordReset: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: false,
              error: AuthenticationPasswordResetError.InvalidPassword,
            }
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const ServerError: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{
          directoryId: "test",
        }}
        actions={{
          ...DefaultActions,
          passwordReset: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: false,
              error: AuthenticationPasswordResetError.ServerError,
            }
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}
