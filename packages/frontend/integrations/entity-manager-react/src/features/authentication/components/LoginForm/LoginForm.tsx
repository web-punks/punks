import { uiRegistry } from "@punks/ui-components"
import { useLoginAction } from "../../../../modules/authentication/actions"
import { classNames } from "@punks/ui-core"
import { AuthenticationLoginError } from "../../../../modules/authentication"
import { useRef } from "react"

export type LoginFormClasses = {
  root?: string
  block?: string
  input?: string
  submit?: string
}

export type LoginFormInputLabel = {
  label: string
  placeholder?: string
}

export type LoginFormCtaLabel = {
  label: string
}

export type LoginFormLabels = {
  username: LoginFormInputLabel
  password: LoginFormInputLabel
  submit: LoginFormCtaLabel
}

export type LoginFormMessages = {
  invalidCredentials: string
  serverError: string
}

export type LoginFormEvents = {
  onSuccess?: () => void
  onFailure?: () => void
}

export interface LoginFormProps {
  fullWidth?: boolean
  className?: string
  classes?: LoginFormClasses
  labels: LoginFormLabels
  messages: LoginFormMessages
  events?: LoginFormEvents
}

const LoginForm = ({
  className,
  classes,
  fullWidth = true,
  labels,
  messages,
  events,
}: LoginFormProps) => {
  const { handleLogin, state } = useLoginAction({
    onSuccess: events?.onSuccess,
    onError: events?.onFailure,
  })

  const usernameRef = useRef<HTMLInputElement>(null)
  const passwordRef = useRef<HTMLInputElement>(null)

  const onSubmit = async () => {
    const username = usernameRef.current?.value
    const password = passwordRef.current?.value

    if (!username || !password) {
      return
    }

    await handleLogin({
      username,
      password,
    })
  }

  return (
    <>
      {uiRegistry().components.renderBox({
        children: uiRegistry().components.renderBox({
          className: classNames(className, classes?.root),
          children: (
            <>
              {uiRegistry().components.renderFormInput({
                className: classes?.block,
                label: labels.username.label,
                children: uiRegistry().components.renderTextInput({
                  className: classes?.input,
                  fullWidth,
                  placeholder: labels.username.label,
                  inputRef: usernameRef,
                }),
              })}
              {uiRegistry().components.renderFormInput({
                className: classes?.block,
                label: labels.password.label,
                children: uiRegistry().components.renderTextInput({
                  className: classes?.input,
                  fullWidth,
                  placeholder: labels.password.label,
                  type: "password",
                  inputRef: passwordRef,
                  onKeyDown: (e) => {
                    if (e.key === "Enter") {
                      onSubmit()
                    }
                  },
                }),
              })}
              {uiRegistry().components.renderBox({
                mt: 4,
                children: uiRegistry().components.renderButton({
                  className: classes?.submit,
                  fullWidth,
                  children: labels.submit.label,
                  variant: "filled",
                  color: "primary",
                  onClick: onSubmit,
                  loading: state.loading,
                }),
              })}
              {state.data?.error ===
                AuthenticationLoginError.InvalidCredentials && (
                <>
                  {uiRegistry().components.renderBox({
                    mt: 4,
                    children: uiRegistry().components.renderAlert({
                      type: "warning",
                      children: messages.invalidCredentials,
                    }),
                  })}
                </>
              )}
              {state.data?.error === AuthenticationLoginError.ServerError && (
                <>
                  {uiRegistry().components.renderBox({
                    mt: 4,
                    children: uiRegistry().components.renderAlert({
                      type: "error",
                      children: messages.serverError,
                    }),
                  })}
                </>
              )}
            </>
          ),
        }),
      })}
    </>
  )
}

export default LoginForm
