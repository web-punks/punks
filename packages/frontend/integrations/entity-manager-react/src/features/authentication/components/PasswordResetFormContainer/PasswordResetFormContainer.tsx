import {
  FormPageContainer,
  FormPageLayout,
} from "../../../../ui/FormPageContainer"
import PasswordResetForm, {
  PasswordResetFormClasses,
  PasswordResetFormEvents,
  PasswordResetFormLabels,
  PasswordResetFormMessages,
} from "../PasswordResetForm/PasswordResetForm"

export type PasswordResetFormParams = {
  classes?: PasswordResetFormClasses
  labels: PasswordResetFormLabels
  messages: PasswordResetFormMessages
  events: PasswordResetFormEvents
  resetToken?: string
}

export interface PasswordResetFormContainerProps {
  form: PasswordResetFormParams
  layout: FormPageLayout
}

const PasswordResetFormContainer = ({
  form,
  layout,
}: PasswordResetFormContainerProps) => {
  return (
    <FormPageContainer layout={layout}>
      <PasswordResetForm {...form} />
    </FormPageContainer>
  )
}

export default PasswordResetFormContainer
