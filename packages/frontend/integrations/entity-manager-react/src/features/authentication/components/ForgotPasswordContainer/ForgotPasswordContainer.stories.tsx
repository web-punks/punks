import type { Meta, StoryObj } from "@storybook/react"

import { ForgotPasswordContainer } from "."
import {
  AuthenticationLoginError,
  AuthenticationProvider,
} from "../../../../modules"

const meta = {
  title: "Components/Authentication/ForgotPasswordContainer",
  component: ForgotPasswordContainer,
  tags: ["autodocs"],
  parameters: {
    layout: "fullscreen",
  },
  args: {
    layout: {
      logo: (
        <img
          src="https://a.storyblok.com/f/196735/601x601/29667fe0ad/fav10.png"
          style={{
            width: 100,
          }}
        />
      ),
      backgroundImageUrl:
        "https://source.unsplash.com/random/900%C3%97700/?nature",
    },
    form: {
      events: {
        onSuccess: () => {
          alert("Forgot password success")
        },
      },
      labels: {
        email: {
          label: "Email",
          placeholder: "Email",
        },
        submit: {
          label: "Submit",
        },
      },
      messages: {
        serverError: "Server error",
        completed: "Completed",
        userNotFound: "User not found",
      },
    },
  },
} satisfies Meta<typeof ForgotPasswordContainer>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{}}
        actions={{
          login: async () => {
            throw new Error("Not implemented")
          },
          forgotPassword: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: true,
            }
          },
          passwordReset: async () => {
            throw new Error("Not implemented")
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const InvalidCredentials: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{
          directoryId: "test",
        }}
        actions={{
          login: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: false,
              error: AuthenticationLoginError.InvalidCredentials,
            }
          },
          forgotPassword: async () => {
            throw new Error("Not implemented")
          },
          passwordReset: async () => {
            throw new Error("Not implemented")
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const ServerError: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{
          directoryId: "test",
        }}
        actions={{
          login: async () => {
            throw new Error("Not implemented")
          },
          forgotPassword: async () => {
            return {
              success: false,
            }
          },
          passwordReset: async () => {
            throw new Error("Not implemented")
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}
