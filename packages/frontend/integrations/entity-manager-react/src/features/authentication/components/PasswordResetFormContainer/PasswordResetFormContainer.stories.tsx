import type { Meta, StoryObj } from "@storybook/react"

import { PasswordResetFormContainer } from "."
import {
  AuthenticationPasswordResetError,
  AuthenticationProvider,
} from "../../../../modules"

const meta = {
  title: "Components/Authentication/PasswordResetFormContainer",
  component: PasswordResetFormContainer,
  tags: ["autodocs"],
  parameters: {
    layout: "fullscreen",
  },
  args: {
    layout: {
      logo: (
        <img
          src="https://a.storyblok.com/f/196735/601x601/29667fe0ad/fav10.png"
          style={{
            width: 100,
          }}
        />
      ),
      backgroundImageUrl:
        "https://source.unsplash.com/random/900%C3%97700/?nature",
    },
    form: {
      events: {
        onSuccess: () => {
          alert("Forgot password success")
        },
      },
      labels: {
        email: {
          label: "Email",
        },
        password: {
          label: "Password",
          placeholder: "Password",
        },
        submit: {
          label: "Submit",
        },
      },
      messages: {
        invalidPassword: "Invalid password",
        serverError: "Server error",
      },
      resetToken: "test",
    },
  },
} satisfies Meta<typeof PasswordResetFormContainer>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{}}
        actions={{
          login: async () => {
            throw new Error("Not implemented")
          },
          forgotPassword: async () => {
            throw new Error("Not implemented")
          },
          passwordReset: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: true,
            }
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const InvalidPassword: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{
          directoryId: "test",
        }}
        actions={{
          login: async () => {
            throw new Error("Not implemented")
          },
          forgotPassword: async () => {
            throw new Error("Not implemented")
          },
          passwordReset: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: false,
              error: AuthenticationPasswordResetError.InvalidPassword,
            }
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const ServerError: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{
          directoryId: "test",
        }}
        actions={{
          login: async () => {
            throw new Error("Not implemented")
          },
          forgotPassword: async () => {
            throw new Error("Not implemented")
          },
          passwordReset: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: false,
              error: AuthenticationPasswordResetError.ServerError,
            }
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}
