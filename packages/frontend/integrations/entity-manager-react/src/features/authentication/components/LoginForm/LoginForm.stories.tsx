import type { Meta, StoryObj } from "@storybook/react"

import { LoginForm } from "."
import {
  AuthenticationLoginError,
  AuthenticationProvider,
} from "../../../../modules"

const meta = {
  title: "Components/Authentication/LoginForm",
  component: LoginForm,
  tags: ["autodocs"],
  args: {
    events: {
      onSuccess: () => {
        alert("Login success")
      },
    },
    labels: {
      username: {
        label: "Username",
        placeholder: "Username",
      },
      password: {
        label: "Password",
        placeholder: "Password",
      },
      submit: {
        label: "Login",
      },
    },
    messages: {
      invalidCredentials: "Invalid credentials",
      serverError: "Server error",
    },
  },
  decorators: [
    (Story: any) => (
      <div
        style={{
          maxWidth: 400,
          margin: "auto",
        }}
      >
        <Story />
      </div>
    ),
  ],
} satisfies Meta<typeof LoginForm>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{}}
        actions={{
          login: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: true,
            }
          },
          forgotPassword: async () => {
            throw new Error("Not implemented")
          },
          passwordReset: async () => {
            throw new Error("Not implemented")
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const InvalidCredentials: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{
          directoryId: "test",
        }}
        actions={{
          login: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: false,
              error: AuthenticationLoginError.InvalidCredentials,
            }
          },
          forgotPassword: async () => {
            throw new Error("Not implemented")
          },
          passwordReset: async () => {
            throw new Error("Not implemented")
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const ServerError: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{
          directoryId: "test",
        }}
        actions={{
          login: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: false,
              error: AuthenticationLoginError.ServerError,
            }
          },
          forgotPassword: async () => {
            throw new Error("Not implemented")
          },
          passwordReset: async () => {
            throw new Error("Not implemented")
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}
