import LoginForm, {
  LoginFormClasses,
  LoginFormEvents,
  LoginFormLabels,
  LoginFormMessages,
} from "../LoginForm/LoginForm"
import {
  FormPageContainer,
  FormPageLayout,
} from "../../../../ui/FormPageContainer"

export type LoginFormParams = {
  classes?: LoginFormClasses
  labels: LoginFormLabels
  messages: LoginFormMessages
  events: LoginFormEvents
}

export interface LoginContainerProps {
  form: LoginFormParams
  layout: FormPageLayout
}

const LoginContainer = ({ form, layout }: LoginContainerProps) => {
  return (
    <FormPageContainer layout={layout}>
      <LoginForm {...form} />
    </FormPageContainer>
  )
}

export default LoginContainer
