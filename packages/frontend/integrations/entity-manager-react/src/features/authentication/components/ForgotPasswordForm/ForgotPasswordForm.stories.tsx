import type { Meta, StoryObj } from "@storybook/react"

import { ForgotPasswordForm } from "."
import { AuthenticationProvider } from "../../../../modules"

const meta = {
  title: "Components/Authentication/ForgotPasswordForm",
  component: ForgotPasswordForm,
  tags: ["autodocs"],
  args: {
    events: {
      onSuccess: () => {
        alert("Forgot password")
      },
    },
    labels: {
      email: {
        label: "Email",
        placeholder: "Email",
      },
      submit: {
        label: "Login",
      },
    },
    messages: {
      serverError: "Server error",
      completed: "Completed",
      userNotFound: "User not found",
    },
  },
  decorators: [
    (Story: any) => (
      <div
        style={{
          maxWidth: 400,
          margin: "auto",
        }}
      >
        <Story />
      </div>
    ),
  ],
} satisfies Meta<typeof ForgotPasswordForm>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{
          directoryId: "test",
        }}
        actions={{
          login: async () => {
            throw new Error("Not implemented")
          },
          forgotPassword: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: true,
            }
          },
          passwordReset: async () => {
            throw new Error("Not implemented")
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}

export const ServerError: Story = {
  args: {},
  decorators: [
    (Story: any) => (
      <AuthenticationProvider
        context={{}}
        actions={{
          login: async () => {
            throw new Error("Not implemented")
          },
          forgotPassword: async () => {
            await new Promise((resolve) => setTimeout(resolve, 300))
            return {
              success: false,
            }
          },
          passwordReset: async () => {
            throw new Error("Not implemented")
          },
          logout: async () => {
            throw new Error("Not implemented")
          },
        }}
      >
        <Story />
      </AuthenticationProvider>
    ),
  ],
}
