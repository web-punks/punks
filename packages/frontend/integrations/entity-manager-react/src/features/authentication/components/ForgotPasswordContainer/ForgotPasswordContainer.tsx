import {
  FormPageContainer,
  FormPageLayout,
} from "../../../../ui/FormPageContainer"
import { ForgotPasswordForm } from "../ForgotPasswordForm"
import {
  ForgotPasswordFormClasses,
  ForgotPasswordFormEvents,
  ForgotPasswordFormLabels,
  ForgotPasswordFormMessages,
} from "../ForgotPasswordForm/ForgotPasswordForm"

export type ForgotPasswordFormParams = {
  classes?: ForgotPasswordFormClasses
  labels: ForgotPasswordFormLabels
  messages: ForgotPasswordFormMessages
  events: ForgotPasswordFormEvents
}

export interface ForgotPasswordContainerProps {
  form: ForgotPasswordFormParams
  layout: FormPageLayout
}

const ForgotPasswordContainer = ({
  form,
  layout,
}: ForgotPasswordContainerProps) => {
  return (
    <FormPageContainer layout={layout}>
      <ForgotPasswordForm {...form} />
    </FormPageContainer>
  )
}

export default ForgotPasswordContainer
