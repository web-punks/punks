export {
  default as ForgotPasswordForm,
  ForgotPasswordFormProps,
  ForgotPasswordFormClasses,
  ForgotPasswordFormLabels,
  ForgotPasswordFormMessages,
  ForgotPasswordFormEvents,
} from "./ForgotPasswordForm"
