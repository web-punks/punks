import { uiRegistry } from "@punks/ui-components"
import { classNames } from "@punks/ui-core"
import React, { CSSProperties, Fragment, useRef, useState } from "react"
import { usePasswordResetAction } from "../../../../modules/authentication/actions/usePasswordResetAction"
import { AuthenticationPasswordResetError } from "../../../../modules/authentication"

export type PasswordResetFormClasses = {
  root?: string
  block?: string
  input?: string
  submit?: string
  checkbox?: {
    root?: string
    label?: string
  }
}

export type PasswordResetFormInputLabel = {
  label: string
  placeholder?: string
}

export type PasswordResetFormCtaLabel = {
  label: string
}

export type PasswordResetFormLabels = {
  email: PasswordResetFormInputLabel
  password: PasswordResetFormInputLabel
  submit: PasswordResetFormCtaLabel
}

export type PasswordResetFormMessages = {
  serverError: string
  invalidPassword: string
}

export type PasswordResetFormEvents = {
  onSuccess?: () => void
  onFailure?: () => void
}

export enum PasswordResetExtrafieldsType {
  CHECKBOX = "checkbox",
}
export type PasswordResetFormExtrafields = {
  type: PasswordResetExtrafieldsType
  name: string
  required?: boolean
  label: string | React.ReactNode
  url?: string
}

export interface PasswordResetFormProps {
  fullWidth?: boolean
  className?: string
  classes?: PasswordResetFormClasses
  labels: PasswordResetFormLabels
  messages: PasswordResetFormMessages
  events?: PasswordResetFormEvents
  resetToken?: string
  email?: string
  extraFields?: PasswordResetFormExtrafields[]
}

type generateExtraFieldsProps = {
  field: PasswordResetFormExtrafields
  classes?: PasswordResetFormClasses
  value: any
  setExtraFieldValues: (
    value: React.SetStateAction<Record<string, any>>
  ) => void
}

const generateExtraFields = ({
  field,
  classes,
  value,
  setExtraFieldValues,
}: generateExtraFieldsProps) => {
  switch (field.type) {
    case PasswordResetExtrafieldsType.CHECKBOX:
      return uiRegistry().components.renderFormInput({
        className: classes?.checkbox?.root,
        children: (
          <>
            {uiRegistry().components.renderCheckbox({
              required: field.required,
              className: classes?.checkbox?.label,
              onChange: (e) =>
                setExtraFieldValues((prev) => ({
                  ...prev,
                  [field.name]: e.target.checked,
                })),
              checked: value || false,
            })}
            {uiRegistry().components.renderFormLabel({
              children: field.label,
              style: { margin: 0 },
              requiredMarker: field.required ? "*" : undefined,
            })}
          </>
        ),
      })
  }
}

const PasswordResetForm = ({
  events,
  labels,
  messages,
  className,
  classes,
  fullWidth = true,
  resetToken,
  email,
  extraFields,
}: PasswordResetFormProps) => {
  const { handleResetPassword, state } = usePasswordResetAction({
    onSuccess: events?.onSuccess,
    onError: events?.onFailure,
  })

  const passwordRef = useRef<HTMLInputElement>(null!)
  const [extraFieldValues, setExtraFieldValues] = useState<Record<string, any>>(
    {}
  )

  const onSubmit = async () => {
    const password = passwordRef.current?.value

    if (!password) {
      return
    }

    if (
      extraFields?.length &&
      extraFields?.some(
        (field) => field.required && !extraFieldValues[field.name]
      )
    )
      return

    if (!resetToken) {
      throw new Error("No reset token provided")
    }

    await handleResetPassword({
      password,
      token: resetToken,
      ...(extraFields?.length ? { extraFields: extraFieldValues } : {}),
    })
  }

  return (
    <>
      {uiRegistry().components.renderBox({
        children: uiRegistry().components.renderBox({
          className: classNames(className, classes?.root),
          children: (
            <>
              {email &&
                uiRegistry().components.renderFormInput({
                  className: classes?.block,
                  label: labels.email.label,
                  children: uiRegistry().components.renderTextInput({
                    className: classes?.input,
                    fullWidth,
                    value: email,
                    disabled: true,
                    readOnly: true,
                  }),
                })}
              {uiRegistry().components.renderFormInput({
                className: classes?.block,
                label: labels.password.label,
                children: uiRegistry().components.renderTextInput({
                  className: classes?.input,
                  fullWidth,
                  placeholder: labels.password.label,
                  inputRef: passwordRef,
                  type: "password",
                  onKeyDown: (e) => {
                    if (e.key === "Enter") {
                      onSubmit()
                    }
                  },
                }),
              })}

              {!!extraFields?.length &&
                extraFields?.map((field) => (
                  <Fragment key={field.name}>
                    {generateExtraFields({
                      field,
                      classes,
                      value: extraFieldValues[field.name],
                      setExtraFieldValues,
                    })}
                  </Fragment>
                ))}

              {uiRegistry().components.renderBox({
                mt: 4,
                children: uiRegistry().components.renderButton({
                  className: classes?.submit,
                  fullWidth,
                  children: labels.submit.label,
                  variant: "filled",
                  color: "primary",
                  onClick: onSubmit,
                  loading: state.loading,
                }),
              })}

              {state.data?.error ===
                AuthenticationPasswordResetError.InvalidPassword && (
                <>
                  {uiRegistry().components.renderBox({
                    mt: 4,
                    children: uiRegistry().components.renderAlert({
                      type: "warning",
                      children: messages.invalidPassword,
                    }),
                  })}
                </>
              )}
              {state.data?.error ===
                AuthenticationPasswordResetError.ServerError && (
                <>
                  {uiRegistry().components.renderBox({
                    mt: 4,
                    children: uiRegistry().components.renderAlert({
                      type: "error",
                      children: messages.serverError,
                    }),
                  })}
                </>
              )}
            </>
          ),
        }),
      })}
    </>
  )
}

export default PasswordResetForm
