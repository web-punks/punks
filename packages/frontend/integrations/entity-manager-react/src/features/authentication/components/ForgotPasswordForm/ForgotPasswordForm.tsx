import { uiRegistry } from "@punks/ui-components"
import { classNames } from "@punks/ui-core"
import { useRef } from "react"
import { useForgotPasswordAction } from "../../../../modules/authentication/actions/useForgotPasswordAction"
import { AuthenticationForgotPasswordError } from "../../../../modules"

export type ForgotPasswordFormClasses = {
  root?: string
  block?: string
  input?: string
  submit?: string
}

export type ForgotPasswordInputLabel = {
  label: string
  placeholder?: string
}

export type ForgotPasswordCtaLabel = {
  label: string
}

export type ForgotPasswordFormLabels = {
  email: ForgotPasswordInputLabel
  submit: ForgotPasswordCtaLabel
}

export type ForgotPasswordFormMessages = {
  serverError: string
  completed: string
  userNotFound: string
}

export type ForgotPasswordFormEvents = {
  onSuccess?: () => void
  onFailure?: () => void
}

export interface ForgotPasswordFormProps {
  fullWidth?: boolean
  className?: string
  classes?: ForgotPasswordFormClasses
  labels: ForgotPasswordFormLabels
  messages: ForgotPasswordFormMessages
  events?: ForgotPasswordFormEvents
}

const ForgotPasswordForm = ({
  events,
  labels,
  messages,
  className,
  classes,
  fullWidth = true,
}: ForgotPasswordFormProps) => {
  const { handleForgotPassword, state } = useForgotPasswordAction({
    onSuccess: events?.onSuccess,
    onError: events?.onFailure,
  })

  const emailRef = useRef<HTMLInputElement>(null)

  const onSubmit = async () => {
    const email = emailRef.current?.value

    if (!email) {
      return
    }

    await handleForgotPassword({
      email,
    })
  }

  return (
    <>
      {uiRegistry().components.renderBox({
        children: uiRegistry().components.renderBox({
          className: classNames(className, classes?.root),
          children: (
            <>
              {uiRegistry().components.renderFormInput({
                className: classes?.block,
                label: labels.email.label,
                children: uiRegistry().components.renderTextInput({
                  className: classes?.input,
                  fullWidth,
                  placeholder: labels.email.label,
                  inputRef: emailRef,
                  onKeyDown: (e) => {
                    if (e.key === "Enter") {
                      onSubmit()
                    }
                  },
                }),
              })}

              {uiRegistry().components.renderBox({
                mt: 4,
                children: uiRegistry().components.renderButton({
                  className: classes?.submit,
                  fullWidth,
                  children: labels.submit.label,
                  variant: "filled",
                  color: "primary",
                  onClick: onSubmit,
                  loading: state.loading,
                }),
              })}

              {state.data?.success === true && (
                <>
                  {uiRegistry().components.renderBox({
                    mt: 4,
                    children: uiRegistry().components.renderAlert({
                      type: "info",
                      children: messages.completed,
                    }),
                  })}
                </>
              )}
              {state.data?.error ===
                AuthenticationForgotPasswordError.UserNotFound && (
                <>
                  {uiRegistry().components.renderBox({
                    mt: 4,
                    children: uiRegistry().components.renderAlert({
                      type: "warning",
                      children: messages.userNotFound,
                    }),
                  })}
                </>
              )}
              {state.data?.error ===
                AuthenticationForgotPasswordError.ServerError && (
                <>
                  {uiRegistry().components.renderBox({
                    mt: 4,
                    children: uiRegistry().components.renderAlert({
                      type: "error",
                      children: messages.serverError,
                    }),
                  })}
                </>
              )}
            </>
          ),
        }),
      })}
    </>
  )
}

export default ForgotPasswordForm
