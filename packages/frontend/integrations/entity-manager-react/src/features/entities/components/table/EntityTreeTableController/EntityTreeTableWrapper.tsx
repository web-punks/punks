import { TreeNode, uiRegistry } from "@punks/ui-components"
import { EntityTableControls, EntityTableLayoutOptions } from "../../../types"
import { EntityCreateModal } from "../../../modals/EntityCreateModal"
import { EntityUpdateModal } from "../../../modals/EntityUpdateModal"
import EntityDeleteModal from "../../../modals/EntityDeleteModal"
import { EntityDetailsModal } from "../../../modals/EntityDetailsModal"
import { useEntityManagerDefinition } from "../../../services/context/definition"
import { useEntityController } from "../../../services/context/controller"
import { EntityImportModal } from "../../../modals/EntityImportModal"
import {
  EntityRowActions,
  EntityTableColumnType,
  EntityTableSelection,
} from "../../../services/types/tables"
import { EntityViewHistoryModal } from "../../../modals/EntityViewHistoryModal"
import { useEntityManagerLabel } from "../../../../../modules/labels"
import { EntityItemControls } from "../../../services/types"
import { EntityMainMenuControls } from "../../../services/types/menuControls"
import { EntityViewJsonModal } from "../../../modals/EntityViewJsonModal"
import { EntityTreeTable } from "./EntityTreeTable"
import { EntityToolbar } from "../common/toolbar/EntityToolbar"
import { EntityChildCreateModal } from "../../../modals/EntityChildCreateModal"
import { EntityControllerLayoutRenderer } from "../../../types/layout"

export interface EntityTreeTableWrapperProps<TEntity, TFilters> {
  columns: EntityTableColumnType<TEntity, TFilters, unknown, unknown>[]
  layout?: EntityTableLayoutOptions
  itemControls?: EntityItemControls<TEntity>
  menuControls?: EntityMainMenuControls<TEntity>
  controls?: EntityTableControls
  title?: React.ReactNode
  rowActions?: EntityRowActions<TreeNode<TEntity>>
  selection?: EntityTableSelection<TreeNode<TEntity>>
  emptyContent?: React.ReactNode
  renderer: EntityControllerLayoutRenderer
}

export function EntityTreeTableWrapper<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  columns,
  layout,
  itemControls,
  menuControls,
  title,
  controls,
  rowActions,
  selection,
  emptyContent,
  renderer,
}: EntityTreeTableWrapperProps<TEntity, TFilters>) {
  const { selectors } = useEntityManagerDefinition<
    TEntity,
    TFilters,
    TCreate,
    TUpdate,
    TSorting
  >()
  const {
    actions: {
      openItemState,
      closeDetailsModal,
      closeViewHistoryModal,
      closeItemJsonModal,
      clearItemParentItemForCreateChild,
      itemParentForCreateChild,
      itemToDelete,
      editItemState,
      closeCreateModal,
      closeEditModal,
      clearItemToDelete,
      closeImportModal,
      fetchState,
      sampleDownload,
      itemsImport,
      importState,
    },
    filteredData,
    empty,
    emptyDataSource,
    firstFetchCompleted,
    ui,
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    TreeNode<TEntity>
  >()

  const handleImport = async (file: File) => {
    await itemsImport({
      file,
    })
    closeImportModal()
  }

  const { getLabel } = useEntityManagerLabel()

  return (
    <>
      <EntityTreeTable
        title={title}
        layout={layout}
        data={filteredData}
        toolbar={<EntityToolbar controls={menuControls} />}
        columns={columns}
        controls={controls}
        itemControls={itemControls}
        grid={layout?.grid}
        rowActions={rowActions}
        selection={selection}
        renderer={renderer}
        emptyDataSource={emptyDataSource}
        firstFetchCompleted={firstFetchCompleted}
        bottomContent={
          <>
            {fetchState.error &&
              uiRegistry().components.renderAlert({
                type: "error",
                children: getLabel("operations:fetch:messages:genericError"),
              })}
            {empty &&
              (emptyContent ?? fetchState.completed) &&
              empty &&
              uiRegistry().components.renderAlert({
                type: "info",
                children: getLabel("table:messages:empty"),
              })}

            {!empty &&
              filteredData?.length === 0 &&
              uiRegistry().components.renderAlert({
                type: "info",
                children: getLabel("table:messages:emptyFilter"),
              })}
          </>
        }
      />

      <EntityDetailsModal
        open={!!openItemState?.data}
        item={openItemState?.data}
        onClose={(options) => closeDetailsModal(options)}
      />
      <EntityUpdateModal
        open={!!editItemState?.data}
        item={
          editItemState?.data
            ? {
                data: editItemState.data,
                id: selectors.getId(editItemState.data),
              }
            : undefined
        }
        onClose={() => closeEditModal()}
        onCancel={(options) => closeEditModal(options)}
      />
      <EntityDeleteModal
        open={!!itemToDelete}
        item={itemToDelete}
        onClose={() => clearItemToDelete()}
      />
      <EntityCreateModal
        open={ui.createModal.open}
        onClose={() => closeCreateModal()}
        onCancel={(options) => closeCreateModal(options)}
      />
      <EntityChildCreateModal
        open={ui.childCreateModal.open}
        onClose={() => clearItemParentItemForCreateChild()}
        onCancel={(options) => clearItemParentItemForCreateChild(options)}
        parentItem={itemParentForCreateChild}
      />
      <EntityImportModal
        open={ui.uploadModal.open}
        onSubmit={(file) => handleImport(file)}
        onSampleDownload={() => sampleDownload()}
        onCancel={(options) => closeImportModal(options)}
        importLoading={importState.loading}
      />
      <EntityViewHistoryModal
        open={ui.viewHistoryModal.open}
        item={ui.viewHistoryModal.item}
        onClose={(options) => closeViewHistoryModal(options)}
      />
      <EntityViewJsonModal
        open={ui.viewJsonModal.open}
        item={ui.viewJsonModal.item}
        onClose={() => closeItemJsonModal()}
      />
    </>
  )
}
