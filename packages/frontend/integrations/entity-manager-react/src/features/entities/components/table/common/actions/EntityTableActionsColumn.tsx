import { BoxProps, uiRegistry } from "@punks/ui-components"
import EntityCustomRowAction from "../../../../actions/EntityCustomRowAction"
import EntityStandardRowAction from "../../../../actions/EntityStandardRowAction"
import {
  EntityItemControls,
  CustomEntityItemControl,
  StandardItemControl,
  MenuItemControl,
} from "../../../../services/types/itemControls"
import { EntityTableEntityContextMenu } from "../menu/EntityTableEntityContextMenu"
import {
  isControlEnabled,
  isCustomRowAction,
  isStandardRowAction,
  isMenuRowAction,
} from "../../../../utils/rowActions"
import { CSSProperties } from "react"

export interface EntityTableActionsColumnProps<TEntity> {
  item: TEntity
  rowControls: EntityItemControls<TEntity>
  boxProps?: BoxProps
}

const EntityTableActionsColumn = <TEntity,>({
  item,
  rowControls,
  boxProps,
}: EntityTableActionsColumnProps<TEntity>) => {
  return (
    <>
      {uiRegistry().components.renderBox({
        flex: true,
        justifyContent: "end",
        style: {
          // .cellBorderLeft::before {
          //   position: absolute;
          //   top: 6px;
          //   left: 0;
          //   height: calc(100% - 12px);
          //   content: "";
          //   border-left: ;
          // }
        },
        gap: 3,
        ...boxProps,
        children: (
          <>
            {rowControls
              .filter((x) => isControlEnabled(x, item))
              .map((x, i) => (
                <div
                  key={i}
                  style={{
                    ...(i === 0 && {
                      borderLeft: "1px solid var(--wp-theme-color-grey-20)",
                      paddingLeft: "12px",
                    }),
                  }}
                >
                  {isCustomRowAction(x.type) && (
                    <EntityCustomRowAction
                      item={item}
                      control={x as CustomEntityItemControl<TEntity>}
                    />
                  )}
                  {isStandardRowAction(x.type) && (
                    <EntityStandardRowAction
                      item={item}
                      control={x as StandardItemControl<TEntity>}
                    />
                  )}
                  {isMenuRowAction(x.type) && (
                    <EntityTableEntityContextMenu
                      item={item}
                      items={(x as MenuItemControl<TEntity>).items}
                    />
                  )}
                </div>
              ))}
          </>
        ),
      })}
    </>
  )
}

export default EntityTableActionsColumn
