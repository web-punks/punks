import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import { useForm } from "react-hook-form"
import { FooListItemDto } from "../../../../../../__test__/infrastructure/api/backend"

const schema = yup.object().shape({
  name: yup.string().required(),
  uid: yup.string().required(),
  type: yup.string().required(),
  other: yup.string(),
  age: yup.number().required(),
})

export type FooEditData = yup.InferType<typeof schema>

export const useFooEditForm = ({
  defaultItem,
}: {
  defaultItem?: FooListItemDto
}) => {
  return useForm<FooEditData>({
    resolver: yupResolver(schema),
    defaultValues: defaultItem
      ? {
          name: defaultItem.name,
          uid: defaultItem.uid,
          age: defaultItem.age,
          type: defaultItem.type,
          other: defaultItem.other,
        }
      : undefined,
  })
}
