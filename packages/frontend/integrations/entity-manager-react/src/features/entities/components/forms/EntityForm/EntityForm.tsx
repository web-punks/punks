import { uiRegistry } from "@punks/ui-components"
import { ReactNode } from "react"

export type EntityFormCta = {
  label: ReactNode
  onClick: () => void
  loading?: boolean
  disabled?: boolean
}

export type EntityFormCtas = {
  confirm: EntityFormCta
  cancel: EntityFormCta
}

export interface EntityFormProps {
  children: any
  cta: EntityFormCtas
}

const EntityForm = ({ cta, children }: EntityFormProps) => {
  return (
    <>
      {uiRegistry().components.renderFormRoot({
        children,
        cta: {
          align: "right",
          cancel: {
            onClick: cta.cancel.onClick,
            label: cta.cancel.label,
            disabled: cta.cancel.disabled,
            loading: cta.cancel.loading,
          },
          confirm: {
            onClick: cta.confirm.onClick,
            label: cta.confirm.label,
            disabled: cta.confirm.disabled,
            loading: cta.confirm.loading,
          },
        },
      })}
    </>
  )
}
export default EntityForm
