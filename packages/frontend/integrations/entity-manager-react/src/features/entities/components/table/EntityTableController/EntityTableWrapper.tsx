import { BoxProps, uiRegistry } from "@punks/ui-components"
import { EntityCreateModal } from "../../../modals/EntityCreateModal"
import { EntityTable } from "./EntityTable"
import { EntityToolbar } from "../common/toolbar/EntityToolbar"
import { EntityUpdateModal } from "../../../modals/EntityUpdateModal"
import EntityDeleteModal from "../../../modals/EntityDeleteModal"
import { EntityDetailsModal } from "../../../modals/EntityDetailsModal"
import { EntityTableControls, EntityTableLayoutOptions } from "../../../types"
import { useEntityManagerDefinition } from "../../../services/context/definition"
import { useEntityController } from "../../../services/context/controller"
import { EntityImportModal } from "../../../modals/EntityImportModal"
import {
  EntityRowActions,
  EntityTableColumnType,
  EntityTableSelection,
} from "../../../services/types/tables"
import { EntityViewHistoryModal } from "../../../modals/EntityViewHistoryModal"
import { useEntityManagerLabel } from "../../../../../modules/labels"
import { EntityItemControls } from "../../../services/types"
import { EntityMainMenuControls } from "../../../services/types/menuControls"
import { EntityViewJsonModal } from "../../../modals/EntityViewJsonModal"
import { EntityControllerLayoutRenderer } from "../../../types/layout"

export interface EntityTableWrapperProps<TEntity, TFilters> {
  columns: EntityTableColumnType<TEntity, TFilters, unknown, unknown>[]
  layout?: EntityTableLayoutOptions
  itemControls?: EntityItemControls<TEntity>
  boxProps?: BoxProps
  menuControls?: EntityMainMenuControls<TEntity>
  searchbarControls?: EntityMainMenuControls<TEntity>
  controls?: EntityTableControls
  title?: React.ReactNode
  rowActions?: EntityRowActions<TEntity>
  selection?: EntityTableSelection<TEntity>
  emptyContent?: React.ReactNode
  renderer: EntityControllerLayoutRenderer
}

export function EntityTableWrapper<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  columns,
  layout,
  itemControls,
  boxProps,
  menuControls,
  searchbarControls,
  title,
  controls,
  rowActions,
  selection,
  emptyContent,
  renderer,
}: EntityTableWrapperProps<TEntity, TFilters>) {
  const { selectors } = useEntityManagerDefinition<
    TEntity,
    TFilters,
    TCreate,
    TUpdate,
    TSorting
  >()
  const {
    actions: {
      openItemState,
      closeDetailsModal,
      closeViewHistoryModal,
      closeItemJsonModal,
      itemToDelete,
      editItemState,
      closeCreateModal,
      closeEditModal,
      clearItemToDelete,
      closeImportModal,
      fetchState,
      sampleDownload,
      itemsImport,
      importState,
    },
    filteredData,
    empty,
    emptyDataSource,
    firstFetchCompleted,
    ui,
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    TEntity
  >()

  const handleImport = async (file: File) => {
    await itemsImport({
      file,
    })
    closeImportModal()
  }

  const { getLabel } = useEntityManagerLabel()

  return (
    <>
      <EntityTable
        title={title}
        layout={layout}
        data={filteredData}
        toolbar={<EntityToolbar controls={menuControls} />}
        searchbarToolbar={<EntityToolbar controls={searchbarControls} />}
        columns={columns}
        controls={controls}
        itemControls={itemControls}
        boxProps={boxProps}
        grid={layout?.grid}
        rowActions={rowActions}
        selection={selection}
        renderer={renderer}
        emptyDataSource={emptyDataSource}
        firstFetchCompleted={firstFetchCompleted}
        bottomContent={
          <>
            {fetchState.error &&
              uiRegistry().components.renderAlert({
                type: "error",
                children: getLabel("operations:fetch:messages:genericError"),
              })}

            {empty &&
              (emptyContent ??
                uiRegistry().components.renderAlert({
                  type: "info",
                  children: getLabel("table:messages:empty"),
                }))}

            {!empty &&
              filteredData?.length === 0 &&
              uiRegistry().components.renderAlert({
                type: "info",
                children: getLabel("table:messages:emptyFilter"),
              })}
          </>
        }
      />

      <EntityDetailsModal
        open={!!openItemState?.data}
        item={openItemState?.data}
        onClose={(options) => closeDetailsModal(options)}
      />
      <EntityUpdateModal
        open={!!editItemState?.data}
        item={
          editItemState?.data
            ? {
                data: editItemState.data,
                id: selectors.getId(editItemState.data),
              }
            : undefined
        }
        onClose={() => closeEditModal()}
        onCancel={(options) => closeEditModal(options)}
      />
      <EntityDeleteModal
        open={!!itemToDelete}
        item={itemToDelete}
        onClose={() => clearItemToDelete()}
      />
      <EntityCreateModal
        open={ui.createModal.open}
        onClose={() => closeCreateModal()}
        onCancel={(options) => closeCreateModal(options)}
      />
      <EntityImportModal
        open={ui.uploadModal.open}
        onSubmit={(file) => handleImport(file)}
        onSampleDownload={() => sampleDownload()}
        onCancel={(options) => closeImportModal(options)}
        importLoading={importState.loading}
      />
      <EntityViewHistoryModal
        open={ui.viewHistoryModal.open}
        item={ui.viewHistoryModal.item}
        onClose={(options) => closeViewHistoryModal(options)}
      />
      <EntityViewJsonModal
        open={ui.viewJsonModal.open}
        item={ui.viewJsonModal.item}
        onClose={() => closeItemJsonModal()}
      />
    </>
  )
}
