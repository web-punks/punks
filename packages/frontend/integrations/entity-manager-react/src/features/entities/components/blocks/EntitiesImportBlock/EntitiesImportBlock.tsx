import { EntityImportPanel } from "../../../panels/EntityImportPanel"
import { useEntityController } from "../../../services/context/controller"

export interface EntitiesImportBlockProps {
  onImportError?: (error: Error) => void
  onImportStarted?: () => void
  onImportSuccess?: () => void
}

export const EntitiesImportBlock = ({
  onImportError,
  onImportStarted,
  onImportSuccess,
}: EntitiesImportBlockProps) => {
  const {
    actions: { sampleDownload, itemsImport, importState },
  } = useEntityController<
    unknown,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()

  const handleImport = async (file: File) => {
    try {
      onImportStarted?.()
      await itemsImport({
        file,
      })
      onImportSuccess?.()
    } catch (error) {
      onImportError?.(error as Error)
      console.error("entities import error", error)
    }
  }

  return (
    <EntityImportPanel
      onSubmit={(file) => handleImport(file)}
      onSampleDownload={() => sampleDownload()}
      importLoading={importState.loading}
    />
  )
}
