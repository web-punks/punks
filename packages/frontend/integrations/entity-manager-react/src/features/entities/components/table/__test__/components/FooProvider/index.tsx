import { BackendClient } from "../../../../../../../__test__/infrastructure/api"
import {
  EntityVersionsSorting,
  FooCreateDto,
  FooEntitiesExportOptions,
  FooListItemDto,
  FooSearchFilters,
  FooSearchSortingField,
  FooUpdateDto,
} from "../../../../../../../__test__/infrastructure/api/backend"
import { EntityProviders, EntityProvidersProps } from "../../../../../providers"
import { EntitiesFetchMode } from "../../../../../services"
import FooCreateForm from "../FooCreateForm"
import FooUpdateForm from "../FooUpdateForm"
import { fooLabels } from "./labels"

const FooEntityProviders = (
  props: EntityProvidersProps<
    FooListItemDto,
    FooSearchFilters,
    FooCreateDto,
    FooUpdateDto,
    FooSearchSortingField.field
  >
) => <EntityProviders {...props} />

export interface FooProviderProps {
  children: any
  dataFilter?: Partial<FooSearchFilters>
  mode: EntitiesFetchMode
}

export const FooProvider = ({
  children,
  dataFilter,
  mode,
}: FooProviderProps) => {
  return (
    <FooEntityProviders
      mode={mode}
      dataFilter={dataFilter}
      options={{
        queryFilterStrategy: "server",
      }}
      operations={{
        fetch: {
          action: (input) =>
            BackendClient.fooSearch({
              params: {
                filters: input.filters,
                traverse: input.traverse,
                options: {
                  includeFacets: true,
                  includeChildrenMap: true,
                  facetsFilters: input.baseFilter,
                },
                query: input.query
                  ? {
                      term: input.query,
                      fields: ["name", "uid"],
                    }
                  : undefined,
                paging: input.paging,
                sorting: input.sorting
                  ? {
                      fields: [
                        {
                          field: input.sorting.value,
                          direction: input.sorting
                            .direction as FooSearchSortingField.direction,
                        },
                      ],
                    }
                  : {
                      fields: [
                        {
                          field: FooSearchSortingField.field.NAME,
                          direction: FooSearchSortingField.direction.ASC,
                        },
                      ],
                    },
              },
            }),
        },
        create: {
          action: (input) =>
            BackendClient.fooCreate({
              ...input.data,
              parentId: input.parent?.id,
            }),
        },
        update: {
          action: (input) => BackendClient.fooUpdate(input.id, input.data),
        },
        delete: {
          action: (input) => BackendClient.fooDelete(input.item.id),
        },
        get: {
          action: (input) => BackendClient.fooGet(input.item.id),
        },
        export: {
          action: () =>
            BackendClient.fooExport({
              options: {
                format: FooEntitiesExportOptions.format.XLSX,
              },
            }),
        },
        sampleDownload: {
          action: () =>
            BackendClient.fooSampleDownload({
              format: FooEntitiesExportOptions.format.XLSX,
            }),
        },
        import: {
          action: async (input) =>
            BackendClient.fooImport(FooEntitiesExportOptions.format.XLSX, {
              file: input.file,
            }),
        },
        parse: {
          action: async (input) =>
            BackendClient.fooParse(FooEntitiesExportOptions.format.XLSX, {
              file: input.file,
            }),
        },
        versions: {
          action: (input) =>
            BackendClient.fooVersions({
              params: {
                entity: {
                  entityId: input.id,
                },
                sorting: {
                  direction:
                    input.sorting.direction === "desc"
                      ? EntityVersionsSorting.direction.DESC
                      : EntityVersionsSorting.direction.ASC,
                },
              },
            }),
        },
      }}
      modals={{
        createModalOptions: {
          size: "lg",
        },
        versions: {
          table: {
            columns: [
              {
                key: "age",
                title: "Age",
                render: (x) => x.age,
              },
              {
                key: "name",
                title: "Name",
                render: (x) => x.name,
              },
              {
                key: "type",
                title: "Type",
                render: (x) => x.type,
              },
              {
                key: "timestamp",
                title: "Timestamp",
                render: (x) => x.updatedOn,
              },
            ],
          },
        },
        import: {
          table: {
            columns: [
              {
                key: "name",
                title: "Name",
                render: (record) => record.item.name,
              },
              {
                key: "uid",
                title: "Uid",
                render: (record) => record.item.uid,
              },
            ],
            labels: {
              errorCodes: {
                required: "Required",
              },
            },
          },
        },
      }}
      metadata={{
        name: "Foo",
      }}
      initialPaging={{
        pageSize: 5,
      }}
      forms={{
        create: FooCreateForm,
        createAdditionalProps: {
          foo: "bar",
        },
        update: FooUpdateForm,
        updateAdditionalProps: {
          foo: "bar",
        },
        details: () => <div>details</div>,
        detailsAdditionalProps: {
          foo: "bar",
        },
      }}
      selectors={{
        getDisplayName: (entity) => entity.name,
        getId: (entity) => entity.id,
        getParentId: (entity) => entity.parentId,
      }}
      labels={fooLabels}
    >
      {children}
    </FooEntityProviders>
  )
}
