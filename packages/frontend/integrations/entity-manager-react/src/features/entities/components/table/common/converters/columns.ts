import {
  EntitiesDateFilter,
  EntitiesFilter,
  EntitiesMultiSelectFilter,
  EntitiesSingleSelectFilter,
  EntitiesTextFilter,
  EntityFacets,
  EntityFilterType,
} from "../../../../services/types/filters"
import {
  TableColumnFilter,
  TableColumnFilterType,
  TableColumnType,
  TableColumnsType,
  TableDateFilter,
  TableFilterDateValue,
  TableMultiSelectFilter,
  TableSingleSelectFilter,
  TableTextFilter,
} from "@punks/ui-components"
import { EntityTableColumnType } from "../../../../services/types/tables"

export interface EntityTableState<TFilters> {
  currentFilter?: TFilters
  onFilterChange: (filter: TFilters) => void
  facets?: EntityFacets
}

export interface TableColumnsInput<TEntity, TFilters> {
  state: EntityTableState<TFilters>
  columns: EntityTableColumnType<TEntity, TFilters, unknown, unknown>[]
}

const toTextColumnFilter = <TFilters>(
  definition: EntitiesTextFilter<TFilters>,
  state: EntityTableState<TFilters>
): TableTextFilter<unknown> => {
  return {
    type: TableColumnFilterType.Text,
    onSubmit: (value) => {
      const newFilter = definition.onSubmit(
        state.currentFilter ?? ({} as TFilters),
        value
      )
      state.onFilterChange(newFilter)
    },
    onClear: () => {
      const newFilter = definition.onClear(
        state.currentFilter ?? ({} as TFilters)
      )
      state.onFilterChange(newFilter)
    },
    enabled: definition.enabled,
    value: definition.value(state.currentFilter ?? ({} as TFilters)),
    types: definition.types ?? ["contains", "isEmpty", "isNotEmpty"],
  }
}

const toSingleSelectColumnFilter = <TFilters>(
  definition: EntitiesSingleSelectFilter<unknown, TFilters>,
  state: EntityTableState<TFilters>
): TableSingleSelectFilter<unknown> => {
  return {
    type: TableColumnFilterType.SingleSelect,
    enabled: definition.enabled,
    options: definition.options(state.facets ?? ({} as EntityFacets)),
    onChange: (value?: unknown) => {
      const newFilter = definition.onChange(
        state.currentFilter ?? ({} as TFilters),
        value
      )
      state.onFilterChange(newFilter)
    },
    selectedValue: definition.selectedValue(
      state.currentFilter ?? ({} as TFilters)
    ),
  }
}

const toMultiSelectColumnFilter = <TFilters>(
  definition: EntitiesMultiSelectFilter<unknown, TFilters>,
  state: EntityTableState<TFilters>
): TableMultiSelectFilter<unknown> => {
  return {
    type: TableColumnFilterType.MultipleSelect,
    enabled: definition.enabled,
    options: definition.options(state.facets ?? ({} as EntityFacets)),
    onChange: (values?: unknown[]) => {
      const newFilter = definition.onChange(
        state.currentFilter ?? ({} as TFilters),
        values
      )
      state.onFilterChange(newFilter)
    },
    selectedValues: definition.selectedValues(
      state.currentFilter ?? ({} as TFilters)
    ),
  }
}

const toDateColumnFilter = <TFilters>(
  definition: EntitiesDateFilter<TFilters>,
  state: EntityTableState<TFilters>
): TableDateFilter<unknown> => {
  return {
    type: TableColumnFilterType.Date,
    onSubmit: (value: TableFilterDateValue) => {
      const newFilter = definition.onSubmit(
        state.currentFilter ?? ({} as TFilters),
        value
      )
      state.onFilterChange(newFilter)
    },
    onClear: () => {
      const newFilter = definition.onClear(
        state.currentFilter ?? ({} as TFilters)
      )
      state.onFilterChange(newFilter)
    },
    enabled: definition.enabled,
    value: definition.value(state.currentFilter ?? ({} as TFilters)),
  }
}

const toTableColumnFilter = <TFilters>(
  definition: EntitiesFilter<unknown, TFilters>,
  state: EntityTableState<TFilters>
): TableColumnFilter<unknown> => {
  switch (definition.type) {
    case EntityFilterType.Text:
      return toTextColumnFilter(definition, state)
    case EntityFilterType.SingleSelect:
      return toSingleSelectColumnFilter(definition, state)
    case EntityFilterType.MultipleSelect:
      return toMultiSelectColumnFilter(definition, state)
    case EntityFilterType.Date:
      return toDateColumnFilter(definition, state)
  }
}

export const toTableColumn = <TEntity, TFilters>(
  column: EntityTableColumnType<TEntity, TFilters, unknown, unknown>,
  state: EntityTableState<TFilters>
): TableColumnType<TEntity, unknown, unknown> => {
  const { filter, span, ...other } = column
  return {
    ...other,
    widthFactor: span,
    filter: filter ? toTableColumnFilter(filter, state) : undefined,
  }
}

export const toTableColumns = <TEntity, TFilters>(
  input: TableColumnsInput<TEntity, TFilters>
): TableColumnsType<TEntity, unknown, unknown> => {
  return input.columns
    .filter((x) => !x.hidden)
    .map((x) => toTableColumn(x, input.state))
}
