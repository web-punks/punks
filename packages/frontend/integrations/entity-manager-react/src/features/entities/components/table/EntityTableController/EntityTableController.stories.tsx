import type { Meta, StoryObj } from "@storybook/react"
import {
  EntityTableController,
  EntityTableControllerProps,
} from "./EntityTableController"
import {
  FooListItemDto,
  FooSearchFilters,
  FooSearchSortingField,
  NumericFacetValue,
} from "../../../../../__test__/infrastructure/api/backend"
import { FooProvider } from "../__test__/components/FooProvider"
import {
  EntitiesFetchMode,
  EntityFilterOption,
  EntityFilterType,
  ItemControlType,
  MenuControlType,
} from "../../../services"
import { getStringFilterValue, toStringFilter } from "./utils"

const FooTableController = (
  props: EntityTableControllerProps<FooListItemDto, FooSearchFilters>
) => <EntityTableController {...props} />

const meta = {
  title: "Components/EntityTableController",
  component: FooTableController,
  args: {
    columns: [
      {
        key: "uid",
        render: (x) => x.uid,
        title: "Uid",
        filter: {
          type: EntityFilterType.Text,
          onSubmit(current, value) {
            return {
              ...current,
              uid: toStringFilter(value),
            }
          },
          onClear: (current) => {
            return {
              ...current,
              uid: undefined,
            }
          },
          value: (current) => getStringFilterValue(current.uid),
        },
      },
      {
        key: "name",
        render: (x) => x.name,
        title: "Name",
        filter: {
          type: EntityFilterType.Text,
          onSubmit(current, value) {
            return {
              ...current,
              name: toStringFilter(value),
            }
          },
          onClear: (current) => {
            return {
              ...current,
              name: undefined,
            }
          },
          value: (current) => getStringFilterValue(current.name),
        },
        sorting: {
          value: FooSearchSortingField.field.NAME,
        },
      },
      {
        key: "type",
        render: (x) => x.type,
        title: "Type",
        filter: {
          type: EntityFilterType.MultipleSelect,
          options: (facets): EntityFilterOption<string>[] => {
            return facets.type?.values.map((x: NumericFacetValue) => ({
              value: x.value,
              label: x.value.toString(),
            }))
          },
          selectedValues(current) {
            return current.type?.in
          },
          onChange(current, values): FooSearchFilters {
            return {
              ...current,
              type: (values as string[]).length
                ? {
                    ...current?.type,
                    in: values as string[],
                  }
                : undefined,
            }
          },
        },
      },
      {
        key: "other",
        render: (x) => x.other,
        title: "Other",
        filter: {
          type: EntityFilterType.Text,
          onSubmit(current, value) {
            return {
              ...current,
              other: toStringFilter(value),
            }
          },
          onClear: (current) => {
            return {
              ...current,
              other: undefined,
            }
          },
          value: (current) => getStringFilterValue(current.other),
        },
      },
      {
        key: "age",
        render: (x) => x.age,
        title: "Age",
        filter: {
          type: EntityFilterType.MultipleSelect,
          options: (facets): EntityFilterOption<number>[] => {
            return facets.age?.values.map((x: NumericFacetValue) => ({
              value: x.value,
              label: x.value.toString(),
            }))
          },
          selectedValues(current) {
            return current.age?.in
          },
          onChange(current, values): FooSearchFilters {
            return {
              ...current,
              age: (values as number[]).length
                ? {
                    ...current?.age,
                    in: values as number[],
                  }
                : undefined,
            }
          },
        },
      },
    ],
    menuControls: {
      primary: {
        styles: {
          width: 140,
        },
        controls: [
          {
            type: MenuControlType.Create,
          },
          {
            type: MenuControlType.Import,
          },
        ],
      },
      secondary: {
        controls: [
          {
            type: MenuControlType.Export,
          },
        ],
      },
    },
    itemControls: [
      {
        type: ItemControlType.Edit,
      },
      {
        type: ItemControlType.ViewJson,
      },
      {
        type: ItemControlType.Menu,
        items: [
          {
            type: ItemControlType.ViewJson,
          },
          {
            type: ItemControlType.ViewHistory,
          },
          {
            type: ItemControlType.Custom,
            action: async () => {},
            cta: {
              label: "Custom",
            },
          },
          {
            type: ItemControlType.Delete,
          },
        ],
      },
    ],
  },
  decorators: [
    (Story) => (
      <div style={{ maxWidth: 1000, margin: "0 auto" }}>
        <Story />
      </div>
    ),
  ],
} satisfies Meta<typeof FooTableController>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
  decorators: [
    (Story) => (
      <FooProvider mode={EntitiesFetchMode.List}>
        <Story />
      </FooProvider>
    ),
  ],
}

export const CustomLayout: Story = {
  decorators: [
    (Story) => (
      <FooProvider mode={EntitiesFetchMode.List}>
        <Story />
      </FooProvider>
    ),
  ],
  args: {
    renderer: ({ content, head }) => (
      <div>
        <div>{head?.title}</div>
        <div>{head?.actions}</div>
        <div>{content.searchBar}</div>
        <div>{content.body}</div>
      </div>
    ),
  },
}

export const HiddenHeadAndSearchBar: Story = {
  args: {
    controls: {
      head: {
        enabled: false,
      },
      searchBar: {
        enabled: false,
      },
    },
  },
  decorators: [
    (Story) => (
      <FooProvider mode={EntitiesFetchMode.List}>
        <Story />
      </FooProvider>
    ),
  ],
}

export const PreFiltered: Story = {
  args: {},
  decorators: [
    (Story) => (
      <FooProvider
        mode={EntitiesFetchMode.List}
        dataFilter={{
          uid: {
            eq: "test",
          },
        }}
      >
        <Story />
      </FooProvider>
    ),
  ],
}

export const EmptyDefault: Story = {
  args: {},
  decorators: [
    (Story) => (
      <FooProvider
        mode={EntitiesFetchMode.List}
        dataFilter={{
          uid: {
            eq: "nonexistentitem",
          },
        }}
      >
        <Story />
      </FooProvider>
    ),
  ],
}

export const EmptyCustom: Story = {
  args: {
    emptyContent: (
      <div
        style={{
          width: "100%",
          textAlign: "center",
          marginTop: 50,
          marginBottom: 50,
        }}
      >
        Custom empty content
      </div>
    ),
  },
  decorators: [
    (Story) => (
      <FooProvider
        mode={EntitiesFetchMode.List}
        dataFilter={{
          uid: {
            eq: "nonexistentitem",
          },
        }}
      >
        <Story />
      </FooProvider>
    ),
  ],
}
