import React, { Fragment, useEffect } from "react"
import { EntityTableControls } from "../../../types"
import { EntityTableTextFilter } from "../common/filters/EntityTableTextFilter"
import { useEntityController } from "../../../services/context/controller"
import { useEntityManagerDefinition } from "../../../services/context/definition"
import { EntityItemControls } from "../../../services/types/itemControls"
import { useEntityManagerLabel } from "../../../../../modules/labels"
import { useEntityCallbacksContext } from "../../../services/context/callbacks"
import {
  EntityListContainerRenderer,
  EntityListItemRenderer,
  EntityListItemsRenderer,
} from "./types"
import { EntityControllerLayoutRenderer } from "../../../types/layout"

export type EntityTextFilterState = {
  value: string
  onChange: (value: string) => void
}

export interface EntityListProps<TEntity, TFilters> {
  data?: TEntity[]
  toolbar?: React.ReactNode
  bottomContent?: React.ReactNode
  itemControls?: EntityItemControls<TEntity>
  title?: React.ReactNode
  controls?: EntityTableControls
  renderItem?: EntityListItemRenderer<TEntity>
  renderItems?: EntityListItemsRenderer<TEntity>
  renderContainer?: EntityListContainerRenderer
  renderer: EntityControllerLayoutRenderer
  emptyDataSource?: boolean
  firstFetchCompleted?: boolean
}

export function EntityList<TEntity, TFilters>({
  data,
  toolbar,
  bottomContent,
  itemControls,
  title,
  controls,
  renderContainer,
  renderItem,
  renderItems,
  renderer,
  emptyDataSource,
  firstFetchCompleted,
}: EntityListProps<TEntity, TFilters>) {
  const { options, metadata } = useEntityManagerDefinition()
  const {
    actions: { fetchState },
    sorting,
    query,
    updateSorting,
    updateQuery,
    updatePaging,
    updateFilters,
    fetchResults,
    clientFilters,
    filters,
  } = useEntityController<
    TEntity,
    TFilters,
    unknown,
    unknown,
    unknown,
    TEntity
  >()
  const { getLabel } = useEntityManagerLabel()

  const handleQueryChange = (value: string) => {
    if (options?.queryFilterStrategy === "server") {
      updateQuery(value)
      return
    }

    clientFilters.query?.onChange(value)
  }

  const getQuery = () => {
    if (options?.queryFilterStrategy === "server") {
      return query
    }

    return clientFilters.query?.value
  }

  const { setCallbacks } = useEntityCallbacksContext()

  useEffect(() => {
    setCallbacks({})
  }, [])

  const content = (
    <>
      {renderItem &&
        data?.map((item, i) => (
          <Fragment key={i}>
            {renderItem({
              item,
              controls: itemControls?.filter((x) => x.enabled !== false) ?? [],
              index: i,
            })}
          </Fragment>
        ))}
      {renderItems && renderItems({ items: data ?? [] })}
    </>
  )

  return (
    <>
      {renderer({
        head:
          controls?.head?.enabled !== false
            ? {
                title:
                  title ??
                  (metadata?.name
                    ? getLabel("table:title", {
                        placeholders: {
                          entity: metadata.name,
                        },
                      })
                    : undefined),
                actions: toolbar,
              }
            : undefined,
        content: {
          searchBar: firstFetchCompleted &&
            !emptyDataSource &&
            controls?.searchBar?.enabled !== false && (
              <EntityTableTextFilter
                value={getQuery() ?? ""}
                onChange={(value) => handleQueryChange(value)}
              />
            ),
          body: (
            <>
              {firstFetchCompleted && !emptyDataSource && renderContainer ? (
                renderContainer({
                  children: content,
                })
              ) : (
                <>{content}</>
              )}
              {bottomContent}
            </>
          ),
        },
      })}
    </>
  )
}
