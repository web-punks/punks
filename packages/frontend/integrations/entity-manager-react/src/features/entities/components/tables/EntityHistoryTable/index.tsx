import { uiRegistry } from "@punks/ui-components"
import { useEntityViewHistoryAction } from "../../../services/hooks/useEntityViewHistoryAction"
import { useEntityManagerLabel } from "../../../../../modules/labels"
import { useEffect } from "react"
import { useEntityManagerDefinition } from "../../../services/context/definition"

export interface EntityHistoryTableProps<TEntity> {
  item: TEntity | undefined
}

const EntityHistoryTable = <TEntity,>({
  item,
}: EntityHistoryTableProps<TEntity>) => {
  const { getLabel } = useEntityManagerLabel()
  const { invoke, state } = useEntityViewHistoryAction<TEntity>()
  const { modals } = useEntityManagerDefinition<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown
  >()

  useEffect(() => {
    if (item) {
      invoke({
        item,
      })
    }
  }, [item])

  if (!modals.versions) {
    return <></>
  }

  return (
    <>
      {uiRegistry().components.renderTable<TEntity, unknown>({
        columns: modals.versions.table.columns.map((column, index) => ({
          key: column.key,
          title: column.title,
          render: (item) => column.render(item, index),
        })),
        loading: state.loading,
        dataSource: state.data?.items,
        emptyData: {
          content: getLabel("modals:versions:table:labels:empty"),
        },
        labels: {
          filters: {
            textFilter: {
              apply: getLabel("filters:textFilter:apply"),
              search: getLabel("filters:textFilter:search"),
              contains: getLabel("filters:textFilter:contains"),
              isEmpty: getLabel("filters:textFilter:isEmpty"),
              isNotEmpty: getLabel("filters:textFilter:isNotEmpty"),
            },
            dateFilter: {
              apply: getLabel("filters:dateFilter:apply"),
            },
          },
        },
      })}
    </>
  )
}

export default EntityHistoryTable
