import { uiRegistry } from "@punks/ui-components"
import { useMenuItems } from "../../../../menu/useMenuItems"
import { useEntityManagerDefinition } from "../../../../services/context/definition"
import { EntityMainMenuControls } from "../../../../services/types/menuControls"

export interface EntityToolbarMenuProps {
  menuControls?: EntityMainMenuControls<unknown>
}

const EntityToolbarMenu = ({ menuControls }: EntityToolbarMenuProps) => {
  const { operations } = useEntityManagerDefinition()
  const { menuItems } = useMenuItems({
    operations,
    menuControls: menuControls?.secondary?.controls ?? [],
  })

  if (!menuItems?.length) {
    return <></>
  }

  return (
    <div>
      {uiRegistry().components.renderDropButton({
        items: menuItems.map((x) => ({
          label: x.content,
          onClick: () => x.onClick?.(),
        })),
        variant: "text",
        rounded: true,
        iconOnly: true,
        children: uiRegistry().renderIcon("options", {
          width: 20,
        }),
        alignment: "end",
      })}
    </div>
  )
}

export default EntityToolbarMenu
