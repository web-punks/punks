import { FormInput, FormRoot, FormRow, TextInput } from "@punks/ui-components"
import { EntityUpdateFormComponent } from "../../../../../../authentication/abstractions"
import { useFooEditForm } from "../../hooks/useFooEditForm"
import {
  FooListItemDto,
  FooUpdateDto,
} from "../../../../../../../__test__/infrastructure/api/backend"

type AdditionalProps = any

const FooUpdateForm: EntityUpdateFormComponent<
  FooUpdateDto,
  FooListItemDto,
  AdditionalProps
> = ({ onCancel, onSave, saving, item, additionalProps }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useFooEditForm({
    defaultItem: item,
  })

  console.log("additionalProps", additionalProps)

  return (
    <FormRoot
      cta={{
        cancel: {
          label: "Cancel",
          onClick: onCancel,
        },
        confirm: {
          label: "Create",
          onClick: handleSubmit(onSave),
          disabled: saving,
        },
      }}
    >
      <FormRow inline>
        <FormInput label="Name" fill>
          <TextInput
            fullWidth
            placeholder="name"
            error={!!errors?.name}
            {...register("name")}
          />
        </FormInput>
      </FormRow>
      <FormRow inline>
        <FormInput label="Uid" fill>
          <TextInput
            fullWidth
            placeholder="uid"
            error={!!errors?.uid}
            {...register("uid")}
          />
        </FormInput>
      </FormRow>
      <FormRow inline>
        <FormInput label="Age" fill>
          <TextInput
            fullWidth
            placeholder="age"
            error={!!errors?.age}
            {...register("age")}
          />
        </FormInput>
      </FormRow>
      <FormRow inline>
        <FormInput label="Type" fill>
          <TextInput
            fullWidth
            placeholder="type"
            error={!!errors?.age}
            {...register("type")}
          />
        </FormInput>
      </FormRow>
      <FormRow inline>
        <FormInput label="Other" fill>
          <TextInput
            fullWidth
            placeholder="other"
            error={!!errors?.other}
            {...register("other")}
          />
        </FormInput>
      </FormRow>
    </FormRoot>
  )
}

export default FooUpdateForm
