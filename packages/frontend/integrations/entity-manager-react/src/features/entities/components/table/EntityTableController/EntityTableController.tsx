import { EntityTableWrapper } from "./EntityTableWrapper"
import { EntityTableControls, EntityTableLayoutOptions } from "../../../types"
import { EntityItemControls } from "../../../services/types/itemControls"
import { EntityMainMenuControls } from "../../../services/types/menuControls"
import {
  EntityRowActions,
  EntityTableColumnType,
  EntityTableSelection,
} from "../../../services/types/tables"
import { EntityControllerLayoutRenderer } from "../../../types/layout"
import { defaultRenderer } from "../../../ui/default"
import { BoxProps } from "@punks/ui-components"

export interface EntityTableControllerProps<TEntity, TFilters> {
  itemControls?: EntityItemControls<TEntity>
  boxProps?: BoxProps
  menuControls?: EntityMainMenuControls<TEntity>
  searchbarControls?: EntityMainMenuControls<TEntity>
  columns: EntityTableColumnType<TEntity, TFilters, unknown, unknown>[]
  layout?: EntityTableLayoutOptions
  controls?: EntityTableControls
  title?: React.ReactNode
  rowActions?: EntityRowActions<TEntity>
  selection?: EntityTableSelection<TEntity>
  emptyContent?: React.ReactNode
  renderer?: EntityControllerLayoutRenderer
}

export function EntityTableController<TEntity, TFilters>({
  title,
  itemControls,
  searchbarControls,
  boxProps,
  menuControls,
  columns,
  controls,
  layout,
  rowActions,
  selection,
  emptyContent,
  renderer = defaultRenderer,
}: EntityTableControllerProps<TEntity, TFilters>) {
  return (
    <EntityTableWrapper
      title={title}
      columns={columns}
      layout={layout}
      itemControls={itemControls}
      boxProps={boxProps}
      menuControls={menuControls}
      searchbarControls={searchbarControls}
      controls={controls}
      rowActions={rowActions}
      selection={selection}
      renderer={renderer}
      emptyContent={emptyContent}
    />
  )
}
