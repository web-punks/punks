import type { Meta, StoryObj } from "@storybook/react"
import { FooProvider } from "../../table/__test__/components/FooProvider"
import { EntitiesFetchMode } from "../../../services"
import {
  EntitiesImportBlock,
  EntitiesImportBlockProps,
} from "./EntitiesImportBlock"

const FooImportBlock = (props: EntitiesImportBlockProps) => (
  <EntitiesImportBlock {...props} />
)

const meta = {
  title: "Components/EntityImportBlock",
  component: FooImportBlock,
  args: {
    onImportError: () => console.error("onImportError"),
    onImportStarted: () => console.log("onImportStarted"),
    onImportSuccess: () => console.log("onImportSuccess"),
  },
  decorators: [
    (Story) => (
      <div style={{ maxWidth: 1000, margin: "0 auto" }}>
        <Story />
      </div>
    ),
  ],
} satisfies Meta<typeof FooImportBlock>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
  decorators: [
    (Story) => (
      <FooProvider mode={EntitiesFetchMode.List}>
        <Story />
      </FooProvider>
    ),
  ],
}
