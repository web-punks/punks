import React, { useEffect } from "react"
import { EntityTableControls, EntityTableLayoutOptions } from "../../../types"
import { toTableColumns } from "../common/converters/columns"
import EntityTableActionsColumn from "../common/actions/EntityTableActionsColumn"
import { EntityTableTextFilter } from "../common/filters/EntityTableTextFilter"
import { useEntityController } from "../../../services/context/controller"
import { useEntityManagerDefinition } from "../../../services/context/definition"
import { EntityItemControls } from "../../../services/types/itemControls"
import { BoxProps, TableGridType, uiRegistry } from "@punks/ui-components"
import {
  EntityRowActions,
  EntityTableColumnType,
  EntityTableRowControlType,
  EntityTableSelection,
} from "../../../services/types/tables"
import { useEntityManagerLabel } from "../../../../../modules/labels"
import { useEntityCallbacksContext } from "../../../services/context/callbacks"
import { EntityControllerLayoutRenderer } from "../../../types/layout"
import EntityTableFiltersTags from "./EntityTableFiltersTags"

export type EntityTextFilterState = {
  value: string
  onChange: (value: string) => void
}

export interface EntityTableProps<TEntity, TFilters> {
  columns: EntityTableColumnType<TEntity, TFilters, unknown, unknown>[]
  rowControls?: EntityTableRowControlType<TEntity>[]
  data?: TEntity[]
  toolbar?: React.ReactNode
  searchbarToolbar?: React.ReactNode
  bottomContent?: React.ReactNode
  layout?: EntityTableLayoutOptions
  itemControls?: EntityItemControls<TEntity>
  boxProps?: BoxProps
  title?: React.ReactNode
  controls?: EntityTableControls
  rowActions?: EntityRowActions<TEntity>
  selection?: EntityTableSelection<TEntity>
  grid?: TableGridType
  renderer: EntityControllerLayoutRenderer
  emptyDataSource?: boolean
  firstFetchCompleted?: boolean
}

const hasRowActions = <TEntity,>(itemControls?: EntityItemControls<TEntity>) =>
  !!itemControls?.find((x) => x.enabled !== false)

export function EntityTable<TEntity, TFilters>({
  data,
  toolbar,
  searchbarToolbar,
  bottomContent,
  columns,
  itemControls,
  boxProps,
  title,
  controls,
  rowActions,
  selection,
  grid,
  renderer,
  emptyDataSource,
  firstFetchCompleted,
}: EntityTableProps<TEntity, TFilters>) {
  const { options, metadata } = useEntityManagerDefinition()
  const {
    actions: { fetchState },
    sorting,
    query,
    updateSorting,
    updateQuery,
    updatePaging,
    updateFilters,
    fetchResults,
    clientFilters,
    filters,
  } = useEntityController<
    TEntity,
    TFilters,
    unknown,
    unknown,
    unknown,
    TEntity
  >()
  const { getLabel } = useEntityManagerLabel()

  const handleQueryChange = (value: string) => {
    if (options?.queryFilterStrategy === "server") {
      updateQuery(value)
      return
    }

    clientFilters.query?.onChange(value)
  }

  const getQuery = () => {
    if (options?.queryFilterStrategy === "server") {
      return query
    }

    return clientFilters.query?.value
  }

  const tableColumns = toTableColumns<TEntity, TFilters>({
    state: {
      currentFilter: filters,
      facets: fetchResults?.facets,
      onFilterChange: updateFilters,
    },
    columns,
  })

  const { setCallbacks } = useEntityCallbacksContext()

  useEffect(() => {
    setCallbacks({})
  }, [])

  return (
    <>
      {renderer({
        head:
          controls?.head?.enabled !== false
            ? {
                title:
                  title ??
                  (metadata?.name
                    ? getLabel("table:title", {
                        placeholders: {
                          entity: metadata.name,
                        },
                      })
                    : undefined),
                actions: toolbar,
              }
            : undefined,
        content: {
          searchBar: firstFetchCompleted &&
            !emptyDataSource &&
            controls?.searchBar?.enabled !== false && (
              <>
                <EntityTableTextFilter
                  value={getQuery() ?? ""}
                  onChange={(value) => handleQueryChange(value)}
                />
                {controls?.searchBar?.actions !== false && searchbarToolbar}
              </>
            ),
          body: (
            <>
              {!firstFetchCompleted &&
                fetchState.loading &&
                uiRegistry().components.renderLoaderBar({})}

              {firstFetchCompleted && !emptyDataSource && (
                <>
                  <EntityTableFiltersTags columns={tableColumns} />
                  {uiRegistry().components.renderTable({
                    labels: {
                      filters: {
                        textFilter: {
                          apply: getLabel("filters:textFilter:apply"),
                          search: getLabel("filters:textFilter:search"),
                          contains: getLabel("filters:textFilter:contains"),
                          isEmpty: getLabel("filters:textFilter:isEmpty"),
                          isNotEmpty: getLabel("filters:textFilter:isNotEmpty"),
                        },
                        dateFilter: {
                          apply: getLabel("filters:dateFilter:apply"),
                        },
                      },
                    },
                    sorting: {
                      value:
                        sorting?.value && sorting?.direction
                          ? {
                              column: sorting.value,
                              direction: sorting.direction,
                            }
                          : undefined,
                      onChange: (value) =>
                        value
                          ? updateSorting({
                              direction: value.direction,
                              value: value.column,
                            })
                          : undefined,
                    },
                    rowActions: rowActions
                      ? {
                          onSingleClick: rowActions.onClick,
                          onDoubleClick: rowActions.onDoubleClick,
                        }
                      : undefined,
                    selection,
                    grid,
                    paging: fetchResults?.paging
                      ? {
                          mode: "serverSide",
                          index: fetchResults.paging.pageIndex,
                          size: fetchResults.paging.pageSize,
                          pageItems: fetchResults.items?.length ?? 0,
                          totPages: fetchResults.paging.totPages,
                          totItems: fetchResults.paging.totItems,
                          onPagingChange: (pageIndex, pageSize) =>
                            updatePaging({
                              pageIndex,
                              pageSize,
                              cursor: pageIndex,
                            }),
                        }
                      : undefined,
                    loading: fetchState.loading,
                    dataSource: data,
                    columns: [...tableColumns],
                    rowControls:
                      itemControls && hasRowActions(itemControls)
                        ? [
                            {
                              key: "actions",
                              render: (record: TEntity) => (
                                <EntityTableActionsColumn
                                  item={record}
                                  rowControls={itemControls}
                                  {...(boxProps ? { boxProps } : {})}
                                />
                              ),
                            },
                          ]
                        : [],
                  })}
                </>
              )}
              {bottomContent}
            </>
          ),
        },
      })}
    </>
  )
}
