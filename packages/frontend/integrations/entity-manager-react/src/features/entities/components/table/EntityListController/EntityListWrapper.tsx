import { uiRegistry } from "@punks/ui-components"
import { EntityCreateModal } from "../../../modals/EntityCreateModal"
import { EntityList } from "./EntityList"
import { EntityToolbar } from "../common/toolbar/EntityToolbar"
import { EntityUpdateModal } from "../../../modals/EntityUpdateModal"
import EntityDeleteModal from "../../../modals/EntityDeleteModal"
import { EntityDetailsModal } from "../../../modals/EntityDetailsModal"
import { useEntityManagerDefinition } from "../../../services/context/definition"
import { useEntityController } from "../../../services/context/controller"
import { EntityImportModal } from "../../../modals/EntityImportModal"
import { EntityViewHistoryModal } from "../../../modals/EntityViewHistoryModal"
import { useEntityManagerLabel } from "../../../../../modules/labels"
import { EntityItemControls } from "../../../services/types"
import { EntityMainMenuControls } from "../../../services/types/menuControls"
import { EntityViewJsonModal } from "../../../modals/EntityViewJsonModal"
import {
  EntityListContainerRenderer,
  EntityListControls,
  EntityListItemRenderer,
  EntityListItemsRenderer,
} from "./types"
import { EntityControllerLayoutRenderer } from "../../../types/layout"
import { defaultRenderer } from "../../../ui/default"

export interface EntityListWrapperProps<TEntity, TFilters> {
  itemControls?: EntityItemControls<TEntity>
  menuControls?: EntityMainMenuControls<TEntity>
  controls?: EntityListControls
  title?: React.ReactNode
  emptyContent?: React.ReactNode
  renderItem?: EntityListItemRenderer<TEntity>
  renderItems?: EntityListItemsRenderer<TEntity>
  renderContainer?: EntityListContainerRenderer
  renderer?: EntityControllerLayoutRenderer
}

export function EntityListWrapper<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  itemControls,
  menuControls,
  title,
  controls,
  emptyContent,
  renderItem,
  renderItems,
  renderContainer,
  renderer = defaultRenderer,
}: EntityListWrapperProps<TEntity, TFilters>) {
  const { selectors } = useEntityManagerDefinition<
    TEntity,
    TFilters,
    TCreate,
    TUpdate,
    TSorting
  >()
  const {
    actions: {
      openItemState,
      closeDetailsModal,
      closeViewHistoryModal,
      closeItemJsonModal,
      itemToDelete,
      editItemState,
      closeCreateModal,
      closeEditModal,
      clearItemToDelete,
      closeImportModal,
      fetchState,
      sampleDownload,
      itemsImport,
      importState,
    },
    filteredData,
    empty,
    emptyDataSource,
    firstFetchCompleted,
    ui,
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    TEntity
  >()

  const handleImport = async (file: File) => {
    await itemsImport({
      file,
    })
    closeImportModal()
  }

  const { getLabel } = useEntityManagerLabel()

  return (
    <>
      <EntityList
        title={title}
        data={filteredData}
        toolbar={<EntityToolbar controls={menuControls} />}
        controls={controls}
        itemControls={itemControls}
        renderItem={renderItem}
        renderItems={renderItems}
        renderContainer={renderContainer}
        renderer={renderer}
        emptyDataSource={emptyDataSource}
        firstFetchCompleted={firstFetchCompleted}
        bottomContent={
          <>
            {fetchState.error &&
              uiRegistry().components.renderAlert({
                type: "error",
                children: getLabel("operations:fetch:messages:genericError"),
              })}

            {empty &&
              (emptyContent ??
                uiRegistry().components.renderAlert({
                  type: "info",
                  children: getLabel("table:messages:empty"),
                }))}

            {!empty &&
              filteredData?.length === 0 &&
              uiRegistry().components.renderAlert({
                type: "info",
                children: getLabel("table:messages:emptyFilter"),
              })}
          </>
        }
      />

      <EntityDetailsModal
        open={!!openItemState?.data}
        item={openItemState?.data}
        onClose={(options) => closeDetailsModal(options)}
      />
      <EntityUpdateModal
        open={!!editItemState?.data}
        item={
          editItemState?.data
            ? {
                data: editItemState.data,
                id: selectors.getId(editItemState.data),
              }
            : undefined
        }
        onClose={() => closeEditModal()}
        onCancel={(options) => closeEditModal(options)}
      />
      <EntityDeleteModal
        open={!!itemToDelete}
        item={itemToDelete}
        onClose={() => clearItemToDelete()}
      />
      <EntityCreateModal
        open={ui.createModal.open}
        onClose={() => closeCreateModal()}
        onCancel={(options) => closeCreateModal(options)}
      />
      <EntityImportModal
        open={ui.uploadModal.open}
        onSubmit={(file) => handleImport(file)}
        onSampleDownload={() => sampleDownload()}
        onCancel={(options) => closeImportModal(options)}
        importLoading={importState.loading}
      />
      <EntityViewHistoryModal
        open={ui.viewHistoryModal.open}
        item={ui.viewHistoryModal.item}
        onClose={(options) => closeViewHistoryModal(options)}
      />
      <EntityViewJsonModal
        open={ui.viewJsonModal.open}
        item={ui.viewJsonModal.item}
        onClose={() => closeItemJsonModal()}
      />
    </>
  )
}
