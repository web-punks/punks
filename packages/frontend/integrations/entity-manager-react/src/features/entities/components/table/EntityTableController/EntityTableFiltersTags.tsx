import {
  TableColumnFilterType,
  TableColumnsType,
  TableColumnType,
  TableDateFilter,
  TableMultiSelectFilter,
  TableSingleSelectFilter,
  TableTextFilter,
  TagBarItem,
  uiRegistry,
} from "@punks/ui-components"
import { useEntityManagerLabel } from "../../../../../modules/labels/hooks"

type ColumnFilterLabels = {
  filters: {
    textFilter: {
      contains: string
      isEmpty: string
      isNotEmpty: string
    }
  }
}

export interface EntityTableFiltersTagsProps<TEntity> {
  columns: TableColumnsType<TEntity, unknown, unknown>
}

const getTextFilterColumnTagItems = <TEntity,>(
  column: TableColumnType<TEntity, unknown, unknown>,
  filter: TableTextFilter<TEntity>,
  labels: ColumnFilterLabels["filters"]["textFilter"]
): TagBarItem[] => {
  return filter.value
    ? [
        ...(filter.value.type === "contains"
          ? [
              {
                label: `${column.title}: ${filter.value.value}`,
                onRemove: () => filter.onClear(),
              },
            ]
          : []),
        ...(filter.value.type === "isEmpty"
          ? [
              {
                label: `${column.title}: ${labels.isEmpty}`,
                onRemove: () => filter.onClear(),
              },
            ]
          : []),
        ...(filter.value.type === "isNotEmpty"
          ? [
              {
                label: `${column.title}: ${labels.isNotEmpty}`,
                onRemove: () => filter.onClear(),
              },
            ]
          : []),
      ]
    : []
}

const getSingleSelectFilterColumnTagItems = <TEntity,>(
  column: TableColumnType<TEntity, unknown, unknown>,
  filter: TableSingleSelectFilter<TEntity>
): TagBarItem[] => {
  const label = filter.options?.find(
    (x) => JSON.stringify(x.value) === JSON.stringify(filter.selectedValue)
  )?.label
  return filter.selectedValue
    ? [
        {
          label: label ? `${column.title}: ${label}` : undefined,
          onRemove: () => filter.onChange(undefined),
        },
      ]
    : []
}

const getMultiSelectFilterColumnTagItems = <TEntity,>(
  column: TableColumnType<TEntity, unknown, unknown>,
  filter: TableMultiSelectFilter<TEntity>
): TagBarItem[] => {
  return filter.selectedValues?.length
    ? filter.selectedValues
        .filter(
          (value) => !!filter.options?.find((x) => x.value === value)?.label
        )
        .map((value) => ({
          label: `${column.title}: ${
            filter.options?.find((x) => x.value === value)?.label
          }`,
          onRemove: () =>
            filter.onChange(
              filter.selectedValues?.filter((x) => x !== value) ?? []
            ),
        }))
    : []
}

const getDateFilterColumnTagItems = <TEntity,>(
  column: TableColumnType<TEntity, unknown, unknown>,
  filter: TableDateFilter<TEntity>
): TagBarItem[] => {
  return filter.value
    ? [
        {
          label: `${column.title} ${filter.value.operator} ${filter.value.date}`,
          onRemove: () => filter.onClear(),
        },
      ]
    : []
}

const getColumnTagItems = <TEntity,>(
  column: TableColumnType<TEntity, unknown, unknown>,
  labels: ColumnFilterLabels
): TagBarItem[] => {
  switch (column.filter?.type) {
    case TableColumnFilterType.Text:
      return getTextFilterColumnTagItems(
        column,
        column.filter as TableTextFilter<TEntity>,
        labels.filters.textFilter
      )
    case TableColumnFilterType.SingleSelect:
      return getSingleSelectFilterColumnTagItems(
        column,
        column.filter as TableSingleSelectFilter<TEntity>
      )
    case TableColumnFilterType.MultipleSelect:
      return getMultiSelectFilterColumnTagItems(
        column,
        column.filter as TableMultiSelectFilter<TEntity>
      )
    case TableColumnFilterType.Date:
      return getDateFilterColumnTagItems(
        column,
        column.filter as TableDateFilter<TEntity>
      )
    default:
      return []
  }
}

const getColumnsTagItems = <TEntity,>(
  columns: TableColumnsType<TEntity, unknown, unknown>,
  labels: ColumnFilterLabels
): TagBarItem[] => {
  return columns
    .map((column) => getColumnTagItems(column, labels))
    .flat()
    .filter((x) => !!x.label)
}

const EntityTableFiltersTags = <TEntity,>({
  columns,
}: EntityTableFiltersTagsProps<TEntity>) => {
  const { getLabel } = useEntityManagerLabel()

  // todo: memoize options in order to avoid filter tags blinking
  const items = getColumnsTagItems(columns, {
    filters: {
      textFilter: {
        contains: getLabel("filters:textFilter:contains"),
        isEmpty: getLabel("filters:textFilter:isEmpty"),
        isNotEmpty: getLabel("filters:textFilter:isNotEmpty"),
      },
    },
  })

  return (
    <>
      {items.length
        ? uiRegistry().components.renderBox({
            mb: 4,
            children: uiRegistry().components.renderTagsBar({
              items,
              color: "primary",
            }),
          })
        : undefined}
    </>
  )
}

export default EntityTableFiltersTags
