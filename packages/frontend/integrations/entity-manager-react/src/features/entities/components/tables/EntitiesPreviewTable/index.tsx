import { uiRegistry } from "@punks/ui-components"
import {
  EntitiesParseResult,
  EntityParseValidationError,
} from "../../../services/types/actions"
import { useEntityManagerDefinition } from "../../../services/context/definition"
import { useEntityManagerLabel } from "../../../../../modules/labels/hooks/useEntityManagerLabel"
import { EntitiesImportModal } from "../../../services/types/modals"

export interface EntitiesPreviewTableProps {
  data: EntitiesParseResult
  onSubmit: () => void
  submitInProgress: boolean
  onBack: () => void
}

const getErrorLabel = (
  error: EntityParseValidationError,
  modal: EntitiesImportModal
) => modal.table.labels?.errorCodes[error.errorCode] ?? error.errorCode

const EntitiesPreviewTable = ({
  data,
  onBack,
  onSubmit,
  submitInProgress,
}: EntitiesPreviewTableProps) => {
  const { getLabel } = useEntityManagerLabel()
  const { modals } = useEntityManagerDefinition<
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()

  if (!modals.import) {
    console.error("modals.import is not defined")
    return <></>
  }

  const hasErrors = data.entries.some((x) => !x.status.isValid)
  const isEmpty = data.entries.length === 0

  return (
    <>
      {uiRegistry().components.renderTable<unknown, unknown>({
        columns: [
          {
            key: "rowNumber",
            title: getLabel("modals:import:preview:columns:rowNumber"),
            render: (item: any) => {
              return item.rowIndex + 1
            },
          },
          ...modals.import.table.columns.map((column, index) => ({
            key: column.key,
            title: column.title,
            render: (item: any) => column.render(item, index),
          })),
          {
            key: "status",
            title: getLabel("modals:import:preview:columns:status"),
            render: (item: any) => {
              return item.status.isValid
                ? getLabel("modals:import:preview:labels:valid")
                : getLabel("modals:import:preview:labels:invalid")
            },
          },
          {
            key: "validationErrors",
            title: getLabel("modals:import:preview:columns:validationErrors"),
            render: (item: any) => {
              return item.status.validationErrors
                .map(
                  (x: EntityParseValidationError) =>
                    `${x.column.key}: ${getErrorLabel(x, modals.import!)}`
                )
                .join(", ")
            },
          },
        ],
        paging: {
          mode: "clientSide",
          size: modals.import.table.options?.pageSize ?? 50,
        },
        dataSource: data.entries,
        emptyData: {
          content: getLabel("modals:import:preview:labels:empty"),
        },
        labels: {
          filters: {
            textFilter: {
              apply: getLabel("filters:textFilter:apply"),
              search: getLabel("filters:textFilter:search"),
              contains: getLabel("filters:textFilter:contains"),
              isEmpty: getLabel("filters:textFilter:isEmpty"),
              isNotEmpty: getLabel("filters:textFilter:isNotEmpty"),
            },
            dateFilter: {
              apply: getLabel("filters:dateFilter:apply"),
            },
          },
        },
      })}
      {isEmpty &&
        uiRegistry().components.renderBox({
          my: 4,
          children: uiRegistry().components.renderAlert({
            type: "warning",
            children: getLabel("modals:import:preview:messages:emptyFile"),
          }),
        })}
      {hasErrors &&
        uiRegistry().components.renderBox({
          my: 4,
          children: uiRegistry().components.renderAlert({
            type: modals.import.table.options?.skipInvalidRows
              ? "warning"
              : "error",
            children: [
              getLabel("modals:import:preview:messages:fileWithErrors"),
              modals.import.table.options?.skipInvalidRows &&
                getLabel("modals:import:preview:messages:willFilterErrors"),
            ]
              .filter(Boolean)
              .join(". "),
          }),
        })}
      {uiRegistry().components.renderCancelConfirmCta({
        align: "center",
        confirm: {
          label: getLabel("modals:import:preview:cta:submit"),
          onClick: () => onSubmit(),
          loading: submitInProgress,
          disabled:
            (hasErrors && !modals.import.table.options?.skipInvalidRows) ||
            isEmpty,
        },
        cancel: {
          label: getLabel("modals:import:preview:cta:back"),
          onClick: () => onBack(),
        },
      })}
    </>
  )
}

export default EntitiesPreviewTable
