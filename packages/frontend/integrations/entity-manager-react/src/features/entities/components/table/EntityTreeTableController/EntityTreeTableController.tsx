import { EntityTreeTableWrapper } from "./EntityTreeTableWrapper"
import { EntityTableControls, EntityTableLayoutOptions } from "../../../types"
import { EntityItemControls } from "../../../services/types/itemControls"
import { EntityMainMenuControls } from "../../../services/types/menuControls"
import {
  EntityRowActions,
  EntityTableColumnType,
  EntityTableSelection,
} from "../../../services/types/tables"
import { TreeNode } from "@punks/ui-components"
import { EntityControllerLayoutRenderer } from "../../../types/layout"
import { defaultRenderer } from "../../../ui/default"

export interface EntityTreeTableControllerProps<TEntity, TFilters> {
  itemControls?: EntityItemControls<TEntity>
  menuControls?: EntityMainMenuControls<TEntity>
  columns: EntityTableColumnType<TEntity, TFilters, unknown, unknown>[]
  layout?: EntityTableLayoutOptions
  controls?: EntityTableControls
  title?: React.ReactNode
  rowActions?: EntityRowActions<TreeNode<TEntity>>
  selection?: EntityTableSelection<TreeNode<TEntity>>
  renderer?: EntityControllerLayoutRenderer
  emptyContent?: React.ReactNode
}

export function EntityTreeTableController<TEntity, TFilters>({
  title,
  itemControls,
  menuControls,
  columns,
  controls,
  layout,
  rowActions,
  selection,
  emptyContent,
  renderer = defaultRenderer,
}: EntityTreeTableControllerProps<TEntity, TFilters>) {
  return (
    <EntityTreeTableWrapper
      title={title}
      columns={columns}
      layout={layout}
      itemControls={itemControls}
      menuControls={menuControls}
      controls={controls}
      rowActions={rowActions}
      selection={selection}
      emptyContent={emptyContent}
      renderer={renderer}
    />
  )
}
