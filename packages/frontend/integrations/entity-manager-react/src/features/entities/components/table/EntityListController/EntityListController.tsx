import { EntityListWrapper } from "./EntityListWrapper"
import { EntityItemControls } from "../../../services/types/itemControls"
import { EntityMainMenuControls } from "../../../services/types/menuControls"
import {
  EntityListContainerRenderer,
  EntityListControls,
  EntityListItemRenderer,
  EntityListItemsRenderer,
} from "./types"
import { EntityControllerLayoutRenderer } from "../../../types/layout"

export interface EntityListControllerProps<TEntity, TFilters> {
  itemControls?: EntityItemControls<TEntity>
  menuControls?: EntityMainMenuControls<TEntity>
  controls?: EntityListControls
  title?: React.ReactNode
  emptyContent?: React.ReactNode
  renderItem?: EntityListItemRenderer<TEntity>
  renderItems?: EntityListItemsRenderer<TEntity>
  renderContainer?: EntityListContainerRenderer
  renderer?: EntityControllerLayoutRenderer
}

export function EntityListController<TEntity, TFilters>({
  title,
  itemControls,
  menuControls,
  controls,
  emptyContent,
  renderContainer,
  renderItem,
  renderItems,
  renderer,
}: EntityListControllerProps<TEntity, TFilters>) {
  return (
    <EntityListWrapper
      title={title}
      itemControls={itemControls}
      menuControls={menuControls}
      controls={controls}
      renderContainer={renderContainer}
      renderItem={renderItem}
      renderItems={renderItems}
      renderer={renderer}
      emptyContent={emptyContent}
    />
  )
}
