import {
  TableFilterDateOperator,
  TableFilterDateValue,
  TableTextFilterValue,
} from "@punks/ui-components"

type DateFilter = {
  in?: Array<string>
  eq?: string
  gt?: string
  gte?: string
  lt?: string
  lte?: string
  isNull?: boolean
}

export const toDateFilter = (
  value?: TableFilterDateValue
): DateFilter | undefined => {
  if (!value?.date) {
    return undefined
  }
  return {
    ...(value.operator === "=" && { eq: value.date }),
    ...(value.operator === ">" && { gt: value.date }),
    ...(value.operator === "<" && { lt: value.date }),
    ...(value.operator === ">=" && { gte: value.date }),
    ...(value.operator === "<=" && { lte: value.date }),
  }
}

const operatorMap = {
  eq: "=",
  gt: ">",
  lt: "<",
  gte: ">=",
  lte: "<=",
}

export const getDateFilterValue = (
  value?: DateFilter
): TableFilterDateValue | undefined => {
  if (!value) {
    return undefined
  }
  for (const op of Object.keys(value)) {
    if (value[op as keyof DateFilter]) {
      return {
        operator: (operatorMap as any)[op] as TableFilterDateOperator,
        date: value[op as keyof DateFilter] as string,
      }
    }
  }
}

export type StringFilter = {
  like?: string
  isEmpty?: boolean
}

export const toStringFilter = (
  value?: TableTextFilterValue
): StringFilter | undefined => {
  if (!value) {
    return undefined
  }
  if (value.type === "contains") {
    return {
      like: value.value,
    }
  }
  if (value.type === "isEmpty") {
    return {
      isEmpty: true,
    }
  }
  if (value.type === "isNotEmpty") {
    return {
      isEmpty: false,
    }
  }
}

export const getStringFilterValue = (
  value?: StringFilter
): TableTextFilterValue | undefined => {
  if (!value) {
    return undefined
  }
  if (value.like) {
    return {
      type: "contains",
      value: value.like,
    }
  }

  if (value.isEmpty) {
    return {
      type: "isEmpty",
    }
  }
  if (value.isEmpty === false) {
    return {
      type: "isNotEmpty",
    }
  }
  return undefined
}
