import { EntityItemControls } from "../../../services"

export type EntityListControls = {
  head?: {
    enabled?: boolean
  }
  searchBar?: {
    enabled?: boolean
  }
}

export type EntityListItemRenderer<TEntity> = (input: {
  item: TEntity
  controls: EntityItemControls<TEntity>
  index: number
}) => React.ReactNode

export type EntityListItemsRenderer<TEntity> = (input: {
  items: TEntity[]
}) => React.ReactNode

export type EntityListContainerRenderer = (input: {
  children: React.ReactNode
}) => React.ReactNode
