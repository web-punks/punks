import React, { useEffect } from "react"
import { EntityTableControls, EntityTableLayoutOptions } from "../../../types"
import { toTableColumns } from "../common/converters/columns"
import { useEntityController } from "../../../services/context/controller"
import { useEntityManagerDefinition } from "../../../services/context/definition"
import { EntityItemControls } from "../../../services/types/itemControls"
import {
  TableGridType,
  TreeNode,
  TreeTableColumnRenderContext,
  uiRegistry,
} from "@punks/ui-components"
import {
  EntityRowActions,
  EntityTableColumnType,
  EntityTableSelection,
} from "../../../services/types/tables"
import { useEntityManagerLabel } from "../../../../../modules/labels"
import { EntityTableTextFilter } from "../common/filters/EntityTableTextFilter"
import { useEntityCallbacksContext } from "../../../services/context/callbacks"
import EntityTableActionsColumn from "../common/actions/EntityTableActionsColumn"
import { EntityControllerLayoutRenderer } from "../../../types/layout"

export type EntityTextFilterState = {
  value: string
  onChange: (value: string) => void
}

export interface EntityTreeTableProps<TEntity, TFilters> {
  columns: EntityTableColumnType<TEntity, TFilters, unknown, unknown>[]
  data?: TreeNode<TEntity>[]
  toolbar?: React.ReactNode
  bottomContent?: React.ReactNode
  layout?: EntityTableLayoutOptions
  itemControls?: EntityItemControls<TEntity>
  title?: React.ReactNode
  controls?: EntityTableControls
  rowActions?: EntityRowActions<TreeNode<TEntity>>
  selection?: EntityTableSelection<TreeNode<TEntity>>
  grid?: TableGridType
  renderer: EntityControllerLayoutRenderer
  emptyDataSource?: boolean
  firstFetchCompleted?: boolean
}

const hasRowActions = <TEntity,>(itemControls?: EntityItemControls<TEntity>) =>
  !!itemControls?.find((x) => x.enabled !== false)

export function EntityTreeTable<TEntity, TFilters>({
  data,
  toolbar,
  bottomContent,
  columns,
  layout,
  itemControls,
  title,
  controls,
  rowActions,
  selection,
  grid,
  renderer,
  emptyDataSource,
  firstFetchCompleted,
}: EntityTreeTableProps<TEntity, TFilters>) {
  const { options, metadata } = useEntityManagerDefinition()
  const {
    actions: { fetchState },
    sorting,
    query,
    updateSorting,
    updateQuery,
    updatePaging,
    updateFilters,
    fetchResults,
    clientFilters,
    filters,
  } = useEntityController<
    TEntity,
    TFilters,
    unknown,
    unknown,
    unknown,
    TreeNode<TEntity>
  >()
  const { getLabel } = useEntityManagerLabel()
  const {
    actions: { fetchEntities },
  } = useEntityController()

  const handleQueryChange = (value: string) => {
    if (options?.queryFilterStrategy === "server") {
      updateQuery(value)
      return
    }

    clientFilters.query?.onChange(value)
  }

  const getQuery = () => {
    if (options?.queryFilterStrategy === "server") {
      return query
    }

    return clientFilters.query?.value
  }

  const tableColumns = toTableColumns<TEntity, TFilters>({
    state: {
      currentFilter: filters,
      facets: fetchResults?.facets,
      onFilterChange: updateFilters,
    },
    columns,
  })

  const { setCallbacks } = useEntityCallbacksContext()

  useEffect(() => {
    setCallbacks({})
  }, [])

  return (
    <>
      {renderer({
        head:
          controls?.head?.enabled !== false
            ? {
                title:
                  title ??
                  (metadata?.name
                    ? getLabel("table:title", {
                        placeholders: {
                          entity: metadata.name,
                        },
                      })
                    : undefined),
                actions: toolbar,
              }
            : undefined,
        content: {
          searchBar: firstFetchCompleted &&
            !emptyDataSource &&
            controls?.searchBar?.enabled !== false && (
              <EntityTableTextFilter
                value={getQuery() ?? ""}
                onChange={(value) => handleQueryChange(value)}
              />
            ),
          body: (
            <>
              {firstFetchCompleted &&
                !emptyDataSource &&
                uiRegistry().components.renderTreeTable({
                  labels: {
                    filters: {
                      textFilter: {
                        apply: getLabel("filters:textFilter:apply"),
                        search: getLabel("filters:textFilter:search"),
                        contains: getLabel("filters:textFilter:contains"),
                        isEmpty: getLabel("filters:textFilter:isEmpty"),
                        isNotEmpty: getLabel("filters:textFilter:isNotEmpty"),
                      },
                      dateFilter: {
                        apply: getLabel("filters:dateFilter:apply"),
                      },
                    },
                  },
                  // sorting: {
                  //   value:
                  //     sorting?.value && sorting?.direction
                  //       ? {
                  //           column: sorting.value,
                  //           direction: sorting.direction,
                  //         }
                  //       : undefined,
                  //   onChange: (value) =>
                  //     value
                  //       ? updateSorting({
                  //           direction: value.direction,
                  //           value: value.column,
                  //         })
                  //       : undefined,
                  // },
                  paging: fetchResults?.paging
                    ? {
                        mode: "serverSide",
                        index: fetchResults.paging.pageIndex,
                        size: fetchResults.paging.pageSize,
                        pageItems: fetchResults.items?.length ?? 0,
                        totPages: fetchResults.paging.totPages,
                        totItems: fetchResults.paging.totItems,
                        onPagingChange: (pageIndex, pageSize) =>
                          updatePaging({
                            pageIndex,
                            pageSize,
                            cursor: pageIndex,
                          }),
                      }
                    : undefined,
                  loading: fetchState.loading,
                  dataSource: data,
                  grid,
                  mainColumn: {
                    ...(tableColumns[0] ?? {}),
                    render: (
                      record: TEntity,
                      context: TreeTableColumnRenderContext
                    ) => tableColumns[0]?.render(record, context.level),
                  },
                  events: {
                    onCollapse: (node: TreeNode<TEntity>) =>
                      fetchEntities({
                        traverse: {
                          collapseNodeId: node.id,
                        },
                      }),
                    onExpand: (node: TreeNode<TEntity>) =>
                      node.childrenCount > 0
                        ? fetchEntities({
                            traverse: {
                              parentId: node.id,
                            },
                          })
                        : () => {},
                  },
                  rowActions: rowActions
                    ? {
                        onSingleClick: (item) =>
                          rowActions.onClick?.(item.node),
                        onDoubleClick: (item) =>
                          rowActions.onDoubleClick?.(item.node),
                      }
                    : undefined,
                  selection: selection
                    ? {
                        mode: selection.mode,
                        onSelectionChange: (selected) =>
                          selection.onSelectionChange(
                            selected.map((x) => x.node)
                          ),
                      }
                    : undefined,
                  columns: [
                    ...tableColumns.slice(1).map((x) => ({
                      ...x,
                      render: (
                        record: TEntity,
                        context: TreeTableColumnRenderContext
                      ) => x.render(record, context.level),
                    })),
                  ],
                  rowControls:
                    itemControls && hasRowActions(itemControls)
                      ? [
                          {
                            key: "actions",
                            render: (record: TEntity) => (
                              <EntityTableActionsColumn
                                item={record}
                                rowControls={itemControls}
                              />
                            ),
                          },
                        ]
                      : [],
                })}
              {bottomContent}
            </>
          ),
        },
      })}
    </>
  )
}
