import { uiRegistry } from "@punks/ui-components"
import EntityToolbarDefaultAction from "./EntityToolbarDefaultAction"
import EntityToolbarMenu from "./EntityToolbarMenu"
import { EntityMainMenuControls } from "../../../../services/types/menuControls"

export interface EntityToolbarProps {
  controls?: EntityMainMenuControls<any>
}

export function EntityToolbar({ controls }: EntityToolbarProps) {
  return uiRegistry().components.renderInlineBar({
    children: (
      <>
        <EntityToolbarDefaultAction menuControls={controls} />
        <EntityToolbarMenu menuControls={controls} />
      </>
    ),
  })
}
