import type { Meta, StoryObj } from "@storybook/react"
import {
  EntityTreeTableController,
  EntityTreeTableControllerProps,
} from "./EntityTreeTableController"
import {
  FooListItemDto,
  FooSearchFilters,
} from "../../../../../__test__/infrastructure/api/backend"
import { FooProvider } from "../__test__/components/FooProvider"
import {
  EntitiesFetchMode,
  ItemControlType,
  MenuControlType,
} from "../../../services"

const FooTreeTableController = (
  props: EntityTreeTableControllerProps<FooListItemDto, FooSearchFilters>
) => <EntityTreeTableController {...props} />

const meta = {
  title: "Components/EntityTreeTableController",
  component: FooTreeTableController,
  args: {
    columns: [
      {
        key: "name",
        render: (x) => x.name,
        title: "Name",
      },
      {
        key: "id",
        render: (x) => x.id,
        title: "Id",
      },
      {
        key: "id2",
        render: (x) => x.id,
        title: "Id2",
      },
      {
        key: "uid",
        render: (x) => x.uid,
        title: "Uid",
      },
      {
        key: "type",
        render: (x) => x.type,
        title: "Type",
      },
      {
        key: "age",
        render: (x) => x.age,
        title: "Age",
      },
    ],
    menuControls: {
      primary: {
        styles: {
          width: 140,
        },
        controls: [
          {
            type: MenuControlType.Create,
          },
          {
            type: MenuControlType.Import,
          },
        ],
      },
      secondary: {
        controls: [
          {
            type: MenuControlType.Export,
          },
        ],
      },
    },
    itemControls: [
      {
        type: ItemControlType.Edit,
      },
      {
        type: ItemControlType.ViewJson,
      },
      {
        type: ItemControlType.Menu,
        items: [
          {
            type: ItemControlType.CreateChild,
          },
          {
            type: ItemControlType.ViewJson,
          },
          {
            type: ItemControlType.ViewHistory,
          },
          {
            type: ItemControlType.Custom,
            action: async () => {},
            cta: {
              label: "Custom",
            },
          },
          {
            type: ItemControlType.Delete,
          },
        ],
      },
    ],
  },
  decorators: [
    (Story) => (
      <div style={{ maxWidth: 1000, margin: "0 auto" }}>
        <Story />
      </div>
    ),
  ],
} satisfies Meta<typeof FooTreeTableController>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
  decorators: [
    (Story) => (
      <FooProvider mode={EntitiesFetchMode.Tree}>
        <Story />
      </FooProvider>
    ),
  ],
}

export const CustomLayout: Story = {
  decorators: [
    (Story) => (
      <FooProvider mode={EntitiesFetchMode.Tree}>
        <Story />
      </FooProvider>
    ),
  ],
  args: {
    renderer: ({ content, head }) => (
      <div>
        <div>{head?.title}</div>
        <div>{head?.actions}</div>
        <div>{content.searchBar}</div>
        <div>{content.body}</div>
      </div>
    ),
  },
}

export const HiddenHeadAndSearchBar: Story = {
  args: {
    controls: {
      head: {
        enabled: false,
      },
      searchBar: {
        enabled: false,
      },
    },
  },
  decorators: [
    (Story) => (
      <FooProvider mode={EntitiesFetchMode.Tree}>
        <Story />
      </FooProvider>
    ),
  ],
}

export const PreFiltered: Story = {
  args: {},
  decorators: [
    (Story) => (
      <FooProvider
        mode={EntitiesFetchMode.Tree}
        dataFilter={{
          uid: {
            eq: "test",
          },
        }}
      >
        <Story />
      </FooProvider>
    ),
  ],
}

export const EmptyDefault: Story = {
  args: {},
  decorators: [
    (Story) => (
      <FooProvider
        mode={EntitiesFetchMode.Tree}
        dataFilter={{
          uid: {
            eq: "nonexistentitem",
          },
        }}
      >
        <Story />
      </FooProvider>
    ),
  ],
}

export const EmptyCustom: Story = {
  args: {
    emptyContent: (
      <div
        style={{
          width: "100%",
          textAlign: "center",
          marginTop: 50,
          marginBottom: 50,
        }}
      >
        Custom empty content
      </div>
    ),
  },
  decorators: [
    (Story) => (
      <FooProvider
        mode={EntitiesFetchMode.Tree}
        dataFilter={{
          uid: {
            eq: "nonexistentitem",
          },
        }}
      >
        <Story />
      </FooProvider>
    ),
  ],
}
