import { uiRegistry } from "@punks/ui-components"
import { useEntityManagerLabel } from "../../../../../../modules/labels"

export interface EntityTableTextFilterProps {
  value: string
  onChange: (value: string) => void
}

export const EntityTableTextFilter = ({
  onChange,
  value,
}: EntityTableTextFilterProps) => {
  const { getLabel } = useEntityManagerLabel()
  return uiRegistry().components.renderFormInput({
    label: getLabel("table:filters:textFilter:label"),
    children: uiRegistry().components.renderSearchBar({
      value,
      placeholder: getLabel("table:filters:textFilter:placeholder"),
      onChange: (e) => onChange(e.target.value),
      fullWidth: true,
      variant: "outlined",
    }),
  })
}
