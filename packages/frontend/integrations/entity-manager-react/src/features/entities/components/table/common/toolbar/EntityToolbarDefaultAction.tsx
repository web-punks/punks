import { uiRegistry } from "@punks/ui-components"
import { useEntityManagerDefinition } from "../../../../services/context/definition"
import { useMenuItems } from "../../../../menu/useMenuItems"
import { useEntityManagerLabel } from "../../../../../../modules/labels"
import { EntityMainMenuControls } from "../../../../services/types/menuControls"

export interface EntityToolbarDefaultActionProps {
  menuControls?: EntityMainMenuControls<unknown>
}

const EntityToolbarDefaultAction = ({
  menuControls,
}: EntityToolbarDefaultActionProps) => {
  const { getLabel } = useEntityManagerLabel()
  const { operations } = useEntityManagerDefinition()
  const defaultActions = menuControls?.primary?.controls?.filter(
    (x) => x.enabled !== false
  )

  const { menuItems } = useMenuItems({
    operations,
    menuControls: defaultActions ?? [],
  })

  if (!menuItems?.length) {
    return <></>
  }

  if (menuItems.length === 1) {
    const defaultAction = menuItems[0]
    return (
      <>
        {uiRegistry().components.renderButton({
          children: menuControls?.primary?.cta ?? defaultAction.content,
          color: "primary",
          variant: "filled",
          onClick: defaultAction.onClick,
          width: menuControls?.primary?.styles?.width,
        })}
      </>
    )
  }

  return (
    <>
      {uiRegistry().components.renderDropButton({
        children:
          menuControls?.primary?.cta ?? getLabel("menuControls:primary:label"),
        color: "primary",
        variant: "filled",
        items: menuItems.map((x) => ({
          label: x.content,
          onClick: () => x.onClick?.(),
        })),
        width: menuControls?.primary?.styles?.width,
        alignment: "end",
      })}
    </>
  )
}

export default EntityToolbarDefaultAction
