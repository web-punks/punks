import { uiRegistry } from "@punks/ui-components"
import {
  CustomEntityItemControl,
  EntityItemControl,
  EntityItemControls,
} from "../../../../services"
import { useEntityManagerLabel } from "../../../../../../modules"
import { useEntityItemControlAction } from "../../../../hooks/useEntityItemControlAction"
import {
  isControlEnabled,
  isCustomRowAction,
  isStandardRowAction,
} from "../../../../utils/rowActions"

export interface EntityTableEntityContextMenuProps<TEntity> {
  item: TEntity
  items: EntityItemControls<TEntity>
}

export const EntityTableEntityContextMenu = <TEntity,>({
  item,
  items,
}: EntityTableEntityContextMenuProps<TEntity>) => {
  const { action } = useEntityItemControlAction<TEntity>(item)

  const { getLabel } = useEntityManagerLabel()

  const toDropDownItem = (menuItem: EntityItemControl<TEntity>) => {
    if (isStandardRowAction(menuItem.type)) {
      return {
        content: getLabel(`table:itemControls:${menuItem.type}:label`),
        onClick: () => action(menuItem.type),
      }
    }

    if (isCustomRowAction(menuItem.type)) {
      const customItem = menuItem as CustomEntityItemControl<TEntity>
      return {
        content: customItem.cta?.label,
        onClick: () => customItem.action(item),
      }
    }

    return {
      content: "",
      onClick: () => {},
    }
  }

  const activeMenuItems = items.filter((x) => isControlEnabled(x, item))

  if (!activeMenuItems.length) {
    return <></>
  }

  return (
    <>
      {uiRegistry().components.renderPopoverMenu({
        control: uiRegistry().components.renderButton({
          iconOnly: true,
          rounded: true,
          children: uiRegistry().renderIcon("options", {
            width: 16,
          }),
        }),
        minPanelWidth: 130,
        align: "end",
        position: "bottom",
        items: activeMenuItems.map((x) => toDropDownItem(x)),
      })}
    </>
  )
}
