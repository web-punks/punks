import type { Meta, StoryObj } from "@storybook/react"
import {
  EntityListController,
  EntityListControllerProps,
} from "./EntityListController"
import {
  FooListItemDto,
  FooSearchFilters,
} from "../../../../../__test__/infrastructure/api/backend"
import { FooProvider } from "../__test__/components/FooProvider"
import {
  EntitiesFetchMode,
  ItemControlType,
  MenuControlType,
} from "../../../services"
import { Col, Row } from "@punks/ui-components"

const FooListController = (
  props: EntityListControllerProps<FooListItemDto, FooSearchFilters>
) => <EntityListController {...props} />

const meta = {
  title: "Components/EntityListController",
  component: FooListController,
  args: {
    menuControls: {
      primary: {
        styles: {
          width: 140,
        },
        controls: [
          {
            type: MenuControlType.Create,
          },
          {
            type: MenuControlType.Import,
          },
        ],
      },
      secondary: {
        controls: [
          {
            type: MenuControlType.Export,
          },
        ],
      },
    },
    itemControls: [
      {
        type: ItemControlType.Edit,
      },
      {
        type: ItemControlType.ViewJson,
      },
      {
        type: ItemControlType.Menu,
        items: [
          {
            type: ItemControlType.ViewJson,
          },
          {
            type: ItemControlType.ViewHistory,
          },
          {
            type: ItemControlType.Custom,
            action: async () => {},
            cta: {
              label: "Custom",
            },
          },
          {
            type: ItemControlType.Delete,
          },
        ],
      },
    ],
    renderContainer: ({ children }) => <Row cols={12}>{children}</Row>,
    renderItem: ({ item }) => (
      <Col
        size={{
          xs: 12,
          md: 6,
          lg: 4,
        }}
      >
        {item.name}
      </Col>
    ),
  },
  decorators: [
    (Story) => (
      <div style={{ maxWidth: 1000, margin: "0 auto" }}>
        <Story />
      </div>
    ),
  ],
} satisfies Meta<typeof FooListController>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
  decorators: [
    (Story) => (
      <FooProvider mode={EntitiesFetchMode.List}>
        <Story />
      </FooProvider>
    ),
  ],
}

export const CustomLayout: Story = {
  decorators: [
    (Story) => (
      <FooProvider mode={EntitiesFetchMode.List}>
        <Story />
      </FooProvider>
    ),
  ],
  args: {
    renderer: ({ content, head }) => (
      <div>
        <div>{head?.title}</div>
        <div>{head?.actions}</div>
        <div>{content.searchBar}</div>
        <div>{content.body}</div>
      </div>
    ),
  },
}

export const HiddenHeadAndSearchBar: Story = {
  args: {
    controls: {
      head: {
        enabled: false,
      },
      searchBar: {
        enabled: false,
      },
    },
  },
  decorators: [
    (Story) => (
      <FooProvider mode={EntitiesFetchMode.List}>
        <Story />
      </FooProvider>
    ),
  ],
}

export const PreFiltered: Story = {
  args: {},
  decorators: [
    (Story) => (
      <FooProvider
        mode={EntitiesFetchMode.List}
        dataFilter={{
          uid: {
            eq: "test",
          },
        }}
      >
        <Story />
      </FooProvider>
    ),
  ],
}

export const EmptyDefault: Story = {
  args: {},
  decorators: [
    (Story) => (
      <FooProvider
        mode={EntitiesFetchMode.List}
        dataFilter={{
          uid: {
            eq: "nonexistentitem",
          },
        }}
      >
        <Story />
      </FooProvider>
    ),
  ],
}

export const EmptyCustom: Story = {
  args: {
    emptyContent: (
      <div
        style={{
          width: "100%",
          textAlign: "center",
          marginTop: 50,
          marginBottom: 50,
        }}
      >
        Custom empty content
      </div>
    ),
  },
  decorators: [
    (Story) => (
      <FooProvider
        mode={EntitiesFetchMode.List}
        dataFilter={{
          uid: {
            eq: "nonexistentitem",
          },
        }}
      >
        <Story />
      </FooProvider>
    ),
  ],
}

export const EmptyNarrow: Story = {
  args: {},
  decorators: [
    (Story) => (
      <FooProvider
        mode={EntitiesFetchMode.List}
        dataFilter={{
          uid: {
            eq: "nonexistentitem",
          },
        }}
      >
        <Story />
      </FooProvider>
    ),
  ],
}

export const WithoutContainer: Story = {
  args: {
    renderContainer: undefined,
    renderItem: undefined,
    renderItems: ({ items }) => <div>{JSON.stringify(items)}</div>,
  },
  decorators: [
    (Story) => (
      <FooProvider mode={EntitiesFetchMode.List}>
        <Story />
      </FooProvider>
    ),
  ],
}
