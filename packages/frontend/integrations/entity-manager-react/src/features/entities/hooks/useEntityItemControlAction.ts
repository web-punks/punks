import { ItemControlType } from "../services"
import { useEntityController } from "../services/context/controller"

export const useEntityItemControlAction = <TEntity>(item: TEntity) => {
  const {
    actions: {
      selectItemToOpen,
      selectItemToEdit,
      selectItemToDelete,
      selectItemParentItemForCreateChild,
      itemExport,
      openViewHistoryModal,
      openItemJsonModal,
    },
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()

  const action = (type: ItemControlType) => {
    switch (type) {
      case ItemControlType.Edit:
        selectItemToEdit(item)
        break
      case ItemControlType.Details:
        selectItemToOpen(item)
        break
      case ItemControlType.Delete:
        selectItemToDelete(item)
        break
      case ItemControlType.Export:
        itemExport({ item })
        break
      case ItemControlType.ViewJson:
        openItemJsonModal(item)
        break
      case ItemControlType.ViewHistory:
        openViewHistoryModal(item)
        break
      case ItemControlType.CreateChild:
        selectItemParentItemForCreateChild(item)
        break
    }
  }

  return {
    action,
  }
}
