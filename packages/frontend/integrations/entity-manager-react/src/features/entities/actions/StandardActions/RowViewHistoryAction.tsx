import { uiRegistry } from "@punks/ui-components"
import { useEntityController } from "../../services/context/controller"
import { ItemActionResult } from "../../services/types"
import { ReactNode } from "react"

export interface RowViewHistoryActionProps<TEntity> {
  item: TEntity
  action?: (item: TEntity) => Promise<ItemActionResult | void>
  icon?: ReactNode
}

const RowViewHistoryAction = <TEntity,>({
  item,
  action,
  icon,
}: RowViewHistoryActionProps<TEntity>) => {
  const {
    actions: { openViewHistoryModal },
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()
  return (
    <>
      {uiRegistry().components.renderButton({
        onClick: () => (action ? action(item) : openViewHistoryModal(item)),
        iconOnly: true,
        rounded: true,
        children:
          icon ??
          uiRegistry().renderIcon("refresh", {
            width: 16,
          }),
      })}
    </>
  )
}

export default RowViewHistoryAction
