import {
  StandardItemControl,
  ItemControlType,
} from "../services/types/itemControls"
import RowDeleteAction from "./StandardActions/RowDeleteAction"
import RowDetailsAction from "./StandardActions/RowDetailsAction"
import RowExportAction from "./StandardActions/RowDownloadAction"
import RowEditAction from "./StandardActions/RowEditAction"
import RowViewHistoryAction from "./StandardActions/RowViewHistoryAction"
import RowViewJsonAction from "./StandardActions/RowViewJsonAction"

export interface EntityStandardRowActionProps<TEntity> {
  control: StandardItemControl<TEntity>
  item: TEntity
}

const EntityStandardRowAction = <TEntity,>({
  control,
  item,
}: EntityStandardRowActionProps<TEntity>) => {
  return (
    <>
      {control.type === ItemControlType.Edit && (
        <RowEditAction
          item={item}
          action={control.action}
          icon={control.icon}
        />
      )}
      {control.type === ItemControlType.Details && (
        <RowDetailsAction
          item={item}
          action={control.action}
          icon={control.icon}
        />
      )}
      {control.type === ItemControlType.Delete && (
        <RowDeleteAction
          item={item}
          action={control.action}
          icon={control.icon}
        />
      )}
      {control.type === ItemControlType.Export && (
        <RowExportAction
          item={item}
          action={control.action}
          icon={control.icon}
        />
      )}
      {control.type === ItemControlType.ViewJson && (
        <RowViewJsonAction
          item={item}
          action={control.action}
          icon={control.icon}
        />
      )}
      {control.type === ItemControlType.ViewHistory && (
        <RowViewHistoryAction
          item={item}
          action={control.action}
          icon={control.icon}
        />
      )}
      {control.type === ItemControlType.CreateChild && (
        <RowViewHistoryAction
          item={item}
          action={control.action}
          icon={control.icon}
        />
      )}
    </>
  )
}

export default EntityStandardRowAction
