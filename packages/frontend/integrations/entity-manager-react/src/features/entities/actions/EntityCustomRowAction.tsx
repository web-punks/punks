import { uiRegistry } from "@punks/ui-components"
import { useEntityController } from "../services/context/controller"
import { CustomEntityItemControl } from "../services/types/itemControls"
import { useOperation } from "@punks/ui-core"

export interface EntityCustomRowActionProps<TEntity> {
  control: CustomEntityItemControl<TEntity>
  item: TEntity
}

const EntityCustomRowAction = <TEntity,>({
  control,
  item,
}: EntityCustomRowActionProps<TEntity>) => {
  const {
    actions: { fetchEntities: refreshItems },
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()
  const { invoke, state } = useOperation({
    operation: () => {
      if (!control?.action) {
        throw new Error("No action defined")
      }
      return control?.action(item)
    },
    onError: () => {
      if (control.cta?.messages?.errorMessage) {
        uiRegistry().showAlert({
          type: "error",
          content: control.cta.messages?.errorMessage,
        })
      }
    },
    onSuccess: (result) => {
      if (control.cta?.messages?.successMessage) {
        uiRegistry().showAlert({
          type: "success",
          content: control.cta.messages.successMessage,
        })
      }
      if (result?.refreshItem) {
        refreshItems({})
      }
    },
  })

  return (
    <>
      {uiRegistry().components.renderButton({
        onClick: () => invoke(),
        
        loading: state.loading ?? control.state?.loading,
        color: control.cta?.color,
        children: control.cta?.icon ?? control.cta?.label,
        iconOnly: true,
        rounded: true,
      })}
    </>
  )
}

export default EntityCustomRowAction
