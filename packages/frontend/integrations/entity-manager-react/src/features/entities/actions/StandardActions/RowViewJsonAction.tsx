import { uiRegistry } from "@punks/ui-components"
import { useEntityController } from "../../services/context/controller"
import { ItemActionResult } from "../../services/types"
import { ReactNode } from "react"

export interface RowViewJsonActionProps<TEntity> {
  item: TEntity
  action?: (item: TEntity) => Promise<ItemActionResult | void>
  icon?: ReactNode
}

const RowViewJsonAction = <TEntity,>({
  item,
  action,
  icon,
}: RowViewJsonActionProps<TEntity>) => {
  const {
    actions: { openItemJsonModal },
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()
  return (
    <>
      {uiRegistry().components.renderButton({
        onClick: () => (action ? action(item) : openItemJsonModal(item)),
        iconOnly: true,
        rounded: true,
        children:
          icon ??
          uiRegistry().renderIcon("code", {
            width: 16,
          }),
      })}
    </>
  )
}

export default RowViewJsonAction
