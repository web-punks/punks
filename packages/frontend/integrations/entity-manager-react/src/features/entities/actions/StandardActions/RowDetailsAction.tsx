import { uiRegistry } from "@punks/ui-components"
import { useEntityController } from "../../services/context/controller"
import { ItemActionResult } from "../../services/types"
import { ReactNode } from "react"

export interface RowEditActionProps<TEntity> {
  item: TEntity
  action?: (item: TEntity) => Promise<ItemActionResult | void>
  icon?: ReactNode
}

const RowDetailsAction = <TEntity,>({
  item,
  action,
  icon,
}: RowEditActionProps<TEntity>) => {
  const {
    actions: { selectItemToOpen, openItemState },
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()
  return uiRegistry().components.renderButton({
    onClick: () => (action ? action(item) : selectItemToOpen(item)),
    iconOnly: true,
    rounded: true,
    loading: openItemState?.input === item && openItemState.loading,
    children:
      icon ??
      uiRegistry().renderIcon("eye", {
        width: 16,
      }),
  })
}

export default RowDetailsAction
