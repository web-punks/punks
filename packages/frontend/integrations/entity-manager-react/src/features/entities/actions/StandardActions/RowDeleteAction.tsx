import { uiRegistry } from "@punks/ui-components"
import { useEntityController } from "../../services/context/controller"
import { ItemActionResult } from "../../services/types"
import { ReactNode } from "react"

export interface RowDeleteActionProps<TEntity> {
  item: TEntity
  action?: (item: TEntity) => Promise<ItemActionResult | void>
  icon?: ReactNode
}

const RowDeleteAction = <TEntity,>({
  item,
  action,
  icon,
}: RowDeleteActionProps<TEntity>) => {
  const {
    actions: { selectItemToDelete },
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()

  return uiRegistry().components.renderButton({
    onClick: () => (action ? action(item) : selectItemToDelete(item)),
    iconOnly: true,
    rounded: true,
    color: "error",
    children:
      icon ??
      uiRegistry().renderIcon("delete", {
        width: 16,
      }),
  })
}

export default RowDeleteAction
