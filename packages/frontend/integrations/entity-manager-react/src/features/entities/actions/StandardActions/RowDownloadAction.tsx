import { useEntityController } from "../../services/context/controller"
import { uiRegistry } from "@punks/ui-components"
import { ItemActionResult } from "../../services/types"
import { ReactNode } from "react"

export interface RowExportActionProps<TEntity> {
  item: TEntity
  action?: (item: TEntity) => Promise<ItemActionResult | void>
  icon?: ReactNode
}

const RowExportAction = <TEntity,>({
  item,
  action,
  icon,
}: RowExportActionProps<TEntity>) => {
  const {
    actions: { itemExport, itemExportState },
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()

  return uiRegistry().components.renderButton({
    onClick: () => (action ? action(item) : itemExport({ item })),
    iconOnly: true,
    rounded: true,
    loading: itemExportState?.input === item && itemExportState.loading,
    children:
      icon ??
      uiRegistry().renderIcon("download", {
        width: 16,
      }),
  })
}

export default RowExportAction
