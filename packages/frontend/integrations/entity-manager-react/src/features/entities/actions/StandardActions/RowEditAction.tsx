import { uiRegistry } from "@punks/ui-components"
import { useEntityController } from "../../services/context/controller"
import { ItemActionResult } from "../../services/types"
import { ReactNode } from "react"

export interface RowEditActionProps<TEntity> {
  item: TEntity
  action?: (item: TEntity) => Promise<ItemActionResult | void>
  icon?: ReactNode
}

const RowEditAction = <TEntity,>({
  item,
  action,
  icon,
}: RowEditActionProps<TEntity>) => {
  const {
    actions: { selectItemToEdit, editItemState },
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    TEntity
  >()

  return uiRegistry().components.renderButton({
    onClick: () => (action ? action(item) : selectItemToEdit(item)),
    iconOnly: true,
    rounded: true,
    loading: editItemState?.input === item && editItemState.loading,
    children:
      icon ??
      uiRegistry().renderIcon("edit", {
        width: 16,
      }),
  })
}

export default RowEditAction
