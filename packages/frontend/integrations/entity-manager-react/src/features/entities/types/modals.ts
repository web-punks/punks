export type EntityModalOptions = {
  size?: "sm" | "md" | "lg" | "xl" | "full" | "auto"
}
