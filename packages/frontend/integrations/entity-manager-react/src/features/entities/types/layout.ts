export type EntityControllerLayoutRendererInput = {
  content: {
    searchBar?: React.ReactNode
    body: React.ReactNode
  }
  head?: {
    title?: React.ReactNode
    actions?: React.ReactNode
  }
}

export type EntityControllerLayoutRenderer = (
  input: EntityControllerLayoutRendererInput
) => React.ReactNode
