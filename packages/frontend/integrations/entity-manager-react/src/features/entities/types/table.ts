import { TableGridType } from "@punks/ui-components"

export type EntityTableLayoutContainer = "card"

export type EntityTableLayoutOptions = {
  grid?: TableGridType
}

export type EntityTableControls = {
  head?: {
    enabled?: boolean
  }
  searchBar?: {
    enabled?: boolean
    actions?: React.ReactNode
  }
}
