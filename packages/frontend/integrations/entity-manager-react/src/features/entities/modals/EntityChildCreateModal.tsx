import { uiRegistry } from "@punks/ui-components"
import { EntityCancelOptions } from "../../authentication/abstractions"
import { useEntityManagerLabel } from "../../../modules/labels"
import { useEntityManagerDefinition } from "../services/context/definition"
import { useEntityController } from "../services/context/controller"

export type EntityChildCreateModalItem<TEntity> = {
  id: string
  data: TEntity
}

export interface EntityChildCreateModalProps<TEntity, TCreate> {
  open: boolean
  onClose: () => void
  onCancel: (options?: EntityCancelOptions) => void
  parentItem: TEntity
}

export function EntityChildCreateModal<TEntity, TCreate>({
  onCancel,
  onClose,
  open,
  parentItem,
}: EntityChildCreateModalProps<TEntity, TCreate>) {
  const { forms, modals, selectors } = useEntityManagerDefinition<
    TEntity,
    unknown,
    TCreate,
    unknown,
    unknown
  >()
  const {
    actions: { itemCreate, createState },
  } = useEntityController<
    TEntity,
    unknown,
    TCreate,
    unknown,
    unknown,
    unknown
  >()
  const { getLabel } = useEntityManagerLabel()

  if (!forms?.create) {
    return <></>
  }

  const Form = forms.create

  const handleSave = async (data: TCreate) => {
    await itemCreate({
      data,
      parent: parentItem
        ? {
            id: selectors.getId(parentItem),
            item: parentItem,
          }
        : undefined,
    })
    onClose()
  }

  return uiRegistry().components.renderModal({
    head: {
      title: getLabel("modals:create:title"),
    },
    size: modals.childCreateModalOptions?.size ?? "sm",
    open,
    onClose,
    children: open ? (
      <Form
        onSave={handleSave}
        onCancel={(options) => onCancel(options)}
        saving={createState.loading}
        additionalProps={forms.createAdditionalProps}
        parentItem={parentItem}
      />
    ) : null,
  })
}
