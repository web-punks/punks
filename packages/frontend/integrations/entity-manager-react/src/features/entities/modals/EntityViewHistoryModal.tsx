import { uiRegistry } from "@punks/ui-components"
import { useEntityManagerDefinition } from "../services/context/definition"
import { EntityCloseOptions } from "../../authentication/abstractions"
import { useEntityManagerLabel } from "../../../modules/labels"
import EntityHistoryTable from "../components/tables/EntityHistoryTable"

export interface EntityViewHistoryModalProps<TEntity> {
  open: boolean
  onClose: (options?: EntityCloseOptions) => void
  item: TEntity | undefined
}

export function EntityViewHistoryModal<TEntity, TUpdate>({
  onClose,
  open,
  item,
}: EntityViewHistoryModalProps<TEntity>) {
  const { modals } = useEntityManagerDefinition<
    TEntity,
    unknown,
    unknown,
    TUpdate,
    unknown
  >()
  const { getLabel } = useEntityManagerLabel()
  if (!modals.versions) {
    return <></>
  }

  const handleClose = async (options?: EntityCloseOptions) => {
    onClose(options)
  }

  return uiRegistry().components.renderModal({
    head: {
      title: getLabel("modals:versions:title"),
    },
    open,
    size: modals?.versionsModalOptions?.size ?? "md",
    onClose,
    children:
      open && item ? (
        <>
          <EntityHistoryTable item={item} />
          {uiRegistry().components.renderBox({
            mt: 4,
            children: uiRegistry().components.renderButton({
              children: getLabel("modals:versions:cta:close"),
              onClick: () => handleClose(),
              variant: "filled",
              color: "primary",
            }),
            alignItems: "center",
            justifyContent: "center",
            flex: true,
          })}
        </>
      ) : undefined,
  })
}
