import { uiRegistry } from "@punks/ui-components"
import { useEntityController } from "../services/context/controller"
import { useEntityManagerDefinition } from "../services/context/definition"
import { EntityCancelOptions } from "../../authentication/abstractions"
import { useEntityManagerLabel } from "../../../modules/labels"

export type EntityUpdateModalItem<TEntity> = {
  id: string
  data: TEntity
}

export interface EntityUpdateModalProps<TEntity> {
  open: boolean
  onClose: () => void
  onCancel: (options?: EntityCancelOptions) => void
  item?: EntityUpdateModalItem<TEntity>
}

export function EntityUpdateModal<TEntity, TUpdate>({
  onClose,
  onCancel,
  open,
  item,
}: EntityUpdateModalProps<TEntity>) {
  const { forms, operations, modals } = useEntityManagerDefinition<
    TEntity,
    unknown,
    unknown,
    TUpdate,
    unknown
  >()
  const {
    actions: { itemUpdate, updateState },
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()
  const { getLabel } = useEntityManagerLabel()

  if (!operations.update) {
    console.trace("Cannot open update dialog: Update operation is not defined")
    return <></>
  }

  if (!forms?.update) {
    console.trace("Cannot open update dialog: Update form is not defined")
    return <></>
  }

  const Form = forms.update

  const handleUpdate = async (data: TUpdate) => {
    if (!item) {
      throw new Error("Item is not defined")
    }

    await itemUpdate({
      data,
      id: item.id,
    })
    onClose()
  }

  return uiRegistry().components.renderModal({
    head: {
      title: getLabel("modals:update:title"),
    },
    children:
      open && item ? (
        <Form
          item={item.data}
          onSave={handleUpdate}
          onCancel={(options) => onCancel(options)}
          saving={updateState.loading}
          additionalProps={forms.updateAdditionalProps}
        />
      ) : null,
    onClose,
    open,
    size: modals?.updateModalOptions?.size ?? "md",
  })
}
