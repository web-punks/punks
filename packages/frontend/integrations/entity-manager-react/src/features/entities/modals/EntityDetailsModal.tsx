import { uiRegistry } from "@punks/ui-components"
import { useEntityManagerDefinition } from "../services/context/definition"
import { EntityCloseOptions } from "../../authentication/abstractions"
import { useEntityManagerLabel } from "../../../modules/labels"

export interface EntityDetailsModalProps<TEntity> {
  open: boolean
  onClose: (options?: EntityCloseOptions) => void
  item: TEntity | undefined
}

export function EntityDetailsModal<TEntity, TUpdate>({
  onClose,
  open,
  item,
}: EntityDetailsModalProps<TEntity>) {
  const { getLabel } = useEntityManagerLabel()
  const { forms, modals } = useEntityManagerDefinition<
    TEntity,
    unknown,
    unknown,
    TUpdate,
    unknown
  >()

  if (!forms?.details) {
    return <></>
  }

  const Form = forms.details

  const handleClose = async (options?: EntityCloseOptions) => {
    onClose(options)
  }

  return uiRegistry().components.renderModal({
    head: {
      title: getLabel("modals:details:title"),
    },
    size: modals.detailsModalOptions?.size ?? "sm",
    open,
    onClose,
    children:
      open && item ? (
        <Form
          item={item}
          onClose={handleClose}
          additionalProps={forms.detailsAdditionalProps}
        />
      ) : null,
  })
}
