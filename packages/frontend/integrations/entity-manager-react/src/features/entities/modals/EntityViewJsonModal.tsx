import { uiRegistry } from "@punks/ui-components"
import { EntityCloseOptions } from "../../authentication/abstractions"
import { useEntityManagerLabel } from "../../../modules/labels"
import { useEntityManagerDefinition } from "../services/context/definition"

export interface EntityViewJsonModalProps<TEntity> {
  open: boolean
  onClose: (options?: EntityCloseOptions) => void
  item: TEntity | undefined
}

export function EntityViewJsonModal<TEntity>({
  onClose,
  open,
  item,
}: EntityViewJsonModalProps<TEntity>) {
  const { getLabel } = useEntityManagerLabel()
  const { modals } = useEntityManagerDefinition<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown
  >()

  return uiRegistry().components.renderModal({
    head: {
      title: getLabel("modals:jsonView:title"),
    },
    open,
    size: modals?.viewJsonModalOptions?.size ?? "md",
    onClose,
    children:
      open && item
        ? uiRegistry().components.renderJsonViewer({
            fullWidth: true,
            src: item,
            labels: {
              explorerTabTitle: getLabel("modals:jsonView:explorerTabTitle"),
              rawTabTitle: getLabel("modals:jsonView:rawTabTitle"),
            },
          })
        : undefined,
  })
}
