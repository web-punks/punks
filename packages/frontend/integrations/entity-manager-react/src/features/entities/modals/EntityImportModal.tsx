import { uiRegistry } from "@punks/ui-components"
import { EntityCancelOptions } from "../../authentication/abstractions"
import { useEntityManagerLabel } from "../../../modules/labels"
import { useEntityManagerDefinition } from "../services/context/definition"
import { EntityImportPanel } from "../panels/EntityImportPanel"

export interface EntityImportModalProps {
  open: boolean
  onSubmit: (file: File) => void | Promise<void>
  onSampleDownload: () => void | Promise<void>
  onCancel: (options?: EntityCancelOptions) => void
  importLoading?: boolean
}

export function EntityImportModal<TEntity>({
  onCancel,
  onSubmit,
  onSampleDownload,
  open,
  importLoading,
}: EntityImportModalProps) {
  const { getLabel } = useEntityManagerLabel()
  const { modals } = useEntityManagerDefinition<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown
  >()

  const handleCancel = () => {
    onCancel()
  }

  return uiRegistry().components.renderModal({
    head: {
      title: getLabel("modals:import:title"),
    },
    size: modals?.importModalOptions?.size ?? "md",
    open,
    onClose: handleCancel,
    children: open ? (
      <EntityImportPanel
        onSampleDownload={onSampleDownload}
        onSubmit={onSubmit}
        importLoading={importLoading}
      />
    ) : undefined,
  })
}
