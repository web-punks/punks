import { uiRegistry } from "@punks/ui-components"
import { useEntityController } from "../services/context/controller"
import { useEntityManagerDefinition } from "../services/context/definition"
import { useEntityManagerLabel } from "../../../modules/labels"

export interface EntityDeleteModalProps<TEntity> {
  item: TEntity
  open: boolean
  onClose: () => void
}

const EntityDeleteModal = <TEntity,>({
  item,
  onClose,
  open,
}: EntityDeleteModalProps<TEntity>) => {
  const { getLabel } = useEntityManagerLabel()
  const { operations, selectors } = useEntityManagerDefinition<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown
  >()

  const {
    actions: { itemDelete, deleteState },
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()

  if (!operations.delete) {
    return <></>
  }

  const handleDelete = async (item: TEntity) => {
    await itemDelete({ item })
    onClose()
  }

  return uiRegistry().components.renderConfirmationDialog({
    title: getLabel("modals:delete:title"),
    open,
    onClose,
    confirm: {
      label: getLabel("modals:delete:cta:confirm"),
      onClick: () => handleDelete(item),
      loading: deleteState.loading,
      color: "error",
    },
    cancel: {
      label: getLabel("modals:delete:cta:cancel"),
      onClick: onClose,
    },
    message: getLabel("modals:delete:messages:confirm"),
    detail: item
      ? uiRegistry().components.renderTypography({
          variant: "label1",
          children: selectors.getDisplayName(item),
        })
      : undefined,
  })
}

export default EntityDeleteModal
