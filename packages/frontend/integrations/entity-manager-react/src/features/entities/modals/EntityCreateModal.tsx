import { uiRegistry } from "@punks/ui-components"
import { useEntityController } from "../services/context/controller"
import { useEntityManagerDefinition } from "../services/context/definition"
import { EntityCancelOptions } from "../../authentication/abstractions"
import { useEntityManagerLabel } from "../../../modules/labels"

export interface EntityCreateModalProps {
  open: boolean
  onClose: () => void
  onCancel: (options?: EntityCancelOptions) => void
}

export function EntityCreateModal<TEntity, TCreate>({
  onCancel,
  onClose,
  open,
}: EntityCreateModalProps) {
  const { forms, modals } = useEntityManagerDefinition<
    TEntity,
    unknown,
    TCreate,
    unknown,
    unknown
  >()
  const {
    actions: { itemCreate, createState },
  } = useEntityController<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown,
    unknown
  >()
  const { getLabel } = useEntityManagerLabel()

  if (!forms?.create) {
    return <></>
  }

  const Form = forms.create
  const handleSave = async (data: TCreate) => {
    await itemCreate({ data })
    onClose()
  }

  return uiRegistry().components.renderModal({
    head: {
      title: getLabel("modals:create:title"),
    },
    size: modals.createModalOptions?.size ?? "sm",
    open,
    onClose,
    children: open ? (
      <Form
        onSave={handleSave}
        onCancel={(options) => onCancel(options)}
        saving={createState.loading}
        additionalProps={forms.createAdditionalProps}
      />
    ) : null,
  })
}
