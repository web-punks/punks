import { useEntityManagerLabel } from "../../../modules/labels"
import { useEntityController } from "../services/context/controller"
import { EntityOperations } from "../services/types"
import {
  EntityMenuControls,
  EntityMenuControl,
  MenuControlType,
} from "../services/types/menuControls"

export interface EntityMenuItem {
  content: React.ReactNode
  onClick?: () => void
}

export interface MenuItemsInput<TEntity> {
  operations: EntityOperations<TEntity, unknown, unknown, unknown, unknown>
  menuControls: EntityMenuControls<TEntity>
}

export const useMenuItems = <TEntity>({
  menuControls,
  operations,
}: MenuItemsInput<TEntity>) => {
  const {
    actions: { itemsExport, openCreateModal, openImportModal: openUploadModal },
  } = useEntityController()

  const { getLabel } = useEntityManagerLabel()
  const handleExport = () => itemsExport()

  const mapItem = (
    control: EntityMenuControl<TEntity>
  ): EntityMenuItem | undefined => {
    switch (control.type) {
      case MenuControlType.Create:
        return operations.create
          ? {
              content: getLabel("operations:create:labels:label"),
              onClick: () => openCreateModal(),
            }
          : undefined
      case MenuControlType.Import:
        return operations.import
          ? {
              content: getLabel("operations:import:labels:label"),
              onClick: () => openUploadModal(),
            }
          : undefined
      case MenuControlType.Export:
        return operations.export
          ? {
              content: getLabel("operations:export:labels:label"),
              onClick: handleExport,
            }
          : undefined
      case MenuControlType.Custom:
        return {
          content: control?.cta?.label,
          onClick: control?.action,
        }
      default:
        return undefined
    }
  }

  const menuItems = menuControls
    .filter((x) => x.enabled !== false)
    .map(mapItem)
    .filter((x) => x) as EntityMenuItem[]

  return {
    menuItems,
  }
}
