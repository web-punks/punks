import { uiRegistry } from "@punks/ui-components"
import { useEntityManagerLabel } from "../../../modules/labels"
import { useEntityManagerDefinition } from "../services/context/definition"
import { useEntityController } from "../services"
import { useState } from "react"
import { EntitiesParseResult } from "../services/types/actions"
import EntitiesPreviewTable from "../components/tables/EntitiesPreviewTable"

export interface EntityImportPanelProps {
  onSubmit: (file: File) => void | Promise<void>
  onSampleDownload: () => void | Promise<void>
  importLoading?: boolean
}

export function EntityImportPanel<TEntity>({
  onSubmit,
  onSampleDownload,
  importLoading,
}: EntityImportPanelProps) {
  const { getLabel } = useEntityManagerLabel()
  const { operations } = useEntityManagerDefinition<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown
  >()

  const {
    actions: { itemsParse, parseState },
  } = useEntityController()

  const [entitiesParseResult, setEntitiesParseResult] = useState<{
    file: File
    result: EntitiesParseResult
  }>()

  const hasPreview = () => !!operations.parse

  const resetParse = () => setEntitiesParseResult(undefined)

  const handleParse = async (file: File) => {
    await uiRegistry().withNotification(
      async () => {
        const parseResult = await itemsParse({
          file,
        })
        setEntitiesParseResult({
          file,
          result: parseResult,
        })
      },
      {
        success: getLabel("modals:import:messages:parseSuccess"),
        error: getLabel("modals:import:messages:parseError"),
        pending: getLabel("modals:import:messages:parsePending"),
      }
    )
  }

  const handleUpload = async (file: File) => {
    await onSubmit(file)
    resetParse()
  }

  const handleFileSubmit = async (file: File) => {
    if (hasPreview()) {
      handleParse(file)
      return
    }

    handleUpload(file)
  }

  const handleSampleDownload = () => {
    onSampleDownload()
  }

  const handleBack = () => {
    setEntitiesParseResult(undefined)
  }

  const stage = !entitiesParseResult ? "upload" : "parse"

  return (
    <>
      {stage === "upload" && (
        <>
          {uiRegistry().components.renderFileUploadPanel({
            multiple: false,
            accept: ".xls, .xlsx",
            onSubmit: (files) =>
              files.length > 0 ? handleFileSubmit(files[0]) : undefined,
            subtitle: getLabel("modals:import:subtitle"),
            selectCta: {
              label: getLabel("modals:import:cta:select"),
            },
            submitCta: {
              label: getLabel("modals:import:cta:submit"),
              loading: parseState?.loading,
            },
            placeholder: (
              <>
                {uiRegistry().components.renderTypography({
                  children: getLabel("modals:import:messages:dropLine1"),
                })}
                {uiRegistry().components.renderTypography({
                  children: getLabel("modals:import:messages:dropLine2"),
                  weight: "bold",
                })}
              </>
            ),
          })}
          {uiRegistry().components.renderBox({
            mt: 12,
            flex: true,
            alignItems: "center",
            justifyContent: "center",
            children: uiRegistry().components.renderButton({
              children: getLabel("modals:import:cta:example"),
              onClick: handleSampleDownload,
              variant: "outlined",
              color: "primary",
            }),
          })}
        </>
      )}
      {stage === "parse" && (
        <>
          <EntitiesPreviewTable
            data={entitiesParseResult!.result}
            submitInProgress={importLoading ?? false}
            onSubmit={() => handleUpload(entitiesParseResult!.file)}
            onBack={handleBack}
          />
        </>
      )}
    </>
  )
}
