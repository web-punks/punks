import { uiRegistry } from "@punks/ui-components"
import { EntityControllerLayoutRenderer } from "../types/layout"

export const defaultRenderer: EntityControllerLayoutRenderer = ({
  content,
  head,
}) => {
  return (
    <>
      {head &&
        uiRegistry().components.renderFormHead({
          title: head.title,
          endControl: head.actions,
        })}

      {content.searchBar &&
        uiRegistry().components.renderBox({
          mt: 8,
          mb: 8,
          children: content.searchBar,
        })}

      {content.body &&
        uiRegistry().components.renderBox({
          mt: 8,
          mb: 8,
          children: content.body,
        })}
    </>
  )
}
