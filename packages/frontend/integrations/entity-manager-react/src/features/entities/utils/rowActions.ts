import { EntityItemControl, ItemControlType } from "../services"

export const isCustomRowAction = (type: ItemControlType) =>
  type === ItemControlType.Custom

export const isStandardRowAction = (type: ItemControlType) =>
  !isCustomRowAction(type)

export const isMenuRowAction = (type: ItemControlType) =>
  type === ItemControlType.Menu

export const isControlEnabled = <TEntity>(
  control: EntityItemControl<TEntity>,
  item: TEntity
) => {
  if (typeof control.enabled === "boolean") {
    return control.enabled
  }

  if (control.enabled) {
    return control.enabled(item)
  }

  return true
}
