import EntityManager from "../../services/components/EntityManager/context"
import {
  EntityOperations,
  EntitySelectors,
  EntityMetadata,
  EntitiesFetchMode,
} from "../../services/types"
import { EntityClientFiltersMatcher } from "../../services/types/filters"
import { EntityForms } from "../../services/types/forms"
import { EntityModals } from "../../services/types/modals"
import { EntityManagerOptions } from "../../services/types/options"
import { EntityPaging } from "../../services/types/paging"
import { EntitiesSortingValue } from "../../services/types/sorting"
import { EntityManagerInitialState } from "../../services/types/state"
import {
  EntityManagerLabelsMap,
  EntityManagerLabelsProvider,
} from "../../../../modules/labels"
import { EntityCallbacksProvider } from "../../services/context/callbacks"

export interface EntityProvidersProps<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
  forms?: EntityForms<TEntity, TCreate, TUpdate>
  dataFilter?: Partial<TFilters>
  initialFilters?: TFilters
  initialSorting?: EntitiesSortingValue<TSorting>
  initialPaging?: EntityPaging
  initialState?: EntityManagerInitialState<TEntity>
  clientFiltersMatcher?: EntityClientFiltersMatcher<TEntity>
  modals: EntityModals<TEntity>
  selectors: EntitySelectors<TEntity>
  options?: EntityManagerOptions
  labels: EntityManagerLabelsMap
  metadata?: EntityMetadata
  mode: EntitiesFetchMode
  children: any
}

export function EntityProviders<TEntity, TFilters, TCreate, TUpdate, TSorting>({
  operations,
  labels,
  dataFilter,
  initialFilters,
  initialSorting,
  initialPaging,
  initialState,
  clientFiltersMatcher,
  modals,
  selectors,
  forms,
  options,
  metadata,
  mode,
  children,
}: EntityProvidersProps<TEntity, TFilters, TCreate, TUpdate, TSorting>) {
  return (
    <EntityManagerLabelsProvider labels={labels}>
      <EntityCallbacksProvider>
        <EntityManager<TEntity, TFilters, TCreate, TUpdate, TSorting>
          operations={operations}
          forms={forms}
          dataFilter={dataFilter}
          initialFilters={initialFilters}
          initialSorting={initialSorting}
          initialState={initialState}
          initialPaging={initialPaging}
          clientFiltersMatcher={clientFiltersMatcher}
          modals={modals}
          selectors={selectors}
          options={options}
          metadata={metadata}
          mode={mode}
          initialize
        >
          {children}
        </EntityManager>
      </EntityCallbacksProvider>
    </EntityManagerLabelsProvider>
  )
}
