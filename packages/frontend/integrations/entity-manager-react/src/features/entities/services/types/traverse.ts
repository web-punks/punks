export type EntityTraverseOptions = {
  nodeIds?: string[]
  parentIds?: string[]
  rootOnly?: boolean
}
