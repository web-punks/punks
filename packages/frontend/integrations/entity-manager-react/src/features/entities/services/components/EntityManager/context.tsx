import { useEntityCallbacksContext } from "../../context/callbacks"
import { EntityControllerProvider } from "../../context/controller"
import { EntityManagerDefinitionProvider } from "../../context/definition"
import {
  EntityOperations,
  EntitySelectors,
  EntityMetadata,
  EntitiesFetchMode,
} from "../../types"
import { EntityClientFiltersMatcher } from "../../types/filters"
import { EntityForms } from "../../types/forms"
import { EntityModals } from "../../types/modals"
import { EntityManagerOptions } from "../../types/options"
import { EntityPaging } from "../../types/paging"
import { EntitiesSortingValue } from "../../types/sorting"
import { EntityManagerInitialState } from "../../types/state"
import { useEntityManager } from "./useEntityManager"

export interface EntityManagerProps<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  children: any
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
  forms?: EntityForms<TEntity, TCreate, TUpdate>
  dataFilter?: Partial<TFilters>
  initialFilters?: TFilters
  initialSorting?: EntitiesSortingValue<TSorting>
  initialState?: EntityManagerInitialState<TEntity>
  initialPaging?: EntityPaging
  modals: EntityModals<TEntity>
  selectors: EntitySelectors<TEntity>
  clientFiltersMatcher?: EntityClientFiltersMatcher<TEntity>
  initialize?: boolean
  options?: EntityManagerOptions
  metadata?: EntityMetadata
  mode: EntitiesFetchMode
}

const EntityManager = <TEntity, TFilters, TCreate, TUpdate, TSorting>({
  operations,
  forms,
  selectors,
  modals,
  children,
  dataFilter,
  initialFilters,
  initialSorting,
  initialState,
  initialPaging,
  clientFiltersMatcher,
  initialize,
  metadata,
  options,
  mode,
}: EntityManagerProps<TEntity, TFilters, TCreate, TUpdate, TSorting>) => {
  const { callbacks, initialized } = useEntityCallbacksContext()
  const {
    fetchResults,
    actions,
    data,
    filteredData,
    ui,
    empty,
    emptyDataSource,
    firstFetchCompleted,
    clientFilters,
    filters,
    sorting,
    paging,
    updatePaging,
    updateFilters,
    updateSorting,
    updateQuery,
    query,
  } = useEntityManager({
    operations,
    clientFiltersMatcher,
    dataFilter,
    initialFilters,
    initialSorting,
    initialState,
    initialPaging,
    initialize,
    selectors,
    callbacks,
    initialized,
    mode,
  })
  return (
    <EntityManagerDefinitionProvider
      value={{
        selectors,
        forms,
        modals,
        operations,
        clientFiltersMatcher,
        filters,
        options,
        metadata,
      }}
    >
      <EntityControllerProvider
        value={{
          fetchResults,
          actions,
          clientFilters,
          data,
          empty,
          emptyDataSource,
          firstFetchCompleted,
          filteredData,
          ui,
          filters,
          sorting,
          paging,
          updateFilters,
          updateSorting,
          updateQuery,
          updatePaging,
          query,
        }}
      >
        {children}
      </EntityControllerProvider>
    </EntityManagerDefinitionProvider>
  )
}

export default EntityManager
