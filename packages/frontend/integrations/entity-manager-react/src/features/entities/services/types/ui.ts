export type EntityGlobalModalState = {
  open: boolean
}

export type EntityItemModalState<TEntity> = {
  open: boolean
  item?: TEntity
}

export type EntityUiState<TEntity> = {
  createModal: EntityGlobalModalState
  uploadModal: EntityGlobalModalState
  deleteModal: EntityItemModalState<TEntity>
  childCreateModal: EntityItemModalState<TEntity>
  viewDetailsModal: EntityItemModalState<TEntity>
  viewHistoryModal: EntityItemModalState<TEntity>
  viewJsonModal: EntityItemModalState<TEntity>
}
