import { useOperation, openUrlInNewWindow } from "@punks/ui-core"
import { EntityOperations } from "../types"
import { uiRegistry } from "@punks/ui-components"
import { useEntityManagerLabel } from "../../../../modules"

export interface EntitySheetExportAction<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
}

export const useEntitySheetExportAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
}: EntitySheetExportAction<TEntity, TFilters, TCreate, TUpdate, TSorting>) => {
  const { getLabel } = useEntityManagerLabel()
  const { invoke: exportSheet, state: exportState } = useOperation({
    operation: async () => {
      if (!operations.export) {
        throw new Error("Export sheets is not defined")
      }
      return await operations.export?.action()
    },
    onError: (error) => operations.export?.onError?.(error),
    onSuccess: (result) => {
      if (result?.downloadUrl) {
        openUrlInNewWindow(result.downloadUrl)
      }
      operations.export?.callback?.(result)
    },
  })

  const itemsExport = async () => {
    await uiRegistry().withNotification(
      async () => {
        await exportSheet()
      },
      {
        success: getLabel("operations:export:messages:success"),
        error: getLabel("operations:export:messages:genericError"),
        pending: getLabel("operations:export:messages:inProgress"),
      }
    )
  }

  return {
    exportSheet,
    exportState,
    itemsExport,
  }
}
