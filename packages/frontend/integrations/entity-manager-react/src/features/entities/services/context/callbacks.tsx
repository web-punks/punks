import { createContext, useContext, useState } from "react"
import { EntityCallbacks } from "../types/actions"

export type EntityCallbacksType<TEntity, TCreate, TUpdate> = {
  initialized: boolean
  callbacks: EntityCallbacks<TEntity, TCreate, TUpdate>
  setCallbacks: (
    callbacks: Omit<EntityCallbacks<TEntity, TCreate, TUpdate>, "initialized">
  ) => void
}

export const EntityCallbacksContext = createContext<
  EntityCallbacksType<unknown, unknown, unknown>
>({
  initialized: false,
  callbacks: {},
  setCallbacks: () => {},
})

export const useEntityCallbacks = <TEntity, TCreate, TUpdate>() =>
  useContext(EntityCallbacksContext) as EntityCallbacksType<
    TEntity,
    TCreate,
    TUpdate
  >

export interface EntityCallbacksProviderProps {
  children: any
}

export const EntityCallbacksProvider = ({
  children,
}: EntityCallbacksProviderProps) => {
  const [initialized, setInitialized] = useState(false)
  const [callbacks, setCallbacks] =
    useState<EntityCallbacks<unknown, unknown, unknown>>()

  const handleSetCallbacks = (
    callbacks: EntityCallbacks<unknown, unknown, unknown>
  ) => {
    setCallbacks(callbacks)
    setInitialized(true)
  }

  return (
    <EntityCallbacksContext.Provider
      value={{
        initialized,
        callbacks: callbacks ?? {},
        setCallbacks: handleSetCallbacks,
      }}
    >
      {children}
    </EntityCallbacksContext.Provider>
  )
}

export const useEntityCallbacksContext = <TEntity, TCreate, TUpdate>() =>
  useContext(EntityCallbacksContext) as EntityCallbacksType<
    TEntity,
    TCreate,
    TUpdate
  >
