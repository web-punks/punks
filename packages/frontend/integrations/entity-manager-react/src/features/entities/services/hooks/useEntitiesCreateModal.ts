import { useState } from "react"

export const useEntitiesCreateModal = () => {
  const [createModalOpen, setCreateModalOpen] = useState(false)

  return {
    createModalOpen,
    setCreateModalOpen,
  }
}
