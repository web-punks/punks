import { OperationalState, ThemeColor } from "@punks/ui-core"
import { ReactNode } from "react"

export enum ItemControlType {
  Edit = "edit",
  Details = "details",
  Delete = "delete",
  Export = "export",
  ViewJson = "viewJson",
  ViewHistory = "viewHistory",
  Custom = "custom",
  Menu = "menu",
  CreateChild = "createChild",
}

export type ItemActionResult = {
  refreshItem?: boolean
}

export type EntityItemControlCtaMessages = {
  errorMessage?: string
  successMessage?: string
}

export type EntityItemControlCta = {
  icon?: ReactNode
  label?: ReactNode
  color?: ThemeColor
  messages?: EntityItemControlCtaMessages
}

export type EntityItemControlBase<TEntity> = {
  enabled?: boolean | ((item: TEntity) => boolean)
}

export type StandardItemControl<TEntity> = EntityItemControlBase<TEntity> & {
  type:
    | ItemControlType.Edit
    | ItemControlType.Details
    | ItemControlType.Delete
    | ItemControlType.Export
    | ItemControlType.ViewHistory
    | ItemControlType.ViewJson
    | ItemControlType.CreateChild
  action?: (item: TEntity) => Promise<ItemActionResult | void>
  icon?: ReactNode
}

export type MenuItemControl<TEntity> = EntityItemControlBase<TEntity> & {
  type: ItemControlType.Menu
  items: EntityItemControls<TEntity>
}

export type CustomEntityItemControl<TEntity> =
  EntityItemControlBase<TEntity> & {
    type: ItemControlType.Custom
    action: (item: TEntity) => Promise<ItemActionResult | void>
    state?: OperationalState<unknown>
    cta: EntityItemControlCta
  }

export type EntityItemControl<TEntity> =
  | StandardItemControl<TEntity>
  | MenuItemControl<TEntity>
  | CustomEntityItemControl<TEntity>

export type EntityItemAction = {
  label: React.ReactNode
  onClick: () => void
}

export type EntityItemControls<TEntity> = EntityItemControl<TEntity>[]
