import { useOperation } from "@punks/ui-core"
import { useEntityManagerDefinition } from "../context/definition"
import { uiRegistry } from "@punks/ui-components"
import { EntityVersionsResult } from "../types/actions"
import { useEntityManagerLabel } from "../../../../modules"

export type EntityVersionsActionInput<TEntity> = {
  item: TEntity
}

export const useEntityViewHistoryAction = <TEntity>() => {
  const { operations, selectors } = useEntityManagerDefinition<
    TEntity,
    unknown,
    unknown,
    unknown,
    unknown
  >()
  const { getLabel } = useEntityManagerLabel()
  const { invoke, state } = useOperation<
    EntityVersionsResult<TEntity>,
    EntityVersionsActionInput<TEntity>
  >({
    operation: (input) => {
      if (!operations.versions?.action) {
        throw new Error("No versions action defined")
      }
      return operations.versions?.action({
        id: selectors.getId(input.item),
        sorting: {
          direction: "desc",
        },
      })
    },
    onError: () =>
      uiRegistry().showAlert({
        type: "error",
        content: getLabel("operations:getVersions:messages:genericError"),
      }),
    onSuccess: () => {
      // operations.fetch?.callback?.()
    },
  })

  return {
    invoke,
    state,
  }
}
