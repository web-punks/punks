export type EntityManagerFilterStrategy = "client" | "server"

export type EntityManagerOptions = {
  queryFilterStrategy?: EntityManagerFilterStrategy
  queryFilterDebounceMs?: number
}
