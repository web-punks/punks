import {
  TableFilterDateValue,
  TableTextFilterType,
  TableTextFilterValue,
} from "@punks/ui-components"

export type EntityTextFilterState = {
  value: string
  onChange: (value: string) => void
}

export type EntityClientFilters = {
  query?: EntityTextFilterState
}

export type EntityClientFiltersMatcher<TEntity> = {
  matchesQuery?: (item: TEntity, query: string) => boolean
}

export enum EntityFilterType {
  Text = "text",
  SingleSelect = "singleSelect",
  MultipleSelect = "multipleSelect",
  Date = "date",
}

export type EntitiesFilterBase = {
  enabled?: boolean
}

export type EntitiesDateFilter<TFilters> = EntitiesFilterBase & {
  type: EntityFilterType.Date
  onSubmit: (current: TFilters, value: TableFilterDateValue) => TFilters
  onClear: (current: TFilters) => TFilters
  value: (current: TFilters) => TableFilterDateValue | undefined
}

export type EntitiesTextFilter<TFilters> = EntitiesFilterBase & {
  type: EntityFilterType.Text
  onSubmit: (current: TFilters, value: TableTextFilterValue) => TFilters
  onClear: (current: TFilters) => TFilters
  value: (current: TFilters) => TableTextFilterValue | undefined
  types?: TableTextFilterType[]
}

export type EntityFacets = {
  [key: string]: any
}

export type EntityFilterOption<TValue> = {
  label: string
  value: TValue
}

export type EntitiesSingleSelectFilter<TValue, TFilters> =
  EntitiesFilterBase & {
    type: EntityFilterType.SingleSelect
    selectedValue: (current: TFilters) => TValue | undefined
    onChange: (current: TFilters, value?: TValue) => TFilters
    options: (facets: EntityFacets) => EntityFilterOption<TValue>[]
  }

export type EntitiesMultiSelectFilter<TValue, TFilters> = EntitiesFilterBase & {
  type: EntityFilterType.MultipleSelect
  selectedValues: (current: TFilters) => TValue[] | undefined
  onChange: (current: TFilters, values?: TValue[]) => TFilters
  options: (facets: EntityFacets) => EntityFilterOption<TValue>[]
}

export type EntitiesFilter<TValue, TFilters> =
  | EntitiesTextFilter<TFilters>
  | EntitiesSingleSelectFilter<TValue, TFilters>
  | EntitiesMultiSelectFilter<TValue, TFilters>
  | EntitiesDateFilter<TFilters>
