import { createContext, useContext } from "react"
import { EntityActions, EntityFilteredResultPaging } from "../types"
import { EntityClientFilters, EntityFacets } from "../types/filters"
import { EntityManagerOptions } from "../types/options"
import { EntityPaging } from "../types/paging"
import { EntitiesSortingValue } from "../types/sorting"
import { EntityUiState } from "../types/ui"

export type EntitiesDataResults<TData> = {
  filteredSearch: boolean
  items: TData[] | undefined
  paging?: EntityFilteredResultPaging
  facets?: EntityFacets
}

export enum EntityDataRefreshType {
  Create = "create",
  Update = "update",
  Delete = "delete",
}

export type EntityController<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting,
  TData
> = {
  fetchResults?: EntitiesDataResults<TData>
  actions: EntityActions<TEntity, TCreate, TUpdate>
  data: TData[] | undefined
  filteredData: TData[] | undefined
  ui: EntityUiState<TEntity>
  empty: boolean
  emptyDataSource: boolean
  firstFetchCompleted: boolean
  clientFilters: EntityClientFilters
  filters?: TFilters
  sorting?: EntitiesSortingValue<TSorting>
  paging?: EntityPaging
  updateFilters: (filters: TFilters) => void
  updateSorting: (sorting: EntitiesSortingValue<TSorting>) => void
  updateQuery: (query: string) => void
  updatePaging: (paging: EntityPaging) => void
  query?: string
  options?: EntityManagerOptions
}

export const EntityControllerContext = createContext<
  EntityController<unknown, unknown, unknown, unknown, unknown, unknown>
>({} as any)

export interface EntityControllerProviderProps<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting,
  TData
> {
  children: any
  value: EntityController<TEntity, TFilters, TCreate, TUpdate, TSorting, TData>
}

export const EntityControllerProvider = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting,
  TData
>({
  children,
  value,
}: EntityControllerProviderProps<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting,
  TData
>) => {
  return (
    <EntityControllerContext.Provider
      value={
        value as EntityController<
          unknown,
          unknown,
          unknown,
          unknown,
          unknown,
          unknown
        >
      }
    >
      {children}
    </EntityControllerContext.Provider>
  )
}

export const useEntityController = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting,
  TData
>() =>
  useContext(EntityControllerContext) as EntityController<
    TEntity,
    TFilters,
    TCreate,
    TUpdate,
    TSorting,
    TData
  >
