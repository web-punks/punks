import { createContext, useContext } from "react"
import { EntityMetadata, EntityOperations, EntitySelectors } from "../types"
import { EntityClientFiltersMatcher } from "../types/filters"
import { EntityForms } from "../types/forms"
import { EntityModals } from "../types/modals"
import { EntityManagerOptions } from "../types/options"

export interface EntityManagerDefinition<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
  forms?: EntityForms<TEntity, TCreate, TUpdate>
  filters?: TFilters
  clientFiltersMatcher?: EntityClientFiltersMatcher<TEntity>
  modals: EntityModals<TEntity>
  selectors: EntitySelectors<TEntity>
  options?: EntityManagerOptions
  metadata?: EntityMetadata
}

export const EntityManagerDefinitionContext = createContext<
  EntityManagerDefinition<unknown, unknown, unknown, unknown, unknown>
>({} as any)

export interface EntityManagerDefinitionProviderProps<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  children: any
  value: EntityManagerDefinition<TEntity, TFilters, TCreate, TUpdate, TSorting>
}

export const EntityManagerDefinitionProvider = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  children,
  value,
}: EntityManagerDefinitionProviderProps<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>) => {
  return (
    <EntityManagerDefinitionContext.Provider
      value={
        value as EntityManagerDefinition<
          unknown,
          unknown,
          unknown,
          unknown,
          unknown
        >
      }
    >
      {children}
    </EntityManagerDefinitionContext.Provider>
  )
}

export const useEntityManagerDefinition = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>() =>
  useContext(EntityManagerDefinitionContext) as EntityManagerDefinition<
    TEntity,
    TFilters,
    TCreate,
    TUpdate,
    TSorting
  >
