import { deepMerge } from "@punks/ui-core"

export const mergeFilters = <TFilters>(
  filter: TFilters,
  dataFilter?: Partial<TFilters>
) => {
  return deepMerge<TFilters>(filter, dataFilter)
}

export const isFilterEmpty = (filter?: any) => {
  return (
    !filter ||
    Object.keys(filter).every((key) => (filter as any)?.[key] === undefined)
  )
}
