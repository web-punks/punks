import {
  TableColumnSorting,
  TableSelectionMode,
  TableRowControlType,
} from "@punks/ui-components"
import { EntitiesFilter } from "./filters"

export type EntityTableSelection<TEntity> = {
  onSelectionChange: (selected: TEntity[]) => void
  mode: TableSelectionMode
}

export type EntityRowActions<TEntity> = {
  onClick?: (record: TEntity) => void
  onDoubleClick?: (record: TEntity) => void
}

export type EntityTableColumnSorting<T> = TableColumnSorting<unknown, T>

export type EntityTableColumnType<TEntity, TFilters, FilterType, SortingType> =
  {
    title: React.ReactNode
    key: string
    span?: number
    minWidth?: string | number
    render: (record: TEntity, index: number) => React.ReactNode
    hidden?: boolean
    filter?: EntitiesFilter<FilterType, TFilters>
    sorting?: EntityTableColumnSorting<SortingType>
  }

export type EntityTableRowControlType<TEntity> = TableRowControlType<TEntity>
