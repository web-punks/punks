export enum MenuControlType {
  Create = "create",
  Import = "upload",
  Export = "export",
  Custom = "custom",
}

export type EntityMenuControlBase<TEntity> = {
  enabled?: boolean | ((item: TEntity) => boolean)
}

export type StandardMenuControl<TEntity> = EntityMenuControlBase<TEntity> & {
  type: MenuControlType.Create | MenuControlType.Import | MenuControlType.Export
}

export type CustomMenuCta = {
  label: string
}

export type CustomMenuControl<TEntity> = EntityMenuControlBase<TEntity> & {
  type: MenuControlType.Custom
  action: () => void | Promise<void>
  cta: CustomMenuCta
}

export type EntityMenuControl<TEntity> =
  | StandardMenuControl<TEntity>
  | CustomMenuControl<TEntity>

export type EntityMenuControls<TEntity> = EntityMenuControl<TEntity>[]

export type EntityMenuControlStyles = {
  width?: number
}

export type EntityPrimaryMenuControls<TEntity> = {
  controls: EntityMenuControls<TEntity>
  cta?: string
  styles?: EntityMenuControlStyles
}

export type EntitySecondaryMenuControls<TEntity> = {
  controls: EntityMenuControls<TEntity>
}

export type EntityMainMenuControls<TEntity> = {
  primary?: EntityPrimaryMenuControls<TEntity>
  secondary?: EntitySecondaryMenuControls<TEntity>
}
