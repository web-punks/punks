import { useOperation } from "@punks/ui-core"
import { EntityOperations } from "../types"
import { uiRegistry } from "@punks/ui-components"
import { EntityUpdateInput } from "../types/operations"
import { useEntityManagerLabel } from "../../../../modules"

export interface EntityUpdateActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
}

export const useEntityUpdateAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
}: EntityUpdateActionInput<TEntity, TFilters, TCreate, TUpdate, TSorting>) => {
  const { getLabel } = useEntityManagerLabel()
  const { invoke: updateItem, state: updateState } = useOperation<
    TEntity | void,
    EntityUpdateInput<TUpdate>
  >({
    operation: async (input: EntityUpdateInput<TUpdate>) => {
      if (!operations.update) {
        throw new Error("Update item is not defined")
      }
      return await operations.update?.action(input)
    },
    onError: (error) =>
      operations.update?.onError
        ? operations.update?.onError(error)
        : uiRegistry().showAlert({
            content: getLabel("operations:update:messages:genericError"),
            type: "error",
          }),
    onSuccess: (result) => {
      uiRegistry().showAlert({
        content: getLabel("operations:update:messages:success"),
        type: "success",
      })
      operations.update?.callback?.(result)
    },
  })

  return {
    updateItem,
    updateState,
  }
}
