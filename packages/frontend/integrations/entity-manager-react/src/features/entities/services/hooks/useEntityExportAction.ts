import { useOperation } from "@punks/ui-core"
import { EntityOperations } from "../types"
import { EntityExportInput } from "../types/operations"

export interface EntityExportActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
}

export const useEntityExportAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
}: EntityExportActionInput<TEntity, TFilters, TCreate, TUpdate, TSorting>) => {
  const { invoke: exportItem, state: itemExportState } = useOperation({
    operation: async (input: EntityExportInput<TEntity>) => {
      if (!operations.exportItem) {
        throw new Error("Export item is not defined")
      }
      return await operations.exportItem?.action(input)
    },
    onError: (error) => operations.exportItem?.onError?.(error),
    onSuccess: () => {
      operations.exportItem?.callback?.()
    },
  })

  return {
    exportItem,
    itemExportState,
  }
}
