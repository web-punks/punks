import {
  EntityCreateFormComponent,
  EntityDetailsFormComponent,
  EntityUpdateFormComponent,
} from "../../../authentication/abstractions"

export type EntityForms<TEntity, TCreate, TUpdate> = {
  create?: EntityCreateFormComponent<TCreate, TEntity, any>
  createAdditionalProps?: any
  update?: EntityUpdateFormComponent<TUpdate, TEntity, any>
  updateAdditionalProps?: any
  details?: EntityDetailsFormComponent<TEntity, any>
  detailsAdditionalProps?: any
}
