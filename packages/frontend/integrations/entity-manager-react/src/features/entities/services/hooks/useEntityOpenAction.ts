import { useState } from "react"
import { EntityOperations } from "../types"
import {
  LoadableStateWithInput,
  triggerCompleted,
  triggerFailed,
  triggered,
} from "@punks/ui-core"
import { EntityCancelOptions } from "../../../authentication/abstractions"
import { EntityGetInput } from "../types/operations"

export interface EntityOpenActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
  onItemUpdated: () => Promise<void>
}

export const useEntityOpenAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
  onItemUpdated,
}: EntityOpenActionInput<TEntity, TFilters, TCreate, TUpdate, TSorting>) => {
  const [openItemState, setOpenItemState] =
    useState<LoadableStateWithInput<TEntity, EntityGetInput<TEntity>, any>>()

  const selectItemToOpen = async (item: TEntity) => {
    if (!operations.get) {
      setOpenItemState(
        triggerCompleted({
          data: item,
          input: {
            item,
          },
        })
      )
      return
    }

    try {
      setOpenItemState(triggered({ item }))
      const entity = await operations.get?.action({ item })
      setOpenItemState(
        triggerCompleted({
          data: entity as TEntity,
          input: { item },
        })
      )
    } catch (e) {
      console.error("Error fetching item", item)
      setOpenItemState(
        triggerFailed({
          input: { item },
        })
      )
    }
  }

  const closeDetailsModal = async (options?: EntityCancelOptions) => {
    setOpenItemState(undefined)
    if (options?.refetch) {
      await onItemUpdated()
    }
  }

  return {
    closeDetailsModal,
    selectItemToOpen,
    openItemState,
  }
}
