import { EntityParseResult } from "../../../../__test__/infrastructure/api/backend"
import { EntityModalOptions } from "../../types/modals"
import { EntityTableColumnType } from "./tables"

export type EntityModal = {}

export type EntityVersionsTableDefinition<TEntity> = {
  columns: EntityTableColumnType<TEntity, unknown, unknown, unknown>[]
}

export type EntityVersionsModal<TEntity> = EntityModal & {
  table: EntityVersionsTableDefinition<TEntity>
}

export type EntitiesImportTableLabels = {
  errorCodes: Record<string, string>
}

export type EntitiesImportTableDefinition = {
  columns: EntityTableColumnType<EntityParseResult, unknown, unknown, unknown>[]
  labels: EntitiesImportTableLabels
  options?: {
    pageSize?: number
    skipInvalidRows?: boolean
  }
}

export type EntitiesImportModal = EntityModal & {
  table: EntitiesImportTableDefinition
}

export type EntityModals<TEntity> = {
  childCreateModalOptions?: EntityModalOptions
  createModalOptions?: EntityModalOptions
  detailsModalOptions?: EntityModalOptions
  updateModalOptions?: EntityModalOptions
  import?: EntitiesImportModal
  importModalOptions?: EntityModalOptions
  viewJsonModalOptions?: EntityModalOptions
  versions?: EntityVersionsModal<TEntity>
  versionsModalOptions?: EntityModalOptions
}
