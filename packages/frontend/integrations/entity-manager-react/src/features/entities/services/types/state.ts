export type EntityManagerInitialState<TEntity> = {
  selectedItemDetails?: TEntity
  selectedItemToEdit?: TEntity
}
