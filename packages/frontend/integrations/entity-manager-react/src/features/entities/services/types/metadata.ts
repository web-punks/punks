export type EntityMetadata = {
  name: string
  pluralName?: string
}
