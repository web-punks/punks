import { EntityDataRefreshType } from "../context/controller"
import { EntityPaging } from "./paging"
import { EntitiesSortingValue, EntitySortingDirection } from "./sorting"
import { EntityTraverseOptions } from "./traverse"

export type EntityDataRefreshItemContext = {
  id?: string
  parentId?: string
  operationType: EntityDataRefreshType
}

export type EntitiesTraverseFetchParams = {
  rootOnly?: boolean
  parentId?: string
  collapseNodeId?: string
}

export type EntitiesFetchParams = {
  context?: EntityDataRefreshItemContext
  traverse?: EntitiesTraverseFetchParams
}

export type EntityExportResult = {
  downloadUrl: string
}

export type EntitySampleDownloadResult = {
  downloadUrl: string
}

export type EntityOperation<TInput, TResult> = {
  action: (input: TInput) => Promise<TResult>
  callback?: (result: TResult) => void
  onError?: (error: Error) => void
}
export type EntityQueryOperation<TInput, TResult> = {
  action: (input: TInput) => Promise<TResult>
  callback?: (result: TResult) => void
  onError?: (error: Error) => void
  key?: string[]
  autoTrigger?: boolean
}

export type EntityFetchInput<TFilters, TSorting> = {
  query?: string
  filters?: TFilters
  baseFilter?: Partial<TFilters>
  sorting?: EntitiesSortingValue<TSorting>
  paging?: EntityPaging
  traverse?: EntityTraverseOptions
  context?: EntitiesFetchParams
}

export type EntityParentData = {
  id: string
  item: any
}

export type EntityCreateInput<TCreate> = {
  data: TCreate
  parent?: EntityParentData
}

export type EntityUpdateInput<TUpdate> = {
  id: string
  data: TUpdate
}

export type EntityDeleteInput<TEntity> = {
  item: TEntity
}

export type EntityGetInput<TEntity> = {
  item: TEntity
}

export type EntityExportInput<TEntity> = {
  item: TEntity
}

export type EntitiesImportInput = {
  file: File
}

export type EntitiesParseInput = {
  file: File
}

export type EntityVersionsSearchInput = {
  id: string
  sorting: {
    direction: EntitySortingDirection
  }
}
