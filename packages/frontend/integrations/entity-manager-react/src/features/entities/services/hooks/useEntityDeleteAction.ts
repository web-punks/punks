import { useOperation } from "@punks/ui-core"
import { EntityOperations } from "../types"
import { uiRegistry } from "@punks/ui-components"
import { EntityDeleteInput } from "../types/operations"
import { useEntityManagerLabel } from "../../../../modules"

export interface EntityDeleteActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
}

export const useEntityDeleteAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
}: EntityDeleteActionInput<TEntity, TFilters, TCreate, TUpdate, TSorting>) => {
  const { getLabel } = useEntityManagerLabel()
  const { invoke: deleteItem, state: deleteState } = useOperation<
    void,
    EntityDeleteInput<TEntity>
  >({
    operation: async (input: EntityDeleteInput<TEntity>) => {
      if (!operations.delete) {
        throw new Error("Delete item is not defined")
      }
      return await operations.delete?.action(input)
    },
    onError: (error) =>
      operations.delete?.onError
        ? operations.delete.onError(error)
        : uiRegistry().showAlert({
            type: "error",
            content: getLabel("operations:delete:messages:genericError"),
          }),
    onSuccess: () => {
      uiRegistry().showAlert({
        type: "success",
        content: getLabel("operations:delete:messages:success"),
      })
      operations.delete?.callback?.()
    },
  })

  return {
    deleteItem,
    deleteState,
  }
}
