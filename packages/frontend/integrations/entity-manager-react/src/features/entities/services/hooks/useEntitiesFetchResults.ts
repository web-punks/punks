import { EntityFilteredResult, EntityFilteredResultPaging } from "../types"
import { EntityFacets } from "../types/filters"

export interface EntitiesFetchActionInput<TEntity> {
  data: TEntity[] | EntityFilteredResult<TEntity>
}

export type EntitiesFetchResults<TEntity> = {
  filteredSearch: boolean
  items: TEntity[] | undefined
  paging?: EntityFilteredResultPaging
  facets?: EntityFacets
}

export const useEntitiesFetchResults = <TEntity>({
  data,
}: EntitiesFetchActionInput<TEntity>):
  | EntitiesFetchResults<TEntity>
  | undefined => {
  if (!data) {
    return undefined
  }

  if (Array.isArray(data)) {
    return {
      filteredSearch: false,
      items: data,
    }
  }
  if (data && "items" in data) {
    return {
      filteredSearch: true,
      items: data.items,
      facets: data.facets,
      paging: data.paging,
    }
  }

  return undefined
}
