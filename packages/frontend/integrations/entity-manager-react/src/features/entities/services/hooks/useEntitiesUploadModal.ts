import { useState } from "react"

export const useEntitiesUploadModal = () => {
  const [uploadModalOpen, setUploadModalOpen] = useState(false)

  return {
    uploadModalOpen,
    setUploadModalOpen,
  }
}
