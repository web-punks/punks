import { useOperation, openUrlInNewWindow } from "@punks/ui-core"
import { EntityOperations } from "../types"
import { uiRegistry } from "@punks/ui-components"
import { useEntityManagerLabel } from "../../../../modules"

export interface EntitySampleDownloadAction<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
}

export const useEntitySampleDownloadAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
}: EntitySampleDownloadAction<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>) => {
  const { getLabel } = useEntityManagerLabel()
  const { invoke, state: sampleDownloadState } = useOperation({
    operation: async () => {
      if (!operations.sampleDownload) {
        throw new Error("Sample download is not defined")
      }
      return await operations.sampleDownload?.action()
    },
    onError: (error) => operations.sampleDownload?.onError?.(error),
    onSuccess: (result) => {
      if (result?.downloadUrl) {
        openUrlInNewWindow(result.downloadUrl)
      }
      operations.sampleDownload?.callback?.(result)
    },
  })

  const sampleDownload = async () => {
    await uiRegistry().withNotification(
      async () => {
        await invoke()
      },
      {
        success: getLabel("operations:sampleDownload:messages:success"),
        error: getLabel("operations:sampleDownload:messages:genericError"),
        pending: getLabel("operations:sampleDownload:messages:inProgress"),
      }
    )
  }

  return {
    sampleDownload,
    sampleDownloadState,
  }
}
