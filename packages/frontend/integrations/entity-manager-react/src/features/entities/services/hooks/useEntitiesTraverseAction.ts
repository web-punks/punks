import { useOperation } from "@punks/ui-core"
import {
  EntityFilteredResult,
  EntityOperations,
  EntitySelectors,
} from "../types"
import { EntityPaging } from "../types/paging"
import { EntitiesSortingValue } from "../types/sorting"
import { TreeNode, uiRegistry } from "@punks/ui-components"
import { useEntityManagerLabel } from "../../../../modules"
import { mergeFilters } from "../filters/utils"
import { EntityTraverseOptions } from "../types/traverse"
import { useRef, useState } from "react"
import { TreeManager } from "../tree/treeManager"

export type EntitiesTraverseActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> = {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
  filters?: TFilters
  dataFilter?: Partial<TFilters>
  sorting?: EntitiesSortingValue<TSorting>
  paging?: EntityPaging
  query?: string
  traverse?: EntityTraverseOptions
  selectors: EntitySelectors<TEntity>
}

export type RootEntitiesTraverseInput = {
  reloadTree?: boolean
}

export type ChildEntitiesTraverseInput = {
  parentId: string
}

export type NodeEntityTraverseInput = {
  parentId?: string
  nodeId: string
}

export const useEntitiesTraverseAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
  filters,
  dataFilter,
  paging,
  sorting,
  query,
  selectors,
}: EntitiesTraverseActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>) => {
  const { getLabel } = useEntityManagerLabel()
  const treeState = useRef(
    new TreeManager<TEntity>({
      getParentId: (node) => {
        if (!selectors.getParentId) {
          throw new Error("No getParentId selector configured")
        }
        return selectors.getParentId(node)
      },
    })
  )
  const [treeNodes, setTreeNodes] = useState<TreeNode<TEntity>[]>([])

  const updateTreeState = () => {
    setTreeNodes([...treeState.current.getRoot()])
  }

  const { invoke: fetchRoot, state: fetchRootState } = useOperation<
    EntityFilteredResult<TEntity>,
    void
  >({
    operation: () =>
      operations.fetch?.action({
        filters: mergeFilters(filters, dataFilter),
        // paging,
        sorting,
        query,
        traverse: {
          rootOnly: true,
        },
      }) as Promise<EntityFilteredResult<TEntity>>,
    onError: () =>
      uiRegistry().showAlert({
        type: "error",
        content: getLabel("operations:traverse:messages:genericError"),
      }),
    onSuccess: () => {},
  })

  const { invoke: fetchChildren, state: fetchChildrenState } = useOperation<
    EntityFilteredResult<TEntity>,
    ChildEntitiesTraverseInput
  >({
    operation: ({ parentId }: ChildEntitiesTraverseInput) =>
      operations.fetch?.action({
        filters: mergeFilters(filters, dataFilter),
        // paging,
        sorting,
        query,
        traverse: {
          parentIds: [parentId],
        },
      }) as Promise<EntityFilteredResult<TEntity>>,
    onError: () =>
      uiRegistry().showAlert({
        type: "error",
        content: getLabel("operations:traverse:messages:genericError"),
      }),
    onSuccess: () => {},
  })

  const { invoke: fetchNode, state: fetchNodeState } = useOperation<
    EntityFilteredResult<TEntity>,
    NodeEntityTraverseInput
  >({
    operation: ({ nodeId }: NodeEntityTraverseInput) =>
      operations.fetch?.action({
        filters: mergeFilters(filters, dataFilter),
        // paging,
        sorting,
        query,
        traverse: {
          nodeIds: [nodeId],
        },
      }) as Promise<EntityFilteredResult<TEntity>>,
    onError: () =>
      uiRegistry().showAlert({
        type: "error",
        content: getLabel("operations:traverse:messages:genericError"),
      }),
    onSuccess: () => {},
  })

  const fetchRootItems = async (input: RootEntitiesTraverseInput) => {
    if (input.reloadTree) {
      treeState.current.clear()
      updateTreeState()
    }

    const result = await fetchRoot()
    if (!result) {
      throw new Error("No result")
    }
    treeState.current.setRootNodes(
      result.items.map((x) => ({
        id: selectors.getId(x),
        expanded: false,
        record: x,
        childrenLoading: false,
        childrenCount: result.childrenMap?.[selectors.getId(x)]?.count ?? 0,
      }))
    )
    updateTreeState()

    return result
  }

  const traverseItems = async (input: ChildEntitiesTraverseInput) => {
    treeState.current.setParentNodeState(input.parentId, {
      childrenLoading: true,
    })
    updateTreeState()

    try {
      const result = await fetchChildren(input)
      if (!result) {
        throw new Error("No result")
      }
      treeState.current.setChildNodes(
        input.parentId,
        result.items.map((x) => ({
          id: selectors.getId(x),
          expanded: false,
          record: x,
          childrenLoading: false,
          childrenCount: result.childrenMap?.[selectors.getId(x)]?.count ?? 0,
        }))
      )
    } finally {
      treeState.current.setParentNodeState(input.parentId, {
        childrenLoading: false,
        expanded: true,
      })
      updateTreeState()
    }
  }

  const refreshNode = async (input: NodeEntityTraverseInput) => {
    const result = await fetchNode(input)
    if (!result) {
      throw new Error("No result")
    }
    if (input.parentId) {
      treeState.current.updateChildNodes(
        input.parentId,
        result.items.map((x) => ({
          id: selectors.getId(x),
          expanded: false,
          record: x,
          childrenLoading: false,
          childrenCount: result.childrenMap?.[selectors.getId(x)]?.count ?? 0,
        }))
      )
    } else {
      treeState.current.updateRootNodes(
        result.items.map((x) => ({
          id: selectors.getId(x),
          expanded: false,
          record: x,
          childrenLoading: false,
          childrenCount: result.childrenMap?.[selectors.getId(x)]?.count ?? 0,
        }))
      )
    }
    updateTreeState()
  }

  const collapseNode = async (nodeId: string) => {
    treeState.current.setParentNodeState(nodeId, {
      expanded: false,
    })
    updateTreeState()
  }

  return {
    fetchRootItems,
    traverseItems,
    refreshNode,
    fetchRootState,
    fetchChildrenState,
    fetchNodeState,
    treeNodes,
    getParentNode: treeState.current.getParentNode,
    collapseNode,
  }
}
