import { uiRegistry } from "@punks/ui-components"
import { EntityOperations } from "../types"
import { useOperation } from "@punks/ui-core"
import { EntityCreateInput } from "../types/operations"
import { useEntityManagerLabel } from "../../../../modules"

export interface EntityCreateAction<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
}

export const useEntityCreateAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
}: EntityCreateAction<TEntity, TFilters, TCreate, TUpdate, TSorting>) => {
  const { getLabel } = useEntityManagerLabel()
  const { invoke: createItem, state: createState } = useOperation<
    TEntity | void,
    EntityCreateInput<TCreate>
  >({
    operation: async (input: EntityCreateInput<TCreate>) => {
      if (!operations.create) {
        throw new Error("Create item is not defined")
      }
      return await operations.create?.action(input)
    },
    onError: (error) =>
      operations.create?.onError
        ? operations.create?.onError(error)
        : uiRegistry().showAlert({
            type: "error",
            content: getLabel("operations:create:messages:genericError"),
          }),
    onSuccess: (result) => {
      uiRegistry().showAlert({
        type: "success",
        content: getLabel("operations:create:messages:success"),
      })
      operations.create?.callback?.(result)
    },
  })

  return {
    createItem,
    createState,
  }
}
