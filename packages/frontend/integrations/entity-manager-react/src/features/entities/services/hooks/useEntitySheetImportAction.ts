import { useOperation } from "@punks/ui-core"
import { EntityOperations } from "../types"
import { EntitiesImportInput } from "../types/operations"
import { uiRegistry } from "@punks/ui-components"
import { useEntityManagerLabel } from "../../../../modules"

export interface EntitySheetImportActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
}

export const useEntitySheetImportAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
}: EntitySheetImportActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>) => {
  const { getLabel } = useEntityManagerLabel()
  const { invoke, state: importState } = useOperation({
    operation: async (input: EntitiesImportInput) => {
      if (!operations.import) {
        throw new Error("Import sheets is not defined")
      }
      return await operations.import?.action(input)
    },
    onError: (error) => operations.import?.onError?.(error),
    onSuccess: () => {
      operations.import?.callback?.()
    },
  })

  const importSheet = async (
    input: EntitiesImportInput,
    callback?: () => Promise<void>
  ) => {
    await uiRegistry().withNotification(
      async () => {
        await invoke(input)
        await callback?.()
      },
      {
        success: getLabel("operations:import:messages:success"),
        error: getLabel("operations:import:messages:genericError"),
        pending: getLabel("operations:import:messages:inProgress"),
      }
    )
  }

  return {
    importSheet,
    importState,
  }
}
