import { useOperation } from "@punks/ui-core"
import { EntityFilteredResult, EntityOperations } from "../types"
import { EntityPaging } from "../types/paging"
import { EntitiesSortingValue } from "../types/sorting"
import { uiRegistry } from "@punks/ui-components"
import { useEntityManagerLabel } from "../../../../modules"
import { mergeFilters } from "../filters/utils"
import { EntitiesFetchParams } from "../types/operations"

export interface EntitiesFetchActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
  filters?: TFilters
  dataFilter?: Partial<TFilters>
  sorting?: EntitiesSortingValue<TSorting>
  paging?: EntityPaging
  query?: string
}

export const useEntitiesFetchAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
  filters,
  dataFilter,
  paging,
  sorting,
  query,
}: EntitiesFetchActionInput<TEntity, TFilters, TCreate, TUpdate, TSorting>) => {
  const { getLabel } = useEntityManagerLabel()
  const { invoke: fetchItems, state: fetchState } = useOperation<
    TEntity[] | EntityFilteredResult<TEntity>,
    EntitiesFetchParams
  >({
    key: operations.fetch?.key,
    autoTrigger: operations?.fetch?.autoTrigger,
    operation: (context: EntitiesFetchParams) => {
      return operations.fetch?.action({
        filters: mergeFilters(filters, dataFilter),
        baseFilter: dataFilter,
        paging,
        sorting,
        query,
        context,
      })
    },
    onError: () =>
      uiRegistry().showAlert({
        type: "error",
        content: getLabel("operations:fetch:messages:genericError"),
      }),
    onSuccess: (result) => {
      operations.fetch?.callback?.(result)
    },
  })
  return {
    fetchItems,
    fetchState,
  }
}
