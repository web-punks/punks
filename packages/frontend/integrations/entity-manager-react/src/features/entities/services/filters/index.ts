import {
  EntitiesMultiSelectFilter,
  EntitiesSingleSelectFilter,
  EntityFilterType,
} from "../types/filters"

export type MultipleSelectFilterInput<TValue, TFilters> = Omit<
  EntitiesMultiSelectFilter<TValue, TFilters>,
  "type"
>

export const multipleSelectFilter = <TFilters, TValue>(
  input: MultipleSelectFilterInput<TValue, TFilters>
): EntitiesMultiSelectFilter<TValue, TFilters> => ({
  type: EntityFilterType.MultipleSelect,
  ...input,
})

export type SingleSelectFilterInput<TValue, TFilters> = Omit<
  EntitiesSingleSelectFilter<TValue, TFilters>,
  "type"
>

export const singleSelectFilter = <TFilters, TValue>(
  input: SingleSelectFilterInput<TValue, TFilters>
): EntitiesSingleSelectFilter<TValue, TFilters> => ({
  type: EntityFilterType.SingleSelect,
  ...input,
})
