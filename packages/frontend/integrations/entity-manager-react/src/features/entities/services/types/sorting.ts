export type EntitySortingDirection = "asc" | "desc"

export type EntitiesSortingValue<T> = {
  value: T
  direction: EntitySortingDirection
}
