import { TreeNode } from "@punks/ui-components"

export type TreeManagerSettings<T> = {
  getParentId: (node: T) => string | undefined
}

export class TreeManager<T> {
  private parentsMap: Record<string, string | null> = {}
  private map: Record<string, TreeNode<T>> = {}
  private root: TreeNode<T>[] = []

  constructor(private readonly settings: TreeManagerSettings<T>) {}

  getRoot() {
    console.log("TreeManager.getRoot", this.root)
    return this.root
  }

  clear() {
    this.root = []
    this.clearMap()
    console.log("TreeManager.clear")
  }

  getParentNode(nodeId: string) {
    const parentId = this.parentsMap[nodeId]
    if (!parentId) {
      return null
    }
    return this.getNode(parentId)
  }

  setRootNodes(nodes: TreeNode<T>[]) {
    this.root = nodes
    this.clearMap()
    this.addNodesToMap(nodes)
    console.log("TreeManager.setRootNodes", nodes)
  }

  setChildNodes(parentId: string, nodes: TreeNode<T>[]) {
    const parent = this.getNodeOrFail(parentId)
    this.removeNodesFromMap(parent.children ?? [])
    this.setNodeChildren(parent, nodes)
    this.addNodesToMap(nodes)
    console.log("TreeManager.setChildNodes", parentId, nodes)
  }

  addChildNodes(parentId: string, nodes: TreeNode<T>[]) {
    const parent = this.getNodeOrFail(parentId)
    this.setNodeChildren(parent, [...(parent.children ?? []), ...nodes])
    this.addNodesToMap(nodes)
    console.log("TreeManager.addChildNodes", parentId, nodes)
  }

  addRootNodes(nodes: TreeNode<T>[]) {
    this.root = [...this.root, ...nodes]
    this.addNodesToMap(nodes)
    console.log("TreeManager.addRootNodes", nodes)
  }

  removeChildNodes(parentId: string, nodes: TreeNode<T>[]) {
    const parent = this.getNodeOrFail(parentId)
    this.setNodeChildren(
      parent,
      parent.children?.filter(
        (child) => !nodes.find((node) => node.id === child.id)
      ) ?? []
    )
    this.removeNodesFromMap(nodes)
    console.log("TreeManager.removeChildNodes", parentId, nodes)
  }

  removeRootNodes(nodes: TreeNode<T>[]) {
    this.root = this.root.filter(
      (rootNode) => !nodes.find((node) => node.id === rootNode.id)
    )
    this.removeNodesFromMap(nodes)
    console.log("TreeManager.removeRootNodes", nodes)
  }

  updateChildNodes(parentId: string, nodes: TreeNode<T>[]) {
    const parent = this.getNodeOrFail(parentId)
    nodes.forEach((node) => {
      const index = parent.children?.findIndex((child) => child.id === node.id)
      if (index !== undefined && index > -1) {
        if (!parent.children) {
          return
        }
        parent.children[index] = node
      }
    })
    this.addNodesToMap(nodes)
    console.log("TreeManager.updateChildNodes", parentId, nodes)
  }

  updateRootNodes(nodes: TreeNode<T>[]) {
    nodes.forEach((node) => {
      const index = this.root.findIndex((child) => child.id === node.id)
      if (index !== undefined && index > -1) {
        this.root[index] = node
      }
    })
    this.addNodesToMap(nodes)
    console.log("TreeManager.updateRootNodes", nodes)
  }

  setParentNodeState(
    parentId: string,
    input: {
      childrenLoading?: boolean
      expanded?: boolean
    }
  ) {
    const parent = this.getNodeOrFail(parentId)

    if (input.childrenLoading !== undefined) {
      parent.childrenLoading = input.childrenLoading
    }
    if (input.expanded !== undefined) {
      parent.expanded = input.expanded
    }

    console.log("TreeManager.setParentNodeState", parentId, input)
  }

  private setNodeChildren(node: TreeNode<T>, children: TreeNode<T>[]) {
    node.children = children
    node.childrenCount = children.length
  }

  private getNodeOrFail(nodeId: string) {
    const node = this.getNode(nodeId)
    if (!node) {
      throw new Error(`Node with id ${nodeId} not found`)
    }
    return node
  }

  private addNodesToMap(nodes: TreeNode<T>[]) {
    nodes.forEach((node) => this.addNodeToMap(node))
  }

  private addNodeToMap(node: TreeNode<T>) {
    this.map[node.id] = node
    this.parentsMap[node.id] = this.settings.getParentId(node.record) ?? null
  }

  private removeNodesFromMap(nodes: TreeNode<T>[]) {
    nodes.forEach((node) => this.removeNodeFromMap(node.id))
  }

  private removeNodeFromMap(nodeId: string) {
    delete this.map[nodeId]
    delete this.parentsMap[nodeId]
  }

  private clearMap() {
    this.map = {}
  }

  private getNode(id: string) {
    return this.map[id]
  }
}
