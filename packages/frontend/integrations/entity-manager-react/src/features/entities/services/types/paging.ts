export type EntityPaging = {
  pageSize: number
  pageIndex?: number
  cursor?: number
}
