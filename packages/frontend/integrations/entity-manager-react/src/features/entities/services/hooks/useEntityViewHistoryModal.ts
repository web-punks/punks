import { useState } from "react"
import { EntityItemModalState } from "../types/ui"

export const useEntityViewHistoryModal = <TEntity>() => {
  const [state, setState] = useState<EntityItemModalState<TEntity>>({
    open: false,
    item: undefined,
  })

  return {
    state,
    actions: {
      open: (item: TEntity) => setState({ open: true, item }),
      close: () => setState({ open: false }),
    },
  }
}
