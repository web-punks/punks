import { LoadableStateWithInput, OperationState } from "@punks/ui-core"
import { EntityFacets } from "./filters"
import {
  EntityExportResult,
  EntityFetchInput,
  EntityOperation,
  EntityCreateInput,
  EntityDeleteInput,
  EntityExportInput,
  EntityGetInput,
  EntityUpdateInput,
  EntitiesImportInput,
  EntitySampleDownloadResult,
  EntityVersionsSearchInput,
  EntitiesFetchParams,
  EntitiesParseInput,
  EntityQueryOperation,
} from "./operations"
import { EntityCancelOptions } from "../../../authentication/abstractions"

export enum EntitiesFetchMode {
  List = "list",
  Tree = "tree",
}

export type EntityCallbacks<TEntity, TCreate, TUpdate> = {
  onFetchCompleted?: (
    results: TEntity[] | EntityFilteredResult<TEntity>,
    context: EntitiesFetchParams
  ) => void
}

export type EntitySelectors<TEntity> = {
  getDisplayName: (entity: TEntity) => string
  getId: (entity: TEntity) => string
  getParentId?: (entity: TEntity) => string | undefined
}

export type EntityFilteredResultPaging = {
  pageIndex: number
  pageSize: number
  totPages: number
  totItems: number
}

export type EntityChildrenMap = Record<
  string,
  {
    count: number
  }
>

export type EntityFilteredResult<TEntity> = {
  items: TEntity[]
  paging?: EntityFilteredResultPaging
  facets?: EntityFacets
  childrenMap?: EntityChildrenMap
}

export type EntityVersionsResult<TEntity> = {
  items: TEntity[]
  paging?: EntityFilteredResultPaging
}

export type EntityParseValidationColumn = {
  name: string
  key: string
}

export type EntityParseValidationError = {
  errorCode: string
  column: EntityParseValidationColumn
}

export type EntityParseStatus = {
  isValid: boolean
  validationErrors: EntityParseValidationError[]
}

export type EntityParseResult = {
  rowIndex: number
  item: any
  status: EntityParseStatus
}

export type EntitiesParseResult = {
  entries: EntityParseResult[]
}

export type EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting> = {
  fetch: EntityQueryOperation<
    EntityFetchInput<TFilters, TSorting>,
    TEntity[] | EntityFilteredResult<TEntity>
  >
  get?: EntityOperation<EntityGetInput<TEntity>, TEntity>
  create?: EntityOperation<EntityCreateInput<TCreate>, TEntity | void>
  update?: EntityOperation<EntityUpdateInput<TUpdate>, TEntity | void>
  delete?: EntityOperation<EntityDeleteInput<TEntity>, void>
  import?: EntityOperation<EntitiesImportInput, void>
  parse?: EntityOperation<EntitiesParseInput, EntitiesParseResult>
  export?: EntityOperation<void, EntityExportResult | void>
  exportItem?: EntityOperation<EntityExportInput<TEntity>, void>
  sampleDownload?: EntityOperation<void, EntitySampleDownloadResult | void>
  versions?: EntityOperation<
    EntityVersionsSearchInput,
    EntityVersionsResult<TEntity>
  >
}

export type FetchItemsState = {
  called: boolean
  completed: boolean
  loading: boolean
  error: boolean
}

export type EntityActions<TEntity, TCreate, TUpdate> = {
  fetchState: FetchItemsState
  itemUpdate: (input: EntityUpdateInput<TUpdate>) => Promise<void>
  updateState: OperationState<void | TEntity>
  itemCreate: (input: EntityCreateInput<TCreate>) => Promise<void>
  createState: OperationState<void | TEntity>
  itemsExport: () => Promise<void>
  exportState: OperationState<EntityExportResult | void>
  itemsImport: (input: EntitiesImportInput) => Promise<void>
  importState: OperationState<void>
  itemsParse: (input: EntitiesParseInput) => Promise<EntitiesParseResult>
  parseState: OperationState<EntitiesParseResult>
  sampleDownload: () => Promise<void>
  sampleDownloadState: OperationState<void | EntitySampleDownloadResult>
  deleteState: OperationState<void>
  itemDelete: (input: EntityDeleteInput<TEntity>) => Promise<void>
  openCreateModal: () => void
  closeCreateModal: (options?: EntityCancelOptions) => void
  openViewHistoryModal: (item: TEntity) => void
  openItemJsonModal: (item: TEntity) => void
  closeItemJsonModal: () => void
  closeViewHistoryModal: (options?: EntityCancelOptions) => void
  selectItemToEdit: (item: TEntity) => void
  selectItemParentItemForCreateChild: (item: TEntity) => void
  clearItemParentItemForCreateChild: (options?: EntityCancelOptions) => void
  closeEditModal: (options?: EntityCancelOptions) => void
  selectItemToOpen: (item: TEntity) => void
  closeDetailsModal: (options?: EntityCancelOptions) => void
  openItemState?: LoadableStateWithInput<TEntity, EntityGetInput<TEntity>, any>
  selectItemToDelete: (item: TEntity) => void
  itemToDelete?: TEntity
  itemParentForCreateChild?: TEntity
  editItemState?: LoadableStateWithInput<TEntity, TEntity, any>
  itemExport: (item: EntityExportInput<TEntity>) => void
  itemExportState?: LoadableStateWithInput<
    TEntity,
    EntityExportInput<TEntity>,
    any
  >
  clearItemToDelete: () => void
  openImportModal: () => void
  closeImportModal: (options?: EntityCancelOptions) => void
  fetchEntities: (params: EntitiesFetchParams) => Promise<void>
}
