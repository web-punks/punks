import { useEntitiesFetchAction } from "../../hooks/useEntitiesFetchAction"
import { useEntitiesTraverseAction } from "../../hooks/useEntitiesTraverseAction"
import {
  EntitiesFetchMode,
  EntitiesSortingValue,
  EntityOperations,
  EntityPaging,
  EntitySelectors,
} from "../../types"
import { EntitiesFetchParams } from "../../types/operations"

export const useEntityManagerFetchAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  mode,
  operations,
  selectors,
  state,
}: {
  mode: EntitiesFetchMode
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
  selectors: EntitySelectors<TEntity>
  state: {
    filters?: TFilters
    dataFilter?: Partial<TFilters>
    sorting?: EntitiesSortingValue<TSorting>
    paging?: EntityPaging
    query?: string
  }
}) => {
  const itemsList = useEntitiesFetchAction({
    operations,
    filters: state.filters,
    dataFilter: state.dataFilter,
    sorting: state.sorting,
    paging: state.paging,
    query: state.query,
  })

  const itemsTree = useEntitiesTraverseAction({
    operations,
    filters: state.filters,
    dataFilter: state.dataFilter,
    sorting: state.sorting,
    paging: state.paging,
    query: state.query,
    selectors,
  })

  const fetchListItems = async (params: EntitiesFetchParams) => {
    return await itemsList.fetchItems(params)
  }

  const fetchTreeItems = async (params: EntitiesFetchParams) => {
    // callbacks after creating or updating tree elements
    if (params.context?.parentId) {
      return await itemsTree.traverseItems({
        parentId: params.context.parentId,
      })
    }

    // call for expanding tree levels
    if (params.traverse?.parentId) {
      return await itemsTree.traverseItems({
        parentId: params.traverse.parentId,
      })
    }

    const collapseNodeId = params.traverse?.collapseNodeId
    if (collapseNodeId) {
      itemsTree.collapseNode(collapseNodeId)
      return undefined
    }

    return await itemsTree.fetchRootItems({
      reloadTree: true,
    })
  }

  const fetchItems = (params: EntitiesFetchParams) => {
    if (mode === EntitiesFetchMode.List) {
      return fetchListItems(params)
    }
    if (mode === EntitiesFetchMode.Tree) {
      return fetchTreeItems(params)
    }

    throw new Error(`Unknown fetch mode: ${mode}`)
  }

  if (mode === EntitiesFetchMode.List) {
    return {
      fetchItems,
      data: itemsList.fetchState.data,
      state: {
        error: !!itemsList.fetchState.error,
        loading: itemsList.fetchState.loading,
        called: itemsList.fetchState.called,
        completed: itemsList.fetchState.completed,
      },
    }
  }

  return {
    fetchItems,
    data: itemsTree.treeNodes,
    state: {
      error: !!itemsTree.fetchRootState.error,
      loading: itemsTree.fetchRootState.loading,
      called: itemsTree.fetchRootState.called,
      completed: itemsTree.fetchRootState.completed,
    },
  }
}
