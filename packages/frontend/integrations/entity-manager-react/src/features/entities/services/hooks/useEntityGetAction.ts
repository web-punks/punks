import { useOperation } from "@punks/ui-core"
import { EntityOperations } from "../types"
import { uiRegistry } from "@punks/ui-components"
import { EntityGetInput } from "../types/operations"
import { useEntityManagerLabel } from "../../../../modules"

export interface EntitiesGetActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
}

export const useEntityGetAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
}: EntitiesGetActionInput<TEntity, TFilters, TCreate, TUpdate, TSorting>) => {
  const { getLabel } = useEntityManagerLabel()
  const { invoke: getItem } = useOperation({
    operation: async (input: EntityGetInput<TEntity>) => {
      if (!operations.get) {
        throw new Error("Get item is not defined")
      }
      return await operations.get?.action(input)
    },
    onError: (error) =>
      operations.get?.onError
        ? operations.get?.onError(error)
        : uiRegistry().showAlert({
            type: "error",
            content: getLabel("operations:get:messages:genericError"),
          }),
    onSuccess: (result) => {
      operations.get?.callback?.(result)
    },
  })

  return {
    getItem,
  }
}
