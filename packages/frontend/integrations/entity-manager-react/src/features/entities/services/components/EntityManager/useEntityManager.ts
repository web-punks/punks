import { useEffect, useState } from "react"
import { useEntitiesFetchResults } from "../../hooks/useEntitiesFetchResults"
import { useEntityCreateAction } from "../../hooks/useEntityCreateAction"
import { useEntityDeleteAction } from "../../hooks/useEntityDeleteAction"
import { useEntityExportAction } from "../../hooks/useEntityExportAction"
import { useEntityGetAction } from "../../hooks/useEntityGetAction"
import { useEntityOpenAction } from "../../hooks/useEntityOpenAction"
import { useEntitySheetExportAction } from "../../hooks/useEntitySheetExportAction"
import { useEntitySheetImportAction } from "../../hooks/useEntitySheetImportAction"
import { useEntityUpdateAction } from "../../hooks/useEntityUpdateAction"
import { EntityActions, EntityOperations, EntitySelectors } from "../../types"
import {
  EntityClientFilters,
  EntityClientFiltersMatcher,
} from "../../types/filters"
import { EntityManagerOptions } from "../../types/options"
import { EntityPaging } from "../../types/paging"
import { EntitiesSortingValue } from "../../types/sorting"
import { EntityManagerInitialState } from "../../types/state"
import { EntityUiState } from "../../types/ui"
import { useDataFilter } from "../../../../../hooks"
import {
  LoadableStateWithInput,
  triggerCompleted,
  triggerFailed,
  triggered,
  useDebounce,
} from "@punks/ui-core"
import { uiRegistry } from "@punks/ui-components"
import { EntityCancelOptions } from "../../../../authentication/abstractions"
import { useEntitiesCreateModal } from "../../hooks/useEntitiesCreateModal"
import { useEntitiesUploadModal } from "../../hooks/useEntitiesUploadModal"
import {
  EntitiesImportInput,
  EntityCreateInput,
  EntitiesFetchParams,
  EntityDeleteInput,
  EntityExportInput,
  EntityUpdateInput,
  EntitiesParseInput,
} from "../../types/operations"
import { useEntitySampleDownloadAction } from "../../hooks/useEntitySampleDownloadAction"
import { useEntityViewHistoryModal } from "../../hooks/useEntityViewHistoryModal"
import { useEntityManagerLabel } from "../../../../../modules"
import { useEntityViewJsonModal } from "../../hooks/useEntityViewJsonModal"
import { EntityDataRefreshType } from "../../context/controller"
import {
  EntitiesFetchMode,
  EntitiesParseResult,
  EntityCallbacks,
  EntityFilteredResult,
} from "../../types/actions"
import { useEntityManagerFetchAction } from "./useEntityManagerFetchAction"
import { isFilterEmpty } from "../../filters/utils"
import { useEntitySheetParseAction } from "../../hooks/useEntitySheetParseAction"

export type EntityManagerInput<TEntity, TFilters, TCreate, TUpdate, TSorting> =
  {
    operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
    clientFiltersMatcher?: EntityClientFiltersMatcher<TEntity>
    dataFilter?: Partial<TFilters>
    initialFilters?: TFilters
    initialSorting?: EntitiesSortingValue<TSorting>
    initialPaging?: EntityPaging
    initialState?: EntityManagerInitialState<TEntity>
    initialize?: boolean
    options?: EntityManagerOptions
    selectors: EntitySelectors<TEntity>
    callbacks?: EntityCallbacks<TEntity, TCreate, TUpdate>
    initialized: boolean
    mode: EntitiesFetchMode
  }

const DEFAULT_QUERY_DEBOUNCE_MS = 500

export const useEntityManager = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
  dataFilter,
  initialFilters,
  initialSorting,
  initialPaging,
  initialState,
  clientFiltersMatcher,
  options,
  selectors,
  initialized,
  mode,
}: EntityManagerInput<TEntity, TFilters, TCreate, TUpdate, TSorting>) => {
  const { getLabel } = useEntityManagerLabel()

  // ** state **
  const [query, setQuery] = useState("")
  const debouncedQuery = useDebounce(
    query,
    options?.queryFilterDebounceMs ?? DEFAULT_QUERY_DEBOUNCE_MS
  )
  const [textFilter, setTextFilter] = useState("")
  const [filters, setFilters] = useState(initialFilters)
  const [sorting, setSorting] = useState(initialSorting)
  const [paging, setPaging] = useState(initialPaging)
  const { createModalOpen, setCreateModalOpen } = useEntitiesCreateModal()
  const { uploadModalOpen, setUploadModalOpen } = useEntitiesUploadModal()
  const viewHistoryModal = useEntityViewHistoryModal<TEntity>()
  const viewJsonModal = useEntityViewJsonModal<TEntity>()
  const [editItemState, setEditItemState] =
    useState<LoadableStateWithInput<TEntity, TEntity, any>>()
  const [deleteItemState, setDeleteItemState] = useState<TEntity>()
  const [itemParentItemForCreateChild, setItemParentItemForCreateChild] =
    useState<TEntity>()
  const [itemExportState, setExportItemState] =
    useState<
      LoadableStateWithInput<TEntity, EntityExportInput<TEntity>, void>
    >()
  const [firstFetchCompleted, setFirstFetchCompleted] = useState(false)

  const {
    fetchItems,
    data: fetchData,
    state: fetchState,
  } = useEntityManagerFetchAction<
    TEntity,
    TFilters,
    TCreate,
    TUpdate,
    TSorting
  >({
    operations,
    mode,
    selectors,
    state: {
      filters,
      dataFilter,
      sorting,
      paging,
      query,
    },
  })

  const loadItems = async (params: EntitiesFetchParams) => {
    await fetchItems(params)
  }

  useEffect(() => {
    if (initialized) {
      loadItems({})
    }
  }, [filters, sorting, paging, debouncedQuery, initialized])

  useEffect(() => {
    if (fetchState.completed && !firstFetchCompleted) {
      setFirstFetchCompleted(true)
    }
  }, [fetchState.completed])

  const { getItem } = useEntityGetAction({
    operations,
  })

  const { deleteItem, deleteState } = useEntityDeleteAction({
    operations,
  })

  const { updateItem, updateState } = useEntityUpdateAction({
    operations,
  })

  const { createItem, createState } = useEntityCreateAction({
    operations,
  })

  const { exportItem } = useEntityExportAction({
    operations,
  })

  const { importSheet, importState } = useEntitySheetImportAction({
    operations,
  })

  const { parseSheet, parseState } = useEntitySheetParseAction({
    operations,
  })

  const { itemsExport, exportState } = useEntitySheetExportAction({
    operations,
  })

  const { sampleDownload, sampleDownloadState } = useEntitySampleDownloadAction(
    {
      operations,
    }
  )

  // import

  const itemsParse = async (
    input: EntitiesParseInput
  ): Promise<EntitiesParseResult> => {
    return await parseSheet(input)
  }

  const itemsImport = async (input: EntitiesImportInput) => {
    await importSheet(input, async () => await loadItems({}))
  }

  // update
  const itemUpdate = async (input: EntityUpdateInput<TUpdate>) => {
    await updateItem(input)
    await loadItems({
      context: {
        id: input.id,
        // parentId: selectors.getParentId?.(input.data),
        operationType: EntityDataRefreshType.Update,
      },
    })
  }

  // create
  const itemCreate = async (input: EntityCreateInput<TCreate>) => {
    await createItem(input)
    input.parent
    setCreateModalOpen(false)
    await loadItems({
      context: {
        parentId: input.parent?.id,
        operationType: EntityDataRefreshType.Create,
      },
    })
  }

  const openCreateModal = () => setCreateModalOpen(true)

  const closeCreateModal = async (options?: EntityCancelOptions) => {
    setCreateModalOpen(false)
    if (options?.refetch) {
      await loadItems({})
    }
  }

  // delete
  const itemDelete = async (input: EntityDeleteInput<TEntity>) => {
    await deleteItem(input)
    await loadItems({
      context: {
        id: selectors.getId(input.item),
        parentId: selectors.getParentId?.(input.item),
        operationType: EntityDataRefreshType.Delete,
      },
    })
  }

  // export item
  const itemExport = async (input: EntityExportInput<TEntity>) => {
    await uiRegistry().withNotification(
      async () => {
        try {
          setExportItemState(triggered(input))
          await exportItem(input)
          setExportItemState(
            triggerCompleted({
              input,
              data: undefined,
            })
          )
        } catch (error) {
          setExportItemState(
            triggerFailed({
              input,
            })
          )
          throw error
        }
      },
      {
        success: getLabel("operations:exportItem:messages:success"),
        error: getLabel("operations:exportItem:messages:genericError"),
        pending: getLabel("operations:exportItem:messages:inProgress"),
      }
    )
  }

  // ** data **
  const clientFilters: EntityClientFilters = {
    query: clientFiltersMatcher?.matchesQuery
      ? {
          value: textFilter,
          onChange: setTextFilter,
        }
      : undefined,
  }

  // fetch results for client side filtering (only list not tree)
  const fetchResults = useEntitiesFetchResults({
    data: fetchData as TEntity[] | EntityFilteredResult<TEntity>,
  })

  const data = fetchResults?.items
  const { filtered: filteredData } = useDataFilter<TEntity>({
    data,
    filter: (item) =>
      textFilter
        ? clientFiltersMatcher?.matchesQuery?.(item, textFilter) ?? true
        : true,
  })

  const openAction = useEntityOpenAction({
    operations,
    onItemUpdated: () => loadItems({}),
  })

  // EDIT
  const selectItemToEdit = async (item: TEntity) => {
    if (!operations.get) {
      setEditItemState(
        triggerCompleted({
          data: item,
          input: item,
        })
      )
      return
    }

    try {
      setEditItemState(triggered(item))
      const entity = await getItem({ item })
      setEditItemState(
        triggerCompleted({
          data: entity as TEntity,
          input: item,
        })
      )
    } catch (e) {
      console.error("Error fetching item", item)
      setEditItemState(
        triggerFailed({
          input: item,
        })
      )
    }
  }

  const closeEditModal = async (options?: EntityCancelOptions) => {
    setEditItemState(undefined)
    if (options?.refetch) {
      await loadItems({})
    }
  }

  const selectItemToDelete = (item: TEntity) => {
    setDeleteItemState(item)
  }

  const clearItemToDelete = () => {
    setDeleteItemState(undefined)
  }

  const selectItemParentItemForCreateChild = (item: TEntity) => {
    setItemParentItemForCreateChild(item)
  }

  const clearItemParentItemForCreateChild = () => {
    setItemParentItemForCreateChild(undefined)
  }

  const openUploadModal = () => setUploadModalOpen(true)
  const closeUploadModal = () => setUploadModalOpen(false)

  // ** actions **
  const actions: EntityActions<TEntity, TCreate, TUpdate> = {
    fetchState,
    itemUpdate,
    updateState,
    itemCreate,
    createState,
    itemsImport,
    importState,
    itemsParse,
    parseState,
    itemsExport,
    exportState,
    itemDelete,
    deleteState,
    openCreateModal,
    closeCreateModal,
    selectItemToEdit,
    closeEditModal,
    selectItemToDelete,
    clearItemToDelete,
    selectItemParentItemForCreateChild,
    clearItemParentItemForCreateChild,
    itemParentForCreateChild: itemParentItemForCreateChild,
    itemExport,
    itemExportState,
    selectItemToOpen: openAction?.selectItemToOpen,
    closeDetailsModal: openAction?.closeDetailsModal,
    openItemState: openAction?.openItemState,
    itemToDelete: deleteItemState,
    editItemState,
    fetchEntities: loadItems,
    openImportModal: openUploadModal,
    closeImportModal: closeUploadModal,
    sampleDownload,
    sampleDownloadState,
    closeViewHistoryModal: viewHistoryModal?.actions?.close,
    openViewHistoryModal: viewHistoryModal?.actions?.open,
    openItemJsonModal: viewJsonModal.actions.open,
    closeItemJsonModal: viewJsonModal.actions.close,
  }

  // ** ui **
  const ui: EntityUiState<TEntity> = {
    createModal: {
      open: createModalOpen,
    },
    uploadModal: {
      open: uploadModalOpen,
    },
    childCreateModal: {
      open: !!itemParentItemForCreateChild,
      item: itemParentItemForCreateChild,
    },
    deleteModal: {
      open: !!deleteItemState,
      item: deleteItemState,
    },
    viewDetailsModal: {
      open: false,
      item: undefined,
    },
    viewHistoryModal: viewHistoryModal.state,
    viewJsonModal: viewJsonModal.state,
  }

  const resetPaging = () => {
    setPaging(initialPaging)
  }

  const updateFilters = (filters: TFilters) => {
    setFilters({
      ...filters,
    })
    resetPaging()
  }

  const updateQuery = (query: string) => {
    setQuery(query)
    resetPaging()
  }

  const updateSorting = (sorting: EntitiesSortingValue<TSorting>) => {
    setSorting({
      ...sorting,
    })
  }

  const updatePaging = (paging: EntityPaging) => {
    setPaging({
      ...paging,
    })
  }

  useEffect(() => {
    if (initialState?.selectedItemDetails) {
      actions.selectItemToOpen(initialState.selectedItemDetails)
    }
  }, [initialState?.selectedItemDetails])

  useEffect(() => {
    if (initialState?.selectedItemToEdit) {
      actions.selectItemToEdit(initialState.selectedItemToEdit)
    }
  }, [initialState?.selectedItemToEdit])

  const dataEmpty = data && data?.length === 0

  return {
    fetchResults,
    data,
    filteredData,
    actions,
    ui,
    clientFilters,
    filters,
    sorting,
    paging,
    empty: dataEmpty ?? false,
    emptyDataSource: (!query && isFilterEmpty(filters) && dataEmpty) ?? false,
    firstFetchCompleted,
    updateFilters,
    updateSorting,
    updatePaging,
    updateQuery,
    query,
  }
}
