import { useOperation } from "@punks/ui-core"
import { EntityOperations } from "../types"
import { EntitiesParseInput } from "../types/operations"
import { EntitiesParseResult } from "../types/actions"

export interface EntitySheetParseActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
> {
  operations: EntityOperations<TEntity, TFilters, TCreate, TUpdate, TSorting>
}

export const useEntitySheetParseAction = <
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>({
  operations,
}: EntitySheetParseActionInput<
  TEntity,
  TFilters,
  TCreate,
  TUpdate,
  TSorting
>) => {
  const { invoke, state: parseState } = useOperation({
    operation: async (input: EntitiesParseInput) => {
      if (!operations.parse) {
        throw new Error("Parse sheets is not defined")
      }
      return await operations.parse.action(input)
    },
    onError: (error) => operations.parse?.onError?.(error),
    onSuccess: (result) => {
      operations.parse?.callback?.(result)
    },
  })

  const parseSheet = async (
    input: EntitiesParseInput,
    callback?: (result: EntitiesParseResult) => Promise<void>
  ): Promise<EntitiesParseResult> => {
    const result = await invoke(input)
    await callback?.(result!)
    return result!
  }

  return {
    parseSheet,
    parseState,
  }
}
