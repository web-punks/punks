import React from "react"
import "./preview.css"
import type { Preview } from "@storybook/react"
import {
  UiRootProvider,
  initializeRegistry,
  registerDefaultIcons,
  uiRegistry,
} from "@punks/ui-components"
import { ApiProvider } from "../src/__test__/infrastructure/api"

registerDefaultIcons(initializeRegistry(uiRegistry()))

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
  decorators: [
    (Story) => (
      <UiRootProvider>
        <ApiProvider>
          <div>
            <Story />
          </div>
        </ApiProvider>
      </UiRootProvider>
    ),
  ],
}

export default preview
